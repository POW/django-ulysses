Django==2.2.9
django-form-utils==1.0.3
django-tinymce==2.7.0
xlwt==1.3.0
xlrd==1.2.0
apidev_django-floppyforms==1.8.3
Fabric==2.4.0
factory_boy==2.12.0
# fake-factory==0.7.4
django-admin-sortable==2.2.3
beautifulsoup4==4.5.1
django-extensions==2.2.6
Pillow==7.0.0
django-registration==3.1
apidev-coop_colorbox==1.5.1
sorl-thumbnail==12.6.2
pycountry==19.8.18
django-celery==3.3.1
djangorestframework==3.11.0
django-recaptcha2==1.4.1
django-hijack==2.1.10
django-hijack-admin==2.1.10
pyelasticsearch==1.4.1
django-haystack==2.8.1
icalendar==4.0.4
-e .
