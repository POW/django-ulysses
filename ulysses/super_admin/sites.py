# -*- coding: utf-8 -*-

from django.conf import settings
from django.conf.urls import url
from django.contrib.auth.models import User, Group
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils.safestring import mark_safe

from ulysses.sites import BaseAdminSite

from ulysses.competitions.models import (
    Candidate, TemporaryMedia, TemporaryDocument, ApplicationDraft, CompetitionManager, JuryMember,
    CandidateDocument, CandidateMedia, Competition
)
from ulysses.composers.models import Composer, Media, Document, BiographicElement

from .forms import MergeComposerProfileForm


class SuperAdminSite(BaseAdminSite):
    # super admin site

    def get_objects_to_merge(self, user):
        model_classes1 = (
            Candidate, TemporaryMedia, TemporaryDocument, ApplicationDraft, Media, Document, BiographicElement,
        )
        model_classes2 = (
            JuryMember,
        )

        objects_to_merge = {}
        for model_class in model_classes1:

            try:
                model_name = model_class._meta.verbose_name
            except AttributeError:
                model_name = model_class.__name__

            for obj in model_class.objects.filter(composer__user=user):
                if model_name not in objects_to_merge:
                    objects_to_merge[model_name] = []
                objects_to_merge[model_name].append(obj)

        for model_class in model_classes2:

            try:
                model_name = model_class._meta.verbose_name
            except AttributeError:
                model_name = model_class.__name__

            for obj in model_class.objects.filter(user=user):
                if model_name not in objects_to_merge:
                    objects_to_merge[model_name] = []
                objects_to_merge[model_name].append(obj)

        return objects_to_merge

    # def has_permission(self, request):
    #     """
    #     Returns True if the given HttpRequest has permission to view
    #     *at least one* page in the admin site.
    #     """
    #     return request.user.is_active and request.user.is_superuser

    def each_context(self, request):
        """
        Returns a dictionary of variables to put in the template context for
        *every* page in the admin site.

        For sites running on a subpath, use the SCRIPT_NAME value if site_url
        hasn't been customized.
        """
        context = super(SuperAdminSite, self).each_context(request)
        context['super_admin_site'] = True
        return context

    def get_obj_name(self, obj):
        if isinstance(obj, Candidate):
            return '{0}'.format(obj.competition)
        else:
            return '{0}'.format(obj)

    def merge_profiles(self, request, user_id):

        user = get_object_or_404(User, id=user_id)

        objects_to_merge = self.get_objects_to_merge(user)

        candidate_files_models = (
            CandidateDocument, CandidateMedia
        )

        if request.method == 'POST':
            # If the form has been submitted...
            form = MergeComposerProfileForm(request.POST)
            if form.is_valid():

                profile = form.cleaned_data['profile']

                # Competition managers need admin access
                if user.is_staff:
                    profile.user.is_staff = True
                    profile.user.save()

                group_names = (settings.COMPETITION_ADMINS_GROUP, settings.JURY_MEMBERS_GROUP)
                for group_name in group_names:
                    try:
                        group = Group.objects.get(name=group_name)
                    except Group.DoesNotExist:
                        group = None
                    if group and group in user.groups.all():
                        profile.user.groups.add(group)
                        profile.user.save()

                # add new user as manager of source user competitions
                try:
                    manager_from = CompetitionManager.objects.get(user=user)
                    manager_to = CompetitionManager.objects.get_or_create(user=profile.user)[0]
                    for competition in Competition.objects.filter(managed_by=manager_from):
                        competition.managed_by.add(manager_to)
                        competition.save()
                except CompetitionManager.DoesNotExist:
                    pass

                new_composer = Composer.get_from_user(user=profile.user)

                objects_text = []
                for model_name, objects_list in list(objects_to_merge.items()):

                    for obj in objects_list:
                        if hasattr(obj, 'composer'):
                            obj.composer = new_composer
                        else:
                            obj.user = profile.user
                        obj.save()
                        if hasattr(obj, 'move_to_other_composer'):
                            obj.move_to_other_composer(new_composer)
                            obj.save()
                        objects_text.append('{0}: {1}'.format(model_name, self.get_obj_name(obj)))
                        if isinstance(obj, Candidate):
                            candidate = obj
                            for candidate_files_model in candidate_files_models:
                                for file_obj in candidate_files_model.objects.filter(candidate=candidate):
                                    file_obj.move_to_other_candidate(candidate)

                text = 'Fusion avec {0}.<br />{1} objet(s) réaffecté(s).<br />{2}'.format(
                    profile.user, len(objects_text), '<br />'.join(objects_text)
                )

                messages.success(request, mark_safe(text))

                url = reverse('admin:auth_user_change', args=[profile.user.id])
                return HttpResponseRedirect(url)
        else:
            form = MergeComposerProfileForm()

        objects_to_display = {}
        for model_name, list_of_objs in list(objects_to_merge.items()):
            objects_to_display[model_name] = [self.get_obj_name(obj) for obj in list_of_objs]

        context = {
            'form': form,
            'user': user,
            'objects_to_merge': list(objects_to_display.items()),
        }
        return render(request, 'admin/merge_profile.html', context)

    def get_urls(self):

        urls = super(SuperAdminSite, self).get_urls()

        custom_urls = [

            url(
                r'^merge-profile/(?P<user_id>\d+)/$',
                self.merge_profiles,
                name='super_admin_merge_profile'
            ),
        ]

        return urls + custom_urls


super_admin_site = SuperAdminSite()

