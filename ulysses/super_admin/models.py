# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db import models
from django.utils.translation import ugettext_lazy as _
from django.utils.safestring import mark_safe

from ulysses.profiles.utils import get_profile


class UlyssesUser(User):

    class Meta:
        proxy = True

    @property
    def profile(self):
        return get_profile(self)

    def __str__(self):
        if self.profile:
            return '{0}'.format(self.profile)
        else:
            return '{0}'.format(self.username)


class Shortcut(models.Model):

    name = models.CharField(max_length=100, verbose_name=_('name'))
    link = models.CharField(max_length=100, verbose_name=_('link'))
    order = models.IntegerField(default=0, verbose_name=_('order'))

    class Meta:
        verbose_name = _("Shortcut")
        verbose_name_plural = _("Shortcuts")
        ordering = ['order', 'name']

    def link_url(self):
        return mark_safe('<a href="{0}">{0}</a>'.format(self.link))
    link_url.short_description = _("Link")

    def __str__(self):
        return self.name
