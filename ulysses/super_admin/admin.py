# -*- coding: utf-8 -*-

from datetime import datetime, date, time, timedelta

from django.conf import settings
from django.contrib.admin import SimpleListFilter, ModelAdmin, BooleanFieldListFilter
from django.contrib.auth.admin import Group, GroupAdmin, User, UserAdmin
from django.contrib.sites.admin import Site, SiteAdmin
from django.contrib import messages
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.dateformat import DateFormat

try:
    from hijack_admin.admin import HijackUserAdmin
except ImportError:
    pass

from ulysses.profiles.utils import get_profile
from ulysses.utils import get_excel_response, get_excel_workbook
from .sites import super_admin_site
from .models import Shortcut, UlyssesUser


def merge_profile(modeladmin, request, queryset):
    if queryset.count() != 1:
        messages.error(request, 'Please select ONE and ONLY ONE user')
    else:
        user_id = queryset[0].id
        url = reverse('admin:super_admin_merge_profile', args=[user_id])
        return HttpResponseRedirect(url)

merge_profile.short_description = 'Merge with Ulysses profile'


class DuplicateUserListFilter(SimpleListFilter):
    parameter_name = 'duplicated'
    title = "Doublon"

    def lookups(self, request, model_admin):
        return [(1, 'Oui')]

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            duplicated_user = []
            email_user_map = {}
            name_user_map = {}

            for user in User.objects.all():
                name = "{0}####{1}".format(user.first_name, user.last_name)
                if name in name_user_map or user.email in email_user_map:
                    user_id_1 = name_user_map.get(name, 0)
                    user_id_2 = email_user_map.get(user.email, 0)
                    duplicated_user += [user.id]
                    duplicated_user += [user_id for user_id in (user_id_1, user_id_2) if user_id]
                else:
                    name_user_map[name] = user.id
                    email_user_map[user.email] = user.id

            return queryset.filter(id__in=duplicated_user)
        else:
            return queryset


class RecentlyJoindedUserListFilter(SimpleListFilter):
    parameter_name = 'recently_joined'
    title = "Recently joined"

    def lookups(self, request, model_admin):
        return [
            (1, "aujourd'hui"),
            (7, "7 jours"),
            (31, "30 jours"),
            (90, "90 jours"),
        ]

    def queryset(self, request, queryset):
        try:
            value = int(self.value())
        except (TypeError, ValueError):
            value = None
        if value:
            limit_date = date.today() - timedelta(value)
            limit_datetime = datetime.combine(limit_date, time(0, 0))
            return queryset.filter(date_joined__gte=limit_datetime)
        else:
            return queryset


class RecentAccessUserListFilter(SimpleListFilter):
    parameter_name = 'recent_access'
    title = "Recent access"

    def lookups(self, request, model_admin):
        return [
            (1, "aujourd'hui"),
            (7, "7 jours"),
            (31, "30 jours"),
            (90, "90 jours"),
        ]

    def queryset(self, request, queryset):
        try:
            value = int(self.value())
        except (TypeError, ValueError):
            value = None
        if value:
            limit_date = date.today() - timedelta(value)
            limit_datetime = datetime.combine(limit_date, time(0, 0))
            return queryset.filter(last_login__gte=limit_datetime)
        else:
            return queryset


class MembersListFilter(SimpleListFilter):
    parameter_name = 'members'
    title = "Members"

    def lookups(self, request, model_admin):
        return [
            (1, "Active members with profile"),
            (2, "Members without profile"),
        ]

    def queryset(self, request, queryset):
        try:
            value = int(self.value())
        except (TypeError, ValueError):
            value = None
        if value == 1:
            return queryset.filter(is_active=True).filter(
                Q(individual__isnull=False) | Q(organization__isnull=False),
                Q(individual__is_enabled=True) | Q(organization__is_enabled=True)
            )
        elif value == 2:
            return queryset.filter(is_active=True).filter(
                Q(individual__isnull=True) | Q(organization__isnull=True),
            )
        else:
            return queryset


class IsActiveListFilter(SimpleListFilter):
    parameter_name = 'is_active'
    title = 'Activated account (email confirmed)'

    def lookups(self, request, model_admin):
        return [
            (1, "Yes"),
            (0, "No"),
        ]

    def queryset(self, request, queryset):
        try:
            value = int(self.value())
        except (TypeError, ValueError):
            value = None
        if value == 1:
            return queryset.filter(is_active=True)
        elif value == 0:
            return queryset.filter(is_active=False)
        else:
            return queryset


def _date_to_str(value):
    if value:
        return DateFormat(value).format("d/m/Y H:i").capitalize()
    return ""


def export_users_to_excel(queryset):
    """
    Export the current change list to an Excel Workbook
    """
    column_headers = [
        'Email',
        'Lastname',
        'Firstname',
        'Organization',
        'Contact Name',
        'Country',
        'Town',
        'Is staff',
        'Subscription date',
        'Last connexion date',
    ]

    # if queryset.count() == 0:
    #     queryset = User.objects.all()

    lines = []
    for user in queryset:
        cells = []
        profile = get_profile(user)
        cells.append({'value': user.email})
        if profile:
            if profile.is_individual():
                cells.append({'value': user.last_name})
                cells.append({'value': user.first_name})
                cells.append({'value': ''})
                cells.append({'value': ''})
            else:
                cells.append({'value': ''})
                cells.append({'value': ''})
                cells.append({'value': user.last_name})
                cells.append({'value': profile.contact_name})
            cells.append({'value': profile.country.name if profile.country else ''})
            cells.append({'value': profile.city})
        else:
            cells.append({'value': user.last_name})
            cells.append({'value': user.first_name})
            cells.append({'value': ''})
            cells.append({'value': ''})
            cells.append({'value': ''})
            cells.append({'value': ''})

        cells.append({'value': 'Yes' if user.is_staff else 'No'})
        cells.append({'value': _date_to_str(user.date_joined), 'format': 'dd/mm/yyyy hh:mm'})
        cells.append({'value': _date_to_str(user.last_login), 'format': 'dd/mm/yyyy hh:mm'})
        lines.append({'cells': cells})
    wb = get_excel_workbook(column_headers, lines)
    return wb


def download_users_to_excel(modeladmin, request, queryset):
    wb = export_users_to_excel(queryset)
    return get_excel_response(wb)
download_users_to_excel.short_description = 'Export to excel'


def patched_user_admin_base_filters():
    filters = list(UserAdmin.list_filter)
    is_active_index = filters.index('is_active')
    if is_active_index > 0:
        filters[is_active_index] = IsActiveListFilter
    return filters


class CustomUserAdmin(HijackUserAdmin, UserAdmin):
    """Custom version of Django Admin"""
    list_display = UserAdmin.list_display + ('date_joined', 'last_login', 'is_duplicated', )
    if settings.ALLOW_HIJACK:
        list_display = list_display + ('hijack_field', )
    list_filter = patched_user_admin_base_filters() + [
        MembersListFilter, RecentlyJoindedUserListFilter, RecentAccessUserListFilter, DuplicateUserListFilter,
    ]
    readonly_fields = ['is_duplicated']
    actions = UserAdmin.actions + [merge_profile, download_users_to_excel]
    date_hierarchy = 'date_joined'
    ordering = ['-date_joined', ]

    def is_duplicated(self, user):
        return User.objects.filter(
            Q(email=user.email) | (Q(last_name=user.last_name) & Q(first_name=user.first_name))
        ).exclude(id=user.id).count() > 0
    is_duplicated.short_description = "Doublon"


class UlyssesUserAdmin(UserAdmin):
    """Custom version of Django Admin"""
    list_display = ('id', 'email', 'profile', 'is_active', 'is_duplicated', )
    list_filter = (
        RecentlyJoindedUserListFilter, RecentAccessUserListFilter, DuplicateUserListFilter,
        'is_active', 'is_staff', 'is_superuser',
    )
    readonly_fields = ['is_duplicated', 'profile',]
    actions = UserAdmin.actions + [merge_profile]
    search_fields = ('last_name', 'first_name', )
    ordering = ['-date_joined']
    date_hierarchy = 'date_joined'

    def is_duplicated(self, user):
        return User.objects.filter(
            Q(email=user.email) | (Q(last_name=user.last_name) & Q(first_name=user.first_name))
        ).exclude(id=user.id).count() > 0
    is_duplicated.short_description = "Doublon"


class ShortcutAdmin(ModelAdmin):
    list_display = ('name', 'link_url', 'order')
    list_editable = ('order', )


# Register default sites
super_admin_site.register(User, CustomUserAdmin)

super_admin_site.register(UlyssesUser, UlyssesUserAdmin)
super_admin_site.register(Group, GroupAdmin)
super_admin_site.register(Site, SiteAdmin)

super_admin_site.register(Shortcut, ShortcutAdmin)
