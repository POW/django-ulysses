
from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from ulysses.competitions.models import TemporaryMedia, CandidateMedia, TemporaryDocument, CandidateDocument
from ulysses.composers.models import Composer, Media, Document


class Command(BaseCommand):
    """ Creates a dynamic form for a competition (based on existing concrete application form) """
    help = "reassign_candidate_files"

    def add_arguments(self, parser):
        parser.add_argument('old_username')
        parser.add_argument('new_username')

    def handle(self, old_username, new_username, *args, **options):

        # old_username = "strauch"
        # new_username = "academie@ircam.fr"

        try:
            user = User.objects.get(username=new_username)
        except User.DoesNotExist:
            print('> user', new_username, 'not found')
            return

        try:
            composer = Composer.objects.get(user=user)
        except Composer.DoesNotExist:
            print('> composer not found')
            return

        composer_files_class = (TemporaryMedia, TemporaryDocument, Media, Document,)
        candidate_files_class = (CandidateMedia, CandidateDocument, )

        for model_class in composer_files_class:
            for obj in model_class.objects.filter(composer=composer):
                obj.reassign_to_other_composer(composer, old_username)

        for model_class in candidate_files_class:
            for obj in model_class.objects.filter(candidate__composer=composer):
                obj.reassign_to_other_candidate(obj.candidate, old_username)
