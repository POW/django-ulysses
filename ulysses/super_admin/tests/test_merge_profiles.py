# -*- coding: utf-8 -*-
"""unit tests"""

import os.path
from os.path import abspath, dirname

from django.core.management import call_command
from django.test.client import RequestFactory
from django.urls import reverse

from ulysses.competitions.factories import (
    CandidateFactory, TemporaryDocumentFactory, TemporaryMediaFactory, ApplicationDraftFactory, JuryMemberFactory,
    CompetitionManagerFactory, CompetitionFactory, DynamicApplicationFormFactory, CompetitionStepFactory
)
from ulysses.competitions.models import (
    CompetitionStatus, Candidate, TemporaryDocument, TemporaryMedia, ApplicationDraft, JuryMember, CompetitionManager,
    check_owner, CandidateMedia, CandidateDocument
)
from ulysses.composers.factories import (
    MediaFactory, DocumentFactory, ComposerFactory, BiographicElementFactory, UserFactory
)
from ulysses.composers.models import Media, Document, BiographicElement
from ulysses.profiles.factories import IndividualFactory
from ulysses.generic.tests import BaseTestCase
from ulysses.web.views.applications import (
    upload_as_temporary_file, get_temporary_file_url, upload_in_personal_space, get_composer_file_url
)


class MergeProfileTest(BaseTestCase):
    # fixtures = ['initial_data', 'initial_data_unittest', 'groups']
    fixtures = ['initial_data', 'groups', ]

    def create_temporary_file(self, user):
        base_dir = abspath(dirname(dirname(__file__)))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')
        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": user.username, "Filedata": file_to_upload})
        upload_request.user = user
        upload_response = upload_as_temporary_file(upload_request)
        return upload_response.content.decode('utf-8')

    def create_personal_file(self, user):
        base_dir = abspath(dirname(dirname(__file__)))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')
        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": user.username, "Filedata": file_to_upload})
        upload_request.user = user
        upload_response = upload_in_personal_space(upload_request)
        return upload_response.content.decode('utf-8')

    def test_merge_profile(self):
        """
        Test the access to the competitions app through the super-admin
        """

        super_user = UserFactory.create(is_superuser=True)
        super_user_profile = IndividualFactory.create(user=super_user)
        profile_from = IndividualFactory.create()
        profile_to = IndividualFactory.create()

        self.client.login(email=profile_from.user.email, password="1234")

        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=DynamicApplicationFormFactory.create(),
        )

        CompetitionStepFactory.create(competition=competition)

        tmp_score = self.create_temporary_file(profile_from.user)
        tmp_score_url = get_temporary_file_url(profile_from.user, tmp_score)

        tmp_file = self.create_temporary_file(profile_from.user)
        tmp_file_url = get_temporary_file_url(profile_from.user, tmp_file)

        candidate_score = self.create_temporary_file(profile_from.user)
        candidate_score_url = get_temporary_file_url(profile_from.user, candidate_score)

        candidate_file = self.create_temporary_file(profile_from.user)
        candidate_file_url = get_temporary_file_url(profile_from.user, candidate_file)

        personal_score = self.create_personal_file(profile_from.user)
        personal_score_url = get_composer_file_url(profile_from.user, personal_score)

        personal_file = self.create_personal_file(profile_from.user)
        personal_file_url = get_composer_file_url(profile_from.user, personal_file)

        # create objects of all types
        composer_from = ComposerFactory.create(user=profile_from.user)
        composer_to = ComposerFactory.create(user=profile_to.user)
        candidate_from = CandidateFactory.create(composer=composer_from, competition=competition)
        temp_media = TemporaryMediaFactory.create(
            composer=composer_from, competition=competition, score=tmp_score_url, key="media1"
        )
        temp_doc = TemporaryDocumentFactory.create(
            composer=composer_from, competition=competition, file=tmp_file_url, key="doc1"
        )

        temp_media2 = TemporaryMediaFactory.create(
            composer=composer_from, competition=competition, score=candidate_score_url, key="media2"
        )
        candidate_media = temp_media2.move_as_candidate_element(candidate_from)
        temp_doc2 = TemporaryDocumentFactory.create(
            composer=composer_from, competition=competition, file=candidate_file_url, key="doc2"
        )
        candidate_doc = temp_doc2.move_as_candidate_element(candidate_from)

        draft = ApplicationDraftFactory.create(composer=composer_from, competition=competition)
        media = MediaFactory.create(composer=composer_from, score=personal_score_url)
        doc = DocumentFactory.create(composer=composer_from, file=personal_file_url)
        bio_elt = BiographicElementFactory.create(composer=composer_from)
        jury = JuryMemberFactory.create(user=profile_from.user)
        mgr = CompetitionManagerFactory.create(user=profile_from.user)

        competition.managed_by.add(mgr)
        competition.save()

        self.client.logout()
        self.client.login(email=super_user_profile.user.email, password="1234")

        url = reverse('admin:super_admin_merge_profile', args=[profile_from.user_id])
        redirect_url = reverse('admin:auth_user_change', args=[profile_to.user.id])
        data = {'profile': profile_to.email}
        response = self.client.post(url, data)
        # self.assertRedirects(response, redirect_url)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['Location'], redirect_url)

        # refresh objects
        candidate_from = Candidate.objects.get(id=candidate_from.id)
        temp_media = TemporaryMedia.objects.get(id=temp_media.id)
        temp_doc = TemporaryDocument.objects.get(id=temp_doc.id)
        candidate_media = CandidateMedia.objects.get(id=candidate_media.id)
        candidate_doc = CandidateDocument.objects.get(id=candidate_doc.id)
        draft = ApplicationDraft.objects.get(id=draft.id)
        media = Media.objects.get(id=media.id)
        doc = Document.objects.get(id=doc.id)
        bio_elt = BiographicElement.objects.get(id=bio_elt.id)
        jury = JuryMember.objects.get(id=jury.id)
        mgr = CompetitionManager.objects.get(id=mgr.id)

        # check users have been merged into other profiles
        self.assertEqual(candidate_from.composer, composer_to)
        self.assertEqual(temp_media.composer, composer_to)
        self.assertEqual(temp_doc.composer, composer_to)
        self.assertEqual(candidate_media.candidate.composer, composer_to)
        self.assertEqual(candidate_doc.candidate.composer, composer_to)
        self.assertEqual(draft.composer, composer_to)
        self.assertEqual(media.composer, composer_to)
        self.assertEqual(doc.composer, composer_to)
        self.assertEqual(bio_elt.composer, composer_to)
        self.assertEqual(jury.user, composer_to.user)
        self.assertEqual(mgr.user, composer_from.user)
        new_manager = CompetitionManager.objects.get(user=composer_to.user)
        self.assertTrue(new_manager in competition.managed_by.all())

        # check files path belongs to new profile
        check_owner(temp_media.score, composer_to)
        check_owner(temp_doc.file, composer_to)
        check_owner(candidate_media.score, composer_to)
        check_owner(candidate_doc.file, composer_to)
        check_owner(media.score, composer_to)
        check_owner(doc.file, composer_to)

    def test_reassign_files(self):
        """
        Test the access to the competitions app through the super-admin
        """

        super_user = UserFactory.create(is_superuser=True)
        profile_from = IndividualFactory.create()
        profile_to = IndividualFactory.create()

        self.client.login(email=profile_from.user.email, password="1234")

        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=DynamicApplicationFormFactory.create(),
        )

        CompetitionStepFactory.create(competition=competition)

        tmp_score = self.create_temporary_file(profile_from.user)
        tmp_score_url = get_temporary_file_url(profile_from.user, tmp_score)

        tmp_file = self.create_temporary_file(profile_from.user)
        tmp_file_url = get_temporary_file_url(profile_from.user, tmp_file)

        candidate_score = self.create_temporary_file(profile_from.user)
        candidate_score_url = get_temporary_file_url(profile_from.user, candidate_score)

        candidate_file = self.create_temporary_file(profile_from.user)
        candidate_file_url = get_temporary_file_url(profile_from.user, candidate_file)

        personal_score = self.create_personal_file(profile_from.user)
        personal_score_url = get_composer_file_url(profile_from.user, personal_score)

        personal_file = self.create_personal_file(profile_from.user)
        personal_file_url = get_composer_file_url(profile_from.user, personal_file)

        # create objects of all types
        composer_from = ComposerFactory.create(user=profile_from.user)
        composer_to = ComposerFactory.create(user=profile_to.user)
        candidate_from = CandidateFactory.create(composer=composer_from, competition=competition)
        candidate_to = CandidateFactory.create(composer=composer_to, competition=competition)
        temp_media = TemporaryMediaFactory.create(
            composer=composer_to, competition=competition, score=tmp_score_url, key="media1"
        )
        temp_doc = TemporaryDocumentFactory.create(
            composer=composer_to, competition=competition, file=tmp_file_url, key="doc1"
        )

        temp_media2 = TemporaryMediaFactory.create(
            composer=composer_from, competition=competition, score=candidate_score_url, key="media2"
        )
        candidate_media = temp_media2.move_as_candidate_element(candidate_from)
        temp_doc2 = TemporaryDocumentFactory.create(
            composer=composer_from, competition=competition, file=candidate_file_url, key="doc2"
        )
        candidate_doc = temp_doc2.move_as_candidate_element(candidate_from)
        candidate_media.candidate = candidate_to
        candidate_media.save()
        candidate_doc.candidate = candidate_to
        candidate_doc.save()

        draft = ApplicationDraftFactory.create(composer=composer_to, competition=competition)
        media = MediaFactory.create(composer=composer_to, score=personal_score_url)
        doc = DocumentFactory.create(composer=composer_to, file=personal_file_url)

        self.client.logout()

        call_command('reassign_candidate_files', profile_from.user.username, profile_to.user.username)

        # refresh objects
        temp_media = TemporaryMedia.objects.get(id=temp_media.id)
        temp_doc = TemporaryDocument.objects.get(id=temp_doc.id)
        candidate_media = CandidateMedia.objects.get(id=candidate_media.id)
        candidate_doc = CandidateDocument.objects.get(id=candidate_doc.id)
        draft = ApplicationDraft.objects.get(id=draft.id)
        media = Media.objects.get(id=media.id)
        doc = Document.objects.get(id=doc.id)

        # check users have been moved to other profile
        self.assertEqual(temp_media.composer, composer_to)
        self.assertEqual(temp_doc.composer, composer_to)
        self.assertEqual(candidate_media.candidate.composer, composer_to)
        self.assertEqual(candidate_doc.candidate.composer, composer_to)
        self.assertEqual(draft.composer, composer_to)
        self.assertEqual(media.composer, composer_to)
        self.assertEqual(doc.composer, composer_to)

        # check files path belongs to new profile
        check_owner(temp_media.score, composer_to)
        check_owner(temp_doc.file, composer_to)
        check_owner(candidate_media.score, composer_to)
        check_owner(candidate_doc.file, composer_to)
        check_owner(media.score, composer_to)
        check_owner(doc.file, composer_to)
