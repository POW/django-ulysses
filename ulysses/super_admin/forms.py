# -*- coding: utf-8 -*-

from django.db.models import Q
from django import forms

from ulysses.profiles.models import Individual, Organization


class MergeComposerProfileForm(forms.Form):

    profile = forms.CharField(
        label='email or username',
        help_text='Adresse email ou username du profil vers lequel vous souhaitez transférer les données'
    )

    def clean_profile(self):
        profile = self.cleaned_data['profile']

        lookup = Q(user__email=profile) | Q(user__username=profile)

        try:
            profile = Individual.objects.get(lookup)
        except Individual.DoesNotExist:
            profile = None
        except Individual.MultipleObjectsReturned:
            raise forms.ValidationError("Plusieurs profils existent pour cette adresse")

        try:
            org_profile = Organization.objects.get(lookup)
        except Organization.DoesNotExist:
            org_profile = None
        except Organization.MultipleObjectsReturned:
            raise forms.ValidationError("Plusieurs profils existent pour cette adresse")

        if org_profile and profile:
            raise forms.ValidationError("Plusieurs profils existent pour cette adresse")

        profile = org_profile or profile
        if profile:
            return profile
        else:
            raise forms.ValidationError("Aucun profil pour cette adresse")





