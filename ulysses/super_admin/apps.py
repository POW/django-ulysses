# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.super_admin'
    label = 'ulysses_super_admin'
    verbose_name = "Ulysses SuperAdmin"
