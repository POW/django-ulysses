import factory

from .models import Partner


class PartnerFactory(factory.DjangoModelFactory):

    class Meta:
        model = Partner

    name = factory.Sequence(lambda n: 'partner-name-{0}'.format(n))
    url = factory.Sequence(lambda n: 'partner-url-{0}'.format(n))
