from django.conf import settings
from django.contrib import admin

from ulysses.partners.models import Partner
from ulysses.super_admin.sites import super_admin_site


class PartnerAdmin(admin.ModelAdmin):
    list_display = ['name', 'url', 'is_ulysses_partner']
    search_fields = ["name", ]
    list_filter = ['is_ulysses_partner', ]
    
    class Media:
        js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_pages.js' % settings.STATIC_URL
        )
    
# Global admin registration
super_admin_site.register(Partner, PartnerAdmin)
