# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _

from sorl.thumbnail import get_thumbnail

from ulysses.utils import text_overview


class Partner(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=200, unique=True)
    url = models.CharField(verbose_name=_("url"), max_length=200, unique=True)
    logo = models.ImageField(verbose_name=_("logo"), upload_to="logos", blank=True, null=True, max_length=200)
    presentation = models.TextField(verbose_name=_("presentation"), blank=True, null=True)
    is_ulysses_partner = models.BooleanField(
        verbose_name=_("is ulysses network partner"),default=False, help_text=_('displayed on Ulysses Network page')
    )

    def get_thumbnail(self, size='400', crop=None):
        """returns a small image"""
        if self.logo:
            thumbnail = get_thumbnail(self.logo, size, crop=crop)
            if thumbnail:
                return thumbnail.url

    def get_logo(self):
        """returns a small image"""
        return self.get_thumbnail(size="400")

    def is_external_url(self):
        return self.url.find('://') > 0

    def get_url(self):
        """returns link to partner page. It can be internal or external"""
        if self.is_external_url():
            return self.url

    def get_presentation_overview(self):
        return text_overview(self.presentation or "", 30)

    def __str__(self):
        return self.name
