# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.partners'
    label = 'ulysses_partners'
    verbose_name = "Ulysses Partners"
