# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-12 09:54


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_partners', '0002_auto_20170317_1354'),
    ]

    operations = [
        migrations.AddField(
            model_name='partner',
            name='is_ulysses_partner',
            field=models.BooleanField(default=False, help_text='displayed on Ulysses Network page', verbose_name='is ulysses network partner'),
        ),
    ]
