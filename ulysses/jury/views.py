# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.shortcuts import render, redirect, get_object_or_404
from django.template import loader
from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.competitions.forms import get_candidate_jury_form
from ulysses.competitions.models import (
    is_jury_member, Evaluation, EvaluationStatus, JuryMember,EvaluationField, EvaluationNote, Competition,
    CompetitionStep, Candidate, CandidateMedia, CandidateAttributeValue, CompetitionAttributeValue,
    JuryApplicationElementToExclude, CompetitionManager, CandidateDocument
)
from ulysses.competitions.utils import is_competition_admin
from ulysses.generic.emails import send_text_email
from ulysses.jury.forms import JuryProfileForm, CandidateFilterForm
from ulysses.utils import json_response, JsonResponse


def jury_member_login_required(view_func):
    """decorator for checking if user can access the jury site"""
    def _wrapped_view(request, *args, **kwargs):
        user = request.user
        if not user.is_authenticated:
            return redirect_to_login(
                request.get_full_path(),
                reverse('login')
            )
        elif (not is_jury_member(user)) and (not is_competition_admin(user)) and (not user.is_superuser):
            raise PermissionDenied
        return view_func(request, *args, **kwargs)
    return _wrapped_view


@jury_member_login_required
def show_empty_dashboard(request):
    params = {}
    return render(request, 'jury/dashboard.html', params)


@jury_member_login_required
def show_help(request,competition_url):
    competition = Competition.objects.get(url=competition_url)
    context = {
        "competition": competition,
    }
    return render(request, 'jury/help.html', context)


def get_evaluation_infos(evaluation, user, evaluation_form=None):
    """get context about evaluation"""
    params = {}
    # Load evaluation infos
    not_jury_member = evaluation.jury_member.user != user
    params['jury_member_name'] = evaluation.jury_member
    params["evaluation"] = evaluation
    params["evaluation_id"] = evaluation.id
    params["evaluation_status"] = '' if not_jury_member else evaluation.status.url
    active_step_id = evaluation.competition_step.id
    # Load candidate infos
    candidate = Candidate.objects.get(id=evaluation.candidate.id)
    params["candidate"] = candidate
    params["competition"] = evaluation.competition_step.competition
    params["competition_step"] = evaluation.competition_step
    candidate_form = get_candidate_jury_form(candidate)
    # Set 'jury_mode'
    candidate_form.jury_mode = True
    # Exclude fields if applicable
    fields_to_exclude = [
        elt.key
        for elt in JuryApplicationElementToExclude.objects.filter(competition__id=evaluation.candidate.competition.id)
    ]
    params["sections"] = candidate_form.get_non_media_fieldsets(fields_to_exclude)

    media_fields = candidate_form.get_media_fields()
    media_elements = []
    default_media = None
    for (field_name, field_label, is_media) in media_fields:
        if is_media:
            if CandidateMedia.objects.filter(candidate=candidate, key=field_name).exists():
                media = CandidateMedia.objects.get(candidate=candidate, key=field_name)
                media_element = {'label': field_label, 'media': media}
                media_elements.append(media_element)
                # Default media = first media with audio file (if it exists)
                if media.audio and not default_media:
                    default_media = media_element
        else:
            if CandidateDocument.objects.filter(candidate=candidate, key=field_name).exists():
                doc = CandidateDocument.objects.get(candidate=candidate, key=field_name)
                doc_element = {'label': field_label, 'doc': doc}
                media_elements.append(doc_element)
    params["media_elements"] = media_elements
    params["default_media"] = default_media
    # Prepare dynamic evaluation form
    if not evaluation_form:
        evaluation_form = evaluation.get_form(read_only=not_jury_member, empty=not_jury_member)
    params["evaluation_form"] = evaluation_form
    params["as_jury_member"] = not not_jury_member
    # Return params
    return params


@jury_member_login_required
def show_dashboard(request):

    params = {}
    user = request.user

    # Get current jury member
    try:
        jury_member = JuryMember.objects.get(user=user)
    except JuryMember.DoesNotExist:
        jury_member = None

    is_competition_manager = CompetitionManager.objects.filter(user=user).exists()

    # Request can contain three parameters :
    # step : the #ID of a CompetitionStep (not mandatory,
    # if omittted, we select the first competition step found for this jury member)
    # status : the url of an EvaluationStatus or 'all' (if omitted, default to 'all')
    # candidate : the #ID a candidate to display

    # Determine active step
    if user.is_superuser:
        allowed_competition_steps_ids = [
            step.id
            for step in CompetitionStep.objects.filter(is_open=True)  # .exclude(competition__status__name="archived")
        ]
    else:
        allowed_competition_steps_ids = []
        if is_competition_manager:
            allowed_competition_steps_ids += [
                step.id
                for step in CompetitionStep.objects.filter(
                    is_open=True, competition__managed_by__user=user
                )
            ]
        if jury_member:
            jury_competition_steps_ids = [
                item['competition_step']
                for item in Evaluation.objects.filter(jury_member=jury_member).values('competition_step').distinct()
            ]
            allowed_competition_steps_ids += [
                step.id for step in CompetitionStep.objects.filter(is_open=True, id__in=jury_competition_steps_ids)
            ]

        if not is_competition_manager and not jury_member:
            raise PermissionDenied

    if len(allowed_competition_steps_ids) == 0:
        return show_empty_dashboard(request)

    if "step" in request.GET:
        active_step_id = int(request.GET["step"])
        if active_step_id not in allowed_competition_steps_ids:
            raise PermissionDenied("You are not a jury member for competition step #%s" % active_step_id)
    else:
        competitions_steps = CompetitionStep.objects.filter(
            id__in=allowed_competition_steps_ids
        ).order_by('competition__title', 'name')
        active_step_id = competitions_steps[0].id

    active_competition_step = CompetitionStep.objects.get(id=active_step_id)
    other_competition_steps = CompetitionStep.objects.filter(
        id__in=allowed_competition_steps_ids
    ).exclude(
        id=active_step_id
    ).order_by('competition__title', 'name')
    params["step_filter"] = "step=%s" % active_competition_step.id
    params["active_competition_step"] = active_competition_step
    params["other_competitions_steps"] = other_competition_steps

    as_jury_member = (jury_member is not None) and (
        Evaluation.objects.filter(competition_step=active_competition_step, jury_member=jury_member).exists()
    )

    evaluation_keys = active_competition_step.get_evaluation_keys()

    filter_choices = [
        (attribute.id, attribute.value)
        for attribute in CompetitionAttributeValue.objects.filter(
            attribute__competition=active_competition_step.competition,
            is_filter=True,
        )
    ]
    if filter_choices:
        filter_choices = [(0, '')] + filter_choices

    # Determine evaluations status filter
    if "status" in request.GET:
        status_url = request.GET["status"]
        if not status_url in [status.url for status in EvaluationStatus.objects.all()] + ["all"]:
            raise RuntimeError("Invalid status url : %s" % status_url)
    else:
        status_url = "all"

    # If the jury member posted the form, we start by saving the evaluation
    # Nota : we must do that before determining evaluations to display,
    # since saving the evaluation can move evaluation status
    extra_filter_candidate_ids = None
    candidate_attribute = None
    evaluation_form = None
    posted_candidate_id = None
    on_filter = False
    if filter_choices:
        params['filter_form'] = CandidateFilterForm(filter_choices)

    if request.method == 'POST':  # If the form has been submitted...
        if "attribute_filter" in request.POST:
            # Jury attribute filter auto-post
            attribute_filter = request.POST["attribute_filter"]
            candidate_attribute = int(request.POST["candidate-attribute"])
            if candidate_attribute > 0:
                extra_filter_candidate_ids = [
                    item["candidate"] for item in CandidateAttributeValue.objects.filter(
                        attribute=attribute_filter, value__id=candidate_attribute
                    ).values("candidate")
                ]
        elif "filters" in request.POST:
            # remove the current candidate
            on_filter = True

            # Jury attribute filter auto-post
            if filter_choices:
                filter_form = CandidateFilterForm(filter_choices, request.POST)
                params['filter_form'] = filter_form
                if filter_form.is_valid():
                    filter_id = filter_form.cleaned_data['filters']
                    try:
                        filter_id = int(filter_id)
                    except ValueError:
                        filter_id = 0
                    if filter_id:
                        extra_filter_candidate_ids = [
                            item["candidate"] for item in CandidateAttributeValue.objects.filter(
                                value__id=filter_id
                            ).values("candidate")
                        ]
                        if not extra_filter_candidate_ids:
                            # If no candidate: we must force the query to return nothing
                            # add a dummy id in the list
                            extra_filter_candidate_ids = [0]
        else:
            # Evaluation save post

            if not jury_member:
                raise PermissionDenied

            jury_says_completed = "is_completed" in request.POST
            posted_candidate_id = int(request.POST["candidate_id"])
            evaluation_form_class = active_competition_step.get_evaluation_form_class(
                jury_says_completed, active_competition_step.competition, active_competition_step
            )
            evaluation_form = evaluation_form_class(
                active_competition_step.get_evaluation_fields(), request.POST
            )
            if evaluation_form.is_valid():
                evaluation = Evaluation.objects.get(
                    competition_step=active_competition_step, jury_member=jury_member, candidate__id=posted_candidate_id
                )
                # Save evaluation values
                in_progress = False
                for key in evaluation_keys:
                    val = evaluation_form.cleaned_data[key]
                    if val:
                        in_progress = True
                        if evaluation.has_note(key):
                            evaluation.change_note(key, val)
                        else:
                            evaluation.set_note(key, val)
                # Set evaluation status
                was_completed = evaluation.status and evaluation.status.url == 'completed'
                if evaluation_form.cleaned_data["is_completed"]:
                    evaluation.status = EvaluationStatus.objects.get(url="completed")
                else:
                    if in_progress:
                        evaluation.status = EvaluationStatus.objects.get(url="in_progress")
                    else:
                        evaluation.status = EvaluationStatus.objects.get(url="to_process")
                evaluation.recommendation = evaluation_form.cleaned_data["recommendation"]
                # Save changes
                evaluation.save()
                if was_completed:
                    admins_emails = [
                        manager.user.email
                        for manager in evaluation.competition_step.competition.managed_by.all()
                    ]
                    context = {
                        'evaluation': evaluation,
                        'competition': evaluation.competition_step.competition,
                        'competition_step': evaluation.competition_step,
                        'candidate': evaluation.candidate,
                        'jury_member': evaluation.jury_member,
                    }
                    the_template = loader.get_template('jury/evaluation_changed_notification.txt')
                    body = the_template.render(context)
                    send_text_email(
                        _('A jury member has changed their evaluation'),
                        body,
                        settings.DEFAULT_FROM_EMAIL,
                        admins_emails
                    )

                params["message"] = _("Your changes have been saved")
            else:
                params["error_message"] = _("Please correct the errors below")

    # Determine evaluations to display
    filters = {
        "competition_step": active_competition_step,
    }
    is_filtered = False
    if as_jury_member:
        filters["jury_member"] = jury_member
    if status_url != "all":
        is_filtered = True
        filters["status__url"] = status_url
    if extra_filter_candidate_ids:
        is_filtered = True
        filters["candidate__id__in"] = extra_filter_candidate_ids
    evaluations = Evaluation.objects.filter(**filters).order_by("candidate__composer__user__last_name")
    if not as_jury_member:
        evaluations_composers = [
            item['candidate__composer'] for item in evaluations.values('candidate__composer').distinct()
        ]
        evaluations_list = []
        for composer in evaluations_composers:
            # Keep only 1 evaluation for each composer
            try:
                evaluations_list.append(evaluations.filter(candidate__composer__id=composer)[0])
            except IndexError:
                pass
        evaluations = evaluations_list

    evaluations_data = [{'obj': obj, 'id': obj.id} for obj in evaluations]
    count = len(evaluations_data)
    evaluation_list = []
    i = 0
    for item in evaluations_data:
        # Determine previous evaluation id
        if i == 0:
            prev_id = 0
        else:
            prev_id = evaluations_data[i-1]["id"]
        # Determine next evaluation id
        if i == count-1:
            next_id = 0
        else:
            next_id = evaluations_data[i+1]["id"]
        evaluation_list .append(
            {
                'evaluation': item["obj"],
                'id': item["id"],
                'next': next_id,
                'prev': prev_id,
                'index': i + 1
            }
        )
        i += 1
    params['is_filtered'] = is_filtered
    params["evaluation_list"] = evaluation_list
    params["status_url"] = status_url

    # Compute evaluation statistics
    if as_jury_member:
        params["total_count"] = Evaluation.objects.filter(
            competition_step=active_competition_step, jury_member=jury_member
        ).count()
        params["to_evaluate_count"] = Evaluation.objects.filter(
            competition_step=active_competition_step,
            jury_member=jury_member, status__url="to_process"
        ).count()
        params["in_progress_count"] = Evaluation.objects.filter(
            competition_step=active_competition_step,
            jury_member=jury_member, status__url="in_progress"
        ).count()
        params["evaluated_count"] = Evaluation.objects.filter(
            competition_step=active_competition_step,
            jury_member=jury_member, status__url="completed"
        ).count()
    # else:
    #     params["total_count"] = Evaluation.objects.filter(
    #         competition_step=active_competition_step
    #     ).count()
    #     params["to_evaluate_count"] = Evaluation.objects.filter(
    #         competition_step=active_competition_step, status__url="to_process"
    #     ).count()
    #     params["in_progress_count"] = Evaluation.objects.filter(
    #         competition_step=active_competition_step, status__url="in_progress"
    #     ).count()
    #     params["evaluated_count"] = Evaluation.objects.filter(
    #         competition_step=active_competition_step, status__url="completed"
    #     ).count()

    # Compute note statistics
    evaluation_fields = EvaluationField.objects.filter(
        competition_step=active_competition_step, display_in_result_list=True
    )
    evaluation_stats = {}
    if len(evaluation_fields) == 1:
        # Case n°1 :  single note model and possible values choices <=4
        ef = evaluation_fields[0]
        if ef.field_type == "forms.ChoiceField":
            field_args = eval(ef.field_extra_args)
            choices = field_args["choices"]
            if len(choices) <= 4:
                for (key, value) in choices:
                    evaluation_stats[key] = EvaluationNote.objects.filter(
                        evaluation__id__in=[ev.id for ev in evaluations],
                        key=ef.key, value=value
                    ).count()
    elif EvaluationField.objects.filter(competition_step=active_competition_step, use_in_stats=True).count() == 1:
        # Case n°2 : use_in_stat field (works only if one and
        # only one use_in_stat field is defined)
        stat_field = EvaluationField.objects.get(
            competition_step=active_competition_step, use_in_stats=True
        )
        if as_jury_member:
            completed_evals = Evaluation.objects.filter(
                competition_step=active_competition_step, jury_member=jury_member, status__url="completed"
            )
        else:
            completed_evals = Evaluation.objects.filter(
                competition_step=active_competition_step, status__url="completed"
            )
        # Competition specific : for Acanthes competitions,
        # we take in account only completed evals for which
        # evaluation field "note=3" (jury) or "decision=1" (final-choice)
        if active_competition_step.competition.url in ["acanthes-composers", "acanthes-performers"]:
            completed_evals_ids = [ce.id for ce in completed_evals]
            if active_competition_step.url == "jury":
                positive_evaluations_ids = [
                    item["evaluation"]
                    for item in EvaluationNote.objects.filter(
                        evaluation__id__in=completed_evals_ids, key="note", value="3").values("evaluation").distinct()
                ]
            elif active_competition_step.url == "final-choice":
                positive_evaluations_ids = [
                    item["evaluation"]
                    for item in EvaluationNote.objects.filter(
                        evaluation__id__in=completed_evals_ids, key="decision", value="1"
                    ).values("evaluation").distinct()
                ]
            completed_evals = completed_evals.filter(id__in=positive_evaluations_ids)

        # End competition specific
        for completed_eval in completed_evals:
            note = EvaluationNote.objects.get(
                evaluation=completed_eval, key=stat_field.key).acronym_value()
            if not note in evaluation_stats:
                evaluation_stats[note] = 1
            else:
                evaluation_stats[note] += 1
    params["evaluation_stats"] = evaluation_stats

    # Set candidate attribute filter values (if applicable)
    if active_competition_step.candidate_attribute_jury_filter:
        # Get values
        ids = []
        if as_jury_member:
            list_of_evaluations = Evaluation.objects.filter(
                competition_step=active_competition_step, jury_member=jury_member
            )
        else:
            list_of_evaluations = Evaluation.objects.none()
        for evaluation in list_of_evaluations:
            cav = CandidateAttributeValue.objects.get(
                candidate=evaluation.candidate,
                attribute=active_competition_step.candidate_attribute_jury_filter
            )
            competition_attribute_value = cav.value
            if not competition_attribute_value.id in ids:
                ids.append(competition_attribute_value.id)

        # Prepare results
        if len(ids) > 0:
            candidate_attribute_jury_filter_values = [{'id': 0, 'name': '--------'}]
            for attr_id in ids:
                competition_attr = CompetitionAttributeValue.objects.get(id=attr_id)
                candidate_attribute_jury_filter_values.append(
                    {
                        'id': competition_attr.id,
                        'name': competition_attr.acronym,
                        'selected': candidate_attribute == competition_attr.id
                    }
                )
            params["candidate_attribute_jury_filter_values"] = candidate_attribute_jury_filter_values
            params["attribute_filter"] = active_competition_step.candidate_attribute_jury_filter

    # Determine candidate to display
    candidate_id = None
    if posted_candidate_id:
        candidate_id = posted_candidate_id

    elif on_filter:
        if len(evaluations):
            candidate_id = evaluations[0].candidate.id

    elif "candidate" in request.GET and as_jury_member:
        candidate_id = int(request.GET["candidate"])
        if status_url != "all":
            evaluation_queryset = Evaluation.objects.filter(
                competition_step=active_competition_step,
                jury_member=jury_member,
                candidate__id=candidate_id,
                status__url=status_url
            )
            if not evaluation_queryset.exists():
                # The candidate does not exist anymore in select status. Redirect.
                url = "%s?step=%s&status=%s" % (reverse("jury:dashboard"), active_competition_step.id, status_url)
                return redirect(url)
    else:
        if as_jury_member and len(evaluations) > 0:
            candidate_id = evaluations[0].candidate.id
            # Redirect to candidate display
            url = "%s?step=%s&status=%s&candidate=%s" % (
                reverse("jury:dashboard"), active_competition_step.id, status_url, candidate_id
            )
            return redirect(url)

    params["as_jury_member"] = as_jury_member
    # Display candidate information, applicable
    # Prepare application form data
    # Left column : 1-to-n sections (each section contain 1-to-n 'non' media fields)
    # Right column : 1-to-n medias
    if candidate_id and as_jury_member:
        evaluation_queryset = Evaluation.objects.filter(
            competition_step=active_competition_step, jury_member=jury_member, candidate__id=candidate_id
        )
        if not evaluation_queryset.exists():
            raise PermissionDenied("You are not allowed to evaluate candidate #%s" % candidate_id)
        evaluation = Evaluation.objects.get(
            competition_step=active_competition_step,
            jury_member=jury_member,
            candidate__id=candidate_id
        )
        params.update(get_evaluation_infos(evaluation, user, evaluation_form))

    return render(request, 'jury/dashboard.html', params)


@jury_member_login_required
@json_response
def reload_evaluation(request):
    evaluation_id = request.GET["id"]
    evaluation = Evaluation.objects.get(id=evaluation_id)
    params = get_evaluation_infos(evaluation, request.user)
    # Prepare evaluated template
    kwargs = {
        "context": params,
        "request": request,
        "template_name": "jury/candidate.html",
    }
    html = loader.render_to_string(**kwargs)
    return html


@jury_member_login_required
def edit_profile(request):

    params = {}
    jm = JuryMember.objects.get(user=request.user)

    if request.method == 'POST': # If the form has been submitted...
        form = JuryProfileForm(request.POST)
        if form.is_valid():
            # Save changes & redirect to jury dashboard
            jm.user.first_name = form.cleaned_data["first_name"]
            jm.user.last_name = form.cleaned_data["last_name"]
            jm.user.email = form.cleaned_data["email"]
            jm.user.save()
            jm.save()
            return redirect(reverse("jury:dashboard"))
    else:
        form = JuryProfileForm(
            initial={
                'first_name': jm.user.first_name,
                'last_name': jm.user.last_name,
                'email': jm.user.email
            }
        )

    params["form"] = form
    return render(request, 'registration/jury/profile_form.html', params)


@jury_member_login_required
def candidate_history(request):
    """show any notes given by a jury to a composer"""
    candidate_id = request.GET.get("id", 0)
    competition_id = request.GET.get("competition_id", 0)
    candidate = get_object_or_404(
        Candidate,
        id=candidate_id,
        competition__display_candidate_name=True,
        competition__allow_access_to_jury_history=True,
    )

    competition = get_object_or_404(Competition, id=competition_id)

    evaluations = Evaluation.objects.filter(
        candidate__composer=candidate.composer,
        candidate__competition__display_candidate_name=True,
        jury_member__user=request.user
    ).exclude(candidate__competition__id=competition_id).order_by('-id')

    try:
        current_evaluation = Evaluation.objects.filter(
            candidate__composer=candidate.composer,
            jury_member__user=request.user,
            candidate__competition__id=competition_id
        ).order_by('-id')[0]
    except IndexError:
        current_evaluation = None

    params = {
        'candidate': candidate,
        'competition': competition,
        'evaluations': evaluations,
        'current_evaluation': current_evaluation,
    }

    kwargs = {
        "context": params,
        "request": request,
        "template_name": "jury/candidate_history.html",
    }

    html = loader.render_to_string(**kwargs)
    return JsonResponse(html)


@jury_member_login_required
def candidate_previous_steps(request):
    """show all notes received on previous steps"""
    candidate_id = request.GET.get("id", 0)
    competition_id = request.GET.get("competition_id", 0)
    step_id = request.GET.get("step_id", 0)
    candidate = get_object_or_404(Candidate, id=candidate_id)
    competition = get_object_or_404(Competition, id=competition_id)
    step = get_object_or_404(CompetitionStep, id=step_id, competition=competition)

    previous_evaluations = Evaluation.objects.filter(
        competition_step__can_access_evaluations=True,
        competition_step=step,
        candidate=candidate,
        status__name="completed"
    )

    try:
        current_evaluation = Evaluation.objects.filter(
            candidate__composer=candidate.composer,
            jury_member__user=request.user,
            candidate__competition=competition
        ).order_by('-id')[0]
    except IndexError:
        raise PermissionDenied

    params = {
        'competition': competition,
        'candidate': candidate,
        'previous_evaluations': previous_evaluations,
        'current_evaluation': current_evaluation,
    }

    kwargs = {
        "context": params,
        "request": request,
        "template_name": "jury/candidate_previous_steps.html",
    }

    html = loader.render_to_string(**kwargs)
    return JsonResponse(html)
