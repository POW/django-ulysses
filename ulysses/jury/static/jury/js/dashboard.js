function play_audio(label,title,url){
	$("#player-title").text(label + " : " + title);
	var player, new_source;
	player = $("#player");
	player.attr("autoplay","");
	player.empty();
	new_source = $("<source>").attr("src", url).appendTo(player);
	player[0].pause();
	player[0].load();
	$("#song-url").attr("href", url);

}

(function( $ ){
  "use strict";

  $.fn.dashboard= function(opts) {

    var options = $.extend({}, $.fn.dashboard.defaults, opts);

    function setAside(sidebarBody, offset) {
      if ($(window).width() > 767) {  // only for desktop
        var height = $(window).height() - offset;
        sidebarBody.height(height);
        $('.main-col').css('min-height', height);
      }
    }


    function myAddListener(obj, evt, handler, captures)
    {
      if ( document.addEventListener )
        obj.addEventListener(evt, handler, captures);
      else
        // IE
        obj.attachEvent('on' + evt, handler);
    }

    function RegisterListener(eventName, objID, embedID, listenerFcn)
          {
      var obj = document.getElementById(objID);
      if ( !obj )
        obj = document.getElementById(embedID);
      if ( obj )
        myAddListener(obj, eventName, listenerFcn, false);
    }

    function showProgress() {
      var percentLoaded = 0 ;
      var player = document.getElementById("player")
      percentLoaded = parseInt((player.GetMaxTimeLoaded() / player.GetDuration()) * 100);
      /* console.debug('Loading: ' + percentLoaded + '% complete...'); */
       $("#player-status").text('Loading (' + percentLoaded + '%)...');
    }

    function movieLoaded() {
      /* console.debug("Movie Loaded"); */
      $("#player-status").text("");
    }

    function RegisterListeners()
    {
      RegisterListener('qt_load', 'player_obj', 'player', movieLoaded);
      RegisterListener('qt_progress', 'player_obj', 'player', showProgress);
    }

    function display_qt_status()
    {
      /* Have Quicktime. Not used? */

      var haveqt = false;

      if (navigator.plugins) {
        for (i=0; i < navigator.plugins.length; i++ ) {
          if (navigator.plugins[i].name.indexOf("QuickTime") >= 0) {
            haveqt = true;
          }
        }
      }

      if (
        (navigator.appVersion.indexOf("Mac") > 0) && (navigator.appName.substring(0, 9) == "Microsoft") &&
        (parseInt(navigator.appVersion) < 5)
      ) {
        haveqt = true;
      }

      return haveqt
    }

    function get_checkbox_status(cb) {
      var checked = false;
      if ($(cb).prop("checked")) {
        checked = true;
      }
      return checked;
    }

    function save_initial_form_values() {

      $("#evaluation_form textarea,select").each(function (i) {
          $(this).data('initial', $(this).val());
      });

      $("#evaluation_form input[type='checkbox'],input[type='radio']").each(function (i) {
          $(this).data('initial', get_checkbox_status($(this)));
      });
    }

    function evaluation_changed() {

      var result = false;

      $("#evaluation_form textarea,select").each(function (i) {
          if ($(this).attr("id") != 'id_filters') {
            if ($(this).val() != $(this).data('initial')) {
              result = true;
            }
          }
      });

      $("#evaluation_form input[type='checkbox'],input[type='radio']").each(function (i) {
        if (get_checkbox_status($(this)) != $(this).data('initial')) {
          result = true;
        }
      });

      return result;
    }

    function reload_evaluation(id){
      var url;

      if (evaluation_changed()){
        if (!confirm("Change candidate without saving current evaluation ?")) {
          return;
        }
      }

      url = "/jury/reload_evaluation/?id=" + id;
      $.getJSON(url, function(html){
        $(".main-col").html(html);
        bind_dashboard_events();
        bind_candidate_events();
        $('ul.candidates-list li.active').removeClass('active');
        $('ul.candidates-list li a[data-id="' + id + '"]').closest('li').addClass('active');
      });
    }

    function reload_history(id, competition_id){
      var url;

      if (evaluation_changed()){
        if (!confirm("View history without saving current evaluation ?")) {
          return;
        }
      }

      url = "/jury/candidate-history/?id=" + id + "&competition_id=" + competition_id;
      $.getJSON(url, function(html){
        $(".main-col").html(html);
        bind_candidate_events();
      });
    }

    function reload_previous_steps(id, competition_id, step_id){
      var url;

      if (evaluation_changed()){
        if (!confirm("View previous steps without saving current evaluation ?")) {
          return;
        }
      }

      url = "/jury/candidate-previous-steps/?id=" + id + "&competition_id=" + competition_id + "+&step_id=" + step_id;
      $.getJSON(url, function(html){
        $(".main-col").html(html);
        bind_candidate_events();
      });
    }

    function get_candidate_link(button) {
      var current_eval_id ,filter_string;
      current_eval_id = $(button).data("id");
      filter_string = "[data-id='"+current_eval_id+"']";
      return $("ul.candidates-list").find(filter_string)[0];
    }

    $(document).on('click', ".candidate-link", function(event){
      console.log(".candidate-link", $(this).data("id"));
      reload_evaluation($(this).data("id"));
      return false;
    });

    $(document).on('click', ".history-link", function(event){
      reload_history($(this).data("id"), $(this).data("competition_id"));
      return false;
    });

    $(document).on('click', ".previous-steps-link", function(event){
      reload_previous_steps($(this).data("id"), $(this).data("competition_id"), $(this).data("step_id"));
      return false;
    });

    $(document).on('click', "#next-link", function(event){
      var next_id;
      next_id = $(get_candidate_link($(this))).data("next");
      if (next_id > 0) {
        reload_evaluation(next_id);
      }
      return false;
    });
    
    $(document).on('click', "#previous-link", function(event){
      var prev_id;
      prev_id = $(get_candidate_link($(this))).data("prev");
      if (prev_id > 0) {
        reload_evaluation(prev_id);
      }
      return false;
    });

    function bind_dashboard_events() {
      var lastChangeValue = null;
      var freeze = false;
      $(".auto-submit select").on('focus', function () {
          // Store the current value on focus and on change
          if (!freeze) {
            lastChangeValue = $(this).val();
          }
      }).change(function() {
          if (evaluation_changed()){
            freeze = true;
            if (!confirm("Change candidate without saving current evaluation ?")) {
              $(this).val(lastChangeValue);
              freeze = false;
              return;
            }
          }
          $(".auto-submit").submit();
        });

    }

  function bind_candidate_events() {

      // Update candidate counter display
      var current_eval_id, filter_string, candidate_link;
      $("#candidate-count").text($("ul.candidates-list").children().length);
      current_eval_id = $("#candidate-index").data("id");
      filter_string = "[data-id='" + current_eval_id + "']";
      candidate_link = $("ul.candidates-list").find(filter_string)[0];
      $("#candidate-index").text($(candidate_link).data("index"));

      var aFold = $("a.fold").click(function() {
          $(this).hide();
          $(this).closest('.section').find('a.unfold').show();
          $(this).closest('.section').find('.section-body').hide();
          return false
      });
      aFold.click();

      $("a.unfold").click(function() {
          $(this).hide();
          $(this).closest('.section').find('a.fold').show();
          $(this).closest('.section').find('.section-body').show();
          return false
      });

      $('.section-header').click(function() {
          $(this).find('a:visible').click();
          return false
      });

      var keepUnfolding = true;
      $('.section-header').each(function(idx, elt) {
          if (!keepUnfolding) {
            return;
          }
          var $elt = $(elt);
          var $content = $('.candidate-content');
          console.log('>', idx, $content.height(), $(window).height());
          if ($content.height() < $(window).height()) {
            $elt.find("a.unfold").click();
          }
          console.log('<', idx, $content.height(), $(window).height());
          if ($content.height() > $(window).height()) {
            $elt.find("a.fold").click();
            keepUnfolding = false;
          }
      });

      $ ("#submit").click( function(event){
          if (!evaluation_changed()){
            event.preventDefault();
            alert("There are no changes to save");
            return false;
          }
          return true;
      });

      $("a.colorbox-form").colorboxify();

      save_initial_form_values();

      RegisterListeners();
  }

  // Event binding
  bind_dashboard_events();
  bind_candidate_events();

  // Dropdown initialization
  // $('#header-main').dropdown();
  // $('#header').dropdown();
  $('.dropdown').dropdown();

  // move the sidebar with scroll
  var aside = $('.content-aside');
  if (aside.length > 0) {
    var offset = $('.row-content').offset().top;
    var verticalOffset = offset + 100;
    var sidebarBody = $(".content-aside-candidates");
    $(window).bind('scroll', function(e) {
      if(offset > $(window).scrollTop() ) {
        setAside(sidebarBody, verticalOffset);
      }
      else {
        setAside(sidebarBody, verticalOffset);
      }
    });
    $(window).bind('resize', function(e) {
      setAside(sidebarBody, verticalOffset);
    });
    setAside(sidebarBody, verticalOffset);
  }


  /* Return this to ensure chainability */
  return this;
};


})( jQuery );
