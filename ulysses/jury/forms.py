# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.generic.forms import BaseForm

from django_registration.forms import RegistrationForm


class JuryProfileForm(forms.Form):
    first_name = forms.CharField()
    last_name = forms.CharField()
    email = forms.EmailField()
    

class JuryRegistrationForm(RegistrationForm):    
    
    def __init__(self, *args, **kwargs):
        super(JuryRegistrationForm, self).__init__(*args, **kwargs)
        self.fields.insert(0, 'last_name', forms.CharField())
        self.fields.insert(0, 'first_name', forms.CharField())

    def clean(self):
        # Call base class first
        super(JuryRegistrationForm,self).clean()
        if self._errors: # no need to go further if the form is already invalid
            return self.cleaned_data

        # Check no user already exists with this e-mail address        
        email = self.cleaned_data["email"]
        failed = User.objects.filter(email=email).exists()        
            
        if failed:
            raise forms.ValidationError(_("A user account already exist with the same email address"))
                
        # Return cleaned data
        return self.cleaned_data


def get_dynamic_jury_registration_form(competition_url):
    
    class _JuryDynamicRegistrationForm(JuryRegistrationForm):
        
        def __init__(self, *args, **kwargs):
            super(_JuryDynamicRegistrationForm, self).__init__(*args, **kwargs)
            self.fields.insert(0,'competition_url',forms.CharField(widget=forms.HiddenInput(),initial=competition_url))
    
    return _JuryDynamicRegistrationForm
        
        
class CandidateFilterForm(BaseForm):
    filters = forms.ChoiceField(required=False)

    def __init__(self, choices, *args, **kwargs):
        super(CandidateFilterForm, self).__init__(*args, **kwargs)
        self.fields["filters"].choices = choices
