# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.jury'
    label = 'ulysses_jury'
    verbose_name = "Ulysses Jury"
