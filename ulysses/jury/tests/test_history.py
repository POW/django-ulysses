"""
jury app unit testing
"""

import json

from django.http import HttpResponseRedirect
from django.test import TestCase
from django.urls import reverse

from ulysses.competitions.models import (
    CompetitionStatus, is_jury_member, EvaluationNote
)
from ulysses.competitions.factories import (
    JuryMemberFactory, CompetitionFactory, CompetitionStepFactory, CandidateFactory, EvaluationFactory,
    EvaluationFieldFactory, DynamicApplicationFormFactory
)
from ulysses.composers.models import is_composer
from ulysses.composers.factories import UserFactory


class JuryCandidateHistoryTest(TestCase):
    fixtures = ['initial_data', 'groups']

    def setUp(self):

        # Create a jury member
        raw_password = "123"
        self.user = UserFactory.create(password=raw_password, email="joe@dalton.fr")
        self.jury_member = JuryMemberFactory.create(user=self.user)

        # Log in as jury member
        login_url = reverse('login')
        self.client.get(login_url)

        login_data = {
            'email': self.user.email,
            'password': raw_password,
        }

        response = self.client.post(login_url, login_data)
        self.assertTrue(type(response) is HttpResponseRedirect)
        self.assertTrue(is_jury_member(self.user))
        self.assertFalse(is_composer(self.user))

    def _create_competition(self, display_candidate_name=True, allow_access_to_jury_history=True):
        return CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=DynamicApplicationFormFactory.create(),
            display_candidate_name=display_candidate_name,
            allow_access_to_jury_history=allow_access_to_jury_history
        )

    def _create_competition_step(self, competition):
        # Create a step for this competition
        return CompetitionStepFactory.create(competition=competition)

    def _create_candidate(self, competition_step, composer=None):
        # Create a candidate for this competition
        if not composer:
            candidate = CandidateFactory.create(competition=competition_step.competition)
        else:
            candidate = CandidateFactory.create(competition=competition_step.competition, composer=composer)
        return candidate

    def test_view_candidate_history(self):
        """view candidate history"""

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1)
        candidate1_1 = self._create_candidate(step1)
        candidate2_1 = self._create_candidate(step1)

        competition2 = self._create_competition()
        step2 = self._create_competition_step(competition2)
        candidate1_2 = self._create_candidate(step2, candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation1_1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            comments="Admin#1"
        )

        evaluation1_2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
        )

        evaluation2_1 = EvaluationFactory(
            candidate=candidate2_1,
            jury_member=self.jury_member,
            competition_step=step1,
        )

        note1_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="note", value='1')
        comment_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="comments", value='Comment#1')
        note1_2 = EvaluationNote.objects.create(evaluation=evaluation1_2, key="note", value='2')
        note2_1 = EvaluationNote.objects.create(evaluation=evaluation2_1, key="note", value='3')

        url = "/jury/candidate-history/?id={0}&competition_id={1}".format(
            candidate1_2.id, competition2.id
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertContains(response, "1 / 3")
        self.assertContains(response, comment_1.value)
        self.assertNotContains(response, "2 / 3")
        self.assertNotContains(response, "3 / 3")

        self.assertNotContains(response, evaluation1_1.comments)

    def test_view_candidate_history_not_allowed(self):
        """view candidate history"""

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1)
        candidate1_1 = self._create_candidate(step1)
        candidate2_1 = self._create_candidate(step1)

        competition2 = self._create_competition(allow_access_to_jury_history=False)
        step2 = self._create_competition_step(competition2)
        candidate1_2 = self._create_candidate(step2, candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation1_1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
        )

        evaluation1_2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
        )

        evaluation2_1 = EvaluationFactory(
            candidate=candidate2_1,
            jury_member=self.jury_member,
            competition_step=step1,
        )

        note1_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="note", value='1')
        comment_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="comments", value='Comment#1')
        note1_2 = EvaluationNote.objects.create(evaluation=evaluation1_2, key="note", value='2')
        note2_1 = EvaluationNote.objects.create(evaluation=evaluation2_1, key="note", value='3')

        url = "/jury/candidate-history/?id={0}&competition_id={1}".format(
            candidate1_2.id, competition2.id
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_candidate_anonymous(self):
        """view candidate history"""

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1)
        candidate1_1 = self._create_candidate(step1)
        candidate2_1 = self._create_candidate(step1)

        competition2 = self._create_competition(display_candidate_name=False)
        step2 = self._create_competition_step(competition2)
        candidate1_2 = self._create_candidate(step2, candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation1_1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            comments="Comment #1"
        )

        evaluation1_2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            comments="Comment #2"
        )

        evaluation2_1 = EvaluationFactory(
            candidate=candidate2_1,
            jury_member=self.jury_member,
            competition_step=step1,
            comments="Comment #3"
        )

        note1_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="note", value='1')
        comment_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="comments", value='Comment#1')
        note1_2 = EvaluationNote.objects.create(evaluation=evaluation1_2, key="note", value='2')
        note2_1 = EvaluationNote.objects.create(evaluation=evaluation2_1, key="note", value='3')

        url = "/jury/candidate-history/?id={0}&competition_id={1}".format(
            candidate1_2.id, competition2.id
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_candidate_history_other_jury(self):
        """view candidate history"""

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1)
        candidate1_1 = self._create_candidate(step1)
        candidate2_1 = self._create_candidate(step1)

        competition2 = self._create_competition()
        step2 = self._create_competition_step(competition2)
        candidate1_2 = self._create_candidate(step2, candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        evaluation1_1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=JuryMemberFactory.create(),
            competition_step=step1,
            comments="Comment #1"
        )

        evaluation1_2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            comments="Comment #2"
        )

        evaluation2_1 = EvaluationFactory(
            candidate=candidate2_1,
            jury_member=self.jury_member,
            competition_step=step1,
            comments="Comment #3"
        )

        note1_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="note", value='1')
        comment_1 = EvaluationNote.objects.create(evaluation=evaluation1_1, key="comments", value='Comment#1')
        comment_2 = EvaluationNote.objects.create(evaluation=evaluation1_2, key="comments", value='Comment#2')
        comment_3 = EvaluationNote.objects.create(evaluation=evaluation2_1, key="comments", value='Comment#3')

        url = "/jury/candidate-history/?id={0}&competition_id={1}".format(
            candidate1_2.id, competition2.id
        )

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertNotContains(response, comment_1.value)
        self.assertNotContains(response, comment_2.value)
        self.assertNotContains(response, comment_3.value)
