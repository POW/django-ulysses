"""
jury app unit testing
"""

from bs4 import BeautifulSoup
import os.path
from os.path import abspath, dirname

from django.core import mail
from django.http import HttpResponseRedirect
from django.test.client import RequestFactory
from django.urls import reverse

from ulysses.competitions.models import (
    CompetitionStatus, is_jury_member, Evaluation, EvaluationNote, CandidateAttributeValue,
    ApplicationFormFieldType, CandidateBiographicElement, ApplicationElement, EvaluationStatus
)
from ulysses.competitions.factories import (
    JuryMemberFactory, CompetitionFactory, CompetitionStepFactory, CandidateFactory, EvaluationFactory,
    EvaluationFieldFactory, DynamicApplicationFormFactory, ApplicationFormFieldSetDefinitionFactory,
    ApplicationFormFieldDefinitionFactory, CompetitionAttributeFactory, CompetitionAttributeValueFactory,
    EvaluationNoteFactory, CompetitionManagerFactory, TemporaryMediaFactory, TemporaryDocumentFactory,
    ComposerFactory
)
from ulysses.composers.models import is_composer
from ulysses.composers.factories import UserFactory
from ulysses.generic.tests import BaseTestCase
from ulysses.web.views.applications import upload_as_temporary_file, get_temporary_file_url


class JuryInterfaceTestBaseTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        # Create a jury member
        raw_password = "123"
        self.user = UserFactory.create(password=raw_password, email='joe@dalton.fr')
        self.jury_member = JuryMemberFactory.create(user=self.user)

        self.user2 = UserFactory.create(password=raw_password, email='jack@dalton.fr')
        self.manager = CompetitionManagerFactory.create(user=self.user2)

        self.user3 = UserFactory.create(password=raw_password, email='william@dalton.fr')
        self.user3.is_superuser = True
        self.user3.save()

        self.user4 = UserFactory.create(password=raw_password, email='arverell@dalton.fr')

        self._login_as(self.user)
        self.assertTrue(is_jury_member(self.user))
        self.assertFalse(is_composer(self.user))

        self.competition = self._create_competition()

        # Create a step for this competition
        self.competition_step = CompetitionStepFactory.create(competition=self.competition)

        # Create an evaluation field for this step
        self.evaluation_field = EvaluationFieldFactory.create(
            competition_step=self.competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        # Create a candidate for this competition
        self.candidate = CandidateFactory.create(competition=self.competition)

        # Associate candidate to jury member for competition step
        self.evaluation = EvaluationFactory(
            candidate=self.candidate, jury_member=self.jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )

        self.competition.managed_by.add(self.manager)
        self.competition.save()

    def _login_as(self, user):
        # Log in as jury member
        login_url = reverse('login')
        self.client.get(login_url)

        raw_password = "123"
        login_data = {
            'email': user.email,
            'password': raw_password,
        }

        response = self.client.post(login_url, login_data)
        self.assertTrue(type(response) is HttpResponseRedirect)

    def _create_competition(self):
        # Create a competition with a concrete application form
        return CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )


class JuryInterfaceTest(JuryInterfaceTestBaseTest):

    def test_view_candidate_as_jury(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(candidate_items), 1)

    def test_view_candidate_as_manager(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        self.client.logout()
        self._login_as(self.user2)
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(candidate_items), 1)

    def test_view_candidate_as_manager_of_other_and_jury(self):
        # Jury member and admin of another competition
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        self.client.logout()
        self._login_as(self.user2)
        jury_member2 = JuryMemberFactory.create(user=self.user2)
        self.evaluation = EvaluationFactory(
            candidate=self.candidate,
            jury_member=jury_member2,
            competition_step=self.competition_step,
            status="to_process"
        )
        competition2 = self._create_competition()
        competition2.managed_by.add(self.manager)
        competition2.save()
        self.competition.managed_by.remove(self.manager)
        self.competition.save()
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(candidate_items), 1)

    def test_view_candidate_as_manager_and_jury_of_other(self):
        # Jury member and admin of another competition
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        self.client.logout()
        self._login_as(self.user2)
        jury_member = JuryMemberFactory.create(user=self.user2)
        self.evaluation = EvaluationFactory(
            candidate=self.candidate,
            jury_member=jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(candidate_items), 1)

    def test_view_candidate_as_superuser(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        self.client.logout()
        self._login_as(self.user3)
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(candidate_items), 1)

    def test_view_candidate_as_other(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        self.client.logout()
        self._login_as(self.user4)
        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 403)

    def test_view_candidate_dont_display_name(self):
        """if competition don't allow it: do not display candidate name"""

        # disable the possibility to view candidate names
        self.competition_step.competition.display_candidate_name = False
        self.competition_step.competition.save()

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        self.assertNotContains(resp, self.candidate.composer.user.first_name)
        self.assertNotContains(resp, self.candidate.composer.user.last_name)

        soup = BeautifulSoup(resp.content, "html.parser")
        candidate_items = soup.select("ul.candidates-list li")

        self.assertEqual(len(candidate_items), 1)

        self.assertNotContains(resp, "Candidate information")

    def test_evaluate_candidate(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)

        note_value = 3

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': note_value,
            'is_completed': True
        }

        resp = self.client.post(candidate_url, post_data)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        self.assertContains(resp, "Your changes have been saved")

        # Ensure evaluation has been updated
        evaluation = Evaluation.objects.get(
            candidate=self.candidate, jury_member=self.jury_member,
            competition_step=self.competition_step
        )

        self.assertEqual(evaluation.status.url, "completed")

        note = EvaluationNote.objects.get(evaluation=evaluation, key=self.evaluation_field.key)
        self.assertEqual(note.value, str(note_value))

    def test_evaluate_candidate_modify(self):
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        evaluation = Evaluation.objects.get(
            candidate=self.candidate,
            jury_member=self.jury_member,
            competition_step=self.competition_step
        )
        EvaluationNoteFactory.create(
            evaluation=evaluation, key=self.evaluation_field.key, value=2
        )
        evaluation.status = EvaluationStatus.objects.get(url='completed')
        evaluation.save()

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)

        note_value = 3

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': note_value,
            'is_completed': True
        }

        resp = self.client.post(candidate_url, post_data)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)
        self.assertContains(resp, "Your changes have been saved")

        # Ensure evaluation has been updated
        evaluation = Evaluation.objects.get(
            candidate=self.candidate, jury_member=self.jury_member,
            competition_step=self.competition_step
        )

        self.assertEqual(evaluation.status.url, "completed")

        note = EvaluationNote.objects.get(evaluation=evaluation, key=self.evaluation_field.key)
        self.assertEqual(note.value, str(note_value))

        # An email has been sent to admins
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, [self.manager.user.email])
        body = mail.outbox[0].body
        self.assertTrue(body.find(self.competition.name) >= 0)
        self.assertTrue(body.find(self.competition_step.name) >= 0)
        self.assertTrue(body.find(self.candidate.full_name()) >= 0)
        self.assertTrue(body.find(self.jury_member.name()) >= 0)


class JuryInterfaceCommentsTest(JuryInterfaceTestBaseTest):

    def setUp(self):
        super().setUp()

    def test_evaluation_with_comments(self):
        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True,
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A',
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'completed')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 2)
        self.assertEqual(notes.get(key='note').value, '3')
        self.assertEqual(notes.get(key='comment').value, 'A')

    def test_evaluation_with_comments_min_length_error(self):

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True, minimum_comment_length=100
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A',
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'to process')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 0)

    def test_evaluation_with_comments_min_length_ok(self):

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True, minimum_comment_length=100
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A' * 101,
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'completed')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 2)
        self.assertEqual(notes.get(key='note').value, '3')
        self.assertEqual(notes.get(key='comment').value, 'A' * 101)

    def test_evaluation_with_comments_max_length_error(self):

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True, maximum_comment_length=100
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A' * 101,
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'to process')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 0)

    def test_evaluation_with_comments_max_length_ok(self):

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True, maximum_comment_length=100
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A',
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'completed')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 2)
        self.assertEqual(notes.get(key='note').value, '3')
        self.assertEqual(notes.get(key='comment').value, 'A')

    def test_evaluation_with_comments_max_max_length_error(self):

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=2, is_note=False, weight=0,
            field_type='forms.CharField', widget_type='forms.Textarea()', key="comment", display_in_result_list=False,
            field_extra_args="", required=True,
        )

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'candidate_id': self.candidate.pk,
            'note': 3,
            'comment': 'A' * 8001,
            'is_completed': True
        }

        resp = self.client.post(url, data=post_data)
        self.assertEqual(resp.status_code, 200)

        queryset = Evaluation.objects.filter(candidate=self.candidate)
        self.assertEqual(1, queryset.count())
        evaluation = queryset[0]
        self.assertEqual(evaluation.status.name, 'to process')
        notes = EvaluationNote.objects.filter(evaluation=evaluation)
        self.assertEqual(notes.count(), 0)


class JuryInterfaceDynamicFormTest(JuryInterfaceTest):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def _create_competition(self):
        # Create a competition with a dynamic application form
        return CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=DynamicApplicationFormFactory.create()
        )

    def test_view_candidate_fields(self):
        """make sure that only allowed competition attributes are displayed to the jury"""

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        set2 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        visible_fields = [
            ApplicationFormFieldDefinitionFactory.create(parent=set1, is_visible_by_jury=True),
        ]

        invisible_fields = [
            ApplicationFormFieldDefinitionFactory.create(parent=set1, is_visible_by_jury=False),
            ApplicationFormFieldDefinitionFactory.create(parent=set2, is_visible_by_jury=False),
        ]

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        soup = BeautifulSoup(resp.content, "html.parser")

        # check if fields are visible
        for field in visible_fields:
            field = soup.select("#id_" + field.name)
            self.assertEqual(len(field), 1)

        for field in invisible_fields:
            field = soup.select("#id_" + field.name)
            self.assertEqual(len(field), 0)

        # expect 2 sections : Candidate information + set1
        sections = soup.select(".section")
        self.assertEqual(len(sections), 2)

    def test_view_candidate_fields_values(self):
        """make sure that only allowed competition attributes are displayed to the jury"""

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        biographic_element_type = ApplicationFormFieldType.objects.get(name="BiographicElementField")
        html_element_type = ApplicationFormFieldType.objects.get(name="ApplicationHtmlElementField")

        ApplicationFormFieldDefinitionFactory.create(
            parent=set1, is_visible_by_jury=True, kind=biographic_element_type, name="short_bio",
        )

        ApplicationFormFieldDefinitionFactory.create(
            parent=set1, is_visible_by_jury=True, kind=html_element_type, name="html1",
        )

        short_bio = CandidateBiographicElement.objects.create(
            key="short_bio", title="bio", text='my bio', candidate=self.candidate
        )
        html1 = ApplicationElement.objects.create(
            key="html1", label="HTML", value="paragraph", candidate=self.candidate
        )

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        soup = BeautifulSoup(resp.content, "html.parser")

        self.assertEqual(soup.select("#id_short_bio")[0].text, short_bio.text)
        self.assertEqual(soup.select("#id_html1")[0].text, html1.value)

    def test_view_candidate_fields_values_duplicates(self):
        """make sure that only allowed competition attributes are displayed to the jury"""

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        biographic_element_type = ApplicationFormFieldType.objects.get(name="BiographicElementField")
        html_element_type = ApplicationFormFieldType.objects.get(name="ApplicationHtmlElementField")

        ApplicationFormFieldDefinitionFactory.create(
            parent=set1, is_visible_by_jury=True, kind=biographic_element_type, name="short_bio",
        )

        ApplicationFormFieldDefinitionFactory.create(
            parent=set1, is_visible_by_jury=True, kind=html_element_type, name="html1",
        )

        short_bio = CandidateBiographicElement.objects.create(
            key="short_bio", title="bio", text='my bio', candidate=self.candidate
        )
        html1 = ApplicationElement.objects.create(
            key="html1", label="HTML", value="paragraph", candidate=self.candidate
        )
        short_bio2 = ApplicationElement.objects.create(
            key="short_bio", label="BIO duplicate", value="bio paragraph", candidate=self.candidate
        )

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        soup = BeautifulSoup(resp.content, "html.parser")

        self.assertEqual(soup.select("#id_short_bio")[0].text, short_bio.text)
        self.assertEqual(soup.select("#id_html1")[0].text, html1.value)

    def test_view_candidate_filter(self):
        """view candidate filter form"""
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        competition_attribute1 = CompetitionAttributeFactory.create(
            competition=self.competition
        )
        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1, competition_attribute=competition_attribute1
        )
        value1 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=True
        )
        value2 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=False
        )
        value3 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=True
        )

        competition_attribute2 = CompetitionAttributeFactory.create(
            competition=self.competition
        )
        field2 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1, competition_attribute=competition_attribute2
        )
        value4 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute2, is_filter=True
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        soup = BeautifulSoup(resp.content, "html.parser")
        filters_items = soup.select("select#id_filters option")
        self.assertEqual(len(filters_items), 4)

        self.assertEqual("0", filters_items[0]["value"])
        elements = [elt.text for elt in filters_items if elt.text]
        self.assertTrue(value1.value in elements)
        self.assertTrue(value2.value not in elements)
        self.assertTrue(value3.value in elements)
        self.assertTrue(value4.value in elements)

    def test_view_candidate_no_filter(self):
        """no filter form if no filter"""
        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        competition_attribute1 = CompetitionAttributeFactory.create(
            competition=self.competition
        )
        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1, competition_attribute=competition_attribute1
        )
        value1 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=False
        )
        value2 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=False
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        soup = BeautifulSoup(resp.content, "html.parser")
        filters_select = soup.select("select#id_filters")

        self.assertEqual(len(filters_select), 0)

    def test_view_filtered_candidates(self):
        """make sure that only candidates are displayed"""

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=self.competition.dynamic_application_form
        )

        competition_attribute1 = CompetitionAttributeFactory.create(
            competition=self.competition
        )
        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1, competition_attribute=competition_attribute1
        )
        attribute1 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=True
        )
        attribute2 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=False
        )
        attribute3 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=True
        )
        attribute4 = CompetitionAttributeValueFactory.create(
            attribute=competition_attribute1, is_filter=True
        )

        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        evaluation2 = EvaluationFactory(
            candidate=candidate2, jury_member=self.jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )

        evaluation3 = EvaluationFactory(
            candidate=candidate3, jury_member=self.jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )

        CandidateAttributeValue.objects.create(
            candidate=self.candidate,
            attribute="dummy",
            value=attribute1
        )

        CandidateAttributeValue.objects.create(
            candidate=candidate2,
            attribute="dummy",
            value=attribute2
        )

        CandidateAttributeValue.objects.create(
            candidate=candidate3,
            attribute="dummy",
            value=attribute3
        )

        #all candidates
        jury_dashboard_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )
        resp = self.client.get(jury_dashboard_url)
        self.assertEqual(resp.status_code, 200)
        soup = BeautifulSoup(resp.content, "html.parser")
        filters_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(filters_items), 3)

        #filter by attribute 1
        resp = self.client.post(jury_dashboard_url, data={'filters': attribute1.id})
        self.assertEqual(resp.status_code, 200)
        soup = BeautifulSoup(resp.content, "html.parser")
        filters_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(filters_items), 1)

        #filter by attribute 2
        resp = self.client.post(jury_dashboard_url, data={'filters': attribute4.id})
        self.assertEqual(resp.status_code, 200)
        soup = BeautifulSoup(resp.content, "html.parser")
        filters_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(filters_items), 0)

        #don't filter
        resp = self.client.post(jury_dashboard_url, data={'filters': 0})
        self.assertEqual(resp.status_code, 200)
        soup = BeautifulSoup(resp.content, "html.parser")
        filters_items = soup.select("ul.candidates-list li")
        self.assertEqual(len(filters_items), 3)

    def test_evaluation_summary(self):
        """make sure short summaru of notes is displayed correctly"""

        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        evaluation2 = EvaluationFactory(
            candidate=candidate2, jury_member=self.jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )

        evaluation3 = EvaluationFactory(
            candidate=candidate3, jury_member=self.jury_member,
            competition_step=self.competition_step,
            status="to_process"
        )

        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=True, order_index=1, is_note=True, weight=1,
            field_type='forms.ChoiceField', widget_type='forms.Select', key="note1", display_in_result_list=True,
            field_extra_args="{'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')]}"
        )
        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=True, order_index=3, is_note=True, weight=1,
            field_type='forms.ChoiceField', widget_type='forms.Select', key="note2", display_in_result_list=True,
            field_extra_args="{'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')]}"
        )
        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=True, order_index=2, is_note=True, weight=1,
            field_type='forms.ChoiceField', widget_type='forms.Select', key="note3", display_in_result_list=True,
            field_extra_args="{'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')]}"
        )
        EvaluationFieldFactory.create(
            competition_step=self.competition_step, principal=False, order_index=4, is_note=True, weight=1,
            field_type='forms.ChoiceField', widget_type='forms.Select', key="note4", display_in_result_list=True,
            field_extra_args="{'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4'), (5, '5')]}"
        )

        EvaluationNoteFactory.create(evaluation=evaluation2, key="note1", value="1")
        EvaluationNoteFactory.create(evaluation=evaluation2, key="note2", value="2")
        EvaluationNoteFactory.create(evaluation=evaluation2, key="note3", value="3")
        EvaluationNoteFactory.create(evaluation=evaluation2, key="note4", value="4")

        EvaluationNoteFactory.create(evaluation=evaluation3, key="note1", value="5")
        EvaluationNoteFactory.create(evaluation=evaluation3, key="note2", value="4")
        EvaluationNoteFactory.create(evaluation=evaluation3, key="note3", value="3")
        EvaluationNoteFactory.create(evaluation=evaluation3, key="note4", value="2")

        self.assertEqual(evaluation2.get_short_summary(), '1, 3, 2')
        self.assertEqual(evaluation3.get_short_summary(), '5, 3, 4')

    def test_help_button_shown(self):

        self.competition.jury_guidelines = "Help Text"
        self.competition.save()

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)

        soup = BeautifulSoup(resp.content, "html.parser")

        help_url = reverse('jury:help', args=[self.competition.url])
        jury_help_link = soup.select('a[href="{0}"]'.format(help_url))

        self.assertEqual(len(jury_help_link), 1)

    def test_help_button_hidden(self):

        self.competition.jury_guidelines = ""
        self.competition.save()

        candidate_url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            self.competition_step.pk, self.candidate.pk
        )

        resp = self.client.get(candidate_url)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.candidate.composer.user.first_name)
        self.assertContains(resp, self.candidate.composer.user.last_name)

        soup = BeautifulSoup(resp.content, "html.parser")
        help_url = reverse('jury:help', args=[self.competition.url])
        jury_help_link = soup.select('a[href="{0}"]'.format(help_url))

        self.assertEqual(len(jury_help_link), 0)

    def test_help_(self):

        help_url = reverse('jury:help', args=[self.competition.url])
        self.competition.jury_guidelines = "Help Text"
        self.competition.save()

        resp = self.client.get(help_url)
        self.assertEqual(resp.status_code, 200)

        self.assertContains(resp, self.competition.jury_guidelines)


class JuryEvaluationInterfaceTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        # Create a jury member
        raw_password = "123"
        self.user = UserFactory.create(password=raw_password, email='joe@dalton.fr')
        self.jury_member = JuryMemberFactory.create(user=self.user)

        self.user2 = UserFactory.create(password=raw_password, email='jack@dalton.fr')
        self.manager = CompetitionManagerFactory.create(user=self.user2)

        self.candidate_user = UserFactory.create(password=raw_password, email='william@dalton.fr')
        self.composer = ComposerFactory.create(user=self.candidate_user)

    def create_temporary_file(self, user):
        base_dir = abspath(dirname(dirname(__file__)))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')
        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": user.username, "Filedata": file_to_upload})
        upload_request.user = user
        upload_response = upload_as_temporary_file(upload_request)
        return upload_response.content.decode('utf-8')

    def _login_as(self, user):
        # Log in as jury member
        login_url = reverse('login')
        self.client.get(login_url)

        raw_password = "123"
        login_data = {
            'email': user.email,
            'password': raw_password,
        }

        response = self.client.post(login_url, login_data)
        self.assertTrue(type(response) is HttpResponseRedirect)

    def _create_competition_and_candidate(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        fieldset_definition1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form,
            order=1
        )

        fieldset_definition2 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form,
            order=2
        )

        text_type = ApplicationFormFieldType.objects.get(name="ApplicationTextElementField")
        bio_type = ApplicationFormFieldType.objects.get(name="BiographicElementField")
        media_type = ApplicationFormFieldType.objects.get(name="MediaField")
        document_type = ApplicationFormFieldType.objects.get(name="DocumentField")

        name_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition1,
            order=1,
            is_visible_by_jury=1,
            kind=text_type,
            name="name"
        )

        bio_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition1,
            order=2,
            is_visible_by_jury=1,
            kind=bio_type,
            name="short_bio"
        )

        cv_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition1,
            order=3,
            is_visible_by_jury=1,
            kind=document_type,
            name="cv"
        )

        work_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition2,
            order=1,
            is_visible_by_jury=1,
            kind=media_type,
            name="work"
        )

        score_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition2,
            order=2,
            is_visible_by_jury=1,
            kind=document_type,
            name="score"
        )

        doc_field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset_definition2,
            order=2,
            is_visible_by_jury=0,
            kind=document_type,
            name="doc"
        )

        # Create a competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )
        competition.managed_by.add(self.manager)
        competition.save()

        # Create a step for this competition
        competition_step = CompetitionStepFactory.create(competition=competition)

        # Create an evaluation field for this step
        evaluation_field = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        # Create a candidate for this competition
        candidate = CandidateFactory.create(competition=competition, composer=self.composer)
        candidate_user = candidate.composer.user
        self._login_as(candidate_user)

        candidate_name = ApplicationElement.objects.create(
            key=name_field.name, label="name", value='My Name', candidate=candidate
        )
        candidate_short_bio = CandidateBiographicElement.objects.create(
            key=bio_field.name, title="bio", text='my bio', candidate=candidate
        )

        candidate_work_score = self.create_temporary_file(candidate_user)
        candidate_work_score_url = get_temporary_file_url(candidate_user, candidate_work_score)

        candidate_cv = self.create_temporary_file(candidate_user)
        candidate_cv_url = get_temporary_file_url(candidate_user, candidate_cv)

        candidate_score = self.create_temporary_file(candidate_user)
        candidate_score_url = get_temporary_file_url(candidate_user, candidate_score)

        candidate_doc = self.create_temporary_file(candidate_user)
        candidate_doc_url = get_temporary_file_url(candidate_user, candidate_doc)

        temp_media2 = TemporaryMediaFactory.create(
            composer=candidate.composer, competition=competition, score=candidate_work_score_url, key=work_field.name
        )
        candidate_work = temp_media2.move_as_candidate_element(candidate)

        temp_doc1 = TemporaryDocumentFactory.create(
            composer=candidate.composer, competition=competition, file=candidate_cv_url, key=cv_field.name
        )
        candidate_cv = temp_doc1.move_as_candidate_element(candidate)

        temp_doc2 = TemporaryDocumentFactory.create(
            composer=candidate.composer, competition=competition, file=candidate_score_url, key=score_field.name
        )
        candidate_score = temp_doc2.move_as_candidate_element(candidate)

        temp_doc3 = TemporaryDocumentFactory.create(
            composer=candidate.composer, competition=competition, file=candidate_doc_url, key=doc_field.name
        )
        candidate_doc = temp_doc3.move_as_candidate_element(candidate)

        # Associate candidate to jury member for competition step
        evaluation = EvaluationFactory(
            candidate=candidate, jury_member=self.jury_member,
            competition_step=competition_step,
            status="to_process"
        )
        self.client.logout()

        return locals()

    def test_view_candidate_fields(self):
        args = self._create_competition_and_candidate()
        self._login_as(self.user)

        url = "/jury/dashboard/?step=%s&status=all&candidate=%s" % (
            args['competition_step'].pk, args['candidate'].pk
        )
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, args['name_field'].label)
        self.assertContains(response, args['bio_field'].label)
        self.assertContains(response, args['cv_field'].label)
        self.assertContains(response, args['work_field'].label)
        self.assertContains(response, args['score_field'].label)
        self.assertNotContains(response, args['doc_field'].label)

        self.assertContains(response, args['candidate_name'].value)
        self.assertContains(response, args['candidate_short_bio'].text)
        self.assertContains(response, args['candidate_work'].score)
        self.assertContains(response, args['candidate_cv'].file)
        self.assertContains(response, args['candidate_score'].file)
        self.assertNotContains(response, args['candidate_doc'].file)