"""
jury app unit testing
"""

import json

from bs4 import BeautifulSoup

from django.http import HttpResponseRedirect
from django.test import TestCase
from django.urls import reverse

from ulysses.competitions.models import (
    CompetitionStatus, is_jury_member, EvaluationNote, Candidate
)
from ulysses.competitions.factories import (
    JuryMemberFactory, CompetitionFactory, CompetitionStepFactory, CandidateFactory, EvaluationFactory,
    EvaluationFieldFactory, DynamicApplicationFormFactory
)
from ulysses.composers.models import is_composer
from ulysses.composers.factories import UserFactory


class JuryPreviousStepsTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']
    raw_password = "123"

    def setUp(self):

        # Create a jury member
        self.user = UserFactory.create(password=self.raw_password, email="joe@dalton.fr")
        self.jury_member = JuryMemberFactory.create(user=self.user)

        self._login(self.user)

        self.assertTrue(is_jury_member(self.user))
        self.assertFalse(is_composer(self.user))

    def _login(self, user):
        # Log in as jury member
        login_url = reverse('login')
        self.client.get(login_url)

        login_data = {
            'email': user.email,
            'password': self.raw_password,
        }

        response = self.client.post(login_url, login_data)
        self.assertTrue(type(response) is HttpResponseRedirect)

    def _create_competition(self, display_candidate_name=True):
        return CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=True,
            dynamic_application_form=DynamicApplicationFormFactory.create(),
            display_candidate_name=display_candidate_name,
        )

    def _create_competition_step(self, competition, can_access_evaluations=False):
        # Create a step for this competition
        return CompetitionStepFactory.create(
            competition=competition, can_access_evaluations=can_access_evaluations
        )

    def _create_candidate(self, competition_step, composer=None):
        # Create a candidate for this competition
        if not composer:
            candidate = CandidateFactory.create(competition=competition_step.competition)
        else:
            try:
                candidate = Candidate.objects.get(competition=competition_step.competition, composer=composer)
            except Candidate.DoesNotExist:
                candidate = CandidateFactory.create(competition=competition_step.competition, composer=composer)
        return candidate

    def test_view_candidate_previous_steps(self):
        """view candidate evaluations in previous steps of all jury members"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url,
            data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertContains(response, "1 / 3")
        self.assertContains(response, comment1_1.value)
        self.assertContains(response, "2 / 3")
        self.assertContains(response, comment1_2.value)
        self.assertNotContains(response, "3 / 3")
        self.assertNotContains(response, comment1_3.value)

    def test_view_candidate_previous_steps_not_allowed(self):
        """it should return an error"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        user3 = UserFactory.create()
        jury_member3 = JuryMemberFactory.create(user=user3)

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member3,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url, data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 403)

    def test_view_candidate_previous_steps_not_jury_member(self):
        """it should return an error"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        user3 = UserFactory.create()
        jury_member3 = JuryMemberFactory.create(user=user3)

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member3,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        self.client.logout()
        regular_user = UserFactory.create(password=self.raw_password, email="jack@dalton.fr")
        self._login(regular_user)

        response = self.client.get(
            url, data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 403)

    def test_view_candidate_previous_steps_anonymous(self):
        """it should return an error"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        self.client.logout()

        response = self.client.get(
            url, data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 302)
        self.assertTrue(reverse('login') in response['Location'])

    def test_view_candidate_previous_steps_only_candidate(self):
        """view candidate evaluations in previous steps of all jury members: only if completed"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="in_progress",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="in_progress",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url, data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertContains(response, "1 / 3")
        self.assertContains(response, comment1_1.value)
        self.assertNotContains(response, "2 / 3")
        self.assertNotContains(response, comment1_2.value)
        self.assertNotContains(response, "3 / 3")
        self.assertNotContains(response, comment1_3.value)

    def test_view_candidate_previous_steps_anonymous_candidates(self):
        """view candidate evaluations in previous steps of all jury members: if anonymous name is not displayed"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition(display_candidate_name=False)
        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url, data={'id': candidate1_1.id, 'competition_id': competition1.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertContains(response, "1 / 3")
        self.assertContains(response, comment1_1.value)
        self.assertContains(response, "2 / 3")
        self.assertContains(response, comment1_2.value)
        self.assertNotContains(response, "3 / 3")
        self.assertNotContains(response, comment1_3.value)

    def test_view_candidate_previous_steps_only_of_current_competition(self):
        """view candidate evaluations in previous steps of all jury members: only if same competition"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()
        competition2 = self._create_competition()

        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition2)
        candidate1_2 = self._create_candidate(step2, composer=candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury1_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url, data={'id': candidate1_2.id, 'competition_id': competition2.id, 'step_id': step1.id}
        )
        self.assertEqual(response.status_code, 404)

    def test_view_candidate_previous_steps_only_of_current_step(self):
        """view candidate evaluations in previous steps of all jury members: only if given step"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()

        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_2 = self._create_candidate(step2, composer=candidate1_1.composer)

        step3 = self._create_competition_step(competition1)
        candidate1_3 = self._create_candidate(step3, composer=candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury1_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            status="completed",
        )

        eval_jury1_candidate1_step3 = EvaluationFactory(
            candidate=candidate1_3,
            jury_member=self.jury_member,
            competition_step=step3,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:candidate_previous_steps')

        response = self.client.get(
            url, data={'id': candidate1_2.id, 'competition_id': competition1.id, 'step_id': step2.id}
        )
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        self.assertNotContains(response, "1 / 3")
        self.assertNotContains(response, comment1_1.value)
        self.assertNotContains(response, "2 / 3")
        self.assertNotContains(response, comment1_2.value)
        self.assertContains(response, "3 / 3")
        self.assertContains(response, comment1_3.value)

    def test_view_candidate_previous_steps_menu1(self):
        """view candidate evaluations in previous steps of all jury members: only if given step"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()

        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1, can_access_evaluations=False)
        candidate1_2 = self._create_candidate(step2, composer=candidate1_1.composer)

        step3 = self._create_competition_step(competition1)
        candidate1_3 = self._create_candidate(step3, composer=candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury1_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            status="completed",
        )

        eval_jury1_candidate1_step3 = EvaluationFactory(
            candidate=candidate1_3,
            jury_member=self.jury_member,
            competition_step=step3,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:reload_evaluation')

        response = self.client.get(url, data={'id': eval_jury1_candidate1_step3.id})
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        soup = BeautifulSoup(html, 'html.parser')

        elts = soup.select('.previous-steps-link')
        self.assertEqual(len(elts), 1)
        self.assertEqual(elts[0]['data-step_id'], str(step1.id))
        self.assertEqual(elts[0]['data-competition_id'], str(competition1.id))
        self.assertEqual(elts[0]['data-id'], str(candidate1_3.id))

    def test_view_candidate_previous_steps_menu2(self):
        """view candidate evaluations in previous steps of all jury members: only if given step"""

        user2 = UserFactory.create()
        jury_member2 = JuryMemberFactory.create(user=user2)

        competition1 = self._create_competition()

        step1 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_1 = self._create_candidate(step1)

        step2 = self._create_competition_step(competition1, can_access_evaluations=True)
        candidate1_2 = self._create_candidate(step2, composer=candidate1_1.composer)

        step3 = self._create_competition_step(competition1)
        candidate1_3 = self._create_candidate(step3, composer=candidate1_1.composer)

        evaluation_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field1 = EvaluationFieldFactory.create(
            competition_step=step1,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        evaluation_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), ] }",
            order_index=0
        )

        comment_field2 = EvaluationFieldFactory.create(
            competition_step=step2,
            key="comments",
            label="Comment",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0
        )

        eval_jury1_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=self.jury_member,
            competition_step=step1,
            status="completed",
        )

        eval_jury2_candidate1_step1 = EvaluationFactory(
            candidate=candidate1_1,
            jury_member=jury_member2,
            competition_step=step1,
            status="completed",
        )

        eval_jury1_candidate1_step2 = EvaluationFactory(
            candidate=candidate1_2,
            jury_member=self.jury_member,
            competition_step=step2,
            status="completed",
        )

        eval_jury1_candidate1_step3 = EvaluationFactory(
            candidate=candidate1_3,
            jury_member=self.jury_member,
            competition_step=step3,
            status="completed",
        )

        note1_1 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step1, key="note", value='1')
        comment1_1 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step1, key="comments", value='Comment#1'
        )

        note1_2 = EvaluationNote.objects.create(evaluation=eval_jury2_candidate1_step1, key="note", value='2')
        comment1_2 = EvaluationNote.objects.create(
            evaluation=eval_jury2_candidate1_step1, key="comments", value='Comment#2'
        )

        note1_3 = EvaluationNote.objects.create(evaluation=eval_jury1_candidate1_step2, key="note", value='3')
        comment1_3 = EvaluationNote.objects.create(
            evaluation=eval_jury1_candidate1_step2, key="comments", value='Comment#3'
        )

        url = reverse('jury:reload_evaluation')

        response = self.client.get(url, data={'id': eval_jury1_candidate1_step3.id})
        self.assertEqual(response.status_code, 200)

        html = json.loads(response.content)

        soup = BeautifulSoup(html, 'html.parser')

        elts = soup.select('.previous-steps-link')
        self.assertEqual(len(elts), 2)
