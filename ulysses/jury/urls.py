# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import show_dashboard, show_help, reload_evaluation, candidate_history, candidate_previous_steps


app_name = "jury"


urlpatterns = [
    url(r'^reload_evaluation/', reload_evaluation, name="reload_evaluation"),
    url(r'^dashboard/', show_dashboard, name="dashboard"),
    url(r'^help/(?P<competition_url>.+)', show_help, name="help"),
    url(r'^candidate-history/', candidate_history, name="candidate_history"),
    url(r'^candidate-previous-steps/', candidate_previous_steps, name="candidate_previous_steps"),
]
