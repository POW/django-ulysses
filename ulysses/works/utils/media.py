# -*- coding: utf-8 -*-

from ulysses.competitions.models import (
    TemporaryMedia, get_unique_title, copy_work_file_to_user_personal_folder, copy_file_to_user_temporary_folder
)
from ulysses.composers.models import Media


def copy_work_as_personal_element(work):
    """Make this work available"""
    composer = work.can_be_copied_to_media()
    if composer:
        work_media = Media()
        target_title = work.title
        work_media.title = get_unique_title(Media, composer, target_title)
        if work.score:
            work_media.score = copy_work_file_to_user_personal_folder(composer, work.score.name)
        if work.audio:
            work_media.audio = copy_work_file_to_user_personal_folder(composer, work.audio.name)
        if work.video:
            work_media.video = copy_work_file_to_user_personal_folder(composer, work.video.name)
        work_media.composer = composer
        work_media.link = work.link1
        work_media.save()
        return work_media


def copy_work_as_temporary_element(work, competition, key):
    """
    Copy the selected element as a temporary media
    Nota : the physical files are copied
    """

    composer = work.can_be_copied_to_media()
    if composer:
        # Remove existing temporary media, if necessary
        TemporaryMedia.objects.filter(competition=competition, composer=composer, key=key).delete()

        # Create new temporary media
        target_title = work.title

        temp_media = TemporaryMedia()
        temp_media.title = get_unique_title(TemporaryMedia, composer, target_title)

        if work.score:
            temp_media.score = copy_file_to_user_temporary_folder(composer, work.score.name)
        if work.audio:
            temp_media.audio = copy_file_to_user_temporary_folder(composer, work.audio.name)
        if work.video:
            temp_media.video = copy_file_to_user_temporary_folder(composer, work.video.name)

        temp_media.composer = composer
        temp_media.competition = competition
        temp_media.key = key
        temp_media.save()
        return temp_media
