# -*- coding: utf-8 -*-

from django.db.models import Q

from ulysses.middleware import get_request

from ..models import Work


def get_works_queryset(only_with_photos=False, queryset=None):
    """
    Returns: queryset of works according currect user permissions
    """
    user = get_request().user

    if queryset is None:
        queryset = Work.objects.all()

    if user.is_anonymous:
        queryset = queryset.filter(
            Q(visibility=Work.VISIBILITY_PUBLIC) |
            Q(visibility=Work.VISIBILITY_MEMBER)
        )

    else:
        queryset = queryset.filter(
            Q(visibility=Work.VISIBILITY_PUBLIC) |
            Q(visibility=Work.VISIBILITY_MEMBER) |
            Q(visibility=Work.VISIBILITY_PRIVATE, owner=user)
        ).distinct()

    if only_with_photos:
        queryset = queryset.exclude(image__isnull=True)

    return queryset


def get_public_works_queryset(queryset=None):
    """
    Returns: queryset of works according currect user permissions
    """
    if queryset is None:
        queryset = Work.objects.all()
    return queryset.exclude(visibility=Work.VISIBILITY_PRIVATE)


def get_works(limit_to=None, order_by=None, only_with_photos=False):
    """
    get works

    Args:
        limit_to: if set, limit to the numbers of works

    Returns: list of works ordered by last_update day (randomly for user who update the same day)

    """
    # get works
    works = get_works_queryset(only_with_photos=only_with_photos)

    if order_by is None:
        order_by = ['-last_update_date']

    works = works.extra(select={'last_update_date': 'DATE(update_date)'}).order_by(*order_by)

    if limit_to:
        # Take limit_to works order by last_update_date, and random
        works = works[:limit_to]

    return works


def get_works_count():
    """
    Returns: the number of works
    """
    return get_works_queryset().count()
