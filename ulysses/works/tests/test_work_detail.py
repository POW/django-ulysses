# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, timedelta
import json
import os.path

from django.test.client import RequestFactory
from django.urls import reverse

from ulysses.events.factories import EventFactory
from ulysses.generic.tests import BaseTestCase, assert_popup_redirects
from ulysses.profiles.factories import IndividualFactory

from ..factories import WorkFactory, KeywordFactory, InstrumentFactory
from ..models import Work, WorkViews, Keyword
from ..views import upload_work_file


class WorkDetailTest(BaseTestCase):

    def test_view_as_anonymous(self):
        """view """

        profile = IndividualFactory.create()

        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        work2 = WorkFactory.create(owner=profile.user)

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

        self.assertEqual(0, WorkViews.objects.count())

    def test_view_views_count(self):
        """view """

        profile = IndividualFactory.create()

        work1 = WorkFactory.create()
        work2 = WorkFactory.create(owner=profile.user)

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        WorkViews.objects.create(work=work1, count=100)

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]
        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 101)

    def test_view_as_me(self):
        """view """

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile.user)

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertContains(response, reverse('works:edit_work', args=[work1.id]))

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]
        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 1)

    def test_view_as_from_search(self):
        """view """

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile2.user)

        url = reverse('works:view_work', args=[work1.id])

        search_url = reverse('works:works_list') + "?title=Toto"

        response = self.client.get(url, HTTP_REFERER='http://example.com' + search_url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertNotContains(response, search_url)  # DO NOT DISPLAY BACK LINK ANYMORE

    def test_view_as_from_misc(self):
        """view """

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile2.user)

        url = reverse('works:view_work', args=[work1.id])

        search_url = "/blabla/?title=Toto"

        response = self.client.get(url, HTTP_REFERER='http://example.com' + search_url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertNotContains(response, search_url)

    def test_view_as_other(self):
        """view """

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile2.user)

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertNotContains(response, reverse('works:edit_work', args=[work1.id]))

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]
        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 1)

    def test_view_calendar(self):
        """view """

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile2.user)

        event = EventFactory.create(start_date=date.today())
        event.works.add(work1)
        event.save()

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertNotContains(response, reverse('works:edit_work', args=[work1.id]))

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]

        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 1)

    def test_view_calendar_passed(self):
        """view """

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile2.user)

        event = EventFactory.create(start_date=date.today() - timedelta(1))
        event.works.add(work1)
        event.save()

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertNotContains(response, reverse('works:edit_work', args=[work1.id]))

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]

        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 1)

    def test_view_as_anonymous_missing(self):
        """view """
        url = reverse('works:view_work', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_as_logged_missing(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:view_work', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_composer(self):
        """view """

        profile = IndividualFactory.create()
        composer = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        work1 = WorkFactory.create(owner=profile.user, composer=composer.user)

        url = reverse('works:view_work', args=[work1.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)
        self.assertContains(response, reverse('works:edit_work', args=[work1.id]))

        self.assertContains(response, composer.name)
        self.assertContains(response, composer.get_absolute_url())

        self.assertEqual(1, WorkViews.objects.count())
        work_views = WorkViews.objects.all()[0]
        self.assertEqual(work_views.work, work1)
        self.assertEqual(work_views.count, 1)

    def test_view_access_private_allowed(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)

    def test_view_access_private_forbidden(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_view_access_public(self):
        """view """
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:view_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)

    def test_view_access_member(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.title)

    def test_view_access_not_member(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_work', args=[work1.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))


class AddWorkTest(BaseTestCase):

    def test_view_as_anonymous(self):
        """view """
        url = reverse('works:add_work')
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_as_user(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:add_work')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select('form.ulysses-form')))

    def test_post_as_anonymous(self):
        """post """
        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'score_media': 'test.pdf',
            "visibility": Work.VISIBILITY_PUBLIC,
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(0, queryset.count())
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_post_as_user(self):
        """post """
        url = reverse('works:add_work')
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'score_media': 'test.pdf',
            "visibility": Work.VISIBILITY_PUBLIC,
        }
        response = self.client.post(url, data=data)
        soup = BeautifulSoup(response.content, "html.parser")
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, performer.user)
        self.assertEqual(work.visibility, Work.VISIBILITY_PUBLIC)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_user(self):
        """post """
        url = reverse('works:add_work')
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'score_media': 'test.pdf',
            "visibility": Work.VISIBILITY_PUBLIC,
        }
        response = self.client.post(url, data=data)
        soup = BeautifulSoup(response.content, "html.parser")
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, performer.user)
        self.assertEqual(work.visibility, Work.VISIBILITY_PUBLIC)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_without_copyright(self):
        """post """
        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "has_copyright": False,
            "visibility": Work.VISIBILITY_PUBLIC,
            "performers": [performer.user.id],
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select("ul.errorlist")))
        queryset = Work.objects.all()
        self.assertEqual(0, queryset.count())

    def test_post_without_performers(self):
        """post """
        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, performer.user)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_add_work_keywords(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        composer = IndividualFactory.create()
        keyword1 = KeywordFactory.create()
        keyword2 = KeywordFactory.create()
        keyword3 = KeywordFactory.create()

        data = {
            'title': 'Obladi-Oblada',
            'composer': composer.user.id,
            "performers": [performer.user.id],
            "keywords": '##'.join([str(keyword1.id), str(keyword2.id), 'NEW']),
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        queryset = Work.objects.all()

        self.assertEqual(1, queryset.count())
        work = queryset[0]

        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, composer.user)

        self.assertEqual(Keyword.objects.count(), 4)
        self.assertEqual(work.keywords.count(), 3)
        for keyword_name in (keyword1.name, keyword2.name, 'NEW'):
            self.assertEqual(work.keywords.filter(name=keyword_name).count(), 1)
        self.assertEqual(work.keywords.filter(name=keyword3.name).count(), 0)

        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_add_work_image(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        data = {
            'title': 'Obladi-Oblada',
            'image_media': 'my_picture.png',
            'composer': profile.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        queryset = Work.objects.all()

        self.assertEqual(1, queryset.count())
        work = queryset[0]

        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, profile.user)

        self.assertEqual(work.image.name, 'works/{0}/{1}'.format(work.owner.id, data['image_media']))

        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_add_work_image_long(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        data = {
            'title': 'Obladi-Oblada',
            'image_media': 'x' * 100 + '.png',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.image.name, 'works/{0}/{1}'.format(work.owner.id, data['image_media']))
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_add_work_image_too_long(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        data = {
            'title': 'Obladi-Oblada',
            'image_media': 'x' * 200 + '.png',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        queryset = Work.objects.all()
        self.assertEqual(0, queryset.count())
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('ul.errorlist')))

    def test_add_work_private(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()

        individual = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        data = {
            'title': 'Obladi-Oblada',
            'image_media': 'x.png',
            'composer': performer.user.id,
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PRIVATE,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.visibility, Work.VISIBILITY_PRIVATE)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_add_work_member(self):
        """post """

        url = reverse('works:add_work')
        performer = IndividualFactory.create()
        profile = IndividualFactory.create()

        individual = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        data = {
            'title': 'Obladi-Oblada',
            'image_media': 'x.png',
            'composer': performer.user.id,
            "has_copyright": True,
            "visibility": Work.VISIBILITY_MEMBER,
            'score_media': 'test.pdf',
        }

        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.visibility, Work.VISIBILITY_MEMBER)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))


class EditWorkTest(BaseTestCase):

    def upload_work_file(self, user, file_name):

        base_dir = os.path.abspath(os.path.dirname(__file__))
        sample_doc_path = os.path.join(base_dir, "fixtures", file_name)

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": user.username, "Filedata": file_to_upload})
        upload_request.user = user

        upload_response = upload_work_file(upload_request)
        return upload_response.content.decode('utf-8')

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_as_user(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select('form.ulysses-form')))

    def test_view_as_other(self):
        """post """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile2.user)
        url = reverse('works:edit_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_post_as_anonymous(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertNotEqual(work.title, data['title'])
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_post_as_user(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.creation_date.date(), date.today())
        self.assertEqual(work.update_date.date(), date.today())
        self.assertEqual(work.visibility, Work.VISIBILITY_PUBLIC)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_user_visibility_member(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_MEMBER,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.creation_date.date(), date.today())
        self.assertEqual(work.update_date.date(), date.today())
        self.assertEqual(work.visibility, Work.VISIBILITY_MEMBER)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_user_audio_valid_mp3(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        file_name = self.upload_work_file(profile.user, 'mp3/valid-mp3.mp3')
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'audio_media': file_name,
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.creation_date.date(), date.today())
        self.assertEqual(work.update_date.date(), date.today())
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_user_audio_invalid_mp3(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        file_name = self.upload_work_file(profile.user, 'mp3/invalid-mp3.mp3')
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'audio_media': file_name,
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertNotEqual(work.title, data['title'])
        self.assertEqual(response.status_code, 200)

    def test_post_as_user_video(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'video_media': 'test.mp4',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.creation_date.date(), date.today())
        self.assertEqual(work.update_date.date(), date.today())
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_user_no_file(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "visibility": Work.VISIBILITY_PUBLIC,
            "has_copyright": True,
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertNotEqual(work.title, data['title'])
        self.assertEqual(200, response.status_code)

    def test_edit_without_performers(self):
        """post """
        url = reverse('works:add_work')
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:edit_work', args=[work1.id])
        data = {
            'title': 'Obladi-Oblada',
            'composer': profile.user.id,
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = Work.objects.all()[0]
        self.assertEqual(work.id, work1.id)
        self.assertEqual(work.title, data['title'])
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_post_as_other(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        performer = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile2.user)
        url = reverse('works:edit_work', args=[work1.id])
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'composer': performer.user.id,
            "performers": [performer.user.id],
            "has_copyright": True,
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertNotEqual(work.title, data['title'])
        self.assertNotEqual(work.owner, profile.user)

    def test_edit_work_reset_keywords(self):
        """post """
        keyword1 = KeywordFactory.create()
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        work1.keywords.add(keyword1)
        work1.save()
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        composer = IndividualFactory.create()
        data = {
            'title': 'Obladi-Oblada',
            'composer': profile.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'keywords': '',
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, profile.user)
        self.assertEqual(work.keywords.count(), 0)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_edit_work_keywords(self):
        """post """
        keyword1 = KeywordFactory.create()
        keyword2 = KeywordFactory.create()
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        work1.keywords.add(keyword1)
        work1.save()
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        composer = IndividualFactory.create()
        data = {
            'title': 'Obladi-Oblada',
            'composer': composer.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'keywords': '##'.join([str(keyword2.id), "NEW"]),
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, composer.user)
        self.assertEqual(work.keywords.count(), 2)
        self.assertEqual(work.keywords.filter(id=keyword1.id).count(), 0)
        self.assertEqual(work.keywords.filter(id=keyword2.id).count(), 1)
        self.assertEqual(work.keywords.filter(name='NEW').count(), 1)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))

    def test_edit_work_instruments(self):
        """post """
        instrument1 = InstrumentFactory.create()
        instrument2 = InstrumentFactory.create()
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        work1.instruments.add(instrument1)
        work1.save()
        url = reverse('works:edit_work', args=[work1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        composer = IndividualFactory.create()
        data = {
            'title': 'Obladi-Oblada',
            'composer': composer.user.id,
            "has_copyright": True,
            "performers": [performer.user.id],
            'instruments': '##'.join([str(instrument2.id)]),
            "visibility": Work.VISIBILITY_PUBLIC,
            'score_media': 'test.pdf',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.title, data['title'])
        self.assertEqual(work.owner, profile.user)
        self.assertEqual(work.composer, composer.user)
        self.assertEqual(work.instruments.count(), 1)
        self.assertEqual(work.instruments.filter(id=instrument1.id).count(), 0)
        self.assertEqual(work.instruments.filter(id=instrument2.id).count(), 1)
        self.assertRedirects(response, reverse('works:view_work', args=[work.id]))


class AjaxWorksListTest(BaseTestCase):

    def test_works_ajax_list_no_term(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABD2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(0, len(data))

    def test_works_ajax_list_term(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABD2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "ABC"})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(1, len(data))
        self.assertEqual(data[0]["id"], work1.id)
        self.assertEqual(data[0]["name"], work1.name)

    def test_works_ajax_list_several(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "ABC"})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(2, len(data))

    def test_works_ajax_list_term_contains(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABD2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "B"})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(0, len(data))

    def test_works_ajax_list_term_none(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABD2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "Z"})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(0, len(data))

    def test_works_ajax_list_only_public(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        WorkFactory.create(visibility=Work.VISIBILITY_PRIVATE, title="ABC2")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "ABC"})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(1, len(data))
        self.assertEqual(data[0]["id"], work1.id)
        self.assertEqual(data[0]["name"], work1.name)

    def test_works_ajax_list_term_anonymous(self):
        WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        url = reverse('works:works_ajax_list')
        response = self.client.get(url, data={'term': "ABC"})
        self.assertEqual(response.status_code, 302)


class DeleteWorkTest(BaseTestCase):

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:delete_work', args=[work1.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

    def test_view_as_user(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:delete_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_as_other(self):
        """post """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile2.user)
        url = reverse('works:delete_work', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_delete_as_anonymous(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:delete_work', args=[work1.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.id, work1.id)
        assert_popup_redirects(response, reverse('login'))

    def test_delete_as_user(self):
        """post """
        profile = IndividualFactory.create()
        performer = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:delete_work', args=[work1.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        queryset = Work.objects.all()
        self.assertEqual(0, queryset.count())
        assert_popup_redirects(response, reverse('profiles:personal_space'))

    def test_delete_as_other(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        performer = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile2.user)
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:delete_work', args=[work1.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)
        queryset = Work.objects.all()
        self.assertEqual(1, queryset.count())
        work = queryset[0]
        self.assertEqual(work.id, work.id)
