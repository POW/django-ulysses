# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory

from ..factories import WorkFactory
from ..models import Work


class PlaylistTest(BaseTestCase):

    def test_playlist_one(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, title="ABC1")
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 1)
        self.assertContains(response, work.audio_url)
        self.assertContains(response, work.title)

    def test_playlist_one_member(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 1)
        self.assertContains(response, work.audio_url)
        self.assertContains(response, work.title)

    def test_playlist_excluded(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, title="ABC1", exclude_from_playlist=True)
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 0)
        self.assertNotContains(response, work.audio_url)
        self.assertNotContains(response, work.title)

    def test_playlist_no_public_anonymous(self):
        work = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, title="ABC1")
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 0)
        self.assertNotContains(response, work.audio_url)
        self.assertNotContains(response, work.title)

    def test_playlist_public_anonymous(self):
        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, title="ABC1")
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 1)
        self.assertContains(response, work.audio_url)
        self.assertContains(response, work.title)

    def test_playlist_not_public(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work = WorkFactory.create(visibility=Work.VISIBILITY_PRIVATE, title="ABC1")
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 0)
        self.assertNotContains(response, work.audio_url)
        self.assertNotContains(response, work.title)

    def test_playlist_no_audio(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, title="ABC1", no_audio=True)
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('audio')), 0)
        self.assertNotContains(response, work.title)
