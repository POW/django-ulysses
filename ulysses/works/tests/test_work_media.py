# -*- coding: utf-8 -*-

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects
from ulysses.profiles.factories import IndividualFactory

from ..factories import WorkFactory
from ..models import Work


class WorkVideoTest(BaseTestCase):

    def test_view_public_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_member_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_private_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_public_as_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_member_as_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_private_as_other_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_view_private_as_owner(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_no_video(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC, no_video=True)
        url = reverse('works:view_work_video', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)


class WorkScoreTest(BaseTestCase):

    def test_view_public_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_member_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_private_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_public_as_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_member_as_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_private_as_other_member(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_view_private_as_owner(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_view_no_video(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PUBLIC, no_score=True)
        url = reverse('works:view_score', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)
