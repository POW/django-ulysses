# -*- coding: utf-8 -*-

from ulysses.generic.tests import BaseTestCase

from ..factories import WorkFactory, InstrumentFactory
from ..models import Work, Instrument


class InstrumentTest(BaseTestCase):

    def test_reassign_instruments(self):

        instrument1 = InstrumentFactory.create(name="violin")
        instrument2 = InstrumentFactory.create(name="guitar")
        instrument3 = InstrumentFactory.create(name="violon")

        work1 = WorkFactory.create(instruments=[instrument1, instrument2])
        work2 = WorkFactory.create(instruments=[instrument1])
        work3 = WorkFactory.create(instruments=[instrument1, instrument3])
        work4 = WorkFactory.create(instruments=[instrument3, instrument2])

        instrument3.name = instrument1.name
        instrument3.save()

        self.assertEqual(Instrument.objects.count(), 2)
        self.assertEqual(Instrument.objects.filter(id=instrument1.id).count(), 0)
        self.assertEqual(Instrument.objects.filter(id=instrument2.id).count(), 1)
        self.assertEqual(Instrument.objects.filter(id=instrument3.id).count(), 1)

        self.assertEqual(Work.objects.count(), 4)
        work1 = Work.objects.get(id=work1.id)
        work2 = Work.objects.get(id=work2.id)
        work3 = Work.objects.get(id=work3.id)
        work4 = Work.objects.get(id=work4.id)

        self.assertEqual(list(work1.instruments.all()), [instrument2, instrument3])
        self.assertEqual(list(work2.instruments.all()), [instrument3])
        self.assertEqual(list(work3.instruments.all()), [instrument3])
        self.assertEqual(list(work4.instruments.all()), [instrument2, instrument3])
