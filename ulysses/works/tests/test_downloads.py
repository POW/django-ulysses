# -*- coding: utf-8 -*-

from django.test.utils import override_settings
from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory, ContactsGroupFactory
from ulysses.social.factories import FollowingFactory

from ..factories import WorkFactory
from ..models import Work


@override_settings(DEBUG=False)
class DownloadDocumentTest(BaseTestCase):

    def test_download_video(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(video__filename='video.mp4', video__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.video.url)
        self.assertEqual(response['Content-Type'], 'video/mp4')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_video_anonymous(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(video__filename='video.mp4', video__data=data, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_public_video_anonymous(self):
        data = 'abcdef'
        work = WorkFactory.create(video__filename='video.mp4', video__data=data, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.video.url)
        self.assertEqual(response['Content-Type'], 'video/mp4')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_audio(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(audio__filename='audio.mp3', audio__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_audio', args=[work.id])
        self.assertEqual(url, work.get_audio_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.audio.url)
        self.assertEqual(response['Content-Type'], 'audio/mpeg')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_audio_anonymous(self):
        data = 'abcdef'
        work = WorkFactory.create(audio__filename='audio.mp3', audio__data=data, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:download_audio', args=[work.id])
        self.assertEqual(url, work.get_audio_url())
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_public_audio_anonymous(self):
        data = 'abcdef'
        work = WorkFactory.create(audio__filename='audio.mp3', audio__data=data, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:download_audio', args=[work.id])
        self.assertEqual(url, work.get_audio_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.audio.url)
        self.assertEqual(response['Content-Type'], 'audio/mpeg')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_score(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(score__filename='score.pdf', score__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_score', args=[work.id])
        self.assertEqual(url, work.get_score_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.score.url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_score_anonymous(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(score__filename='score.pdf', score__data=data, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:download_score', args=[work.id])
        self.assertEqual(url, work.get_score_url())
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_public_score_anonymous(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(score__filename='score.pdf', score__data=data, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:download_score', args=[work.id])
        self.assertEqual(url, work.get_score_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.score.url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_other(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(other__filename='other.pdf', other__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_other', args=[work.id])
        self.assertEqual(url, work.get_other_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.other.url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_other_anonymous(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(other__filename='other.pdf', other__data=data, visibility=Work.VISIBILITY_MEMBER)
        url = reverse('works:download_other', args=[work.id])
        self.assertEqual(url, work.get_other_url())
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_public_other_anonymous(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(other__filename='other.pdf', other__data=data, visibility=Work.VISIBILITY_PUBLIC)
        url = reverse('works:download_other', args=[work.id])
        self.assertEqual(url, work.get_other_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.other.url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_score_unknown_type(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(score__filename='other.zzz', score__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_score', args=[work.id])
        self.assertEqual(url, work.get_score_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.score.url)
        self.assertEqual(response['Content-Type'], 'application/octet-stream')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_unknown_video(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(video__filename='video.mp4', video__data=data, visibility=Work.VISIBILITY_MEMBER)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        work.delete()
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

    def test_download_video_private_allowed(self):
        profile1 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(
            video__filename='video.mp4', video__data=data, owner=profile1.user, visibility=Work.VISIBILITY_PRIVATE
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(response['X-Accel-Redirect'], work.video.url)
        self.assertEqual(response['Content-Type'], 'video/mp4')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_video_private_forbidden(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        data = 'abcdef'
        work = WorkFactory.create(
            video__filename='video.mp4', video__data=data, owner=profile2.user, visibility=Work.VISIBILITY_PRIVATE
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_video', args=[work.id])
        self.assertEqual(url, work.get_video_url())
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_score_missing(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create(no_score=True)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('works:download_score', args=[work.id])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)
