# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory, ContactsGroupFactory
from ulysses.social.factories import FollowingFactory

from ..factories import WorkFactory, InstrumentFactory, KeywordFactory, TechnologyFactory, OtherArtisticMediumFactory
from ..models import WorkViews, Work


class WorksListTest(BaseTestCase):

    def test_view_empty_as_anonymous(self):
        """view """
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_empty_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        work2 = WorkFactory.create(owner=profile.user)
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.get_absolute_url())
        self.assertContains(response, work2.get_absolute_url())

    def test_view_as_user(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user)
        work2 = WorkFactory.create(owner=profile2.user)
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.get_absolute_url())
        self.assertContains(response, work2.get_absolute_url())
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(2, len(soup.select(".list-item")))

    def test_view_as_owner(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        work1 = WorkFactory.create(owner=profile.user, visibility=Work.VISIBILITY_PRIVATE)
        work2 = WorkFactory.create(owner=profile2.user)
        work3 = WorkFactory.create(owner=profile2.user, visibility=Work.VISIBILITY_PRIVATE)
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, work1.get_absolute_url())
        self.assertContains(response, work2.get_absolute_url())
        self.assertNotContains(response, work3.get_absolute_url())
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select(".list-item")))

    def test_view_pagination(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        in_works = [WorkFactory.create(owner=profile2.user) for i in range(36)]
        out_works = [WorkFactory.create(owner=profile2.user) for i in range(3)]
        url = reverse('works:works_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(36, len(soup.select(".list-item")))
        self.assertEqual(18, len(soup.select(".hide-on-md .list-item")))

    def test_view_ordering_alpha(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        [WorkFactory.create(title=str(prefix) + "blabla") for prefix in range(40)]
        WorkFactory.create(title="zblabla")
        url = reverse('works:works_list')
        response = self.client.get(url, data={'orders': 'alpha:1$'})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(36, len(soup.select(".list-item")))
        self.assertEqual(18, len(soup.select(".hide-on-md .list-item")))
        for item in soup.select(".list-item"):
            content = str(item)
            self.assertFalse(content.find("zblabla") >= 0)

    def test_view_ordering_alpha_page2(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        [WorkFactory.create(title=str(prefix) + "blabla") for prefix in range(40)]
        WorkFactory.create(title="zblabla")
        url = reverse('works:works_list')
        response = self.client.get(url, data={'orders': 'alpha:1$', 'counter': 'items:42'})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(41, len(soup.select(".list-item")))
        self.assertEqual(0, len(soup.select(".hide-on-md .list-item")))
        self.assertContains(response, "zblabla")


class WorksSearchTest(BaseTestCase):

    def test_search_instruments_as_anonymous(self):
        """view """
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        instrument1 = InstrumentFactory.create()
        instrument2 = InstrumentFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, instruments=[instrument1])
        work2 = WorkFactory.create(owner=profile2.user, instruments=[instrument1, instrument2])
        work3 = WorkFactory.create(owner=profile3.user, instruments=[instrument2])

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'instruments:{0}$'.format(instrument1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_instruments_as_user(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        instrument1 = InstrumentFactory.create()
        instrument2 = InstrumentFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, instruments=[instrument1])
        work2 = WorkFactory.create(owner=profile2.user, instruments=[instrument1, instrument2])
        work3 = WorkFactory.create(owner=profile3.user, instruments=[instrument2])

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'instruments:{0}$'.format(instrument1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_keywords(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        keyword1 = KeywordFactory.create()
        keyword2 = KeywordFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, keywords=[keyword1])
        work2 = WorkFactory.create(owner=profile2.user, keywords=[keyword1, keyword2])
        work3 = WorkFactory.create(owner=profile3.user, keywords=[keyword2])

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'keywords:{0}$'.format(keyword1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_technology(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        tech1 = TechnologyFactory.create()
        tech2 = TechnologyFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, technologies=[tech1])
        work2 = WorkFactory.create(owner=profile2.user, technologies=[tech1, tech2])
        work3 = WorkFactory.create(owner=profile3.user, technologies=[tech2])

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'technologies:{0}$'.format(tech1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_artistic_medium(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        art_medium1 = OtherArtisticMediumFactory.create()
        art_medium2 = OtherArtisticMediumFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, other_artistic_media=[art_medium1])
        work2 = WorkFactory.create(owner=profile2.user, other_artistic_media=[art_medium1, art_medium2])
        work3 = WorkFactory.create(owner=profile3.user, other_artistic_media=[art_medium2])

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'other_artistic_medium:{0}$'.format(art_medium1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_duration(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, duration=30)
        work2 = WorkFactory.create(owner=profile2.user, duration=22)
        work3 = WorkFactory.create(owner=profile3.user, duration=31)

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'duration:{0}$'.format(30)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_with_electronics(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        work1 = WorkFactory.create(owner=profile1.user, with_electronics=True)
        work2 = WorkFactory.create(owner=profile2.user, with_electronics=True)
        work3 = WorkFactory.create(owner=profile3.user, with_electronics=False)

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'with_electronics:{0}$'.format(1)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertEqual(2, len(soup.select(".list-item")))
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_search_pagination(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()

        instrument1 = InstrumentFactory.create()

        [WorkFactory.create(owner=profile1.user, instruments=[instrument1]) for i in range(37)]

        url = reverse('works:works_list')
        response = self.client.get(url, data={'filters': 'instrument:{0}$'.format(instrument1.id)})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(36, len(soup.select(".list-item")))

    def test_search_pagination_page2(self):
        """view """

        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        profile1 = IndividualFactory.create()
        instrument1 = InstrumentFactory.create()

        [WorkFactory.create(owner=profile1.user, instruments=[instrument1]) for i in range(37)]

        url = reverse('works:works_list')
        response = self.client.get(
            url, data={'filters': 'instrument:{0}$'.format(instrument1.id), 'counter': 'items:54$'}
        )
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(37, len(soup.select(".list-item")))
