# -*- coding: utf-8 -*-

from django.urls import reverse

from ulysses.generic.tests import BaseAPITestCase

from ulysses.profiles.factories import IndividualFactory
from ulysses.social.factories import WorkBookmark

from ..factories import WorkFactory


class WorkDetailApiTest(BaseAPITestCase):

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile.user)
        url = reverse('works:works_api_detail', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_view_work_not_found(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:works_api_detail', args=[1222222])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

    def test_view_work(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile2.user)
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:works_api_detail', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        data = response.data
        self.assertEqual(work1.id, data['id'])
        self.assertEqual(work1.title, data['title'])
        self.assertEqual(False, data['isBookmarked'])

    def test_view_bookmarked_work(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        work1 = WorkFactory.create(owner=profile2.user)
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        WorkBookmark.objects.create(work=work1, member_user=profile.user)
        url = reverse('works:works_api_detail', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        data = response.data
        self.assertEqual(work1.id, data['id'])
        self.assertEqual(work1.title, data['title'])
        self.assertEqual(True, data['isBookmarked'])

    def test_view_work_no_owner_profile(self):
        """view """
        profile = IndividualFactory.create()
        work1 = WorkFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('works:works_api_detail', args=[work1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        data = response.data
        self.assertEqual(work1.id, data['id'])
        self.assertEqual(work1.title, data['title'])
        self.assertEqual(False, data['isBookmarked'])
