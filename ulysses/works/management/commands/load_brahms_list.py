# -*- coding: utf-8 -*-

import os.path

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.works.models import Instrument


class Command(BaseCommand):
    """
    load Instruments from "Brahms list"
    """
    help = 'load Instruments from "Brahms list"'

    def matching_brahms(self, brahms_list, instrument_name):
        for elt in brahms_list:
            print("****", instrument_name.lower() in elt.lower())
            if instrument_name.lower() in elt.lower():
                return elt

    def handle(self, *args, **options):

        app_dir = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))

        print(app_dir)

        data_dir = os.path.join(app_dir, 'data/brahms_list_genre.txt')

        brahms_list = []
        with open(data_dir, 'r') as file:
            for line in file:
                level = 1 + line.count('    ')
                name = line.strip()
                print(level, name)
                if name == 2:
                    brahms_list.append(name)

        for instrument in Instrument.objects.all():
            print(instrument.name, self.matching_brahms(brahms_list, instrument.name))
