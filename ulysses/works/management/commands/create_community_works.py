# -*- coding: utf-8 -*-

import os.path
import pycountry

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.community import models
from ulysses.community.utils import download_ftp, copy_media
from ulysses.reference.models import Country

from ulysses.works.models import get_user_works_directory, Work, Instrument, Keyword


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "load works from Ulysses Community DB"

    def get_country(self, code):
        country = pycountry.countries.get(alpha_2=code)
        try:
            return Country.objects.get(name=country.name)
        except Country.DoesNotExist:
            print("Country does'nt exist:", country.name, code)
            return None
        except Country.MultipleObjectsReturned:
            print("duplicate Country:", country.name, code)
            return Country.objects.filter(name=country.name)[0]

    def handle(self, *args, **options):

        fake = True

        folders = models.Folder.objects.all()

        videos = ('.m4a', '.m4v', '.mov', '.wma', '.mp4')
        audio = ('.mp3', '.wav', '.aif', '.aiff')
        doc = ('.pdf', )

        def get_file_type(ext):
            if ext in ('.m4a', '.m4v', '.mov', '.wma', '.mp4'):
                return 'video'

            if ext in ('.mp3', '.wav', '.aif', '.aiff'):
                return 'audio'

            if ext in ('.pdf', ):
                return 'score'

            return "?"

        folders_count = folders.count()

        for instrument in models.Instrument.objects.all():
            Instrument.objects.get_or_create(name=instrument.label)

        for keyword in models.FolderKeyword.objects.filter(in_use=True):
            if not Instrument.objects.filter(name=keyword.name).exists():
                Keyword.objects.get_or_create(name=keyword.name)

        for keyword in models.FolderCategory.objects.filter(in_use=True):
            Keyword.objects.get_or_create(name=keyword.name)

        for index, folder in enumerate(folders):

            owner = User.objects.get(email=folder.owner.email)

            dir_name = get_user_works_directory(owner.id)

            work = Work.objects.get_or_create(title=folder.title, owner=owner)[0]
            work.work_creation_date = None
            work.creation_date = folder.creation_date
            work.notes = folder.description

            composer_name = folder.composer
            # if index == 207:
            #     composer_name = u"Bojana Saljic"  # Unicde issue with composer_name "Bojana Šaljić"
            work.composer_name = composer_name

            if folder.composer_nationality:
                work.country = self.get_country(folder.composer_nationality)

            work.work_creation_date = folder.work_creation_date
            work.duration = folder.work_length
            if folder.post_as == 3:
                work.post_as = Work.POSTED_AS_PRODUCER
            elif folder.post_as == 2:
                work.post_as = work.POSTED_AS_COMPOSER
                work.composer = work.owner
            else:
                work.post_as = Work.POSTED_AS_UNKNOWN
            work.has_copyright = folder.copyright
            work.allow_documents_printing = folder.allow_printing

            if folder.photo:
                media_path = copy_media(folder.photo_storage_path, dir_name, folder.photo, fake=fake)
                work.image = media_path

            for attachment in folder.attachment_set.all():

                ext = os.path.splitext(attachment.file)[-1].lower()

                file_type = get_file_type(ext)

                media_path = copy_media(attachment.file_storage_path, dir_name, attachment.file, fake=fake)

                setattr(work, file_type, media_path)

            work.save()

            if folder.category:
                work.keywords.add(Keyword.objects.get(name=folder.category.name))

            for keyword in folder.keywords.all():
                if Instrument.objects.filter(name=keyword.name).exists():
                    work.instruments.add(Instrument.objects.get(name=keyword.name))
                else:
                    work.keywords.add(Keyword.objects.get(name=keyword.name))

