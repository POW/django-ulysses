# -*- coding: utf-8 -*-

import csv
import os

from django.core.management.base import BaseCommand
from ulysses.works import models

filename = 'people-work.csv'

# models.Work.objects.all()[0].owner.email
# => test@test.com

def export_users_with_works():
    users = {}
    works = models.Work.objects.all()
    for work in works:
        if work.owner.email not in users:
            owner = work.owner
            users[work.owner.email] = {
                    'email': owner.email,
                    'first_name': owner.first_name,
                    'last_name': owner.last_name
            }
    
    with open(filename, 'w') as writeFile:
        fieldnames = ['email', 'first_name', 'last_name']
        writer = csv.DictWriter(writeFile, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(users.values())
    
    writeFile.close()

    print('file has been written to', os.path.join(os.getcwd(), filename))


class Command(BaseCommand):

    def handle(self, *args, **options):
        export_users_with_works()
