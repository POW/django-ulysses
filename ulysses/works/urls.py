# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import (
    AddWorkView, WorkView, WorkListView, EditWorkView, upload_work_file, works_ajax_list,
    WorkMediaDownloadView, CopyWorkToMediaView, WorkVideoView, WorksAPIView, DeleteWorkView, WorkScorePopupView
)

app_name = 'ulysses-works'


urlpatterns = [
    url(r'^view/(?P<id>\d+)/$', WorkView.as_view(), name='view_work'),
    url(r'^view/(?P<work_id>\d+)/video/$', WorkVideoView.as_view(), name='view_work_video'),
    url(r'^edit/(?P<id>\d+)/$', EditWorkView.as_view(), name='edit_work'),
    url(r'^delete/(?P<id>\d+)/$', DeleteWorkView.as_view(), name='delete_work'),
    url(r'^add/$', AddWorkView.as_view(), name='add_work'),
    url(r'^list/$', WorkListView.as_view(), name='works_list'),
    url(r'upload-file/$', upload_work_file, name='upload_work_file'),
    url(r'upload-file/(?P<user_id>\d+)/$', upload_work_file, name='upload_work_file'),
    url(r'^ajax-list/$', works_ajax_list, name='works_ajax_list'),
    url(r'^download-video/(?P<id>\d+)/$', WorkMediaDownloadView.as_view(field_name='video'), name='download_video'),
    url(r'^download-audio/(?P<id>\d+)/$', WorkMediaDownloadView.as_view(field_name='audio'), name='download_audio'),
    url(r'^download-score/(?P<id>\d+)/$', WorkMediaDownloadView.as_view(field_name='score'), name='download_score'),
    url(r'^download-other/(?P<id>\d+)/$', WorkMediaDownloadView.as_view(field_name='other'), name='download_other'),
    url(r'^copy-work-to-media/(?P<id>\d+)/$', CopyWorkToMediaView.as_view(), name='copy_work_to_media'),
    url(r'^works-api/(?P<pk>\d+)/$', WorksAPIView.as_view(), name="works_api_detail"),
    url(r'^view-score/(?P<id>\d+)/$', WorkScorePopupView.as_view(), name='view_score'),

]
