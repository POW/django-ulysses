# -*- coding: utf-8 -*-

from django.utils.translation import ugettext_lazy as _, ugettext

import floppyforms.__future__ as forms

from ulysses.generic.forms import FileUploadForm, ItemsSearchForm
from ulysses.generic.widgets import HtmlWidget, StaticLabelWidget
from ulysses.profiles.fields import MultipleUserChoiceField
from ulysses.reference.models import Country

from .fields import MultipleKeywordsChoiceField
from .models import Work, Keyword, Instrument, Technology, OtherArtisticMedium


class WorkForm(FileUploadForm):
    """add or edit a work"""
    instance = None
    performers = MultipleUserChoiceField(required=False, label=_('Performers'))
    keywords = MultipleKeywordsChoiceField(required=False, label=_('Keywords'))
    premiere_venue = forms.CharField(
        required=False,
        label=_('Venue of the premiere'),
        widget=forms.TextInput(attrs={'placeholder': _('Type in the name of the venue of the premiere')})
    )
    country_name = forms.CharField(required=False, widget=forms.TextInput(attrs={'readonly': 'readonly'}))
    country = forms.CharField(required=False, widget=forms.HiddenInput())
    media_label = forms.CharField(
        required=False,
        widget=StaticLabelWidget(_("Please upload at least a score, or an audio or a video file.")),
        label=''
    )

    class Meta:
        model = Work
        fields = (
            'image', 'title', 'composer_name', 'composer', 'performers', 'instruments', 'with_electronics',
            'other_artistic_medium', 'technologies',
            'work_creation_date',
            'premiere_venue', 'place_name', 'city', 'country', 'country_name', 'location',
            'duration', 'notes', 'link1', 'link2',
            'keywords',
            'media_label', 'score', 'audio', 'video',
            'allow_documents_printing', 'has_copyright',
            'visibility',
        )
        upload_fields = ('image', 'score', 'audio', 'video', )
        upload_fields_args = {
            'image': {
                'extensions': 'png|jpg',
                'label': _("Work image"),
                'upload_url_name': 'works:upload_work_file',
                'no_help': True,
            },
            'score': {
                'extensions': 'pdf', 'label': _("Score file"), 'upload_url_name': 'works:upload_work_file',
                'no_help': True,
            },
            'audio': {
                'extensions': 'mp3', 'label': _("Audio file"), 'upload_url_name': 'works:upload_work_file',
                'no_help': True,
            },
            'video': {
                'extensions': 'mp4|webm', 'label': _("Video file"), 'upload_url_name': 'works:upload_work_file',
                'no_help': True,
            },
        }
        widgets = {
            'composer': forms.HiddenInput,
            'work_creation_date': forms.TextInput(),
            'notes': HtmlWidget(),
            'place_name': forms.TextInput(attrs={'readonly': 'readonly'}),
            'city': forms.TextInput(attrs={'readonly': 'readonly'}),
            'location': forms.HiddenInput(),
        }

    def __init__(self, data=None, *args, **kwargs):
        self.instance = kwargs.get('instance', None)
        initial = kwargs.get('initial', None) or {}
        if self.instance and self.instance.country:
            initial['country'] = self.instance.country.code.upper()
            initial['country_name'] = self.instance.country.name
            kwargs['initial'] = initial
        super(WorkForm, self).__init__(data, *args, **kwargs)
        self.instance = kwargs.get('instance', None)
        self.fields['visibility'].required = False
        self.fields['has_copyright'].required = True
        if self.instance and self.instance.id:
            self.fields['performers'].queryset = self.instance.performers.all()
            self.fields['performers'].set_initial(self.instance.performers.all())

    def clean(self):
        title = self.cleaned_data.get("title")
        composer = self.cleaned_data.get("composer")
        queryset = Work.objects.filter(title__iexact=title, composer=composer)
        if self.instance:
            queryset = queryset.exclude(id=self.instance.id)
        if queryset.count() > 0:
            raise forms.ValidationError(ugettext('This work already exists'))

        media_fields = ('score', 'audio', 'video')
        media_values = [self.cleaned_data.get(media_field + "_media") for media_field in media_fields]
        if not any(media_values):
            media_error_text = ugettext("Please upload at least a score, or an audio or a video file.")
            self.fields['media_label'].widget.has_error = True
            for media_field in media_fields:
                self.add_error(media_field + '_media', media_error_text)

        return super(WorkForm, self).clean()

    def clean_country(self):
        country = self.cleaned_data.get('country')
        if country:
            try:
                return Country.objects.get(code=country.lower())
            except Country.DoesNotExist:
                raise forms.ValidationError(ugettext('Unknown country'))
        return None

    def save(self, *args, **kwargs):
        instance = super(WorkForm, self).save()
        instance.save()
        return instance


class WorkSearchForm(ItemsSearchForm):
    """search for works"""

    instruments = forms.ModelChoiceField(
        queryset=Instrument.objects.all(), required=False, label=_('Instruments')
    )

    other_artistic_medium = forms.ModelChoiceField(
        queryset=OtherArtisticMedium.objects.all(), required=False, label=_('Music and ...')
    )

    with_electronics = forms.ChoiceField(
        required=False, label=_('With electronics'), choices=(('', ''), (0, _('No')), (1, _('Yes'), ))
    )
    technologies = forms.ModelChoiceField(
        queryset=Technology.objects.all(), required=False, label=_('Technologies')
    )
    keywords = forms.ModelChoiceField(queryset=Keyword.objects.all(), required=False, label=_('Keywords'))

    # duration = IntRangeField(required=False, label=_(u'Duration'), help_text=_(u'in minutes'))
    duration = forms.ChoiceField(
        required=False, label=_('Duration'),
        choices=(
            ('', ''),
            (1, _('Less than 1 minute')),
            (2, _('Less than 2 minutes')),
            (3, _('Less than 3 minutes')),
            (4, _('Less than 4 minutes')),
            (5, _('Less than 5 minutes')),
            (6, _('Less than 6 minutes')),
            (7, _('Less than 7 minutes')),
            (8, _('Less than 8 minutes')),
            (9, _('Less than 9 minutes')),
            (10, _('Less than 10 minutes')),
            (11, _('Less than 11 minutes')),
            (12, _('Less than 12 minutes')),
            (13, _('Less than 13 minutes')),
            (14, _('Less than 14 minutes')),
            (15, _('Less than 15 minutes')),
            (30, _('Less than 30 minutes')),
            (60, _('Less than 1 hour')),
            (120, _('Less than 2 hours')),
        )
    )

    def __init__(self, *args, **kwargs):
        super(WorkSearchForm, self).__init__(*args, **kwargs)
        self._post_init()
