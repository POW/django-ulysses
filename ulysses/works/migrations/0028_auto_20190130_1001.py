# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-01-30 10:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_works', '0027_auto_20181105_1109'),
    ]

    operations = [
        migrations.AlterField(
            model_name='work',
            name='city',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='City'),
        ),
        migrations.AlterField(
            model_name='work',
            name='composer_name',
            field=models.CharField(blank=True, db_index=True, default='', max_length=100, verbose_name='composer name'),
        ),
        migrations.AlterField(
            model_name='work',
            name='location',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Location'),
        ),
        migrations.AlterField(
            model_name='work',
            name='place_name',
            field=models.CharField(blank=True, default='', max_length=100, verbose_name='Place'),
        ),
    ]
