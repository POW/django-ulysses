# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-11 16:56


from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_works','0015_auto_20170324_1102'),
    ]

    operations = [
        migrations.AlterField(
            model_name='work',
            name='participants',
            field=models.ManyToManyField(blank=True, related_name='work_as_participant_set', to=settings.AUTH_USER_MODEL, verbose_name='Collaborators'),
        ),
    ]
