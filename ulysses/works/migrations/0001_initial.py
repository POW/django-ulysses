# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-10 15:18


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ulysses_reference', '0003_remove_city'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Work',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=400)),
                ('score', models.CharField(blank=True, help_text=b"score file url, relative to application's MEDIA_URL", max_length=400, null=True)),
                ('audio', models.CharField(blank=True, help_text=b"audio file url, relative to application's MEDIA_URL", max_length=400, null=True)),
                ('video', models.CharField(blank=True, help_text=b"video file url, relative to application's MEDIA_URL", max_length=400, null=True)),
                ('notes', models.TextField(blank=True, max_length=4000, null=True, verbose_name='notes')),
                ('link', models.URLField(blank=True, null=True, verbose_name='link')),
                ('creation_date', models.DateTimeField(auto_now_add=True, verbose_name='creation date')),
                ('update_date', models.DateTimeField(auto_now=True, null=True, verbose_name='last update date')),
                ('order_index', models.IntegerField(blank=True, help_text='used to handle display order', null=True, verbose_name='order index')),
                ('image', models.ImageField(blank=True, default=None, null=True, upload_to='works', verbose_name='image')),
                ('work_creation_date', models.DateField(blank=True, default=None, null=True, verbose_name='Date of work creation')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('participants', models.ManyToManyField(blank=True, related_name='work_as_participant_set', to=settings.AUTH_USER_MODEL, verbose_name='participants')),
                ('work_creation_country', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Country', verbose_name='Country of work creation')),
            ],
            options={
                'verbose_name': 'Work',
                'verbose_name_plural': 'Works',
            },
        ),
    ]
