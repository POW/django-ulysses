# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-03-24 11:02


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_works','0014_auto_20170317_1354'),
    ]

    operations = [
        migrations.AlterField(
            model_name='work',
            name='creation_date',
            field=models.DateTimeField(auto_now_add=True, verbose_name='creation date'),
        ),
        migrations.AlterField(
            model_name='work',
            name='instruments',
            field=models.ManyToManyField(blank=True, to='ulysses_works.Instrument', verbose_name='Instrument(s)'),
        ),
        migrations.AlterField(
            model_name='work',
            name='keywords',
            field=models.ManyToManyField(blank=True, to='ulysses_works.Keyword', verbose_name='Keyword(s)'),
        ),
    ]
