# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-10-12 14:04


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_works', '0025_work_place_name'),
    ]

    operations = [
        migrations.AlterField(
            model_name='work',
            name='notes',
            field=models.TextField(blank=True, max_length=800, null=True, verbose_name='description'),
        ),
        migrations.AlterField(
            model_name='work',
            name='other_artistic_medium',
            field=models.ManyToManyField(blank=True, to='ulysses_works.OtherArtisticMedium', verbose_name='Music and...'),
        ),
        migrations.AlterField(
            model_name='work',
            name='work_creation_date',
            field=models.DateField(blank=True, default=None, null=True, verbose_name='Work premiered'),
        ),
    ]
