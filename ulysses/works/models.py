# -*- coding: utf-8 -*-

import mimetypes
import os
import os.path

from django.conf import settings
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.db import models
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext_lazy as _

from sorl.thumbnail import get_thumbnail

from ulysses.generic.directories import DirectoryName, WORKS_DIRECTORY
from ulysses.generic.models import NameIndexBaseModel
from ulysses.reference.models import Country


def works_directory(*args):
    """return where to upload file"""
    return DirectoryName(WORKS_DIRECTORY)(*args)


class Instrument(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("name"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _("Instrument")
        verbose_name_plural = _("Instruments")

    def works(self):
        return Work.objects.filter(instruments=self)

    def works_count(self):
        return self.works().count()

    def save(self, *args, **kwargs):
        ret = super(Instrument, self).save(*args, **kwargs)
        if self.id:
            siblings = Instrument.objects.filter(name=self.name).exclude(id=self.id)
            for sibling in siblings:
                for work in sibling.works():
                    work.instruments.remove(sibling)
                    work.instruments.add(self)
                    work.save()
            siblings.delete()
        return ret


class Keyword(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("name"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _("Keyword")
        verbose_name_plural = _("Keywords")


class Technology(NameIndexBaseModel):
    """
    technologies
    examples : real time interaction, improvisation, computer assisted composition, sound spatialization, autre
    """

    class Meta:
        ordering = ['order_index', 'name']
        verbose_name = _("Technology")
        verbose_name_plural = _("Technologies")


class OtherArtisticMedium(NameIndexBaseModel):
    """
    Other artistic medium
    examples : visual arts, video, theatre, dance, installation, other
    """

    class Meta:
        ordering = ['order_index', 'name']
        verbose_name = _("Other artistic medium")
        verbose_name_plural = _("Other artistic media")

    def works(self):
        return Work.objects.filter(other_artistic_medium=self)

    def works_count(self):
        return self.works().count()


class Work(models.Model):
    """A work is a piece played by an artist"""

    POSTED_AS_UNKNOWN = 0
    POSTED_AS_COMPOSER = 1
    POSTED_AS_PRODUCER = 2

    POSTED_AS_CHOICES = (
        (POSTED_AS_UNKNOWN, ''),
        (POSTED_AS_COMPOSER, _('Composer')),
        (POSTED_AS_PRODUCER, _('Producer')),
    )

    ACCESS_MEMBERS = 2
    ACCESS_FOLLOWERS = 3
    ACCESS_DEFINED = 4
    ACCESS_USER = 5
    ACCESS_GROUP = 6

    VISIBILITY_MEMBER = 1
    VISIBILITY_PRIVATE = 2
    VISIBILITY_PUBLIC = 3

    ACCESS_CHOICES = (
        # (ACCESS_ALL, _(u'All')),
        (ACCESS_MEMBERS, _('All Members')),
        (ACCESS_FOLLOWERS, _('My Followers')),
        (ACCESS_DEFINED, _('Selected contacts')),
        (ACCESS_GROUP, _('Contacts of group')),
        (ACCESS_USER, _('Private')),
    )

    VISIBILITY_CHOICES = (
        (VISIBILITY_PUBLIC, _('Public')),
        (VISIBILITY_MEMBER, _('Ulysses network')),
        (VISIBILITY_PRIVATE, _('Just yourself')),
    )

    title = models.CharField(max_length=200, db_index=True)
    notes = models.TextField(verbose_name=_("description"), max_length=800, blank=True, null=True)

    posted_as = models.IntegerField(verbose_name=_('Posted as'), choices=POSTED_AS_CHOICES, default=POSTED_AS_UNKNOWN)

    image = models.ImageField(
        verbose_name=_('Image'), upload_to=works_directory, blank=True, default=None, null=True, max_length=200,
        help_text=_('Supported formats: JPG, PNG. 100MB max.')
    )
    score = models.FileField(
        verbose_name=_('Score'), upload_to=works_directory, blank=True, default=None, null=True, max_length=200,
        help_text=_('Supported formats: PDF. 100MB max.')
    )
    audio = models.FileField(
        verbose_name=_('Audio'), upload_to=works_directory, blank=True, default=None, null=True, max_length=200,
        help_text=_("Supported format: mp3 (bitrate ≥ 256Kbps). 100MB max.")
    )
    video = models.FileField(
        verbose_name=_('Video'), upload_to=works_directory, blank=True, default=None, null=True, max_length=200,
        help_text=_(
            "Supported formats: mp4 (H.264 video codec with AAC or MP3 stereo audio codec)"
            " or WebM (VP8 video codec with Vorbis audio codec). 100MB max."
        )
    )
    link1 = models.URLField(verbose_name=_("Link 1"), blank=True, null=True, default=None)
    link2 = models.URLField(verbose_name=_("Link 2"), blank=True, null=True, default=None)
    other = models.FileField(
        verbose_name=_('Other'), upload_to=works_directory, blank=True, default=None, null=True, max_length=200,
        help_text=_(
            "Supported formats: jpg, png, pdf, mp3 (bandwidth ≥ 256Kbps), "
            "mp4 (H.264 video codec with AAC or MP3 stereo audio codec) or "
            "WebM (VP8 video codec with Vorbis audio codec). 100MB max."
        )
    )

    creation_date = models.DateTimeField(verbose_name=_("creation date"), auto_now_add=True)
    update_date = models.DateTimeField(verbose_name=_("last update date"), blank=True, null=True, auto_now=True)

    owner = models.ForeignKey(User, verbose_name=_('owner'), on_delete=models.CASCADE)
    composer = models.ForeignKey(
        User, blank=True, default=None, null=True, verbose_name=_('composer'),
        related_name='work_as_composer_set', on_delete=models.SET_NULL
    )
    composer_name = models.CharField(
        max_length=100, blank=True, default="", verbose_name=_('composer name'), db_index=True
    )
    performers = models.ManyToManyField(
        User, verbose_name=_('Performers'), blank=True, related_name='work_as_performer_set'
    )
    participants = models.ManyToManyField(
        User, verbose_name=_('Collaborators'), blank=True, related_name='work_as_participant_set'
    )
    work_creation_date = models.DateField(verbose_name=_('Work premiered'), blank=True, default=None, null=True)
    # location = gpl_latitude#gps_longitude#country_code
    location = models.CharField(verbose_name=_('Location'), default='', blank=True, max_length=100)
    place_name = models.CharField(verbose_name=_('Place'), default='', blank=True, max_length=100)
    city = models.CharField(verbose_name=_('City'), default='', blank=True, max_length=100)
    country = models.ForeignKey(
        Country, verbose_name=_('Country'), default=None, blank=True, null=True, on_delete=models.SET_NULL
    )

    instruments = models.ManyToManyField(Instrument, blank=True, verbose_name=_("Instrument(s)"))
    keywords = models.ManyToManyField(Keyword, blank=True, verbose_name=_("Keyword(s)"))

    duration = models.IntegerField(
        verbose_name=_("duration"), default=None, blank=True, null=True, help_text=_('in minutes')
    )

    allow_documents_printing = models.BooleanField(verbose_name=_('Allow document printing'), default=True)
    has_copyright = models.BooleanField(
        verbose_name=_('Copyright'), default=False, blank=False,
        help_text=_('I have the publication rights for this work')
    )
    ulysses_label = models.BooleanField(default=False, verbose_name=_('Ulysses label'), db_index=True)
    with_electronics = models.BooleanField(default=False, verbose_name=_('With electronics'), db_index=True)
    technologies = models.ManyToManyField(Technology, verbose_name=_('technologies'), blank=True)
    other_artistic_medium = models.ManyToManyField(
        OtherArtisticMedium, verbose_name=_('Music and...'), blank=True
    )

    access_type = models.IntegerField(
        verbose_name=_('access type'), default=ACCESS_MEMBERS, choices=ACCESS_CHOICES
    )
    visibility = models.IntegerField(
        verbose_name=_('visibility'), default=VISIBILITY_PUBLIC, choices=VISIBILITY_CHOICES
    )
    allowed_users = models.ManyToManyField(
        User, blank=True, verbose_name=_('Allowed users'), related_name="+",
        help_text=_('These users will be allowed to access this work if option is set')
    )
    allowed_groups = models.ManyToManyField(
        "ulysses_profiles.ContactsGroup", blank=True, verbose_name=_('Allowed groups'),
        help_text=_('These groups of contacts will be allowed to access this work if option is set')
    )
    exclude_from_playlist = models.BooleanField(default=False, verbose_name=_('exclude from playlist'))

    def __str__(self):
        if self.composer_name:
            return '{0} ({1})'.format(self.title, self.composer_name)
        return self.title

    @property
    def name(self):
        return self.title

    @property
    def audio_url(self):
        return self.get_audio_url()
        # FOR TEST
        # test_urls = [
        #     'http://www.afinerweb.com/music/bensound-slowmotion.1.mp3',
        #     'http://www.afinerweb.com/music/bensound-slowmotion.2.mp3',
        #     "http://www.alexkatz.me/codepen/music/interlude.mp3",
        # ]
        # import random
        # index = random.randint(0, len(test_urls)) - 1
        # return test_urls[index]

    @property
    def owners(self):
        users = [self.owner]
        if self.composer and self.composer != self.owner:
            users.append(self.composer)
        return users

    class Meta:
        verbose_name = _('Work')
        verbose_name_plural = _('Works')
        ordering = ['-update_date', 'title']

    def get_absolute_url(self):
        return reverse('works:view_work', args=[self.id])

    def get_thumbnail(self):
        return self.get_image("100x100")

    def get_image_fb(self):
        return self.get_image("1200x630")

    def get_square_medium_image(self):
        return self.get_image("400x400", crop="center")

    def get_large_image(self):
        return self.get_image("1200x240")

    def get_player_thumbnail(self):
        if self.image:
            try:
                return get_thumbnail(self.image, "64x64", crop='center').url
            except:
                pass
        return '{0}icons/player-turntable.svg'.format(settings.STATIC_URL)

    def get_image(self, size='400x400', crop='center'):
        if self.image:
            try:
                return get_thumbnail(self.image, size, crop=crop).url
            except:
                pass
        return '{0}images/placeholder_work.jpg'.format(settings.STATIC_URL)

    def is_public(self):
        return self.visibility != Work.VISIBILITY_PRIVATE

    @property
    def favorites_count(self):
        return self.bookmarks.count()

    def is_user_allowed(self, user):
        """Is the user allowed to acces this work"""
        if user.is_anonymous:
            return self.visibility == Work.VISIBILITY_PUBLIC

        # The owner is always allowed to access
        if user == self.owner:
            return True
        else:
            # Other users can only access public works
            return self.is_public()

    def get_composer_name(self):
        if self.composer:
            return '{0} {1}'.format(self.composer.first_name, self.composer.last_name.upper())
        else:
            return self.composer_name

    def instruments_str(self):
        return ", ".join([instrument.name for instrument in self.instruments.all()])

    def keywords_str(self):
        return ", ".join([keyword.name for keyword in self.keywords.all()])

    def technologies_str(self):
        return ", ".join([tech.name for tech in self.technologies.all()])

    def other_artistic_medium_str(self):
        return ", ".join([other.name for other in self.other_artistic_medium.all()])

    def latitude(self):
        return self.location.split("#")[0]

    def longitude(self):
        return self.location.split("#")[1]

    def place(self):
        value = ''
        if self.city and self.country:
            value = '{0} ({1})'.format(self.city, self.country.name)
        elif self.city:
            value = self.city
        elif self.country:
            value = self.country.name
        if value and self.place_name:
            value = '{0}, {1}'.format(self.place_name, value)
        elif self.place_name:
            value = self.place_name
        return value

    def central_latitude(self):
        if self.location:
            return self.latitude()
        else:
            for event in self.event_set.all():
                if event.location:
                    return event.latitude()

    def central_longitude(self):
        if self.location:
            return self.longitude()
        else:
            for event in self.event_set.all():
                if event.location:
                    return event.longitude()

    def _get_file_name(self, obj):
        try:
            return os.path.split(obj.file.name)[-1]
        except (ValueError, IOError):
            pass

    def _is_valid(self, obj):
        try:
            obj.file.name
            return True
        except (ValueError, IOError):
            return False

    def other_file_name(self):
        return self._get_file_name(self.other)

    def video_file_name(self):
        return self._get_file_name(self.video)

    def audio_file_name(self):
        return self._get_file_name(self.audio)

    def score_file_name(self):
        return self._get_file_name(self.score)

    def is_other_valid(self):
        return self._is_valid(self.other)

    def is_video_valid(self):
        return self._is_valid(self.video)

    def is_audio_valid(self):
        return self._is_valid(self.audio)

    def is_score_valid(self):
        return self._is_valid(self.score)

    def _get_extension(self, obj):
        if obj:
            try:
                return os.path.splitext(obj.file.name)[-1].lower()
            except:
                pass

    def _get_file_format(self, obj):
        if obj:
            try:
                file_type = mimetypes.MimeTypes().guess_type(obj.file.name)[0]
                if file_type is None:
                    # On Dev server, it doesn't work. TODO : clean it
                    ext = os.path.splitext(obj.file.name)[-1]
                    if ext == '.m4v':
                        file_type = 'video/x-m4v'
                return file_type
            except:
                pass

    def composers(self):
        if self.composer:
            return [self.composer]

    def video_format(self):
        return self._get_file_format(self.video)

    def player_supported_format(self):
        if self.video_format() in ('video/mp4', 'video/webm', 'video/ogg', 'video/x-m4v'):
            return True
        return False

    def other_format(self):
        return self._get_file_format(self.other)

    def is_other_video(self):
        if self.other_format() in ('video/mp4', 'video/webm', 'video/ogg', 'video/x-m4v'):
            return True
        return False

    def is_other_audio(self):
        if self.other_format() in ('audio/mpeg', ):
            return True
        return False

    def audio_format(self):
        return self._get_file_format(self.audio)

    def get_video_url(self):
        if self.video:
            return reverse('works:download_video', args=[self.id])

    def get_audio_url(self):
        if self.audio:
            return reverse('works:download_audio', args=[self.id])

    def is_score_displayed_in_iframe(self):
        if self.score:
            ext = self._get_extension(self.score)
            return ext in ('.pdf',)
        return False

    def is_other_displayed_in_iframe(self):
        if self.other:
            ext = self._get_extension(self.other)
            return ext in ('.pdf',)
        return False

    def _viewerjs_iframe(self, doc_url):
        iframe = '''<iframe id="score" class="{2}" src="{0}ViewerJS/index.html#{1}"'''
        iframe += ''' height=500 width=100% allowfullscreen webkitallowfullscreen></iframe>'''
        return mark_safe(
            iframe.format(
                settings.STATIC_URL,
                doc_url,
                'no-document-printing' if not self.allow_documents_printing else ''
            )
        )

    def get_score_iframe(self):
        """get iframe to view document in ViewerJS document"""
        if self.score:
            return self._viewerjs_iframe(self.get_score_url())

    def get_other_iframe(self):
        """get iframe to view document in ViewerJS document"""
        if self.other:
            return self._viewerjs_iframe(self.get_other_url())

    def get_score_url(self):
        if self.score:
            return reverse('works:download_score', args=[self.id])

    def get_other_url(self):
        if self.other:
            return reverse('works:download_other', args=[self.id])

    def can_be_copied_to_media(self):
        try:
            composer = self.owner.composer
        except ObjectDoesNotExist:
            composer = None

        if composer and (self.score or self.audio or self.video):
            return composer

    def views_count(self):
        return self.workviews.count

    def bookmarks_count(self):
        return self.bookmarks.count()

    def owner_works(self):
        return self.owner.work_set.exclude(visibility=Work.VISIBILITY_PRIVATE).exclude(id=self.id)


class WorkViews(models.Model):
    """Log the numbers of views by other users in order to calculate popularity"""
    work = models.OneToOneField(Work, verbose_name=_('work'), on_delete=models.CASCADE)
    count = models.IntegerField(default=0, verbose_name=_('views count'))

    def __str__(self):
        return '{0}'.format(self.work)

    class Meta:
        verbose_name = _('Work views')
        verbose_name_plural = _('Work views')
        ordering = ['-count']