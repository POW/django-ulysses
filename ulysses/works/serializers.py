# -*- coding: utf-8 -*-

from django.urls import reverse

from rest_framework.serializers import Serializer

from ulysses.middleware import get_request
from ulysses.profiles.utils import get_profile
from ulysses.social.utils import is_work_bookmarked_by


class WorkSerializer(Serializer):

    def to_representation(self, work):
        request = get_request()
        is_bookmarked = False
        if request.user.is_authenticated:
            is_bookmarked = is_work_bookmarked_by(work, request.user)

        owner_profile = get_profile(work.owner)

        return {
            'id': work.id,
            'title': work.title,
            'url': work.get_absolute_url(),
            'playerImage': work.get_player_thumbnail(),
            'thumbnail': work.get_thumbnail(),
            'isBookmarked':  is_bookmarked,
            'ownerAvatar': owner_profile.get_avatar() if owner_profile else '',
            'ownerAvatarClass': owner_profile.get_avatar_class() if owner_profile else '',
            'ownerFollowersCount': owner_profile.followers_count()  if owner_profile else 0,
            'ownerWorksCount': owner_profile.works_count()  if owner_profile else 0,
            'bookmarkUrl': reverse("social:bookmark_work", args=[work.id]),
            'refreshUrl': reverse("works:works_api_detail", args=[work.id]),
        }
