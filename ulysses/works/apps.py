# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.works'
    label = 'ulysses_works'
    verbose_name = "Ulysses Works"
