# -*- coding: utf-8 -*-

from django.contrib.admin import ModelAdmin

from ulysses.super_admin.sites import super_admin_site

from .models import Work, WorkViews, Instrument, Keyword, OtherArtisticMedium, Technology


class InstrumentAdmin(ModelAdmin):
    list_display = ("__str__", "name", 'works_count', )
    list_editable = ("name", )
    search_fields = ("name", )


class KeywordAdmin(ModelAdmin):
    list_display = ("__str__", "name",)
    list_editable = ("name",)
    search_fields = ("name",)


class WorkAdmin(ModelAdmin):
    list_display = (
        'title', 'owner', 'creation_date', 'update_date', "ulysses_label", 'visibility', 'exclude_from_playlist'
    )
    date_hierarchy = 'update_date'
    list_filter = ("ulysses_label", 'exclude_from_playlist', 'visibility', 'exclude_from_playlist')
    search_fields = ('title', )


class WorkViewsAdmin(ModelAdmin):
    list_display = ('__str__', 'count')
    search_fields = ("work__title", )


class OtherArtisticMediumAdmin(ModelAdmin):
    list_display = ('name', 'order_index', 'works_count', )
    search_fields = ("name", )


class TechnologyAdmin(ModelAdmin):
    list_display = ('name', 'order_index')
    search_fields = ("name", )


super_admin_site.register(Work, WorkAdmin)
super_admin_site.register(WorkViews, WorkViewsAdmin)
super_admin_site.register(Instrument, InstrumentAdmin)
super_admin_site.register(Keyword, KeywordAdmin)
super_admin_site.register(OtherArtisticMedium, OtherArtisticMediumAdmin)
super_admin_site.register(Technology, TechnologyAdmin)
