# -*- coding: utf-8 -*-

import factory

from ulysses.profiles.factories import ProfileUserFactory

from .models import Work, Instrument, Keyword, Technology, OtherArtisticMedium


class InstrumentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Instrument

    name = factory.Sequence(lambda n: 'instrument-{0}'.format(n))


class KeywordFactory(factory.DjangoModelFactory):
    class Meta:
        model = Keyword

    name = factory.Sequence(lambda n: 'keyword-{0}'.format(n))


class TechnologyFactory(factory.DjangoModelFactory):
    class Meta:
        model = Technology

    name = factory.Sequence(lambda n: 'technology-{0}'.format(n))


class OtherArtisticMediumFactory(factory.DjangoModelFactory):
    class Meta:
        model = OtherArtisticMedium

    name = factory.Sequence(lambda n: 'artistic-medium-{0}'.format(n))


class WorkFactory(factory.DjangoModelFactory):
    class Meta:
        model = Work

    title = factory.Sequence(lambda n: 'work-title-{0}#'.format(n))

    video = factory.django.FileField()
    audio = factory.django.FileField()
    score = factory.django.FileField()
    other = factory.django.FileField()

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        owner = kwargs.get('owner', None)
        performers = kwargs.pop('performers', [])
        participants = kwargs.pop('participants', [])
        instruments = kwargs.pop('instruments', [])
        keywords = kwargs.pop('keywords', [])
        technologies = kwargs.pop('technologies', [])
        other_artistic_media = kwargs.pop('other_artistic_media', [])
        no_audio = kwargs.pop('no_audio', False)
        no_video = kwargs.pop('no_video', False)
        no_score = kwargs.pop('no_score', False)

        if not owner:
            kwargs['owner'] = ProfileUserFactory.create()

        work = super(WorkFactory, cls)._create(model_class, *args, **kwargs)

        if no_video:
            work.video.delete()
            work.video = None

        if no_audio:
            work.audio.delete()
            work.audio = None

        if no_score:
            work.score.delete()
            work.score = None

        for participant in participants:
            work.participants.add(participant.user)

        for performer in performers:
            work.performers.add(performer.user)

        for instrument in instruments:
            work.instruments.add(instrument)

        for keyword in keywords:
            work.keywords.add(keyword)

        for technology in technologies:
            work.technologies.add(technology)

        for other_artistic_medium in other_artistic_media:
            work.other_artistic_medium.add(other_artistic_medium)

        work.save()
        return work
