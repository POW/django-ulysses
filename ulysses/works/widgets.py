# -*- coding: utf-8 -*-

from django.urls import reverse_lazy

from ulysses.generic.widgets import AutocompleteAutocreateWidget

from .models import Keyword, Instrument, Work


class KeywordAutocompleteWidget(AutocompleteAutocreateWidget):
    """An autocomplete widget for choosing or adding tags"""
    model = Keyword


class InstrumentAutocompleteWidget(AutocompleteAutocreateWidget):
    """An autocomplete widget for choosing or adding tags"""
    model = Instrument


class WorkAutocompleteWidget(AutocompleteAutocreateWidget):
    """An autocomplete widget for choosing or adding works"""
    model = Work
    autocomplete_url = reverse_lazy("works:works_ajax_list")

    def __init__(self, text_field, *args, **kwargs):
        super(WorkAutocompleteWidget, self).__init__(*args, **kwargs)
        self.text_field = text_field
