# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.generic.fields import MultipleElementChoiceField

from .models import Work
from .utils.querysets import get_works
from .widgets import KeywordAutocompleteWidget, InstrumentAutocompleteWidget, WorkAutocompleteWidget


class MultipleKeywordsChoiceField(forms.CharField):
    """Autocomplete select between keywords. Can create new ones"""

    def __init__(self, *args, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = KeywordAutocompleteWidget(placeholder_text=_('Enter a keyword'))
        super(MultipleKeywordsChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class MultipleInstrumentsChoiceField(forms.CharField):
    """Autocomplete select between instruments. Can create new ones"""

    def __init__(self, *args, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = InstrumentAutocompleteWidget(placeholder_text=_('Enter an instrument'))
        super(MultipleInstrumentsChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class MultipleWorksChoiceField(forms.CharField):
    """Autocomplete select between wroks. Can create new ones"""

    def __init__(self, text_field, *args, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = WorkAutocompleteWidget(
                placeholder_text=_('Enter a work name'), text_field=text_field
            )
        super(MultipleWorksChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class MultipleWorksReadonlyChoiceField(MultipleElementChoiceField):
    """Autocomplete select between works: do not create any"""

    def __init__(self, queryset=None, *args, **kwargs):
        if queryset is None:
            queryset = Work.objects.none()
        super(MultipleWorksReadonlyChoiceField, self).__init__(queryset=queryset, *args, **kwargs)

    def label_from_instance(self, instance):
        """Get the label for a given user"""
        return instance.title

    def get_element_queryset(self):
        return get_works(order_by=['title'])
