# -*- coding: utf-8 -*-

from haystack import indexes

from .models import Work


class WorkIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)
    content_auto = indexes.EdgeNgramField(model_attr='title')

    def get_model(self):
        return Work

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.exclude(visibility=Work.VISIBILITY_PRIVATE)
