# -*- coding: utf-8 -*-

import os.path
import json
from datetime import datetime

from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied

from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext as _, ugettext_lazy as __
from django.views.decorators.csrf import csrf_exempt

from rest_framework.generics import RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import (
    ProfileFormBaseView, ListBasePageView, DetailView, PrivateDownloadView, LoginRequiredPopupFormView,
    PopupView, PopupFormView
)
from ulysses.social.models import WorkBookmark, MemberPost, FeedItem
from ulysses.utils import upload_helper, get_back_url_for_search
from ulysses.generic.views import redirect_to_login
from ulysses.generic.directories import upload_file, WORKS_DIRECTORY

from .forms import WorkForm, WorkSearchForm
from .models import Work, WorkViews
from .serializers import WorkSerializer
from .utils.querysets import get_public_works_queryset, get_works_queryset
from .utils.media import copy_work_as_personal_element


@csrf_exempt
def upload_work_file(request, user_id=0):
    return upload_file(request, WORKS_DIRECTORY, user_id)


class WorkListView(ListBasePageView):
    """Displays the list of works"""
    template_name = 'works/works_list.html'

    ITEMS_COUNTER_STEP = 18
    ROWS_LG_LIMIT = 3  # By default only 3 lines on small screens

    ORDER_BY_CHOICES = [
        (ListBasePageView.DATE, __('Newest')),
        (ListBasePageView.ALPHABETICAL, __('A-Z')),
    ]

    def get_ordering(self, order_by):
        """returns the ordering fields"""
        ordering = ['-creation_date']

        if order_by == self.DATE:
            ordering = ['-creation_date']

        elif order_by == self.ALPHABETICAL:
            ordering = ['title']

        return ordering

    def get_queryset(self):
        return get_public_works_queryset()

    def get_filter_form_class(self):
        return WorkSearchForm

    def _do_filter_items(self, queryset, field, values):

        if field == 'duration':
            for value in values:
                queryset = queryset.filter(duration__lte=value)

        if field == 'instruments':
            queryset = queryset.filter(instruments__in=values)

        if field == 'keywords':
            queryset = queryset.filter(keywords__in=values)

        if field == 'technologies':
            queryset = queryset.filter(technologies__in=values)

        if field == 'other_artistic_medium':
            queryset = queryset.filter(other_artistic_medium__in=values)

        if field == 'with_electronics':
            if values:
                if True in values and False in values:
                    pass
                elif True in values:
                    queryset = queryset.filter(with_electronics=True)
                elif False in values:
                    queryset = queryset.filter(with_electronics=False)

        return queryset


class WorkView(DetailView):
    """View an existing work"""
    template_name = 'works/view_work.html'
    default_back_url = reverse_lazy('works:works_list')
    work = None

    def dispatch(self, request, *args, **kwargs):
        self.work = get_object_or_404(Work, id=self.kwargs['id'])
        if self.work.visibility != Work.VISIBILITY_PUBLIC:
            if self.request.user.is_anonymous:
                return redirect_to_login(self.request)
            if not self.work.is_user_allowed(self.request.user):
                raise PermissionDenied
        return super(WorkView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(WorkView, self).get_context_data(**kwargs)

        in_bookmarks = False
        if not self.request.user.is_anonymous:
            # update the number of views.
            # Only for members in order to avoid google bots to generate fake views
            work_views = WorkViews.objects.get_or_create(work=self.work)[0]
            work_views.count += 1
            work_views.save()

            # Is in bookmark?
            if WorkBookmark.objects.filter(work=self.work, member_user=self.request.user).exists():
                in_bookmarks = True

        context.update(
            {
                'work': self.work,
                'in_bookmarks': in_bookmarks,
                "search_back_url": get_back_url_for_search(self.request, "works:works_list"),
            }
        )

        return context


def create_work_feed(work):
    content_type = ContentType.objects.get_for_model(work)
    lookup = dict(
        owner=work.owner,
        content_type=content_type,
        object_id=work.id,
        is_original_post=True,
        tag=MemberPost.COMMUNITY_NEWS_POST
    )
    try:
        feed = FeedItem.objects.get(**lookup)
    except FeedItem.DoesNotExist:
        if not work.is_public():
            return   # If work is private: no need to create a new feed if it doesn't exist
        feed = FeedItem(**lookup)
        feed.feed_datetime = work.creation_date
    feed.post_text = work.title
    feed.is_deleted = not work.is_public()
    feed.save()


class AddWorkView(ProfileFormBaseView):
    """Add a new work"""
    template_name = 'works/add_work.html'
    form_class = WorkForm

    def get_form_kwargs(self):
        form_kwargs = super(AddWorkView, self).get_form_kwargs() or {}
        form_kwargs['instance'] = Work(
            owner=self.request.user,
            creation_date=datetime.now(),
            update_date=datetime.now(),
        )
        return form_kwargs

    def form_valid(self, form):
        new_work = form.save()
        messages.success(
            self.request,
            _('Your work has been created.')
        )
        if new_work.is_public():
            create_work_feed(new_work)
        return HttpResponseRedirect(reverse('works:view_work', args=[new_work.id]))


class EditWorkView(ProfileFormBaseView):
    """Edit an existing work"""
    template_name = 'works/edit_work.html'
    form_class = WorkForm
    work = None

    def get_work(self):
        if self.work is None:
            self.work = get_object_or_404(Work, id=self.kwargs['id'])
        return self.work

    def get_form_kwargs(self):
        form_kwargs = super(EditWorkView, self).get_form_kwargs() or {}
        work = self.get_work()
        if work.owner != self.request.user:
            raise PermissionDenied
        form_kwargs['instance'] = work
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(EditWorkView, self).get_context_data(**kwargs)
        context['work'] = self.get_work()
        return context

    def form_valid(self, form):
        work = form.save()
        create_work_feed(work)
        messages.success(
            self.request,
            _('Your work has been updated.')
        )
        return HttpResponseRedirect(reverse('works:view_work', args=[work.id]))


@login_required
def works_ajax_list(request):
    """for autocomplete"""
    term = request.GET.get('term')
    works = []
    if term:
        works_queryset = Work.objects.exclude(visibility=Work.VISIBILITY_PRIVATE)
        works = [
            {'id': work.id, 'name': '{0}'.format(work)}
            for work in works_queryset.filter(title__istartswith=term)[:10]
        ]
    return HttpResponse(json.dumps(works), content_type="application/json")


class WorkMediaDownloadView(PrivateDownloadView):
    """
    private download for works.
    Media files can not be accessed from file url and must pass through this verification
    """
    field_name = ''
    as_attachment = False

    def get_file(self, request, **kwargs):
        """
        get work from id and check if user can access it
        returns the score, other, audio or video of the file
        """
        work = get_object_or_404(Work, id=kwargs['id'])

        if work.visibility != Work.VISIBILITY_PUBLIC:
            if request.user.is_anonymous:
                raise PermissionDenied

            if not work.is_user_allowed(request.user):
                raise PermissionDenied

        return getattr(work, self.field_name)


class CopyWorkToMediaView(LoginRequiredPopupFormView):
    template_name = 'works/copy_work_to_media.html'
    form_class = ConfirmForm

    def get_context_data(self, **kwargs):
        context = super(CopyWorkToMediaView, self).get_context_data(**kwargs)

        work = get_object_or_404(Work, id=self.kwargs['id'])
        if work.owner != self.request.user:
            raise PermissionDenied

        context['work'] = work
        return context

    def form_valid(self, form):
        work = get_object_or_404(Work, id=self.kwargs['id'])
        if work.owner != self.request.user:
            raise PermissionDenied
        copy_work_as_personal_element(work)
        return HttpResponseRedirect(reverse('web:show_medias'))


class WorkVideoView(PopupFormView):
    template_name = 'works/popup_work_video.html'
    form_class = ConfirmForm

    def get_form_kwargs(self):
        form_kwargs = super(WorkVideoView, self).get_form_kwargs()
        return form_kwargs

    def get(self, request, *args, **kwargs):
        work = get_object_or_404(Work, id=self.kwargs['work_id'])
        if not work.video:
            raise Http404
        if work.visibility != Work.VISIBILITY_PUBLIC:
            if request.user.is_anonymous:
                return HttpResponseRedirect(reverse('login') + '?next={0}'.format(request.path))
            if not work.is_user_allowed(request.user):
                raise PermissionDenied
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(WorkVideoView, self).get_context_data(**kwargs)
        work = get_object_or_404(Work, id=self.kwargs['work_id'])
        context['work'] = work
        return context

    def form_valid(self, form):
        redirect_url = self.request.META.get('HTTP_REFERER')
        return HttpResponseRedirect(redirect_url)


class WorksAPIView(RetrieveAPIView):
    """View received messages"""
    queryset = Work.objects.all()
    serializer_class = WorkSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """returns messages of current user"""
        queryset = get_works_queryset()
        return queryset


class DeleteWorkView(LoginRequiredPopupFormView):
    template_name = 'works/popup_delete_work.html'
    form_class = ConfirmForm

    def get_context_data(self, **kwargs):
        context = super(DeleteWorkView, self).get_context_data(**kwargs)
        work = get_object_or_404(Work, id=self.kwargs['id'])
        if work.owner != self.request.user:
            raise PermissionDenied
        context['work'] = work
        return context

    def form_valid(self, form):
        work = get_object_or_404(Work, id=self.kwargs['id'])
        if work.owner != self.request.user:
            raise PermissionDenied
        messages.success(self.request, _('"{0}" has been deleted').format(work.title))
        work.delete()
        return HttpResponseRedirect(reverse('profiles:personal_space'))


class WorkScorePopupView(PopupView):
    template_name = 'works/popup_work_score.html'

    def get(self, request, *args, **kwargs):
        work = get_object_or_404(Work, id=self.kwargs['id'])
        if not work.score:
            raise Http404
        if work.visibility != Work.VISIBILITY_PUBLIC:
            if request.user.is_anonymous:
                return HttpResponseRedirect(reverse('login') + '?next={0}'.format(request.path))
            if not work.is_user_allowed(request.user):
                raise PermissionDenied
        return super().get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(WorkScorePopupView, self).get_context_data(**kwargs)
        context['work'] = get_object_or_404(Work, id=self.kwargs['id'])
        return context
