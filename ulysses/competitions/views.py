# -*- coding: utf-8 -*-

from datetime import datetime
from decimal import Decimal
import json
import os.path
import xlwt

from django.conf import settings
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.core.mail import mail_managers
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView

from ulysses.generic.directories import upload_file, CALLS_DIRECTORY
from ulysses.generic.views import XlsExportView
from ulysses.profiles.utils import get_profile
from ulysses.utils import upload_helper_ex
from ulysses.web.models import AnnounceCallImage

from .forms import CallRequestForm
from .models import (
    get_candidate_folder, Candidate, JuryMemberGroup, CandidateGroup, CompetitionManager, StaticContent,
    CallRequest, Competition, Evaluation, EvaluationNote, EvaluationField
)


@csrf_exempt
def upload_from_superadmin(request, candidate_id):
    # target_dir = get_candidate_admin_folder(Candidate.objects.get(id=candidate_id))*
    target_dir = get_candidate_folder(Candidate.objects.get(id=candidate_id))

    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    print(absolute_target_dir)
    return upload_helper_ex(request, absolute_target_dir)

@csrf_exempt
def upload_call_file(request, user_id=0):
    return upload_file(request, CALLS_DIRECTORY, user_id)


@csrf_exempt
def remove_group(request):
    group_id = request.POST["group_id"]
    model_name = request.POST.get("model", "")

    if not request.user.is_authenticated:
        raise PermissionDenied

    try:
        model_class = {
            'jury': JuryMemberGroup,
            'candidate': CandidateGroup,
        }[model_name]
    except KeyError:
        raise Http404

    try:
        group = model_class.objects.get(id=group_id)
    except model_class.DoesNotExist:
        raise Http404

    if not request.user.is_superuser:
        try:
            manager = CompetitionManager.objects.get(user=request.user)
            if manager not in group.competition.managed_by.all():
                raise PermissionDenied
        except CompetitionManager.DoesNotExist:
            raise PermissionDenied

    group.delete()

    data = {
        'success': True,
    }
    return HttpResponse(json.dumps(data), content_type="application/json")


class OrganizeCallView(FormView):
    template_name = 'competitions/organize_call.html'
    form_class = CallRequestForm

    def get_context_data(self, **kwargs):
        context = super(OrganizeCallView, self).get_context_data(**kwargs)
        context['post_url'] = reverse('competitions:organize_call')
        return context

    def get_form_kwargs(self):
        form_kwargs = super(OrganizeCallView, self).get_form_kwargs()
        form_kwargs['instance'] = CallRequest(
            request_datetime=datetime.now(),
        )
        return form_kwargs

    def notify_managers(self, call):
        """sends an email to platform admins to inform about a new call request"""
        message = render_to_string(
            'emails/organize_call_notification_message.txt',
            context={
                'site': Site.objects.get_current(),
                'obj': call,
            }
        )
        mail_managers(
            subject=_('Organize a call request'), message=message,
        )

    def form_valid(self, form):
        """The form is valid, send an email to super-admin"""
        call = form.save()
        self.notify_managers(call)
        messages.success(
            self.request,
            _(
                'Thank you for your interest in the ULYSSES Platform.'
                ' Your request has been sent to our team. We will get back to you shortly.'
            )
        )
        return HttpResponseRedirect(reverse("competitions:show_call_guidelines"))


def show_call_guidelines(request):
    content, is_new = StaticContent.objects.get_or_create(key="call_guidelines")
    if is_new:
        content.description = _('Call guidelines')
        content.save()
    context = {
        "call_guidelines": content,
    }
    try:
        context['announce_call_image'] = AnnounceCallImage.objects.get(is_active=True)
    except (AnnounceCallImage.DoesNotExist, AnnounceCallImage.MultipleObjectsReturned):
        context['announce_call_image'] = None

    protocol = 'http://'
    if request and request.is_secure():
        protocol = 'https://'
    context.update({'site': Site.objects.get_current(), 'protocol': protocol})

    return render(request, 'competitions/call_guidelines.html', context)


class CompetitionXlsExportView(XlsExportView):
    _competition = None

    def get_competition(self):
        if self._competition is None:
            self._competition = get_object_or_404(Competition, id=self.kwargs.get('competition_id'))
        return self._competition

    def get_doc_name(self):
        competition = self.get_competition()
        return "Export-{0}.xls".format(competition.url)

    def check_permission(self):
        user = self.request.user
        if not user.is_authenticated:
            return False
        if user.is_superuser:
            return True
        competition = self.get_competition()
        try:
            manager = CompetitionManager.objects.get(user=user)
            return competition.managed_by.filter(id=manager.id).exists()
        except CompetitionManager.DoesNotExist:
            return False

    def do_fill_workbook(self, workbook):
        """fill the workbook : called in parent class"""

        header_style = xlwt.XFStyle()
        header_style.pattern = xlwt.Pattern()
        header_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        header_style.pattern.pattern_fore_colour = 22

        competition = self.get_competition()

        candidate_fields = [
            ('last_name', _('Last name')),
            ('first_name', _('First name')),
            ('contact', _('Contact')),
            ('birth_date', _('Birth date')),
            ('gender', _('Pronoun')),
            ('citizenship', _('Citizenship')),
            ('address1', _('Address 1')),
            ('address2', _('Address 2')),
            ('zipcode', _('Zip code')),
            ('city', _('City')),
            ('country', _('Country')),
            ('phone1', _('Phone 1')),
            ('phone2', _('Phone 2')),
            ('website', _('Website')),
        ]
        sheet = workbook.add_sheet(self.get_safe_sheet_name(_("Candidates")))
        mandatory_fields = [
            field.name for field in competition.profile_mandatory_fields.all()
        ]
        if 'last_name' in mandatory_fields:
            mandatory_fields.append('contact')
        cols_index = 0
        self.write_cell(sheet, 0, cols_index, _('Email'), style=header_style)
        cols_index += 1
        for field, header in candidate_fields:
            if field in mandatory_fields:
                self.write_cell(sheet, 0, cols_index, header, style=header_style)
                cols_index += 1
        self.write_cell(sheet, 0, cols_index, _('Is valid'), style=header_style)
        cols_index += 1
        self.write_cell(sheet, 0, cols_index, _('Notes'), style=header_style)
        cols_index += 1

        candidates = Candidate.objects.filter(competition=competition).order_by('composer__user__last_name')
        application_form = competition.get_application_form_instance(read_only=True)

        jury_rows = {}
        candidates_cols = {}

        form_field_names = []
        step_sheets = {}
        step_sheets_cols = {}
        step_sheets_rows = {}
        for step in competition.steps():
            step_sheets[step.name] = workbook.add_sheet(self.get_safe_sheet_name(_("Step {0}").format(step.name)))
            step_sheets_cols[step.name] = 0
            step_sheets_rows[step.name] = 0

        for field in application_form:
            field_definition = application_form.get_field_definition_from_name(field.name, allow_missing=True)
            if field_definition is not None:
                form_field_names.append(field.name)
                self.write_cell(sheet, 0, cols_index, field.label, style=header_style)
                cols_index += 1

        for index, candidate in enumerate(candidates):
            excel_data = application_form.get_excel_data_from_candidate(application_form, candidate)
            line_number = index + 1
            profile = get_profile(candidate.composer.user)
            col_index = 0
            self.write_cell(sheet, line_number, col_index, profile.user.email)
            col_index += 1
            if profile.is_individual():
                if 'last_name' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.user.last_name)
                    col_index += 1
                if 'first_name' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.user.first_name)
                    col_index += 1
                if 'contact' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, '')
                    col_index += 1
                if 'birth_date' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.birth_date)
                    col_index += 1
                if 'gender' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.gender.name if profile.gender else '')
                    col_index += 1
                if 'citizenship' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.citizenship_str())
                    col_index += 1
            else:
                if 'last_name' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.name)
                    col_index += 1
                if 'first_name' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, '')
                    col_index += 1
                if 'contact' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, profile.contact_name)
                    col_index += 1
                if 'birth_date' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, '')
                    col_index += 1
                if 'gender' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, _('Organization'))
                    col_index += 1
                if 'citizenship' in mandatory_fields:
                    self.write_cell(sheet, line_number, col_index, '')
                    col_index += 1
            if 'address1' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.address1)
                col_index += 1
            if 'address2' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.address2)
                col_index += 1
            if 'zipcode' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.zipcode)
                col_index += 1
            if 'city' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.city)
                col_index += 1
            if 'country' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.country.name if profile.country else '')
                col_index += 1
            if 'phone1' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.phone1)
                col_index += 1
            if 'phone2' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.phone2)
                col_index += 1
            if 'website' in mandatory_fields:
                self.write_cell(sheet, line_number, col_index, profile.website)
                col_index += 1

            self.write_cell(sheet, line_number, col_index, _('Yes') if candidate.is_valid else _('No'))
            col_index += 1

            self.write_cell(sheet, line_number, col_index, candidate.notes)
            col_index += 1

            for sub_index, field_name in enumerate(form_field_names):
                col_number = col_index + sub_index
                value = excel_data.get(field_name, '')
                self.write_cell(sheet, line_number, col_number, value)

            for step in competition.steps():
                step_sheet = step_sheets.get(step.name)

                step_criteria = EvaluationField.objects.filter(competition_step=step).order_by('order_index')

                for evaluation in Evaluation.objects.filter(candidate=candidate, competition_step=step):
                    notes = EvaluationNote.objects.filter(evaluation=evaluation)

                    candidate_key = "{0}_{1}".format(step.id, candidate.id)
                    candidate_col = candidates_cols.get(candidate_key)
                    if candidate_col is None:
                        cols_count = step_sheets_cols.get(step.name) + 1
                        step_sheets_cols[step.name] = cols_count
                        candidate_col = cols_count
                        self.write_cell(
                            step_sheet, 0, candidate_col, '{0}'.format(candidate), style=header_style
                        )
                        candidates_cols[candidate_key] = candidate_col

                    jury_key = "{0}_{1}".format(step.id, evaluation.jury_member.id)
                    jury_row = jury_rows.get(jury_key)
                    is_new_jury = False
                    if jury_row is None:
                        is_new_jury = True
                        jury_row = step_sheets_rows.get(step.name) + 1
                        self.write_cell(
                            step_sheet, jury_row, 0, '{0}'.format(evaluation.jury_member), style=header_style
                        )
                        jury_rows[jury_key] = jury_row

                    values = [note.value for note in notes if note.evaluation_field and note.evaluation_field.is_note]
                    numeric_values = [Decimal(value) for value in values if value.isnumeric()]
                    if numeric_values:
                        average = sum(numeric_values) / len(numeric_values)
                    else:
                        average = ''

                    self.write_cell(step_sheet, jury_row, candidate_col, '', style=header_style)
                    criteria_count = len(step_criteria)
                    for criteria_index, criteria in enumerate(step_criteria):
                        cell_label = criteria.key
                        try:
                            note = EvaluationNote.objects.get(evaluation=evaluation, key=criteria.key)
                            cell_value = note.value
                        except EvaluationNote.DoesNotExist:
                            cell_value = "empty"
                        if is_new_jury:
                            self.write_cell(step_sheet, jury_row + criteria_index + 1, 0, cell_label)
                        self.write_cell(step_sheet, jury_row + criteria_index + 1, candidate_col, cell_value)
                    if is_new_jury:
                        self.write_cell(step_sheet, jury_row + criteria_count + 1, 0, "average", style=header_style)
                        self.write_cell(step_sheet, jury_row + criteria_count + 2, 0, "comments", style=header_style)
                        step_sheets_rows[step.name] = jury_row + criteria_count + 2

                    self.write_cell(step_sheet, jury_row + criteria_count + 1, candidate_col, average)
                    self.write_cell(step_sheet, jury_row + criteria_count + 2, candidate_col, evaluation.comments)
