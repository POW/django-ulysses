# -*- coding: utf-8 -*-

import codecs
import sys

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from ulysses.composers.models import is_composer
from ulysses.competitions.models import is_jury_member


class TerminalPrinter(object):
    last_print_len = 0

    def print_in_place(self, text):
        if self.last_print_len:
            sys.stdout.write("\033[{0}D".format(self.last_print_len))
            sys.stdout.write("\033[K")
        self.last_print_len = len(text)
        sys.stdout.write(text)
        sys.stdout.flush()

    def __del__(self):
        print("")


class Command(BaseCommand):
    """
    print a file with info about duplicated user emails
    """
    help = "print a file with info about duplicated user emails"
    
    def handle(self, *args, **options):

        printer = TerminalPrinter()

        with codecs.open('duplicated-emails.txt', 'w', 'utf-8') as out_file:

            emails = User.objects.exclude(email='').values('email').distinct()

            total = emails.count()

            printer.print_in_place("0 / {0}".format(total))

            for index, data in enumerate(emails):
                index_1 = index + 1
                if index_1 % 100 == 0:
                    printer.print_in_place("{0} / {1}".format(index_1, total))
                email = data['email']
                email_users = User.objects.filter(email=email)
                if email_users.count() > 1:
                    out_file.write(email + ": \n")
                    for user in email_users:
                        out_file.write(user.username + ": ")
                        flags = []
                        if is_composer(user):
                            flags.append('compositeur')
                        if is_jury_member(user):
                            flags.append('jury')
                        if user.is_staff:
                            flags.append('admin')
                        if user.is_superuser:
                            flags.append('super-admin')
                        out_file.write(', '.join(flags) + "\n")

                    out_file.write("\n")
                    out_file.flush()

            printer.print_in_place("{0} / {0}".format(total))
