from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User
from ulysses.composers.models import Composer, Document, Media
from ulysses.competitions.models import CandidateDocument, CandidateMedia
from ulysses.competitions.models import get_user_personal_folder, get_candidate_folder
import os

PROD_MEDIA_ROOT = "https://www.ulysses_network.eu/upload/"

def import_file(relative_url,target_dir):
    source_url = "%s%s" % (PROD_MEDIA_ROOT,relative_url)
    cmd = "wget -P %s %s" % (target_dir,source_url)
    os.system(cmd)

class Command(BaseCommand):
    """ Import all composer files from production server (for test purposes) """
    help = "Import all composer files from production server"

    def handle(self, *args, **options):
        if not args or len(args) != 1:
            raise CommandError("Syntax : import_composer_file username")
        username = args[0]
        composer = Composer.objects.get(user__username=username)
        # Personal space
        personal_dir = get_user_personal_folder(composer.user.username)
        for document in Document.objects.filter(composer=composer):
            import_file(document.file,personal_dir)
        for media in Media.objects.filter(composer=composer):
            if media.score:
                import_file(media.score,personal_dir)
            if media.audio:
                import_file(media.audio,personal_dir)
            if media.video:
                import_file(media.video,personal_dir)
        # Application history
        for document in CandidateDocument.objects.filter(candidate__composer=composer):
            candidate_dir = get_candidate_folder(document.candidate)
            import_file(document.file,candidate_dir )
        for media in CandidateMedia.objects.filter(candidate__composer=composer):
            candidate_dir = get_candidate_folder(document.candidate)
            if media.score:
                import_file(media.score,candidate_dir)
            if media.audio:
                import_file(media.audio,candidate_dir)
            if media.video:
                import_file(media.video,candidate_dir)
