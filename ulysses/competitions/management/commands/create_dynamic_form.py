import sys
from optparse import make_option
from django.core.management.base import BaseCommand, CommandError

from ulysses.competitions.models import Competition, ApplicationFormFieldSetDefinition
from ulysses.competitions.models import ApplicationFormFieldDefinition, DynamicApplicationForm

class Command(BaseCommand):
    """ Creates a dynamic form for a competition (based on existing concrete application form) """
    help = "Creates a dynamic form for a competition (based on existing concrete application form)"
    option_list = BaseCommand.option_list + (
        make_option('--all',
            action='store_true',
            dest='all',
            default=False,
            help='Create dynamic forms for all existing competitions'),
        )

    def handle(self, *args, **options):

        all_competitions = options["all"]
        if not all_competitions:
            if not args or len(args)!=1:
                raise CommandError("Syntax : create_dynamic_form [<competition_id>] or create_dynamic_form --all")
            competition_id = args[0]
            competitions = Competition.objects.filter(id=competition_id)
        else:
            competitions = Competition.objects.all().exclude(application_form_class__exact='')

        for competition in competitions:
            # Ensure competition has a concrete form class
            if not competition.application_form_class:
                raise RuntimeError("Competition #%s has not concrete application form class" % competition.pk)
            # Create a dynamic application form (with competition name).
            # NOTA : Existing dynamic application form with the same name is DELETED
            DynamicApplicationForm.objects.filter(name=competition.title).delete()
            dynamic_application_form = DynamicApplicationForm.objects.create(name=competition.title)
            # Load concrete form class
            module_name = "ulysses.competitions.forms"
            __import__(module_name)
            ApplicationForm = getattr(sys.modules[module_name], competition.application_form_class)
            application_form_instance = ApplicationForm(competition=competition)
            # Create existing fieldset & fields, if any
            ApplicationFormFieldDefinition.objects.filter(parent__parent=dynamic_application_form).delete()
            ApplicationFormFieldSetDefinition.objects.filter(parent=dynamic_application_form).delete()
            # Create application form fieldsets & fields
            fieldset_order_index = 1
            for fieldset in application_form_instance.fieldsets:
                fieldset_definition = ApplicationFormFieldSetDefinition.objects.create(
                    parent=dynamic_application_form,
                    name=fieldset.name,
                    legend=fieldset.legend,
                    description=fieldset.description,
                    classes=fieldset.classes,
                    order=fieldset_order_index)
                # Create fields
                field_order_index = 1
                for field in fieldset:
                    if not field.name.endswith("_hidden"):
                        field_definition = application_form_instance.get_field_definition_from_name(field.name)
                        data = field_definition.get_data()
                        # Replace empty labels by message
                        if "label" in data and not data["label"]:
                            data["label"] = "(empty)"
                        ApplicationFormFieldDefinition.objects.create(
                            parent=fieldset_definition,
                            kind = field_definition.get_kind(),
                            order=field_order_index,
                            **data)
                    field_order_index += 1

                fieldset_order_index += 1

