from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

class Command(BaseCommand):
    """
    Update all to user emails (for test purposes)
    Source email : first_name.last_name@original_domain.com
    Target email : first_name.last_name@target_domain.com
    """
    help = "Update all user emails with target domain"    
    
    def handle(self, *args, **options):        
        if not args or len(args)!=1:
            raise CommandError("Syntax : update_emails target_domain")        
        target_domain = args[0]
        
        confirm = input("""
This will override email addresses for all users with target domain '%s'. 
Are you sure you want to do this?

Type 'yes' to continue, or 'no' to cancel: """
% target_domain)
        if confirm != 'yes':
            raise CommandError("Operation cancelled.")
                   
        cpt = 0
        for user in User.objects.all():
            if user.email:
                source_email = user.email
                tokens = source_email.split('@')
                target_email = "%s@%s" % (tokens[0],target_domain)
                user.email = target_email
                cpt += 1
                user.save()
                #print "User '%s' : updated email from %s to %s" % (user.username,source_email,target_email)
        #print "%s emails have been updated" % cpt