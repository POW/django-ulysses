# -*- coding: utf-8 -*-

from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext as _

from ulysses.competitions import get_admin_site
from ulysses.competitions.models import CompetitionStep, CandidateGroup, Candidate, JuryMemberGroup, JuryMember
from ulysses.competitions.models import CompetitionAttributeValue


class CompetitionStepFilter(SimpleListFilter):
    title = _("competition step")
    parameter_name = 'steps'
    
    def lookups(self, request, model_admin):        
        admin_site = get_admin_site()
        active_competition = admin_site.get_active_competition(request)
        active_steps = CompetitionStep.objects.filter(competition=active_competition)
        results = ()
        for active_step in active_steps:
            results += (("%s" % active_step.id, active_step.name),)
        results += (("none", _("(None)")), )
        return results

    def queryset(self, request, queryset):    
        if self.value():                        
            if self.value()=="none":
                return queryset.filter(steps__isnull=True)
            else:                
                return queryset.filter(steps__id__exact=self.value())                


class CandidateGroupFilter(SimpleListFilter):
    title = _("groups")
    parameter_name = 'groups'
    
    def lookups(self, request, model_admin):
        admin_site = get_admin_site()
        active_competition = admin_site.get_active_competition(request)
        groups = CandidateGroup.objects.filter(competition=active_competition)        
        results = ()
        for group in groups:
            results += (("%s" % group.id, group.name),)
        results += (("none", _("(None)")), )
        return results
    
    def queryset(self, request, queryset):    
        if self.value():                        
            if self.value() == "none":
                return queryset.filter(groups__isnull=True)
            else:                
                return queryset.filter(groups__id__exact=self.value())                


class JuryMemberGroupFilter(SimpleListFilter):
    title = _("group")
    parameter_name = 'groups'
    
    def lookups(self, request, model_admin):
        admin_site = get_admin_site()
        active_competition = admin_site.get_active_competition(request)
        groups = JuryMemberGroup.objects.filter(competition=active_competition)        
        results = ()
        for group in groups:
            results += (("%s" % group.id, group.name),)
        results += (("none", _("(None)")),)
        return results
    
    def queryset(self, request, queryset):    
        if self.value():                        
            if self.value() == "none":
                return queryset.filter(groups__isnull=True)
            else:                
                return queryset.filter(groups__id__exact=self.value())         


class JuryMemberCompetitionFilter(SimpleListFilter):
    title = _("competition")
    parameter_name = "competition"
    
    def lookups(self, request, model_admin):
        admin_site = get_admin_site()
        active_competition = admin_site.get_active_competition(request)
        if active_competition:
            results = (("%s" % active_competition.id, _("Active competition")),)
            return results
        return []
    
    def queryset(self, request, queryset):    
        if self.value():                                    
            return queryset.filter(competitions__id__exact=self.value())                


def get_competition_attribute_filter_class_helper(attribute_key,filter_title):
            
    class _CompetitionAttributeFilter(SimpleListFilter):
        title = filter_title
        parameter_name = "attributes"
        
        def lookups(self, request, model_admin):
            admin_site = get_admin_site()
            active_competition = admin_site.get_active_competition(request)
            attributes = CompetitionAttributeValue.objects.filter(attribute__key=attribute_key,attribute__competition__url=active_competition.url)
            results = ()
            for attribute in attributes:
                results += (("%s" % attribute.id, attribute.acronym),)
            results += (("none", _("(None)")),)
            return results
        
        def queryset(self, request, queryset):    
            if self.value():                        
                if self.value() == "none":
                    return queryset.filter(attributes__isnull=True)
                else:                
                    return queryset.filter(attributes__id__exact=self.value())         
            
    return _CompetitionAttributeFilter


def get_competition_attribute_filter_class(code):
    """
    Gets a competition specific filter class, used to filter by
    competition attribute. To be generalized in a future refactoring
    """
    if code == "residency":
        attribute_key = "topic_areas"
        filter_title = _("topic area")        
    elif code == "acanthes":
        attribute_key = "workshops"
        filter_title = _("workshop")
    else:
        raise RuntimeError("Unhandled code")
    return get_competition_attribute_filter_class_helper(attribute_key,filter_title)
