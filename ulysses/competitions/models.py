# -*- coding: utf-8 -*-

from datetime import datetime, timedelta
import pickle
import shutil
import os
import os.path
import re
import sys
from bs4 import BeautifulSoup

from django import forms as django_forms  # keep this : dynamically imported
from django.core.exceptions import ValidationError
from django.core.validators import RegexValidator, URLValidator, EmailValidator
from django.conf import settings
from django.contrib.auth.models import User, Group, Permission
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import NON_FIELD_ERRORS
from django.core.validators import MaxLengthValidator, MinLengthValidator
from django.db import models
from django.db.models import Max
from django.db.models.signals import pre_delete
from django.db.models.signals import pre_save, post_save, post_delete, m2m_changed
from django.utils.translation import ugettext as _
from django.urls import reverse, NoReverseMatch
from django.utils.safestring import mark_safe

from adminsortable.models import Sortable
import floppyforms.__future__ as forms
from sorl.thumbnail import get_thumbnail

from ulysses.competitions import get_admin_site
from ulysses.composers.models import (
    Composer, MediaBase, DocumentBase, BiographicElementBase, DocumentExtraFileBase, BiographicElement, Document,
    Media, DocumentExtraFile
)
from ulysses.generic.directories import DirectoryName, CALLS_DIRECTORY
from ulysses.generic.forms import BetterForm, BootstrapFormMixin
from ulysses.partners.models import Partner
from ulysses.profiles.utils import get_profile
from ulysses.reference.models import CompetitionCategory
from ulysses.utils import (
    make_string_storable_in_dict, get_user_personal_folder, dehtml, get_youtube_code, limited_html
)


def get_thumbnail_safe(*args, **kwargs):
    try:
        return get_thumbnail(*args, **kwargs).url
    except:
        pass


class FieldNameValidator(RegexValidator):

    def __init__(self):
        super(FieldNameValidator, self).__init__(
            "^[\w\d_]+$",
            _("This field cannot contains spaces.")
        )


def is_jury_member(user):
    """
    Indicates whether the current user is a jury member
    """
    return JuryMember.objects.filter(user=user).exists()


def get_nav_button(request, url, label, children=None):
    is_selected = request.path.startswith(url)
    result = {'url': url, 'label': label, 'is_selected': is_selected}
    if children:
        result["children"] = children
    return result


class ApplicationDraft(models.Model):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)
    competition = models.ForeignKey("Competition", on_delete=models.CASCADE)
    creation_date = models.DateTimeField(auto_now_add=True)
    data = models.TextField()

    def __str__(self):
        return "Application draft #%s" % self.id

    class Meta:
        unique_together = (('composer', 'competition'),)


class CompetitionStatus(models.Model):
    name = models.CharField(max_length=200)
    order_index = models.IntegerField(blank=True, null=True)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['order_index', ]
        verbose_name = _("competition status")
        verbose_name_plural = _("competition status")


class StaticContent(models.Model):
    key = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=200)
    text = models.TextField()
    published = models.BooleanField(default=True)
    name = models.CharField(max_length=100, default="", blank=True)

    class Meta:
        ordering = ['key', ]
        verbose_name = _("platform static content")
        verbose_name_plural = _("platform static contents")

    @property
    def raw_text(self):
        return dehtml(self.text)

    @property
    def title(self):
        return self.name or self.key

    def get_absolute_url(self):
        views_map = {
            'call_guidelines': 'competitions:show_call_guidelines',
            'contact': 'web:contact_us',
            'legal_mentions': 'web:legal_mentions',
            'privacy_policy': 'web:privacy_policy',
            'about': 'web:about',
        }
        view_name = views_map.get(self.key, '')
        if view_name:
            try:
                return reverse(view_name)
            except NoReverseMatch:
                pass
        return ""


def add_clean_method_to_form_class(form_class,clean_method_name):
    """
    Dynamically adds a clean method to a form class
    Nota : this method should be defined in competitions.forms
    """
    mod = __import__("ulysses.competitions.forms", fromlist=[clean_method_name])
    clean_method = getattr(mod, clean_method_name)
    setattr(form_class, "clean", clean_method)


class ApplicationFieldValidator(object):
    """This function-object is in charge of making sure that a value is not greater than a maximum_length"""
    def __init__(self, form, field):
        self.form = form
        self.field = field

    def __call__(self):
        """object is called like a clean_field method"""
        value = self.form.cleaned_data[self.field.name]

        if self.field.kind.name in ('ApplicationHtmlElementField', "BiographicElementField"):
            soup = BeautifulSoup(value, "html.parser")
            raw_value = soup.text
        else:
            raw_value = value

        if self.field.validator == 'url':
            url_validator = URLValidator()
            try:
                url_validator(raw_value)
            except ValidationError:
                raise ValidationError("{0} is not a valid URL".format(raw_value))

        if self.field.validator == 'email':
            email_validator = EmailValidator()
            try:
                email_validator(raw_value)
            except ValidationError:
                raise ValidationError("{0} is not a valid email".format(raw_value))

        if self.field.maximum_length:
            if len(raw_value) > self.field.maximum_length:
                raise ValidationError("The length is greater than {0} characters".format(self.field.maximum_length))
        return value


class ProfileMandatoryFields(models.Model):
    name = models.CharField(max_length=50, verbose_name=_('name'))

    class Meta:
        ordering = ['name', 'id', ]
        verbose_name = _("profile mandatory field")
        verbose_name_plural = _("profile mandatory fields")

    def __str__(self):
        return self.name


class Competition(models.Model):
    url = models.CharField(max_length=200, unique=True)
    title = models.CharField(max_length=200)
    email = models.EmailField(max_length=200, blank=True, null=True)
    subtitle = models.CharField(verbose_name=_("subtitle"), max_length=200, blank=True, default='')
    presentation = models.TextField(verbose_name=_("summary"), help_text=_("competition summary"))
    application_email_header = models.TextField(
        verbose_name=_("application email header"),
        help_text=_("text to be inserted in the header of application confirmation e-mail sent to candidate"),
        max_length=4000, blank=True, null=True
    )
    jury_guidelines = models.TextField(
        verbose_name=_("jury guidelines"), blank=True, null=True,
        help_text="specific jury guidelines for this competition"
    )
    order_index = models.IntegerField(
        blank=True, null=True, help_text="use this field to specify competition display order on the website"
    )
    organized_by = models.ForeignKey(
        Partner, verbose_name=_("organized by"), related_name="organized_by", on_delete=models.CASCADE
    )
    partners = models.ManyToManyField(
        Partner, verbose_name=_("partners"), help_text=_("partners"), related_name="partners", blank=True
    )
    managed_by = models.ManyToManyField(
        "CompetitionManager", verbose_name=_("managers"), help_text=_("competition managers"), blank=True
    )
    image = models.ImageField(
        blank=True, null=True, upload_to="competitions",
        help_text="Panoramic image associated to this competition (viewable on the Call detail page)."
                  "Recommended format: 1440 x 240px"
    )
    small_image = models.ImageField(
        blank=True, null=True, upload_to="competitions",
        help_text="Thumbnail image associated to this competition (viewable on the Calls list). "
                  "Recommended format: 220 x 190px"
    )
    publication_date = models.DateTimeField(
        verbose_name=_('Publish Date'),
        help_text=_('sets the date and time of the call publication on the ULYSSES website')
    )
    opening_date = models.DateTimeField(
        verbose_name=_('Submission Period Starting Date'),
        help_text=_('sets the date and time when the APPLY button appears on the competition page')
    )
    closing_date = models.DateTimeField(
        verbose_name=_('Submission Period Closing Date'),
        help_text=_('sets the date and time when the APPLY button disappears from the competition page')
    )
    result_date = models.DateTimeField(
        verbose_name=_('Results Date'),
        help_text=_('provided for information purposes only')
    )
    unpublish_date = models.DateTimeField(
        verbose_name=_('Unpublish Date'),
        help_text=_('sets the date and time when the call disappears from the ULYSSES website'),
        blank=True, default=None, null=True
    )
    category = models.ForeignKey(
        CompetitionCategory, verbose_name=_('category'), default=None, blank=True, null=True,
        help_text=_('Users, interested by this category, will received an email when the competition is published'),
        on_delete=models.SET_NULL
    )
    status = models.ForeignKey(CompetitionStatus, on_delete=models.CASCADE)
    application_form_class = models.CharField(verbose_name=_("application form class"), max_length=200, blank=True)
    use_dynamic_form = models.BooleanField(default=False)
    dynamic_application_form = models.ForeignKey(
        "DynamicApplicationForm", null=True, blank=True, on_delete=models.CASCADE
    )
    display_candidate_name = models.BooleanField(default=True)
    archived = models.BooleanField(default=False)
    allow_access_to_jury_history = models.BooleanField(default=False)
    profile_mandatory_fields = models.ManyToManyField(
        ProfileMandatoryFields, blank=True,
        help_text=_('a user can not apply if these fields are not set on his profile')
    )
    show_on_homepage = models.BooleanField(default=False, verbose_name=_('show on homepage'))
    is_external_call = models.BooleanField(default=False)
    external_call_url = models.URLField(blank=True, default="")
    closed_message = models.TextField(
        blank=True, default='', verbose_name=_('closed message'),
        help_text=_('This message is displayed when the competition is closed')
    )
    publication_notified = models.BooleanField(
        default=False,
        help_text=_('If email has already been sent to inform some users that the competition has been published ')
    )
    entry_fee = models.CharField(
        max_length=200, verbose_name=_('Entry or course fee'), default='', blank=True, help_text=_('Max 200 characters')
    )
    restrictions = models.CharField(
        max_length=200, verbose_name=_('Restrictions'), default='', blank=True, help_text=_('Max 200 characters')
    )

    def get_status(self):
        now = datetime.now()
        if now < self.publication_date:
            status = CompetitionStatus.objects.get_or_create(name="new")[0]
        elif now < self.opening_date:
            status = CompetitionStatus.objects.get_or_create(name="published")[0]
        elif now < self.closing_date:
            status = CompetitionStatus.objects.get_or_create(name="open")[0]
        elif now < self.unpublish_date:
            status = CompetitionStatus.objects.get_or_create(name="closed")[0]
        else:
            status = CompetitionStatus.objects.get_or_create(name="archived")[0]
        return status

    @property
    def name(self):
        return self.title

    @property
    def raw_text(self):
        return dehtml(self.presentation)

    @property
    def safe_html(self):
        return limited_html(self.presentation)

    def status_text(self):
        text = '{0}'.format(self.status.name)
        if self.status.name == 'open':
            text = _('{0} from {1} to {2}').format(self.status.name, self.opening_date, self.closing_date)
        return text

    def opening_soon(self):
        if self.is_published():
            delta = self.opening_date - datetime.now()
            if delta.days >=0 and delta.days < 20:
                return True
        return False

    def closing_soon(self):
        if self.is_open():
            delta = self.closing_date - datetime.now()
            if delta.days >=0 and delta.days < 7:
                return True
        return False

    def is_closed(self):
        now = datetime.now()
        return self.closing_date <= now and self.unpublish_date > now

    def is_open(self):
        now = datetime.now()
        return self.opening_date <= now and self.closing_date > now

    def is_draft(self):
        now = datetime.now()
        return self.publication_date > now

    def is_published(self):
        now = datetime.now()
        return self.publication_date <= now and self.opening_date > now

    def is_archived(self):
        now = datetime.now()
        return self.unpublish_date <= now

    def get_absolute_url(self):
        try:
            return reverse('web:competition', args=[self.url])
        except NoReverseMatch:
            pass

    def _get_best_image(self, small):
        image = self.image
        if small and self.small_image:
            image = self.small_image
        return image

    def get_square_thumbnail(self):
        """image on homepage"""
        image = self._get_best_image(True)
        if image:
            return get_thumbnail_safe(image, "100x100", crop="center")

    def get_thumbnail(self):
        """image on homepage"""
        image = self._get_best_image(True)
        if image:
            return get_thumbnail_safe(image, "100x48", crop="center")

    def get_square_medium_image(self):
        """image on dashboard"""
        image = self._get_best_image(True)
        if image:
            return get_thumbnail_safe(image, "400x400", crop="center")

    def get_medium_image(self):
        """image on competition list"""
        image = self._get_best_image(True)
        if image:
            return get_thumbnail_safe(image, "400", crop="center")

    def get_homepage_image(self):
        """image on competition list"""
        image = self._get_best_image(True)
        if image:
            return get_thumbnail_safe(image, "400")

    def get_large_image(self):
        """image on competition detail"""
        image = self._get_best_image(False)
        if image:
            return get_thumbnail_safe(image, "900x450", crop="center")

    def image_filename(self):
        """"""
        if self.image:
            return os.path.split(self.image.name)[-1]
        return ""

    def presentation_html_summary(self):
        text = re.sub(
            r"[\r?\n]+",
            "\n",
            dehtml(self.presentation, allow_spaces=True, allow_html_chars=False)
        )
        words = text.split(" ")
        if len(words) > 100:
            return " ".join(words[:100]) + "..."
        else:
            return text

    def __str__(self):
        return self.title

    @property
    def owners(self):
        return [manager.user for manager in self.managed_by.all()]

    def save(self, *args, **kwargs):
        if not self.unpublish_date and self.closing_date:
            self.unpublish_date = self.closing_date + timedelta(days=30)
        self.status = self.get_status()
        if self.status:
            self.archived = (self.status.name == 'archived')
        else:
            self.archived = False
        return super(Competition, self).save(*args, **kwargs)

    def construct_dynamic_form_class(self, is_jury_member=False):

        from .forms import ApplicationForm
        assert self.dynamic_application_form

        # Construct field definitions
        _field_definitions = []
        for item in ApplicationFormFieldDefinition.objects.filter(parent__parent=self.dynamic_application_form):
            _field_definitions.append(item.get_form_field(competition=self))

        # Construct fieldsets
        _fieldsets = []
        _clean_methods = []

        fieldset_queryset = ApplicationFormFieldSetDefinition.objects.filter(
            parent__competition__url=self.url
        ).order_by("order")

        for fieldset in fieldset_queryset:
            fieldset_fields = []
            fields_queryset = ApplicationFormFieldDefinition.objects.filter(
                parent=fieldset
            )
            if is_jury_member:
                fields_queryset = fields_queryset.filter(is_visible_by_jury=True)

            fields_queryset = fields_queryset.order_by("order")
            for elt in fields_queryset:
                fieldset_fields.append(elt.name)
                # Add  _hidden field for each document & media
                doc_and_media_fields = [
                    "DocumentField", "MediaField", "VideoField", "AudioField", "ScoreVideoField", "ScoreAudioField",
                    "YouTubeField", "VideoYouTubeField",
                ]
                if elt.kind.name in doc_and_media_fields:
                    fieldset_fields.append("{0}_hidden".format(elt.name))
                if elt.maximum_length or elt.validator:
                    _clean_methods += [
                        ('clean_{0}'.format(elt.name), elt),
                    ]

            if fieldset_fields:
                _fieldsets.append(
                    (
                        fieldset.name,
                        {
                            'fields': fieldset_fields,
                            'legend': fieldset.legend,
                            'description': fieldset.description,
                            'classes': [fieldset.classes]
                        }
                    )
                )

        class _DynamicApplicationForm(ApplicationForm):

            def get_field_definitions(self):
                return _field_definitions

            class Meta:
                fieldsets = _fieldsets

            def __init__(self, *args, **kwargs):
                super(_DynamicApplicationForm, self).__init__(*args, **kwargs)

                for (name, field) in _clean_methods:
                    setattr(self, name, ApplicationFieldValidator(self, field))

        # Add dynamically clean method, if any
        if self.dynamic_application_form.clean_method:
            add_clean_method_to_form_class(_DynamicApplicationForm, self.dynamic_application_form.clean_method)

        return _DynamicApplicationForm

    def get_application_form_instance(self, **kwargs):
        is_jury_member = kwargs.pop('is_jury_member', False)
        read_only = kwargs.pop('read_only', False)
        if self.use_dynamic_form:
            application_form = self.construct_dynamic_form_class(is_jury_member=is_jury_member)
        else:
            module_name = "ulysses.competitions.forms"
            __import__(module_name)
            application_form = getattr(sys.modules[module_name], self.application_form_class)
        if read_only:
            application_form.preview = True
        kwargs["competition"] = self
        return application_form(**kwargs)

    def is_open(self):
        """Is the competition open for application?"""
        if self.status.name == "open":
            return True

        if self.status.name in ("draft", "new", ):
            # Draft are considered as open for test-purpose
            return True

        return False

    def get_composer_application_status(self, composer):
        # Possible values 'can_apply', 'exist_draft', 'already_applied'
        if Candidate.objects.filter(competition=self, composer=composer).count() == 1:
            return "already_applied"
        elif ApplicationDraft.objects.filter(competition=self, composer=composer).count() == 1:
            return "exist_draft"
        else:
            return "can_apply"

    def get_composer_application_candidate(self, composer):
        queryset = Candidate.objects.filter(competition=self, composer=composer)
        if queryset.count() > 0:
            return queryset[0]
        return None

    def get_composer_application_date(self, composer):
        # Possible values 'can_apply', 'exist_draft', 'already_applied'
        if Candidate.objects.filter(competition=self, composer=composer).count() == 1:
            return Candidate.objects.filter(competition=self, composer=composer)[0].application_date
        elif ApplicationDraft.objects.filter(competition=self, composer=composer).count() == 1:
            return ApplicationDraft.objects.filter(competition=self, composer=composer)[0].creation_date
        else:
            return None

    def get_unique_candidate_id(self):
        if Candidate.objects.filter(competition=self).count()>0:
            return Candidate.objects.filter(
                competition=self).aggregate(Max('id_in_competition'))["id_in_competition__max"] + 1
        else:
            return 1

    def get_menu(self, request):
        admin_site = get_admin_site()
        buttons = []
        second_level_buttons = []
        is_competition_admin = False
        if admin_site.is_competition_admin(request) or request.user.is_superuser:
            is_competition_admin = True
            buttons.append(get_nav_button(request, reverse('competition-admin:show_informations'), _("Informations")))
            buttons.append(
                get_nav_button(
                    request,
                    reverse('competition-admin:ulysses_competitions_competitionnews_changelist'),
                    _("News")
                )
            )
            buttons.append(
                get_nav_button(request, reverse('competition-admin:show_candidates'), _("Application details"))
            )
            buttons.append(
                get_nav_button(request, reverse('competition-admin:show_application_drafts'), _("Application drafts"))
            )
            buttons.append(
                get_nav_button(request, reverse('competition-admin:show_jury_members'), _("Jury members"))
            )
            # Add steps dynamically
            for step in self.steps():
                step_url = reverse('competition-admin:competition_step', args=[step.url])
                step_button = get_nav_button(request, step_url, _("'{0}' step").format(step.name))
                buttons.append(step_button)
                if step_button['is_selected']:
                    second_level_buttons.append(
                        get_nav_button(
                            request,
                            reverse('competition-admin:step_manage_allocations', args=[step.url]),
                            _("Manage jury / candidates"))
                    )
                    second_level_buttons.append(
                        get_nav_button(
                            request,
                            reverse('competition-admin:step_follow_evaluations', args=[step.url]),
                            _("Follow evaluations"))
                    )
                    second_level_buttons.append(
                        get_nav_button(
                            request,
                            reverse('competition-admin:step_follow_results', args=[step.url]),
                            _("Follow results"))
                    )
                    second_level_buttons.append(
                        get_nav_button(
                            request,
                            reverse('competition-admin:step_settings', args=[step.url]),
                            _("Settings")
                        )
                    )
        if admin_site.is_jury_member(request) or is_competition_admin:
            buttons.append(
                get_nav_button(request, reverse('competition-admin:show_evaluations'), _("Evaluations"))
            )
        menu = {}
        menu["buttons"] = buttons
        menu["second_level_buttons"] = second_level_buttons
        return menu

    def steps(self):
        return CompetitionStep.objects.filter(competition=self).order_by("order_index")

    def news(self):
        return CompetitionNews.objects.filter(competition=self, is_published=True).order_by("-date")


class CompetitionAttribute(models.Model):
    """
    A attribute attached to a competition (e.g : a list of 'workshops' for Acanthes competition)
    """
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    key = models.CharField(max_length=100)

    def __str__(self):
        return "%s : %s " % (self.competition.title, self.key)

    class Meta:
        unique_together = (('competition', 'key'),)


class CompetitionAttributeValue(models.Model):
    """
    A value for a criterium (e.g : the name a workshop for Acanthes competition)
    """
    attribute = models.ForeignKey(CompetitionAttribute, on_delete=models.CASCADE)
    # The primary key for this attribute in the competition (ex : '1')
    key = models.CharField(max_length=100)
    # An acronym for this attribute (optional), ex : "CAO"
    acronym = models.CharField(max_length=100, blank=True, null=True)
    # The value of the attribute (ex : "Computer Aided Orchestration")
    value = models.CharField(max_length=100)
    is_filter = models.BooleanField(default=False)
    hide_recommendation = models.BooleanField(default=False)
    order = models.IntegerField(default=0)

    def __str__(self):
        return "[%s] %s : %s" % (self.attribute.competition.title, self.attribute.key, self.value)

    class Meta:
        unique_together = (('attribute', 'key'),)
        ordering = ('order', 'id', )


class CompetitionManager(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)

    def __str__(self):
        return "%s %s [%s]" % (self.user.first_name, self.user.last_name, self.user.username)

    class Meta:
        verbose_name = "competition manager"
        verbose_name_plural = "competition managers"

    def full_name(self):
        full_name = ' '.join([self.user.last_name, self.user.first_name,]).strip()
        if not full_name:
            full_name = "*" + self.user.username
        return full_name


class CompletedEvaluationWidget(forms.CheckboxInput):
    template_name = "floppyforms/completed_evaluation.html"


def get_dynamic_validator(name, values_interpeted_as_empty=None):
    """ Gets a dynamic validator method for the specified field """
    if values_interpeted_as_empty is None:
        values_interpeted_as_empty = []

    def _clean_field(host):
        data = host.cleaned_data[name]
        if not data:
            raise forms.ValidationError(_("This field is required"))
        else:
            if data in values_interpeted_as_empty:
                raise forms.ValidationError(_("This field is required"))
        # Always return the cleaned data, whether you have changed it or not
        return data
    return _clean_field


class CompetitionStep(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    url = models.CharField(max_length=200)
    order_index = models.IntegerField(verbose_name=_("index"))
    is_open = models.BooleanField(
        default=False,
        verbose_name=_("is open to evaluations"),
        help_text=_(
            "When selected, the jury interface becomes available to selected jury members."
            " Please remember to uncheck this box after the evaluation is over."
        ),
    )
    closing_date = models.DateField(
        verbose_name="closing date",
        help_text=_("this date is indicative and has no automatic effect")
    )
    candidate_attribute_jury_filter = models.CharField(
        max_length=100, blank=True, null=True,
        help_text="The key of a candidate attribute we need to filter by in jury interface"
    )
    is_recommendation_required = models.BooleanField(
        default=False,
        verbose_name="is recommendation required",
        help_text=_("When selected, jury members have to propose a recommendation for each candidate.")
    )
    can_access_evaluations = models.BooleanField(
        default=False,
        verbose_name=_("can access evaluations"),
        help_text=_(
            "When selected, the jury interface displays the other jury members evaluations for the other step(s) "
            "on each candidate application view."
        )
    )
    notify_association_by_email = models.BooleanField(
        default=False, verbose_name=_('notify association by email'),
        help_text=_('Send an email when a candidate is associated to the step')
    )
    notification_email = models.TextField(
        blank=True, default='', verbose_name=_('notification email'),
        help_text=_('The text of the email sent when a candidate is associated to the step')
    )

    def get_evaluation_keys(self):
        return [ef.key for ef in self.get_evaluation_fields()]

    def public_previous_steps(self):
        """True if possible to access previous step evaluations"""
        return CompetitionStep.objects.filter(
            competition=self.competition,
            can_access_evaluations=True,
            evaluation__isnull=False
        ).exclude(id=self.id).distinct()

    def get_jury_members(self):
        return JuryMember.objects.filter(
            jurymembercompetition__competition=self.competition,
            steps=self,
            id__in=[
                item['jury_member']
                for item in Evaluation.objects.filter(
                    competition_step=self
                ).values('jury_member').distinct()
            ]
        ).order_by("user__last_name", "user__first_name")

    def get_candidates(self, extra_filters=None, order_by='id'):
        if extra_filters is None:
            extra_filters = {}
        filters = {
            'id__in': [
                item['candidate']
                for item in Evaluation.objects.filter(competition_step=self).values('candidate').distinct()
            ]
        }
        filters.update(extra_filters)
        return Candidate.objects.filter(**filters).order_by(order_by)

    def get_evaluation_fields(self, is_note=None):
        qs = EvaluationField.objects.filter(competition_step=self)
        if is_note is not None:
            qs = qs.filter(is_note=is_note)
        return qs.order_by("order_index")

    def get_evaluation_form_class(self, validation_required, competition, step, read_only=False):
        """
        Gets a generic evaluation form

        Argument :
        * validation_required (boolean) : should we or not perform validation on save ?

        Returns a dynamically generated class, containing appropriate form fields
        and, - if necessary - validators, based on competition step evaluation fields

        Implementation notes :
        ---------------------

        In current implementation, form fields are generated on EvaluationForm class instanciation
        so we have to pass 'evaluation_fields' to the form constructor (which can seem
        kind of redondant since we can access these evaluation fields through 'self')

        A better implementation could consist in generate dynamically fields by attaching
        them to the class structure (using setattr, as we do it for the validators)...

        This alternative implementation have been tried but unsuccessfully (dynamically fields
        are not retrieved in 'fields' dictionary, so we stick to current implementation).

        """
        step = self
        # Define dynamic class
        class _EvaluationForm(BetterForm, BootstrapFormMixin):

            def __init__(self, evaluation_fields, *args, **kwargs):

                is_completed = False
                if len(args) and args[0].get('is_completed', ''):
                    is_completed = True

                super(_EvaluationForm, self).__init__(*args, **kwargs)
                # Add field definitions, based on competition step evaluation fields
                field_definitions = []
                for evaluation_field in evaluation_fields:
                    if evaluation_field.widget_type == "forms.RadioSelect":
                         evaluation_field.widget_type = "django_forms.RadioSelect"
                    field_args = {}
                    field_args["label"] = evaluation_field.label
                    field_args["widget"] = eval(evaluation_field.widget_type)
                    field_args["required"] = False
                    if evaluation_field.is_comment_field():
                        validators = []
                        if evaluation_field.minimum_comment_length:
                            validators.append(MinLengthValidator(evaluation_field.minimum_comment_length))
                        if evaluation_field.maximum_comment_length:
                            validators.append(MaxLengthValidator(min(evaluation_field.maximum_comment_length, 8000)))
                        else:
                            validators.append(MaxLengthValidator(8000))

                        help_text = ''
                        if evaluation_field.minimum_comment_length and evaluation_field.maximum_comment_length:
                            help_text = _('The number of characters must be between {0} and {1}').format(
                                evaluation_field.minimum_comment_length, evaluation_field.maximum_comment_length
                            )
                        elif evaluation_field.minimum_comment_length:
                            help_text = _('The minimum number of characters is {0}').format(
                                evaluation_field.minimum_comment_length
                            )
                        elif evaluation_field.maximum_comment_length:
                            help_text = _('The maximum number of characters is {0}').format(
                                evaluation_field.maximum_comment_length
                            )

                        if validators:
                            field_args["validators"] = validators
                            field_args["help_text"] = help_text

                    if evaluation_field.field_extra_args:
                        try:
                            extra_args = eval(evaluation_field.field_extra_args)
                        except:
                            # An error : ignore
                            extra_args = {}
                        for (key, value) in list(extra_args.items()):
                            field_args[key] = eval(str(value))

                    error_on_field = False
                    try:
                        field = eval(evaluation_field.field_type)(**field_args)
                    except:
                        error_on_field = True
                    if not error_on_field:
                        field_definitions.append((evaluation_field.key, field))

                for (key, field) in field_definitions:
                    self.fields[key] = field

                # Add an hidden field for candidate id
                self.fields["candidate_id"] = forms.CharField(widget=forms.HiddenInput)

                self.fields['recommendation'] = forms.ModelChoiceField(
                    queryset=CompetitionAttributeValue.objects.none(),
                    required=False,
                )

                recommendation_attributes = CompetitionAttributeValue.objects.filter(
                    attribute__competition=competition,
                    is_filter=True,
                    hide_recommendation=False
                )
                self.fields['recommendation'].queryset = recommendation_attributes
                # self.fields['recommendation'].initial = recommendation_attributes

                if is_completed and step.is_recommendation_required:
                    self.fields['recommendation'].required = True

                if recommendation_attributes.count():
                    self.fields['recommendation'].widget = forms.Select(
                        choices=[('', '')] + [
                            (attribute.id, attribute.value)
                            for attribute in recommendation_attributes
                        ]
                    )
                else:
                    self.fields['recommendation'].widget = forms.HiddenInput()

                self.fields['is_completed'] = forms.BooleanField(
                    label="I have completed this evaluation",
                    widget=CompletedEvaluationWidget, required=False
                )

                if read_only:
                    for field in self.fields:
                        self.fields[field].widget.attrs['disabled'] = True
                self._patch_fields()

        # If applicable, add validators, based on competition step evaluation fields
        if validation_required:
            for evaluation_field in self.get_evaluation_fields():
                if evaluation_field.required:
                    field_name = evaluation_field.key
                    values_interpeted_as_empty = []
                    setattr(
                        _EvaluationForm, "clean_%s" % field_name,
                        get_dynamic_validator(field_name, values_interpeted_as_empty)
                    )

        # Return the class
        return _EvaluationForm

    def __str__(self):
        return "%s : %s" % (self.competition, self.name)

    class Meta:
        verbose_name = _("competition step")
        verbose_name_plural = _("competition steps")


class CompetitionNews(models.Model):
    competition = models.ForeignKey(Competition, verbose_name=_("competition"), on_delete=models.CASCADE)
    title = models.CharField(verbose_name=_("title"), max_length=200)
    date = models.DateField(verbose_name=_("date"))
    text = models.TextField(verbose_name=_("text"), help_text=("news content"), max_length=200)
    image = models.ImageField(verbose_name=_("image"), blank=True, null=True, upload_to="news")
    is_published = models.BooleanField(default=False, verbose_name=_("is published"))

    def __str__(self):
        return self.title

    def get_thumbnail(self):
        if self.image:
            return get_thumbnail_safe(self.image, "200x100", crop="center")

    def image_filename(self):
        """"""
        if self.image:
            return os.path.split(self.image.name)[-1]
        return ""

    class Meta:
        verbose_name = _("competition news")
        verbose_name_plural = _("competition news")


class JuryMemberGroup(models.Model):
    competition = models.ForeignKey(Competition, verbose_name="concours", on_delete=models.CASCADE)
    name = models.CharField(verbose_name="nom", max_length=200)

    def __str__(self):
        return self.name

    def members(self):
        return JuryMember.objects.filter(groups=self)

    class Meta:
        verbose_name = _("jury member group")
        verbose_name_plural = _("jury member groups")
        ordering = ['name']


class JuryMember(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    phone = models.CharField(max_length=100, blank=True, null=True)
    competitions = models.ManyToManyField(Competition, through="ulysses_competitions.JuryMemberCompetition", blank=True)
    groups = models.ManyToManyField(JuryMemberGroup, blank=True)
    steps = models.ManyToManyField(CompetitionStep, blank=True)

    class Meta:
        verbose_name = _("jury member")
        verbose_name_plural = _("jury members")

    def __str__(self):
        return self.name()

    def name(self):
        return "{0}, {1}".format(self.user.last_name, self.user.first_name)

    def save(self, *args, **kwargs):
        # make sure the user is also added to the "jury-members" group
        if not hasattr(settings, 'JURY_MEMBERS_GROUP'):
            raise RuntimeError("Please specify 'JURY_MEMBERS_GROUP' in settings")
        if not Group.objects.filter(name=settings.JURY_MEMBERS_GROUP).exists():
            raise RuntimeError("Please define '%s' group" % settings.JURY_MEMBERS_GROUP)
        jury_member_group = Group.objects.get(name=settings.JURY_MEMBERS_GROUP)
        self.user.groups.add(jury_member_group)
        super(JuryMember, self).save(*args, **kwargs)


class JuryMemberCompetition(models.Model):
    jury_member = models.ForeignKey(JuryMember, on_delete=models.CASCADE)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    notes = models.TextField(blank=True, null=True)


def on_jury_member_delete(sender, instance, **kwargs):
    """Remove the 'jury-members' group on the user of a deleted JuryMember.

    A signal is used here instead of the JuryMember.delete() method as it would
    not be called on bulk deletion otherwise (select multiple JuryMembers in
    the admin and use the "delete" action)

    """
    jm_group = Group.objects.get(name=settings.JURY_MEMBERS_GROUP)
    instance.user.groups.remove(jm_group)


def on_post_delete(sender, **kwargs):
    instance = kwargs['instance']
    if hasattr(instance, "on_post_delete"):
        eval("instance.on_post_delete()")

pre_delete.connect(on_jury_member_delete, sender=JuryMember)
post_delete.connect(on_post_delete)


class JuryMemberNotification(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    from_email = models.EmailField(blank=True, null=True)
    jury_member = models.ForeignKey(JuryMember, on_delete=models.CASCADE)
    competition_step = models.ForeignKey(CompetitionStep, on_delete=models.CASCADE)
    subject = models.CharField(max_length=400)
    body = models.TextField(max_length=4000)
    date = models.DateTimeField()


class CandidateGroup(models.Model):
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    name = models.CharField(verbose_name=_("group name"), max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _("candidate group")
        verbose_name_plural = _("candidate groups")
        ordering = ['name']

    def save(self, *args, **kwargs):
        # Avoid spaces and tabs at the beginning and end of a candidate group name
        # This can cause error when selecting a group in admin
        self.name = self.name.strip()
        return super(CandidateGroup, self).save(*args, **kwargs)


class Candidate(models.Model):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    groups = models.ManyToManyField(CandidateGroup, blank=True)
    steps = models.ManyToManyField(CompetitionStep, blank=True)
    is_valid = models.BooleanField(default=False)
    id_in_competition = models.IntegerField()
    application_date = models.DateTimeField()
    application_summary = models.TextField(max_length=16000, blank=True, null=True)
    notes = models.TextField(blank=True, null=True)
    attributes = models.ManyToManyField(CompetitionAttributeValue, through='CandidateAttributeValue', blank=True)

    def __str__(self):
        return "%s, %s" % (self.composer.user.last_name, self.composer.user.first_name)

    def get_profile_as_excel_data(self):

        profile = get_profile(self.composer.user)

        gender = getattr(profile, 'gender', None)
        birth_date = getattr(profile, 'birth_date', None)
        citizenship = getattr(profile, 'citizenship', None)

        lines = [
            {'cells': [{'value': "Last name"}, {'value': profile.user.last_name}]},
            {'cells': [{'value': "First name"}, {'value': profile.user.first_name}]},
            {'cells': [{'value': "Citizenship"}, {'value': citizenship.name if citizenship else ''}]},
            {'cells': [{'value': "Pronoun"}, {'value': gender.name if gender else ""}]},
            {'cells': [{'value': "Birth date"}, {'value': birth_date.strftime("%d-%m-%Y") if birth_date else ''}]},
            {'cells': [{'value': "Email"}, {'value': profile.user.email}]},
            {'cells': [{'value': "Address 1"}, {'value': profile.address1}]},
            {'cells': [{'value': "Address 2"}, {'value': profile.address2}]},
            {'cells': [{'value': "Zipcode"}, {'value':  profile.zipcode}]},
            {'cells': [{'value': "City"}, {'value': profile.city}]},
            {'cells': [{'value': "Country"}, {'value': profile.country.name if profile.country else ''}]},
            {'cells': [{'value': "Phone 1"}, {'value': profile.phone1}]},
            {'cells': [{'value': "Phone 2"}, {'value': profile.phone2}]},
            {'cells': [{'value': "Website"}, {'value': '{0}'.format(profile.website)}]},
        ]

        return {
            "name": "Profile",
            "cols": ['', ''],
            "lines": lines,
        }

    def get_administrator_notes_as_excel_data(self):

        tab = {
            "name": "Administrator notes",
            "cols": ['', ''],
            "lines": [
                {
                    'cells': [
                        {'value': "Application date"},
                        {'value': self.application_date.strftime("%d-%m-%Y")}
                    ]
                },
                {
                    'cells': [
                        {'value': "Is valid"},
                        {'value': self.is_valid}
                    ]
                },
                {
                    'cells': [
                        {'value': "Steps"},
                        {'value': ", ".join([group.name for group in self.steps.all()])}
                    ]
                },
                {
                    'cells': [
                        {'value': "Groups"},
                        {'value': ", ".join([group.name for group in self.groups.all()])}
                    ]
                },
                {
                    'cells': [
                        {'value': "Notes"}, {'value': self.notes}
                    ]
                }
            ]
        }

        return tab

    def get_application_as_excel_data(self):

        tab = {
            "name": "Application data",
            "cols": ['', '']
        }
        lines = []

        application_form = self.competition.get_application_form_instance()

        for fieldset in application_form.fieldsets:
            is_non_media_fieldset = True
            for field in fieldset:
                if not field.name.endswith("_hidden"):
                    field_definition = application_form.get_field_definition_from_name(field.name)
                    lines.append(
                        {
                            'cells': [
                                {'value': field.name},
                                {'value': field_definition.get_value_for_candidate(self)}
                            ]
                        }
                    )
        tab["lines"] = lines

        return tab

    def get_evaluations_for_step_as_excel_data(self, competition_step):

        tab = {
            "name": "Evaluations (%s)" % competition_step.name,
            "cols": ['Jury member', 'Status', 'Notes', 'Comments'],
        }

        lines = []

        for evaluation in Evaluation.objects.filter(candidate=self, competition_step=competition_step):
            notes = EvaluationNote.objects.filter(evaluation=evaluation)
            notes_str = ", " .join(["%s : %s" % (note.key, note.value) for note in notes])
            lines.append(
                {
                    'cells': [
                        {'value': str(evaluation.jury_member)},
                        {'value': evaluation.status.name},
                        {'value': notes_str},
                        {'value': evaluation.comments}
                    ]
                }
            )

        tab["lines"] = lines

        return tab

    def get_evaluations_as_excel_data(self):
        tabs = []
        for step in self.steps.all():
            tabs.append(self.get_evaluations_for_step_as_excel_data(step))
        return tabs

    def get_informations_as_excel_data(self):

        tabs = [
            self.get_profile_as_excel_data(),
            self.get_administrator_notes_as_excel_data(),
            self.get_application_as_excel_data()
        ]

        evaluation_tabs = self.get_evaluations_as_excel_data()
        for tab in evaluation_tabs:
            tabs.append(tab)

        return tabs

    def get_admin_file_url_prefix(self):
        return "admin/%s/%s/" % (self.competition.url, self.composer.user.username)

    def get_field_raw_value(self, field_definition):
        key = field_definition.name
        field_kind = field_definition.kind

        if field_kind.name == 'DocumentField':
            try:
                elt = CandidateDocument.objects.get(candidate=self, key=key)
                return elt.id
            except CandidateDocument.DoesNotExist:
                return ""

        media_fields = [
            "MediaField", "VideoField", "AudioField", "ScoreVideoField", "ScoreAudioField", "YouTubeField",
            "VideoYouTubeField",
        ]
        if field_kind.name in media_fields:
            try:
                elt = CandidateMedia.objects.get(candidate=self, key=key)
                return elt.id
            except CandidateMedia.DoesNotExist:
                return ""

        return self.get_field_value(field_definition)

    def get_field_value(self, field_definition):
        value = None
        key = field_definition.name
        field_kind = field_definition.kind

        if field_kind.name == 'BiographicElementField':
            try:
                elt = CandidateBiographicElement.objects.get(candidate=self, key=key)
                value = elt.get_html()
            except CandidateBiographicElement.DoesNotExist:
                pass

        if field_kind.name == 'DocumentField':
            try:
                elt = CandidateDocument.objects.get(candidate=self, key=key)
                value = elt.get_html()
            except CandidateDocument.DoesNotExist:
                pass

        media_fields = [
            "MediaField", "VideoField", "AudioField", "ScoreVideoField", "ScoreAudioField", "YouTubeField",
            "VideoYouTubeField",
        ]
        if field_kind.name in media_fields:
            try:
                elt = CandidateMedia.objects.get(candidate=self, key=key)
                value = elt.get_html()
            except CandidateMedia.DoesNotExist:
                pass

        if field_kind.name in ('CompetitionAttributeValueField', ):
            try:
                elt = CandidateAttributeValue.objects.get(candidate=self, attribute=key)
                value = make_string_storable_in_dict(elt.value.key)
            except CandidateAttributeValue.DoesNotExist:
                pass

        if field_kind.name in ('CompetitionAttributeMultipleValueField', ):
            value = []
            attribute_values = field_definition.attribute_values()
            for attr_index, (attr_key, attr_value) in enumerate(attribute_values):
                elt_key = "{0}_{1}".format(key, attr_index + 1)
                try:
                    elt = CandidateAttributeValue.objects.get(candidate=self, attribute=elt_key)
                    if elt and elt.value.key == attr_key:
                        value.append(attr_key)
                except CandidateAttributeValue.DoesNotExist:
                    pass

        application_element_kinds = (
            'ApplicationTextElementField', 'ApplicationHtmlElementField', 'ApplicationBooleanElementField',
        )
        if field_kind.name in application_element_kinds:
            try:
                elt = ApplicationElement.objects.get(candidate=self, key=key)
                if elt.multiple_values:
                    value = pickle.loads(str(elt.value))
                else:
                    value = make_string_storable_in_dict(elt.value)
            except ApplicationElement.DoesNotExist:
                pass

        return value

    def set_field_value(self, field_definition, data):
        key = field_definition.name
        field_kind = field_definition.kind
        value = data.get(key)

        if field_kind.name in ('CompetitionAttributeValueField', ):
            try:
                value = CompetitionAttributeValue.objects.get(
                    attribute=field_definition.competition_attribute, key=value
                )
                try:
                    elt = CandidateAttributeValue.objects.get(candidate=self, attribute=key)
                    elt.value = value
                    elt.save()
                except CandidateAttributeValue.DoesNotExist:
                    CandidateAttributeValue.objects.create(candidate=self, attribute=key, value=value)

            except CompetitionAttributeValue.DoesNotExist:
                pass

        if field_kind.name in ('CompetitionAttributeMultipleValueField', ):
            values = dict(data).get(key)
            attribute_values = field_definition.attribute_values()
            elt_prefix = "{0}_".format(key)
            CandidateAttributeValue.objects.filter(candidate=self, attribute__startswith=elt_prefix).delete()
            for attr_index, (attr_key, attr_value) in enumerate(attribute_values):
                elt_key = "{0}_{1}".format(key, attr_index + 1)
                if values:
                    if attr_key in values:
                        try:
                            competition_attr_value = CompetitionAttributeValue.objects.get(
                                attribute=field_definition.competition_attribute, key=attr_key
                            )
                            CandidateAttributeValue.objects.create(
                                candidate=self, attribute=elt_key, value=competition_attr_value
                            )
                        except CompetitionAttributeValue.DoesNotExist:
                            pass

        application_element_kinds = (
            'ApplicationTextElementField', 'ApplicationHtmlElementField',
            'ApplicationBooleanElementField'
        )
        if field_kind.name in application_element_kinds:
            if field_kind.name == 'ApplicationBooleanElementField':
                if isinstance(value, str):
                    # checkbox : on si coché
                    value = (value.lower() in ('on', 'true', ))
                value = 'True' if value else 'False'
            try:
                elt = ApplicationElement.objects.get(candidate=self, key=key)
                elt.value = value
                elt.save()
            except ApplicationElement.DoesNotExist:
                ApplicationElement.objects.create(candidate=self, key=key, value=value)

    def set_application_data(self, data):
        if self.competition.dynamic_application_form:
            # If we have an application form, we know all fields to query
            # So we look for a value for each field of the application form
            # This can avoid mistake of duplicate fields if the type of a field of the application_form has been
            # changed after some applications.
            for field_definition in self.competition.dynamic_application_form.get_fields():
                self.set_field_value(field_definition, data)
        else:
            # All forms are dynamic form now
            raise NotImplemented

    def get_application_data(self):
        data = {}

        if self.competition.dynamic_application_form:
            # If we have an application form, we know all fields to query
            # So we look for a value for each field of the application form
            # This can avoid mistake of duplicate fields if the type of a field of the application_form has been
            # changed after some applications.
            for field_definition in self.competition.dynamic_application_form.get_fields():
                value = self.get_field_value(field_definition)
                if value is not None:
                    data[field_definition.name] = value
                    doc_and_media_fields = [
                        "DocumentField", "MediaField", "VideoField", "AudioField", "ScoreVideoField", "ScoreAudioField",
                        "YouTubeField", "VideoYouTubeField",
                    ]
                    if field_definition.kind.name in doc_and_media_fields:
                        raw_value = self.get_field_raw_value(field_definition)
                        hidden_field_name = '{0}_hidden'.format(field_definition.name)
                        data[hidden_field_name] = raw_value
        else:

            for item in CandidateBiographicElement.objects.filter(candidate=self):
                data[item.key] = make_string_storable_in_dict(item.get_html())
            for item in CandidateDocument.objects.filter(candidate=self):
                data[item.key] = make_string_storable_in_dict(item.get_html())
            for item in CandidateMedia.objects.filter(candidate=self):
                data[item.key] = make_string_storable_in_dict(item.get_html())
            for item in CandidateAttributeValue.objects.filter(candidate=self):
                data[item.attribute] = make_string_storable_in_dict(item.value.key)
            for item in ApplicationElement.objects.filter(candidate=self):
                if item.multiple_values:
                    data[item.key] = pickle.loads(str(item.value))
                else:
                    data[item.key] = make_string_storable_in_dict(item.value)

        # PATCH : BUG FIX - Topic areas are not correctly loaded
        # The problem is the following : we can have several CandidateAttributeValue
        # linked to a single form field, and we need to identify the values
        # and group them in a list
        # We make this ugly hard-coded patch by now for residency
        # This has to be redesign in a generic way in a future version
        if self.competition.url == "residency":
            # Look for keys beginning by 'topic_areas_...'
            # Group their values in a list
            # Replace them by a key named 'topic_areas'
            # associated to the list
            topic_areas = []
            keys_to_remove = []
            for key, value in list(data.items()):
                if key.startswith("topic_areas_"):
                    topic_areas.append(value)
                    keys_to_remove.append(key)
            for key in keys_to_remove:
                del data[key]
            # Note : this replace the existing entry 'topic_areas'
            # for pre-CandidateAttributeValue candidates
            # who have also the topic areas store in application elements
            data["topic_areas"] = topic_areas
        # END PATCH
        return data

    def last_name(self):
        return self.composer.user.last_name

    def first_name(self):
        return self.composer.user.first_name

    def full_name(self):
        return '{0} {1}'.format(self.composer.user.first_name, self.composer.user.last_name)

    def filters_str(self):
        return ", ".join([att.value for att in self.attributes.filter(is_filter=True)])
    filters_str.short_description = "Choices"

    def recommendations_str(self):
        recommendations = [evaluation.recommendation for evaluation in Evaluation.objects.filter(candidate=self)]
        return ", ".join([att.value for att in recommendations if att])
    recommendations_str.short_description = "Recommendations"

    class Meta:
        verbose_name = _("candidate")
        unique_together = (('composer', 'competition'), ('id_in_competition', 'competition'),)


class CandidateNotification(models.Model):
    sender = models.ForeignKey(User, on_delete=models.CASCADE)
    from_email = models.EmailField(blank=True, null=True)
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    competition_step = models.ForeignKey(CompetitionStep, on_delete=models.CASCADE)
    subject = models.CharField(max_length=400)
    body = models.TextField(max_length=4000)
    date = models.DateTimeField()
    is_test = models.BooleanField()


class CandidateAttributeValue(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    attribute = models.CharField(max_length=200)
    value = models.ForeignKey(CompetitionAttributeValue, on_delete=models.CASCADE)

    def __str__(self):
        return "%s : %s" % (self.attribute, self.value)

    class Meta:
        unique_together = (('candidate', 'attribute'),)


def get_user_temporary_folder(username):
    # Create media root, if necessary
    if not os.path.exists(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)
    # Create a temporary directory, if if doesn't exist
    if not os.path.exists(settings.TEMPORARY_FILES_ROOT):
        os.mkdir(settings.TEMPORARY_FILES_ROOT)
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    user_temporary_folder = os.path.join(settings.TEMPORARY_FILES_ROOT, username)
    if not os.path.exists(user_temporary_folder):
        os.mkdir(user_temporary_folder)
    # Return folder
    return user_temporary_folder


def get_candidate_folder_helper(competition, composer):
    # Create media root, if necessary
    if not os.path.exists(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)
    # Create candidates files root, if necessary
    if not os.path.exists(settings.CANDIDATE_FILES_ROOT):
        os.mkdir(settings.CANDIDATE_FILES_ROOT)
    # Create a competition directory, if if doesn't exist
    competition_dir_path = os.path.join(settings.CANDIDATE_FILES_ROOT, competition.url)
    if not os.path.exists(competition_dir_path):
        os.mkdir(competition_dir_path)
    # Create a candidate directory inside the competition directory, if if doesn't exist
    candidate_dir_path = os.path.join(competition_dir_path, composer.user.username)
    if not os.path.exists(candidate_dir_path):
        os.mkdir(candidate_dir_path)
    # Return folder
    return candidate_dir_path

def get_candidate_folder(candidate):
    return get_candidate_folder_helper(candidate.competition, candidate.composer)


def get_candidate_admin_folder(candidate):
    # Create media root, if necessary
    if not os.path.exists(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)
    # Create admin files root, if necessary
    if not os.path.exists(settings.ADMIN_FILES_ROOT):
        os.mkdir(settings.ADMIN_FILES_ROOT)
    # Create a competition directory, if if doesn't exist
    competition_dir_path = os.path.join(settings.ADMIN_FILES_ROOT, candidate.competition.url)
    if not os.path.exists(competition_dir_path):
        os.mkdir(competition_dir_path)
    # Create a candidate directory inside the competition directory, if if doesn't exist
    candidate_dir_path = os.path.join(competition_dir_path, candidate.composer.user.username)
    if not os.path.exists(candidate_dir_path):
        os.mkdir(candidate_dir_path)
    # Return folder
    return candidate_dir_path


def check_location(file, root_directories):
    tokens = file.split('/')
    if not tokens[0] in root_directories:
        raise RuntimeError(
            "Source file must be located in one of theses folders : '%s'. Current file : '%s'" % (
                ", ".join(root_directories), file))


def get_root_helper(path):
    return os.path.split(path)[1]


def get_personal_files_root():
    return get_root_helper(settings.PERSONAL_FILES_ROOT)


def get_candidate_files_root():
    return get_root_helper(settings.CANDIDATE_FILES_ROOT)


def get_temporary_files_root():
    return get_root_helper(settings.TEMPORARY_FILES_ROOT)


def get_draft_files_root():
    return get_root_helper(settings.DRAFT_FILES_ROOT)


def check_owner(file_url, composer):
    tokens = file_url.split('/')
    root = tokens[0]
    # Determine owner from file url

    if root == get_personal_files_root():
        owner = tokens[1]

    elif root == get_candidate_files_root():
        # Two possible patterns:
        # candidates/yairklar/passport.jpg'  (old pattern)
        # candidates/acanthes-composers/yairklar/passport.jpg'  (new pattern)
        if composer.user.username == tokens[1]:
            owner = tokens[1] # Old pattern is used
        else:
            owner = tokens[2]

    elif root == get_draft_files_root():
        owner = tokens[2]

    elif root == get_temporary_files_root():
        owner = tokens[1]

    elif root == "works":  # ulysses works root
        owner_id = tokens[1]
        owner = User.objects.get(id=owner_id).username

    else:
        raise RuntimeError("Unknown file type : '%s'" % file_url)

    # Check owner
    if owner != composer.user.username:
        raise RuntimeError("Source file '%s' doesn't belong to user '%s'" % (file_url, composer.user.username))


def ensure_target_path_is_new(target_path):
    """
    Ensures that the given target path corresponds to a new (still inexistent) file.
    If it corresponds to an existing file, modify the target path (with one
    or more '_' at the end of the filename) to ensure that this will create a new file.
    """
    final_path = target_path
    cpt = 0
    while os.path.isfile(final_path):
        (base_dir, filename)= os.path.split(final_path)
        (name, ext) = os.path.splitext(filename)
        final_name = "%s_%s" % (name, ext)
        final_path = os.path.join(base_dir, final_name)
        cpt += 1
        if cpt > 50:
            raise RuntimeError("Possible infinite loop ! Stopping iterations...final_path : '%s'" % final_path)
    return final_path


def move_file(source_path, target_path):
    target_path = ensure_target_path_is_new(target_path)
    shutil.move(source_path, target_path)
    return get_url_from_path(target_path)


def copy_file(source_path, target_path):
    target_path = ensure_target_path_is_new(target_path)
    try:
        shutil.copyfile(source_path, target_path)
    except IOError:
        pass
    return get_url_from_path(target_path)


def move_file_to_draft_folder(competition, composer, file_url):
    """ Copies the specified file into draft folder for specified competition & composer """
    source_path = get_path_from_url(file_url)
    # if not is_temporary_file(source_path):
    #     raise RuntimeError("Source file '%s' is not a temporary file" % file_url)
    # Create draft directory, if it doesn't exist
    if not os.path.exists(settings.DRAFT_FILES_ROOT):
        os.mkdir(settings.DRAFT_FILES_ROOT)
    # Create a competition directory into draft directory, if if doesn't exist
    competition_dir_path = os.path.join(settings.DRAFT_FILES_ROOT, competition.url)
    if not os.path.exists(competition_dir_path):
        os.mkdir(competition_dir_path)
    # Create a composer directory inside the competition directory, if if doesn't exist
    composer_dir_path = os.path.join(competition_dir_path , composer.user.username)
    if not os.path.exists(composer_dir_path):
        os.mkdir(composer_dir_path )
    # Move the file
    target_path = os.path.join(composer_dir_path, os.path.basename(source_path))
    return move_file(source_path, target_path)


def get_path_from_url(url):
    """ Gets file path from relative media url """
    return os.path.join(settings.MEDIA_ROOT, *url.split('/'))


def get_url_from_path(path):
    relative_path = os.path.relpath(path, settings.MEDIA_ROOT)
    dirs = []
    current_path = relative_path
    while current_path:
        dirs.append(os.path.split(current_path)[1])
        current_path = os.path.dirname(current_path)
    dirs.reverse()
    return "/".join(dirs)


def is_work_file(source_path):
    expected_dir = os.path.join(settings.MEDIA_ROOT, 'works')
    return source_path.find(expected_dir) == 0


def copy_file_to_user_temporary_folder(composer, file_url):
    """ Copies the specified file into user temporary folder  """
    # Check source file
    check_owner(file_url, composer)
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)

    # Check if file is located in allowed directory
    if is_personal_file(source_path):
        pass

    elif is_candidate_file(source_path):
        pass

    elif is_draft_file(source_path):
        pass

    elif is_work_file(source_path):
        pass

    elif is_temporary_file(source_path):
        pass

    else:
        raise RuntimeError("Source file must be located in personal space, application history or draft files folders")

    # Copy the file
    user_temporary_folder = get_user_temporary_folder(composer.user.username)
    target_path = os.path.join(user_temporary_folder, filename)
    if not is_temporary_file(target_path):
        raise RuntimeError("'%s' is not a temporary file" % target_path)
    return copy_file(source_path, target_path)


def copy_file_to_user_personal_folder(composer, file_url):
    """ Copies the specified file into user personal folder  """
    # Check source file
    check_owner(file_url, composer)
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_candidate_file(source_path):
        raise RuntimeError("Source file must be located in application history")
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    user_personal_folder = get_user_personal_folder(composer.user.username)
    if not os.path.exists(user_personal_folder):
        os.mkdir(user_personal_folder)
    # Copy the file
    target_path = os.path.join(user_personal_folder, filename)
    if not is_personal_file(target_path):
        raise RuntimeError("'%s' is not a personal file" % target_path)
    return copy_file(source_path, target_path)


def copy_work_file_to_user_personal_folder(composer, file_url):
    """ Copies the specified file into user personal folder  """
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    user_personal_folder = get_user_personal_folder(composer.user.username)
    if not os.path.exists(user_personal_folder):
        os.mkdir(user_personal_folder)
    # Copy the file
    target_path = os.path.join(user_personal_folder, filename)
    return copy_file(source_path, target_path)


def move_file_to_candidate_folder(candidate, file_url):
    """
    Copies the specified file into target directory
    /candidates/<competition-url>/<candidate-username>
    """
    # Check source file
    check_owner(file_url, candidate.composer)
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_temporary_file(source_path):
        raise RuntimeError("Source file {0} must be a temporary file".format(source_path))
    # Move the file
    candidate_dir_path = get_candidate_folder(candidate)
    target_path = os.path.join(candidate_dir_path, filename)
    if not is_candidate_file(target_path):
        raise RuntimeError("'%s' is not a candidate file" % target_path)
    return move_file(source_path, target_path)


def move_file_to_other_candidate_folder(candidate, file_url):
    """ Copies the specified file into user personal folder  """
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_candidate_file(source_path):
        raise RuntimeError("Source file must be located in application history")
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    candidate_dir_path = get_candidate_folder(candidate)
    if not os.path.exists(candidate_dir_path):
        os.mkdir(candidate_dir_path)
    # Copy the file
    target_path = os.path.join(candidate_dir_path, filename)
    if not is_candidate_file(target_path):
        raise RuntimeError("'%s' is not a candidate file" % target_path)
    return move_file(source_path, target_path)


def move_file_to_other_personal_folder(composer, file_url):
    """ Copies the specified file into user personal folder  """
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_personal_file(source_path):
        raise RuntimeError("Source file must be located in personal folder")
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    user_personal_folder = get_user_personal_folder(composer.user.username)
    if not os.path.exists(user_personal_folder):
        os.mkdir(user_personal_folder)
    # Copy the file
    target_path = os.path.join(user_personal_folder, filename)
    if not is_personal_file(target_path):
        raise RuntimeError("'%s' is not a personal file" % target_path)
    return move_file(source_path, target_path)


def move_file_to_user_personal_folder(composer, file_url):
    """ Copies the specified file into user personal folder  """
    # Check source file
    check_owner(file_url, composer)
    # Determine file infos
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_candidate_file(source_path):
        raise RuntimeError("Source file must be located in application history")
    # Create a user temporary directory inside the temporary directory, if if doesn't exist
    user_personal_folder = get_user_personal_folder(composer.user.username)
    if not os.path.exists(user_personal_folder):
        os.mkdir(user_personal_folder)
    # Copy the file
    target_path = os.path.join(user_personal_folder, filename)
    if not is_personal_file(target_path):
        raise RuntimeError("'%s' is not a personal file" % target_path)
    return move_file(source_path, target_path)


def move_file_to_other_temporary_folder(composer, file_url):
    """
    Copies the specified file into target directory
    /candidates/<competition-url>/<candidate-username>
    """
    # Check source file
    source_path = get_path_from_url(file_url)
    filename = os.path.basename(source_path)
    if not is_temporary_file(source_path):
        raise RuntimeError("Source file must be a temporary file")
    # Move the file
    temporary_dir_path = get_user_temporary_folder(composer.user.username)
    target_path = os.path.join(temporary_dir_path, filename)
    if not is_temporary_file(target_path):
        raise RuntimeError("'%s' is not a temporary file" % target_path)
    return move_file(source_path, target_path)


class ApplicationMedia(MediaBase):
    key = models.CharField(max_length=200)

    class Meta:
        abstract = True


def delete_media_helper(media):
    # Delete physical files, if they exist
    if media.score:
        delete_file_helper(media.score)
    if media.audio:
        delete_file_helper(media.audio)
    if media.video:
        delete_file_helper(media.video)


class TemporaryMedia(ApplicationMedia):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)
    competition = models.ForeignKey("Competition", on_delete=models.CASCADE)

    def move_as_draft_element(self, draft):
        # Remove existing draft media, if necessary
        DraftMedia.objects.filter(draft=draft, key=self.key).delete()
        # Create new draft media
        draft_media = DraftMedia()
        draft_media.title = self.title
        if self.score:
            draft_media.score = move_file_to_draft_folder(self.competition, self.composer, self.score)
        else:
            draft_media.score = ''
        if self.audio:
            draft_media.audio = move_file_to_draft_folder(self.competition, self.composer, self.audio)
        else:
            draft_media.audio = ''
        if self.video:
            draft_media.video = move_file_to_draft_folder(self.competition, self.composer, self.video)
        else:
            draft_media.video = ''
        draft_media.draft = draft
        draft_media.key = self.key
        draft_media.link = self.link
        draft_media.notes = self.notes
        draft_media.save()
        # Delete self
        self.delete()
        # Return draft media
        return draft_media

    def move_as_candidate_element(self, candidate):
        # Create new candidate media
        candidate_media = CandidateMedia()
        candidate_media.title = self.title
        if self.score:
            candidate_media.score = move_file_to_candidate_folder(candidate, self.score)
        if self.audio:
            candidate_media.audio = move_file_to_candidate_folder(candidate, self.audio)
        if self.video:
            candidate_media.video = move_file_to_candidate_folder(candidate, self.video)
        candidate_media.candidate = candidate
        candidate_media.key = self.key
        candidate_media.link = self.link
        candidate_media.notes = self.notes
        candidate_media.save()
        # Delete self
        self.delete()
        # Return candidate media
        return candidate_media

    def move_to_other_composer(self, composer):
        # Create new candidate media
        if self.score:
            self.score = move_file_to_other_temporary_folder(composer, self.score)
        if self.audio:
            self.audio = move_file_to_other_temporary_folder(composer, self.audio)
        if self.video:
            self.video = move_file_to_other_temporary_folder(composer, self.video)
        self.save()

    def reassign_to_other_composer(self, composer, old_username):
        # Move candidate media
        verbose = 'reassign_candidate_files' in sys.argv
        if self.score and old_username in self.score:
            if verbose:
                print('> move score', self.score, 'to temporary folder', composer.user.username)
            self.score = move_file_to_other_temporary_folder(composer, self.score)
        if self.audio and old_username in self.audio:
            if verbose:
                print('> move audio', self.audio, 'to temporary folder', composer.user.username)
            self.audio = move_file_to_other_temporary_folder(composer, self.audio)
        if self.video and old_username in self.video:
            if verbose:
                print('> move video', self.video, 'to temporary folder', composer.user.username)
            self.video = move_file_to_other_temporary_folder(composer, self.video)
        self.save()

    def get_youtube_link(self, link):
        if link:
            youtube_code = get_youtube_code(link)
            if youtube_code:
                return '<a href="{0}" class="colorbox-form">YouTube</a>'.format(
                    reverse('web:youtube_popup', args=[youtube_code])
                )
            else:
                return '<a href="{0}" target="_blank">YouTube</a>'.format(link)
        return ""

    @property
    def as_html(self):
        links = []
        if self.score:
            links.append(
                '<a href="{0}{1}" target="_blank">score</a>'.format(settings.MEDIA_URL, self.score)
            )
        if self.audio:
            links.append(
                '<a href="{0}{1}" target="_blank">audio</a>'.format(settings.MEDIA_URL, self.audio)
            )
        if self.video:
            links.append(
                '<a href="{0}{1}" target="_blank">video</a>'.format(settings.MEDIA_URL, self.video)
            )
        if self.link:
            links.append(
                self.get_youtube_link(self.link)
            )

        return '{0} {1}'.format(
            self.title,
            ', '.join(['(' + link + ')' for link in links])
        )

    def on_post_delete(self):
        delete_media_helper(self)

    class Meta:
        unique_together = (('composer', 'competition', 'key'),)


class DraftMedia(ApplicationMedia):
    draft = models.ForeignKey(ApplicationDraft, on_delete=models.CASCADE)

    def copy_as_temporary_element(self):
        # Remove existing temporary document, if necessary
        return create_temporary_media(
            self.draft.competition, self.draft.composer, self.key, self.title, self.score, self.audio, self.video,
            self.notes, self.link
        )

    def on_post_delete(self):
        delete_media_helper(self)

    @property
    def as_html(self):
        links = []
        if self.score:
            links.append(
                '<a href="{0}{1}" target="_blank">score</a>'.format(settings.MEDIA_URL, self.score)
            )
        if self.audio:
            links.append(
                '<a href="{0}{1}" target="_blank">audio</a>'.format(settings.MEDIA_URL, self.audio)
            )
        if self.video:
            links.append(
                '<a href="{0}{1}" target="_blank">video</a>'.format(settings.MEDIA_URL, self.video)
            )

        return '{0} {1}'.format(
            self.title,
            ', '.join(['(' + link + ')' for link in links])
        )


class CandidateMedia(ApplicationMedia):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False, help_text="True if this media is owned by administrator")

    def copy_as_personal_element(self):
        work = Media()
        target_title = "%s (from %s)" % (self.title, self.candidate.competition.title)
        work.title = get_unique_title(Media, self.candidate.composer, target_title)
        if self.score:
            work.score = copy_file_to_user_personal_folder(self.candidate.composer, self.score)
        if self.audio:
            work.audio = copy_file_to_user_personal_folder(self.candidate.composer, self.audio)
        if self.video:
            work.video = copy_file_to_user_personal_folder(self.candidate.composer, self.video)
        work.composer = self.candidate.composer
        work.link = self.link
        work.save()

    def move_to_other_candidate(self, candidate):
        # Move candidate media
        if self.score:
            self.score = move_file_to_other_candidate_folder(candidate, self.score)
        if self.audio:
            self.audio = move_file_to_other_candidate_folder(candidate, self.audio)
        if self.video:
            self.video = move_file_to_other_candidate_folder(candidate, self.video)
        self.save()

    def reassign_to_other_candidate(self, candidate, old_username):
        # Move candidate media
        verbose = 'reassign_candidate_files' in sys.argv
        if self.score and old_username in self.score:
            if verbose:
                print('> move score', self.score, 'to candidate folder', candidate.composer.user.username)
            self.score = move_file_to_other_candidate_folder(candidate, self.score)
        if self.audio and old_username in self.audio:
            if verbose:
                print('> move audio', self.audio, 'to candidate folder', candidate.composer.user.username)
            self.audio = move_file_to_other_candidate_folder(candidate, self.audio)
        if self.video and old_username in self.video:
            if verbose:
                print('> move video', self.video, 'to candidate folder', candidate.composer.user.username)
            self.video = move_file_to_other_candidate_folder(candidate, self.video)
        self.save()

    class Meta:
        verbose_name = "candidate media"
        verbose_name_plural = "candidate medias"


class ApplicationDocument(DocumentBase):
    key = models.CharField(max_length=200)

    class Meta:
        abstract = True


def check_file_type_helper(path, root_path):
    return os.path.normpath(os.path.commonprefix([path, root_path])) == os.path.normpath(root_path)


def is_temporary_file(path):
    return check_file_type_helper(path, settings.TEMPORARY_FILES_ROOT)


def is_draft_file(path):
    return check_file_type_helper(path, settings.DRAFT_FILES_ROOT)


def is_personal_file(path):
    return check_file_type_helper(path, settings.PERSONAL_FILES_ROOT)


def is_candidate_file(path):
    return check_file_type_helper(path, settings.CANDIDATE_FILES_ROOT)


def delete_file_helper(file_relative_path):
    path = os.path.join(settings.MEDIA_ROOT, file_relative_path)
    if os.path.isfile(path):
        if is_temporary_file(path) or is_draft_file(path) or is_personal_file(path):
            os.remove(path)
        else:
            raise RuntimeError("Only temporary files, personal and draft files can be deleted")


class TemporaryDocument(ApplicationDocument):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)
    competition = models.ForeignKey("Competition", on_delete=models.CASCADE)

    def move_as_draft_element(self, draft):
        # Remove existing draft document, if necessary
        DraftDocument.objects.filter(draft=draft, key=self.key).delete()
        # Create new draft document
        draft_doc = DraftDocument()
        try:
            draft_doc.file = move_file_to_draft_folder(self.competition, self.composer, self.file)
        except IOError:
            self.delete()
            return None
        draft_doc.title = self.title
        draft_doc.draft = draft
        draft_doc.key = self.key
        draft_doc.save()

        for extra_file in self.get_extra_files():
            draft_extra_file = DraftDocumentExtraFile(document=draft_doc)
            draft_extra_file.file = move_file_to_draft_folder(self.competition, self.composer, extra_file.file)
            draft_extra_file.save()

        # Delete self
        self.delete()
        # Return draft
        return draft_doc

    def move_as_candidate_element(self, candidate):
        # Create new candidate media
        candidate_doc = CandidateDocument()
        candidate_doc.title = self.title
        candidate_doc.file = move_file_to_candidate_folder(candidate, self.file)
        candidate_doc.candidate = candidate
        candidate_doc.key = self.key
        candidate_doc.save()

        for extra_file in self.get_extra_files():
            candidate_extra_file = CandidateDocumentExtraFile(document=candidate_doc)
            candidate_extra_file.file = move_file_to_candidate_folder(candidate, extra_file.file)
            candidate_extra_file.save()

        # Delete self
        self.delete()
        # Return candidate document
        return candidate_doc

    def move_to_other_composer(self, composer):
        # Create new candidate media
        self.file = move_file_to_other_temporary_folder(composer, self.file)
        self.save()
        for extra_file in self.get_extra_files():
            extra_file.move_to_other_composer(composer)

    def reassign_to_other_composer(self, composer, old_username):
        # Move candidate media
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to temporary folder', composer.user.username)
            self.file = move_file_to_other_temporary_folder(composer, self.file)
            self.save()
        for extra_file in self.get_extra_files():
            extra_file.reassign_to_other_composer(composer, old_username)

    def on_post_delete(self):
        # Delete physical file, if it exists
        delete_file_helper(self.file)

    class Meta:
        unique_together = (('competition', 'composer', 'key'),)

    def get_extra_files(self):
        return self.extra_files.all()


class TemporaryDocumentExtraFile(DocumentExtraFileBase):
    document = models.ForeignKey(TemporaryDocument, on_delete=models.CASCADE, related_name='extra_files')

    def move_to_other_composer(self, composer):
        # Create new candidate media
        self.file = move_file_to_other_temporary_folder(composer, self.file)
        self.save()

    def reassign_to_other_composer(self, composer, old_username):
        # Move candidate media
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to temporary folder', composer.user.username)
            self.file = move_file_to_other_temporary_folder(composer, self.file)
            self.save()

    def as_edit_html(self, preview=False):
        if not preview:
            remove_link = '<a href="{0}" class="document-file-item-remove colorbox-form"><img src="{1}{2}" /></a>'.format(
                reverse('web:remove_temporary_document_file', args=[self.document.id, self.id]),
                settings.STATIC_URL,
                'icons/trash-can-green.svg'
            )
        else:
            remove_link = ''
        html = '<span class="document-file-item"><a href="{0}{1}" target="_blank">{2}</a> {3}</span>'.format(
            settings.MEDIA_URL, self.file, os.path.split(self.file)[-1], remove_link
        )
        return html


class DraftDocument(ApplicationDocument):
    draft = models.ForeignKey(ApplicationDraft, on_delete=models.CASCADE)

    def copy_as_temporary_element(self):
        # Remove existing temporary document, if necessary
        return create_temporary_document(
            self.draft.competition, self.draft.composer, self.key, self.title, self.file, self.get_extra_files()
        )

    def on_post_delete(self):
        delete_file_helper(self.file)

    def get_extra_files(self):
        return self.extra_files.all()


class DraftDocumentExtraFile(DocumentExtraFileBase):
    document = models.ForeignKey(DraftDocument, on_delete=models.CASCADE, related_name='extra_files')


def get_unique_title(element_class, composer, title):
    final_title = title[:380]
    cpt = 1
    while element_class.objects.filter(title=final_title).exists():
        final_title = "{0} ({1})".format(title, cpt)
        cpt += 1
    return final_title


class CandidateDocument(ApplicationDocument):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False, help_text="True if this document is owned by administrator")

    def copy_as_personal_element(self):
        doc = Document()
        target_title = "%s (from %s)" % (self.title, self.candidate.competition.title)
        doc.title = get_unique_title(Document, self.candidate.composer, target_title)
        doc.file = copy_file_to_user_personal_folder(self.candidate.composer, self.file)
        doc.composer = self.candidate.composer
        doc.save()
        for extra_file in self.get_extra_files():
            extra_file.copy_as_personal_element(doc)

    def move_to_other_candidate(self, candidate):
        # Create new candidate media
        self.file = move_file_to_other_candidate_folder(candidate, self.file)
        self.save()
        for extra_file in self.get_extra_files():
            extra_file.move_to_other_candidate(candidate)

    def reassign_to_other_candidate(self, candidate, old_username):
        # Move candidate media
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to candidate folder', candidate.composer.user.username)
            self.file = move_file_to_other_candidate_folder(candidate, self.file)
            self.save()
            for extra_file in self.get_extra_files():
                extra_file.reassign_to_other_candidate(candidate, old_username)

    class Meta:
        verbose_name = "candidate document"
        verbose_name_plural = "candidate documents"

    def get_extra_files(self):
        return self.extra_files.all()


class CandidateDocumentExtraFile(DocumentExtraFileBase):
    document = models.ForeignKey(CandidateDocument, on_delete=models.CASCADE, related_name='extra_files')

    def copy_as_personal_element(self, document):
        extra_file = DocumentExtraFile(document=document)
        extra_file.file = copy_file_to_user_personal_folder(document.composer, self.file)
        extra_file.save()

    def move_to_other_candidate(self, candidate):
        # Create new candidate media
        self.file = move_file_to_other_candidate_folder(candidate, self.file)
        self.save()

    def reassign_to_other_candidate(self, candidate, old_username):
        # Move candidate media
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to candidate folder', candidate.composer.user.username)
            self.file = move_file_to_other_candidate_folder(candidate, self.file)
            self.save()

    def as_edit_html(self, preview=False):
        return super().as_edit_html(True)


class ApplicationBiographicElement(BiographicElementBase):
    key = models.CharField(max_length=200)

    class Meta:
        abstract = True
        ordering = ['creation_date',]


class DraftBiographicElement(ApplicationBiographicElement):
    draft = models.ForeignKey(ApplicationDraft, on_delete=models.CASCADE)


class CandidateBiographicElement(ApplicationBiographicElement):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    is_admin = models.BooleanField(default=False, help_text="True if this media is owned by administrator")

    def copy_as_personal_element(self):
        # Create new biographic element
        bio = BiographicElement()
        target_title = "%s (from %s)" % (self.title, self.candidate.competition.title)
        bio.title = get_unique_title(BiographicElement, self.candidate.composer, target_title)
        bio.text = self.text
        bio.composer = self.candidate.composer
        bio.save()

    class Meta:
        verbose_name = "candidate biographic detail"
        verbose_name_plural = "candidate biographic details"


class ApplicationElement(models.Model):
    candidate = models.ForeignKey(Candidate, on_delete=models.CASCADE)
    key = models.CharField(max_length=200)
    label = models.CharField(max_length=200)
    value = models.CharField(max_length=4000)
    multiple_values = models.BooleanField(
        default=False,
        help_text="Indicates that application element can have multiple values"
    )

    class Meta:
        verbose_name = "application element"
        verbose_name_plural = "appplication elements"


class EvaluationStatus(models.Model):
    name = models.CharField(verbose_name="name", max_length=50)
    url = models.CharField(max_length=50)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "evaluation status"
        verbose_name_plural = "evaluation status"


class EvaluationNote(models.Model):
    evaluation = models.ForeignKey("Evaluation", on_delete=models.CASCADE)
    key = models.CharField(max_length=50)
    value = models.CharField(verbose_name=_("note value"), max_length=8000)

    def get_edit_html(self):
        error_text = 'unknown error'
        try:
            ef = EvaluationField.objects.get(competition_step=self.evaluation.competition_step, key=self.key)
        except EvaluationField.DoesNotExist:
            ef = None
            error_text = 'No field with key {0}'.format(self.key)
        except EvaluationField.MultipleObjectsReturned:
            ef = None
            error_text = 'Duplicate fields with key {0}'.format(self.key)
        if ef:
            if ef.is_choice_field():
                edit_html = forms.Select(choices=ef.get_choices()).render("note_%s" % self.id, self, attrs={})
            else:
                raise RuntimeError("Unhandled field type : %s" % ef.field_type)
        else:
            edit_html = '<span class="error">{0}</span>'.format(error_text)
        return edit_html

    def set_value(self, val):
        self.value = "%s" % val
        self.save()

    def acronym_value(self):
        full_name = self.interpreted_value()
        # Lookup acronym in CompetitionAttributeValue for active competition
        return CompetitionAttributeValue.objects.get(
            attribute__competition=self.evaluation.competition_step.competition,
            value=full_name).acronym

    def interpreted_value(self):
        error_text = 'unknown error'
        try:
            ef = EvaluationField.objects.get(competition_step=self.evaluation.competition_step, key=self.key)
        except EvaluationField.DoesNotExist:
            ef = None
            error_text = 'No field with key {0}'.format(self.key)
        except EvaluationField.MultipleObjectsReturned:
            ef = None
            error_text = 'Duplicate fields with key {0}'.format(self.key)
        if ef:
            if ef.is_choice_field():
                choices = ef.get_choices()
                choice = None
                for key, value in choices:
                    if str(self.value) == str(key):
                        choice = value
                if choice:
                    result = choice
                else:
                    result = self.value
            else:
                result = self.value
        else:
            result = mark_safe('<span class="error-evaluation-field">{0}</span>'.format(error_text))
        return result

    def maximum_value(self):
        ef = self.evaluation_field
        if ef and ef.is_choice_field():
            choices = ef.get_choices()
            max_value = max([str(key) for (key, value) in choices])
            for key, value in choices:
                if str(key) == str(max_value):
                    return value
        return None

    @property
    def evaluation_field(self):
        try:
            ef = EvaluationField.objects.get(competition_step=self.evaluation.competition_step, key=self.key)
        except EvaluationField.DoesNotExist:
            ef = None
        except Evaluation.MultipleObjectsReturned:
            ef = None
        return ef

    def __str__(self):
        return self.value

    class Meta:
        verbose_name = "evaluation note"
        verbose_name_plural = "evaluation notes"
        unique_together = (('evaluation', 'key'),)


class EvaluationField(models.Model):
    """
    A evaluation field definition.

    To a specific competition step are associated 1 to n evaluation fields
    This fields are dynamically used for jury interface and result list generation
    """
    competition_step = models.ForeignKey(
        CompetitionStep, verbose_name=_("competition step"), on_delete=models.CASCADE
    )
    key = models.CharField(max_length=50)
    label = models.CharField(max_length=100)
    weight = models.IntegerField()
    field_type = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("field type"))
    widget_type = models.CharField(max_length=100, blank=True, null=True, verbose_name=_("widget type"))
    required = models.BooleanField(default=False)
    principal = models.BooleanField(
        default=False,
        help_text=_(
            "If set to True, this field is displayed to the right of candidate name in jury interface " +
            "(in case of multiple evaluation fields)"
        )
    )
    use_in_stats = models.BooleanField(
        default=False,
        help_text=_("If set to True, this field is used to compute evaluation statistics in jury interface"))
    display_in_result_list = models.BooleanField(default=False)
    field_extra_args = models.TextField(
        blank=True, null=True,
        verbose_name=_("field extra args"),
        help_text="a dictionary of extra arguments to be passed to the constructor of the field")
    order_index = models.IntegerField()
    is_note = models.BooleanField(default=False)
    minimum_comment_length = models.IntegerField(
        default=0, verbose_name=_('Minimum number of characters for a comment'), help_text=_('If 0, no minimum number')
    )
    maximum_comment_length = models.IntegerField(
        default=0, verbose_name=_('Maximum number of characters for a comment'), help_text=_('If 0, no maximum number')
    )

    def is_choice_field(self):
        return self.field_type == "forms.ChoiceField"

    def is_comment_field(self):
        return self.field_type == 'forms.CharField'

    def get_choices(self):
        if not self.is_choice_field():
            raise RuntimeError("%s is not a choice field" % self)
        extra_args = eval(self.field_extra_args)
        choices = eval(str(extra_args["choices"]))
        return choices


class Evaluation(models.Model):
    """
    Evaluation of a specific candidate by a specific jury member for a specific competition step
    To this evaluation are associated 1 to evaluation notes (in accordance with the evaluation field \
    definitions associated to this competition step)
    """
    competition_step = models.ForeignKey(
        CompetitionStep, verbose_name=_("competition step"), on_delete=models.CASCADE
    )
    candidate = models.ForeignKey(Candidate, verbose_name=_("candidate"), on_delete=models.CASCADE)
    jury_member = models.ForeignKey(JuryMember, verbose_name=_("jury member"), on_delete=models.CASCADE)
    status = models.ForeignKey(EvaluationStatus, on_delete=models.CASCADE)
    comments = models.TextField()
    recommendation = models.ForeignKey(
        CompetitionAttributeValue, blank=True, default=None, null=True, on_delete=models.CASCADE
    )

    def get_notes(self):
        evaluation_fields = EvaluationField.objects.filter(
            competition_step=self.competition_step
        ).order_by("order_index")
        notes = []
        for evaluation_field in evaluation_fields:
            if EvaluationNote.objects.filter(evaluation=self, key=evaluation_field.key).exists():
                notes.append(EvaluationNote.objects.get(evaluation=self, key=evaluation_field.key))
        return notes
    
    def get_average_note(self):
        """
        get average of all notes
        """
        values = []
        for note in self.get_notes():
            try:
                value = int(note.value)
            except ValueError:
                continue
            values.append(value)
        if values:
            return sum(values) / len(values)
        return None
    
    def get_sum_note(self):
        """
        get sum of all notes
        """
        note = 0
        for note_obj in self.get_notes():
            try:
                value = int(note_obj.value)
            except ValueError:
                continue
            note += value
        if note == 0:
            return None
        return note
    
    def get_notes_for_summary(self):
        evaluation_fields = EvaluationField.objects.filter(
            competition_step=self.competition_step, display_in_result_list=True
        ).order_by("order_index")
        notes = []
        for evaluation_field in evaluation_fields:
            if EvaluationNote.objects.filter(evaluation=self, key=evaluation_field.key).exists():
                notes.append(EvaluationNote.objects.get(evaluation=self, key=evaluation_field.key))
        return notes

    def get_short_summary(self):
        return ", ".join(self.get_short_summary_items())

    def get_short_summary_items(self):
        if self.has_single_evaluation_field():
            return [self.get_unique_note().value]
        else:
            principal_fields = EvaluationField.objects.filter(
                competition_step=self.competition_step, principal=True
            ).order_by('order_index')
            notes = []
            for field in principal_fields:
                field_notes = EvaluationNote.objects.filter(evaluation=self, key=field.key)
                notes.extend(field_notes)
            return [note.interpreted_value() for note in notes]

    def get_form(self, read_only=False, empty=False):
        if not empty:
            initial_data = {
                'is_completed': self.status.url == "completed",
                'candidate_id': self.candidate.id,
                'recommendation': self.recommendation,
            }
            for key in self.competition_step.get_evaluation_keys():
                if self.has_note(key):

                    initial_data[key] = self.get_note(key).value
        else:
            initial_data = None
        competition = self.competition_step.competition
        form_class = self.competition_step.get_evaluation_form_class(
            False, competition, self.competition_step, read_only=read_only
        )
        return form_class(
            self.competition_step.get_evaluation_fields(), initial=initial_data
        )

    def get_unique_note(self):
        """
        Gets evaluation unique note

        Nota : this method works only in there is one and only one 'displayable-in-list' evaluation field
        """
        evaluation_field_count = EvaluationField.objects.filter(
            competition_step=self.competition_step, display_in_result_list=True
        ).count()

        if evaluation_field_count == 1:
            key = EvaluationField.objects.filter(
                competition_step=self.competition_step, display_in_result_list=True
            )[0].key
            return EvaluationNote.objects.get(evaluation=self, key=key)
        else:
            raise RuntimeError("Bad evaluation field count : %s" % evaluation_field_count)

    def has_single_evaluation_field(self):
        return EvaluationField.objects.filter(
            competition_step=self.competition_step, display_in_result_list=True
        ).count() == 1

    def get_unique_evaluation_field(self):
        """
        Gets evaluation unique field

        Nota : this method works only in there is one and only one 'displayable-in-list' evaluation field
        """
        evaluation_fields = EvaluationField.objects.filter(
            competition_step=self.competition_step, display_in_result_list=True
        )
        if len(evaluation_fields) == 1:
            return evaluation_fields[0]
        else:
            raise RuntimeError("Bad evaluation field count : %s" % len(evaluation_fields))

    def has_note(self, key):
        return EvaluationNote.objects.filter(evaluation=self, key=key).exists()

    def change_note(self, key, val):
        assert self.has_note(key)
        note = EvaluationNote.objects.get(evaluation=self, key=key)
        note.set_value(val)

    def set_note(self, key, val):
        assert not self.has_note(key)
        note = EvaluationNote()
        note.evaluation = self
        note.key = key
        note.value = "%s" % val
        note.save()

    def get_note(self, key):
        if not self.has_note(key):
            raise RuntimeError("Evaluation #%s : note '%s' doesn't exist" % (self.id, key))
        return EvaluationNote.objects.get(evaluation=self, key=key)

    class Meta:
        verbose_name = _("evaluation")
        unique_together = ('competition_step', 'candidate', 'jury_member')


class JuryApplicationElementToExclude(models.Model):
    """ An application element that has to be excluded from ulysses.jury interface """
    competition = models.ForeignKey(Competition, on_delete=models.CASCADE)
    key = models.CharField(max_length=50, help_text="application form element key")

    class Meta:
        verbose_name_plural = _("jury application elements to exclude")


#-------------------------------------
#
#  Signals
#
#-------------------------------------


def on_pre_save(sender, **kwargs):
    if sender is CandidateDocument:
        instance = kwargs['instance']
        if instance.file.startswith("admin/"):
            instance.is_admin = True
    if sender is Evaluation:
    # Set evaluation default status (to process)
        instance = kwargs['instance']
        try:
            status = instance.status
        except:
            status = None
        if not status:
            instance.status = EvaluationStatus.objects.get(url='to_process')

pre_save.connect(on_pre_save)


def make_competition_admin(user):
    """add the user to the 'competitions admins' group"""
    competition_admins_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)
    if competition_admins_group not in user.groups.all():
        user.groups.add(competition_admins_group)
        user.is_staff = True
        user.save()
        return True
    return False


def make_competition_manager(user):
    """create or remove competition manager if the user is member or not of the 'competitions admins' group"""
    competition_admins_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

    competition_mangers_count = CompetitionManager.objects.filter(user=user).count()

    if competition_admins_group in user.groups.all() and competition_mangers_count == 0:
        # If user is in group and competition manager doesn't exist : create it
        CompetitionManager.objects.create(user=user)
        return True

    if competition_admins_group not in user.groups.all() and competition_mangers_count == 1:
        # If user is not in group and competition manager exists : delete it
        CompetitionManager.objects.filter(user=user).delete()
        return True

    return False


def on_post_save(sender, **kwargs):
    """signal called when an object is saved"""
    if sender is CompetitionManager:
        instance = kwargs['instance']
        make_competition_admin(instance.user)

post_save.connect(on_post_save)


def on_m2m_changed(sender, **kwargs):
    #print "*** on_m2m_changed", kwargs['action']
    if kwargs['model'] is Group and kwargs['action'] in ('post_add', 'post_remove', 'post_clear'):
        user = kwargs['instance']
        make_competition_manager(user)


m2m_changed.connect(on_m2m_changed)


def on_competition_manager_delete(sender, instance, **kwargs):
    """
    Remove the 'competition-admins' group on the user of a deleted CompetitionManager.
    """
    competition_admins_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)
    for user in User.objects.filter(groups=competition_admins_group):
        if not CompetitionManager.objects.filter(user=user):
            user.groups.remove(competition_admins_group)
            user.save()
post_delete.connect(on_competition_manager_delete, sender=CompetitionManager)


#-------------------------------------
#
#  Dynamic models
#
#-------------------------------------


def get_candidate_in_step_manager(step):
    """
    Gets a dynamic Manager class for a specific competition step
    """
    class _CandidateInStepManager(models.Manager):
        def get_query_set(self):
            return super(_CandidateInStepManager, self).get_query_set().filter(steps=step)
    return _CandidateInStepManager


def get_candidate_in_step_proxy_model_class(step):
    """
    Gets a dynamic Candidate (proxy) class for a specific competition step
    """
    class _CandidateProxy(Candidate):
        objects = get_candidate_in_step_manager(step)()
        class Meta:
            proxy = True
            managed = False
            verbose_name = _("candidate")

    # Force the permission
    content_type = ContentType.objects.get(app_label="ulysses_competitions", model="candidate")
    permission = Permission.objects.get_or_create(
        content_type=content_type, name="Can change _CandidateProxy", codename="change__candidateproxy"
    )[0]

    group = Group.objects.get(name="competition-admins")
    group.permissions.add(permission)
    group.save()

    return _CandidateProxy


def get_candidate_evaluation_manager(step, candidate):
    """
    Gets a dynamic manager class for the evaluations of a specific candidate in a specific step
    """
    class _CandidateEvaluationManager(models.Manager):
        def get_query_set(self):
            return super(_CandidateEvaluationManager, self).get_query_set().filter(
                competition_step=step, candidate=candidate
            )
    return _CandidateEvaluationManager


def get_candidate_evaluation_proxy_model_class(step, candidate):
    """
    Gets a dynamic proxy model for the evaluations of a specific candidate in a specific step
    """
    class _CandidateEvaluation(Evaluation):
        objects = get_candidate_evaluation_manager(step, candidate)()

        class Meta:
            proxy = True
            verbose_name = _("evaluation")
    return _CandidateEvaluation


def create_temporary_document(competition, composer, key, title, file, extra_files=None):
    # Remove existing temporary document, if applicable
    try:
        temp_doc = TemporaryDocument.objects.get(competition=competition, composer=composer, key=key)
    except TemporaryDocument.DoesNotExist:
        # Create new Temporary document
        temp_doc = TemporaryDocument(competition=competition, composer=composer, key=key)
    temp_doc.title = title
    temp_doc.file = copy_file_to_user_temporary_folder(composer, file)
    temp_doc.save()
    extra_files = extra_files or []
    temp_doc.get_extra_files().delete()  # remove existing extra_file
    for extra_file in extra_files:
        # recreate extra files
        temp_extra_file = TemporaryDocumentExtraFile(document=temp_doc)
        temp_extra_file.file = copy_file_to_user_temporary_folder(composer, extra_file.file)
        temp_extra_file.save()
    return temp_doc


def create_temporary_media(competition, composer, key, title, score, audio, video, notes, link):
    # Remove existing temporary work, if applicable
    if score:
        temp_media_score = copy_file_to_user_temporary_folder(composer, score)
    else:
        temp_media_score = ''

    if audio:
        temp_media_audio = copy_file_to_user_temporary_folder(composer, audio)
    else:
        temp_media_audio = ''

    if video:
        temp_media_video = copy_file_to_user_temporary_folder(composer, video)
    else:
        temp_media_video = ''

    try:
        temp_media = TemporaryMedia.objects.get(competition=competition, composer=composer, key=key)
    except TemporaryMedia.DoesNotExist:
        # Create new Temporary document
        temp_media = TemporaryMedia(competition=competition, composer=composer, key=key)
    # Create temporary work
    temp_media.title = title
    temp_media.score = temp_media_score
    temp_media.audio = temp_media_audio
    temp_media.video = temp_media_video
    temp_media.notes = notes
    temp_media.link = link
    temp_media.save()
    return temp_media


class DynamicApplicationForm(Sortable):
    name = models.CharField(max_length=255, unique=True)
    clean_method = models.CharField(
        max_length=255,
        blank=True,
        help_text=_(
            "Name of an optional clean method (located in ulysses.competitions.forms) to be called on form validation"
        )
    )

    def get_fieldsets(self):
        return ApplicationFormFieldSetDefinition.objects.filter(parent=self).order_by("order")

    def get_fields(self):
        return ApplicationFormFieldDefinition.objects.filter(parent__parent=self)

    def __str__(self):
        return self.name

    def duplicate(self):
        new_name = self.name + " (copy)"
        # Create another dynamic application form
        new_form = DynamicApplicationForm.objects.create(name=new_name, clean_method=self.clean_method)
        # Duplicate fieldset definitions
        for fieldset_definition in ApplicationFormFieldSetDefinition.objects.filter(parent=self):
            field_definitions = ApplicationFormFieldDefinition.objects.filter(parent=fieldset_definition)
            fieldset_definition.pk = None
            fieldset_definition.parent = new_form
            fieldset_definition.save()
            # Duplicate fields
            for field_definition in field_definitions:
                field_definition.pk = None
                field_definition.parent = fieldset_definition
                field_definition.save()

    class Meta(Sortable.Meta):
        verbose_name = "Dynamic application form"
        verbose_name_plural = "Dynamic application forms"


class ApplicationFormFieldSetDefinition(Sortable):
    parent = models.ForeignKey(DynamicApplicationForm, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, validators=[FieldNameValidator()])
    legend = models.CharField(max_length=255)
    description = models.TextField(blank=True)
    classes = models.CharField(max_length=255, blank=True, default="ProfileFieldset")

    def get_fields(self):
        return ApplicationFormFieldDefinition.objects.filter(parent=self)

    def __str__(self):
        result = ""
        if self.parent:
            result = "{0} > ".format(self.parent.name)
        result += self.name
        return result

    def clean(self):
        super(ApplicationFormFieldSetDefinition, self).clean()
        queryset = ApplicationFormFieldSetDefinition.objects.filter(name=self.name, parent=self.parent)
        if self.id:
            queryset = queryset.exclude(id=self.id)
        if queryset.count() > 0:
            raise ValidationError({NON_FIELD_ERRORS: [_('This field already exists'), ]})

    def select_inline(self):
        if self.id:
            return mark_safe(
                '<a href="{0}" class="select-inline"><img src="/static/img/arrow.png" /></a>'.format(self.id)
            )
        else:
            return mark_safe("&nbsp;")
    select_inline.short_description = ""

    class Meta(Sortable.Meta):
        verbose_name = "Application form fieldset"
        verbose_name_plural = "Application form fieldsets"


class ApplicationFormFieldType(models.Model):
    name = models.CharField(max_length=255)

    def __str__(self):
        return self.name


class ApplicationFormFieldDefinition(Sortable):

    VALIDATORS = [
        ('', ''),
        ('email', 'email'),
        ('url', 'URL'),
    ]

    parent = models.ForeignKey(ApplicationFormFieldSetDefinition, on_delete=models.CASCADE)
    kind = models.ForeignKey(ApplicationFormFieldType, on_delete=models.CASCADE)
    name = models.CharField(max_length=255, validators=[FieldNameValidator()])
    label = models.CharField(max_length=255)
    required = models.BooleanField(default=False)
    help_text = models.TextField(blank=True)
    competition_attribute = models.ForeignKey(CompetitionAttribute, null=True, blank=True, on_delete=models.CASCADE)
    is_visible_by_jury = models.BooleanField(default=True)
    grand_parent = models.ForeignKey(
        DynamicApplicationForm, null=True, blank=True, default=None, on_delete=models.CASCADE
    )
    maximum_length = models.IntegerField(default=0, blank=True, null=True)
    validator = models.CharField(default="", choices=VALIDATORS, blank=True, max_length=20)
    extra_files = models.IntegerField(default=0, help_text=_('Documents only. Number of allowed addition files'))

    def __str__(self):
        result = ""
        if self.parent:
            result = "{0} > ".format(self.parent)
        result += self.name
        return result

    def get_form_field(self, competition):

        kwargs = {
            'name': self.name,
            'label': self.label,
            'required': self.required,
            'help_text': self.help_text,
            'competition': competition,
        }

        if self.kind.name in ["CompetitionAttributeValueField", "CompetitionAttributeMultipleValueField"]:
            kwargs['competition_attribute'] = self.competition_attribute

        if self.kind.name in ["BiographicElementField", 'ApplicationHtmlElementField', 'ApplicationTextElementField']:
            kwargs['maximum_length'] = self.maximum_length

        if self.kind.name == "DocumentField":
            kwargs['extra_files'] = self.extra_files or 0

        # TODO : REFACTOR : no dynamic import
        module_name = "ulysses.competitions.fields"
        __import__(module_name)
        field_class = getattr(sys.modules[module_name], self.kind.name)
        return field_class(**kwargs)

    def clean(self):
        """Perform custom field validation on this model."""
        super(ApplicationFormFieldDefinition, self).clean()
        competition_attribute_fields = ["CompetitionAttributeValueField", "CompetitionAttributeMultipleValueField"]
        if self.kind.name in competition_attribute_fields and not self.competition_attribute:
            raise ValidationError(
                "`competition_attribute` field is mandatory for CompetitionAttributeValueField and "
                "CompetitionAttributeMultipleValueField"
            )
        queryset = ApplicationFormFieldDefinition.objects.filter(name=self.name, parent__parent=self.parent.parent)
        if self.id:
            queryset = queryset.exclude(id=self.id)
        if queryset.count() > 0:
            raise ValidationError({NON_FIELD_ERRORS: [_('This field already exists'), ]})

    def attribute_values(self):
        if self.competition_attribute:
            return [
                (attr_value.key, attr_value.value)
                for attr_value in CompetitionAttributeValue.objects.filter(attribute=self.competition_attribute)
            ]
        return []

    def save(self, *args, **kwargs):
        self.full_clean()
        self.grand_parent = self.parent.parent
        super(ApplicationFormFieldDefinition, self).save(*args, **kwargs)

    class Meta(Sortable.Meta):
        verbose_name = "Application form field"
        verbose_name_plural = "Application form fields"


class EvaluationType(models.Model):
    """A type of evaluation for competition"""
    name = models.CharField(max_length=100, verbose_name=_('name'))

    class Meta:
        verbose_name = _("Evaluation scale type")
        verbose_name_plural = _("Evaluation scale types")

    def __str__(self):
        return self.name


class Call(models.Model):
    """Store a request Call organization"""
    BOOL_CHOICES = (
        (1, _('Yes')),
        (0, _('No')),
    )

    requester = models.ForeignKey(User, verbose_name=_('requester'), on_delete=models.CASCADE)
    request_datetime = models.DateTimeField(verbose_name=_('request datetime'))
    processed = models.BooleanField(verbose_name=_('processed'), default=False)
    draft = models.BooleanField(verbose_name=_('draft'), default=False)

    name = models.CharField(max_length=100, verbose_name=_('name'))

    opening_date = models.DateField(verbose_name=_('opening date'), blank=True, null=True, default=None)
    closing_date = models.DateField(verbose_name=_('closing date'), blank=True, null=True, default=None)

    mandatory_fields = models.TextField(
        verbose_name=_('mandatory fields'), blank=True, default='',
        help_text=_('Please describe the mandatory fields to be filled by the candidate')
    )

    optional_fields = models.TextField(
        verbose_name=_('optional fields'), blank=True, default='',
        help_text=_('Please describe the optional fields to be filled by the candidate')
    )

    evaluation_type = models.ForeignKey(
        EvaluationType, blank=True, default=None, null=True, verbose_name=_('evaluation scale type'),
        on_delete=models.CASCADE
    )
    evaluation_steps = models.IntegerField(
        verbose_name=_('number of evaluation phases'), help_text=_('number...'), default=1
    )
    jury_history = models.IntegerField(
        choices=BOOL_CHOICES,
        default=0,
        verbose_name=_('Access past evaluations'),
        help_text=_(
            'Do you want the jury to be able to access previous evaluations of candidates ?'
        )
    )

    other_information = models.TextField(
        verbose_name=_('other information'), help_text=_('Any other useful information'), blank=True, default=''
    )

    class Meta:
        verbose_name = _("Call [OLD]")
        verbose_name_plural = _("Calls [OLD]")

    def __str__(self):
        return '{0} by {1}'.format(self.name, self.requester.email)


def calls_directory(*args):
    return DirectoryName(CALLS_DIRECTORY)(*args)


class CallRequest(models.Model):
    """Store a request of Call organization"""
    request_datetime = models.DateTimeField(verbose_name=_('request datetime'))
    processed = models.BooleanField(verbose_name=_('processed'), default=False)
    first_name = models.CharField(max_length=100, verbose_name=_('First name'))
    last_name = models.CharField(max_length=100, verbose_name=_('Last name'))
    organization_name = models.CharField(max_length=100, verbose_name=_('Name of your organization'))
    country = models.CharField(max_length=100, verbose_name=_('Country'))
    email = models.EmailField(verbose_name=_('Email'))
    call_description = models.TextField(verbose_name=_('Call description'))
    picture = models.ImageField(
        verbose_name=_('Call picture'), upload_to=calls_directory, blank=True, default=None, null=True, max_length=200,
        help_text=''
    )

    class Meta:
        verbose_name = _("Call request")
        verbose_name_plural = _("Call requests")

    def __str__(self):
        return '{0} by {1}'.format(self.organization_name, self.email)
