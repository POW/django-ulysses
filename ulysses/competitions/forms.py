# -*- coding: utf-8 -*-

import os.path

from django.conf import settings
from django.contrib.admin.forms import AdminAuthenticationForm
from django.contrib.auth.models import User
from django.urls import reverse_lazy
from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.utils import make_string_storable_in_dict, generate_username
from ulysses.generic.forms import BetterForm, BaseModelForm, FileUploadForm
from ulysses.generic.widgets import FileUploadControl
from ulysses.profiles.utils import get_profile_from_email

from .fields import (
    BiographicElementField, DocumentField, MediaField, ApplicationElementFieldBase, ApplicationTextElementField,
    ApplicationHtmlElementField, ApplicationBooleanElementField, CompetitionAttributeMultipleValueField,
    CompetitionAttributeValueField, AudioField, VideoField, ScoreAudioField, ScoreVideoField, YouTubeField,
    VideoYouTubeField,
)
from .models import (
    CompetitionManager, CompetitionAttribute, CompetitionStep, CandidateDocument, CandidateMedia,
    CandidateBiographicElement, DraftBiographicElement, DraftDocument, DraftMedia, CallRequest,
    get_candidate_folder
)
from .widgets import ApplicationHtmlElementWidget

# required when called in eval?
from ulysses.composers.models import BiographicElement, Document, Media
from .models import JuryMember, Evaluation, EvaluationStatus
from .models import Candidate, ApplicationElement
from .models import TemporaryMedia, TemporaryDocument
# from .models import create_temporary_document


class CallRequestForm(FileUploadForm):

    class Meta:
        model = CallRequest
        fields = (
            'first_name', 'last_name', 'organization_name', 'country', 'email', 'call_description', 'picture',
        )
        upload_fields = ('picture',)
        upload_fields_args = {
            'picture': {
                'extensions': 'png|jpg', 'label': _("Photo"), 'upload_url_name': 'competitions:upload_call_file'
            },
        }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['picture_media'].required = True


def get_candidate_admin_form(candidate, data=None):
    if data:
        form = candidate.competition.get_application_form_instance(data=data, is_jury_member=False)
    else:
        initial_data = candidate.get_application_data()
        form = candidate.competition.get_application_form_instance(
            initial=initial_data, is_jury_member=False
        )
    form.admin_mode = True
    return form


def get_candidate_jury_form(candidate):
    application_data = candidate.get_application_data()
    form = candidate.competition.get_application_form_instance(
        initial=application_data,
        is_jury_member=True
    )
    form.jury_mode = True
    return form


def clean_sampleform(host):
    """ A sample clean method  """
    cleaned_data = host.cleaned_data

    raise forms.ValidationError("You called clean_sampleform method")

    # Always return the full collection of cleaned data.
    return cleaned_data


class CompetitionAdminAuthenticationForm(AdminAuthenticationForm):
    """
    A custom authentication form used in the competion admin app.

    """

    def clean(self):
        # Call base class first
        super(CompetitionAdminAuthenticationForm,self).clean()
        # no need to go further if the form is already invalid
        if self._errors:
            return

        # Do extra check
        logged_user = self.user_cache
        check = False
        if logged_user.is_superuser:
            check = True
        else:
            if CompetitionManager.objects.filter(user=logged_user).exists():
                check = True

        if not check:
            raise forms.ValidationError(
                _("Only competition managers and super-users can connect to this administration site")
            )


class CandidateNotificationForm(forms.Form):
    HELP_TEXT = '''
    You can use the following token in your email:
    [[ first_name ]] : first name of the candidate,
    [[ last_name ]] : last name of the candidate,
    [[ Competitionname ]] : name of the competition,
    [[ 'step'.comments ]] : the comments. (Replace 'step' by the key-name of the step),
    [[ 'step'.notes ]] : the notes for an evaluation field. (Replace 'field' by the key-name of the field),
    [[ 'step'.average ]] : the average note. The notes must be numeric values
    '''

    from_email = forms.EmailField(label=_("From email"), widget=forms.TextInput(attrs={'size': '80'}))
    subject = forms.CharField(label=_("Subject"), widget=forms.TextInput(attrs={'size': '80'}))
    body = forms.CharField(widget=forms.Textarea(attrs={'cols': 80, 'rows': 20}), help_text=HELP_TEXT)
    cc_me = forms.BooleanField(label="I want to be cced of all notifications sent", required=False)


class JuryMemberNotificationForm(forms.Form):
    from_email = forms.EmailField(label=_("From email"), widget=forms.TextInput(attrs={'size': '80'}))
    subject = forms.CharField(label=_("Subject"), widget=forms.TextInput(attrs={'size': '80'}))
    body = forms.CharField(widget=forms.Textarea(attrs={'cols': 80}))
    cc_me = forms.BooleanField(label="I want to be cced of all notifications sent", required=False)


class JuryMemberEditForm(forms.Form):
    first_name = forms.CharField(label=_("First name"))
    last_name = forms.CharField(label=_("Last name"))
    email = forms.EmailField(label=_("Email"))


def get_candidate_document_key_validator(candidate):
    def _clean_key(host):
        key = host.cleaned_data['key']
        if CandidateDocument.objects.filter(candidate=candidate, key=key).exists():
            raise forms.ValidationError(_("A document with the key '%s' already exist for this candidate" % key))
        return key
    return _clean_key


def get_candidate_document_modelform(candidate, field_definition):
    extra_files = []
    class _CandidateDocumentForm(forms.ModelForm):
        file = forms.CharField(
            label="file",
            help_text=_('Supported formats: pdf, jpg. 100MB max.'),
            widget=FileUploadControl(
                upload_url=reverse_lazy('upload_from_superadmin', args=[candidate.id]),
                allowed_extensions='pdf|jpg',
                no_help=True
            )
        )

        class Meta:
            model = CandidateDocument
            fields = ('title', 'file')

        def get_target(self):
            target_dir = get_candidate_folder(candidate)
            return target_dir.replace(settings.MEDIA_ROOT, '')

        def get_name(self, file):
            return os.path.split(file)[-1]

        def __init__(self, data=None, *args, **kwargs):
            self._data = data
            instance = kwargs.pop('instance', None)
            super(_CandidateDocumentForm, self).__init__(data, *args, **kwargs)
            if instance:
                self.fields['file'].initial = self.get_name(instance.file)
                self.fields['title'].initial = instance.title
                self.instance = instance
                # If No value -> Delete the document
                self.fields['file'].required = False
                self.fields['file'].help_text += ' If empty, the document will be deleted'

            self.extra_files = list(instance.get_extra_files()) if instance else []

            for index in range(field_definition.extra_files):
                field_name = 'extra_file_{0}'.format(index)
                initial = ''
                if index < len(self.extra_files):
                    initial = self.get_name(self.extra_files[index].file)
                self.fields[field_name] = forms.CharField(
                    label="additional file {0}".format(index + 1),
                    help_text=_('Supported formats: pdf, jpg. 100MB max.'),
                    initial=initial,
                    required=False,
                    widget=FileUploadControl(
                        upload_url=reverse_lazy('upload_from_superadmin', args=[candidate.id]),
                        allowed_extensions='pdf|jpg',
                        no_help=True
                    )
                )

        def clean_file(self):
            is_new_file = self._data.get('file_path', '')
            if is_new_file:
                # A new file has been uploaded
                return os.path.join(self.get_target(), self.cleaned_data['file'])
            else:
                if self.cleaned_data['file']:
                    # No change was made (no new file uploaded)
                    return self.instance.file
                else:
                    # The file has been cleared
                    return ''

        def clean(self):
            for index in range(field_definition.extra_files):
                field_name = 'extra_file_{0}'.format(index)
                field_name_path = 'extra_file_{0}_path'.format(index)
                is_new_file = self._data.get(field_name_path, '')
                extra_file = self.extra_files[index] if index < len(self.extra_files) else None
                if is_new_file:
                    self.cleaned_data[field_name] = os.path.join(self.get_target(), self.cleaned_data[field_name])
                else:
                    value = self.cleaned_data[field_name]
                    if value:
                        # No change was made (no new file uploaded)
                        self.cleaned_data[field_name] = extra_file.file if extra_file else ''
                    else:
                        # The file has been cleared
                        self.cleaned_data[field_name] = ''

    return _CandidateDocumentForm


def get_candidate_biographic_element_modelform(candidate, edit_mode=False):
    application_form = candidate.competition.get_application_form_instance()
    form_choices = [('', '----------')]
    form_choices.extend([(key, key) for key in application_form.get_biographic_element_keys()])

    class _CandidateBiographicElementForm(forms.ModelForm):
        text = forms.CharField(
            label="text", widget=ApplicationHtmlElementWidget(), required=False
        )

        class Meta:
            model = CandidateBiographicElement
            fields = ('title', 'text', )

    # if not edit_mode:
    #     setattr(_CandidateBiographicElementForm, "clean_key", get_candidate_document_key_validator(candidate))

    return _CandidateBiographicElementForm


def patch_media_fields(form_fields, field_type):

    youtube_help = _(
        'Make sure you were able to read the video on YouTube first, and that your video is public or unlisted.'
    )

    if field_type == 'VideoField':
        form_fields['audio'].widget = forms.HiddenInput()
        form_fields['score'].widget = forms.HiddenInput()
        form_fields['notes'].widget = forms.HiddenInput()
        form_fields['link'].widget = forms.HiddenInput()
        form_fields['video'].required = True

    if field_type == 'AudioField':
        form_fields['video'].widget = forms.HiddenInput()
        form_fields['score'].widget = forms.HiddenInput()
        form_fields['notes'].widget = forms.HiddenInput()
        form_fields['link'].widget = forms.HiddenInput()
        form_fields['audio'].required = True

    if field_type == 'ScoreVideoField':
        form_fields['audio'].widget = forms.HiddenInput()
        form_fields['link'].widget = forms.HiddenInput()
        form_fields['video'].required = True
        form_fields['score'].required = True

    if field_type == 'ScoreAudioField':
        form_fields['video'].widget = forms.HiddenInput()
        form_fields['link'].widget = forms.HiddenInput()
        form_fields['audio'].required = True
        form_fields['score'].required = True

    if field_type == 'YouTubeField':
        form_fields['audio'].widget = forms.HiddenInput()
        form_fields['score'].widget = forms.HiddenInput()
        form_fields['video'].widget = forms.HiddenInput()
        form_fields['notes'].widget = forms.HiddenInput()
        form_fields['link'].required = True
        form_fields['link'].help_text = youtube_help
        form_fields['link'].label = "YouTube link"

    if field_type == 'VideoYouTubeField':
        form_fields['audio'].widget = forms.HiddenInput()
        form_fields['score'].widget = forms.HiddenInput()
        form_fields['notes'].widget = forms.HiddenInput()
        form_fields['link'].help_text = youtube_help
        form_fields['link'].label = "YouTube link"


def get_candidate_media_modelform(candidate, edit_mode=False):

    class _CandidateMediaForm(forms.ModelForm):
        score = forms.CharField(
            label="score",
            required=False,
            help_text=_('Supported format: pdf. 100MB max.'),
            widget=FileUploadControl(
                upload_url=reverse_lazy('upload_from_superadmin', args=[candidate.id]),
                allowed_extensions='pdf',
                no_help=True
            )
        )
        audio = forms.CharField(
            label="audio",
            required=False,
            help_text=_('Supported format: mp3 (bitrate ≥ 256Kbps). 100MB max.'),
            widget=FileUploadControl(
                upload_url=reverse_lazy('upload_from_superadmin', args=[candidate.id]),
                allowed_extensions='mp3',
                no_help=True
            )
        )
        video = forms.CharField(
            label="video",
            required=False,
            help_text=_(
                'Supported formats: mp4 (H.264 video codec with AAC or MP3 stereo audio codec) '
                'or WebM (VP8 video codec with Vorbis audio codec). 100MB max.'
            ),
            widget=FileUploadControl(
                upload_url=reverse_lazy('upload_from_superadmin', args=[candidate.id]),
                allowed_extensions='mp4|webm',
                no_help=True
            )
        )

        class Meta:
            model = CandidateMedia
            fields = ('title', 'score', 'audio', 'video', 'link', 'notes')

        def get_target(self):
            target_dir = get_candidate_folder(candidate)
            return target_dir.replace(settings.MEDIA_ROOT, '')

        def get_name(self, file):
            if file:
                return os.path.split(file)[-1]
            return ''

        def __init__(self, data=None, *args, **kwargs):
            instance = kwargs.pop('instance', None)
            super(_CandidateMediaForm, self).__init__(data, *args, **kwargs)
            self._data = data
            if instance:
                self.fields['score'].initial = self.get_name(instance.score)
                self.fields['audio'].initial = self.get_name(instance.audio)
                self.fields['video'].initial = self.get_name(instance.video)
                self.fields['title'].initial = instance.title
                self.fields['link'].initial = instance.link
                self.fields['notes'].initial = instance.notes
                self.instance = instance
                self.fields['title'].required = False  # if no title then the media must be deleted
                self.fields['title'].help_text = _('If blank, the media will be deleted')

        def _clean_file(self, key):
            file = self.cleaned_data.get(key, '')
            if file:
                is_new_file = self._data.get(key + '_path', '')
                if is_new_file:
                    target = os.path.join(self.get_target(), file)
                    return target
                else:
                    origin_file = getattr(self.instance, key, '')
                    return origin_file
            return file

        def clean_score(self):
            return self._clean_file('score')

        def clean_audio(self):
            return self._clean_file('audio')

        def clean_video(self):
            return self._clean_file('video')

    return _CandidateMediaForm


class CreateJuryMemberForm(forms.Form):
    first_name = forms.CharField(label=_("First name"))
    last_name = forms.CharField(label=_("Last name"))
    email = forms.EmailField(label=_("Email"), required=True)
    username = forms.CharField(label=_("Login"), required=False)
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput())
    password2 = forms.CharField(label=_("Password (again)"), widget=forms.PasswordInput())

    def __init__(self, *args, **kwargs):
        super(CreateJuryMemberForm, self).__init__(*args, **kwargs)
        self._profile_email = False
        self.fields['username'].required = False
        self.fields['username'].widget = forms.HiddenInput()

    def clean_email(self):
        email = self.cleaned_data['email']

        if User.objects.filter(email=email).exists():
            self._profile_email = email
            raise forms.ValidationError(_("A user with the same email already exists"))

        return email

    def clean(self):
        # Call base class first
        super(CreateJuryMemberForm, self).clean()
        # no need to go further if the form is already invalid
        if self._errors:
            return

        cleaned_data = self.cleaned_data

        email = self.cleaned_data['email']
        username = generate_username(email)

        if User.objects.filter(username=username).exists():
            raise forms.ValidationError(_("A user with the same username already exists"))

        cleaned_data['username'] = username

        if User.objects.filter(last_name=cleaned_data["last_name"], first_name=cleaned_data["first_name"], ).exists():
            raise forms.ValidationError(_("A user with the same name already exists"))

        if cleaned_data["password"] != cleaned_data["password2"]:
            raise forms.ValidationError(_("Passwords don't match"))

        return cleaned_data

    def get_profile_email(self):
        return self._profile_email


class MakeJuryMemberForm(forms.Form):
    profile_email = forms.EmailField(label=_("Email"))

    def __init__(self, *args, **kwargs):
        super(MakeJuryMemberForm, self).__init__(*args, **kwargs)
        self.fields['profile_email'].widget.attrs['readonly'] = 'readonly'
        self.fields['profile_email'].widget.attrs['style'] = 'width: 100%; border: none; background: #fff;'

    def clean_profile_email(self):
        email = self.cleaned_data['profile_email']
        profile = get_profile_from_email(email)
        if profile is None:
            raise forms.ValidationError(_("The email doesn't correspond to any profile"))
        return profile


class CandidateAdminForm(BetterForm):
    is_valid = forms.BooleanField(required=False)
    notes = forms.CharField(
        widget=forms.Textarea(attrs={'cols': 80}),
        required=False,
        help_text="Nota : this text area is shared by all competition managers for this candidate"
    )

    class Meta:
        fieldsets = [
            ('administration_notes', {'fields': ['is_valid', 'notes'], 'legend': '', 'classes': ['ProfileFieldset']}, )
        ]


def get_competition_attribute_validator(name):
    """
    Gets a dynamic validator method for competition attribute
    """
    def _clean_field(host):
        data = host.cleaned_data[name]
        if data == "0":
            raise forms.ValidationError(_("This field is required"))
        # Always return the cleaned data, whether you have changed it or not
        return data
    return _clean_field


def get_checkbox_validator(name):
    """
    Gets a dynamic validator method for check box

    If the value is u'False' => validation fails
    In other cases => validation succeeds
    """
    def _clean_field(host):
        data = host.cleaned_data[name]
        if data == "False":
            raise forms.ValidationError(_("This field is required"))
        # Always return the cleaned data, whether you have changed it or not
        return data
    return _clean_field


class ApplicationForm(BetterForm):
    """
    Generic application form
    Nota : you should derive a class from this form to define a competition specific application form
    """

    def get_field_definition_from_name(self, name, allow_missing=False):
        for field in self.field_definitions:
            if field.name == name:
                return field
        if allow_missing:
            return None
        raise RuntimeError("No field definition found for name '%s'" % name)

    def __init__(self, competition, data=None, *args, **kwargs):
        super(ApplicationForm, self).__init__(data, *args, **kwargs)
        self._preview = False
        self._admin_mode = False
        self._jury_mode = False
        self.competition = competition
        self.field_definitions = self.get_field_definitions()
        self.initialize_fields_from_field_definitions()
        self.label_suffix = ""

    def get_field_definitions(self):
        """
        This method should be overridden in derived classes and return a list of field definitions
        (i.e a list of objects derived from ApplicationField)
        """
        raise RuntimeError("Should be override in derived class")

    def get_field_keys(self, field_type):
        return [field_def.name for field_def in self.field_definitions if isinstance(field_def, field_type)]

    def get_document_keys(self):
        return self.get_field_keys(DocumentField)

    def get_biographic_element_keys(self):
        return self.get_field_keys(BiographicElementField)

    def get_media_keys(self):
        return self.get_field_keys(MediaField)

    def get_attribute_value_keys(self):
        return self.get_field_keys(CompetitionAttributeValueField)

    def get_attribute_multiple_value_keys(self):
        return self.get_field_keys(CompetitionAttributeMultipleValueField)

    def get_elements_keys(self):
        return self.get_field_keys(ApplicationElementFieldBase)

    def get_preview(self):
        return self._preview

    def set_preview(self, value):
        self._preview = value
        if self._preview:
            for field_def in self.field_definitions:
                field = self.fields[field_def.name]
                field.widget = getattr(field_def, "preview_widget")

    def get_admin_mode(self):
        return self._admin_mode

    def set_admin_mode(self, value):
        self._admin_mode = value
        if self._admin_mode:
            # Set 'admin' widgets
            for field_definition in self.field_definitions:
                field = self.fields[field_definition.name]
                field.widget = getattr(field_definition, "admin_widget")

    def get_jury_mode(self):
        return self._jury_mode

    def set_jury_mode(self, value):
        self._jury_mode = value
        if self._jury_mode:
            # Set 'preview' widgets for document fields to make edition impossible
            for field_definition in self.field_definitions:
                field = self.fields[field_definition.name]
                field.widget = getattr(field_definition, "jury_widget")

    preview = property(get_preview, set_preview)
    admin_mode = property(get_admin_mode, set_admin_mode)
    jury_mode = property(get_jury_mode, set_jury_mode)

    def get_hidden_key(self,name):
        return name + "_hidden"

    def get_non_media_fieldsets(self, fields_to_exclude=None):
        """
        Returns the fieldsets of the form containing no media
        fields_to_exclude: a list of fields key to exclude
        """
        if fields_to_exclude is None:
            fields_to_exclude = []

        results = []
        for fieldset in self.fieldsets:
            is_non_media_fieldset = True
            for field in fieldset:
                if not field.name.endswith("_hidden"):
                    field_definition = self.get_field_definition_from_name(field.name)
                    if isinstance(field_definition, MediaField):
                        is_non_media_fieldset = False
                        break
            if is_non_media_fieldset:
                # Remove fields to exclude, if applicable
                new_fields = []
                for field in fieldset.boundfields:
                    if field.name not in fields_to_exclude:
                        new_fields.append(field)
                if len(new_fields) > 0:
                    fieldset.boundfields = new_fields
                    results.append(fieldset)
        return results

    def get_media_fields(self):
        """
        Returns the media fields for this form, as a list of (field_name, field_label)
        """
        results = []
        for fieldset in self.fieldsets:
            at_least_one_media = False
            for field in fieldset:
                if not field.name.endswith("_hidden"):
                    field_definition = self.get_field_definition_from_name(field.name)
                    if isinstance(field_definition, MediaField):
                        at_least_one_media = True
                        break
            if at_least_one_media:
                for field in fieldset:
                    if not field.name.endswith("_hidden"):
                        field_definition = self.get_field_definition_from_name(field.name)
                        if isinstance(field_definition, MediaField) or isinstance(field_definition, DocumentField):
                            results.append((field.name, field.label, isinstance(field_definition, MediaField)))
        return results

    def initialize_fields_from_field_definitions(self):
        widget_key = "widget"

        if self.preview:
            widget_key = "preview_widget"

        for field_definition in self.field_definitions:
            # Create main field
            self.fields[field_definition.name] = forms.CharField(
                label=field_definition.label,
                required=field_definition.required,
                widget=getattr(field_definition, widget_key),
                help_text=field_definition.help_text
            )
            attrs = self.fields[field_definition.name].widget.attrs or {}
            if 'class' in attrs:
                if 'form-control' not in attrs['class']:
                    attrs['class'] += ' form-control'
            else:
                attrs['class'] = ' form-control'
            self.fields[field_definition.name].widget.attrs = attrs

            # If applicable, create companion hidden field
            if field_definition.has_model():
                self.fields[self.get_hidden_key(field_definition.name)] = forms.IntegerField(
                    label=self.get_hidden_key(field_definition.name),
                    required=field_definition.required,
                    widget=forms.HiddenInput(
                        attrs={'class': 'HiddenCompanion', 'model': field_definition.Meta.model.__name__}
                    )
                )

            # For CompetitionAttributeValueField, add dynamic validators
            # We need to do that since, even if no value in selected in the combo-box,
            # Django considers that a value has been selected (value = "0")
            if type(field_definition) is CompetitionAttributeValueField:
                if field_definition.required:
                    field_name = field_definition.name
                    setattr(ApplicationForm, "clean_%s" % field_name, get_competition_attribute_validator(field_name))

            # We add dynamic validator for boolean fields
            # An unchecked box sends a value of u'False'
            # which is considered by an existing value by Django
            # but means 'missing' for us...
            # This behaviour is strange...anyway !
            # To be improved in a future refactoring ?
            if type(field_definition) is ApplicationBooleanElementField:
                if field_definition.required:
                    field_name = field_definition.name
                    setattr(ApplicationForm, "clean_%s" % field_name, get_checkbox_validator(field_name))

    def get_form_data_from_draft(self, draft):
        draft_data_dict = eval(draft.data)
        form_data = {}
        for name in list(draft_data_dict.keys()):
            field_definition = self.get_field_definition_from_name(name)
            if field_definition.has_model():
                key = draft_data_dict[name]
                # We check the existence of the corresponding element (document, media...)
                # and load it into the draft only if it exists
                # If the element is missing (i.e has been deleted since draft was saved),
                # loading it in the draft would raise an exception
                model_class = getattr(field_definition.Meta, "model")
                if eval("%s.objects.filter(id=%s).exists()" % (model_class.__name__, key)):
                    form_data[self.get_hidden_key(name)] = key
                    form_data[name] = make_string_storable_in_dict(
                        field_definition.Meta.model.objects.get(id=key).get_html()
                    )
            else:
                # MONKEY-PATCH BUG FIX - TO BE IMPROVED
                # In case of multiple values stored in the draft (ex: topic areas for the Residence),
                # the draft value is stored has a string : "[u'10', u'11']"
                # If we want the CheckBoxSelectMultiple to work correctly, we have to convert
                # this string to a list...
                #
                # To be improved : we must stored differently the value in the draft
                # and/or find a generic way to handle this case...
                if draft.competition.url == "residency" and name == "topic_areas":
                    form_data[name] = eval(draft_data_dict[name])
                else:
                    form_data[name] = draft_data_dict[name]

        # Process draft biographic elemnets, if any
        for item in DraftBiographicElement.objects.filter(draft=draft):
            form_data[item.key] = item.text

        # Process draft documents, if any
        for item in DraftDocument.objects.filter(draft=draft):
            # Create a temporary document
            doc = item.copy_as_temporary_element()
            hidden_key = "%s_hidden" % item.key
            form_data[hidden_key] = doc.id
            form_data[item.key] = doc.id

        # Process draft media, if any
        for item in DraftMedia.objects.filter(draft=draft):
            # Create a temporary media
            media = item.copy_as_temporary_element()
            hidden_key = "%s_hidden" % item.key
            form_data[hidden_key] = media.id
            form_data[item.key] = media.id
        return form_data

    def get_form_data_from_candidate(self, application_form, candidate):
        form_data = {}
        for field in application_form:
            field_definition = application_form.get_field_definition_from_name(field.name, allow_missing=True)
            if field_definition is not None:
                form_data[field.name] = field_definition.get_value_for_candidate(candidate, False)
        return form_data

    def get_excel_data_from_candidate(self, application_form, candidate):
        form_data = {}
        for field in application_form:
            field_definition = application_form.get_field_definition_from_name(field.name, allow_missing=True)
            if field_definition is not None:
                form_data[field.name] = field_definition.get_excel_value_for_candidate(candidate)
        return form_data

    def ensure_not_duplicate_elements(self,cleaned_data,keys,element_type):
        val_list = []
        for key in keys:
            if key in cleaned_data:
                val = cleaned_data.get(key)
                if val:
                    if val in val_list:
                       raise forms.ValidationError("You provided the same %s for two different fields" % element_type)
                    else:
                        val_list.append(val)

    def clean(self):
        cleaned_data = self.cleaned_data

        # Ensure not duplicate elements
        self.ensure_not_duplicate_elements(cleaned_data, self.get_document_keys(), "document")
        self.ensure_not_duplicate_elements(cleaned_data, self.get_media_keys(), "work")

        # Always return the full collection of cleaned data.
        return cleaned_data


NEWFORUM_COMPOSERS_ENSEMBLE_DESCRIPTION = "Select below ensembles in order of preference, depending on your project. \
    Details of these ensembles are given on the presentation page of the competition"


class NewForumJeuneCreationApplicationForm(ApplicationForm):

    def clean(self):
        cleaned_data = self.cleaned_data
        # Validate workshops
        workshop1 = None
        workshop2 = None
        workshop3 = None
        workshop1 = self.cleaned_data.get('workshop1')
        workshop2 = self.cleaned_data.get('workshop2')
        workshop3 = self.cleaned_data.get('workshop3')
        workshop_values = []
        duplicate_workshops = False
        if workshop1 and int(workshop1)!=0:
            workshop_values.append(workshop1)
        if workshop2 and int(workshop2)!=0:
            if workshop2 not in workshop_values:
                workshop_values.append(workshop2)
            else:
                duplicate_workshops = True
        if workshop3 and int(workshop3)!=0:
            if workshop3 not in workshop_values:
                workshop_values.append(workshop3)
            else:
                duplicate_workshops = True
        if duplicate_workshops:
            raise forms.ValidationError("You have selected several times the same workshop. Please select distinct workshops.")
        # Always return the full collection of cleaned data.
        return cleaned_data

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="languages") # anglais / francais
        field_definitions.append(
            CompetitionAttributeValueField(
                self.competition, languages_attribute, "contact_language", "Language", True,
                "Please select your preferred communication language"
            )
        )
        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=False))
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(DocumentField(self.competition, "cv", _("CV")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="ensembles") # ensemble mosaik, Champ d'Action, Ensemble Orchestral Contemporain
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop1",
                                                                "First choice",True,
                                                                "Please select your preferred ensemble (mandatory)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop2",
                                                                "Second choice",False,
                                                                "Please select your second choice ensemble (optional)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop3",
                                                                "Third choice",False,
                                                                "Please select your third choice ensemble (optional)"))
        # Projet Draft
        field_definitions.append(DocumentField(self.competition, "project_draft",_("Draft of the project"),required=True))
        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommandation letter (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommandation letter (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("Work #1")))
        field_definitions.append(MediaField(self.competition, "media2",_("Work #2")))
        field_definitions.append(MediaField(self.competition, "media3",_("Work #3")))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree to take part in public performances,\
                                                                       radio and television broadcast without payment"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('workshops', {'fields': ['workshop1','workshop2','workshop3'],
                'description' : NEWFORUM_COMPOSERS_ENSEMBLE_DESCRIPTION,
                'legend': 'Workshops' , 'classes': ['ProfileFieldset'] }),
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden'],
                'legend': 'Personal informations' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience',
                                                 'prices', 'cv','cv_hidden'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('project_draft', {'fields': ['project_draft','project_draft_hidden',
                                          ],
                    'legend': 'Draft of the project' ,
                    'classes': ['ProfileFieldset'] }),
            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden',
                                   ],
                'legend': 'Works' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description': "If applicable, join additional recommandation letters or other documents (optional)",
                'classes': ['ProfileFieldset'] }),
            ('agreement', {'fields': ['agreement'], 'legend': 'Agreement' , 'classes': ['ProfileFieldset'] }),
        ]


class IrcamCursus1ApplicationForm(ApplicationForm):
    """used for unit-tests"""

    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education", _("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience", _("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices", _("Prizes & awards")))
        # Documents
        field_definitions.append(DocumentField(self.competition, "official_id", _("ID")))
        field_definitions.append(DocumentField(self.competition, "tef",_("TEF"), required=False))
        field_definitions.append(DocumentField(self.competition, "motivation_letter", _("Motivation letter")))
        field_definitions.append(DocumentField(
            self.competition, "other_document1", _("Other document (#1)"), required=False)
        )
        field_definitions.append(DocumentField(
            self.competition, "other_document2", _("Other document (#2)"), required=False)
        )
        # Works
        field_definitions.append(MediaField(self.competition, "work1", _("Work 1")))
        field_definitions.append(MediaField(self.competition, "work2", _("Work 2")))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            (
                'biographic_elements',
                {
                    'fields': ['education', 'experience', 'prices'],
                    'legend': 'Biographic details',
                    'classes': ['ProfileFieldset']
                }
            ),
            (
                'documents',
                {
                    'fields': [
                        'official_id','official_id_hidden','tef','tef_hidden', 'motivation_letter',
                        'motivation_letter_hidden', 'other_document1', 'other_document1_hidden', 'other_document2',
                        'other_document2_hidden'
                    ],
                    'legend': 'Documents',
                    'classes': ['ProfileFieldset'],
                    'description': 'Please upload the following documents : A photocopy of your legal ID\
                        , a motivation letter and a proof of TEF results (French Evaluation Level Test)\
                        for whom French is not the native language). The minimum required TEF score is level 3\
                        or higher/B1. Recommendation letters, and other documents you would like\
                        to value can be added in the optional fields.'
                }
            ),
            (
                'medias',
                {
                    'fields': ['work1', 'work1_hidden', 'work2', 'work2_hidden'],
                    'legend': 'Work samples',
                    'classes': ['ProfileFieldset']
                }
            ),
        ]


class IrcamCursus2ApplicationForm(ApplicationForm):
    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        # Documents
        field_definitions.append(DocumentField(self.competition, "official_id",_("ID")))
        field_definitions.append(DocumentField(self.competition, "compositionalproject",_("Compositional project")))
        field_definitions.append(DocumentField(self.competition, "motivation_letter",_("Motivation letter")))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Works
        field_definitions.append(MediaField(self.competition, "work1",_("Work 1")))
        field_definitions.append(MediaField(self.competition, "work2",_("Work 2")))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements', {'fields': ['education', 'experience',
                                                 'prices'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['official_id','official_id_hidden','compositionalproject','compositionalproject_hidden',
                                      'motivation_letter','motivation_letter_hidden', 'other_document1',
                                      'other_document1_hidden', 'other_document2', 'other_document2_hidden'],
                'legend': 'Documents' , 'classes': ['ProfileFieldset'],
                    'description' : 'Please upload the following documents : A photocopy of your legal ID\
                    , a motivation letter and a proof of TEF results (French Evaluation Level Test)\
                    for whom French is not the native language). The minimum required TEF score is level 3\
                    or higher/B1. Recommendation letters, and other documents you would like\
                    to value can be added in the optional fields.'}),
            ('medias', {'fields': ['work1','work1_hidden','work2','work2_hidden'],
                'legend': 'Work samples' , 'classes': ['ProfileFieldset']}),
            ]


ACANTHES_COMPOSERS_MEDIA_DESCRIPTION = "Please join the right works relating to your first choice and second choice (3 or 6 works) :\
                             <ul style='list-style-type:circle;padding-left:20px;'><li><b>composition workshop for string quartett</b> : 3 works, at least one of which for chamber music\
                             (and at least 2 scores and 1 recording among the 3 works)</li>\
                             <li><b>composition workshop for ensemble/orchestra</b> : 3 works, at least one of which\
                             for big ensemble (and at least 2 scores and 1 recording among the 3 works)</li>\
                             <li><b>composition workshop electronic pieces for 'In Vivo Danse'</b>  : 3 works,\
                             at least one of which with electronics (and at least 1 instrumental score and 2 recordings among the 3 works)</li>\
                             <li><b>composition workshop for 12 voices men's choir</b> : 3 works, at least two of which for choir\
                             or vocal-ensemble or voice(s) and instruments (and at least 2 scores and 2 recordings among the 3 works)</li>\
                             </ul>"

ACANTHES_PERFORMERS_MEDIA_DESCRIPTION = "Please join the right files relating to your choice:\
                             <ul style='list-style-type:circle;padding-left:20px;'>\
                             <li><b>Cello master-class</b> : 2 recordings, one from classical and one from contemporary repertoires</li>\
                             <li><b>Piano master-class</b> : 2 recordings, one from classical and one from contemporary repertoires</li>\
                             <li><b>Lieder master-class</b> : 2 recordings, one from classical and one from modern or contemporary repertoires</li>\
                             <li><b>New Technologies Workshop for Musical Creation</b> : 3 works,\
                             at least one of which is a piece with electronics (and at least 1 score and 2 recording among the 3 works)</li></ul>"


class AcanthesComposersApplicationForm(ApplicationForm):

    def clean(self):
        cleaned_data = self.cleaned_data

        # Validate workshops
        workshop1 = None
        workshop2 = None
        workshop3 = None

        workshop1 = self.cleaned_data.get('workshop1')
        workshop2 = self.cleaned_data.get('workshop2')
        workshop3 = self.cleaned_data.get('workshop3')

        workshop_values = []

        duplicate_workshops = False

        if workshop1 and int(workshop1)!=0:
            workshop_values.append(workshop1)
        if workshop2 and int(workshop2)!=0:
            if workshop2 not in workshop_values:
                workshop_values.append(workshop2)
            else:
                duplicate_workshops = True
        if workshop3 and int(workshop3)!=0:
            if workshop3 not in workshop_values:
                workshop_values.append(workshop3)
            else:
                duplicate_workshops = True

        if duplicate_workshops:
            raise forms.ValidationError("You have selected several times the same workshop. Please select distinct workshops.")

        # Always return the full collection of cleaned data.
        return cleaned_data

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=False))
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(DocumentField(self.competition, "cv",_("CV")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop1",
                                                                "First choice",True,
                                                                "Please select your preferred workshop (mandatory)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop2",
                                                                "Second choice",False,
                                                                "Please select your second choice workshop (optional)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop3",
                                                                "Third choice",False,
                                                                "Please select your third choice workshop (optional)"))
        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommandation letter (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommandation letter (#2)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("Work #1")))
        field_definitions.append(MediaField(self.competition, "media2",_("Work #2")))
        field_definitions.append(MediaField(self.competition, "media3",_("Work #3")))
        field_definitions.append(MediaField(self.competition, "media4",_("Work #4"),required=False))
        field_definitions.append(MediaField(self.competition, "media5",_("Work #5"),required=False))
        field_definitions.append(MediaField(self.competition, "media6",_("Work #6"),required=False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree to take part in public performances,\
                                                                              radio and television broadcast without payment"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('workshops', {'fields': ['workshop1','workshop2','workshop3'],
                'description' : "Please select one or more composition workshops",
                'legend': 'Workshops' , 'classes': ['ProfileFieldset'] }),
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden'],
                'legend': 'Personal informations' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience',
                                                 'prices', 'cv','cv_hidden'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden',
                                   'media4','media4_hidden',
                                   'media5','media5_hidden',
                                   'media6','media6_hidden',
                                   ],
                'description': ACANTHES_COMPOSERS_MEDIA_DESCRIPTION,
                'legend': 'Works' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description': "If applicable, join additional recommandation letters or other documents (optional)",
                'classes': ['ProfileFieldset'] }),
            ('agreement', {'fields': ['agreement'], 'legend': 'Agreement' , 'classes': ['ProfileFieldset'] }),
        ]


class AcanthesPerformersApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=False))
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(DocumentField(self.competition, "cv",_("CV")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop","Master-class",True,"Please select an interpretation Master-class or the 'New technologies workshop'"))
        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommandation letter (#1)"),required=True))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommandation letter (#2)"),required=True))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("File #1"),True,"Drag & drop a file from your personal space"))
        field_definitions.append(MediaField(self.competition, "media2",_("File #2"),True,"Drag & drop a file from your personal space"))
        field_definitions.append(MediaField(self.competition, "media3",_("File #3"),False,"Drag & drop a file from your personal space"))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree to take part in public performances,\
                                                                              radio and television broadcast without payment"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('workshops', {'fields': ['workshop'],
                'legend': 'Master classes' , 'classes': ['ProfileFieldset'] }),
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden'],
                'legend': 'Personal informations' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience',
                                                 'prices', 'cv','cv_hidden'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden'
                                   ],
                'description': ACANTHES_PERFORMERS_MEDIA_DESCRIPTION,
                'legend': 'Works' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description' : "Please join 2 recommandation letters (mandatory for performers, optional for composers). Composers for New Technology Workshop : please drag & drop the technical questionnaire 'NTW-acanthes@ircam' from your personal space (mandatory).",
                'classes': ['ProfileFieldset'] }),
            ('agreement', {'fields': ['agreement'], 'legend': 'Agreement' , 'classes': ['ProfileFieldset'] }),
            ]


class RoyaumontApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        # Documents
        field_definitions.append(DocumentField(self.competition, "official_id",_("ID")))
        field_definitions.append(DocumentField(self.competition, "tef",_("TEF"),required=False))
        field_definitions.append(DocumentField(self.competition, "motivation_letter",_("Motivation letter")))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Works
        field_definitions.append(MediaField(self.competition, "work1",_("Work 1")))
        field_definitions.append(MediaField(self.competition, "work2",_("Work 2")))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements',
             {'fields': ['education', 'experience', 'prices'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('documents',
             {'fields': ['official_id','official_id_hidden','tef','tef_hidden','motivation_letter','motivation_letter_hidden',
                         'other_document1', 'other_document1_hidden', 'other_document2', 'other_document2_hidden'],
                'legend': 'Documents' , 'classes': ['ProfileFieldset'],
                    'description' : 'Please upload the following documents : A photocopy of your legal ID,\
                    a motivation letter and a proof of TEF results (French Evaluation Level Test) \
                    for whom French is not the native language). The minimum required TEF score\
                    is level 3 or higher/B1. Recommendation letters, and other documents\
                    you would like to value can be added in the optional fields.'}),
            ('medias', {'fields': ['work1','work1_hidden','work2','work2_hidden'],
                'legend': 'Work samples' , 'classes': ['ProfileFieldset']}),
        ]


class IrcamTremplinApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        # Documents
        field_definitions.append(DocumentField(self.competition, "official_id",_("ID")))
        field_definitions.append(DocumentField(self.competition, "motivation_letter",_("MotivationLetter")))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Works
        field_definitions.append(MediaField(self.competition, "work1",_("Work 1")))
        field_definitions.append(MediaField(self.competition, "work2",_("Work 2")))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements',
             {'fields': ['education', 'experience', 'prices'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['official_id','official_id_hidden','tef','tef_hidden','motivation_letter',
                                      'motivation_letter_hidden', 'other_document1', 'other_document1_hidden',
                                      'other_document2', 'other_document2_hidden'],
                'legend': 'Documents' , 'classes': ['ProfileFieldset'],
                    'description' : 'Please upload the following documents : A photocopy of your legal ID\
                    and a motivation letter. Recommendation letters, and other documents\
                    you would like to value can be added in the optional fields.'}),
            ('medias', {'fields': ['work1','work1_hidden','work2','work2_hidden'],
                'legend': 'Work samples' , 'classes': ['ProfileFieldset']}),
        ]


class ResidenceApplicationForm(ApplicationForm):

    def get_field_definitions(self, topic_key="topic_areas"):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "biography",_("Short biography")))
        field_definitions.append(DocumentField(self.competition, "cv",_("CV")))
        field_definitions.append(DocumentField(self.competition, "motivation_letter",_("MotivationLetter")))
        # Project submission
        field_definitions.append(ApplicationTextElementField(self.competition, "title",_("Title"),True,_("Title of the project")))
        field_definitions.append(ApplicationTextElementField(self.competition, "additional_authors",_("Additional authors"),True,""))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "abstract",_("Abstract"),True,
                                                             _("Short summary of the proposed project (max. 300 words)")))
        topic_areas_attribute = CompetitionAttribute.objects.get(self.competition, competition=self.competition,key=topic_key)
        field_definitions.append(CompetitionAttributeMultipleValueField(self.competition, topic_areas_attribute,"topic_areas",
                                                                        _("Topic areas"),True,_("Select one or several topic areas")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "collaborative_aspect",
                                                             _("Collaborative aspect"),True,
                                                             _("Discuss Ircam research teams or staff who whould be involved with the proposed project\
                                                               with a focus on collaborative aspects of the proposed research (max. 300 words)")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "work_plan",_("Work plan"),True,
                                                             _("Provide asked residency calendar and period and preliminary work plan\
                                                               involving Ircam teams (max. 150 words).")))
        field_definitions.append(DocumentField(self.competition, "project",_("Project proposal"),True,
                                               _("Drag & drop from your personal space a document providing \
                                                 detailed description of the proposed project. Situate with regard\
                                                 to state-of-the-art in the domain and previous work; \
                                                 clearly define objectives; argument technical and artistic novelties\
                                                 in the proposal and eventual means to achieve goals (6 pages max.)")))
        field_definitions.append(MediaField(self.competition, "sample1",_("Multimedia sample #1"),True,
                                            _("Drag & drop from your personal space previous art-work demonstrating\
                                              prior background in the field and/or discussed in project proposal.")))
        field_definitions.append(MediaField(self.competition, "sample2",_("Multimedia sample #2"),True,_("Drag & drop from your personal\
                                            space previous art-work demonstrating prior background in the field\
                                            and/or discussed in project proposal.")))
        return field_definitions

    class Meta:
        # Nota : it is NECESSARY to manually define a _hidden field for each document & media
        # Should be improved in a future version
        fieldsets = [
            ('biographic_elements',
             {'fields': ['biography', 'cv','cv_hidden','motivation_letter','motivation_letter_hidden'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('project_submissions',
             {'fields': ['title','additional_authors','project','project_hidden','abstract','topic_areas','collaborative_aspect','work_plan'],
                'legend': 'Project submission' ,'classes': ['ProfileFieldset']}),
            ('medias',
             {'fields': ['sample1','sample1_hidden','sample2','sample2_hidden'],
                'legend': 'Multimedia samples' , 'classes': ['ProfileFieldset']})
        ]


class Residence2012ApplicationForm(ResidenceApplicationForm):
    def get_field_definitions(self):
        field_definitions = super(Residence2012ApplicationForm, self).get_field_definitions("residency2012_topic_areas")
        return field_definitions


class ForumApplicationForm(ApplicationForm):
    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "cv",_("CV")))
        field_definitions.append(BiographicElementField(self.competition, "biography",_("Biography")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "abstract",_("Abstract"),True,
                                                             _("Short summary of the proposed project (max. 300 words)")))
        # Project
        field_definitions.append(DocumentField(self.competition, "project",_("Project proposal"),True,
                                               _("A PDF document describing the proposed piece: \
                                                   artistic description, technical description and technical requirements. \
                                                   Drag the document from your personal online space")))
        field_definitions.append(MediaField(self.competition, "work",_("Work sample"),True,
                                            _("Drag & drop from your personal space a recording \
                                                (audio or video and optional score) of the proposed piece.")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "technical",_("Technical Datasheet"),True,
                                            _("Detailed text description of the technical requirements for the piece production \
                                                (hardware and software materials required + performers) indicating whether \
                                                they will be provided by the artist or should be provided by the venue.")))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField("agreement",_("Are you an Ircam Forum Member ?"),False))
        return field_definitions
    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements', {'fields': ['cv', 'biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('project', {'fields': ['abstract', 'project','project_hidden', 'technical'],
                'legend': 'Project' , 'classes': ['ProfileFieldset'],
                    'description' : 'Project description'}),
            ('project_media', {'fields': ['work','work_hidden'],
                'legend': 'Project Media' , 'classes': ['ProfileFieldset'],
                    'description' : 'Project description'}),
            ('agreement', {'fields': ['agreement'], 'legend': 'Agreement' , 'classes': ['ProfileFieldset'] }),
        ]


AGREEMENT_TEXT = """
The student authorizes IRCAM to have photographs taken of artist-performers, composers, and members of the technical team during the classes given during the academy, during work sessions (rehearsals, dress rehearsals, and the workshop presentation).
The student authorizes IRCAM to record (or have recorded by a third party) all or part of the classes taught at IRCAM and outside of IRCAM, during work sessions (rehearsals, dress rehearsals, and the workshop presentation), showing artist-performers, composers, and members of the technical team.
These photographs and video recordings may be used for marketing and publicity needs, notably on IRCAM’s website as well as during any thematic or retrospective exhibition produced by IRCAM or one of its partners. This authorization is given for the legal duration of the copyright to the copyright holder or the copyright holder’s eligible parties, according to French law and to the law of any other country, current or future, including possible prolongations that could extend the validity of this agreement. This agreement is applicable worldwide. Any other use, notably any commercial use, must be the object of a separate agreement.
Filming may be carried out with the intention of broadcasting concert excerpts that will not exceed three minutes on television or on the radio. IRCAM and any person hired by IRCAM, promises to contact the student concerning all communication on the radio, in the press, or on television prior to the presentation workshop.
These recordings can be archived by IRCAM and used within IRCAM for educational purposes.
IRCAM may freely use, directly or indirectly, and particularly for publicity, the students’ names. These promotional and marketing activities in which the student could appear will not be remunerated.
"""


class IrcamCursus12013ApplicationForm(IrcamCursus1ApplicationForm):
    def get_field_definitions(self):
        field_definitions = super(IrcamCursus12013ApplicationForm, self).get_field_definitions()
        field_definitions.append(MediaField(self.competition, "work3", _("Work 3"), False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement", _("I agree"), True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements', {'fields': ['education', 'experience', 'prices'],
                'legend': 'Biographic details', 'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['official_id', 'official_id_hidden', 'tef', 'tef_hidden',
                                      'motivation_letter', 'motivation_letter_hidden', 'other_document1',
                                      'other_document1_hidden', 'other_document2', 'other_document2_hidden'],
                'legend': 'Documents', 'classes': ['ProfileFieldset'],
                    'description': 'Please upload the following documents : A photocopy of your legal ID\
                    , a motivation letter and a proof of TEF results (French Evaluation Level Test)\
                    for whom French is not the native language). The minimum required TEF score is level 3\
                    or higher/B1. Recommendation letters, and other documents you would like\
                    to value can be added in the optional fields.'}),
            ('medias', {'fields': ['work1', 'work1_hidden', 'work2', 'work2_hidden', 'work3', 'work3_hidden'],
                'legend': 'Work samples', 'description': 'If you have used Max, PD, or OpenMusic for one of these works, please indicate the URL of the location where the patch or maquette can be downloaded.', 'classes': ['ProfileFieldset']}),
            ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]


ACANTHES_COMPOSERS_2013_WORKS = """
Please submit works and/or information related to the workshops you apply for (corresponding to your 1st and 2nd choice).
<ul>
<li>Composition Workshop for EIC ensemble: 3 works, at least one of which is for large ensemble. (Please submit at least 2 scores and 1 recording among the 3 works.)</li>
<li>Composition Workshop for EIC ensemble and Philharmonic Orchestra: 3 works, at least one of which is for large ensemble. (Please submit at least 2 scores and 1 recording among the 3 works.)</li>
<li>Composition Workshop for the EXAUDI Ensemble for 6 voices: 3 works, at least two of which are for choir, vocal-ensemble, or voice(s) and instruments. (Please submit at least 2 scores and 2 recordings among the 3 works.)</li>
<li>Composition workshop for “In Vivo Danse”: 3 works, at least one of which is with electronics (if your chose to write with electronics). (Please submit at least 1 instrumental score and 2 recordings among the 3 works. NB: you can also send a score or a recording that gives a good idea of the future work that you will create during the “In Vivo Danse” project.)</li>
<li>Composition Workshop electronic pieces for "In Vivo Video": 3 works, at least 2 of which is with electronics.  (Please submit at least 1 instrumental score and 2 recordings among the 3 works.)</li>
<li>Electro-acoustic Interpretation Master Class: Please download and complete the following questionnaire concerning your level of technical mastery and experience in computer music.
[<a target="_blank" href="https://www.ulysses_network.eu/upload/news/technical-questionnaire-EN-v0.doc">download</a> and send it to the competition administrator]</li>
<li>Computer Music Workshop:  Analysis and Hands On: 3 works, at least 2 of which is with electronics.  (Please submit at least 1 instrumental score and 2 recordings among the 3 works.) NB: Please download and complete the following questionnaire concerning your level of technical mastery and experience in computer music.
[<a target="_blank" href="https://www.ulysses_network.eu/upload/news/technical-questionnaire-EN-v0.doc">download</a> and send it to the competition administrator]</li>
</ul>
"""


class AcanthesComposers2013ApplicationForm(ApplicationForm):

    def clean(self):
        cleaned_data = self.cleaned_data

        # Validate workshops
        workshop1 = None
        workshop2 = None

        workshop1 = self.cleaned_data.get('workshop1')
        workshop2 = self.cleaned_data.get('workshop2')

        workshop_values = []

        duplicate_workshops = False

        if workshop1 and int(workshop1) != 0:
            workshop_values.append(workshop1)
        if workshop2 and int(workshop2) != 0:
            if workshop2 not in workshop_values:
                workshop_values.append(workshop2)
            else:
                duplicate_workshops = True

        if duplicate_workshops:
            raise forms.ValidationError("You have selected several times the same workshop. Please select distinct workshops.")

        # Always return the full collection of cleaned data.
        return cleaned_data

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        gender_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="genders")
        field_definitions.append(CompetitionAttributeValueField(self.competition, gender_attribute, "gender",
                                                                "Pronoun", True, "Please select your pronoun"))

        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=False))
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(BiographicElementField(self.competition, "short_biography",_("Short biography (150 words maximum)")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop1",
                                                                "First choice",True,
                                                                "Please select your preferred workshop (mandatory)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop2",
                                                                "Second choice",False,
                                                                "Please select your second choice workshop (optional)"))

        field_definitions.append(ApplicationTextElementField(self.competition, "eic_specific",'',False))
        field_definitions.append(ApplicationTextElementField(self.competition, "exaudi_specific",'',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 2})))


        invivo_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="invivos")
        #field_definitions.append(ApplicationTextElementField(self.competition, "invivo_specific",'', False))
        field_definitions.append(CompetitionAttributeValueField(self.competition, invivo_attribute, "invivo_specific",
                                                                "Type", False, ""))

        invivo_video_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="invivos_video")
        #field_definitions.append(ApplicationTextElementField(self.competition, "invivo_specific",'', False))
        field_definitions.append(CompetitionAttributeValueField(self.competition, invivo_video_attribute, "invivo_video_specific",
                                                                "Type", False, ""))

        eaim_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="eaim")
        #field_definitions.append(ApplicationTextElementField(self.competition, "invivo_specific",'', False))
        field_definitions.append(CompetitionAttributeValueField(self.competition, eaim_attribute, "eaim_attribute",
                                                                "Type", False, ""))


        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommandation letter (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommandation letter (#2)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("Work #1")))
        field_definitions.append(MediaField(self.competition, "media2",_("Work #2")))
        field_definitions.append(MediaField(self.competition, "media3",_("Work #3")))
        field_definitions.append(MediaField(self.competition, "media4",_("Work #4"),required=False))
        field_definitions.append(MediaField(self.competition, "media5",_("Work #5"),required=False))
        field_definitions.append(MediaField(self.competition, "media6",_("Work #6"),required=False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each element, document & media
            # Should be improved in a future version
            ('workshops', {'fields': ['workshop1','workshop2'],
                'description' : "Please select your first and second choice among the composition workshops",
                'legend': 'Workshops' , 'classes': ['ProfileFieldset'] }),

            ('eic_specific_', {'fields': ['eic_specific'], 'legend':'For Composition Workshop with EIC', 'description':'Please specify with or without voice', 'classes': ['ProfileFieldset']}),
            ('exaudi_specific_', {'fields': ['exaudi_specific'], 'legend':'For Composition Workshop with EXAUDI', 'description':'Please select 3 to 6 voices (no duos and solos)', 'classes': ['ProfileFieldset']}),
            ('invivo_specific_', {'fields': ['invivo_specific'], 'legend':'For Composition Workshop with In Vivo Danse', 'description':'Please specify in the space below whether the work you plan to compose is for 1) dance, electronics and percussion, 2) dance and percussion, 3) dance and electronics', 'classes': ['ProfileFieldset']}),
            ('invivo_specific_video', {'fields': ['invivo_video_specific'], 'legend':'For Composition Workshop with In Vivo Video', 'description':'Please specify in the space below the video for which you plan to compose', 'classes': ['ProfileFieldset']}),
            ('eaim_attribute', {'fields': ['eaim_attribute'], 'legend':'For Electro-acoustic interpretation Master Class', 'description':'Please specify below the composition you would like to work on', 'classes': ['ProfileFieldset']}),


            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden', 'gender'],
                'legend': 'Personal information' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience', 'prices', 'short_biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden',
                                   'media4','media4_hidden',
                                   'media5','media5_hidden',
                                   'media6','media6_hidden',
                                   ],
                'description': ACANTHES_COMPOSERS_2013_WORKS,
                'legend': 'Works' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description': "If applicable, join additional recommandation letters or other documents (optional). You can directly send or have the letter sent to the Acanthes Academy email address : acanthes@ircam.fr",
                'classes': ['ProfileFieldset'] }),
                        ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]

PIANO_SPECIFIC = """
<p>
You will have to specify which proposition you would like to apply for:
</p>
<ul>
<li>- Bernd Alois Zimmermann: Monologue (for 2 pianos)</li>
<li>- Luciano Berio: Linea (2 pianos and vibraphone and marimba)</li>
<li>- Béla Bartok: Sonata (2 pianos and 2 percussions)</li>
<li>- Karl Heinz Stockhausen: Mantra (2 pianos and electronics)</li>
<li>- Luciano Berio: Linea (2 pianos and 2 percussions)</li>
</li>- Magnus Lindberg: Related Rocks (2 pianos, 2 percussions and electronics)</li>
</ul>
<p>
The repertoire proposed for each master class does not exclude suggestions made by the applying musicians. The instructors of each master class must validate these suggestions.
</p>
<p>
Please specify your selection of pieces and suggestions below:
</p>
"""

PERCUSSION_SPECIFIC = """
<p>
You will have to specify which proposition you would like to apply for:
</p>

<p>
Proposition n°1 (Percussion Master Class Steven Schick and in Vivo Dance)
<br />
June 12-21: master class by Steven Schick
<br />
and June 17-30: in Vivo Dance workshop, new works for dance, percussion with or without electronics by Samuel Favre
</p>

<p>
Proposition n°2 (Percussion Master Class Steven Schick and Pianos Duo)
<br />
June 12-21: master class by Steven Schick
<br />
and June 26-29: master class for percussionists and piano duo, by Jean-François Heisser and Jean-Frédéric Neuburger
</p>
<p>
You will be asked to specify a maximum of 2 pieces that you wish to work on:
</p>
<ul>
<li>- Helmut Lachenmann: Interieur</li>
<li>- Iannis Xenakis: Psappha</li>
<li>- Karlheinz Stockhausen: Zyklus</li>
<li>- James Dillon: Tiri Tiki Dha</li>
<li>- Brian Ferneyhough: Bone Alphabet</li>
<li>- John Luther Adams: The Mathematics of Resonant Bodies (for solo percussion and processed sounds)</li>
<li>- Kaija Saariaho: Six Japanese Gardens (for percussion and electronics)</li>
<li>- Thierry De Mey: Silence must be (for two percussions)</li>
</ul>
<p>
If you apply for Proposition n°2 (Percussion Master class Steven Schick and in Vivo Dance), you will be asked to specifiy a maximum of 2 pieces that you wish to work on:
</p>
<ul>
<li>- Luciano Berio: Linea (2 pianos and vibraphone and marimba)</li>
<li>- Béla Bartok: Sonata (2 pianos and 2 percussions)</li>
<li>- Luciano Berio: Linea (2 pianos and 2 percussions)</li>
<li>- Magnus Lindberg: Related Rocks (2 pianos, 2 percussions and electronics)</li>
</ul>
<p>
The repertoire proposed for each master class does not exclude suggestions made by the applying musicians. The instructors of each master class must validate these suggestions.
</p>
<p>
Please specify your selection of pieces and suggestions below:
</p>
"""

ACANTHES_PERFORMERS2013_MEDIA_DESCRIPTION = """
<p>
Please submit recordings related to the workshop you apply for.
</p>
"""

class AcanthesPerformers2013ApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=False))
        gender_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="genders")
        field_definitions.append(CompetitionAttributeValueField(self.competition, gender_attribute, "gender",
                                                                "Pronoun", True, "Please select your pronoun"))

        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(BiographicElementField(self.competition, "short_biography",_("Short biography (150 words maximum)")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop","Master Class",True,"Please select a Master Class"))

        field_definitions.append(ApplicationTextElementField(self.competition, "percussion_specific",'',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 3})))
        field_definitions.append(ApplicationTextElementField(self.competition, "piano_specific",'',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 3})))

        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommandation letter (#1)"),required=True))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommandation letter (#2)"),required=True))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("File #1"),True,"Drag & drop a file from your personal space"))
        field_definitions.append(MediaField(self.competition, "media2",_("File #2"),True,"Drag & drop a file from your personal space"))
        field_definitions.append(MediaField(self.competition, "media3",_("File #3"),False,"Drag & drop a file from your personal space"))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each bigographic element, document & media
            # Should be improved in a future version
            ('workshops', {'fields': ['workshop'],
                'legend': 'Master Classes' , 'classes': ['ProfileFieldset'] }),
            ('percussion_specific', {'fields': ['piano_specific'], 'legend':'For the percussion master class', 'description': PERCUSSION_SPECIFIC, 'classes': ['ProfileFieldset']}),
            ('piano_specific', {'fields': ['percussion_specific'], 'legend':'For the piano duos master class', 'description': PIANO_SPECIFIC, 'classes': ['ProfileFieldset']}),
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden', 'gender'],
                'legend': 'Personal informations' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience', 'prices', 'short_biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden'
                                   ],
                'description': ACANTHES_PERFORMERS2013_MEDIA_DESCRIPTION,
                'legend': 'Recordings' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description' : "Please join 2 recommendation letters (mandatory for performers, optional for composers).",
                'classes': ['ProfileFieldset'] }),
            ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]


class Royaumont2013ApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # double_nationality
        field_definitions.append(ApplicationTextElementField(self.competition, "double_nationality",
             '', False))
        # composition_courses
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute, "workshop1",
                                                                "First choice",True,
                                                                "Please select your first choice (mandatory)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute, "workshop2",
                                                                "Second choice",False,
                                                                "Please select your second choice workshop (optional)"))
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute, "workshop3",
                                                                "Third choice",False,
                                                                "Please select your third choice workshop (optional)"))

        # biographic detail
        field_definitions.append(BiographicElementField(self.competition, "education", _("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience", _("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices", _("Prizes & awards")))
        field_definitions.append(BiographicElementField(self.competition, "biography", _("Biography (600 characters maximum, spaces included)")))

        # other fieldset
        field_definitions.append(DocumentField(self.competition, "photo", _("Photo"), required=True))
        field_definitions.append(DocumentField(self.competition, "cv", _("CV"), required=False))

        # works
        field_definitions.append(MediaField(self.competition, "media1", _("Work #1")))
        field_definitions.append(MediaField(self.competition, "media2", _("Work #2")))
        field_definitions.append(MediaField(self.competition, "media3", _("Work #3")))
        field_definitions.append(MediaField(self.competition, "media4", _("Work #4"), required=False))
        field_definitions.append(MediaField(self.competition, "media5", _("Work #5"), required=False))
        field_definitions.append(MediaField(self.competition, "media6", _("Work #6"), required=False))

        # motivation letter
        field_definitions.append(BiographicElementField(self.competition, "motivation_letter", '', required=False))
        # document
        return field_definitions

    class Meta:
        fieldsets = [
            ('double_nationality', {'fields': ['double_nationality'], 'description':'If you have second nationality, precise it in the input below', 'legend': 'Double nationality', 'classes': ['ProfileFieldset']}),
            ('workshops', {'fields': ['workshop1', 'workshop2', 'workshop3'],
                'description': "Please select three composition courses, in order of preference",
                'legend': 'Workshops', 'classes': ['ProfileFieldset']}),
            ('biographic_detail', {'fields': ['education', 'experience', 'prices', 'biography'], 'legend':'Biographic details', 'classes': ['ProfileFieldset']}),
            ('biographic_documents', {'fields': ['cv', 'cv_hidden', 'photo', 'photo_hidden'], 'legend': 'Other biographic documents (optionnal)', 'classes': ['ProfileFieldset']}),
            ('medias', {'fields': ['media1', 'media1_hidden',
                                   'media2', 'media2_hidden',
                                   'media3', 'media3_hidden',
                                   'media4', 'media4_hidden',
                                   'media5', 'media5_hidden',
                                   'media6', 'media6_hidden',
                                   ],
                'description': 'Please add at least 3 of your works. Be careful: for each work, scores and recordings are mandatory.',
                'legend': 'Works' , 'classes': ['ProfileFieldset']}),
            ('motivation_letter', {'fields': ['motivation_letter'], 'legend': 'Motivation letter', 'classes': ['ProfileFieldset']})
        ]


TACTUS0213_MEDIA_DESCRIPTION = """Please submit the score and an audio file for the following two works:
<ul>
<li>- A chamber music piece</li>
<li>- A piece or part of a piece (movement) for symphony orchestra of a duration of approximately 10 minutes.  Please note this is the piece, which will be rehearsed during the workshops with the Brussels Philharmonic.  Orchestral accompaniment cannot exceed the following instrumentation:  Woodwinds in groups of 3: 3 flutes [including piccolo] - 3 oboes [including English horn] - 3 clarinets [including bass clarinet or soprano clarinet] - 3 bassoons [including contrabassoon] / Brass: 4 horns - 3 trumpets - 3 trombones – 1 tuba / Percussion: 3 players [1 timpanist and 2 percussionists] / Harp: 1 player / Strings: 15-13-11-10-8
</li>
</ul>
"""

TACTUS0213_CONSENT = """
I hereby submit my application for the next Tactus – Young Composers’ Forum to take place in Belgium October 21st to 25th, 2013.
</br>
Should my application be selected, I agree to :
<ul>
<li>
- provide the Tactus secretariat with the orchestra materials needed by September 1st, 2013
</li>
<li>
- be present in Belgium (Brussels and Mons) between October 21st and 25th, 2013
</li>
<li>
- attend all the reading sessions and other activities organized.
</li>
</ul>
Also, in the event that my work/s presented within the framework of this forum should be published or performed in public, I agree to include the following reference on the score or in the concert program : “piece written within the framework of Tactus – Young composers’forum”

"""


class Tactus2013ApplicationForm(ApplicationForm):
    """
    Tactus 2013 Application Form
    """

    def get_field_definitions(self):
        field_definitions = []

        # accommodation
        accommodation_attribute = CompetitionAttribute.objects.get(competition=self.competition,
            key="accommodations")
        field_definitions.append(CompetitionAttributeValueField(self.competition, accommodation_attribute,
            'accommodation', 'I would like to be accommodated', True, ""))

        # biographic detail
        field_definitions.append(BiographicElementField(self.competition, "education", _("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience", _("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices", _("Prizes & awards")))

        # biographie detail
        field_definitions.append(DocumentField(self.competition, "photo", _("Photo"), required=True))
        field_definitions.append(DocumentField(self.competition, "cv", _("CV"), required=True))
        field_definitions.append(DocumentField(self.competition, "birth_certificate", _("Certificat de naissance"), required=True))
        field_definitions.append(DocumentField(self.competition, "passport", _("Scan of passport"), required=True))

        # works
        field_definitions.append(MediaField(self.competition, "media1", _("Work #1")))
        field_definitions.append(MediaField(self.competition, "media2", _("Work #2")))

        # motivation letter
        field_definitions.append(DocumentField(self.competition, "motivation_letter", 'Motivation letter', required=False))

        # consent
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement", _("I agree"), True))

        return field_definitions

    class Meta:
        fieldsets = [
            ('accommodation',
                {'fields': ['accommodation', ],
                'description':'',
                'legend':'Accommodation',
                'classes': ['ProfileFieldset']}),
            ('biographic_detail',
                {'fields': ['education', 'experience', 'prices'],
                'description':'',
                'legend':'Biographic details',
                'classes': ['ProfileFieldset']}),
            ('biographic_documents',
                {'fields': ['cv', 'cv_hidden', 'photo', 'photo_hidden',
                    'birth_certificate', 'birth_certificate_hidden', 'passport', 'passport_hidden'],
                'legend': 'Other biographic documents',
                'classes': ['ProfileFieldset']}),
            ('works', {'fields': ['media1', 'media1_hidden', 'media2', 'media2_hidden'],
                'legend': 'Works',
                'description':TACTUS0213_MEDIA_DESCRIPTION,
                'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['motivation_letter', 'motivation_letter_hidden'],
                'description':'',
                'legend':'Other documents',
                'classes': ['ProfileFieldset']}),
            ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':TACTUS0213_CONSENT,
                 'classes': ['ProfileFieldset']}
            ),
        ]


FORUM2013AGREEMENT = """
The applicant authorizes IRCAM to have photographs taken of artist-performers, composers,
and members of the technical team during the event, during the Forum workshops,
during work sessions (rehearsals, dress rehearsals, and the workshop presentation).
The applicant authorizes IRCAM to record (or have recorded by a third party) all or part
of the event taught at IRCAM and outside of IRCAM, during works sessions (rehearsals,
    dress rehearsals, and presentation), showing artist-performers, composers, and embers
of the technical team. These photographs and video recordings may be used for marketing and
publicity needs, notably on IRCAM’s website as well as during any thematic or retrospective
exhibition produced by IRCAM or one of its partners. This authorization is given for the legal
duration of the copyright to copyrigh holder or the copyright holder’s eligible parties,
according to French law and to the law of any other country, current or future, including
possible prolongations that could extend the validity of ths agreement. This agreement is
applicable worldwide. Any other use, notably any commercial use, must be the object of a separate
agreement. Filming maybe carried out with the intention of broadcasting concert excerpts
that will not exceed three minutes on television or on the radio. IRCAM and any person
hired by IRCAM promises to contact the applicant concerning all communication on the radio,
in the press, or on television prior to the presentation workshop. These recordings can be archived by IRCAM
 and used within IRCAM for educational purposes and website. IRCAM may freely use, directly
 or indirectly, and particularly for publicity, the applicants names. These promotional
 and marketing activities in which the applicant could appear will not be remunerated.
"""


class Forum2013ApplicationForm(ApplicationForm):
    def get_field_definitions(self):
        field_definitions = []
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "cv", _("CV (Max.300 words)"), True))
        field_definitions.append(BiographicElementField(self.competition, "biography",_("Short Biography (Max.300 words)")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "abstract",_("Abstract"),True,
                                                             _("Short summary of the proposed project (max. 300 words)")))
        # Project
        field_definitions.append(DocumentField(self.competition, "project",_("Project proposal"),True,
                                               _("A PDF document describing the proposed piece: \
                                                   artistic description, technical description and technical requirements. \
                                                   Drag the document from your personal online space")))
        field_definitions.append(MediaField(self.competition, "work",_("Work sample"),True,
                                            _("Drag & drop from your personal space a recording \
                                                (audio or video and optional score) of the proposed piece.")))
        field_definitions.append(ApplicationHtmlElementField(self.competition, "technical",_("Technical Datasheet"),True,
                                            _("Detailed text description of the technical requirements for the piece production \
                                                (hardware and software materials required + performers) indicating whether \
                                                they will be provided by the artist or should be provided by the venue.")))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree"),False))
        return field_definitions
    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements', {'fields': ['cv', 'biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('project', {'fields': ['abstract', 'project','project_hidden', 'technical'],
                'legend': 'Project' , 'classes': ['ProfileFieldset']}),
            ('project_media', {'fields': ['work','work_hidden'],
                'legend': 'Project Media' , 'classes': ['ProfileFieldset'],
                    'description' : 'Project description'}),
            ('agreement', {'fields': ['agreement'], 'legend': 'Agreement', 'description':FORUM2013AGREEMENT, 'classes': ['ProfileFieldset'] }),
        ]


class Residence2013ApplicationForm(ResidenceApplicationForm):
    def get_field_definitions(self):
        field_definitions = super(Residence2013ApplicationForm, self).get_field_definitions("residency2013_topic_areas")
        return field_definitions


class RoyaumontPrototype2013ApplicationForm(ApplicationForm):
    def get_field_definitions(self):
        field_definitions = []

        field_definitions.append(ApplicationTextElementField(self.competition, "birth_place",_("Lieu de naissance / Place of birth"), True))
        field_definitions.append(ApplicationTextElementField(self.competition, "country",_("Pays / Country"), True))
        field_definitions.append(ApplicationTextElementField(self.competition, "mobile",_("Portable / Mobile"), False))

        field_definitions.append(ApplicationTextElementField(self.competition, "antecedant",'Activité / Activity',False,_("Activité à l'entrée du stage / Activity as of the beginning of the workshop")))

        administrativ_status = CompetitionAttribute.objects.get(competition=self.competition, key="administrativ_status")
        field_definitions.append(CompetitionAttributeValueField(self.competition, administrativ_status,"administrativ_status", "Status",
                                                                True,"Statut / Adminsitrativ status"))
        professional_experience = CompetitionAttribute.objects.get(competition=self.competition, key="professional_experience")
        field_definitions.append(CompetitionAttributeValueField(self.competition, professional_experience,"professional_experience", "Experience",
                                                                True,"Expérience Professionnelle / Professional experience"))
        english_level = CompetitionAttribute.objects.get(competition=self.competition, key="english_level")
        field_definitions.append(CompetitionAttributeValueField(self.competition, english_level,"english_level", "Anglais / English",
                                                                False,"Votre niveau de compréhension orale de l'anglais / Your understanding of spoken English"))
        french_level = CompetitionAttribute.objects.get(competition=self.competition, key="french_level")
        field_definitions.append(CompetitionAttributeValueField(self.competition, french_level,"french_level", "Français / French",
                                                                False,"Votre niveau de compréhension orale du français / Your understanding of spoken French"))

        field_definitions.append(ApplicationTextElementField(self.competition, "health","Santé / Health", False, _("Avez-vous une situation de santé particulière à signaler / Do you have anything to indicate concerning your health")))

        field_definitions.append(DocumentField(self.competition, "cv", _("CV"), True))
        field_definitions.append(DocumentField(self.competition, "photo", _("Photo"), True))
        field_definitions.append(DocumentField(self.competition, "motivation_letter", _("Lettre de motivation / Motivation letter"), True, _("Une lettre décrivant vos projets personnels et votre motivation à participer à cet atelier / A letter with a description of your personal projects and motivation to participate in this workshop.")))

        # works
        field_definitions.append(MediaField(self.competition, "media1", _("Work #1")))

        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('personal_details', {'fields': ['birth_place', 'country', 'mobile', 'antecedant', 'administrativ_status', 'professional_experience', 'english_level', 'french_level', 'health'],
                'legend': 'Personal details' , 'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['cv', 'cv_hidden', 'photo', 'photo_hidden', 'motivation_letter', 'motivation_letter_hidden'],
                'legend': 'Documents' , 'classes': ['ProfileFieldset']}),
            ('work', {'fields': ['media1', 'media1_hidden'], 'legend': 'Extrait audio', 'description':'Un extrait audio de votre travail / A musical extract of your work', 'classes': ['ProfileFieldset'] }),
        ]



class IrcamCursus12014ApplicationForm(IrcamCursus1ApplicationForm):
    def get_field_definitions(self):
        field_definitions = super(IrcamCursus12014ApplicationForm, self).get_field_definitions()
        field_definitions.append(MediaField(self.competition, "work3", _("Work 3"), True))
        field_definitions.append(MediaField(self.competition, "work4", _("Work 4"), False))
        field_definitions.append(MediaField(self.competition, "work5", _("Work 5"), False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement", _("I agree"), True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each document & media
            # Should be improved in a future version
            ('biographic_elements', {'fields': ['education', 'experience', 'prices'],
                'legend': 'Biographic details', 'classes': ['ProfileFieldset']}),
            ('documents', {'fields': ['official_id', 'official_id_hidden', 'tef', 'tef_hidden',
                                      'motivation_letter', 'motivation_letter_hidden', 'other_document1',
                                      'other_document1_hidden', 'other_document2', 'other_document2_hidden'],
                'legend': 'Documents', 'classes': ['ProfileFieldset'],
                    'description': 'Please upload the following documents : A photocopy of your legal ID\
                    , a motivation letter and a proof of TEF results (French Evaluation Level Test)\
                    for whom French is not the native language). The minimum required TEF score is level 3\
                    or higher/B1. Recommendation letters, and other documents you would like\
                    to value can be added in the optional fields.'}),
            ('medias', {'fields': ['work1', 'work1_hidden', 'work2', 'work2_hidden', 'work3', 'work3_hidden', 'work4', 'work4_hidden', 'work5', 'work5_hidden'],
                'legend': 'Work samples', 'description': 'If you have used Max, PD, or OpenMusic for one of these works, please indicate the URL of the location where the patch or maquette can be downloaded.', 'classes': ['ProfileFieldset']}),
            ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]



#######

ACADEMY_2014_AGREEMENT_TEXT = """
<p>
<b>Application</b>
<br />
Prerequisite requirements, description, instrumentarium, registration fee are provided by IRCAM online www.ircam.fr. It is the applicant's responsibility to ensure she/he understands and meets the workshop or master class information.
</p>
<p>
<b>Age</b>
<br />
Participant must be born after 1st of January, 1982.
</p>
<p>
<b>Selection</b>
<br />
The jury will meet in December, 2013 and each applicant will receive an answer by e.mail before December, 20, 2013.
</p>
<p>
<b>Participation</b>
<br />
The selected participant will assure to be free during all the period, as detailed schedules will be known only in the beginning of the academy.
</p>
<p>
<b>Registration Fee</b>
<br />
Registration of selected participant is confirmed only upon reception of payment of the participation fee that should be not done after January, 6, 2014 (no full repayment after the legal French 10 days delay).
</p>
<p>
<b>Cancellation</b>
<br />
If the participant does not respect scores deliver delays, IRCAM might cancel her/his participation and will not refund registration fee.
In case of IRCAM should cancel a workshop, participation fee will be fully refunded.
</p>
<p>
<b>Attestation</b>
<br />
After the academy, IRCAM will provide an attestation to the participant.
</p>
<p>
<b>Photographs and video</b>
<br />
The participant in the academy authorizes IRCAM to have photographs taken of artist-performers, composers, and members of the technical team during the classes given during the academy, during work sessions (rehearsals, dress rehearsals, and the workshop presentation). The participant authorizes IRCAM to record (or have recorded by a third party) all or part of the classes taught at IRCAM and outside of IRCAM, during work sessions (rehearsals, dress rehearsals, and the workshop presentation), showing artist-performers, composers, and members of the technical team. These photographs and video recordings may be used for marketing and publicity needs, notably on IRCAM’s website as well as during any thematic or retrospective exhibition produced by IRCAM or one of its partners. This authorization is given for the legal duration of the copyright to the copyright holder or the copyright holder’s eligible parties, according to French law and to the law of any other country, current or future, including possible prolongations that could extend the validity of this agreement. This agreement is applicable worldwide. Any other use, notably any commercial use, must be the object of a separate agreement. Filming may be carried out with the intention of broadcasting concert excerpts that will not exceed three minutes on television or on the radio. IRCAM and any person hired by IRCAM, promises to contact the participant concerning all communication on the radio, in the press, or on television prior to the presentation workshop. These recordings can be archived by IRCAM and used within IRCAM for educational purposes. IRCAM may freely use, directly or indirectly, and particularly for publicity, the participant’s names. These promotional and marketing activities in which the participant could appear will not be remunerated.
</p>
"""

ACADEMY_COMPOSERS_2014_WORKS = """
Please submit works/and or information related to the workshops you apply for:
<p>
<b>
For 2 voices and ensemble composition workshop (EXAUDI and Ensemble intercontemporain) and orchestra work session (Orchestre Philharmonique de Radio France)
</b>
<br />
3 works, one of large ensemble, one for voice(s), one for orchestra. Please submit at least 2 scores and 1 recording among the 3 works.
</p>
<p>
<b>
For Chamber music composition workshop (Soloists of the Ensemble intercontemporain)
</b>
<br />
3 works, at least 1 of which for chamber music. Please submit at least 2 scores and 1 recording among the 3 works.
</p>
<p>
<b>
For In Vivo Danse (L. Touzé)
</b>
<br />
3 works, at least 1 of which with electronics (if you wish to write with electronics), 1 of which for voice(s) (if you wish to write for Exaudi vocal ensemble). Please submit at least 1 instrumental score and 2 recordings among the 3 works.
</p>
<p>
<b>
For In Vivo Théâtre (G. Aperghis)
</b>
<br />
3 works, at least 1 of which for voice(s), and 1 of which with electronics (if you wish to work with electronics) or 1 one of which with video (if you wish to work with video). Please submit at least 1 instrumental score, and 2 recordings among the 3 works.
</p>
<p>
<b>
For In Vivo Electro Live (R. Henke)
</b>
<br />
3 works for live electronics with or without improvisation.
</p>


"""

ACADEMY_2014_DOCUMENTS = """
<p>
Please submit following additional documents according to your composition workshop choice:
</p>
<p>
<b>
For any workshop
</b>
You can join additional recommendation letter(s) or other documents (optional). You can directly send or have the letter(s) sent to academy@ircam.fr (in that case, thank you to indicate your name and which workshop you apply for).
</p>
<p>
<b>
For In Vivo Théâtre (G. Aperghis)
</b>
You have to submit a document if possible in FRENCH with the detailed project you would like to work on (please see on our website all required information <a target="_blank" href="http://www.ircam.fr/academie.html">http://www.ircam.fr/academie.html</a>)
</p>
<p>
<b>
For In Vivo Electro Live (R. Henke)
</b>
You have to submit a document describing your project including spatial aspects and a questionnaire to be download on our website <a target="_blank" href="http://www.ircam.fr/academie.html">http://www.ircam.fr/academie.html</a>
</p>
<p>
<b>
For Electro-acoustic Interpretation
</b>
You have to submit a questionnaire concerning your level of technical mastery and experience in computer music, to be download on our website <a target="_blank" href="http://www.ircam.fr/academie.html">http://www.ircam.fr/academie.html</a>
</p>

"""

class AcademyComposers2014ApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        gender_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="genders")
        field_definitions.append(CompetitionAttributeValueField(self.competition, gender_attribute, "gender",
                                                                "Pronoun", True, "Please select your pronoun"))

        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"), required=True))
        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(BiographicElementField(self.competition, "short_biography",_("Short biography (150 words maximum)")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop1",
                                                                "Choice",True,
                                                                ""))

        two_voices_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="two_voices")
        field_definitions.append(CompetitionAttributeValueField(self.competition, two_voices_attribute, "two_voices", "Choice", False, ""))

        #four_voices_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="four_voices")
        #field_definitions.append(CompetitionAttributeValueField(self.competition, four_voices_attribute, "four_voices", "Choice", False, ""))
        field_definitions.append(ApplicationTextElementField(self.competition, "four_voices",'Please specify instruments',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 2})))

        invivo_danse_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="invivo_danse")
        field_definitions.append(CompetitionAttributeValueField(self.competition, invivo_danse_attribute, "invivo_danse", "Choice", False, ""))


        electro_interpretation = CompetitionAttribute.objects.get(competition=self.competition, key="electro_interpretation")
        field_definitions.append(CompetitionAttributeValueField(self.competition, electro_interpretation, "electro_interpretation", "Choice", False, ""))

        #field_definitions.append(CompetitionAttributeValueField(self.competition, invivo_attribute, "invivo_specific",
        #                                                        "Type", False, ""))

        #invivo_video_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="invivos_video")
        #field_definitions.append(ApplicationTextElementField(self.competition, "invivo_specific",'', False))
        #field_definitions.append(CompetitionAttributeValueField(self.competition, invivo_video_attribute, "invivo_video_specific",
        #                                                        "Type", False, ""))

        #eaim_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="eaim")
        #field_definitions.append(ApplicationTextElementField(self.competition, "invivo_specific",'', False))
        #field_definitions.append(CompetitionAttributeValueField(self.competition, eaim_attribute, "eaim_attribute",
        #                                                        "Type", False, ""))


        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommendation letter (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommendation letter (#2)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("Work #1"),required=False))
        field_definitions.append(MediaField(self.competition, "media2",_("Work #2"),required=False))
        field_definitions.append(MediaField(self.competition, "media3",_("Work #3"),required=False))
        #field_definitions.append(MediaField(self.competition, "media4",_(u"Work #4"),required=False))
        #field_definitions.append(MediaField(self.competition, "media5",_(u"Work #5"),required=False))
        #field_definitions.append(MediaField(self.competition, "media6",_(u"Work #6"),required=False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement",_("I agree"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each element, document & media
            # Should be improved in a future version
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden', 'gender'],
                'legend': 'Personal information' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience', 'prices', 'short_biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),

            ('workshops', {'fields': ['workshop1',],
                'description' : "Please select a choice among the workshops",
                'legend': 'Workshops' , 'classes': ['ProfileFieldset'] }),

            ('two_voices', {'fields': ['two_voices'], 'legend':'For 2 voices and ensemble composition workshop (EXAUDI and Ensemble intercontemporain)', 'description':'Please specify voice(s)', 'classes': ['ProfileFieldset']}),
            ('four_voices', {'fields': ['four_voices'], 'legend':'For Chamber music composition workshop (Ensemble intercontemporain)', 'description':'', 'classes': ['ProfileFieldset']}),
            ('invivo_danse', {'fields': ['invivo_danse'], 'legend':'For In Vivo Danse (L. Touzé)', 'description':'Please specify if you wish to write for', 'classes': ['ProfileFieldset']}),
            ('electro_interpretation', {'fields': ['electro_interpretation'], 'legend':'For Electro-acoustic Interpretation', 'description':'Please specify which piece you would like to work on', 'classes': ['ProfileFieldset']}),

            #('invivo_specific_video', {'fields': ['invivo_video_specific'], 'legend':'For Composition Workshop with In Vivo Video', 'description':'Please specify in the space below the video for which you plan to compose', 'classes': ['ProfileFieldset']}),
            #('eaim_attribute', {'fields': ['eaim_attribute'], 'legend':'For Electro-acoustic interpretation Master Class', 'description':'Please specify below the composition you would like to work on', 'classes': ['ProfileFieldset']}),


            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden'],
                'description': ACADEMY_COMPOSERS_2014_WORKS,
                'legend': 'Works' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description': ACADEMY_2014_DOCUMENTS,
                'classes': ['ProfileFieldset'] }),
                        ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':ACADEMY_2014_AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]


ACADEMY_PERFORMER_2014_AGREEMENT_TEXT = """
<p>
<b>Application</b>
<br />
Prerequisite requirements, description, instrumentarium, registration fee are provided by IRCAM online www.ircam.fr. It is the applicant's responsibility to ensure she/he understands and meets the workshop or master class information.
</p>
<p>
<b>Age</b>
<br />
Participant must be born after 1st of January, 1982.
</p>
<p>
<b>Selection</b>
<br />
The jury will meet in April, 2014 and each applicant will receive an answer by e.mail before April, 11, 2014.
</p>
<p>
<b>Participation</b>
<br />
The selected participant will assure to be free during all the period, as detailed schedules will be known only in the beginning of the academy.
</p>
<p>
<b>Registration Fee</b>
<br />
Registration of selected participant is confirmed only upon reception of payment of the participation fee that should be not done after April, 29, 2014 (no full repayment after the legal French 10 days delay).
</p>
<p>
<b>Cancellation</b>
<br />
In case of IRCAM should cancel a workshop, participation fee will be fully refunded.
</p>
<p>
<b>Attestation</b>
<br />
After the academy, IRCAM will provide an attestation to the participant.
</p>
<p>
<b>Photographs and video</b>
<br />
The participant in the academy authorizes IRCAM to have photographs taken of artist-performers, composers, and members of the technical team during the classes given during the academy, during work sessions (rehearsals, dress rehearsals, and the workshop presentation). The participant authorizes IRCAM to record (or have recorded by a third party) all or part of the classes taught at IRCAM and outside of IRCAM, during work sessions (rehearsals, dress rehearsals, and the workshop presentation), showing artist-performers, composers, and members of the technical team. These photographs and video recordings may be used for marketing and publicity needs, notably on IRCAM’s website as well as during any thematic or retrospective exhibition produced by IRCAM or one of its partners. This authorization is given for the legal duration of the copyright to the copyright holder or the copyright holder’s eligible parties, according to French law and to the law of any other country, current or future, including possible prolongations that could extend the validity of this agreement. This agreement is applicable worldwide. Any other use, notably any commercial use, must be the object of a separate agreement. Filming may be carried out with the intention of broadcasting concert excerpts that will not exceed three minutes on television or on the radio. IRCAM and any person hired by IRCAM, promises to contact the participant concerning all communication on the radio, in the press, or on television prior to the presentation workshop. These recordings can be archived by IRCAM and used within IRCAM for educational purposes. IRCAM may freely use, directly or indirectly, and particularly for publicity, the participant’s names. These promotional and marketing activities in which the participant could appear will not be remunerated.
</p>

"""

ACADEMY_PERFORMER_2014_MEDIA_DESCRIPTION  = """

<p>
Please submit recordings related to the master class you apply for:
</p>
<p>
<b>Peter Eötvös Lucerne Festival Academy Orchestra and Orchestre Philharmonique de Radio France conducting master classes</b>
<br />
3 videos of rehearsals (no performance recordings)
</p>
<p>
<b>EXAUDI small vocal ensemble master class</b>
<br />
3 audio or video of performances and/or rehearsals
</p>

"""


class AcademyPerformers2014ApplicationForm(ApplicationForm):

    def get_field_definitions(self):
        field_definitions = []
        # Personal informations
        field_definitions.append(ApplicationTextElementField(self.competition, "occupation",_("Occupation"),True))
        languages_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="languages")
        field_definitions.append(CompetitionAttributeValueField(self.competition, languages_attribute,"contact_language",
                                                                "Language",True,"Please select your preferred communication language"))
        field_definitions.append(DocumentField(self.competition, "photo",_("Photo"),required=True))
        gender_attribute = CompetitionAttribute.objects.get(competition=self.competition, key="genders")
        field_definitions.append(CompetitionAttributeValueField(self.competition, gender_attribute, "gender",
                                                                "Pronoun", True, "Please select your pronoun"))

        # Biographic elements
        field_definitions.append(BiographicElementField(self.competition, "education",_("Educations & diplomas")))
        field_definitions.append(BiographicElementField(self.competition, "experience",_("Professional experience")))
        field_definitions.append(BiographicElementField(self.competition, "prices",_("Prizes & awards")))
        field_definitions.append(BiographicElementField(self.competition, "short_biography",_("Short biography (150 words maximum)")))
        # Workshops
        workshops_attribute = CompetitionAttribute.objects.get(competition=self.competition,key="workshops")
        field_definitions.append(CompetitionAttributeValueField(self.competition, workshops_attribute,"workshop","Choice",True,""))
        field_definitions.append(ApplicationTextElementField(self.competition, "exaudi_specific",'For Exaudi small vocal ensemble',False, "Please propose one or several pieces you would like to work on.",  widget=forms.Textarea(attrs={'cols': 80, 'rows': 3})))


        #field_definitions.append(ApplicationTextElementField(self.competition, "percussion_specific",'',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 3})))
        #field_definitions.append(ApplicationTextElementField(self.competition, "piano_specific",'',False, widget=forms.Textarea(attrs={'cols': 80, 'rows': 3})))

        # Documents
        field_definitions.append(DocumentField(self.competition, "recommandation_letter",_("Recommendation letter (#1)"),required=True))
        field_definitions.append(DocumentField(self.competition, "recommandation_letter2",_("Recommendation letter (#2)"),required=True))
        field_definitions.append(DocumentField(self.competition, "other_document1",_("Other document (#1)"),required=False))
        field_definitions.append(DocumentField(self.competition, "other_document2",_("Other document (#2)"),required=False))
        # Media
        field_definitions.append(MediaField(self.competition, "media1",_("File #1"),True))
        field_definitions.append(MediaField(self.competition, "media2",_("File #2"),True))
        field_definitions.append(MediaField(self.competition, "media3",_("File #3"),False))
        # Agreement
        field_definitions.append(ApplicationBooleanElementField(self.competition, "agreement", _("I agree"),True))
        # Return list
        return field_definitions

    class Meta:
        fieldsets = [
            # Nota : it is NECESSARY to manually define a _hidden field for each bigographic element, document & media
            # Should be improved in a future version
            #('percussion_specific', {'fields': ['piano_specific'], 'legend':'For the percussion master class', 'description': PERCUSSION_SPECIFIC, 'classes': ['ProfileFieldset']}),
            #('piano_specific', {'fields': ['percussion_specific'], 'legend':'For the piano duos master class', 'description': PIANO_SPECIFIC, 'classes': ['ProfileFieldset']}),
            ('personal_informations', {'fields': ['occupation','contact_language','photo','photo_hidden', 'gender'],
                'legend': 'Personal informations' , 'classes': ['ProfileFieldset']}),
            ('biographic_elements', {'fields': ['education', 'experience', 'prices', 'short_biography'],
                'legend': 'Biographic details' , 'classes': ['ProfileFieldset']}),
            ('workshops', {'fields': ['workshop', 'exaudi_specific'],
                'legend': 'Select your Master Class' , 'classes': ['ProfileFieldset'] }),

            ('medias', {'fields': ['media1','media1_hidden',
                                   'media2','media2_hidden',
                                   'media3','media3_hidden'
                                   ],
                'description': ACADEMY_PERFORMER_2014_MEDIA_DESCRIPTION,
                'legend': 'Recordings' , 'classes': ['ProfileFieldset'] }),
            ('documents', {'fields': ['recommandation_letter','recommandation_letter_hidden',
                                      'recommandation_letter2','recommandation_letter2_hidden',
                                      'other_document1','other_document1_hidden',
                                      'other_document2','other_document2_hidden',
                                      ],
                'legend': 'Documents' ,
                'description' : "Please join 2 recommendation letters or other documents. You can directly send or have the letter(s) sent to academy@ircam.fr (in that case, thank you to indicate your name and which master class you apply for).",
                'classes': ['ProfileFieldset'] }),
            ('agreement',
                {'fields': ['agreement'],
                 'legend': 'Consent and Release Form',
                 'description':ACADEMY_PERFORMER_2014_AGREEMENT_TEXT,
                 'classes': ['ProfileFieldset']}
            ),
        ]


class CompetitionStepForm(forms.Form):

    competition_step = forms.IntegerField(required=True, label="Step")

    def __init__(self, competition, *args, **kwargs):
        self.competition = competition
        super(CompetitionStepForm, self).__init__(*args, **kwargs)
        self.fields['competition_step'].widget = forms.Select(
            choices=[(step.id, step.name) for step in competition.steps()]
        )

    def clean_competition_step(self):
        try:
            return CompetitionStep.objects.get(
                competition=self.competition,
                id=self.cleaned_data['competition_step']
            )
        except CompetitionStep.DoesNotExist:
            raise forms.ValidationError("Unknown step")


class AddRemoveCompetitionStepForm(CompetitionStepForm):
    add_or_remove = forms.ChoiceField(
        required=True,
        label="Action",
        choices=[("add", "Add to step"), ("remove", "Remove from step"), ]
    )
    competition_step = forms.IntegerField(required=True, label="Step")


class CompetitionStepSettingsForm(forms.ModelForm):

    class Meta:
        model = CompetitionStep
        fields = (
            'notify_association_by_email', 'notification_email',
        )

    def clean(self):
        clean_result = super().clean()
        if self.cleaned_data['notify_association_by_email'] and not self.cleaned_data['notification_email']:
            raise forms.ValidationError(_('Please enter the text of the notification email'))
        return clean_result
