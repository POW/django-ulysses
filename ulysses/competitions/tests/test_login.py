# -*- coding: utf-8 -*-

"""unit tests"""

from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import Group, User
from django.urls import reverse

from ulysses.composers.factories import UserFactory
from ulysses.profiles.factories import IndividualFactory

from ..models import JuryMember


class LoginTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        self.super_login = {  # superuser
            'email': 'admin@ircam.fr',
            'password': 'admin',
        }
        self.compadmin_login = {  # staff, member of "competition-admins"
            'email': 'compadmin@ircam.fr',
            'password': 'compadmin',
        }
        self.jury_login = {  # staff, member of "jury-members"
            'email': 'jury@ircam.fr',
            'password': 'jury',
        }
        self.staff_login = {  # staff
            'email': 'staff@ircam.fr',
            'password': 'staff',
        }
        self.regular_login = {  # simple user, not staff
            'email': 'web@ircam.fr',
            'password': 'web',
        }

        # Create sample users

        super_user = UserFactory.create(
            username=self.super_login['email'], password=self.super_login['password'], email=self.super_login['email']
        )
        super_user.is_superuser = True
        super_user.is_staff = True
        super_user.is_active = True
        super_user.save()
        IndividualFactory.create(user=super_user)

        compadmin_user = UserFactory.create(
            username=self.compadmin_login['email'], password=self.compadmin_login['password'],
            email=self.compadmin_login['email'],
        )
        compadmin_user.is_active = True
        compadmin_user.is_staff = True
        compadmin_user.save()
        compadmin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        IndividualFactory.create(user=compadmin_user)
        # CompetitionManager.objects.create(user=compadmin_user)

        jury_user = UserFactory.create(
            username=self.jury_login['email'], password=self.jury_login['password'], email=self.jury_login['email']
        )
        jury_user.is_active = True
        jury_user.save()
        jury_user.groups.add(Group.objects.get(name=settings.JURY_MEMBERS_GROUP))
        IndividualFactory.create(user=jury_user)

        staff_user = UserFactory.create(
            username=self.staff_login['email'], password=self.staff_login['password'], email=self.staff_login['email']
        )
        staff_user.is_staff = True
        staff_user.is_active = True
        staff_user.save()
        IndividualFactory.create(user=staff_user)

        regular_user = UserFactory.create(
            username=self.regular_login['email'], password=self.regular_login['password'],
            email=self.regular_login['email']
        )
        regular_user.is_staff = False
        staff_user.is_active = True
        regular_user.save()
        IndividualFactory.create(user=regular_user)

    def test_admin_login_redirect_regular_login(self):
        """
        Test that everybody login with the same form
        """

        login_url = reverse('login')
        admin_login_url = reverse('competition-admin:login')

        response = self.client.get(admin_login_url)
        self.assertRedirects(response, login_url)

    def test_superadmin_login_redirect_regular_login(self):
        """
        Test that everybody login with the same form
        """

        login_url = reverse('login')
        admin_login_url = reverse('admin:login')

        response = self.client.get(admin_login_url)
        self.assertRedirects(response, login_url)

    def test_super_user_on_super_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.super_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('admin:index'))
        self.assertEqual(200, response.status_code)

    def test_competition_admin_on_super_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.compadmin_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('admin:index'))
        self.assertEqual(403, response.status_code)

    def test_jury_member_on_super_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.jury_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('admin:index'))
        self.assertEqual(403, response.status_code)

    def test_staff_on_super_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.staff_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('admin:index'))
        self.assertEqual(403, response.status_code)

    def test_regular_user_on_super_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.regular_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('admin:index'))
        self.assertEqual(403, response.status_code)

    def test_super_user_on_competition_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.super_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('competition-admin:index'))
        self.assertEqual(200, response.status_code)

    def test_competition_admin_on_competition_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.compadmin_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('competition-admin:index'))
        self.assertEqual(200, response.status_code)

    def test_jury_member_on_competition_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.jury_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('competition-admin:index'))
        self.assertEqual(403, response.status_code)

    def test_staff_on_competition_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.staff_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('competition-admin:index'))
        self.assertEqual(403, response.status_code)

    def test_regular_user_on_competition_admin(self):
        """
        Test that only allowed members can login to the competition admin
        """

        login_url = reverse('login')

        response = self.client.post(login_url, self.regular_login, follow=True)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        response = self.client.get(reverse('competition-admin:index'))
        self.assertEqual(403, response.status_code)

    def test_jury_member_group(self):
        """
        Test that the 'jury-member' group is correctly set and assigned
        """

        # Make sure the group exist...
        jm_group = Group.objects.get(name=settings.JURY_MEMBERS_GROUP)
        # and has the correct permissions
        perms = ['ulysses_competitions.change_evaluation', ]

        # Create a new jury member
        u = User.objects.get(username='staff@ircam.fr')
        jm = JuryMember(user=u)
        jm.save()

        # Ensure user has the correct permissions
        user_perms = jm.user.get_all_permissions()
        for p in perms:
            self.assertTrue(p in user_perms)

        # Ensure that the new jury member is a member of jury-member group
        self.assertTrue(jm_group in jm.user.groups.all())
