# -*- coding: utf-8 -*-

"""unit tests"""

from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import Group
from django.core import mail
from django.urls import reverse

from ulysses.composers.factories import UserFactory

from ..models import CompetitionManager, CompetitionStatus, CandidateNotification, JuryMemberNotification
from ..factories import (
    CompetitionFactory, CandidateFactory, JuryMemberFactory, CompetitionStepFactory,
    EvaluationNoteFactory, EvaluationFieldFactory, EvaluationFactory,
)
from ..sites import evaluate_notification_message


class EvaluateNotificationMessage(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_basic_keys(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        body = """
            Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs
        """

        expected_body = """
            Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_unknown_keys(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        body = """
            Dear [[ UNKNOWN ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs
        """

        expected_body = """
            Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs
        """.format(
            '[[ UNKNOWN ]]',
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_comments(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()
        jury3 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="comments",
            label="Commentaire",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0,
            is_note=False
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Admin#1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Admin#2"
        )

        evaluation3 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury3, comments=""
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value="Comment#1")
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field1.key, value="Comment#2")

        body = """
            Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            [[ {0}.comments ]]

            Sincerely, Program Chairs
        """.format(competition_step.url)

        expected_body = """
            Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Comment#1

Comment#2

            Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_comments_key_comment(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()
        jury3 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="comment",
            label="Commentaire",
            weight=0,
            field_type="forms.TextField",
            widget_type="forms.Textarea",
            required=True,
            display_in_result_list=True,
            field_extra_args="",
            order_index=0,
            is_note=False
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Admin#1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Admin#2"
        )

        evaluation3 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury3, comments=""
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value="Comment#1")
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field1.key, value="Comment#2")

        body = """
            Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            [[ {0}.comments ]]

            Sincerely, Program Chairs
        """.format(competition_step.url)

        expected_body = """
            Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Comment#1

Comment#2

            Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_average(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()
        jury3 = JuryMemberFactory.create()

        field = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Comment 1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Comment 2"
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field.key, value="2")
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field.key, value="1")

        body = """
            Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Average: [[ {0}.average ]]

            Sincerely, Program Chairs
        """.format(competition_step.url, field.key)

        expected_body = """
            Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Average: 1.5

            Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_notes(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=9,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        field2 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note2",
            label="Note2",
            weight=1,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Comment 1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Comment 2"
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value=1)
        EvaluationNoteFactory.create(evaluation=evaluation1, key=field2.key, value=2)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field1.key, value=3)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field2.key, value=4)

        body = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Notes:
[[ {0}.notes ]]

Sincerely, Program Chairs
        """.format(competition_step.url)

        expected_body = """
Dear {0} {1},

On behalf of the {2} Committee,
we regret to inform you that your application has not been accepted for the {2}.
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Notes:
1.1
3.1

Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_notes_is_note(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        field2 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note2",
            label="Note2",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=False
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Comment 1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Comment 2"
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value=2)
        EvaluationNoteFactory.create(evaluation=evaluation1, key=field2.key, value=1)

        body = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Notes:
[[ {0}.notes ]]

Sincerely, Program Chairs
        """.format(competition_step.url, field1.key)

        expected_body = """
Dear {0} {1},

On behalf of the {2} Committee,
we regret to inform you that your application has not been accepted for the {2}.
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Notes:
2

Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_notes_by_fields(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=9,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        field2 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note2",
            label="Note2",
            weight=1,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Comment 1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Comment 2"
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value=1)
        EvaluationNoteFactory.create(evaluation=evaluation1, key=field2.key, value=2)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field1.key, value=3)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field2.key, value=4)

        body = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Field1:
[[ {0}.{1}.notes ]]

Field2:
[[ {0}.{2}.notes ]]

Sincerely, Program Chairs
        """.format(competition_step.url, field1.key, field2.key)

        expected_body = """
Dear {0} {1},

On behalf of the {2} Committee,
we regret to inform you that your application has not been accepted for the {2}.
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Field1:
1
3

Field2:
2
4

Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_average_several_notes(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=9,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        field2 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note2",
            label="Note2",
            weight=1,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        evaluation1 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury1, comments="Comment 1"
        )

        evaluation2 = EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate, jury_member=jury2, comments="Comment 2"
        )

        EvaluationNoteFactory.create(evaluation=evaluation1, key=field1.key, value=1)
        EvaluationNoteFactory.create(evaluation=evaluation1, key=field2.key, value=2)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field1.key, value=3)
        EvaluationNoteFactory.create(evaluation=evaluation2, key=field2.key, value=4)

        body = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Average
[[ {0}.average ]]

Field1:
[[ {0}.{1}.average ]]

Field2:
[[ {0}.{2}.average ]]

Sincerely, Program Chairs
        """.format(competition_step.url, field1.key, field2.key)

        expected_body = """
Dear {0} {1},

On behalf of the {2} Committee,
we regret to inform you that your application has not been accepted for the {2}.
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Average
2.1

Field1:
2

Field2:
3

Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)

    def test_average_no_notes(self):
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        competition_step = CompetitionStepFactory.create(competition=competition)

        candidate = CandidateFactory.create(competition=competition)

        jury1 = JuryMemberFactory.create()
        jury2 = JuryMemberFactory.create()

        field1 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=9,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        field2 = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note2",
            label="Note2",
            weight=1,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3'), (4, '4')] }",
            order_index=0,
            is_note=True
        )

        body = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Average
[[ {0}.average ]]

Field1:
[[ {0}.{1}.average ]]

Field2:
[[ {0}.{2}.average ]]

Sincerely, Program Chairs
        """.format(competition_step.url, field1.key, field2.key)

        expected_body = """
Dear {0} {1},

On behalf of the {2} Committee,
we regret to inform you that your application has not been accepted for the {2}.
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Average


Field1:


Field2:


Sincerely, Program Chairs
        """.format(
            candidate.composer.user.first_name,
            candidate.composer.user.last_name,
            competition_step.competition.title
        )

        evaluated_body = evaluate_notification_message(body, candidate, competition_step)

        self.assertEqual(expected_body, evaluated_body)


class NotifyCandidateTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        self.competition_admin_user = UserFactory.create(
            username="joe", password="joe", is_active=True, is_staff=True, email='joe@joe.fr'
        )
        self.competition_admin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        self.manager = CompetitionManager.objects.get(user=self.competition_admin_user)
        self.competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=True,
            title="Big Competition 2015"
        )
        self.competition.managed_by.add(self.manager)
        self.competition.save()

        self.client.login(username="joe", password="joe")

        session = self.client.session
        session[settings.ACTIVE_COMPETITION_KEY] = self.competition
        session.save()

    def test_view_notify_candidate(self):
        """test that notify candidate is correctly displayed"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate = CandidateFactory.create(competition=self.competition)

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += "?ids={0}".format(candidate.id)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, candidate.composer.user.last_name)

    def test_view_notify_several_candidates(self):
        """test that notify candidate is correctly displayed"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += "?ids={0},{1}".format(candidate1.id, candidate2.id)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, candidate1.composer.user.last_name)
        self.assertContains(response, candidate2.composer.user.last_name)
        self.assertNotContains(response, candidate3.composer.user.last_name)

    def test_view_notify_no_candidates(self):
        """test that notify candidate is correctly displayed"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate = CandidateFactory.create(competition=self.competition)

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_post_notify_several_candidates(self):
        """test notify candidate"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        for candidate in (candidate1, candidate2, candidate3):
            candidate.composer.user.email = '{0}@toto.fr'.format(candidate.composer.user.last_name)
            candidate.composer.user.save()

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += '?ids={0},{1}'.format(candidate1.id, candidate2.id)

        body = """Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        expected_body_template = """Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        data = {
            'send_message': 'send_message',
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body,
            'cc_me': False,
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(2, CandidateNotification.objects.count())

        for email, candidate in zip(mail.outbox, (candidate1, candidate2)):

            expected_body = expected_body_template.format(
                candidate.composer.user.first_name,
                candidate.composer.user.last_name,
                competition_step.competition.title
            )

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])
            self.assertEqual(email.body, expected_body)
            self.assertEqual(email.to, [candidate.composer.user.email])
            self.assertEqual(email.cc, [])

            self.assertEqual(1, CandidateNotification.objects.filter(candidate=candidate).count())
            notif = CandidateNotification.objects.get(candidate=candidate)
            self.assertEqual(notif.is_test, False)

    def test_test_notify_several_candidates(self):
        """test notify candidate"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        for candidate in (candidate1, candidate2, candidate3):
            candidate.composer.user.email = '{0}@toto.fr'.format(candidate.composer.user.last_name)
            candidate.composer.user.save()

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += '?ids={0},{1}'.format(candidate1.id, candidate2.id)

        body = """Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        expected_body_template = """Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        data = {
            'send_test_message': 'send_test_message',
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(1, CandidateNotification.objects.count())

        for email, candidate in zip(mail.outbox, (candidate1,)):

            expected_body = expected_body_template.format(
                candidate.composer.user.first_name,
                candidate.composer.user.last_name,
                competition_step.competition.title
            )

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])
            self.assertEqual(email.body, expected_body)
            self.assertEqual(email.to, [data['from_email']])
            self.assertEqual(1, CandidateNotification.objects.filter(candidate=candidate).count())

    def test_post_notify_test(self):
        """test notify candidate"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        for candidate in (candidate1, candidate2, candidate3):
            candidate.composer.user.email = '{0}@toto.fr'.format(candidate.composer.user.last_name)
            candidate.composer.user.save()

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += '?ids={0},{1}'.format(candidate1.id, candidate2.id)

        body = """
        Dear [[ first_name ]] [[ last_name ]],

        On behalf of the [[ Competitionname ]] Committee,
        we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
        At the end of this email, you will find your results (based on novelty, musical quality, practicality,
         and past experience), as well as some comments for you from anonymous reviewers.

        Sorry!!

        Sincerely, Program Chairs"""

        expected_body_template = """Dear {0} {1},

        On behalf of the {2} Committee,
        we regret to inform you that your application has not been accepted for the {2}.
        At the end of this email, you will find your results (based on novelty, musical quality, practicality,
         and past experience), as well as some comments for you from anonymous reviewers.

        Sorry!!

        Sincerely, Program Chairs"""

        #

        data = {
            'send_test_message': 'send_test_message',
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(1, CandidateNotification.objects.count())

        for email, candidate in zip(mail.outbox, (candidate1,)):

            expected_body = expected_body_template.format(
                candidate.composer.user.first_name,
                candidate.composer.user.last_name,
                competition_step.competition.title
            )

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])

            self.assertEqual(email.body, expected_body)
            self.assertEqual(email.to, [data['from_email']])

            self.assertEqual(1, CandidateNotification.objects.filter(candidate=candidate).count())
            notif = CandidateNotification.objects.get(candidate=candidate)
            self.assertEqual(notif.is_test, True)

    def test_post_notify_several_candidates_cc(self):
        """test notify candidate"""

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        for candidate in (candidate1, candidate2, candidate3):
            candidate.composer.user.email = '{0}@toto.fr'.format(candidate.composer.user.last_name)
            candidate.composer.user.save()

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += '?ids={0},{1}'.format(candidate1.id, candidate2.id)

        body = """Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        expected_body_template = """Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        data = {
            'send_message': 'send_message',
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body,
            'cc_me': True,
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(2, CandidateNotification.objects.count())

        for email, candidate in zip(mail.outbox, (candidate1, candidate2)):

            expected_body = expected_body_template.format(
                candidate.composer.user.first_name,
                candidate.composer.user.last_name,
                competition_step.competition.title
            )

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])
            self.assertEqual(email.body, expected_body)
            self.assertEqual(email.to, [candidate.composer.user.email])
            self.assertEqual(email.cc, [data['from_email']])

            self.assertEqual(1, CandidateNotification.objects.filter(candidate=candidate).count())
            notif = CandidateNotification.objects.get(candidate=candidate)
            self.assertEqual(notif.is_test, False)

    def test_post_notify_several_candidates_cc_no_email(self):
        """test notify candidate"""

        self.competition_admin_user.email = ''
        self.competition_admin_user.save()

        competition_step = CompetitionStepFactory.create(competition=self.competition)

        candidate1 = CandidateFactory.create(competition=self.competition)
        candidate2 = CandidateFactory.create(competition=self.competition)
        candidate3 = CandidateFactory.create(competition=self.competition)

        for candidate in (candidate1, candidate2, candidate3):
            candidate.composer.user.email = '{0}@toto.fr'.format(candidate.composer.user.last_name)
            candidate.composer.user.save()

        url = reverse('competition-admin:step_notify_candidates', args=[competition_step.url])
        url += '?ids={0},{1}'.format(candidate1.id, candidate2.id)

        body = """Dear [[ first_name ]] [[ last_name ]],

            On behalf of the [[ Competitionname ]] Committee,
            we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        expected_body_template = """Dear {0} {1},

            On behalf of the {2} Committee,
            we regret to inform you that your application has not been accepted for the {2}.
            At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

            Sincerely, Program Chairs"""

        data = {
            'send_message': 'send_message',
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body,
            'cc_me': True,
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(2, CandidateNotification.objects.count())

        for email, candidate in zip(mail.outbox, (candidate1, candidate2)):

            expected_body = expected_body_template.format(
                candidate.composer.user.first_name,
                candidate.composer.user.last_name,
                competition_step.competition.title
            )

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])
            self.assertEqual(email.body, expected_body)
            self.assertEqual(email.to, [candidate.composer.user.email])
            self.assertEqual(email.cc, [data['from_email']])

            self.assertEqual(1, CandidateNotification.objects.filter(candidate=candidate).count())
            notif = CandidateNotification.objects.get(candidate=candidate)
            self.assertEqual(notif.is_test, False)

    def test_post_notify_jury_members(self):
        """test notify candidate"""

        self.competition_admin_user.email = ''
        self.competition_admin_user.save()

        competition_step = CompetitionStepFactory.create(competition=self.competition)
        candidate = CandidateFactory.create(competition=self.competition)

        jury_member1 = JuryMemberFactory.create()
        jury_member2 = JuryMemberFactory.create()
        jury_member3 = JuryMemberFactory.create()

        for jury_member in (jury_member1, jury_member2, jury_member3):

            EvaluationFactory.create(
                competition_step=competition_step, jury_member=jury_member, candidate=candidate
            )

            jury_member.competitions.add(self.competition)
            jury_member.save()
            jury_member.user.email = '{0}@toto.fr'.format(jury_member.user.last_name)
            jury_member.user.save()

        url = reverse('competition-admin:step_notify_jury_members', args=[competition_step.url])
        url += '?ids={0},{1}'.format(jury_member1.id, jury_member2.id)

        body = """Dear jury member,
            Please connect to Ulysses platform to evaluate '15th "Pablo Sorozabal" 
            International Composition Competition' Evaluations (Technology criteria 1) candidates.
        """

        data = {
            'from_email': 'toto@toto.fr',
            'subject': 'This is unit test',
            'body': body,
            'cc_me': True,
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)

        self.assertEqual(len(mail.outbox), 2)
        self.assertEqual(2, JuryMemberNotification.objects.count())

        for email, jury_member in zip(mail.outbox, (jury_member1, jury_member2)):

            self.assertEqual(email.subject, data['subject'])
            self.assertEqual(email.from_email, data['from_email'])
            self.assertTrue("Dear jury member" in email.body)
            self.assertEqual(email.to, [jury_member.user.email])
            self.assertEqual(email.cc, [data['from_email']])

            self.assertEqual(1, JuryMemberNotification.objects.filter(jury_member=jury_member).count())
