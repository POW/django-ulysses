# -*- coding: utf-8 -*-

"""unit tests"""

from django.conf import settings
from django.contrib.auth.models import Group
from django.urls import reverse

from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.generic.tests import BaseTestCase

from ..models import CompetitionManager, CandidateBiographicElement, ApplicationFormFieldType
from ..factories import (
    CompetitionFactory, CompetitionStepFactory, DynamicApplicationFormFactory, EvaluationFieldFactory,
    ApplicationFormFieldSetDefinitionFactory, ApplicationFormFieldDefinitionFactory, CandidateFactory, ComposerFactory,
    EvaluationFactory, JuryMemberFactory, JuryMemberCompetitionFactory
)


class ExcelExportTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def _create_manager(self):
        admin_profile = IndividualFactory.create()
        admin_user = admin_profile.user
        admin_user.is_staff = True
        admin_user.save()
        admin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        manager = CompetitionManager.objects.get(user=admin_user)
        return manager

    def _create_competition(self, comments="test-comments", step_name="Step"):
        # Create competition & concrete applicationform
        form = DynamicApplicationFormFactory()
        fieldset1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        fieldset2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        ApplicationFormFieldDefinitionFactory.create(parent=fieldset1, name="notes")
        ApplicationFormFieldDefinitionFactory.create(parent=fieldset2, name="about")
        biography_field_kind = ApplicationFormFieldType.objects.get(name="BiographicElementField")
        ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset2, kind=biography_field_kind, name="biography"
        )

        # competition
        competition = CompetitionFactory.create(use_dynamic_form=True, dynamic_application_form=form)

        # Create a competition step
        competition_step = CompetitionStepFactory.create(competition=competition, name=step_name)

        # Create evaluation fields for this competition step
        evaluation_field = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        individual1 = IndividualFactory.create()
        individual2 = IndividualFactory.create()
        organization1 = OrganizationFactory.create()

        composer1 = ComposerFactory.create(user=individual1.user)
        composer2 = ComposerFactory.create(user=individual2.user)
        composer3 = ComposerFactory.create(user=organization1.user)

        candidate1 = CandidateFactory.create(composer=composer1, competition=competition)
        candidate2 = CandidateFactory.create(composer=composer3, competition=competition)

        jury_member1 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member1, competition=competition)

        # comments
        EvaluationFactory.create(
            competition_step=competition_step, candidate=candidate1, jury_member=jury_member1, comments=comments
        )

        return competition

    def test_export_competition(self):
        manager = self._create_manager()
        competition = self._create_competition()
        competition.managed_by.add(manager)
        competition.save()
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_export_competition_anonymous(self):
        manager = self._create_manager()
        competition = self._create_competition()
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_export_competition_non_manager(self):
        manager = self._create_manager()
        competition = self._create_competition()
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_export_competition_text_limit(self):
        manager = self._create_manager()
        competition = self._create_competition(comments="a" * 32768)
        competition.managed_by.add(manager)
        competition.save()
        individual = IndividualFactory.create()
        composer = ComposerFactory.create(user=individual.user)
        candidate = CandidateFactory.create(composer=composer, competition=competition)
        CandidateBiographicElement.objects.create(
            candidate=candidate, key='biography', text="a" * 32768, title="Title"
        )
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_export_competition_html_limit(self):
        manager = self._create_manager()
        competition = self._create_competition()
        competition.managed_by.add(manager)
        competition.save()
        individual = IndividualFactory.create()
        composer = ComposerFactory.create(user=individual.user)
        candidate = CandidateFactory.create(composer=composer, competition=competition)
        CandidateBiographicElement.objects.create(
            candidate=candidate, key='biography', text="<p>" + ("a" * 32765) + "</p>", title="Title"
        )
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_export_competition_invalid_sheet_name(self):
        manager = self._create_manager()
        competition = self._create_competition(step_name="a:[]" + ("b" * 32))
        competition.managed_by.add(manager)
        competition.save()
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

    def test_export_competition_invalid_sheet_name2(self):
        manager = self._create_manager()
        competition = self._create_competition(step_name='Evaluations (Technology criteria 1)')
        competition.managed_by.add(manager)
        competition.save()
        url = reverse('competitions:export_xls_competition', args=[competition.id])
        self.assertEqual(True, self.client.login(email=manager.user.email, password=1234))
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

