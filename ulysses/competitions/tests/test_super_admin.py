# -*- coding: utf-8 -*-

"""unit tests"""

import datetime
from datetime import timedelta

from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.urls import reverse
from django.utils.encoding import force_text

from ulysses.partners.factories import PartnerFactory
from ulysses.composers.factories import UserFactory

from ..forms import IrcamCursus1ApplicationForm
from ..models import CompetitionManager, CompetitionStatus, Competition
from ..factories import (
    CompetitionFactory, CompetitionAttributeFactory, DynamicApplicationFormFactory,
    ApplicationFormFieldSetDefinitionFactory, ApplicationFormFieldDefinitionFactory,
)


class SuperAdminTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        self.super_login = { # superuser
            # REDIRECT_FIELD_NAME: '/admin/infos/',
            'username': 'admin',
            'password': 'admin',
        }
        self.staff_login = {  # superuser
            # REDIRECT_FIELD_NAME: '/admin/infos/',
            'username': 'staff',
            'password': 'staff',
        }

        super_user = UserFactory.create(username=self.super_login['username'], password=self.super_login['password'])
        super_user.is_superuser = True
        super_user.is_staff = True
        super_user.is_active = True
        super_user.save()

        staff_user = UserFactory.create(username=self.staff_login['username'], password=self.staff_login['password'])
        staff_user.is_superuser = False
        staff_user.is_staff = True
        staff_user.is_active = True
        staff_user.save()

    def test_super_admin_competitions_app(self):
        """
        Test the access to the competitions app through the super-admin
        """

        logged_in = self.client.login(username=self.super_login['username'], password=self.super_login['password'])
        self.assertTrue(logged_in)

        resp = self.client.get('/super-admin/ulysses_competitions/competition/')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/super-admin/ulysses_competitions/competition/add/')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/add/')
        self.assertEqual(resp.status_code, 200)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/add/')
        self.assertEqual(resp.status_code, 200)

    def test_super_admin_competitions_app_as_staff(self):
        """
        Test the access to the competitions app through the super-admin
        """

        logged_in = self.client.login(username=self.staff_login['username'], password=self.staff_login['password'])
        self.assertTrue(logged_in)

        resp = self.client.get('/super-admin/ulysses_competitions/competition/')
        self.assertEqual(resp.status_code, 403)

        resp = self.client.get('/super-admin/ulysses_competitions/competition/add/')
        self.assertEqual(resp.status_code, 403)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/')
        self.assertEqual(resp.status_code, 403)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/add/')
        self.assertEqual(resp.status_code, 403)

        resp = self.client.get('/super-admin/ulysses_competitions/jurymember/add/')
        self.assertEqual(resp.status_code, 403)

    def test_competition_attribute_creation(self, ):

        logged_in = self.client.login(username=self.super_login['username'], password=self.super_login['password'])
        self.assertTrue(logged_in)

        competition = CompetitionFactory.create(status=CompetitionStatus.objects.get(name="open"))
        competition_attribute = CompetitionAttributeFactory.create(competition=competition)

        url = reverse('admin:ulysses_competitions_competitionattribute_changelist')
        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)


class CompetitionCreationTest(TestCase):
    """ Tests competition creation via super-admin interface & competition administration"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):

        # Create super user

        self.superuser_infos = { # superuser
            'username': 'admin',
            'password': 'admin',
        }

        self.super_user = UserFactory.create(username=self.superuser_infos['username'],
                                        password=self.superuser_infos['password'])
        self.super_user.is_superuser = True
        self.super_user.is_staff = True
        self.super_user.is_active = True
        self.super_user.save()

        # Create competition administrator

        self.compadmin_infos = { # staff, member of "competition-admins"
            'username': 'compadmin',
            'password': 'compadmin',
        }

        self.compadmin_user = UserFactory.create(username=self.compadmin_infos['username'],
                                            password=self.compadmin_infos['password'])
        self.compadmin_user.is_active = True
        self.compadmin_user.is_staff = True
        self.compadmin_user.save()
        self.compadmin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        self.compadmin_manager = CompetitionManager.objects.get(user=self.compadmin_user)

    def add_formset_management_form_to_post_data(self, post_data, formset_name, total_forms=1, initial_forms=0):
        post_data['%s-INITIAL_FORMS' % formset_name] = force_text(initial_forms)
        post_data['%s-TOTAL_FORMS' % formset_name] = force_text(total_forms)
        post_data['%s-MAX_NUM_FORMS' % formset_name] = ''

    def test_competition_creation(self):

        # Log in as super-user
        logged_in = self.client.login(
            username=self.superuser_infos['username'], password=self.superuser_infos['password']
        )
        self.assertTrue(logged_in)

        # Add a competition
        add_competition_url = reverse('admin:ulysses_competitions_competition_add')
        view_competitions_url = reverse('admin:ulysses_competitions_competition_changelist')

        date_format = '%Y-%m-%d'
        time_format = '%H:%M:%S'
        now = datetime.datetime.now()
        publication_date = now + timedelta(days=1)
        opening_date = now + timedelta(days=30)
        closing_date = now + timedelta(days=60)
        result_date = now + timedelta(days=90)
        unpublish_date = now + timedelta(days=120)
        url = 'sample-competition'
        title = "Sample competition"
        partner = PartnerFactory.create()
        self.assertTrue(partner.pk)

        post_data = {
            'publication_date_0': publication_date.strftime(date_format),
            'opening_date_0': opening_date.strftime(date_format),
            'closing_date_0': closing_date.strftime(date_format),
            'result_date_0': result_date.strftime(date_format),
            'unpublish_date_0': unpublish_date.strftime(date_format),
            'publication_date_1': publication_date.strftime(time_format),
            'opening_date_1': opening_date.strftime(time_format),
            'closing_date_1': closing_date.strftime(time_format),
            'result_date_1': result_date.strftime(time_format),
            'unpublish_date_1': unpublish_date.strftime(time_format),
            'url': url,
            'title': title,
            'subtitle': "Competition subtitle here...",
            'presentation': "Competition summary here...",
            'status': CompetitionStatus.objects.get(name='new').pk,
            'application_form_class': IrcamCursus1ApplicationForm,
            'use_dynamic_form': False,
            'organized_by': partner.pk,
            "email": "toto@toto.fr",
        }
        self.add_formset_management_form_to_post_data(
            post_data, 'competitionstep_set', total_forms=0, initial_forms=0
        )
        self.add_formset_management_form_to_post_data(
            post_data, 'competitionattribute_set', total_forms=0, initial_forms=0
        )

        response = self.client.post(add_competition_url, post_data)
        self.assertRedirects(response, view_competitions_url)

        # Ensure competition has been created
        competition = Competition.objects.get(url=url)
        self.assertTrue(competition.pk)

        # Allow our competition admin to manage this competition
        competition.managed_by.add(self.compadmin_manager)

        # Logout
        self.client.logout()

        # Ensure competition is publicly visible in Draft
        public_url = reverse('web:competition', args=[url])
        response = self.client.get(public_url)
        self.assertNotContains(response, "Competition not found")
        self.assertContains(response, title)

        # Log in as competition administrator
        logged_in = self.client.login(username=self.compadmin_infos['username'],
                                      password=self.compadmin_infos['password'])
        self.assertTrue(logged_in)

        # Select competition to administrate
        select_competition_url = reverse('competition-admin:select_competition', args=[competition.pk])
        response = self.client.get(select_competition_url)
        self.assertEqual(response.status_code, 302)
        competition_admin_url = reverse('competition-admin:show_informations')

        competition_admin_response = self.client.get(competition_admin_url)
        self.assertContains(competition_admin_response, title)
        self.assertContains(competition_admin_response, "Informations")
        self.assertContains(competition_admin_response, "News")
        self.assertContains(competition_admin_response, "Candidates")
        self.assertContains(competition_admin_response, "Jury members")

        # # Change competition status to 'published'
        # updated_post_data = post_data.copy()
        # updated_post_data['status'] = CompetitionStatus.objects.get(name='published').pk,
        # self.client.post(competition_admin_url, updated_post_data)
        #
        # # Ensure status has been updated
        # reloaded_competition = Competition.objects.get(url=url)
        # self.assertEquals(reloaded_competition.status.name, 'published')

        # Logout
        self.client.logout()

        # Ensure competition is publicly visible
        public_url = reverse('web:competition', args=[url])
        response = self.client.get(public_url)
        self.assertContains(response, title)


class ApplicationFormFieldSetDefinitionTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_key_valid(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "notes"
        field.full_clean()

    def test_key_invalid_with_spaces_before(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = " notes"
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_invalid_with_spaces_after(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "notes "
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_invalid_with_spaces_inside(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "no tes"
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_valid_with_numbers(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "notes2"
        field.full_clean()

    def test_key_valid_with_underscore(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "no_tes"
        field.full_clean()

    def test_key_valid_with_one_char(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = "a"
        field.full_clean()

    def test_key_invalid_if_empty(self):
        field = ApplicationFormFieldSetDefinitionFactory.create()
        field.name = ""
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_duplicate_set_same_form(self):
        form = DynamicApplicationFormFactory()

        field1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field1.name = "notes"
        field1.full_clean()
        field1.save()

        field2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field2.name = "notes"

        self.assertRaises(ValidationError, lambda: field2.full_clean())

    def test_key_same_name_set_different_form(self):
        form1 = DynamicApplicationFormFactory()
        form2 = DynamicApplicationFormFactory()

        field1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form1)
        field1.name = "notes"
        field1.full_clean()
        field1.save()

        field2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form2)
        field2.name = "notes"
        field2.full_clean()

    def test_key_duplicate_same_form(self):
        form = DynamicApplicationFormFactory()
        fieldset1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        fieldset2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)

        field1 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset1)
        field1.name = "notes"
        field1.full_clean()
        field1.save()

        field2 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset2)
        field2.name = "notes"

        self.assertRaises(ValidationError, lambda: field2.full_clean())

    def test_key_duplicate_same_form_same_fieldset(self):
        form = DynamicApplicationFormFactory()
        fieldset1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)

        field1 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset1)
        field1.name = "notes"
        field1.full_clean()
        field1.save()

        field2 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset1)
        field2.name = "notes"

        self.assertRaises(ValidationError, lambda: field2.full_clean())

    def test_key_same_name_different_form(self):
        form1 = DynamicApplicationFormFactory()
        form2 = DynamicApplicationFormFactory()
        fieldset1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form1)
        fieldset2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form2)

        field1 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset1)
        field1.name = "notes"
        field1.full_clean()
        field1.save()

        field2 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset2)
        field2.name = "notes"
        field2.full_clean()


class ApplicationFormFieldDefinitionTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_key_valid(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "notes"
        field.full_clean()

    def test_key_invalid_with_spaces_before(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = " notes"
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_invalid_with_spaces_after(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "notes "
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_invalid_with_spaces_inside(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "no tes"
        self.assertRaises(ValidationError, lambda: field.full_clean())

    def test_key_valid_with_numbers(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "notes2"
        field.full_clean()

    def test_key_valid_with_underscore(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "no_tes"
        field.full_clean()

    def test_key_valid_with_one_char(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = "a"
        field.full_clean()

    def test_key_invalid_if_empty(self):
        field = ApplicationFormFieldDefinitionFactory.create()
        field.name = ""
        self.assertRaises(ValidationError, lambda: field.full_clean())
