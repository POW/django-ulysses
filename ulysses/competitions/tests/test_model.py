# -*- coding: utf-8 -*-

"""unit tests"""

from datetime import datetime, timedelta

from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import Group

from ..factories import (
    CompetitionFactory, UserFactory, CompetitionStepFactory, EvaluationFieldFactory, DynamicApplicationFormFactory,
    ApplicationFormFieldSetDefinitionFactory, ApplicationFormFieldDefinitionFactory
)
from ..models import CompetitionManager
from ..utils import clone_competition


class CompetitionModelTest(TestCase):
    """ Tests competition management (jury / candidates) via competition administration"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_closing_soon(self):
        """ensure closing shoon is shown"""
        opening_date = datetime.now() - timedelta(days=30)
        closing_date = datetime.now() + timedelta(days=6)
        competition = CompetitionFactory.create(open=True)
        competition.opening_date = opening_date
        competition.closing_date = closing_date
        competition.save()
        self.assertEqual(competition.closing_soon(), True)

    def test_not_closing_soon(self):
        """ensure closing shoon is shown"""
        opening_date = datetime.now() - timedelta(days=30)
        closing_date = datetime.now() + timedelta(days=8)
        competition = CompetitionFactory.create(open=True)
        competition.opening_date = opening_date
        competition.closing_date = closing_date
        competition.save()
        self.assertEqual(competition.closing_soon(), False)

    def test_already_closed(self):
        """ensure closing shoon is shown"""
        opening_date = datetime.now() - timedelta(days=30)
        closing_date = datetime.now() - timedelta(days=6)
        competition = CompetitionFactory.create(archived=True)
        competition.opening_date = opening_date
        competition.closing_date = closing_date
        competition.save()
        self.assertEqual(competition.closing_soon(), False)


class CloneCompetitionTest(TestCase):
    """ Tests competition management (jury / candidates) via competition administration"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_clone_competition(self):
        admin_user = UserFactory.create()
        admin_user.is_active = True
        admin_user.is_staff = True
        admin_user.save()
        admin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        manager = CompetitionManager.objects.get(user=admin_user)

        # Create competition & concrete applicationform
        form = DynamicApplicationFormFactory()

        fieldset1 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        fieldset2 = ApplicationFormFieldSetDefinitionFactory.create(parent=form)

        field1 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset1)
        field1.name = "notes"
        field1.save()

        field2 = ApplicationFormFieldDefinitionFactory.create(parent=fieldset2)
        field2.name = "about"
        field2.save()

        # competition
        competition = CompetitionFactory.create(use_dynamic_form=True, dynamic_application_form=form)
        competition.managed_by.add(manager)

        # Create a competition step
        competition_step = CompetitionStepFactory.create(competition=competition)

        # Create evaluation fields for this competition step
        evaluation_field = EvaluationFieldFactory.create(
            competition_step=competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        clone = clone_competition(competition)
        self.assertNotEqual(clone.id, competition.id)
        self.assertNotEqual(clone.dynamic_application_form.id, competition.dynamic_application_form.id)


class CompetitionRawText(TestCase):

    def test_raw_text_with_lines(self):
        html_text = '''
        LATE APPLICATION: <br>
        Flute<br>
        Clarinet<br>
        Violin<br>
        Piano <br>
        <br>
        TEDARIM - M.Mus. program for contemporary music performance<br>
        27 Oct 2019 - 30 Jun 2021 <br>
        <br>
        '''
        competition = CompetitionFactory.create(presentation=html_text)
        self.assertEqual(competition.safe_html, html_text.strip())

    def test_raw_text_with_script(self):
        html_text = '''
        LATE APPLICATION: <br>
        <script>something evil</script>
        <br>
        '''
        competition = CompetitionFactory.create(presentation=html_text)
        self.assertEqual(
            competition.safe_html,
            html_text.strip().replace('<script>something evil</script>', 'something evil')
        )
