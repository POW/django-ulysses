# -*- coding: utf-8 -*-
"""unit tests"""

from bs4 import BeautifulSoup
from datetime import date
from os.path import abspath, dirname, join as path_join

from django.contrib.auth.models import AnonymousUser
from django.core import mail
from django.test.client import RequestFactory
from django.test.utils import override_settings
from django.urls import reverse

from ulysses.competitions.factories import StaticContentFactory
from ulysses.competitions.models import CallRequest, StaticContent
from ulysses.generic.tests import BaseTestCase
from ulysses.profiles.factories import IndividualFactory

from ..views import upload_call_file


@override_settings(MANAGERS=(('Boss', 'boss@ircam.fr'), ))
class OrganizeCallTest(BaseTestCase):
    """ Tests Organize Call form"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def create_call_file(self, user):

        base_dir = abspath(dirname(__file__))
        img_path = path_join(base_dir, "fixtures", 'img.png')

        file_to_upload = open(img_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"Filedata": file_to_upload})
        upload_request.user = user

        upload_response = upload_call_file(upload_request)
        return upload_response.content.decode('utf-8')

    def test_view_call_guidelines(self):
        url = reverse('competitions:show_call_guidelines')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, StaticContent.objects.filter(key='call_guidelines').count())
        self.assertContains(response, reverse('competitions:organize_call'))

    def test_view_call_guidelines_existing(self):
        content = StaticContentFactory.create(key='call_guidelines', text='How to call me')
        url = reverse('competitions:show_call_guidelines')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, StaticContent.objects.filter(key='call_guidelines').count())
        self.assertContains(response, content.text)
        self.assertContains(response, reverse('competitions:organize_call'))

    def test_view_organize_calll(self):
        """it should show form"""
        url = reverse('competitions:organize_call')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(len(soup.select("#id_first_name")), 1)
        self.assertEqual(0, CallRequest.objects.count())
        self.assertEqual(len(mail.outbox), 0)

    def test_post_organize_call_anonymous(self):
        """it should redirect to login page"""
        filename = self.create_call_file(AnonymousUser())
        data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'organization_name': 'ACME',
            'country': 'France',
            'email': 'john.doe@mailinator.com',
            'call_description': "Lorem ipsum",
            'picture_media': "test.png",
            'picture_media_path': filename,
        }
        url = reverse('competitions:organize_call')
        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, reverse('competitions:show_call_guidelines'))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, ['boss@ircam.fr'])
        self.assertEqual(1, CallRequest.objects.count())
        call = CallRequest.objects.all()[0]
        data.pop('picture_media')
        data.pop('picture_media_path')
        for field in data:
            self.assertEqual(getattr(call, field), data[field])
        self.assertEqual(date.today(), call.request_datetime.date())
        self.assertEqual(False, call.processed)
        self.assertNotEqual(call.picture, None)
        self.assertEqual(call.picture.name, 'calls/0/test.png')

    def test_post_organize_call_logged(self):
        """it should redirect to login page"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")
        filename = self.create_call_file(individual.user)
        data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'organization_name': 'ACME',
            'country': 'France',
            'email': 'john.doe@mailinator.com',
            'call_description': "Lorem ipsum",
            'picture_media': "test.png",
            'picture_media_path': filename,
        }
        url = reverse('competitions:organize_call')
        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, reverse('competitions:show_call_guidelines'))
        self.assertEqual(len(mail.outbox), 1)
        self.assertEqual(mail.outbox[0].to, ['boss@ircam.fr'])
        self.assertEqual(1, CallRequest.objects.count())
        call = CallRequest.objects.all()[0]
        data.pop('picture_media')
        data.pop('picture_media_path')
        for field in data:
            self.assertEqual(getattr(call, field), data[field])
        self.assertEqual(date.today(), call.request_datetime.date())
        self.assertEqual(False, call.processed)
        self.assertNotEqual(call.picture, None)
        self.assertEqual(call.picture.name, 'calls/1/test.png')

    def test_post_empty_name(self):
        """it should send message to admin"""
        data = {
            'first_name': '',
            'last_name': '',
            'organization_name': '',
            'country': '',
            'email': '',
            'call_description': "",
            'picture_media': "",
        }
        url = reverse('competitions:organize_call')
        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(0, CallRequest.objects.count())
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(len(soup.select("#id_first_name")), 1)
        self.assertEqual(len(soup.select(".field-error")), len(data))

    def test_post_organize_missing_image(self):
        """it should redirect to login page"""
        data = {
            'first_name': 'John',
            'last_name': 'Doe',
            'organization_name': 'ACME',
            'country': 'France',
            'email': 'john.doe@mailinator.com',
            'call_description': "Lorem ipsum",
            'picture_media': "",
        }
        url = reverse('competitions:organize_call')
        response = self.client.post(url, data=data, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(0, CallRequest.objects.count())
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(len(soup.select("#id_picture_media")), 1)
        self.assertEqual(len(soup.select(".field-error")), 1)
