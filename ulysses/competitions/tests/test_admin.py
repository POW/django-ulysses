# -*- coding: utf-8 -*-

"""unit tests"""

import json

from django.conf import settings
from django.test import TestCase
from django.contrib.auth.models import Group, User
from django.core import mail
from django.urls import reverse

from ulysses.composers.factories import UserFactory
from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.profiles.models import Individual, Organization


from ..models import (
    JuryMember, CompetitionManager, CompetitionStatus, CompetitionStep, Evaluation, JuryMemberGroup, CandidateGroup,
    Candidate, EvaluationStatus, CandidateBiographicElement,
)
from ..factories import (
    CompetitionFactory, CandidateFactory, JuryMemberFactory, CompetitionStepFactory, JuryMemberCompetitionFactory,
    EvaluationNoteFactory, EvaluationFieldFactory, EvaluationFactory, CandidateGroupFactory,
    JuryMemberGroupFactory, CompetitionManagerFactory
)


class CompetitionManagementTest(TestCase):
    """ Tests competition management (jury / candidates) via competition administration"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):

        # Create competition administrator

        self.compadmin_infos = { # staff, member of "competition-admins"
            'username': 'compadmin',
            'password': 'compadmin',
        }

        self.compadmin_user = UserFactory.create(
            username=self.compadmin_infos['username'], password=self.compadmin_infos['password']
        )
        self.compadmin_user.is_active = True
        self.compadmin_user.is_staff = True
        self.compadmin_user.save()
        self.compadmin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        self.compadmin_manager = CompetitionManager.objects.get(user=self.compadmin_user)

        # Create competition & concrete applicationform

        self.competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"), use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )
        self.assertTrue(self.competition)

        # Create a competition step
        self.competition_step = CompetitionStepFactory.create(competition=self.competition)

        # Create evaluation fields for this competition step

        self.evaluation_field = EvaluationFieldFactory.create(
            competition_step=self.competition_step,
            key="note",
            label="Note",
            weight=0,
            field_type="forms.ChoiceField",
            widget_type="forms.Select",
            required=True,
            display_in_result_list=True,
            field_extra_args="{  'choices': [(1, '1'), (2, '2'), (3, '3')] }",
            order_index=0
        )

        # Allow our competition admin to manage this competition
        self.competition.managed_by.add(self.compadmin_manager)
        self.competition.save()

        # Log in as competition administrator & select competition

        logged_in = self.client.login(username=self.compadmin_infos['username'],
                                      password=self.compadmin_infos['password'])
        self.assertTrue(logged_in)

        # Select competition to administrate
        select_competition_url = reverse('competition-admin:select_competition', args=[self.competition.pk])
        response = self.client.get(select_competition_url)
        self.assertEqual(response.status_code, 302)

        # Create some candidates for this competition
        self.candidates = CandidateFactory.create_batch(competition=self.competition, size=10)

        # Create some jury members & associate them to candidates
        self.jury_members = JuryMemberFactory.create_batch(size=3)

        # Create evaluations & notes for these candidates (for all jury members)
        for candidate in self.candidates:
            for jury_member in self.jury_members:
                evaluation = EvaluationFactory.create(
                    candidate=candidate, jury_member=jury_member, competition_step=self.competition_step,
                    status="completed"
                )
                EvaluationNoteFactory.create(evaluation=evaluation, key=self.evaluation_field.key, value="3")

    def test_infos_as_anonymous(self):
        # Ensure candidates can be viewed
        self.client.logout()
        response = self.client.get(reverse('competition-admin:show_informations'))
        self.assertEqual(response.status_code, 302)

    def test_infos(self):
        # Ensure candidates can be viewed
        response = self.client.get(reverse('competition-admin:show_informations'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['truncated_cells_in_excel'], 0)
        self.assertNotContains(response, 'biography field(s) have more more than 32 667 characters')

    def test_infos_long_biography(self):
        # Ensure candidates can be viewed
        candidate = self.candidates[0]
        CandidateBiographicElement.objects.create(
            candidate=candidate, key='education', text="a" * 32768, title="Title"
        )
        response = self.client.get(reverse('competition-admin:show_informations'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['truncated_cells_in_excel'], 1)
        self.assertContains(response, 'biography field(s) have more more than 32 667 characters')

    def test_infos_long_biography_html(self):
        # Ensure candidates can be viewed
        candidate = self.candidates[0]
        html = "<p>" + ("a" * 32765) + "</p>"
        CandidateBiographicElement.objects.create(
            candidate=candidate, key='education', text=html, title="Title"
        )
        response = self.client.get(reverse('competition-admin:show_informations'))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['truncated_cells_in_excel'], 0)
        self.assertNotContains(response, 'biography field(s) have more more than 32 667 characters')

    def test_candidate_view(self):
        # Ensure candidates can be viewed
        for candidate in self.candidates:
            view_candidate_url = reverse('competition-admin:edit_candidate', args=[candidate.pk])
            resp = self.client.get(view_candidate_url)
            self.assertEqual(resp.status_code, 200)

    def test_follow_results(self):
        follow_results_url = reverse('competition-admin:step_follow_results', args=[self.competition_step.url])
        resp = self.client.get(follow_results_url)
        self.assertEqual(resp.status_code, 200)

    def test_export_results(self):
        url = reverse('competition-admin:step_follow_results', args=[self.competition_step.url])
        export_results_url = url + '?export'
        resp = self.client.get(export_results_url)
        self.assertTrue("attachment" in resp["Content-Disposition"])
        self.assertTrue("export.xls" in resp["Content-Disposition"])

    def test_jury_member_creation(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"

        self.assertEqual(0, Individual.objects.filter(user__email=email).count())

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            "email": email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        self.assertRedirects(resp, reverse('competition-admin:show_jury_members'))

        # Ensure jury member has been created
        member = JuryMember.objects.get(competitions=self.competition, user__email=email)

        self.assertEqual(1, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

        self.assertEqual(member.user.is_staff, True)

    def test_jury_member_creation_missing_email(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"

        self.assertEqual(0, Individual.objects.filter(user__email=email).count())

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        self.assertEqual(resp.status_code, 200)

        # Ensure jury member has not been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(0, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_jury_member_creation_existing_user_no_profile(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"

        UserFactory.create(email=email)

        self.assertEqual(0, Individual.objects.filter(user__email=email).count())

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'email': email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        self.assertEqual(resp.status_code, 200)

        # Ensure jury member has not been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(0, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_jury_member_creation_existing_individual_profile(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"
        escaped_email = "john%40mailinator.com"

        user = UserFactory.create(email=email)
        IndividualFactory.create(user=user)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            "email": email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        url = reverse('competition-admin:make_jury_members')
        self.assertRedirects(resp, '{0}?email={1}'.format(url, escaped_email))

        # Ensure no jury member has been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(1, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_jury_member_creation_existing_individual_profile_escape(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john+doe@mailinator.com"
        escaped_email = "john%2Bdoe%40mailinator.com"

        user = UserFactory.create(email=email)
        IndividualFactory.create(user=user)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            "email": email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        url = reverse('competition-admin:make_jury_members')
        self.assertRedirects(resp, '{0}?email={1}'.format(url, escaped_email))

        # Ensure no jury member has been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(1, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_make_jury_member_existing_individual_profile(self):
        make_jury_member_url = reverse('competition-admin:make_jury_members')
        resp = self.client.get(make_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        email = "john@mailinator.com"

        user = UserFactory.create(email=email)
        IndividualFactory.create(user=user)

        post_data = {
            'profile_email': email,
        }

        resp = self.client.post(make_jury_member_url, post_data)
        self.assertRedirects(resp, reverse('competition-admin:show_jury_members'))

        # Ensure a jury member has been created
        self.assertEqual(1, Individual.objects.filter(user__email=email).count())
        profile = Individual.objects.filter(user__email=email).all()[0]
        self.assertEqual(profile.user.is_staff, True)
        self.assertEqual(1, JuryMember.objects.filter(competitions=self.competition, user=profile.user).count())
        jury = JuryMember.objects.get(competitions=self.competition, user=profile.user)

        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_make_jury_member_not_existing_individual_profile(self):
        make_jury_member_url = reverse('competition-admin:make_jury_members')
        resp = self.client.get(make_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        email = "john@mailinator.com"

        user = UserFactory.create(email=email)

        post_data = {
            'profile_email': email,
        }

        resp = self.client.post(make_jury_member_url, post_data)
        self.assertEqual(resp.status_code, 200)

        # Ensure a jury member has not been created
        self.assertEqual(0, Individual.objects.filter(user__email=email).count())
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(0, Organization.objects.filter(user__email=email).count())

    def test_jury_member_creation_existing_organization_profile(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"
        escaped_email = "john%40mailinator.com"

        user = UserFactory.create(email=email)
        OrganizationFactory.create(user=user)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            "email": email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        url = reverse('competition-admin:make_jury_members')
        self.assertRedirects(resp, '{0}?email={1}'.format(url, escaped_email))

        # Ensure no jury member has been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(0, Individual.objects.filter(user__email=email).count())
        self.assertEqual(1, Organization.objects.filter(user__email=email).count())

    def test_make_jury_member_existing_organization_profile(self):
        make_jury_member_url = reverse('competition-admin:make_jury_members')
        resp = self.client.get(make_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        email = "john@mailinator.com"

        user = UserFactory.create(email=email)
        OrganizationFactory.create(user=user)

        post_data = {
            'profile_email': email,
        }

        resp = self.client.post(make_jury_member_url, post_data)
        self.assertRedirects(resp, reverse('competition-admin:show_jury_members'))

        # Ensure a jury member has been created
        self.assertEqual(0, Individual.objects.filter(user__email=email).count())
        self.assertEqual(1, Organization.objects.filter(user__email=email).count())
        profile = Organization.objects.filter(user__email=email).all()[0]
        self.assertEqual(profile.user.is_staff, True)
        self.assertEqual(1, JuryMember.objects.filter(competitions=self.competition, user=profile.user).count())
        jury = JuryMember.objects.get(competitions=self.competition, user=profile.user)

    def test_jury_member_creation_several_profiles(self):
        add_jury_member_url = reverse('competition-admin:add_jury_member')
        resp = self.client.get(add_jury_member_url)
        self.assertEqual(resp.status_code, 200)

        pwd = 123
        email = "john@mailinator.com"
        escaped_email = "john%40mailinator.com"

        user1 = UserFactory.create(email=email)
        IndividualFactory.create(user=user1)

        user2 = UserFactory.create(email=email)
        IndividualFactory.create(user=user2)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'email': email,
            'password': pwd,
            'password2': pwd,
        }

        resp = self.client.post(add_jury_member_url, post_data)
        url = reverse('competition-admin:make_jury_members')
        self.assertRedirects(resp, '{0}?email={1}'.format(url, escaped_email))

        # Ensure no jury member has been created
        self.assertEqual(0, JuryMember.objects.filter(competitions=self.competition, user__email=email).count())
        self.assertEqual(2, Individual.objects.filter(user__email=email).count())

    def test_jury_member_candidate_association(self):

        # Create a jury member, and associate it to current competition
        jury_member = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member, competition=self.competition)

        # Create some candidates for this competition
        candidates = CandidateFactory.create_batch(competition=self.competition, size=10)

        # Associate jury member to candidate via user interface
        step = CompetitionStep.objects.get(competition=self.competition)
        allocations_url = reverse('competition-admin:step_manage_allocations', args=[step.url])

        resp = self.client.get(allocations_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {}

        for candidate in candidates:
            key = "c_%s_j_%s" % (candidate.pk, jury_member.pk)
            post_data[key] = True

        resp = self.client.post(allocations_url, post_data)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Your changes have been saved.")

        # Ensure jury member / candidate association have been created (through 'Evaluation' objects)

        for candidate in candidates:
            evaluation = Evaluation.objects.get(competition_step=step, jury_member=jury_member, candidate=candidate)
            self.assertEqual(evaluation.status.url, "to_process")

    def test_jury_member_candidate_association_existing(self):

        # Create a jury member, and associate it to current competition
        jury_member1 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member1, competition=self.competition)

        jury_member2 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member2, competition=self.competition)

        # Create some candidates for this competition
        candidates = CandidateFactory.create_batch(competition=self.competition, size=5)

        # Associate jury member to candidate via user interface
        step = CompetitionStep.objects.get(competition=self.competition)

        to_process = EvaluationStatus.objects.get(url="to_process")
        in_progress = EvaluationStatus.objects.get(url="in_progress")
        completed = EvaluationStatus.objects.get(url="completed")

        candidate0 = candidates[0]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member1, candidate=candidate0, status=to_process
        )
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate0, status=to_process
        )

        candidate1 = candidates[1]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate1, status=to_process
        )

        candidate2 = candidates[2]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member1, candidate=candidate2, status=in_progress
        )
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate2, status=in_progress
        )

        candidate3 = candidates[3]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate3, status=completed
        )

        candidate4 = candidates[4]

        allocations_url = reverse('competition-admin:step_manage_allocations', args=[step.url])

        resp = self.client.get(allocations_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {}
        created_allocations = [
            (candidate0, jury_member1, to_process),
            (candidate1, jury_member1, to_process),
            (candidate1, jury_member2, to_process),
            (candidate2, jury_member2, in_progress),
            (candidate4, jury_member1, to_process),
            (candidate4, jury_member2, to_process),
        ]

        no_allocations = [
            (candidate0, jury_member2, to_process),
            (candidate3, jury_member1, to_process),
        ]

        kept_allocations = [
            (candidate2, jury_member1, in_progress),
            (candidate2, jury_member2, in_progress),
            (candidate3, jury_member2, completed),
        ]

        for (candidate, jury_member, status) in created_allocations:
            key = "c_%s_j_%s" % (candidate.pk, jury_member.pk)
            post_data[key] = True

        resp = self.client.post(allocations_url, post_data)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Your changes have been saved.")

        for (candidate, jury_member, status) in created_allocations + kept_allocations:
            evaluation = Evaluation.objects.get(competition_step=step, jury_member=jury_member, candidate=candidate)
            self.assertEqual(evaluation.status, status)

        for (candidate, jury_member, status) in no_allocations:
            evaluations = Evaluation.objects.filter(competition_step=step, jury_member=jury_member, candidate=candidate)
            self.assertEqual(evaluations.count(), 0)

    def test_jury_member_candidate_association_existing_ajax(self):

        # Create a jury member, and associate it to current competition
        jury_member1 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member1, competition=self.competition)

        jury_member2 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member2, competition=self.competition)

        # Create some candidates for this competition
        candidates = CandidateFactory.create_batch(competition=self.competition, size=5)

        # Associate jury member to candidate via user interface
        step = CompetitionStep.objects.get(competition=self.competition)

        to_process = EvaluationStatus.objects.get(url="to_process")
        in_progress = EvaluationStatus.objects.get(url="in_progress")
        completed = EvaluationStatus.objects.get(url="completed")

        candidate0 = candidates[0]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member1, candidate=candidate0, status=to_process
        )
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate0, status=to_process
        )

        candidate1 = candidates[1]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate1, status=to_process
        )

        candidate2 = candidates[2]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member1, candidate=candidate2, status=in_progress
        )
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate2, status=in_progress
        )

        candidate3 = candidates[3]
        Evaluation.objects.create(
            competition_step=step, jury_member=jury_member2, candidate=candidate3, status=completed
        )

        candidate4 = candidates[4]

        allocations_url = reverse('competition-admin:step_manage_allocations', args=[step.url])

        resp = self.client.get(allocations_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {}
        created_allocations = [
            (candidate0, jury_member1, to_process),
            (candidate1, jury_member1, to_process),
            (candidate1, jury_member2, to_process),
            (candidate2, jury_member2, in_progress),
            (candidate4, jury_member1, to_process),
            (candidate4, jury_member2, to_process),
        ]

        no_allocations = [
            (candidate0, jury_member2, to_process),
            (candidate3, jury_member1, to_process),
        ]

        kept_allocations = [
            (candidate2, jury_member1, in_progress),
            (candidate2, jury_member2, in_progress),
            (candidate3, jury_member2, completed),
        ]

        for (candidate, jury_member, status) in created_allocations:
            key = "c_%s_j_%s" % (candidate.pk, jury_member.pk)
            post_data[key] = True

        json_data = json.dumps(post_data)
        resp = self.client.post(
            allocations_url,
            {'form_data': json_data},
            HTTP_X_REQUESTED_WITH='XMLHttpRequest'  # as ajax
        )
        self.assertEqual(resp.status_code, 200)
        json_resp = json.loads(resp.content)
        self.assertEqual(json_resp['url'], allocations_url)

        for (candidate, jury_member, status) in created_allocations + kept_allocations:
            evaluation = Evaluation.objects.get(competition_step=step, jury_member=jury_member, candidate=candidate)
            self.assertEqual(evaluation.status, status)

        for (candidate, jury_member, status) in no_allocations:
            evaluations = Evaluation.objects.filter(competition_step=step, jury_member=jury_member, candidate=candidate)
            self.assertEqual(evaluations.count(), 0)

    def test_jury_member_candidate_association_filters(self):

        # Create a jury member, and associate it to current competition
        jury_member1 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member1, competition=self.competition)

        jury_member2 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member2, competition=self.competition)

        # Create some candidates for this competition
        candidates = CandidateFactory.create_batch(competition=self.competition, size=10)

        candidate_group = CandidateGroupFactory.create(competition=self.competition)

        candidate1 = candidates[0]
        candidate1.groups.add(candidate_group)
        candidate1.save()

        candidate2 = candidates[1]
        candidate2.groups.add(candidate_group)
        candidate2.save()

        candidate3 = candidates[2]

        candidate4 = candidates[3]
        candidate4.groups.add(candidate_group)
        candidate4.save()

        jury_member_group = JuryMemberGroupFactory.create(competition=self.competition)
        jury_member1.groups.add(jury_member_group)
        jury_member1.save()

        # Associate jury member to candidate via user interface
        step = CompetitionStep.objects.get(competition=self.competition)
        allocations_url = reverse('competition-admin:step_manage_allocations', args=[step.url])
        allocations_url += "?cg={0}&jg={1}".format(candidate_group.id, jury_member_group.id)

        resp = self.client.get(allocations_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {}

        key = "c_%s_j_%s" % (candidate1.pk, jury_member1.pk)
        post_data[key] = True

        key = "c_%s_j_%s" % (candidate3.pk, jury_member1.pk)
        post_data[key] = True

        key = "c_%s_j_%s" % (candidate4.pk, jury_member2.pk)
        post_data[key] = True

        resp = self.client.post(allocations_url, post_data)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Your changes have been saved.")

        # Ensure jury member / candidate association have been created (through 'Evaluation' objects)

        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate1)
        self.assertEqual(queryset.count(), 1)
        evaluation = queryset[0]
        self.assertEqual(evaluation.jury_member, jury_member1)
        self.assertEqual(evaluation.status.url, "to_process")

        # Candidate is not checked
        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate2)
        self.assertEqual(queryset.count(), 0)

        # Candidate is not in group
        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate3)
        self.assertEqual(queryset.count(), 0)

        # Jury is not in group
        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate4)
        self.assertEqual(queryset.count(), 0)

    def test_jury_member_candidate_remove_association_filters(self):

        # Create a jury member, and associate it to current competition
        jury_member1 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member1, competition=self.competition)

        jury_member2 = JuryMemberFactory.create()
        JuryMemberCompetitionFactory.create(jury_member=jury_member2, competition=self.competition)

        # Create some candidates for this competition
        candidates = CandidateFactory.create_batch(competition=self.competition, size=10)

        candidate_group = CandidateGroupFactory.create(competition=self.competition)

        candidate1 = candidates[0]
        candidate1.groups.add(candidate_group)
        candidate1.save()

        candidate2 = candidates[1]
        candidate2.groups.add(candidate_group)
        candidate2.save()

        candidate3 = candidates[2]

        candidate4 = candidates[3]
        candidate4.groups.add(candidate_group)
        candidate4.save()

        for candidate in candidates:
            EvaluationFactory.create(
                candidate=candidate,
                jury_member=jury_member1 if candidate != candidate4 else jury_member2,
                competition_step=self.competition_step,
                status="to_process"
            )

        jury_member_group = JuryMemberGroupFactory.create(competition=self.competition)
        jury_member1.groups.add(jury_member_group)
        jury_member1.save()

        # Associate jury member to candidate via user interface
        step = CompetitionStep.objects.get(competition=self.competition)
        allocations_url = reverse('competition-admin:step_manage_allocations', args=[step.url])
        allocations_url += "?cg={0}&jg={1}".format(candidate_group.id, jury_member_group.id)

        resp = self.client.get(allocations_url)
        self.assertEqual(resp.status_code, 200)

        post_data = {}

        key = "c_%s_j_%s" % (candidate1.pk, jury_member1.pk)
        post_data[key] = False

        resp = self.client.post(allocations_url, post_data)
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, "Your changes have been saved.")

        # Ensure jury member / candidate association have been created (through 'Evaluation' objects)

        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate1)
        self.assertEqual(queryset.count(), 1)

        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate2)
        self.assertEqual(queryset.count(), 0)

        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate3)
        self.assertEqual(queryset.count(), 1)

        queryset = Evaluation.objects.filter(competition_step=step, candidate=candidate4)
        self.assertEqual(queryset.count(), 1)

    def test_edit_jury_member_no_reset(self):
        user = UserFactory.create(email="jury@ircam.fr")
        jury_member = JuryMemberFactory.create(user=user)
        JuryMemberCompetitionFactory.create(jury_member=jury_member, competition=self.competition)

        url = reverse('competition-admin:ulysses_competitions_jurymember_change', args=[jury_member.id])
        redirect_url = reverse('competition-admin:ulysses_competitions_jurymember_changelist')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'send_reset_password': False,
            'email': jury_member.user.email,
            'username': jury_member.user.username,
        }

        resp = self.client.post(url, post_data)
        self.assertRedirects(resp, redirect_url)

        jury_member = JuryMember.objects.get(id=jury_member.id)
        self.assertEqual(jury_member.user.last_name, post_data['last_name'])
        self.assertEqual(jury_member.user.first_name, post_data['first_name'])
        self.assertEqual(0, len(mail.outbox))

    def test_edit_jury_member_reset(self):
        user = UserFactory.create(email="jury@ircam.fr")
        jury_member = JuryMemberFactory.create(user=user)
        JuryMemberCompetitionFactory.create(jury_member=jury_member, competition=self.competition)
        url = reverse('competition-admin:ulysses_competitions_jurymember_change', args=[jury_member.id])
        redirect_url = reverse('competition-admin:ulysses_competitions_jurymember_changelist')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'send_reset_password': True,
            'email': jury_member.user.email,
            'username': jury_member.user.username,
        }

        resp = self.client.post(url, post_data)
        self.assertRedirects(resp, redirect_url)

        jury_member = JuryMember.objects.get(id=jury_member.id)
        self.assertEqual(jury_member.user.last_name, post_data['last_name'])
        self.assertEqual(jury_member.user.first_name, post_data['first_name'])
        self.assertEqual(1, len(mail.outbox))
        email = mail.outbox[0]
        self.assertEqual(email.to, [jury_member.user.email])

    def test_edit_jury_member_readonly_fields(self):
        user = UserFactory.create(email="jury@ircam.fr")
        jury_member = JuryMemberFactory.create(user=user)
        JuryMemberCompetitionFactory.create(jury_member=jury_member, competition=self.competition)

        url = reverse('competition-admin:ulysses_competitions_jurymember_change', args=[jury_member.id])
        redirect_url = reverse('competition-admin:ulysses_competitions_jurymember_changelist')

        resp = self.client.get(url)
        self.assertEqual(resp.status_code, 200)

        post_data = {
            'first_name': "John",
            'last_name': "Doe",
            'send_reset_password': False,
            'email': 'toto@email.fr',
            'username': 'toto',
        }

        resp = self.client.post(url, post_data)
        self.assertRedirects(resp, redirect_url)

        jury_member = JuryMember.objects.get(id=jury_member.id)
        self.assertEqual(jury_member.user.last_name, post_data['last_name'])
        self.assertEqual(jury_member.user.first_name, post_data['first_name'])
        self.assertNotEqual(jury_member.user.email, post_data['email'])
        self.assertNotEqual(jury_member.user.username, post_data['username'])
        self.assertEqual(0, len(mail.outbox))

    def test_step_settings(self):
        url = reverse('competition-admin:step_settings', args=[self.competition_step.url])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_step_settings_as_anonymous(self):
        self.client.logout()
        url = reverse('competition-admin:step_settings', args=[self.competition_step.url])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 302)

    def test_save_step_settings(self):
        url = reverse('competition-admin:step_settings', args=[self.competition_step.url])
        data = {
            'notify_association_by_email': True,
            'notification_email': 'This is the text of the email'
        }
        self.assertEqual(self.competition_step.notify_association_by_email, False)
        self.assertEqual(self.competition_step.notification_email, '')

        response = self.client.post(url, data=data)
        self.assertRedirects(response, url)
        step = CompetitionStep.objects.get(id=self.competition_step.id)
        self.assertEqual(step.notify_association_by_email, True)
        self.assertEqual(step.notification_email, data['notification_email'])

    def test_save_step_settings_disable_email_notification(self):
        url = reverse('competition-admin:step_settings', args=[self.competition_step.url])
        data = {
            'notify_association_by_email': False,
            'notification_email': ''
        }

        self.competition_step.notify_association_by_email = True
        self.competition_step.notification_email = 'Test'
        self.competition_step.save()

        response = self.client.post(url, data=data)
        self.assertRedirects(response, url)
        step = CompetitionStep.objects.get(id=self.competition_step.id)
        self.assertEqual(step.notify_association_by_email, False)
        self.assertEqual(step.notification_email, '')

    def test_save_step_settings_missing_text(self):
        url = reverse('competition-admin:step_settings', args=[self.competition_step.url])
        data = {
            'notify_association_by_email': True,
            'notification_email': ''
        }
        self.assertEqual(self.competition_step.notify_association_by_email, False)
        self.assertEqual(self.competition_step.notification_email, '')

        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 200)
        step = CompetitionStep.objects.get(id=self.competition_step.id)
        self.assertEqual(step.notify_association_by_email, False)
        self.assertEqual(step.notification_email, '')

    def test_remove_jury_member_from_competition(self):

        candidate11 = CandidateFactory.create(competition=self.competition)
        candidate12 = CandidateFactory.create(competition=self.competition)

        for jury_member in self.jury_members:
            jury_member.competitions.add(self.competition)
            jury_member.steps.add(self.competition_step)
            jury_member.save()
            EvaluationFactory.create(
                competition_step=self.competition_step,
                jury_member=jury_member,
                candidate=candidate11
            )
            EvaluationFactory.create(
                competition_step=self.competition_step,
                jury_member=jury_member,
                candidate=candidate12
            )


        competition2 = CompetitionFactory.create()
        competition2.managed_by.add(self.compadmin_manager)
        competition2.save()
        step2 = CompetitionStepFactory.create(competition=competition2)

        competition2_jury = JuryMemberFactory.create()
        competition2_jury.competitions.add(competition2)
        competition2_jury.save()

        other_competition = CompetitionFactory.create()
        other_jury_member = JuryMemberFactory.create()
        competition_jury_members = self.jury_members
        all_my_juries = competition_jury_members + [competition2_jury]
        all_juries = all_my_juries + [other_jury_member]

        remove_juries = competition_jury_members[:2] + [competition2_jury]
        keep_jury = competition_jury_members[2]
        candidate22 = CandidateFactory.create(competition=competition2)
        EvaluationFactory.create(competition_step=step2, jury_member=remove_juries[0], candidate=candidate22)

        jury_member_url = reverse('competition-admin:show_jury_members')
        resp = self.client.get(jury_member_url)
        self.assertEqual(resp.status_code, 200)
        for jury_member in all_my_juries:
            self.assertContains(resp, jury_member.name())
        for jury_member in [other_jury_member]:
            self.assertNotContains(resp, jury_member.name())

        # same but only members of
        jury_member_url = reverse(
            'competition-admin:show_jury_members'
        ) + '?competition={0}'.format(self.competition.id)
        resp = self.client.get(jury_member_url)
        self.assertEqual(resp.status_code, 200)
        for jury_member in competition_jury_members:
            self.assertContains(resp, jury_member.name())
        for jury_member in [other_jury_member, competition2_jury]:
            self.assertNotContains(resp, jury_member.name())

        post_data = {
            'action': 'remove_jury_members_from_competition',
            '_selected_action': [elt.id for elt in remove_juries]
        }
        resp = self.client.post(jury_member_url, post_data)
        self.assertRedirects(resp, jury_member_url)

        self.assertEqual(5, JuryMember.objects.count())
        self.assertEqual(1, JuryMember.objects.filter(competitions=self.competition).count())
        self.assertEqual(1, JuryMember.objects.filter(steps=self.competition_step).count())
        jury = JuryMember.objects.filter(competitions=self.competition).all()[0]
        self.assertEqual(keep_jury.id, jury.id)

        self.assertEqual(13, Evaluation.objects.count())  # 10 + 2 + 1
        self.assertEqual(1, Evaluation.objects.filter(competition_step__competition=competition2).count())
        competition_evaluations = Evaluation.objects.filter(competition_step__competition=self.competition)
        self.assertEqual(12, competition_evaluations.count())
        for evaluation in competition_evaluations:
            self.assertEqual(evaluation.jury_member, keep_jury)

    def test_step_associate_jury_members(self):

        jury_member1 = JuryMemberFactory.create()
        candidate11 = CandidateFactory.create(competition=self.competition)
        step = self.competition_step

        url = reverse('competition-admin:step_associate_jury_members', args=[step.url])
        url += '?ids={0}&referer=/'.format(candidate11.id)

        data = {
            'action_choice': 1,
            'jury_member': jury_member1.id,
        }
        resp = self.client.post(url, data)
        self.assertRedirects(resp, '/')

        self.assertEqual(
            1,
            Evaluation.objects.filter(candidate=candidate11, jury_member=jury_member1, competition_step=step).count()
        )

    def test_step_associate_jury_members_existing(self):

        jury_member1 = JuryMemberFactory.create()
        candidate11 = CandidateFactory.create(competition=self.competition)
        step = self.competition_step

        EvaluationFactory.create(
            candidate=candidate11, jury_member=jury_member1, competition_step=step
        )

        url = reverse('competition-admin:step_associate_jury_members', args=[step.url])
        url += '?ids={0}&referer=/'.format(candidate11.id)

        data = {
            'action_choice': 1,
            'jury_member': jury_member1.id,
        }
        resp = self.client.post(url, data)
        self.assertRedirects(resp, '/')

        self.assertEqual(
            1,
            Evaluation.objects.filter(candidate=candidate11, jury_member=jury_member1, competition_step=step).count()
        )

    def test_step_associate_jury_members_group(self):

        group = JuryMemberGroupFactory.create(competition=self.competition)
        jury_member1 = JuryMemberFactory.create()
        jury_member1.groups.add(group)
        jury_member1.save()
        jury_member2 = JuryMemberFactory.create()

        candidate11 = CandidateFactory.create(competition=self.competition)
        step = self.competition_step

        EvaluationFactory.create(
            candidate=candidate11, jury_member=jury_member1, competition_step=step
        )

        url = reverse('competition-admin:step_associate_jury_members', args=[step.url])
        url += '?ids={0}&referer=/'.format(candidate11.id)

        data = {
            'action_choice': 2,
            'jury_member_group': group.id,
        }
        resp = self.client.post(url, data)
        self.assertRedirects(resp, '/')

        self.assertEqual(
            1,
            Evaluation.objects.filter(candidate=candidate11, jury_member=jury_member1, competition_step=step).count()
        )
        self.assertEqual(
            0,
            Evaluation.objects.filter(candidate=candidate11, jury_member=jury_member2, competition_step=step).count()
        )

    def test_step_associate_jury_members_no_members(self):

        jury_member1 = JuryMemberFactory.create()
        candidate11 = CandidateFactory.create(competition=self.competition)
        step = self.competition_step

        url = reverse('competition-admin:step_associate_jury_members', args=[step.url])
        url += '?ids={0}&referer=/'.format(candidate11.id)

        data = {
            'action_choice': 1,
        }
        resp = self.client.post(url, data)
        self.assertRedirects(resp, '/')

        self.assertEqual(
            0,
            Evaluation.objects.filter(candidate=candidate11, competition_step=step).count()
        )


class CandidatesToCompetitionStepTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        self.competition_admin_user = UserFactory.create(
            username="joe", password="joe", is_active=True, is_staff=True
        )
        self.competition_admin_user.groups.add(Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP))
        self.manager = CompetitionManager.objects.get(user=self.competition_admin_user)
        self.competition1 = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm")

        self.competition2 = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name="open"),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm")
        self.competition1.managed_by.add(self.manager)

        self.client.login(username="joe", password="joe")

        session = self.client.session
        session[settings.ACTIVE_COMPETITION_KEY] = self.competition1
        session.save()

    def test_add_candidate_to_step(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        # activate notifications for another step
        competition_step21.notify_association_by_email = True
        competition_step21.notification_email = 'Congratulations'
        competition_step21.save()

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "add"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertTrue(competition_step11 in steps)
        self.assertFalse(competition_step21 in steps)
        self.assertFalse(competition_step12 in steps)

        self.assertEqual(0, candidate12.steps.count())
        self.assertEqual(0, len(mail.outbox))

    def test_add_candidate_to_step_notification_active(self):
        self.competition1.email = 'competition@competition.com'
        self.competition1.save()

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        # activate notifications for this step
        competition_step11.notify_association_by_email = True
        competition_step11.notification_email = 'Congratulations'
        competition_step11.save()

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "add"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertTrue(competition_step11 in steps)
        self.assertFalse(competition_step21 in steps)
        self.assertFalse(competition_step12 in steps)

        self.assertEqual(0, candidate12.steps.count())
        self.assertEqual(1, len(mail.outbox))
        received_email = mail.outbox[0]
        self.assertEqual(received_email.subject, self.competition1.name)
        self.assertEqual(received_email.body, 'Congratulations')
        self.assertEqual(received_email.to, [candidate11.composer.user.email])
        self.assertEqual(received_email.from_email, self.competition1.email)

    def test_add_candidate_to_step_notification_active_no_competition_email(self):
        self.competition1.email = ''
        self.competition1.save()

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        # activate notifications for this step
        competition_step11.notify_association_by_email = True
        competition_step11.notification_email = 'Congratulations'
        competition_step11.save()

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "add"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertTrue(competition_step11 in steps)
        self.assertFalse(competition_step21 in steps)
        self.assertFalse(competition_step12 in steps)

        self.assertEqual(0, candidate12.steps.count())
        self.assertEqual(1, len(mail.outbox))
        received_email = mail.outbox[0]
        self.assertEqual(received_email.subject, self.competition1.name)
        self.assertEqual(received_email.body, 'Congratulations')
        self.assertEqual(received_email.to, [candidate11.composer.user.email])
        self.assertEqual(received_email.from_email, settings.DEFAULT_FROM_EMAIL)

    def test_add_several_candidates_to_step(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0},{1}".format(
            candidate11.id, candidate21.id
        )
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "add"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertTrue(competition_step11 in steps)
        self.assertFalse(competition_step21 in steps)
        self.assertFalse(competition_step12 in steps)

        steps = candidate21.steps.all()
        self.assertTrue(competition_step11 in steps)
        self.assertFalse(competition_step21 in steps)
        self.assertFalse(competition_step12 in steps)

        self.assertEqual(0, candidate12.steps.count())

    def test_view_add_candidate_to_step(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0}".format(candidate11.id)
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(0, candidate11.steps.count())
        self.assertEqual(0, candidate12.steps.count())

        self.assertContains(response, str(candidate11))
        self.assertNotContains(response, str(candidate21))

    def test_remove_candidate_to_step(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "remove"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertEqual(1, candidate11.steps.count())
        self.assertTrue(competition_step21 in steps)
        self.assertFalse(competition_step11 in steps)
        self.assertEqual(2, candidate21.steps.count())
        self.assertEqual(1, candidate12.steps.count())

    def test_remove_several_candidates_to_step(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse('competition-admin:manage_candidates_to_competition_step')
        url += "?ids={0},{1}".format(candidate11.id, candidate21.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "remove"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        steps = candidate11.steps.all()
        self.assertEqual(1, candidate11.steps.count())
        self.assertTrue(competition_step21 in steps)
        self.assertFalse(competition_step11 in steps)
        self.assertEqual(1, candidate21.steps.count())
        self.assertTrue(competition_step21 in steps)
        self.assertFalse(competition_step11 in steps)
        self.assertEqual(1, candidate12.steps.count())

    def test_remove_candidate_to_jury_of_other_competition(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse("competition-admin:manage_candidates_to_competition_step") + "?ids={0}".format(candidate12.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "remove"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        self.assertEqual(2, candidate11.steps.count())
        self.assertEqual(2, candidate21.steps.count())
        self.assertEqual(1, candidate12.steps.count())

    def test_add_candidate_to_jury_of_other_competition(self):

        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse("competition-admin:manage_candidates_to_competition_step") + "?ids={0}".format(candidate12.id)
        response = self.client.post(url, data={'competition_step': competition_step11.id, "add_or_remove": "add"})
        self.assertEqual(302, response.status_code)
        self.assertEqual(
            response['Location'],
            reverse('competition-admin:step_manage_allocations', args=[competition_step11.url])
        )

        self.assertEqual(0, candidate11.steps.count())
        self.assertEqual(0, candidate12.steps.count())

    def test_add_candidate_to_other_step_of_other_competition(self):
        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        url = reverse("competition-admin:manage_candidates_to_competition_step") + "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step12.id, "add_or_remove": "add"})
        self.assertEqual(200, response.status_code)

        self.assertEqual(0, candidate11.steps.count())
        self.assertEqual(0, candidate12.steps.count())

    def test_remove_candidate_to_other_step_of_other_competition(self):
        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse("competition-admin:manage_candidates_to_competition_step") + "?ids={0}".format(candidate11.id)
        response = self.client.post(url, data={'competition_step': competition_step12.id, "add_or_remove": "remove"})
        self.assertEqual(200, response.status_code)

        self.assertEqual(2, candidate11.steps.count())
        self.assertEqual(2, candidate21.steps.count())
        self.assertEqual(1, candidate12.steps.count())

    def test_remove_candidate_from_competition(self):
        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse("competition-admin:remove_candidate_from_competition") + "?ids={0}&referer=/".format(
            candidate11.id
        )
        response = self.client.post(url, data={'yes-do-it': True})
        self.assertEqual(302, response.status_code)

        self.assertEqual(0, Candidate.objects.filter(id=candidate11.id).count())
        self.assertEqual(1, competition_step11.candidate_set.count())
        self.assertEqual(1, competition_step21.candidate_set.count())

    def test_get_remove_candidate_from_competition(self):
        competition_step11 = CompetitionStepFactory.create(competition=self.competition1, name="Jury", url="jury")
        competition_step21 = CompetitionStepFactory.create(competition=self.competition1, name="Jury2", url="jury2")
        competition_step12 = CompetitionStepFactory.create(competition=self.competition2, name="Jury", url="jury")

        candidate11 = CandidateFactory.create(competition=self.competition1)
        candidate21 = CandidateFactory.create(competition=self.competition1)
        candidate12 = CandidateFactory.create(competition=self.competition2)

        candidate11.steps.add(competition_step11, competition_step21)
        candidate11.save()

        candidate21.steps.add(competition_step11, competition_step21)
        candidate21.save()

        candidate12.steps.add(competition_step12)
        candidate12.save()

        url = reverse("competition-admin:remove_candidate_from_competition") + "?ids={0}&referer=/".format(
            candidate11.id
        )
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, Candidate.objects.filter(id=candidate11.id).count())


class CompetitionAdminTest(TestCase):
    """check that CompetitionManager are linked to a user with 'competition admins' group"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_save_competition_manager(self):
        """create a CompetitionManager and make sure that the user is member of the 'competition admin' group"""
        manager_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

        manager = CompetitionManagerFactory.create()

        self.assertTrue(manager_group in manager.user.groups.all())

    def test_delete_competition_manager(self):
        """delete a CompetitionManager and make sure that the user is not member of the 'competition admin' group"""
        manager_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

        manager = CompetitionManagerFactory.create()
        user = manager.user

        self.assertTrue(manager_group in user.groups.all())

        manager.delete()

        user = User.objects.get(id=user.id)
        self.assertTrue(manager_group not in user.groups.all())

    def test_add_user_to_competition_admin_group(self):
        """make user member of 'competition admins' group and make sure a CompetitionManager is created for this user"""
        manager_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

        user = UserFactory.create()

        self.assertEqual(0, CompetitionManager.objects.filter(user=user).count())

        user.groups.add(manager_group)
        user.save()

        self.assertTrue(manager_group in user.groups.all())
        self.assertEqual(1, CompetitionManager.objects.filter(user=user).count())

    def test_add_user_to_group_existing_competition_manager(self):
        """
        make user member of 'competition admins' group
        if already a comeptition manager make sure everything is ok
        """
        manager_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

        user = UserFactory.create()
        CompetitionManagerFactory.create(user=user)

        self.assertEqual(1, CompetitionManager.objects.filter(user=user).count())

        user.groups.add(manager_group)
        user.save()

        self.assertTrue(manager_group in user.groups.all())
        self.assertEqual(1, CompetitionManager.objects.filter(user=user).count())

    def test_remove_user_from_competition_admin_groups(self):
        """
        removee user from members of 'competition admins' group and
        make sure the CompetitionManager is removed for this user
        """
        manager_group = Group.objects.get(name=settings.COMPETITION_ADMINS_GROUP)

        user = UserFactory.create()
        CompetitionManagerFactory.create(user=user)

        self.assertEqual(1, CompetitionManager.objects.filter(user=user).count())
        self.assertTrue(manager_group in user.groups.all())

        user.groups.remove(manager_group)
        user.save()

        self.assertTrue(manager_group not in user.groups.all())
        self.assertEqual(0, CompetitionManager.objects.filter(user=user).count())


class RemoveGroupTest(TestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):
        """before each test: create user"""
        self.user = UserFactory.create(username="joe", password="bar")
        self.manager = CompetitionManagerFactory.create(user=self.user)
        self.competition = CompetitionFactory.create(status=CompetitionStatus.objects.get(name="open"))
        self.competition.managed_by.add(self.manager)
        self.competition.save()

        self.client.login(username="joe", password="bar")

    def test_delete_candidate_group(self):
        """delete a candidate group"""
        group = CandidateGroupFactory.create(competition=self.competition)

        candidate = CandidateFactory.create(competition=self.competition)
        candidate.groups.add(group)
        candidate.save()

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'candidate'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)
        self.assertEqual(json_data["success"], True)
        self.assertEqual(CandidateGroup.objects.count(), 0)
        self.assertEqual(Candidate.objects.filter(id=candidate.id).count(), 1)

    def test_delete_jury_group(self):
        """delete a jury group"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        url = reverse("admin_remove_group")

        jury = JuryMemberFactory.create()
        jury.groups.add(group)
        jury.save()

        response = self.client.post(url, data={'group_id': group.id, 'model': 'jury'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)
        self.assertEqual(json_data["success"], True)
        self.assertEqual(JuryMemberGroup.objects.count(), 0)
        self.assertEqual(JuryMember.objects.filter(id=jury.id).count(), 1)

    def test_delete_jury_group_model_mismatch(self):
        """delete a jury group: wrong group type"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'candidate'})
        self.assertEqual(response.status_code, 404)

        self.assertEqual(JuryMemberGroup.objects.count(), 1)

    def test_delete_jury_group_model_unkkown(self):
        """delete a jury group: unkown group type"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': ''})
        self.assertEqual(response.status_code, 404)

        self.assertEqual(JuryMemberGroup.objects.count(), 1)

    def test_delete_not_manager(self):
        """delete a jury group : not manager"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        self.manager.delete()

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'jury'})
        self.assertEqual(response.status_code, 403)

        self.assertEqual(JuryMemberGroup.objects.count(), 1)

    def test_delete_super_user(self):
        """delete a jury group : not manager but super user"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        self.manager.delete()

        self.user.is_superuser = True
        self.user.save()

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'jury'})
        self.assertEqual(response.status_code, 200)

        self.assertEqual(JuryMemberGroup.objects.count(), 0)

    def test_delete_not_manager_of_this_competition(self):
        """delete a jury group : not manager"""
        group = JuryMemberGroupFactory.create(competition=self.competition)

        self.competition.managed_by.clear()
        self.competition.save()

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'jury'})
        self.assertEqual(response.status_code, 403)

        self.assertEqual(JuryMemberGroup.objects.count(), 1)

    def test_delete_anonymous(self):
        """delete a jury group : not manager"""
        self.client.logout()

        group = JuryMemberGroupFactory.create(competition=self.competition)

        url = reverse("admin_remove_group")

        response = self.client.post(url, data={'group_id': group.id, 'model': 'jury'})
        self.assertEqual(response.status_code, 403)

        self.assertEqual(JuryMemberGroup.objects.count(), 1)
