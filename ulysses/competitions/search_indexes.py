# -*- coding: utf-8 -*-

from haystack import indexes

from .models import Competition, StaticContent


class CompetitionIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)
    content_auto = indexes.EdgeNgramField(model_attr='title')

    def get_model(self):
        return Competition

    def index_queryset(self, using=None):
        return self.get_model().objects.exclude(status__name__in=('draft', 'new', 'archived'))


class StaticContentIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return StaticContent

    def index_queryset(self, using=None):
        return self.get_model().objects.filter(published=True).exclude(name='')
