# -*- coding: utf-8 -*-

from django.conf import settings
from django.urls import reverse

import floppyforms.__future__ as forms


from ulysses.utils import dehtml, full_path_url, get_youtube_code

from .widgets import (
    ApplicationHtmlElementWidget, ApplicationHtmlElementPreviewWidget, ApplicationHtmlElementJuryWidget,
    BiographicElementEditWidget, BiographicElementPreviewWidget, BiographicElementAdminWidget,
    ApplicationTextElementWidget, ApplicationTextElementPreviewWidget, ApplicationTextElementJuryWidget,
    ApplicationBooleanElementWidget, ApplicationBooleanElementAdminWidget, ApplicationBooleanElementPreviewWidget,
    DocumentEditWidget, DocumentPreviewWidget, DocumentAdminWidget, CompetitionAttributeValuePreviewWidget,
    CompetitionAttributeValueJuryWidget, MediaEditWidget, MediaPreviewWidget, MediaAdminWidget,
    CheckboxSelectMultiplePreviewWidget, CheckboxSelectMultipleWidget
)
from .models import CompetitionAttributeValue, TemporaryDocument, TemporaryMedia


class ApplicationField(object):

    def get_value_key(self):
        return self.name

    def get_draft_key(self):
        return self.name

    def get_kind(self):
        from .models import ApplicationFormFieldType
        filters = {'name': type(self).__name__}
        if not ApplicationFormFieldType.objects.filter(**filters).exists():
            raise RuntimeError("Missing ApplicationFormFieldType '%s'" % type(self).__name__)
        return ApplicationFormFieldType.objects.get(**filters)

    def has_model(self):
        if hasattr(self, "Meta"):
            if hasattr(self.Meta, "model"):
                return True
        return False

    def get_data(self):
        return {'name': self.name, 'label': self.label, 'required': self.required, 'help_text': self.help_text}

    def __init__(
            self, competition, name, label, required, help_text, widget, preview_widget, admin_widget, jury_widget
    ):
        self.competition = competition
        self.name = name
        self.label = label
        self.required = required
        self.help_text = help_text
        self.widget = widget
        self.preview_widget = preview_widget
        self.admin_widget = admin_widget
        self.jury_widget = jury_widget


class BiographicElementField(ApplicationField):

    def get_value_key(self):
        return self.name

    def __init__(self, competition, name, label, required=True, help_text="", maximum_length=0):
        super(BiographicElementField,self).__init__(
            competition, name, label, required, help_text,
            BiographicElementEditWidget(competition, maximum_length),
            BiographicElementPreviewWidget(competition),
            BiographicElementAdminWidget(),
            BiographicElementPreviewWidget(competition)
        )

    def get_data(self):
        return {'name': self.name, 'label': self.label, 'required': self.required}

    def get_value_for_candidate(self, candidate, text=True):
        from .models import CandidateBiographicElement
        filters = {'candidate': candidate, 'key': self.name}
        if CandidateBiographicElement.objects.filter(**filters).exists():
            return CandidateBiographicElement.objects.get(**filters).text

    def get_excel_value_for_candidate(self, candidate):
        value = self.get_value_for_candidate(candidate)
        if value:
            return dehtml(value)
        return value


class DocumentField(ApplicationField):

    def get_value_key(self):
        return "%s_hidden" % self.name

    def __init__(self, competition, name, label, extra_files=0, required=True, help_text=""):
        super(DocumentField, self).__init__(
            competition, name, label, required, help_text,
            DocumentEditWidget(competition=competition, extra_files=extra_files),
            DocumentPreviewWidget(competition=competition, extra_files=extra_files),
            DocumentAdminWidget(extra_files=extra_files),
            DocumentPreviewWidget(competition=competition, extra_files=extra_files),
        )

    def get_value_for_candidate(self, candidate, text=True):
        from .models import CandidateDocument
        filters = {'candidate': candidate, 'key': self.name}
        if CandidateDocument.objects.filter(**filters).exists():
            if text:
                return CandidateDocument.objects.get(**filters).file
            else:
                return self.as_html(CandidateDocument.objects.get(**filters))

    def get_excel_value_for_candidate(self, candidate):
        from .models import CandidateDocument
        filters = {'candidate': candidate, 'key': self.name}
        if CandidateDocument.objects.filter(**filters).exists():
            document = CandidateDocument.objects.get(**filters)
            url = full_path_url('{0}{1}'.format(settings.MEDIA_URL, document.file))
            return '{0}: {1}'.format(document.title, url)
        return ''

    def as_html(self, document):
        return document.as_html

    class Meta:
        model = TemporaryDocument


class MediaField(ApplicationField):
    field_type = 'MediaField'

    def get_value_key(self):
        return "%s_hidden" % self.name

    def __init__(self, competition, name, label, required=True, help_text=""):
        super(MediaField, self).__init__(
            competition, name, label, required, help_text,
            MediaEditWidget(competition, self.field_type),
            MediaPreviewWidget(competition),
            MediaAdminWidget(),
            MediaPreviewWidget(competition),
        )

    def get_value_for_candidate(self, candidate, text=True):
        from .models import CandidateMedia
        filters = {'candidate': candidate, 'key': self.name}
        if CandidateMedia.objects.filter(**filters).exists():
            media = CandidateMedia.objects.get(**filters)
            if text:
                output = ['title : %s ' % media.title]
                if media.score:
                    output.append("score : %s" % media.score)
                if media.audio:
                    output.append("audio : %s" % media.audio)
                if media.video:
                    output.append("video : %s" % media.video)
                if media.link:
                    output.append("link : %s" % media.link)
                if media.notes:
                    output.append("notes : %s" % media.notes)
                return ", ".join(output)
            else:
                return self.as_html(media)

    def get_excel_value_for_candidate(self, candidate):
        from .models import CandidateMedia
        filters = {'candidate' : candidate, 'key': self.name}
        if CandidateMedia.objects.filter(**filters).exists():
            media = CandidateMedia.objects.get(**filters)
            elements = [
                full_path_url('{0}{1}'.format(settings.MEDIA_URL, media.score)) if media.score else '',
                full_path_url('{0}{1}'.format(settings.MEDIA_URL, media.audio)) if media.audio else '',
                full_path_url('{0}{1}'.format(settings.MEDIA_URL, media.video)) if media.video else '',
                media.link,
                media.notes
            ]
            return '{0}: {1}'.format(media.title, ', '.join(
                [elt for elt in elements if elt]
            ))
        return ""

    def as_html(self, media):
        links = []
        if media.score:
            links.append(
                '<a href="{0}{1}" target="_blank">score</a>'.format(settings.MEDIA_URL, media.score)
            )
        if media.audio:
            links.append(
                '<a href="{0}{1}" target="_blank">audio</a>'.format(settings.MEDIA_URL, media.audio)
            )
        if media.video:
            links.append(
                '<a href="{0}{1}" target="_blank">video</a>'.format(settings.MEDIA_URL, media.video)
            )
        if media.link:
            links.append(
                '<a href="{0}" target="_blank">{1}</a>'.format(media.link, media.link)
            )


        return '{0} {1}'.format(
            media.title,
            ', '.join(['(' + link + ')' for link in links])
        )

    class Meta:
        model = TemporaryMedia


class VideoField(MediaField):
    field_type = 'VideoField'


class AudioField(MediaField):
    field_type = 'AudioField'


class ScoreAudioField(MediaField):
    field_type = 'ScoreAudioField'


class ScoreVideoField(MediaField):
    field_type = 'ScoreVideoField'


class YouTubeField(MediaField):
    field_type = 'YouTubeField'

    def get_youtube_link(self, link):
        if link:
            youtube_code = get_youtube_code(link)
            if youtube_code:
                return '<a href="{0}" class="colorbox-form">YouTube</a>'.format(
                    reverse('web:youtube_popup', args=[youtube_code])
                )
            else:
                return '<a href="{0}" target="_blank">YouTube</a>'.format(link)
        return ""

    def as_html(self, media):
        links = []
        if media.link:
            links.append(
                self.get_youtube_link(media.link)
            )
        return '{0} {1}'.format(
            media.title,
            ', '.join(['(' + link + ')' for link in links])
        )


class VideoYouTubeField(YouTubeField):
    field_type = 'VideoYouTubeField'

    def as_html(self, media):
        links = []
        if media.video:
            links.append(
                '<a href="{0}{1}" target="_blank">video</a>'.format(settings.MEDIA_URL, media.video)
            )
        if media.link:
            links.append(
                self.get_youtube_link(media.link)
            )
        return '{0} {1}'.format(
            media.title,
            ', '.join(['(' + link + ')' for link in links])
        )


class ApplicationElementFieldBase(ApplicationField):
    """
    Base class form application fields (value fields not linked to an item in personal composer space)
    """
    def __init__(
            self, competition, name, label, required, help_text, widget, preview_widget, admin_widget, jury_widget
    ):
        super(ApplicationElementFieldBase, self).__init__(
            competition, name, label, required, help_text, widget, preview_widget, admin_widget, jury_widget
        )

    def get_value_for_candidate(self, candidate, text=True):
        from .models import ApplicationElement
        filters = {'candidate': candidate, 'key': self.name}
        if ApplicationElement.objects.filter(**filters).exists():
            return '{0}'.format(ApplicationElement.objects.get(**filters).value)
        return ""

    def get_excel_value_for_candidate(self, candidate):
        return self.get_value_for_candidate(candidate)


class ApplicationTextElementField(ApplicationElementFieldBase):
    """
    Application single line text field  (e.g : "Project title")
    """
    def __init__(
        self, competition, name, label, required, help_text="", widget=None, preview_widget=None,
        admin_widget=None, jury_widget=None, maximum_length=0
    ):
        widget = widget or ApplicationTextElementWidget(maximum_length=maximum_length)
        preview_widget = preview_widget or ApplicationTextElementPreviewWidget(maximum_length=maximum_length)
        admin_widget = admin_widget or widget
        jury_widget = jury_widget or ApplicationTextElementJuryWidget()

        super(ApplicationTextElementField, self).__init__(
            competition, name, label, required, help_text, widget, preview_widget, admin_widget, jury_widget
        )


class ApplicationBooleanElementField(ApplicationElementFieldBase):
    """
    Application boolean field
    """
    def __init__(
        self, competition, name, label, required, help_text="", widget=ApplicationBooleanElementWidget(),
        preview_widget=ApplicationBooleanElementPreviewWidget(), admin_widget=ApplicationBooleanElementAdminWidget()
    ):
        super(ApplicationBooleanElementField,self).__init__(
            competition, name, label, required, help_text, widget, preview_widget, admin_widget, preview_widget
        )


class ApplicationHtmlElementField(ApplicationElementFieldBase):
    """
    Application multiline HTML field  (e.g : "Project abstract" or "Work plan")
    """
    def __init__(
        self, competition, name, label, required, help_text, widget=None,
        preview_widget=ApplicationHtmlElementPreviewWidget(),
        jury_widget=ApplicationHtmlElementJuryWidget(),
        maximum_length=0
    ):
        if widget is None:
            widget = ApplicationHtmlElementWidget(maximum_length)
        admin_widget = ApplicationHtmlElementWidget(maximum_length)
        super(ApplicationHtmlElementField,self).__init__(
            competition, name, label, required, help_text, widget, preview_widget, admin_widget, jury_widget
        )

    def get_excel_value_for_candidate(self, candidate):
        return dehtml(self.get_value_for_candidate(candidate))


class CompetitionAttributeValueField(ApplicationField):
    """
    Application field corresponding to an (attribute,value) for the candidate.
    The values are choosed among those of the specified competition attribute
    """
    def __init__(self, competition, competition_attribute, name, label, required, help_text):
        attribute_choices = [(0, '-----------')]
        attribute_choices.extend(
            [
                (cav.key,cav.value)
                for cav in CompetitionAttributeValue.objects.filter(attribute=competition_attribute)
            ]
        )
        widget = forms.Select(choices=attribute_choices)
        super(CompetitionAttributeValueField,self).__init__(
            competition, name, label, required, help_text,
            widget,
            CompetitionAttributeValuePreviewWidget(competition_attribute),
            widget,  # admin widget makes possible to edit
            CompetitionAttributeValueJuryWidget(competition_attribute)
        )
        self.competition_attribute = competition_attribute

    def get_data(self):
        return {
            'name': self.name, 'label': self.label, 'required': self.required, 'help_text': self.help_text,
            'competition_attribute': self.competition_attribute
        }

    def get_value_for_candidate(self, candidate, text=True):
        from .models import CandidateAttributeValue
        filters = {'candidate': candidate, 'value__attribute': self.competition_attribute, 'attribute': self.name}
        if CandidateAttributeValue.objects.filter(**filters).exists():
            return '{0}'.format(CandidateAttributeValue.objects.get(**filters).value.value)
        else:
            return "Do not exist"

    def get_excel_value_for_candidate(self, candidate):
        return self.get_value_for_candidate(candidate)


class CompetitionAttributeMultipleValueField(ApplicationField):
    """
    Application field corresponding to 1 to n (attribute,value)(s) for the candidate.
    The values are choosed among those of the specified competition attribute
    """
    def __init__(self, competition, competition_attribute, name, label, required, help_text):
        attribute_choices = [
            (cav.key, cav.value) for cav in CompetitionAttributeValue.objects.filter(attribute=competition_attribute)
        ]
        widget = CheckboxSelectMultipleWidget(choices=attribute_choices, attrs={'class': 'SelectMultipleElement'})
        preview_widget = CheckboxSelectMultiplePreviewWidget(
            choices=attribute_choices, attrs={'class':'SelectMultipleElementPreview'}
        )
        jury_widget = CheckboxSelectMultiplePreviewWidget(
            choices=attribute_choices, attrs={'class': 'SelectMultipleElement'}
        )
        super(CompetitionAttributeMultipleValueField, self).__init__(
            competition, name, label, required, help_text, widget, preview_widget, widget, jury_widget
        )
        self.competition_attribute = competition_attribute

    def get_data(self):
        return {
            'name': self.name, 'label': self.label, 'required': self.required, 'help_text': self.help_text,
            'competition_attribute': self.competition_attribute
        }

    def get_value_for_candidate(self, candidate, text=True):
        from .models import CandidateAttributeValue
        filters = {
            'candidate': candidate, 'value__attribute': self.competition_attribute, 'attribute__startswith': self.name
        }
        if CandidateAttributeValue.objects.filter(**filters).exists():
            values = CandidateAttributeValue.objects.filter(**filters)
            return ", ".join([item.value.key for item in values])

    def get_excel_value_for_candidate(self, candidate, text=True):
        from .models import CandidateAttributeValue
        filters = {
            'candidate': candidate, 'value__attribute': self.competition_attribute, 'attribute__startswith': self.name
        }
        if CandidateAttributeValue.objects.filter(**filters).exists():
            values = CandidateAttributeValue.objects.filter(**filters)
            return ", ".join([item.value.value for item in values])
        return ""


class ApplicationElementMultipleValueField(ApplicationElementFieldBase):

    def __init__(self, name, label, required, help_text, widget, preview_widget, jury_widget):
        super(ApplicationElementMultipleValueField,self).__init__(
            name, label, required, help_text, widget, preview_widget, widget, jury_widget
        )
