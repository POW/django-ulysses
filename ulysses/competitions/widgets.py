# -*- coding: utf-8 -*-

from django.conf import settings
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.html import escape
from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.competitions.models import CompetitionAttributeValue
from ulysses.generic.widgets import DEFAULT_TINCY_MCE_ATTRS, MINI_TINCY_MCE_ATTRS
from ulysses.generic.utils import url_to_link, get_request

from .models import TemporaryDocument, TemporaryMedia


def allow_import_from_personal_space():
    return getattr(settings, 'ULYSSES_ALLOW_IMPORT_FROM_PERSONAL_SPACE', True)


class ApplicationWidget(forms.Widget):
    include_hidden_input = False

    def render(self, name, value, attrs=None, **kwargs):
        div_opening_tag = '<div name="{0}" id="id_{0}"'.format(name)
        final_attrs = self.build_attrs(attrs, {'name': name})
        final_attrs['class'] = 'form-control'
        if "class" in final_attrs:
            div_opening_tag += ' class="{0}"'.format(final_attrs["class"])
        div_opening_tag += ">"
        value_str = ""
        if value:
            value_str = "{0}".format(value)
        result = mark_safe("{0}{1}</div>".format(div_opening_tag, value_str))
        if "show_clear_link" in final_attrs:
            result += mark_safe(
                "<div class='ClearButton'><span class='ClearLink' id='clear_{0}'>Clear</span></div>".format(name)
            )
        if self.include_hidden_input:
            value = escape(value)
            result += '<input type="hidden" name={0} value="{1}" />'.format(name, value)
        return result


class CompetitionAttributeValueWidget(forms.Widget):

    def render(self, name, value, attrs=None, **kwargs):
        # Render the value
        if CompetitionAttributeValue.objects.filter(key=value, attribute=self.competition_attribute).exists():
            text = CompetitionAttributeValue.objects.get(key=value, attribute=self.competition_attribute).value
        else:
            text = value
        div_opening_tag = "<div name='{0}_text' id='id_{0}_text'".format(name)
        final_attrs = self.build_attrs(attrs)
        if "class" in final_attrs:
            div_opening_tag += " class='%s'" % final_attrs["class"]
        div_opening_tag += ">"
        # Store the key in a hidden field
        result = mark_safe(
            "{0}{1}</div><input type='hidden' name='{2}' id='id_{2}' value='{3}'></input>".format(
                div_opening_tag, text, name, value
            )
        )
        return result


class CompetitionAttributeValuePreviewWidget(CompetitionAttributeValueWidget):

    def __init__(self, competition_attribute, attrs=None, *args, **kwargs):
        default_attrs = {'class': 'CompetitionAttributeValuePreview'}
        if attrs:
            default_attrs.update(attrs)
        super(CompetitionAttributeValuePreviewWidget,self).__init__(default_attrs, *args, **kwargs)
        self.competition_attribute = competition_attribute


class CompetitionAttributeValueJuryWidget(CompetitionAttributeValueWidget):

    def __init__(self, competition_attribute, attrs=None, *args, **kwargs):
        default_attrs = {'class': 'CompetitionAttributeValueJury'}
        if attrs:
            default_attrs.update(attrs)
        super(CompetitionAttributeValueJuryWidget,self).__init__(default_attrs, *args, **kwargs)
        self.competition_attribute = competition_attribute


class ApplicationTextElementWidget(forms.TextInput):
    def __init__(self, attrs=None, *args, **kwargs):
        maximum_length = kwargs.pop("maximum_length", 0)
        if attrs is None:
            attrs = {}
        css_class = 'form-control'
        if maximum_length:
            css_class += ' maxlength'
            attrs.update({'maxlength': maximum_length})
        attrs.update({'class': css_class})
        self.maximum_length = maximum_length
        super(ApplicationTextElementWidget, self).__init__(attrs, *args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(ApplicationTextElementWidget, self).render(name, value, attrs, **kwargs)
        if self.maximum_length:
            counter = '<div class="tinymce-word-counter" id="word_counter_id_{0}">{1}</div>'.format(
                name, self.maximum_length
            )
        else:
            counter = ''

        return '{0}{1}'.format(
            counter,
            html
        )


class ApplicationTextElementPreviewWidget(ApplicationTextElementWidget):

    def __init__(self, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {}
        default_attrs = {'readonly': 'readonly'}
        attrs.update(default_attrs)
        super(ApplicationTextElementPreviewWidget, self).__init__(attrs, *args, **kwargs)


class ApplicationTextElementJuryWidget(forms.Widget):

     def render(self, name, value, attrs=None, **kwargs):
        div_opening_tag = '<div name="{0}" id="id_{0}"'.format(name)
        final_attrs = self.build_attrs(attrs)
        if "class" in final_attrs:
            div_opening_tag += ' class="{0}"'.format(final_attrs["class"])
        div_opening_tag += ">"
        value_str = ""
        if value:
            value_str = "{0}".format(value)
        value_str = url_to_link(value_str)
        result = mark_safe("{0}{1}</div>".format(div_opening_tag, value_str))
        return result


class ApplicationBooleanElementWidget(forms.CheckboxInput):
    template_name = "floppyforms/checkbox.html"

    def __init__(self, attrs=None, *args, **kwargs):
        super(ApplicationBooleanElementWidget, self).__init__(attrs, *args, **kwargs)


class ApplicationBooleanElementPreviewWidget(forms.CheckboxInput):
    template_name = "floppyforms/checkbox_disabled.html"

    def __init__(self, attrs=None, *args, **kwargs):
        default_attrs = {'readonly': 'readonly'}
        if attrs:
            attrs.update(default_attrs)
        super(ApplicationBooleanElementPreviewWidget,self).__init__(attrs, *args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        if isinstance(value, str):
            value = (value.lower() == 'true')
        return super().render(name, value, attrs, **kwargs)


class ApplicationBooleanElementAdminWidget(ApplicationBooleanElementWidget):

    def render(self, name, value, attrs=None, **kwargs):
        if isinstance(value, str):
            value = (value.lower() == 'true')
        return super().render(name, value, attrs, **kwargs)


class ApplicationHtmlElementWidget(forms.Textarea):

    def __init__(self, maximum_length=0, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {}
        attrs.update(DEFAULT_TINCY_MCE_ATTRS)
        attrs['data-maximum_length'] = maximum_length
        attrs.update({'class': 'form-control tinymce'})
        super(ApplicationHtmlElementWidget, self).__init__(attrs, *args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(ApplicationHtmlElementWidget, self).render(name, value, attrs, **kwargs)
        button_html = ''
        button_html += '&nbsp;&nbsp;<div class="import-portfolio clear-tinymce" rel="id_{0}">{1}</div>'.format(
            name, _('Clear'),
        )
        button_html += '&nbsp;&nbsp;<div class="tinymce-word-counter" id="word_counter_id_{0}">0</div>'.format(
            name,
        )
        output = mark_safe("{0}{1}".format(button_html, html))
        return output


class ApplicationHtmlElementPreviewWidget(forms.Textarea):
    def __init__(self, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {}
        tiny_mce_attrs = {
            'data-height': '100px',
        }
        attrs.update(tiny_mce_attrs)
        attrs.update({'data-readonly': '1'})
        attrs.update({'class': 'form-control tinymce'})
        super(ApplicationHtmlElementPreviewWidget, self).__init__(attrs, *args, **kwargs)


class ApplicationHtmlElementJuryWidget(forms.Textarea):

    def __init__(self, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {'class': 'tinymce'}
        attrs.update({'readonly': 'readonly'})
        super(ApplicationHtmlElementJuryWidget,self).__init__(attrs, *args, **kwargs)


class ElementWithHiddenFieldWidget(ApplicationWidget):

    def value_from_datadict(self, data, files, name):
        key = "%s_hidden" % name
        return data.get(key, None)


class ElementAdmindWidget(ApplicationWidget):
    include_hidden_input = True


class BiographicElementAdminWidget(ElementAdmindWidget):

    def __init__(self, attrs=None):
        default_attrs = {'class': 'CandidateBiographicElement'}
        if attrs:
            default_attrs.update(attrs)
        super(BiographicElementAdminWidget, self).__init__(default_attrs)

    def render(self, name, value, attrs=None, **kwargs):
        result = super(BiographicElementAdminWidget, self).render(name, value, attrs, **kwargs)
        result += mark_safe(
            "<div class='admin-edit-link'><a href='edit_biographicelement/?key=%s'>Edit element</a></div>" % (
                attrs["id"]
            )
        )
        return result


class DocumentAdminWidget(ElementWithHiddenFieldWidget):
    extra_files = False

    def __init__(self, extra_files, attrs=None):
        self.extra_files = extra_files
        default_attrs = {'class': 'CandidateDocument'}
        if attrs:
            default_attrs.update(attrs)
        super(DocumentAdminWidget, self).__init__(default_attrs)

    def render(self, name, value, attrs=None, **kwargs):
        result = super(DocumentAdminWidget, self).render(name, value, attrs, **kwargs)
        result += mark_safe(
            "<div class='admin-edit-link'><a href='edit_document/?key=%s'>Edit document</a></div>" % attrs["id"]
        )
        return result


class MediaAdminWidget(ElementWithHiddenFieldWidget):

    def __init__(self, attrs=None):
        default_attrs = {'class': 'CandidateMedia'}
        if attrs:
            default_attrs.update(attrs)
        super(MediaAdminWidget, self).__init__(default_attrs)

    def render(self, name, value, attrs=None, **kwargs):
        result = super(MediaAdminWidget, self).render(name, value, attrs, **kwargs)
        result += mark_safe(
            "<div class='admin-edit-link'><a href='edit_media/?key=%s'>Edit media</a></div>" % attrs["id"]
        )
        return result


class BiographicElementPreviewWidget(forms.Textarea):
    def __init__(self, competition, attrs=None, *args, **kwargs):
        self.competition = competition
        if attrs is None:
            attrs = {}
        tiny_mce_attrs = {
            'data-height': '100px',
        }
        attrs.update(tiny_mce_attrs)
        attrs.update({'data-readonly': '1'})
        attrs.update({'class': 'form-control tinymce'})
        super(BiographicElementPreviewWidget, self).__init__(attrs=attrs, *args, **kwargs)


class BiographicElementEditWidget(forms.Textarea):

    def __init__(self, competition, maximum_length=0, attrs=None, *args, **kwargs):
        self.competition = competition
        if attrs is None:
            attrs = {'class': 'form-control tinymce'}
        attrs.update(MINI_TINCY_MCE_ATTRS)
        attrs['data-maximum_length'] = maximum_length
        super(BiographicElementEditWidget, self).__init__(attrs=attrs, *args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(BiographicElementEditWidget, self).render(name, value, attrs, **kwargs)
        if allow_import_from_personal_space():
            button_html = '<a href="{0}" class="import-portfolio colorbox-form">{1}</a>'.format(
                reverse('web:select_biography_from_portfolio', args=[self.competition.url, name]),
                _('Upload from profile'),
            )
        else:
            button_html = ''
        button_html += '&nbsp;&nbsp;<div class="import-portfolio clear-tinymce" rel="id_{0}">{1}</div>'.format(
            name, _('Clear'),
        )
        button_html += '&nbsp;&nbsp;<div class="tinymce-word-counter" id="word_counter_id_{0}">0</div>'.format(
            name,
        )
        output = mark_safe("{0}{1}".format(button_html, html))
        return output


class DocumentBaseWidget(ElementWithHiddenFieldWidget):
    extra_files = False
    preview = False

    def get_value_for_display(self, value):
        if value:
            try:
                if self.extra_files:
                    return TemporaryDocument.objects.get(id=value).as_edit_html(self.preview)
                else:
                    return TemporaryDocument.objects.get(id=value).as_html
            except (ValueError, TemporaryDocument.DoesNotExist):
                pass
        return value

    def render(self, name, value, attrs=None, **kwargs):
        return super(DocumentBaseWidget, self).render(name, self.get_value_for_display(value), attrs, **kwargs)


class DocumentEditWidget(DocumentBaseWidget):

    def __init__(self, competition, extra_files, *args, **kwargs):
        self.competition = competition
        self.extra_files = extra_files
        super(DocumentEditWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(DocumentEditWidget, self).render(name, value, attrs, **kwargs)

        button1_html = '''
            <a class="btn-icon colorbox-form" href="{0}"><img src="{1}icons/upload-doc2.svg" /></a>
        '''.format(
            reverse('web:upload_document', args=[self.competition.url, name]),
            settings.STATIC_URL
        )
        if not self.extra_files:
            button2_html = '''
                <div href class="btn-icon application-remove-document"><img src="{0}icons/trash-can-green.svg" /></div>
            '''.format(
                settings.STATIC_URL
            )
        else:
            button2_html = '''
                <a href="{1}" id="{3}_add_file" class="{4} btn-icon colorbox-form"><img src="{0}icons/add-square.svg" /></a>
                <a href="{2}" class="btn-icon colorbox-form"><img src="{0}icons/trash-can-green.svg" /></a>
            '''.format(
                settings.STATIC_URL,
                reverse('web:add_temporary_document_file', args=[self.competition.url, name]),
                reverse('web:remove_document', args=[self.competition.url, name]),
                name,
                'disabled' if not value else ''
            )

        if not self.extra_files and allow_import_from_personal_space():
            button3_html = '<a href={0} class="import-portfolio colorbox-form">{1}</a>'.format(
                reverse('web:select_document_from_portfolio', args=[self.competition.url, name]),
                _('Upload from profile'),
            )
        else:
            button3_html = ''

        output = mark_safe('{3}<div class="upload-field">{0}{1}{2}</div>'.format(
            html, button1_html, button2_html, button3_html)
        )
        return output


class DocumentPreviewWidget(DocumentBaseWidget):

    def __init__(self, competition, extra_files, *args, **kwargs):
        self.competition = competition
        self.extra_files = extra_files
        self.preview = True
        super(DocumentPreviewWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(DocumentPreviewWidget, self).render(name, value, attrs, **kwargs)
        output = mark_safe('<div class="upload-field">{0}</div>'.format(html))
        return output


class MediaBaseWidget(ElementWithHiddenFieldWidget):

    def get_value_for_display(self, value):
        if value:
            try:
                value = TemporaryMedia.objects.get(id=value).as_html
            except (ValueError, TemporaryMedia.DoesNotExist):
                pass
        return value

    def render(self, name, value, attrs=None, **kwargs):
        return super(MediaBaseWidget, self).render(name, self.get_value_for_display(value), attrs, **kwargs)


class MediaEditWidget(MediaBaseWidget):

    def __init__(self, competition, field_type, *args, **kwargs):
        self.competition = competition
        self.field_type = field_type
        super(MediaEditWidget, self).__init__(*args, **kwargs)

    def get_upload_url(self, name):
        return reverse('web:upload_media', args=[self.competition.url, name, self.field_type])

    def render(self, name, value, attrs=None, **kwargs):
        html = super(MediaEditWidget, self).render(name, value, attrs, **kwargs)
        button1_html = '''
            <a class="btn-icon colorbox-form" href="{0}"><img src="{1}icons/upload-doc2.svg" /></a>
        '''.format(
            self.get_upload_url(name),
            settings.STATIC_URL
        )
        button2_html = '''
            <div href class="btn-icon application-remove-media"><img src="{0}icons/trash-can-green.svg" /></div>
        '''.format(
            settings.STATIC_URL
        )
        personal_space_fields = ["MediaField", "AudioField", "VideoField", "ScoreAudioField", "ScoreVideoField"]
        if self.field_type in personal_space_fields and allow_import_from_personal_space():
            button3_html = '<a href="{0}" class="import-portfolio colorbox-form">{1}</a>'.format(
                reverse('web:select_media_from_portfolio', args=[self.competition.url, name, self.field_type]),
                _('Upload from profile'),
            )
        else:
            button3_html = ""
        output = mark_safe('{3}<div class="upload-field">{0}{1}{2}</div>'.format(
            html, button1_html, button2_html, button3_html
        ))
        return output


class MediaPreviewWidget(MediaBaseWidget):

    def __init__(self, competition, *args, **kwargs):
        self.competition = competition
        super(MediaPreviewWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(MediaPreviewWidget, self).render(name, value, attrs, **kwargs)
        output = mark_safe('<div class="upload-field">{0}</div>'.format(html))
        return output


class CheckboxSelectMultipleWidget(forms.CheckboxSelectMultiple):
    template_name = "floppyforms/checkbox_select.html"


class CheckboxSelectMultiplePreviewWidget(forms.CheckboxSelectMultiple):
    template_name = "floppyforms/checkbox_select_disabled.html"
