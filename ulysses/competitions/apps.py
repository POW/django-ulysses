# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.competitions'
    label = 'ulysses_competitions'
    verbose_name = "Ulysses Competitions"
