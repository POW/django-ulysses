# -*- coding: utf-8 -*-
import math

from django.conf import settings
from django.conf.urls import url
from django.contrib import admin
from django.contrib.admin import TabularInline
from django.contrib.admin.utils import quote
from django.contrib.auth.tokens import default_token_generator
from django.contrib.messages import success, error
from django.contrib.sites.models import Site
from django.core.mail import EmailMultiAlternatives
from django.db import connection
from django.forms.models import BaseInlineFormSet
from django.http import HttpResponseRedirect
from django.template import loader
from django.template.defaultfilters import slugify
from django.urls import reverse
from django.utils.translation import ugettext as _, ugettext_lazy as __
from django.utils import encoding
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.safestring import mark_safe

from functools import update_wrapper
from django.shortcuts import redirect, render

import floppyforms.__future__ as forms
from adminsortable.admin import SortableTabularInline, SortableAdmin
from django_extensions.admin import ForeignKeyAutocompleteAdmin

from ulysses.middleware import get_request
from ulysses.utils import get_age, get_excel_response, get_excel_workbook
from ulysses.context_processors import in_competition_admin
from ulysses.super_admin.sites import super_admin_site

from ulysses.profiles.utils import get_profile

from .filters import (
    CompetitionStepFilter, CandidateGroupFilter, JuryMemberCompetitionFilter, JuryMemberGroupFilter,
    get_competition_attribute_filter_class
)
from .models import (
    Candidate, CandidateBiographicElement, CandidateDocument, CandidateMedia, ApplicationDraft, ApplicationElement,
    Call, EvaluationType, Competition, CompetitionStatus, CompetitionAttribute, CompetitionAttributeValue,
    CompetitionStep, CompetitionNews, DraftMedia, JuryMember, JuryMemberNotification, CandidateNotification,
    JuryMemberGroup, CandidateGroup, JuryMemberCompetition, DynamicApplicationForm, CompetitionManager, StaticContent,
    DraftBiographicElement, TemporaryDocument, DraftDocument, TemporaryMedia, ApplicationFormFieldType,
    JuryApplicationElementToExclude, ApplicationFormFieldSetDefinition, ApplicationFormFieldDefinition,
    Evaluation,EvaluationNote, EvaluationStatus, EvaluationField, CandidateAttributeValue, ProfileMandatoryFields,
    CallRequest, CandidateDocumentExtraFile, TemporaryDocumentExtraFile, DraftDocumentExtraFile
)
from .utils import clone_competition

class CandidateFilterListFilter(admin.SimpleListFilter):
    """Make possible to add an extra argument in url for checking the competition"""
    parameter_name = 'filter_str'
    title = "Choice"

    def lookups(self, request, model_admin):
        active_competition = model_admin.admin_site.get_active_competition(request)
        #candidate__competition=active_competition --> ne retourne que les attrbuts choisis par au moins un candidat
        queryset = CompetitionAttributeValue.objects.filter(
            attribute__competition=active_competition,
            is_filter=True
        )
        return [(attribute.id, attribute.value) for attribute in queryset]

    def queryset(self, request, queryset):
        attribute = self.value()
        if attribute:
            return queryset.filter(attributes=attribute)
        else:
            return queryset


class CandidateRecommendationListFilter(admin.SimpleListFilter):
    """Make possible to add an extra argument in url for checking the competition"""
    parameter_name = 'recommendation_str'
    title = "Recommendation"

    def lookups(self, request, model_admin):
        active_competition = model_admin.admin_site.get_active_competition(request)
        #candidate__competition=active_competition --> ne retourne que les attrbuts choisis par au moins un candidat
        queryset = CompetitionAttributeValue.objects.filter(
            attribute__competition=active_competition,
            is_filter=True,
            hide_recommendation=False
        )
        return [(attribute.id, attribute.value) for attribute in queryset]

    def queryset(self, request, queryset):
        attribute = self.value()
        if attribute:
            return queryset.filter(evaluation__recommendation=attribute)
        else:
            return queryset


def wrap_queryset(model_admin,qs):
    ordering = model_admin.ordering or () # otherwise we might try to *None, which is bad ;)
    if ordering:
        qs = qs.order_by(*ordering)
    return qs


def manage_candidates_to_competition_step(modeladmin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    url = reverse('competition-admin:manage_candidates_to_competition_step')
    return redirect("{0}?ids={1}".format(url, ",".join(selected)))
manage_candidates_to_competition_step.short_description = __("Manage steps for selected candidates")


def manage_steps_for_candidates(model_admin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    url = reverse('competition-admin:manage_steps_candidates')
    return redirect("{0}?ids={1}&referer={2}ƒ".format(
        url, ",".join(selected), referer)
    )
manage_steps_for_candidates.short_description = __("Manage steps for selected candidates")


def manage_jury_members_to_competition_step(model_admin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    url = reverse('competition-admin:manage_jury_members_to_competition_step')
    return redirect("{0}?ids={1}".format(url, ",".join(selected)))
manage_jury_members_to_competition_step.short_description = __("Manage steps for selected jury members")


def manage_steps_for_jury_members(model_admin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    url = reverse('competition-admin:jury_manage_steps')
    return redirect("{0}?ids={1}&referer={2}".format(
        url, ",".join(selected), referer)
    )
manage_steps_for_jury_members.short_description = __("Manage steps for selected jury members")


def manage_groups_for_candidates(model_admin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    url = reverse('competition-admin:manage_groups_candidates')
    # To be improved with the use of reverse
    return redirect("{0}?ids={1}&referer={2}".format(url, ",".join(selected), referer))
manage_groups_for_candidates.short_description = __("Manage groups for selected candidates")


def remove_candidate_from_competition(modeladmin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    url = reverse('competition-admin:remove_candidate_from_competition')
    return redirect("{0}?ids={1}&referer={2}".format(url, ",".join(selected), referer))
remove_candidate_from_competition.short_description = __("Remove selected candidates from current competition")


def _get_column_headers(modeladmin, columns):
    column_headers = []
    for column in columns:
        admin_col = getattr(modeladmin, column, None)
        if admin_col and hasattr(admin_col, 'short_description'):
            column_headers.append(admin_col.short_description)
        else:
            column_headers.append(column)
    return column_headers

def export_to_excel(modeladmin, request, queryset):
    """
    Export the current change list to Excel
    """
    columns = getattr(modeladmin, 'export_fields', modeladmin.list_display)
    columns = [name for name in columns if name != "action_checkbox"]
    column_headers = _get_column_headers(modeladmin, columns)

    lines = []
    export_qs = getattr(modeladmin, 'get_export_queryset', None)
    queryset = export_qs(request) if export_qs else queryset
    for obj in queryset:
        cells = []
        for column in columns:
            if hasattr(obj, column):
                value = getattr(obj, column)
                if callable(value):
                    value = value()
            elif hasattr(modeladmin, column):
                value = eval("modeladmin.%s(obj)" % column)
            else:
                value = "## ERROR ##"
            cell_content = ""
            if value:
                try:
                    cell_content = '{0}'.format(value)
                except UnicodeDecodeError:
                    cell_content = encoding.smart_str(value, encoding='ascii', errors='ignore')
            cells.append({'value': cell_content})
        lines.append({'cells': cells})
    wb = get_excel_workbook(column_headers, lines)
    return get_excel_response(wb)
export_to_excel.short_description = __("Export to excel")


def export_candidates_to_excel(modeladmin, request, queryset):
    """ Export the current candidate list to Excel """
    # Base on the following list_display = ('name','groups_str',...)
    # import pdb;pdb.set_trace()
    columns = [name for name in getattr(modeladmin, 'export_fields', modeladmin.list_display) if name!="action_checkbox"]
    column_headers = _get_column_headers(modeladmin, columns)

    lines = []
    export_qs = getattr(modeladmin, 'get_export_queryset')
    queryset = export_qs(request) if export_qs else queryset
    for obj in queryset:
        cells = []
        for column in columns:
            if hasattr(modeladmin,column):
                value = eval("modeladmin.%s(obj)" % column)
            elif hasattr(obj,column):
                value = getattr(obj,column)
            else:
                value = "## ERROR ##"
            cell_content = ""
            if value:
                # This may be a callable : if so, call it for getting the value
                if callable(value):
                    value = value()
                cell_content = '{0}'.format(value)
            cells.append({'value': cell_content})
        lines.append({'cells': cells})

    wb = get_excel_workbook(column_headers, lines)
    return get_excel_response(wb)
export_candidates_to_excel.short_description = __("Export candidates to excel")

def export_results_to_excel(modeladmin, request, queryset):
    """ Export the current candidate list to Excel """
    # Base on the following list_display = ('name','groups_str',...)
    # Base columns without jury, average...
    columns = [name for name in getattr(modeladmin, 'export_fields', modeladmin.list_display) if name!="action_checkbox"][:10]
    # all columns including jury, average
    column_headers = _get_column_headers(modeladmin, columns)
    lines = []
    export_qs = getattr(modeladmin, 'get_export_queryset')
    queryset = export_qs(request) if export_qs else queryset

    jury_members = Evaluation.objects.filter(competition_step=modeladmin.competition_step).values('jury_member__user__last_name').distinct().order_by('jury_member__user__last_name')
    jury_members = [jury_member['jury_member__user__last_name'] for jury_member in jury_members]
    column_headers.extend(jury_members)

    for i, obj in enumerate(queryset):
        cells = []
        for column in columns:
            if hasattr(modeladmin,column):
                value = eval("modeladmin.%s(obj)" % column)
            elif hasattr(obj,column):
                value = getattr(obj,column)
            else:
                value = "## ERROR ##"
            cell_content = ""
            if value:
                # This may be a callable : if so, call it for getting the value
                if callable(value):
                    value = value()
                cell_content = '{0}'.format(value)
            cells.append({'value': cell_content})


        evaluations = Evaluation.objects.filter(candidate=obj, competition_step=modeladmin.competition_step)
        evaluations_per_jury = {}
        for evaluation in evaluations:
            evaluations_per_jury[evaluation.jury_member.user.last_name] = evaluation
        # add jury names to columns
        notes = []
        for jury_member in jury_members:
            evaluation = evaluations_per_jury.get(jury_member)
            if not evaluation:
                cells.append({'value': ''})
            else:
                jury_member_sum = evaluation.get_sum_note()
                if jury_member_sum:
                    notes.append(jury_member_sum)
                cells.append({'value': jury_member_sum})
        if notes:
            # average
            average = sum(notes) / len(notes)
            cells.append({'value': round(average, 2)})
            # standard deviation
            var = sum((note - average) ** 2 for note in notes) / len(notes)
            st_dev = math.sqrt(var)
            cells.append({'value': round(st_dev, 2)})

        lines.append({'cells': cells})

    column_headers.extend(('average', 'standard deviation'))
    wb = get_excel_workbook(column_headers, lines)
    return get_excel_response(wb)
export_results_to_excel.short_description = __("Export results to excel")

def manage_groups_for_jury_members(model_admin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    return redirect("manage_groups?ids=%s&referer=%s" % (",".join(selected), referer))
manage_groups_for_jury_members.short_description = __("Manage groups for selected jury members")


def remove_jury_members_from_competition(model_admin, request, queryset):
    jury_names = []
    active_competition = model_admin.admin_site.get_active_competition(request)
    for jury_member in queryset:
        if jury_member.competitions.filter(id=active_competition.id).exists():
            jury_names.append(jury_member.name())
            jury_member.competitions.remove(active_competition)
            # Remove steps
            for step in jury_member.steps.filter(competition=active_competition):
                jury_member.steps.remove(step)
            jury_member.save()
            # Remove evaluations
            Evaluation.objects.filter(
                jury_member=jury_member, competition_step__competition=active_competition
            ).delete()

    if jury_names:
        success(request, _('{0} have been removed from competition jurys').format(
            ', '.join(jury_names)
        ))
    else:
        error(request, _('None of the selected juries are juries of the current competition'))

remove_jury_members_from_competition.short_description = __("Remove selected jury members from current competition")


def add_candidates_to_group(modeladmin, request, queryset):
    selected = list(map(str, queryset.values_list('pk', flat=True)))
    referer = request.META["HTTP_REFERER"]
    return redirect("add_to_group?ids=%s&referer=%s" % (",".join(selected),referer) )

add_candidates_to_group.short_description = __("Add selected candidates to a group")


def mark_candidates_as_valid(modeladmin, request, queryset):
    selected = queryset.values_list('pk', flat=True)
    Candidate.objects.filter(id__in=list(selected)).update(is_valid=True)

mark_candidates_as_valid.short_description = __("Mark selected candidates as valid")


def mark_candidates_as_invalid(modeladmin, request, queryset):
    selected = queryset.values_list('pk', flat=True)
    Candidate.objects.filter(id__in=list(selected)).update(is_valid=False)

mark_candidates_as_invalid.short_description = __("Mark selected candidates as invalid")


class JuryMemberCompetitionInline(TabularInline):
    model = JuryMemberCompetition


class CompetitionModelAdmin(admin.ModelAdmin):

    def url_for_pk(self, pk, app_label, model_name):

        is_super_admin = getattr(self, 'is_super_admin', True)
        if is_super_admin:
            prefix = 'admin'
        else:
            prefix = 'competition-admin'

        if model_name == "_candidateproxy":
            model_name = "candidate"

        return reverse(
            '%s:%s_%s_change' % (prefix, app_label, model_name), args=(quote(pk),),
            current_app=self.admin_site.name
        )

    def response_change(self, request, obj):
        opts = obj._meta
        verbose_name = opts.verbose_name
        model_name = opts.model_name
        is_super_admin = getattr(self, 'is_super_admin', True)
        prefix = 'admin' if is_super_admin else 'competition-admin'
        post_url = reverse(
            '%s:%s_%s_changelist' % (prefix, opts.app_label, model_name),
            current_app=self.admin_site.name
        )
        return HttpResponseRedirect(post_url)

    def get_changelist(self, request, **kwargs):
        ChangeList = super(CompetitionModelAdmin, self).get_changelist(request, **kwargs)

        _host = self

        class _ChangeList(ChangeList):

            def url_for_result(self, result):
                pk = getattr(result, self.pk_attname)
                return _host.url_for_pk(pk, self.opts.app_label, self.opts.model_name)

        return _ChangeList

    def get_urls(self):

        def wrap(view):
            def wrapper(*args, **kwargs):
                # "super admin" doesn't ask for competition selection
                if hasattr(self.admin_site, 'competition_admin_view'):
                    return self.admin_site.competition_admin_view(view)(*args, **kwargs)
                return view(*args, **kwargs)
            return update_wrapper(wrapper, view)

        info = self.model._meta.app_label, self.model._meta.model_name

        urlpatterns = [
            url(r'^$', wrap(self.changelist_view), name='%s_%s_changelist' % info),
            url(r'^add/$', wrap(self.add_view), name='%s_%s_add' % info),
            url(r'^(.+)/history/$', wrap(self.history_view), name='%s_%s_history' % info),
            url(r'^(.+)/delete/$', wrap(self.delete_view), name='%s_%s_delete' % info),
            url(r'^(.+)/$', wrap(self.change_view), name='%s_%s_change' % info),
        ]

        return urlpatterns


class CandidateAttributeValueAdmin(CompetitionModelAdmin):
    search_fields = ('candidate__composer__user__last_name', )
    list_display = ('candidate', 'attribute', 'value', )
    raw_id_fields = ('value', )


class CompetitionStatusAdmin(CompetitionModelAdmin):
    list_display = ('name', 'order_index')


class ApplicationElementInline(TabularInline):
    model = ApplicationElement


class CandidateBiographicElementInline(TabularInline):
    model = CandidateBiographicElement


def get_candidate_document_inline_form(candidate):
    application_form = candidate.competition.get_application_form_instance()
    form_choices = [('', '----------')]
    form_choices.extend([(key,key) for key in application_form.get_document_keys()])

    class _CandidateDocumentInlineForm(forms.ModelForm):
        model = CandidateDocument
        key = forms.ChoiceField(label="key",choices=form_choices)
        file = forms.CharField(
            label="file",
        )

    return _CandidateDocumentInlineForm


def get_candidate_media_inline_form(candidate):
    application_form = candidate.competition.get_application_form_instance()
    form_choices = [('', '----------')]
    form_choices.extend([(key,key) for key in application_form.get_media_keys()])

    class _CandidateMediaInlineForm(forms.ModelForm):
        model = CandidateMedia
        key = forms.ChoiceField(label="key",choices=form_choices)
        score = forms.CharField(
            label="score",
            required=False,
        )
        audio = forms.CharField(
            label="audio", required=False,
        )
        video = forms.CharField(
            label="video",
            required=False,
        )

        def clean(self):
            # Call base class first
            super(_CandidateMediaInlineForm,self).clean()
            if self._errors:  # no need to go further if the form is already invalid
                return self.cleaned_data

            # Ensure at least one one the following elements (audio, score, video) has been provided
            score = self.cleaned_data["score"]
            audio = self.cleaned_data["audio"]
            video = self.cleaned_data["video"]

            if not (score or audio or video):
                raise forms.ValidationError(
                    _("Please provide at least one on the following elements : score, audio or video")
                )

            # Return cleaned data
            return self.cleaned_data

    return _CandidateMediaInlineForm


def ensure_no_duplicate_keys(formset):
    keys = []
    for form in formset.forms:
        if hasattr(form,"cleaned_data"):
            cleaned_data = form.cleaned_data
            if cleaned_data:
                delete = cleaned_data["DELETE"]
                key = cleaned_data["key"]
                if not delete:
                    if key in keys:
                        raise forms.ValidationError("You specified multiple elements with key '%s'" %key)
                    keys.append(key)

class CandidateDocumentInlineFormset(BaseInlineFormSet):

    def clean(self):
        ensure_no_duplicate_keys(self)


class CandidateMediaInlineFormset(BaseInlineFormSet):

    def clean(self):
        ensure_no_duplicate_keys(self)


class CandidateDocumentInline(TabularInline):
    formset = CandidateDocumentInlineFormset
    model = CandidateDocument


class CandidateMediaInline(TabularInline):
    formset = CandidateMediaInlineFormset
    model = CandidateMedia


class CandidateAttributeValueAdminInline(TabularInline):
    model = CandidateAttributeValue
    extra = 5
    raw_id_fields = ['value']


class CandidateSuperAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'composer', 'username_str', 'email_str', 'competition', 'application_date',
        'steps_str', 'groups_str', 'is_valid', 'notes',
    )
    list_display_links = ['id','composer',]
    list_filter = ['competition','is_valid','steps','groups']
    search_fields = ['composer__user__last_name','composer__user__first_name','groups__name','steps__name','notes']
    actions = [export_to_excel]
    inlines = [
        CandidateDocumentInline, CandidateMediaInline, CandidateBiographicElementInline,
        CandidateAttributeValueAdminInline, ApplicationElementInline
    ]
    raw_id_fields = ('composer', 'competition', 'steps', 'groups', )

    def get_formsets(self, request, obj=None):
        # Replace 'CandidateDocumentInline' and 'CandidateMediaInline' by a competition specific forms
        self.inlines[0].form = get_candidate_document_inline_form(obj)
        self.inlines[1].form = get_candidate_media_inline_form(obj)
        return super(CandidateSuperAdmin, self).get_formsets(request,obj)

    def username_str(self, obj):
        return obj.composer.user.username
    username_str.short_description = 'username'

    def email_str(self, obj):
        return obj.composer.user.email
    email_str.short_description = 'email'

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])
    groups_str.short_description = 'groups'

    def steps_str(self, obj):
        return ", ".join([step.name for step in obj.steps.all()])
    steps_str.short_description = 'steps'

class CandidateAdmin(CompetitionModelAdmin):
    is_super_admin = False

    def url_for_pk(self, pk, app_label, model_name):
        return reverse('competition-admin:edit_candidate', args=[pk])

    list_display = (
        'id_in_competition_str', 'candidate_str', 'email_str', 'application_date', 'steps_str',
        'groups_str', 'is_valid', 'notes', 'filters_str', 'recommendations_str'
    )
    list_display_links = ['id_in_competition_str', 'candidate_str', ]
    list_filter = [
        'is_valid', CompetitionStepFilter, CandidateGroupFilter,
        CandidateFilterListFilter, CandidateRecommendationListFilter
    ]
    search_fields = ['composer__user__last_name', 'composer__user__first_name', 'groups__name', 'steps__name', 'notes']
    inlines = [CandidateBiographicElementInline, CandidateDocumentInline, CandidateMediaInline]
    list_per_page = 250
    export_fields = ('lastname_str', 'firstname_str', 'email_str', 'id_in_competition_str', 'application_date', 'steps_str',
        'groups_str', 'is_valid', 'notes', 'filters_str', 'recommendations_str')
    actions = [
        export_to_excel, mark_candidates_as_valid, mark_candidates_as_invalid, manage_groups_for_candidates,
        manage_candidates_to_competition_step, remove_candidate_from_competition
    ]

    def id_in_competition_str(self, obj):
          return obj.id_in_competition
    id_in_competition_str.short_description = 'id'
    id_in_competition_str.admin_order_field = 'id_in_competition'

    def candidate_str(self, obj):
          return obj.composer.name
    candidate_str.short_description = 'Candidate'

    def firstname_str(self, obj):
        return obj.composer.user.first_name
    firstname_str.short_description = "First name"

    def lastname_str(self, obj):
        return obj.composer.user.last_name
    lastname_str.short_description = "Last name"

    def email_str(self, obj):
        return obj.composer.user.email
    email_str.short_description = 'email'

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])
    groups_str.short_description = 'groups'

    def steps_str(self, obj):
        return ", ".join([step.name for step in obj.steps.all()])
    steps_str.short_description = 'steps'

    def filters_str(self, obj):
        return obj.filters_str()
    filters_str.short_description = "Choices"

    def recommendations_str(self, obj):
        obj.recommendations_str()
    recommendations_str.short_description = "Recommendations"

    def edit_candidate(self, request, id):
        params = {}
        candidate = Candidate.objects.filter(id=id)[0]
        params["candidate"] = candidate
        active_competition = self.admin_site.get_active_competition(request)
        is_competition_admin = self.admin_site.is_competition_admin(request)
        is_jury_member = self.admin_site.is_jury_member(request)

        if is_jury_member:
            jury_member = JuryMember.objects.get(user=request.user)
            evaluations = []
            for step in active_competition.steps():
                evals = Evaluation.objects.filter(
                    candidate=candidate, competition_step=step, jury_member=jury_member
                )
                for eval_ in evals:
                    evaluations.append(eval_)
            params["jury_member"] = jury_member
            params["evaluations"] = evaluations

        return render(request, 'admin/candidate_jury_view.html', params)

    def get_queryset(self, request):
        if in_competition_admin(request):
            # Get candidates for active competition
            queryset = Candidate.objects.filter(competition=self.admin_site.get_active_competition(request))
            return wrap_queryset(self, queryset)
        else:
            return super(CandidateAdmin, self).get_queryset(request)

    def get_export_queryset(self, request):
        return self.get_queryset(request).order_by('composer__user__last_name')

    def save_model(self, request, obj, form, change):
        if in_competition_admin(request):
            obj.competition = self.admin_site.get_active_competition(request)
        # Call base class
        super(CandidateAdmin, self).save_model(request,obj,form,change)


class AcanthesCandidateAdmin(CandidateAdmin):
    list_display = ('id_in_competition_str','composer','email_str','application_date','workshop1','workshop2','workshop3','steps_str','groups_str','is_valid')
    list_filter = ['is_valid',CompetitionStepFilter,CandidateGroupFilter,get_competition_attribute_filter_class("acanthes")]

    def get_workshop_name_helper(self,obj,key):
        if CandidateAttributeValue.objects.filter(candidate=obj,attribute=key).exists():
            return CandidateAttributeValue.objects.get(candidate=obj,attribute=key).value.acronym

    def workshop1(self, obj):
        return self.get_workshop_name_helper(obj,"workshop1")
    workshop1.short_description = 'Workshop #1'

    def workshop2(self, obj):
        return self.get_workshop_name_helper(obj,"workshop2")
    workshop2.short_description = "Workshop #2"

    def workshop3(self, obj):
        return self.get_workshop_name_helper(obj,"workshop3")
    workshop3.short_description = "Workshop #3"


class ResidencyCandidateAdmin(CandidateAdmin):
    list_display = ('id_in_competition_str','composer','email_str','application_date','topic_areas','steps_str','groups_str','is_valid')
    list_filter = ['is_valid',CompetitionStepFilter,CandidateGroupFilter,get_competition_attribute_filter_class("residency")]

    def topic_areas(self, obj):
        return ", ".join([cav.value.acronym for cav in CandidateAttributeValue.objects.filter(candidate=obj)])
    topic_areas.short_description = 'Topic areas'


# Candidate specific admin classes
CANDIDATE_ADMIN_CLASSES = {}
CANDIDATE_ADMIN_CLASSES["acanthes-composers"] = AcanthesCandidateAdmin
CANDIDATE_ADMIN_CLASSES["acanthes-performers"] = AcanthesCandidateAdmin
CANDIDATE_ADMIN_CLASSES["residency"] = ResidencyCandidateAdmin


class JuryMemberSuperAdmin(CompetitionModelAdmin):
    """
    Super-admin class for jury members

    Nota : admin class (for competition admin) is dynamic, see : get_jury_member_admin_class
    """
    is_super_admin = True
    search_fields = ['user__last_name']
    list_display = ('name_str', 'email_str', 'competitions_str', 'steps_str', 'groups_str', )
    inlines = [JuryMemberCompetitionInline, ]
    raw_id_fields = ('user', )

    def name_str(self, obj):
        return "%s, %s" % (obj.user.last_name, obj.user.first_name)
    name_str.short_description = 'name'
    name_str.admin_order_field = "user__last_name"

    def email_str(self, obj):
        return obj.user.email
    email_str.short_description = 'email'
    email_str.admin_order_field = "user__email"

    def competitions_str(self, obj):
        return ", ".join([c.title for c in obj.competitions.all()])
    competitions_str.short_description = 'competitions'

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])
    groups_str.short_description = 'groups'

    def steps_str(self, obj):
        return ", ".join([step.name for step in obj.steps.all()])
    steps_str.short_description = 'steps'


class JuryMemberAdminForm(forms.ModelForm):
    username = forms.CharField(label=__('Username'), disabled=True, widget=forms.HiddenInput())
    last_name = forms.CharField(label=__('Last name'))
    first_name = forms.CharField(label=__('First name'))
    email = forms.EmailField(label=__('Email'), disabled=True)
    send_reset_password = forms.BooleanField(
        required=False,
        help_text=__("Instructions for resetting the password will be e-mailed to the jury member."),
        label=__('Reset Password')
    )

    def __init__(self, *args, **kwargs):
        super(JuryMemberAdminForm, self).__init__(*args, **kwargs)

        # fix field width
        for field_name in self.fields:
            field = self.fields[field_name]
            if isinstance(field, (forms.CharField, forms.EmailField)):
                css_class = "vTextField"
                widget_class = field.widget.attrs.get('class', '')
                if css_class not in widget_class:
                    field.widget.attrs['class'] = widget_class + " " + css_class

        self.fields['first_name'].initial = self.instance.user.first_name
        self.fields['last_name'].initial = self.instance.user.last_name
        self.fields['username'].initial = self.instance.user.username
        self.fields['email'].initial = self.instance.user.email

    class Meta:
        model = JuryMember
        fields = ['username', 'email', 'last_name', 'first_name', 'send_reset_password', ]

    def send_password_reset_email(self, user):
        subject_template_name = 'registration/password_reset_subject.txt'
        email_template_name = 'registration/password_reset_email.html'
        current_site = Site.objects.get_current()
        site_name = current_site.name
        domain = current_site.domain
        token_generator = default_token_generator
        use_https = not settings.DEBUG

        context = {
            'email': user.email,
            'domain': domain,
            'site_name': site_name,
            'uid': urlsafe_base64_encode(force_bytes(user.pk)),
            'user': user,
            'token': token_generator.make_token(user),
            'protocol': 'https' if use_https else 'http',
        }

        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        body = loader.render_to_string(email_template_name, context)
        email_message = EmailMultiAlternatives(subject, body, None, [user.email])
        email_message.send()

        request = get_request()
        if request:
            success(request, _('An email has been sent to {0}').format(user.email))

    def save(self, *args, **kwargs):
        ret = super(JuryMemberAdminForm, self).save(*args, **kwargs)
        send_reset_password = self.cleaned_data.get('send_reset_password')
        user = self.instance.user
        user.last_name = self.cleaned_data['last_name']
        user.first_name = self.cleaned_data['first_name']
        user.save()
        if send_reset_password:
            self.send_password_reset_email(user)
        return ret


class JuryMemberAdmin(JuryMemberSuperAdmin):
    is_super_admin = False
    search_fields = ['user__last_name', 'user__email']
    list_display = ('name_str', 'email_str', 'competitions_str', 'steps_str', 'groups_str',)
    form = JuryMemberAdminForm
    readonly_fields = []
    inlines = []

    actions = [
        manage_groups_for_jury_members, manage_jury_members_to_competition_step,
        remove_jury_members_from_competition
    ]
    list_filter = [
        JuryMemberCompetitionFilter, CompetitionStepFilter, JuryMemberGroupFilter,
    ]

    def get_manager_competitions(self, request, queryset=None):
        if not queryset:
            queryset = Competition.objects.all()
        return queryset.filter(
            managed_by__user=request.user
        )

    def get_queryset(self, request):
        queryset = super(JuryMemberAdmin, self).get_queryset(request)
        if request.user.is_superuser:
            return queryset
        # active_competition = self.admin_site.get_active_competition(request)
        manager_competitions = self.get_manager_competitions(request)
        queryset = queryset.filter(competitions__in=manager_competitions).distinct()
        return queryset

    def competitions_str(self, obj):
        competitions = obj.competitions.all()
        request = get_request()
        if request and not request.user.is_superuser:
            competitions = self.get_manager_competitions(request, competitions)
        return ", ".join([c.title for c in competitions])

    competitions_str.short_description = 'competitions'

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])

    groups_str.short_description = 'groups'

    def steps_str(self, obj):
        steps = obj.steps.all()
        request = get_request()
        if request and not request.user.is_superuser:
            manager_competitions = self.get_manager_competitions(request)
            steps = steps.filter(competition__in=manager_competitions)
        return ", ".join([step.name for step in steps])

    steps_str.short_description = 'steps'


class CandidateGroupAdmin(admin.ModelAdmin):
    # fields = ('name', "competition", )
    list_display = ("name", "competition", )
    list_filter = ("competition", )

    # I think that this is not the right way for super-admin site
    # def save_model(self, request, obj, form, change):
    #     obj.competition = self.admin_site.get_active_competition(request)
    #     obj.save()


class CompetitionStepFormSet(BaseInlineFormSet):
    def clean(self):
        for form in self.forms:
            if hasattr(form, "cleaned_data"):
                cleaned_data = form.cleaned_data
                if cleaned_data:
                    try:
                        delete = cleaned_data["DELETE"]
                        if delete:
                            raise forms.ValidationError(_("You are not allowed to delete a competition step"))
                    except:
                        pass


class CompetitionStepInlineForm(forms.ModelForm):
    name = forms.CharField(label="Name", max_length=50, widget=forms.TextInput(attrs={'readonly': 'readonly'}))

    class Meta:
        exclude = (
            'url', 'order_index', 'evaluation_form_class', 'candidate_attribute_jury_filter', 'notification_email',
            'notification_email'
        )


class CompetitionStepInline(TabularInline):
    formset = CompetitionStepFormSet
    form = CompetitionStepInlineForm
    model = CompetitionStep
    readonly_fields = ('notify_association_by_email', )
    extra = 2


class CompetitionStepAdminInline(TabularInline):
    model = CompetitionStep
    extra = 2


class CompetitionAttributeAdminInline(TabularInline):
    model = CompetitionAttribute
    extra = 2


class CompetitionAttributeValueAdminInline(TabularInline):
    model = CompetitionAttributeValue
    extra = 10


class CompetitionAttributeAdmin(ForeignKeyAutocompleteAdmin):
    list_display = ('competition', 'key', )
    list_filters = ('competition', )
    search_fields = ('competition__title', 'key')
    ordering = ['competition__title']
    inlines = [CompetitionAttributeValueAdminInline, ]
    related_search_fields = {
        'competition': ('title', ),
    }
    #form = CompetitionAttributeForm


class EvaluationFieldInline(TabularInline):
    model = EvaluationField
    extra = 2


class CompetitionStepSuperAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'competition', 'is_open')
    list_display_links = ('id', 'name')
    inlines = [EvaluationFieldInline, ]
    list_filter = ('is_open', 'competition', )
    search_fields = ('name', )


class CompetitionAdminForm(forms.ModelForm):
    class Meta:
        model = Competition
        fields = [
            'title', 'url', 'subtitle', 'email', 'presentation', 'image', 'small_image', 'application_email_header',
            'organized_by', 'partners', 'managed_by', 'category', 'jury_guidelines', 'status',
            'publication_date', 'opening_date', 'closing_date', 'result_date', 'unpublish_date',
            "use_dynamic_form", "dynamic_application_form", "application_form_class", 'profile_mandatory_fields',
            'is_external_call', 'external_call_url', 'closed_message', 'entry_fee', 'restrictions',
        ]

    def __init__(self, *args, **kwargs):
        super(CompetitionAdminForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['unpublish_date'].required = True

    def clean_url(self):
        data = self.cleaned_data["url"]
        if slugify(data) != data:
            raise forms.ValidationError(
                "url must be a valid slug (it can not contain spaces, capital letters...). "
                "Try : '%s' instead of '%s' " % (slugify(data),data)
            )
        return data

    def clean(self):
        publication_date = self.cleaned_data.get("publication_date")
        opening_date = self.cleaned_data.get("opening_date")
        closing_date = self.cleaned_data.get("closing_date")
        result_date = self.cleaned_data.get("result_date")
        unpublish_date = self.cleaned_data.get("unpublish_date")
        if publication_date is None:
            raise forms.ValidationError("Publication date is not valid")
        if opening_date is None:
            raise forms.ValidationError("Opening date is not valid")
        if closing_date is None:
            raise forms.ValidationError("Closing date is not valid")
        if result_date is None:
            raise forms.ValidationError("Result date is not valid")
        if unpublish_date is None:
            raise forms.ValidationError("Unpublish date is not valid")

        if publication_date > opening_date:
            raise forms.ValidationError("Publication date must be anterior to opening date")
        if opening_date > closing_date:
            raise forms.ValidationError("Opening date must be anterior to closing date")
        if closing_date > result_date:
            raise forms.ValidationError("Closing date must be anterior to result date")
        if result_date > unpublish_date:
            raise forms.ValidationError("Result date must be anterior to unpublish date")

        dynamic_application_form = self.cleaned_data.get("dynamic_application_form")
        dynamic_application_form = dynamic_application_form or getattr(self.instance, 'dynamic_application_form', None)
        managed_by = self.cleaned_data.get("managed_by")
        if dynamic_application_form and managed_by.count() == 0:
            raise forms.ValidationError("At least one manager is required for a competition")

        return self.cleaned_data


class CompetitionSuperAdminForm(CompetitionAdminForm):

    class Meta:
        model = Competition
        fields = [
            'title', 'url', 'subtitle', 'email', 'presentation', 'image', 'small_image', 'application_email_header',
            'organized_by', 'partners', 'managed_by', 'category', 'jury_guidelines', 'status',
            'publication_date', 'opening_date', 'closing_date', 'result_date', 'unpublish_date',
            "use_dynamic_form", "dynamic_application_form", "application_form_class", 'profile_mandatory_fields',
            'allow_access_to_jury_history', 'display_candidate_name', 'show_on_homepage', 'order_index',
            'is_external_call', 'external_call_url', 'publication_notified', 'entry_fee', 'restrictions',
        ]

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['organized_by'].queryset = self.fields['organized_by'].queryset.order_by('name')
        self.fields['partners'].queryset = self.fields['partners'].queryset.order_by('name')
        self.fields['managed_by'].queryset = self.fields['managed_by'].queryset.order_by('user__last_name')

    def clean(self):
        super(CompetitionSuperAdminForm, self).clean()

        use_dynamic_form = self.cleaned_data["use_dynamic_form"]
        dynamic_application_form = self.cleaned_data["dynamic_application_form"]
        application_form_class = self.cleaned_data["application_form_class"]
        url = self.cleaned_data.get('url', '')

        if url:
            siblings = Competition.objects.filter(url=url)
            if self.instance and self.instance.id:
                siblings = siblings.exclude(id=self.instance.id)
            if siblings.count() > 0:
                raise forms.ValidationError(
                    _("There is already 1 competition with the URL {0}".format(url))
                )

        if use_dynamic_form and not dynamic_application_form:
            raise forms.ValidationError(
                _("A dynamic application form must be specified when `use_dynamic_form` is checked")
            )
        if not use_dynamic_form and dynamic_application_form:
            raise forms.ValidationError(
                _("You must check `use_dynamic_form` if you specify a dynamic application form")
            )
        if use_dynamic_form and application_form_class:
            raise forms.ValidationError(
                _("`application_form_class` can not be specified when `use_dynamic_form` is checked")
            )

        if not application_form_class and not dynamic_application_form:
            raise forms.ValidationError(
                _("You must specify either `application_form_class` or `dynamic_application_form`")
            )

        return self.cleaned_data


def clone_competition_action(modeladmin, request, queryset):
    count = queryset.count()
    clone = None
    for competition in queryset:
        clone = clone_competition(competition)
        if clone:
            success(request, _('Competition {0} has been cloned into {1}').format(competition.title, clone.title))
        else:
            error(request, _('Unable to close Competition {0}').format(competition.title))
    if count == 1 and clone:
        return redirect("/super-admin/ulysses_competitions/competition/{0}/".format(clone.id))
    else:
        return redirect("/super-admin/ulysses_competitions/competition/")


clone_competition_action.short_description = __("Clone the selected competitions")


class ManagersFilter(admin.SimpleListFilter):
    parameter_name = 'managers'
    title = "Managers"

    def lookups(self, request, model_admin):
        queryset = CompetitionManager.objects.all().order_by('user__last_name', 'user__first_name', )
        return [(manager.id, manager.full_name()) for manager in queryset]

    def queryset(self, request, queryset):
        manager_id = self.value()
        if manager_id:
            return queryset.filter(managed_by=manager_id)
        else:
            return queryset


class CompetitionSuperAdmin(admin.ModelAdmin):
    list_display = (
        'id', 'title', 'url', 'status', 'use_dynamic_form', 'dynamic_application_form', 'application_form_class',
        'order_index', 'allow_access_to_jury_history', 'show_on_homepage', 'is_external_call', 'category',
    )
    list_display_links = ('id', 'title')
    readonly_fields = ('archived', "status",)
    list_filter = ('show_on_homepage', 'status', ManagersFilter, 'allow_access_to_jury_history', 'category', )
    list_editable = ('show_on_homepage', 'order_index', )
    search_fields = ["title"]
    form = CompetitionSuperAdminForm
    inlines = (CompetitionStepAdminInline, CompetitionAttributeAdminInline)
    filter_horizontal = ('profile_mandatory_fields', )
    ordering = ('title', )
    actions = [clone_competition_action]


class CompetitionAdmin(CompetitionModelAdmin):
    form = CompetitionAdminForm
    search_fields = ['title']
    list_display = ('title', 'subtitle', 'organized_by', 'opening_date', 'closing_date', 'status', )
    list_filter = ['status', ]
    filter_vertical = ['partners', 'managed_by']
    readonly_fields = ["url", "status"]
    inlines = (CompetitionStepInline, )

    fieldsets = (
        (__('General informations'), {
            'classes': ('collapse',),
            'fields': (
                'title', 'url', 'subtitle', 'email', 'presentation', 'image', 'small_image', 'application_email_header',
                'category',
            )
        }),
        (__('Organized by'), {
            'classes': ('collapse',),
            'fields': ('organized_by', 'partners')
        }),
        (__('Managed by'), {
            'classes': ('collapse',),
            'fields': ('managed_by',)
        }),
        (__('Jury guidelines'), {
            'classes': ('collapse',),
            'fields': ('jury_guidelines',)
        }),
        (__('Dates'), {
            'classes': ('collapse',),
            'fields': (
                'publication_date', 'opening_date', 'closing_date', 'result_date', 'unpublish_date', 'closed_message',
            )
        }),
        (__('Fee and Restrictions'), {
            'classes': ('collapse',),
            'fields': (
                'entry_fee', 'restrictions',
            )
        }),
        (__('Status'), {
            'classes': ('collapse',),
            'fields': ('status',)
        }),
    )

    class Media:
        js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_pages.js' % settings.STATIC_URL
        )


class CompetitionInfoAdmin(CompetitionAdmin):
    is_super_admin = False

    def changelist_view(self, request, **kwargs):
        return redirect(reverse('competition-admin:show_informations'))


class CompetitionStepAdmin(CompetitionModelAdmin):
    list_display = ('name', 'competition', 'is_open', )
    list_filter = ('competition', )


class CompetitionNewsAdmin(CompetitionModelAdmin):
    is_super_admin = False
    search_fields = ('title', 'text')
    list_display = ('date', 'title', 'is_published')
    list_filter = ('is_published', )
    fieldsets = (
        ('Competition news', {
            'classes': ('wide',),
            'fields': ('title', 'date', 'text', 'image', 'is_published')
        }),
    )

    def get_queryset(self, request):
        if in_competition_admin(request):
            # Get news for active competition
            queryset = CompetitionNews.objects.filter(competition=self.admin_site.get_active_competition(request))
            return wrap_queryset(self, queryset)
        else:
            return super(CompetitionNewsAdmin, self).get_queryset(request)

    def save_model(self, request, obj, form, change):
        if not self.is_super_admin:
            obj.competition = self.admin_site.get_active_competition(request)
            obj.save()

    class Media:
        js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_pages.js' % settings.STATIC_URL
        )


class CompetitionNewsSuperAdmin(admin.ModelAdmin):
    list_display = ('date', 'title', 'competition', 'is_published')
    list_filter = ('is_published', 'competition', )
    fieldsets = (
        ('Competition news', {
            'classes': ('wide',),
            'fields': ('competition', 'title', 'date', 'text', 'image', 'is_published')
        }),
    )
    class Media:
        js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_pages.js' % settings.STATIC_URL
        )


class JuryMemberGroupAdmin(CompetitionModelAdmin):
    fields = ('name', )
    list_display = ("name", )

    def save_model(self, request, obj, form, change):
        obj.competition = self.admin_site.get_active_competition(request)
        obj.save()

    def get_queryset(self, request):
        if in_competition_admin(request):
            # Get jury member groups for active competition
            queryset = JuryMemberGroup.objects.filter(competition=self.admin_site.get_active_competition(request))
            return wrap_queryset(self, queryset)
        else:
            return super(JuryMemberGroupAdmin, self).get_queryset(request)


class CompetitionManagerAdmin(CompetitionModelAdmin):
    raw_id_fields = ('user', )
    search_fields = ('user__last_name', 'user__username')


def get_candidate_evaluation_admin(criteria):
    """
    Gets a dynamic admin class for candidate evaluations. Note : criteria must be a list of tuples ((key,label),)
    """
    class _CandidateEvaluationAdmin(CompetitionModelAdmin):
        change_list_template = "admin/candidate_results.html"
        list_display = ('jury_member',) + tuple([c[0] for c in criteria])

        @staticmethod
        def get_evaluation(obj,key):
            if EvaluationNote.objects.filter(evaluation=obj,key=key).exists():
                return EvaluationNote.objects.get(evaluation=obj,key=key)
            else:
                return _("N/A")

        @staticmethod
        def get_criteria_display_method(key,label):
            """
            Gets a dynamic method displaying an evaluation criteria value
            """
            def _criteria(self,obj):
                return _CandidateEvaluationAdmin.get_evaluation(obj,key)
            _criteria.short_description = label
            _criteria.admin_order_field = key
            return _criteria

        def __init__(self,*args,**kwargs):
            super(_CandidateEvaluationAdmin, self).__init__(*args, **kwargs)
            # Define criteria display methods dynamically
            for c in criteria:
                setattr(self.__class__,c[0],_CandidateEvaluationAdmin.get_criteria_display_method(c[0],c[1]))

        def get_queryset(self, request):
            select_dict = {}
            for c in criteria:
                select_dict[c[0]]= """
                    select value from ulysses_competitions_evaluationnote where `key` ='{0}' 
                    and evaluation_id = competitions_evaluation.id""".format(c[0])
            qs = super(_CandidateEvaluationAdmin, self).get_queryset(request).extra(select = select_dict)
            return qs

    return _CandidateEvaluationAdmin


class AcanthesJuryResultsAdminBase(CompetitionModelAdmin):
    list_display = ('name','groups_str','note','average','recommandation_1','recommandation_2')
    list_filter = (CandidateGroupFilter,)
    change_list_template = "admin/follow_results_multi_notes.html"
    actions = [manage_groups_for_candidates, manage_steps_for_candidates, export_candidates_to_excel]

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])
    groups_str.short_description = 'groups'

    def gender_str(self,obj):
        profile = get_profile(obj.composer.user)
        gender = getattr(profile, 'gender', None)
        if gender:
            return gender.name
        else:
            return ""
    gender_str.short_description = 'pronoun'

    def birth_date_str(self, obj):
        profile = get_profile(obj.composer.user)
        birth_date = getattr(profile, 'birth_date', None)
        if birth_date:
            return str(birth_date)
        return ""
    birth_date_str.short_description = 'birth date'

    def citizenship_str(self, obj):
        profile = get_profile(obj.composer.user)
        citizenship = getattr(profile, 'citizenship', None)
        if citizenship:
            return citizenship.name
        else:
            return ""
    citizenship_str.short_description = 'citizenship'

    def get_competition_url(self):
        raise RuntimeError("You should override this method in derived classes")

    def name(self, obj):
        return "%s %s" % (obj.composer.user.first_name,obj.composer.user.last_name)
    name.short_description = 'Candidate'
    name.admin_order_field = "composer"

    def average(self, obj):
        sql = """
             select AVG(value)
             from ulysses_competitions_evaluationnote
             inner join ulysses_competitions_evaluation
             on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
             where `key` ='{0}' and candidate_id = {1}
        """.format("note", obj.id)
        cursor = connection.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()
        return row[0]
    average.short_description = "average"
    average.admin_order_field = "average"

    def note(self, obj):
        acanthes_jury = CompetitionStep.objects.get(competition__url=self.get_competition_url(),url="jury")
        notes = EvaluationNote.objects.filter(evaluation__candidate=obj,evaluation__competition_step=acanthes_jury,key="note")
        d = {}
        ef = EvaluationField.objects.get(competition_step=acanthes_jury,key="note")
        choices = ef.get_choices()
        for n in notes:
            key = None
            for choice_key,choice_value in choices:
                if str(n)==str(choice_key):
                    key = choice_value
            if not key:
                raise RuntimeError("Key '%s' not found in choices '%s'" % (n,choices))
            if key not in d:
                d[key] = 1
            else:
                d[key] = d[key] + 1
        result = ", ".join(["%s (%s/%s)" % (key,value,len(notes)) for (key,value) in list(d.items())])
        return result

    def get_queryset(self, request):
        select_dict = {}
        select_dict["average"] = """
            select AVG(value)
            from ulysses_competitions_evaluationnote
            inner join ulysses_competitions_evaluation
            on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
            where `key` ='{0}' and candidate_id = ulysses_competitions_candidate.id
        """.format("note")
        qs = super(AcanthesJuryResultsAdminBase, self).get_queryset(request).extra(select = select_dict)
        return qs

    def recommandation_helper(self, obj,key):
        acanthes_jury = CompetitionStep.objects.get(competition__url=self.get_competition_url(),url="jury")
        workshops = EvaluationNote.objects.filter(evaluation__candidate=obj,evaluation__competition_step=acanthes_jury,key=key)
        d = {}
        for w in workshops:
            acr = CompetitionAttributeValue.objects.get(attribute__competition__url=self.get_competition_url(),
                                                        attribute__key="workshops",key=w.value).acronym
            if acr not in d:
                d[acr] = 1
            else:
                d[acr] = d[acr] + 1
        result = ", ".join(["%s (%s/%s)" % (key,value,len(workshops)) for (key,value) in list(d.items())])
        return result

    def recommandation_1(self, obj):
        return self.recommandation_helper(obj,"reco_1")

    def recommandation_2(self, obj):
        return self.recommandation_helper(obj,"reco_2")


class AcanthesComposersJuryResultsAdmin(AcanthesJuryResultsAdminBase):

    def get_competition_url(self):
        return "acanthes-composers"


class AcanthesPerformersJuryResultsAdmin(AcanthesJuryResultsAdminBase):

    def get_competition_url(self):
        return "acanthes-performers"


class AcanthesFinalChoiceResultsAdminBase(CompetitionModelAdmin):
    list_display = ('name','groups_str','decision','average','workshop')
    list_filter = (CandidateGroupFilter,)
    change_list_template = "admin/follow_results_multi_notes.html"
    actions = [manage_groups_for_candidates, manage_steps_for_candidates,export_candidates_to_excel]

    def groups_str(self, obj):
        return ", ".join([group.name for group in obj.groups.all()])
    groups_str.short_description = 'groups'

    def gender_str(self,obj):
        profile = get_profile(obj.composer.user)
        gender = getattr(profile, 'gender', None)
        if gender:
            return gender.name
        else:
            return ""
    gender_str.short_description = 'pronoun'

    def birth_date_str(self, obj):
        profile = get_profile(obj.composer.user)
        birth_date = getattr(profile, 'birth_date', None)
        if birth_date:
            return str(birth_date)
        return ""
    birth_date_str.short_description = 'birth date'

    def citizenship_str(self, obj):
        profile = get_profile(obj.composer.user)
        citizenship = getattr(profile, 'citizenship', None)
        if citizenship:
            return citizenship.name
        else:
            return ""
    citizenship_str.short_description = 'citizenship'

    def get_competition_url(self):
        raise RuntimeError("You should override this method in derived classes")

    def name(self, obj):
        return "%s %s" % (obj.composer.user.first_name,obj.composer.user.last_name)
    name.short_description = 'Candidate'
    name.admin_order_field = "composer"

    def decision(self, obj):
        acanthes_final_choice = CompetitionStep.objects.get(competition__url=self.get_competition_url(),url="final-choice")
        notes = EvaluationNote.objects.filter(evaluation__candidate=obj,evaluation__competition_step=acanthes_final_choice,key="decision")
        d = {}
        ef = EvaluationField.objects.get(competition_step=acanthes_final_choice,key="decision")
        choices = ef.get_choices()
        for n in notes:
            key = None
            for choice_key,choice_value in choices:
                if str(n)==str(choice_key):
                    key = choice_value
            if not key:
                raise RuntimeError("Key '%s' not found in choices '%s'" % (n,choices))
            if key not in d:
                d[key] = 1
            else:
                d[key] = d[key] + 1
        result = ", ".join(["%s (%s/%s)" % (key,value,len(notes)) for (key,value) in list(d.items())])
        return result

    def average(self, obj):
        sql = """
             select AVG(value)
             from ulysses_competitions_evaluationnote
             inner join ulysses_competitions_evaluation
             on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
             where `key` ='{0}' and candidate_id = {1}
        """.format("decision",obj.id)
        cursor = connection.cursor()
        cursor.execute(sql)
        row = cursor.fetchone()
        return row[0]
    average.short_description = "average"
    average.admin_order_field = "average"

    def get_queryset(self, request):
        select_dict = {}
        select_dict["average"] = """
             select AVG(value)
             from ulysses_competitions_evaluationnote
             inner join ulysses_competitions_evaluation
             on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
             where `key` ='{0}' and candidate_id = ulysses_competitions_candidate.id
        """.format("decision")
        qs = super(AcanthesFinalChoiceResultsAdminBase, self).get_queryset(request).extra(select = select_dict)
        return qs

    def workshop(self, obj):
        acanthes_final_choice = CompetitionStep.objects.get(competition__url=self.get_competition_url(),url="final-choice")
        workshops = EvaluationNote.objects.filter(evaluation__candidate=obj,evaluation__competition_step=acanthes_final_choice,key="workshop")
        d = {}
        for w in workshops:
            acr = CompetitionAttributeValue.objects.get(
                attribute__competition__url=self.get_competition_url(),
                attribute__key="workshops",key=w.value
            ).acronym
            if acr not in d:
                d[acr] = 1
            else:
                d[acr] = d[acr] + 1
        result = ", ".join(["%s (%s/%s)" % (key,value,len(workshops)) for (key,value) in list(d.items())])
        return result


class AcanthesComposersFinalChoiceResultsAdmin(AcanthesFinalChoiceResultsAdminBase):

    def get_competition_url(self):
        return "acanthes-composers"


class AcanthesPerformersFinalChoiceResultsAdmin(AcanthesFinalChoiceResultsAdminBase):

    def get_competition_url(self):
        return "acanthes-performers"


def get_candidate_step_results_admin(competition_step, display_average):
    """
    Gets a dynamic class for candidate step results admin. Note : criteria must be a list of tuples ((key,label),)
    """
    # Specific admin classes for Acanthes
    if competition_step.competition.url == "acanthes-composers" and competition_step.url == "jury":
        return AcanthesComposersJuryResultsAdmin
    if competition_step.competition.url == "acanthes-performers" and competition_step.url == "jury":
        return AcanthesPerformersJuryResultsAdmin
    if competition_step.competition.url == "acanthes-composers" and competition_step.url == "final-choice":
        return AcanthesComposersFinalChoiceResultsAdmin
    if competition_step.competition.url == "acanthes-performers" and competition_step.url == "final-choice":
        return AcanthesPerformersFinalChoiceResultsAdmin
    # In other case : use default class
    criteria = [(f.key, f.label) for f in competition_step.get_evaluation_fields(is_note=True) if f.display_in_result_list]
    class _CandidateStepResultsAdmin(CompetitionModelAdmin):
        is_super_admin = False

        list_display = (
            'name', 'status', 'groups_str', 'filters_str', 'recommendations_str',
        ) + tuple([c[0] for c in criteria])
        export_fields = ('email', 'lastname', 'firstname', 'groups_str', 'birth_date_str', 'age_str', 'gender_str', 'citizenship_str', 'city_str', 'country_str', 'filters_str', 'recommendations_str') + tuple([c[0] for c in criteria]) + ('average_value',)

        list_filter = (CandidateGroupFilter, CandidateFilterListFilter, CandidateRecommendationListFilter)
        if display_average:
            list_display += ('average_value', )
        change_list_template = "admin/follow_results_multi_notes.html"
        actions = [manage_groups_for_candidates, manage_steps_for_candidates, export_candidates_to_excel, export_results_to_excel]

        def email(self, obj):
            return obj.composer.user.email
        email.short_description = 'email'
    
        def lastname(self, obj):
            return obj.composer.user.last_name
        lastname.short_description = 'last name'

        def firstname(self, obj):
            return obj.composer.user.first_name
        firstname.short_description = 'first name'

        def groups_str(self, obj):
            return ", ".join([group.name for group in obj.groups.all()])
        groups_str.short_description = 'groups'

        def filters_str(self, obj):
            return obj.filters_str()
        filters_str.short_description = "Choices"
        
        def url_for_pk(self, pk, app_label, model_name):
            return reverse('competition-admin:step_candidate_results', args=[competition_step.url, pk])

        def gender_str(self, obj):
            profile = get_profile(obj.composer.user)
            gender = getattr(profile, 'gender', None)
            if gender:
                return gender.name
            else:
                return ""
        gender_str.short_description = 'pronoun'

        def birth_date_str(self, obj):
            profile = get_profile(obj.composer.user)
            birth_date = getattr(profile, 'birth_date', None)
            if birth_date:
                return str(birth_date)
            return ""
        birth_date_str.short_description = 'birth date'

        def age_str(self, obj):
            profile = get_profile(obj.composer.user)
            birth_date = getattr(profile, 'birth_date', None)
            if birth_date:
                return get_age(birth_date)
            return ""
        age_str.short_description = 'age'

        def citizenship_str(self, obj):
            profile = get_profile(obj.composer.user)
            citizenship = getattr(profile, 'citizenship', None)
            if citizenship:
                return citizenship.name
            else:
                return ""
        citizenship_str.short_description = 'citizenship'

        def country_str(self, obj):
            profile = get_profile(obj.composer.user)
            country = getattr(profile, 'country', None)
            if country:
                return country.name
            else:
                return ""
        country_str.short_description = 'country'
        
        def city_str(self, obj):
            profile = get_profile(obj.composer.user)
            return getattr(profile, 'city', None)
        city_str.short_description = 'city'

        def status(self, obj):
            status = [
                evaluation.status
                for evaluation in Evaluation.objects.filter(candidate=obj, competition_step=competition_step)
            ]
            counting = []
            for status_name in set(status):
                counting.append(
                    '<span style="white-space: nowrap;">{0} x {1}</span>'.format(
                        status.count(status_name), status_name
                    )
                )
            if counting:
                return mark_safe('<br />'.join(counting))
            else:
                return 'No evaluations'
        status.short_description = 'Status'

        @staticmethod
        def get_field_average(key, obj, round_value=False):
            sql = """
              select AVG(value)
              from ulysses_competitions_evaluationnote
              inner join ulysses_competitions_evaluation
                on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
                where `key` ='{0}' and candidate_id = {1}
            """.format(key, obj.id)
            cursor = connection.cursor()
            cursor.execute(sql)
            row = cursor.fetchone()
            if round_value:
                return round(row[0], 2) if row[0] is not None else None
            else:
                return row[0]

        @staticmethod
        def get_criteria_display_method(key, label):
            """
            Gets a dynamic method displaying an evaluation criteria value
            """
            def _criteria(self, obj):
                return _CandidateStepResultsAdmin.get_field_average(key, obj, True)
            _criteria.short_description = label
            _criteria.admin_order_field = key
            return _criteria

        def __init__(self,*args,**kwargs):
            super(_CandidateStepResultsAdmin, self).__init__(*args, **kwargs)
            self.competition_step = competition_step
            # Define criteria display methods dynamically
            for c in criteria:
                setattr(self.__class__, c[0], _CandidateStepResultsAdmin.get_criteria_display_method(c[0], c[1]))

        def name(self, obj):
            return "%s %s" % (obj.composer.user.first_name,obj.composer.user.last_name)
        name.short_description = 'Candidate'
        name.admin_order_field = "composer"

        # def weighted_average(self, obj):
        #     """
        #     Get weighted average note for this candidate (note : 'obj' is a candidate)
        #     """
        #     sql = """
        #         select sum(x.criteria_weighted_value) / (select sum(weight)
        #         from ulysses_competitions_evaluationfield
        #         where ulysses_competitions_evaluationfield.competition_step_id = {0} ) as weighted_average
        #          from (
        #             select avg(value * (select weight
        #                 from ulysses_competitions_evaluationfield
        #                 where ulysses_competitions_evaluationfield.competition_step_id={1}
        #                 and ulysses_competitions_evaluationfield.key = ulysses_competitions_evaluationnote.key)
        #             ) as criteria_weighted_value
        #             from ulysses_competitions_evaluationnote
        #             inner join ulysses_competitions_evaluation
        #             on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
        #             where candidate_id = {2}
        #             group by ulysses_competitions_evaluationnote.key) x
        #     """.format(competition_step.id,competition_step.id,obj.id)
        #     cursor = connection.cursor()
        #     cursor.execute(sql)
        #     row = cursor.fetchone()
        #     return round(row[0], 2) if row[0] is not None else None
        # weighted_average.short_description = 'weighted average'

        def average_value(self, obj):
            """
            Get average note for this candidate (note : 'obj' is a candidate)
            """
            fields = [
                f for f in competition_step.get_evaluation_fields(is_note=True) if f.display_in_result_list and f.is_note
            ]
            sum_of_notes = 0
            nb_of_notes = 0
            for field in fields:
                field_average = _CandidateStepResultsAdmin.get_field_average(field.key, obj)
                if field_average:
                    nb_of_notes += 1
                    sum_of_notes += field_average
            if nb_of_notes:
                return round(sum_of_notes / nb_of_notes, 2)
            else:
                return None
        average_value.short_description = 'average'
        average_value.admin_order_field = 'average_value'

        def get_queryset(self, request):
            queryset = competition_step.get_candidates()
            select_dict = {}
            for c in criteria:
                select_dict[c[0]] = """
                    select AVG(value)
                    from ulysses_competitions_evaluationnote
                    inner join ulysses_competitions_evaluation
                    on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
                    where `key` ='{0}'
                    and candidate_id = ulysses_competitions_candidate.id
                """.format(c[0])
                select_dict['average_value'] = """
                    select AVG(value)
                    from ulysses_competitions_evaluationnote
                    inner join ulysses_competitions_evaluation
                    on ulysses_competitions_evaluationnote.evaluation_id = ulysses_competitions_evaluation.id
                    where candidate_id = ulysses_competitions_candidate.id
                """
            queryset = queryset.extra(select=select_dict)
            return queryset

        def get_export_queryset(self, request):
            return self.get_queryset(request).order_by('composer__user__last_name')

    return _CandidateStepResultsAdmin


class EvaluationNoteAdmin(CompetitionModelAdmin):
    list_filter = [
        'evaluation__competition_step__competition', 'evaluation__competition_step', 'key','evaluation__jury_member',
        'evaluation__candidate'
    ]
    list_display = ('competition', 'competition_step', 'candidate', 'jury_member', 'key', 'value')

    def competition(self, obj):
        return obj.evaluation.competition_step.competition.title
    competition.short_description = 'competition'

    def competition_step(self, obj):
        return obj.evaluation.competition_step.name
    competition_step.short_description = 'competition step'

    def candidate(self, obj):
        return "%s %s" % (obj.evaluation.candidate.composer.user.first_name,obj.evaluation.candidate.composer.user.last_name)
    candidate.short_description = 'candidate'

    def jury_member(self, obj):
        return "%s %s" % (obj.evaluation.jury_member.user.first_name,obj.evaluation.jury_member.user.last_name)
    jury_member.short_description = 'jury member'


class EvaluationStatusAdmin(CompetitionModelAdmin):
    pass


class ApplicationDraftAdmin(CompetitionModelAdmin):
    list_display = ('composer', 'email', 'creation_date')
    export_fields = ('lastname', 'firstname', 'email', 'creation_date')
    list_display_links = None
    search_fields = ['composer__user__last_name', ]
    actions = [export_to_excel]

    is_super_admin = False
    def email(self, obj):
        return obj.composer.user.email
    email.short_description = 'email'

    def lastname(self, obj):
        return obj.composer.user.last_name
    lastname.short_description = 'last name'

    def firstname(self, obj):
        return obj.composer.user.first_name
    firstname.short_description = 'first name'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def get_queryset(self, request):
        if in_competition_admin(request):
            # Get candidates for active competition
            queryset = super().get_queryset(request).filter(competition=self.admin_site.get_active_competition(request))
            return wrap_queryset(self, queryset)
        else:
            return super(ApplicationDraftAdmin, self).get_queryset(request)
    
    def get_export_queryset(self, request):
        return self.get_queryset(request).order_by('composer__user__last_name')


class ApplicationDraftSuperAdmin(admin.ModelAdmin):
    list_display = ('competition', 'composer', 'email', 'creation_date')
    list_filter = ['competition', ]
    search_fields = ['composer__user__last_name', ]
    actions = [export_to_excel]

    def email(self, obj):
        return obj.composer.user.email
    email.short_description = 'email'


class EvaluationNoteAdminInline(admin.TabularInline):
    model = EvaluationNote


class EvaluationAdmin(CompetitionModelAdmin):
    list_display = ('competition_step', 'candidate', 'jury_member', 'status')
    list_filter = ['status', 'competition_step', 'jury_member', ]
    inlines = [EvaluationNoteAdminInline, ]
    search_fields = ['candidate__composer__user__last_name', 'jury_member__user__last_name']
    raw_id_fields = ('candidate', 'jury_member', )


class CandidateBiographicElementAdmin(CompetitionModelAdmin):
    list_display = ('candidate', 'key', 'title', 'text')
    search_fields = ['key', 'title', 'candidate__composer__user__last_name']
    raw_id_fields = ('candidate',)


class CandidateDocumentExtraFileInline(TabularInline):
    model = CandidateDocumentExtraFile
    fields = ['file', ]
    extra = 0


class CandidateDocumentAdmin(CompetitionModelAdmin):
    list_display = ('candidate', 'key', 'title', 'file', 'is_admin')
    list_filter = ['is_admin']
    search_fields = ['key', 'title', 'candidate__composer__user__username', 'candidate__composer__user__last_name']
    raw_id_fields = ('candidate',)
    inlines = [CandidateDocumentExtraFileInline, ]


class CandidateMediaAdmin(CompetitionModelAdmin):
    list_display = ('candidate', 'key', 'title', 'audio', 'video', 'score')
    search_fields = ['candidate__composer__user__last_name', 'key', 'title']
    raw_id_fields = ('candidate',)


class DuplicateApplicationElementListFilter(admin.SimpleListFilter):
    parameter_name = 'duplicated'
    title = "Doublon"

    def lookups(self, request, model_admin):
        return [(1, 'Oui')]

    def queryset(self, request, queryset):
        value = self.value()
        if value:
            duplicated_elts = []
            the_map = {}

            for elt in ApplicationElement.objects.all():
                key = "{0}####{1}".format(elt.candidate.id, elt.key)
                if key in the_map:
                    duplicated_elts += [elt.id]
                else:
                    the_map[key] = elt.id

            return queryset.filter(id__in=duplicated_elts)
        else:
            return queryset


class ApplicationElementAdmin(CompetitionModelAdmin):
    list_display = ('candidate', 'key', 'label', 'multiple_values', 'value')
    list_filter = (DuplicateApplicationElementListFilter, 'candidate__competition', 'key', )
    search_fields = ['candidate__composer__user__last_name']
    raw_id_fields = ('candidate', )


class JuryMemberNotificationAdmin(admin.ModelAdmin):
    list_display = ('sender', 'jury_member', 'subject')


class CandidateNotificationAdmin(admin.ModelAdmin):
    list_display = ('sender', 'candidate', 'subject')


class DraftBiographicElementAdmin(admin.ModelAdmin):
    list_display = ('key', 'title', 'draft')


class DraftDocumentExtraFileInline(TabularInline):
    model = DraftDocumentExtraFile
    fields = ['file', ]
    extra = 0


class DraftDocumentAdmin(admin.ModelAdmin):
    list_display = ('key', 'title', 'file', 'draft')
    search_fields = ('key', 'title',)
    inlines = [DraftDocumentExtraFileInline, ]


class DraftMediaAdmin(admin.ModelAdmin):
    list_display = ('key', 'title', 'score', 'audio', 'video', 'draft')


class TemporaryDocumentExtraFileInline(TabularInline):
    model = TemporaryDocumentExtraFile
    fields = ['file', ]
    extra = 0


class TemporaryDocumentAdmin(admin.ModelAdmin):
    list_display = ('key', 'composer', 'competition', 'title', 'file', )
    search_fields = ('key', 'title', )
    raw_id_fields = ('composer', 'competition', )
    inlines = [TemporaryDocumentExtraFileInline, ]


class TemporaryMediaAdmin(admin.ModelAdmin):
    list_display = ('key', 'composer', 'competition', 'title', 'score', 'audio', 'video', )
    search_fields = ('key', 'title',)
    raw_id_fields = ('composer', 'competition', )


class StaticContentAdmin(admin.ModelAdmin):
    list_display = ('key', 'name', 'description', 'text', 'published')
    list_filter = ('published', )
    list_editable = ('name', )

    class Media:
        js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_img_pages.js' % settings.STATIC_URL
        )


class JuryApplicationElementToExcludeAdmin(admin.ModelAdmin):
    list_display = ('key', 'competition')
    list_filter = ('competition',)


class ApplicationFormFieldSetInline(SortableTabularInline):
    model = ApplicationFormFieldSetDefinition
    fields = ('select_inline', 'name', 'legend', )
    readonly_fields = ('select_inline',)


class DynamicApplicationFormSet(BaseInlineFormSet):
    def _construct_form(self, i, *args, **kwargs):
        kwargs['parent'] = self.instance
        return super(DynamicApplicationFormSet, self)._construct_form(i, **kwargs)


class ApplicationFormFieldDefinitionInlineForm(forms.ModelForm):

    class Meta:
        model = ApplicationFormFieldDefinition
        fields = (
            'parent', 'name', 'label', 'kind', 'competition_attribute', 'required',
            'is_visible_by_jury', 'maximum_length', 'validator', 'extra_files', 'help_text',
        )

    def __init__(self, *args, **kwargs):
        parent = kwargs.pop('parent', None)
        super(ApplicationFormFieldDefinitionInlineForm, self).__init__(*args, **kwargs)

        self.fields['parent'].queryset = ApplicationFormFieldSetDefinition.objects.filter(
            parent=parent
        )
        if parent:
            self.fields['competition_attribute'].queryset = CompetitionAttribute.objects.filter(
                competition__in=parent.competition_set.all()
            )


class ApplicationFormFieldDefinitionInline(SortableTabularInline):
    model = ApplicationFormFieldDefinition
    fields = (
        'parent', 'name', 'label', 'kind', 'competition_attribute',
        'required', 'is_visible_by_jury', 'maximum_length', 'validator', 'extra_files', 'help_text',
    )
    form = ApplicationFormFieldDefinitionInlineForm
    formset = DynamicApplicationFormSet


class DynamicApplicationFormAdmin(SortableAdmin):
    list_display = ('name', 'clean_method')
    search_fields = ('name', )
    inlines = [ApplicationFormFieldSetInline, ApplicationFormFieldDefinitionInline]
    change_form_template_extends = 'admin/ulysses_competitions/dynamicapplicationform/change_form.html'

    def duplicate(self, request, id):
        """ Duplicates a dynamic application form """
        form = DynamicApplicationForm.objects.get(id=id).duplicate()
        info = self.model._meta.app_label, self.model._meta.model_name
        return redirect(reverse("admin:%s_%s_changelist" % info ))

    def get_urls(self):
        urls = super(DynamicApplicationFormAdmin, self).get_urls()
        info = self.model._meta.app_label, self.model._meta.model_name
        my_urls = [
            url(r'^(.+)/duplicate/$', self.duplicate, name='%s_%s_duplicate' % info),
        ]
        return my_urls + urls


class ApplicationFormFieldInline(SortableTabularInline):
    model = ApplicationFormFieldDefinition
    fields = ('name', 'label', 'kind', 'competition_attribute', 'is_visible_by_jury', 'extra_files', )


class ApplicationFormFieldTypeAdmin(admin.ModelAdmin):
    model = ApplicationFormFieldType
    list_display = ('name',)


class ApplicationFormFieldSetDefinitionAdmin(SortableAdmin):
    list_display = ('name', 'legend', 'order', 'parent')
    list_filter = ('parent',)
    inlines = [ApplicationFormFieldInline]
    change_form_template_extends = 'admin/ulysses_competitions/applicationformfieldsetdefinition/change_form.html'


class ApplicationFormFieldDefinitionAdmin(admin.ModelAdmin):
    list_display = ('name', 'label', 'kind', 'parent')
    list_filter = ('kind', )
    search_fields = ['name']


class CallAdmin(admin.ModelAdmin):
    list_display = ('name', 'requester', 'request_datetime', 'processed', 'draft')
    list_filter = ('processed', 'draft')
    date_hierarchy = 'request_datetime'


class CallRequestAdmin(admin.ModelAdmin):
    list_display = ('organization_name', 'email', 'request_datetime', 'processed', )
    list_filter = ('processed', )
    date_hierarchy = 'request_datetime'


# Super-admin registration
super_admin_site.register(StaticContent, StaticContentAdmin)
super_admin_site.register(Competition, CompetitionSuperAdmin)
super_admin_site.register(CompetitionAttribute, CompetitionAttributeAdmin)
super_admin_site.register(CompetitionNews, CompetitionNewsSuperAdmin)
super_admin_site.register(CompetitionManager, CompetitionManagerAdmin)
super_admin_site.register(Candidate, CandidateSuperAdmin)
super_admin_site.register(JuryMember, JuryMemberSuperAdmin)
super_admin_site.register(Evaluation, EvaluationAdmin)
super_admin_site.register(EvaluationNote, EvaluationNoteAdmin)
super_admin_site.register(EvaluationStatus, EvaluationStatusAdmin)
super_admin_site.register(CompetitionStep, CompetitionStepSuperAdmin)
super_admin_site.register(CompetitionStatus, CompetitionStatusAdmin)
super_admin_site.register(ApplicationDraft, ApplicationDraftSuperAdmin)
super_admin_site.register(CandidateBiographicElement, CandidateBiographicElementAdmin)
super_admin_site.register(CandidateDocument, CandidateDocumentAdmin)
super_admin_site.register(ApplicationElement, ApplicationElementAdmin)
super_admin_site.register(CandidateMedia, CandidateMediaAdmin)
super_admin_site.register(JuryMemberGroup, JuryMemberGroupAdmin)
super_admin_site.register(CandidateGroup, CandidateGroupAdmin)
super_admin_site.register(JuryMemberNotification, JuryMemberNotificationAdmin)
super_admin_site.register(CandidateNotification, CandidateNotificationAdmin)
super_admin_site.register(CandidateAttributeValue, CandidateAttributeValueAdmin)
super_admin_site.register(DraftBiographicElement, DraftBiographicElementAdmin)
super_admin_site.register(DraftDocument, DraftDocumentAdmin)
super_admin_site.register(DraftMedia, DraftMediaAdmin)
super_admin_site.register(TemporaryDocument, TemporaryDocumentAdmin)
super_admin_site.register(TemporaryMedia, TemporaryMediaAdmin)
super_admin_site.register(JuryApplicationElementToExclude, JuryApplicationElementToExcludeAdmin)
super_admin_site.register(DynamicApplicationForm, DynamicApplicationFormAdmin)
super_admin_site.register(ApplicationFormFieldType, ApplicationFormFieldTypeAdmin)
super_admin_site.register(ApplicationFormFieldSetDefinition, ApplicationFormFieldSetDefinitionAdmin)
super_admin_site.register(ApplicationFormFieldDefinition, ApplicationFormFieldDefinitionAdmin)
super_admin_site.register(EvaluationType)
super_admin_site.register(Call, CallAdmin)
super_admin_site.register(CallRequest, CallRequestAdmin)
super_admin_site.register(ProfileMandatoryFields)
super_admin_site.register(CompetitionAttributeValue)
