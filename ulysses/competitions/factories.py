# -*- coding: utf-8 -*-

import factory
import datetime
import random

from .models import (
    Competition, CompetitionManager, TemporaryDocument, TemporaryMedia, CompetitionStep,
    Candidate, JuryMember, JuryMemberCompetition, Evaluation, EvaluationField, EvaluationNote,
    ApplicationFormFieldType, CompetitionAttribute, DynamicApplicationForm, ApplicationFormFieldSetDefinition,
    ApplicationFormFieldDefinition, CandidateGroup, JuryMemberGroup, CompetitionAttributeValue, ApplicationDraft,
    EvaluationType, Call, StaticContent, EvaluationStatus, TemporaryDocumentExtraFile
)
from ulysses.composers.factories import ComposerFactory, UserFactory
from ulysses.partners.factories import PartnerFactory


class DynamicApplicationFormFactory(factory.DjangoModelFactory):
    class Meta:
        model = DynamicApplicationForm
    name = factory.Sequence(lambda n: 'name-{0}'.format(n))


class ApplicationFormFieldSetDefinitionFactory(factory.DjangoModelFactory):
    class Meta:
        model = ApplicationFormFieldSetDefinition

    parent = factory.SubFactory(DynamicApplicationFormFactory)
    legend = factory.Sequence(lambda n: 'legend-{0}'.format(n))


def get_random_application_form_field_type(n):
    valid_types = ApplicationFormFieldType.objects.exclude(
        name__in=[
            "ApplicationBooleanElementField",
            "CompetitionAttributeValueField",
            "CompetitionAttributeMultipleValueField",
            "MediaField",
        ]
    )
    index = random.randint(0, valid_types.count() - 1)
    return valid_types[index]


class CompetitionAttributeFactory(factory.DjangoModelFactory):

    class Meta:
        model = CompetitionAttribute

    key = factory.Sequence(lambda n: 'attribute-key-{0}'.format(n))


class CompetitionAttributeValueFactory(factory.DjangoModelFactory):
    class Meta:
        model = CompetitionAttributeValue
    attribute = factory.SubFactory(CompetitionAttributeFactory)
    # The primary key for this attribute in the competition (ex : '1')
    key = factory.Sequence(lambda n: 'key_{0}'.format(n))
    acronym = factory.Sequence(lambda n: 'acronym_{0}'.format(n))
    value = factory.Sequence(lambda n: 'value_{0}'.format(n))


class ApplicationFormFieldDefinitionFactory(factory.DjangoModelFactory):

    class Meta:
        model = ApplicationFormFieldDefinition

    parent = factory.SubFactory(ApplicationFormFieldSetDefinitionFactory)
    name = factory.Sequence(lambda n: 'field_name_{0}'.format(n))
    label = factory.Sequence(lambda n: 'field-label-{0}'.format(n))
    kind = factory.Sequence(lambda n: get_random_application_form_field_type(n))


class CompetitionManagerFactory(factory.DjangoModelFactory):
    class Meta:
        model = CompetitionManager

    user = factory.SubFactory(UserFactory)


class JuryMemberFactory(factory.DjangoModelFactory):
    class Meta:
        model = JuryMember

    user = factory.SubFactory(UserFactory)


class JuryMemberCompetitionFactory(factory.DjangoModelFactory):
    class Meta:
        model = JuryMemberCompetition


class CandidateFactory(factory.DjangoModelFactory):
    class Meta:
        model = Candidate

    composer = factory.SubFactory(ComposerFactory)
    id_in_competition = factory.Sequence(lambda n: n+1)
    application_date = datetime.datetime.now()


class EvaluationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Evaluation

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        status = kwargs.pop('status', None)
        if status:
            kwargs['status'] = EvaluationStatus.objects.get(url=status)
        return super(EvaluationFactory, cls)._create(model_class, *args, **kwargs)


class EvaluationFieldFactory(factory.DjangoModelFactory):
    class Meta:
        model = EvaluationField


class EvaluationNoteFactory(factory.DjangoModelFactory):
    class Meta:
        model = EvaluationNote


class CompetitionStepFactory(factory.DjangoModelFactory):
    class Meta:
        model = CompetitionStep

    url = factory.Sequence(lambda n: 'competition-step-url-{0}'.format(n))

    order_index = factory.Sequence(lambda n: n+1)
    is_open = True
    closing_date = datetime.date.today()


class CompetitionFactory(factory.DjangoModelFactory):
    class Meta:
        model = Competition

    title = factory.Sequence(lambda n: 'competition-title-{0}'.format(n))
    url = factory.Sequence(lambda n: 'competition-url-{0}'.format(n))

    organized_by = factory.SubFactory(PartnerFactory)
    # publication_date = datetime.datetime.now()
    # opening_date = datetime.datetime.now()
    # closing_date = datetime.datetime.now()
    # result_date = datetime.datetime.now()
    # unpublish_date = datetime.datetime.now()

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        kwargs.pop('open', False)

        if kwargs.pop('published', False):
            publication_date = datetime.datetime.now() - datetime.timedelta(days=5)
            opening_date = datetime.datetime.now() + datetime.timedelta(days=5)
            closing_date = datetime.datetime.now() + datetime.timedelta(days=10)
            unpublish_date = datetime.datetime.now() + datetime.timedelta(days=15)

        elif kwargs.pop('draft', False):
            # new
            publication_date = datetime.datetime.now() + datetime.timedelta(days=5)
            opening_date = datetime.datetime.now() + datetime.timedelta(days=10)
            closing_date = datetime.datetime.now() + datetime.timedelta(days=15)
            unpublish_date = datetime.datetime.now() + datetime.timedelta(days=20)

        elif kwargs.pop('archived', False):
            publication_date = datetime.datetime.now() - datetime.timedelta(days=20)
            opening_date = datetime.datetime.now() - datetime.timedelta(days=15)
            closing_date = datetime.datetime.now() - datetime.timedelta(days=10)
            unpublish_date = datetime.datetime.now() - datetime.timedelta(days=5)

        elif kwargs.pop('closed', False):
            publication_date = datetime.datetime.now() - datetime.timedelta(days=30)
            opening_date = datetime.datetime.now() - datetime.timedelta(days=20)
            closing_date = datetime.datetime.now() - datetime.timedelta(days=10)
            unpublish_date = datetime.datetime.now() + datetime.timedelta(days=5)

        else:
            # open
            publication_date = datetime.datetime.now() - datetime.timedelta(days=10)
            opening_date = datetime.datetime.now() - datetime.timedelta(days=5)
            closing_date = datetime.datetime.now() + datetime.timedelta(days=5)
            unpublish_date = datetime.datetime.now() + datetime.timedelta(days=10)

        result_date = closing_date
        kwargs['publication_date'] = publication_date
        kwargs['opening_date'] = opening_date
        kwargs['closing_date'] = closing_date
        kwargs['unpublish_date'] = unpublish_date
        kwargs['result_date'] = result_date

        return super(CompetitionFactory, cls)._create(model_class, *args, **kwargs)


class TemporaryDocumentFactory(factory.DjangoModelFactory):
    class Meta:
        model = TemporaryDocument


class TemporaryDocumentExtraFileFactory(factory.DjangoModelFactory):
    class Meta:
        model = TemporaryDocumentExtraFile


class TemporaryMediaFactory(factory.DjangoModelFactory):
    class Meta:
        model = TemporaryMedia


class CandidateGroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = CandidateGroup


class JuryMemberGroupFactory(factory.DjangoModelFactory):
    class Meta:
        model = JuryMemberGroup


class EvaluationTypeFactory(factory.DjangoModelFactory):
    class Meta:
        model = EvaluationType

    name = factory.Sequence(lambda n: 'evaluation-type-{0}'.format(n))


class CallFactory(factory.DjangoModelFactory):
    class Meta:
        model = Call

    name = factory.Sequence(lambda n: 'call-{0}'.format(n))
    request_datetime = datetime.datetime.now()
    requester = factory.SubFactory(UserFactory)


class StaticContentFactory(factory.DjangoModelFactory):
    class Meta:
        model = StaticContent

    key = factory.Sequence(lambda n: 'key-{0}'.format(n))
    description = factory.Sequence(lambda n: 'static-content-description-{0}'.format(n))
    text = factory.Sequence(lambda n: 'static-content-text-{0}'.format(n))


class ApplicationDraftFactory(factory.DjangoModelFactory):
    class Meta:
        model = ApplicationDraft

    composer = factory.SubFactory(ComposerFactory)
    competition = factory.SubFactory(CompetitionFactory)
