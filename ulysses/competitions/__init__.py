# -*- coding: utf-8 -*-

default_app_config = 'ulysses.competitions.apps.AppConfig'


def get_admin_site():
    """returns the admin site"""
    from .sites import CompetitionAdminSite
    admin_site = CompetitionAdminSite(name="competition-admin")
    admin_site.register_models()
    admin_site.disable_action('delete_selected')
    return admin_site