# -*- coding: utf-8 -*-

import datetime
import json
import os.path
import re
from urllib.parse import quote

import xlwt

from django.conf import settings
from django.conf.urls import url, include
from django.core.exceptions import PermissionDenied, ObjectDoesNotExist, ValidationError
from django.core.mail import send_mail
from django.core.paginator import Paginator
from django.contrib.auth.models import User
from django.contrib.auth.signals import user_logged_out
from django.contrib import messages
from django.contrib.sites.models import Site
from django.db.models.functions import Length
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _
from django.views.generic import RedirectView

from ulysses.generic.emails import send_text_email
from ulysses.sites import BaseAdminSite
from ulysses.utils import get_excel_response, get_excel_workbook, add_excel_worksheet, get_age, get_youtube_code

from ulysses.partners.models import Partner
from ulysses.partners.admin import PartnerAdmin
from ulysses.composers.models import Composer, Media, Document, BiographicElement
from ulysses.composers.admin import ComposerAdmin, MediaAdmin, DocumentAdmin, BiographicElementAdmin
from ulysses.profiles.models import Individual
from ulysses.profiles.utils import get_profile, get_profile_from_email
from ulysses.utils import dehtml, is_mp3_valid

from .admin import (
    JuryMemberAdmin, CandidateAdmin, CompetitionNewsAdmin, get_candidate_step_results_admin,
    CompetitionInfoAdmin, ApplicationDraftAdmin
)
from .forms import (
    CompetitionAdminAuthenticationForm, JuryMemberNotificationForm, CreateJuryMemberForm, JuryMemberEditForm,
    CandidateAdminForm, AddRemoveCompetitionStepForm, CandidateNotificationForm, get_candidate_media_modelform,
    get_candidate_admin_form, get_candidate_document_modelform, get_candidate_biographic_element_modelform,
    MakeJuryMemberForm, patch_media_fields, CompetitionStepSettingsForm
)
from .models import (
    Competition, CompetitionStep, JuryMember, Candidate, CompetitionManager, CompetitionNews, Evaluation,
    EvaluationStatus, CandidateGroup, JuryMemberGroup, JuryMemberNotification, EvaluationField,
    ApplicationDraft, JuryMemberCompetition, EvaluationNote, CandidateBiographicElement, CandidateNotification,
    CompetitionAttributeValue, CandidateDocument, CandidateMedia, get_candidate_in_step_proxy_model_class,
    get_candidate_in_step_manager, ApplicationFormFieldDefinition, get_candidate_folder,
    CandidateDocumentExtraFile
)


CANDIDATE_FILTER_SESSION_KEY = "candidate-filters"

DEFAULT_CANDIDATE_NOTIFICATION_BODY = """
Dear [[ first_name ]] [[ last_name ]],

On behalf of the [[ Competitionname ]] Committee,
we regret to inform you that your application has not been accepted for the [[ Competitionname ]].
At the end of this email, you will find your results (based on novelty, musical quality, practicality, and past experience), as well as some comments for you from anonymous reviewers.

Comments:
[[ {0}.comments ]]

Notes:
[[ {0}.notes ]]

Average:
[[ {0}.average ]]

Sincerely, Program Chairs

"""

DEAR_JURY_TEXT = """
Dear jury member,

Please connect to Ulysses platform to evaluate '%s' %s candidates.

Click here : https://%s%s


"""


def on_user_logged_out(sender, **kwargs):
    request = kwargs['request']
    from . import get_admin_site
    get_admin_site().clear_active_competition(request)
user_logged_out.connect(on_user_logged_out)


def evaluate_notification_message(body, candidate, competition_step):
    replacements = {
        'first_name': candidate.composer.user.first_name,
        'last_name': candidate.composer.user.last_name,
        'competitionname': competition_step.competition.title,
        # 'etape.average': moyenne des notes
        # 'etape.notes': liste des notes (moyenne pondere pour les champs)
        # 'etape.cle.average': moyenne des notes
        # 'etape.cle.notes': liste des notes
        # 'etape.comments': liste des commentaires
    }

    evaluations = Evaluation.objects.filter(competition_step=competition_step, candidate=candidate)
    replacements['{0}.comments'.format(competition_step.url)] = '\n\n'.join(
        [evaluation.comments for evaluation in evaluations]
    )

    evaluation_fields = EvaluationField.objects.filter(
        competition_step=competition_step, is_note=True
    ).order_by("order_index")

    comment_fields = EvaluationField.objects.filter(
        competition_step=competition_step, key__in=('comments', 'comment', 'candidate_comments')
    ).order_by("order_index")

    notes_by_fields = {}
    notes_by_jury = {}
    key_list = []
    jury_key_list = []

    for evaluation_field in evaluation_fields:
        key_list.append(evaluation_field.key)
        weight = evaluation_field.weight
        notes_by_fields[evaluation_field.key] = {}

        if EvaluationNote.objects.filter(evaluation__candidate=candidate, key=evaluation_field.key).exists():
            notes_queryset = EvaluationNote.objects.filter(evaluation__candidate=candidate, key=evaluation_field.key)
            for note_obj in notes_queryset:
                try:
                    note_val = float(note_obj.interpreted_value())
                    if note_val % 1 == 0:
                        note_val = int(note_val)
                except (ValueError, TypeError):
                    continue  # ignore the end of the loop

                jury_key = note_obj.evaluation.jury_member.id
                if jury_key not in jury_key_list:
                    jury_key_list.append(jury_key)

                if jury_key not in notes_by_fields[evaluation_field.key]:
                    notes_by_fields[evaluation_field.key][jury_key] = []
                notes_by_fields[evaluation_field.key][jury_key].append([note_val, weight])

                if jury_key not in notes_by_jury:
                    notes_by_jury[jury_key] = []
                notes_by_jury[jury_key].append((note_val, weight))

    # Note et moyenne par champ
    for key in key_list:
        fields_notes = []

        for jury in notes_by_fields[key]:
            for jury_note in notes_by_fields[key][jury]:
                fields_notes.append(jury_note[0])

        if fields_notes:
            replacements['{0}.{1}.notes'.format(competition_step.url, key)] = '\n'.join(
                [("{0}".format(note)) for note in fields_notes]
            )

            average_value = sum(fields_notes) / len(fields_notes)
            if average_value % 1:
                average_value = "{0}".format(average_value)
            else:
                average_value = "{0}".format(int(average_value))
            replacements['{0}.{1}.average'.format(competition_step.url, key)] = average_value
        else:
            replacements['{0}.{1}.average'.format(competition_step.url, key)] = ""
            replacements['{0}.{1}.notes'.format(competition_step.url, key)] = ""

    # Note et moyenne par jury
    jury_notes = []
    for jury_key in jury_key_list:
        notes_and_weights = notes_by_jury[jury_key]
        total_weight = sum([x[1] for x in notes_and_weights])
        if total_weight:
            total_value = sum([(float(x[0]) * x[1]) for x in notes_and_weights])
        else:
            total_value = sum([(float(x[0])) for x in notes_and_weights])
            total_weight = len(notes_and_weights)

        if total_weight:
            jury_notes.append(total_value / total_weight)

    # Comments
    comments = []
    for comment_field in comment_fields:
        comment_values = EvaluationNote.objects.filter(evaluation__candidate=candidate, key=comment_field.key)
        comments += [comment_value.value for comment_value in comment_values]
    replacements['{0}.comments'.format(competition_step.url)] = '\n\n'.join(comments)

    if jury_notes:
        replacements['{0}.notes'.format(competition_step.url)] = '\n'.join(
            ["{0}".format(note) if note % 1 else "{0}".format(int(note)) for note in jury_notes]
        )
        average_value = sum(note for note in jury_notes) / len(jury_notes)
        if average_value % 1:
            average_value = "{0}".format(average_value)
        else:
            average_value = int(average_value)
        replacements['{0}.average'.format(competition_step.url)] = average_value
    else:
        replacements['{0}.average'.format(competition_step.url)] = ""
        replacements['{0}.notes'.format(competition_step.url)] = ""

    key_matches = re.finditer("\[\[(?P<key>[\s\w\d_\.-]+)\]\]", body)
    if key_matches:
        for key_match in key_matches:
            key_text = key_match.group('key')
            key = key_text.strip().lower()
            key_value = replacements.get(key, None)
            if key_value is not None:
                body = body.replace("[[{0}]]".format(key_text), '{0}'.format(key_value))
    return body


def get_safe_ids(request):
    return [int(id_) for id_ in request.GET["ids"].split(',') if id_.isdigit()]


class CompetitionAdminSite(BaseAdminSite):
    login_form = CompetitionAdminAuthenticationForm
    logout_template = "admin/logged_out.html"
    name = 'competition_admin'

    def check_competition(self, request):
        if not self.get_active_competition(request):
            return self.choose_competition(request)

    def admin_view(self, view, cacheable=False, **kwargs):
        """Main wrapper for displaying any admin view"""
        return super(CompetitionAdminSite, self).admin_view(
            view, cacheable=cacheable, extra_handler=self.check_competition, **kwargs
        )

    def show_candidates(self, request):
        # Store filter in sessions
        filter_args = {}
        for key, val in list(request.GET.items()):
             # Ignore 'o', 'p' and 'q' on query string
            if key in ['o', 'p', 'q']:
                continue
            filter_args[key] = val
        request.session[CANDIDATE_FILTER_SESSION_KEY] = filter_args

        candidate_admin_class = self._registry[Candidate]
        return candidate_admin_class.changelist_view(request, {'has_add_permission': False})

    def show_application_drafts(self, request):

        # Store filter in sessions
        filter_args = {}
        for key, val in list(request.GET.items()):
            # Ignore 'o', 'p' and 'q' on query string
            if key in ['o', 'p', 'q']:
                continue
            filter_args[key] = val
        request.session[CANDIDATE_FILTER_SESSION_KEY] = filter_args

        candidate_admin_class = self._registry[ApplicationDraft]
        return candidate_admin_class.changelist_view(
            request, {'has_add_permission': False, 'has_update_permission': False}
        )

    def show_jury_members(self, request):
        jury_admin_class = self._registry[JuryMember]
        return jury_admin_class.changelist_view(request, {'has_add_permission': True})

    def show_test(self, request):
        params = {}
        from ulysses.reference.models import Country
        params["countries"] = Country.objects.all()[0:20]
        return render(request, 'admin/design_test.html', params)

    def edit_candidate(self, request, id):
        params = {}
        active_competition = self.get_active_competition(request)
        if not active_competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        candidate = Candidate.objects.get(id=id)
        params["profile"] = get_profile(candidate.composer.user)

        if "export" in request.GET:
            tabs = candidate.get_informations_as_excel_data()
            wb = xlwt.Workbook(encoding='utf8')
            for tab in tabs:
                add_excel_worksheet(wb, tab["name"], tab["cols"], tab["lines"])
            return get_excel_response(wb)

        if request.method == 'POST':  # If the form has been submitted
            candidate_admin_form = get_candidate_admin_form(candidate, request.POST)
            admin_form = CandidateAdminForm(request.POST)
            if candidate_admin_form.is_valid() and admin_form.is_valid():  # All validation rules pass
                is_valid = False
                if admin_form.cleaned_data["is_valid"]:
                    is_valid = True
                candidate.is_valid = is_valid
                candidate.notes = admin_form.cleaned_data["notes"]
                candidate.save()
                candidate.set_application_data(request.POST)
                messages.success(request, _('The candidate has been updated'))
                return redirect(reverse('competition-admin:edit_candidate', args=[id]))
        else:
            admin_form = CandidateAdminForm(initial={'is_valid': candidate.is_valid, 'notes': candidate.notes})
            candidate_admin_form = get_candidate_admin_form(candidate)

        params["form"] = candidate_admin_form
        params["admin_form"] = admin_form

        # Load evaluations
        evaluations = {}
        for step in active_competition.steps():
            objects = []
            for evaluation in Evaluation.objects.filter(competition_step=step, candidate=candidate):
                objects.append(
                    {
                        'evaluation': evaluation,
                        'notes': EvaluationNote.objects.filter(evaluation=evaluation)
                    }
                )
            if objects:
                evaluations[step] = objects
        params["evaluations"] = evaluations

        # Find previous / Next candidates
        filter_args = {"competition": active_competition}
        if CANDIDATE_FILTER_SESSION_KEY in request.session:
            filter_args.update(request.session[CANDIDATE_FILTER_SESSION_KEY])
        filter_args.pop('filter_str', None)
        filter_args.pop('recommendation_str', None)
        filter_args.pop('groups', None)
        filter_args.pop('steps', None)

        candidates_id = [item["id"] for item in Candidate.objects.filter(**filter_args).values("id")]
        prev_candidate_url = "#"
        next_candidate_url = "#"
        current_index = -1
        cpt = 0
        for candidate_id in candidates_id:
            if candidate_id == candidate.id:
                current_index = cpt
            cpt += 1
        if current_index >= 0:
            if current_index > 0:
                prev_id = candidates_id[current_index-1]
                prev_candidate_url = reverse('competition-admin:edit_candidate', args=[prev_id])
            if current_index < len(candidates_id)-1:
                next_id = candidates_id[current_index+1]
                next_candidate_url = reverse('competition-admin:edit_candidate', args=[next_id])
            params["prev_candidate_url"] = prev_candidate_url
            params["next_candidate_url"] = next_candidate_url
            params["candidate_index"] = current_index+1
            params["candidate_count"] = len(candidates_id)
        return render(request, 'admin/candidate_admin_view.html', params)

    def get_active_competition_step(self, request, step_url):
        competition = self.get_active_competition(request)
        try:
            active_step = CompetitionStep.objects.get(competition=competition, url=step_url)
        except CompetitionStep.DoesNotExist:
            messages.error(request, _("Step {0} doesn't exist in competition {1}".format(step_url, competition)))
            return None
        return active_step

    def show_competition_step_jury_member(self, request, step_url, id):
        params = {}
        jury_member = JuryMember.objects.get(id=id)
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        params["active_step"] = active_step
        params["evaluations"] = Evaluation.objects.filter(competition_step=active_step ,jury_member=jury_member)
        params["notifications"] = JuryMemberNotification.objects.filter(
            competition_step=active_step, jury_member=jury_member
        ).order_by("-date")
        params["jury_member"] = jury_member
        return render(request, 'admin/competition_step_jury_member.html', params)

    def create_evaluation(self, competition_step, candidate_id, jury_member_id):
        evaluation, is_new = Evaluation.objects.get_or_create(
            candidate=Candidate.objects.get(id=candidate_id),
            competition_step=competition_step,
            jury_member=JuryMember.objects.get(id=jury_member_id)
        )
        if is_new:
            evaluation.status = EvaluationStatus.objects.get(name='to process')
            evaluation.save()
        return evaluation

    def associate_jury_members(self, request, step_url):

        params = {}

        ids = get_safe_ids(request)
        candidates = Candidate.objects.filter(id__in=ids)
        referer = request.GET.get("referer") or reverse('competition-admin:index')
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        params["active_step"] = active_step
        params["candidates"] = candidates
        params["jury_members"] = JuryMember.objects.filter(steps=active_step).order_by("user__last_name")
        params["groups"] = JuryMemberGroup.objects.filter(
            competition=self.get_active_competition(request)
        ).order_by("name")

        if request.method == 'POST':  # If the form has been submitted...
            action_choice = None
            # Perform selected action
            if "action_choice" in request.POST:
                action_choice = int(request.POST["action_choice"])
                if action_choice == 1:
                    # Associate selected candidates to selected jury members
                    try:
                        jury_member_id = int(request.POST.get("jury_member"))
                    except (ValueError, TypeError):
                        jury_member_id = 0
                    if jury_member_id:
                        for candidate in candidates:
                            evaluations = Evaluation.objects.filter(
                                candidate=candidate,
                                jury_member__id=jury_member_id,
                                competition_step=active_step
                            )
                            if not evaluations.exists():
                                self.create_evaluation(active_step, candidate.id, jury_member_id)
                elif action_choice == 2:
                    # Associate selected candidates to selected jury member group
                    try:
                        group_id = int(request.POST.get("jury_member_group"))
                    except (ValueError, TypeError):
                        group_id = 0
                    if group_id:
                        try:
                            group = JuryMemberGroup.objects.get(id=group_id)
                        except JuryMemberGroup.DoesNotExist:
                            group = None
                        if group:
                            for candidate in candidates:
                                for jm in group.members():
                                    evaluations = Evaluation.objects.filter(
                                        candidate=candidate,
                                        jury_member__id=jm.id,
                                        competition_step=active_step
                                    )
                                    if not evaluations.exists():
                                        self.create_evaluation(active_step, candidate.id, jm.id)
                else:
                    messages.error(request, "Unhandled case")
            else:
                messages.error(request, "No action to perform")
            # Redirect to referer
            return redirect(referer)

        return render(request, 'admin/associate_jury_members.html', params)

    def manage_allocations(self, request, step_url):

        is_ajax = request.method == 'POST' and request.is_ajax()

        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            if is_ajax:
                return HttpResponse(
                    json.dumps({'url': reverse('competition-admin:index')}), content_type='application/json'
                )
            else:
                return HttpResponseRedirect(reverse('competition-admin:index'))

        if request.method == 'POST':  # If the form has been submitted...

            if is_ajax:
                form_data = json.loads(request.POST.get('form_data'))
            else:
                form_data = request.POST

            if "run_action" in form_data:
                action = form_data["action"]
                if action:
                    referer = "."
                    if is_ajax:
                        selected = form_data.get("_candidate")
                    else:
                        selected = form_data.getlist("_candidate")
                    if len(selected) > 0:
                        if action == "associate_jury_members":
                            redirect_url = "./associate_jury_members?ids=%s&referer=%s" % (",".join(selected), referer)
                            if is_ajax:
                                HttpResponse(json.dumps({'url': redirect_url}), content_type='application/json')
                            else:
                                return redirect(redirect_url)
                        else:
                            raise RuntimeError("Unhandled action")
                    else:
                        messages.add_message(
                            request, messages.ERROR, _('Can not run action since no candidates have been selected.')
                        )
                else:
                       messages.add_message(request, messages.ERROR, _('No action selected.'))

            else:
                competition_step = active_step
                candidate_group_id = request.GET.get('cg', None)
                jury_group_id = request.GET.get('jg', None)

                if candidate_group_id:
                    candidate_group_id = int(candidate_group_id)
                    try:
                        candidate_group = CandidateGroup.objects.get(
                            competition=competition_step.competition,
                            id=candidate_group_id
                        )
                    except CandidateGroup.DoesNotExist:
                        candidate_group = None
                else:
                    candidate_group = None

                if jury_group_id:
                    jury_group_id = int(jury_group_id)
                    try:
                        jury_group = JuryMemberGroup.objects.get(
                            competition=competition_step.competition,
                            id=jury_group_id
                        )
                    except CandidateGroup.DoesNotExist:
                        jury_group = None
                else:
                    jury_group = None

                existing_evaluations = Evaluation.objects.filter(
                    competition_step=active_step,
                    status__url='to_process'
                )
                if candidate_group:
                    existing_evaluations = existing_evaluations.filter(candidate__groups=candidate_group)
                if jury_group:
                    existing_evaluations = existing_evaluations.filter(jury_member__groups=jury_group)
                keep_these_evaluation_ids = []

                # Create blank evaluations for checked boxes
                keys = list(form_data)
                candidates_map = {}
                jury_members_map = {}

                for key in keys:
                    if key.startswith("c_"):
                        # Analyze key
                        candidate_suffix_index = key.find("c_")
                        jury_suffix_index = key.find("j_")
                        candidate_id = int(key[candidate_suffix_index+2:jury_suffix_index-1])
                        jury_member_id = int(key[jury_suffix_index+2:])

                        candidate_evaluations = existing_evaluations.filter(
                            candidate__id=candidate_id,
                            jury_member__id=jury_member_id
                        )

                        if candidate_evaluations.count():
                            candidate_evaluation = candidate_evaluations[0]
                            keep_these_evaluation_ids.append(candidate_evaluation.id)
                        else:

                            candidate = candidates_map.get(candidate_id)
                            if not candidate:
                                try:
                                    candidate = Candidate.objects.get(
                                        competition=competition_step.competition,
                                        id=candidate_id,
                                    )
                                    candidates_map[candidate_id] = candidate
                                except Candidate.DoesNotExist:
                                    candidate = None

                            jury = jury_members_map.get(jury_member_id)
                            if not jury:
                                try:
                                    jury = JuryMember.objects.get(
                                        competitions=competition_step.competition,
                                        id=jury_member_id,
                                    )
                                    jury_members_map[jury_member_id] = jury
                                except JuryMember.DoesNotExist:
                                    jury = None

                            # Create evaluation
                            if candidate and jury:
                                skip = False
                                if candidate_group:
                                    if candidate_group not in candidate.groups.all():
                                        skip = True
                                if jury_group:
                                    if jury_group not in jury.groups.all():
                                        skip = True

                                if not skip:
                                    evaluation = self.create_evaluation(competition_step, candidate_id, jury_member_id)
                                    keep_these_evaluation_ids.append(evaluation.id)

                # Delete everything we don't want to keep
                existing_evaluations.filter(status__url='to_process').exclude(id__in=keep_these_evaluation_ids).delete()

                # Set message
                messages.add_message(request, messages.SUCCESS, 'Your changes have been saved.')

        # Detect selected groups (if any)
        selected_candidate_group = None
        selected_jury_member_group = None
        if "cg" in request.GET:
            selected_candidate_group = int(request.GET["cg"])
        if "jg" in request.GET:
            selected_jury_member_group = int(request.GET["jg"])

        params = {}
        params["active_step"] = active_step
        competition = self.get_active_competition(request)
        # Get all candidates & jury members active step
        jm_filters = {}
        jm_filters["steps"] = active_step
        if selected_jury_member_group:
            jm_filters["groups__id"] = selected_jury_member_group
        jury_members = JuryMember.objects.filter(**jm_filters).order_by("user__last_name")
        candidate_filters = {}
        candidate_filters["steps"] = active_step
        if selected_candidate_group:
            candidate_filters["groups__id"] = selected_candidate_group
        candidates = Candidate.objects.filter(**candidate_filters).order_by("composer__user__last_name")
        # Prepare header row
        header_row = []
        for jury_member in jury_members:
            if jury_member.user.first_name:
                first_name_letter = jury_member.user.first_name[:1].upper() + ". "
            else:
                first_name_letter = ''
            header_row.append("{0}{1}".format(first_name_letter, jury_member.user.last_name))
        header_row.append(_('Total'))
        params["header_row"] = header_row
        # Prepare rows
        rows = []
        for candidate in candidates:
            row_dict = {
                "candidate_id": candidate.id,
            }
            row = [
                "{0} {1}".format(
                    candidate.composer.user.first_name, candidate.composer.user.last_name
                )
            ]
            for jury_member in jury_members:
                status = None
                results = Evaluation.objects.filter(
                    candidate=candidate, jury_member=jury_member, competition_step=active_step
                )
                if results:
                    status = results[0].status.url
                infos = {
                    'key': "c_%s_j_%s" % (candidate.id, jury_member.id),
                    'status': status,
                }
                row.append(infos)
            row.append(str(Evaluation.objects.filter(
                candidate=candidate, competition_step=active_step
            ).count()))
            row_dict["row"] = row
            rows.append(row_dict)
        params["rows"] = rows

        # Prepare footer row
        footer_row = []
        for jury_member in jury_members:
            footer_row.append(str(Evaluation.objects.filter(
                jury_member=jury_member, competition_step=active_step
            ).count()))
        params["footer_row"] = footer_row

        # Prepare filters
        candidate_groups = []
        jury_member_groups = []
        # Default values
        default_url = "?"
        if selected_jury_member_group:
            default_url = "?jg=%s" % selected_jury_member_group
        candidate_groups.append({
            "url": default_url,
            "name": "------------",
            "selected": (selected_candidate_group is None)
        })
        default_url = "?"
        if selected_candidate_group:
            default_url = "?cg=%s" % selected_candidate_group
        jury_member_groups.append({
            "url": default_url,
            "name": "------------",
            "selected": (selected_jury_member_group is None)
        })

        # Other values
        for obj in CandidateGroup.objects.filter(competition=active_step.competition):
            keys = {'cg': obj.id}
            if selected_jury_member_group:
                keys['jg'] = selected_jury_member_group
            url = "?%s" % "&".join(["%s=%s" % (key, value) for (key,value) in list(keys.items())])
            candidate_groups.append(
                {"url": url, "name": obj.name, "selected": (obj.id == selected_candidate_group)}
            )

        for obj in JuryMemberGroup.objects.filter(competition=active_step.competition):
            keys = {'jg': obj.id}
            if selected_candidate_group:
                keys['cg'] = selected_candidate_group

            url = "?%s" % "&".join(["%s=%s" % (key, value) for (key, value) in list(keys.items())])
            jury_member_groups.append(
                {"url": url, "name": obj.name, "selected": (obj.id == selected_jury_member_group)}
            )
        params["candidate_groups"] = candidate_groups
        params["jury_member_groups"] = jury_member_groups
        params['has_permission'] = self.has_permission(request)

        if is_ajax:
            return HttpResponse(json.dumps({'url': request.get_full_path()}), content_type='application/json')
        else:
            return render(request, 'admin/manage_allocations.html', params)

    def select_competition(self, request, id):
        """
        Sets the active competition and redirect to competition admin home page
        """
        # Perform security check
        if not request.user.is_authenticated:
            return self.login(request)

        competitions = Competition.objects.filter(id=id)
        if not competitions.exists():
            raise Http404

        checked = False
        if request.user.is_superuser:
            checked = True
        else:
            if self.is_competition_admin(request):
                # Competition should be managed by current user
                checked = competitions.filter(managed_by__user=request.user).exists()
            else:
                # Jury member should be authorized to evaluate this competition
                jm = JuryMember.objects.get(user=request.user)
                for step in jm.steps.all():
                    if step.competition.id == int(id):
                        checked = True
        if not checked:
            raise PermissionDenied()

        # Set active competition
        self.set_active_competition(request, int(id))
        # Redirect to admin home page
        return redirect('/admin')

    def get_active_competition(self, request):
        if not request.session.__contains__(settings.ACTIVE_COMPETITION_KEY):
            return None
        active_competition = request.session[settings.ACTIVE_COMPETITION_KEY]
        if not type(active_competition) is Competition:
            return None
        return active_competition

    def set_active_competition(self,request,id):
        # Clear session filter, if applicable
        if CANDIDATE_FILTER_SESSION_KEY in request.session:
            del request.session[CANDIDATE_FILTER_SESSION_KEY]
        # Set active competitions
        request.session[settings.ACTIVE_COMPETITION_KEY] = Competition.objects.get(id=id)

    def clear_active_competition(self,request):
        if settings.ACTIVE_COMPETITION_KEY in request.session:
            del request.session[settings.ACTIVE_COMPETITION_KEY]

    def is_jury_member(self, request):
        return request.user.is_authenticated and JuryMember.objects.filter(user=request.user).exists()

    def is_competition_admin(self, request):
        return request.user.is_authenticated and CompetitionManager.objects.filter(user=request.user).exists()

    def choose_competition(self, request):
        """
        Displays a view allowing logged user to select which competition he want to administrate
        """
        competitions = []
        if request.user.is_superuser:
            # If current user is a super user : choose among all competitions
            competitions = Competition.objects.all().order_by('title')
            active_competitions = competitions.filter(archived=False)
            archived_competitions = competitions.filter(archived=True)

        elif self.is_competition_admin(request):  # user is a 'competition-admin'
            competitions = Competition.objects.filter(managed_by__user=request.user).order_by('title')
            active_competitions = competitions.filter(archived=False)
            archived_competitions = competitions.filter(archived=True)

        elif self.is_jury_member(request):  # user is a'jury-member'
            competitions = []
            jury_member = JuryMember.objects.get(user=request.user)
            for step in jury_member.steps.all():
                if step.competition not in competitions:
                    competitions.append(step.competition)

            active_competitions_ids = []
            archived_competitions_ids = []
            competitions = sorted(competitions, key=lambda cpt: cpt.title)
            for competition in competitions:
                if competition.archived:
                    archived_competitions_ids.append(competition.id)
                else:
                    active_competitions_ids.append(competition.id)
            active_competitions = Competition.objects.filter(id__in=active_competitions_ids)
            archived_competitions = Competition.objects.filter(id__in=archived_competitions_ids)
        else:
            raise PermissionDenied

        active_competitions = active_competitions.order_by('-closing_date')
        archived_competitions = archived_competitions.order_by('closing_date')

        active_competitions_paginator = Paginator(active_competitions, 6)
        archived_competitions_paginator = Paginator(archived_competitions, 24)
        active_competitions_page_number = request.GET.get('active', 1)
        archived_competitions_page_number = request.GET.get('archived', 1)
        active_competitions = active_competitions_paginator.page(active_competitions_page_number)
        archived_competitions = archived_competitions_paginator.page(archived_competitions_page_number)

        # Display a view allowing user to select the competition
        context = {
            "is_superuser": request.user.is_superuser,
            "competitions": list(competitions),
            "active_competitions": active_competitions,
            "archived_competitions": archived_competitions,
        }
        return render(request, 'admin/choose_competition.html', context)

    def index(self, request, extra_context=None):
        if self.is_competition_admin(request) or request.user.is_superuser:
            return redirect('%sinfos/' % request.META["PATH_INFO"])
        return redirect('%sevaluations/' % request.META['PATH_INFO'])

    def register_models(self):
        self.register(Partner, PartnerAdmin)
        self.register(Composer, ComposerAdmin)
        self.register(Media, MediaAdmin)
        self.register(Document, DocumentAdmin)
        self.register(BiographicElement, BiographicElementAdmin)
        self.register(Candidate, CandidateAdmin)
        self.register(ApplicationDraft, ApplicationDraftAdmin)
        self.register(JuryMember, JuryMemberAdmin)
        self.register(CompetitionNews, CompetitionNewsAdmin)
        self.register(Competition, CompetitionInfoAdmin)

    def show_informations(self, request):
        from .models import Competition
        context = {}
        context['competition_admin'] = True
        context['title'] = ''
        active_competition = self.get_active_competition(request)
        if not active_competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))
        context['candidate_count'] = Candidate.objects.filter(competition__id=active_competition.id).count()
        context['draft_count'] = ApplicationDraft.objects.filter(competition__id=active_competition.id).count()
        context['jury_member_count'] = JuryMemberCompetition.objects.filter(
            competition__id=active_competition.id
        ).count()
        cell_limit = 32767
        long_biographies = CandidateBiographicElement.objects.filter(
            candidate__competition=active_competition
        ).annotate(text_length=Length('text')).filter(text_length__gte=cell_limit)
        truncated_cells_in_excel = 0
        for long_biography in long_biographies:
            if len(dehtml(long_biography.text)) > cell_limit:
                truncated_cells_in_excel += 1
        context['truncated_cells_in_excel'] = truncated_cells_in_excel
        competition_admin = CompetitionInfoAdmin(Competition, self)
        return competition_admin.change_view(
            request, '%s' % active_competition.id, extra_context=context
        )

    def switch_competition(self,request):
        id = int(request.POST["competition"])
        return redirect(reverse('competition-admin:select_competition', args=[id]))

    def notify_jury_member(self, subject, body, sender, from_email, jury_member, competition_step, cc_me=None):
        # Send mail
        send_text_email(subject, body, from_email, (jury_member.user.email, ), ccs=cc_me or [])
        # Log notification
        jmn = JuryMemberNotification()
        jmn.sender = sender
        jmn.from_email = from_email
        jmn.jury_member = jury_member
        jmn.competition_step = competition_step
        jmn.subject = subject
        jmn.body = body
        jmn.date = datetime.datetime.now()
        jmn.save()

    def notify_jury_members(self, request, step_url):

        params = {}

        ids = get_safe_ids(request)
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        jury_members = JuryMember.objects.filter(pk__in=ids)

        if request.method == "POST":
            form = JuryMemberNotificationForm(request.POST)
            if form.is_valid():
                subject = form.cleaned_data["subject"]
                body = form.cleaned_data["body"]
                from_email = form.cleaned_data["from_email"]
                cc_me = form.cleaned_data["cc_me"]
                ccs = []
                if cc_me:
                    ccs = [from_email]
                # Send the message to each jury member
                for jm in jury_members:
                    self.notify_jury_member(subject, body, request.user, from_email, jm, active_step, ccs)
                # Redirect to 'follow evaluations'
                url = reverse('competition-admin:step_follow_evaluations', args=[active_step.url])
                return redirect(url)
        else:
            default_data = {}
            default_data["from_email"] = active_step.competition.email
            default_data["subject"] = '%s / %s evaluations' % (active_step.competition.title,active_step.name)
            current_site = Site.objects.get(id=settings.SITE_ID)
            default_data["body"] = DEAR_JURY_TEXT % (
                active_step.competition.title,
                active_step.name,
                current_site.domain,
                reverse("jury:dashboard")
            )
            form = JuryMemberNotificationForm(initial=default_data)

        params["form"] = form
        params["jury_members"] = jury_members
        params["active_step"] = active_step

        return render(request, 'admin/notify_jury_members.html', params)

    def notify_candidate(
        self, subject, msg_body, from_email, email_address, sender, candidate, competition_step, is_test, css=None
    ):
        # Send mail
        send_text_email(subject, msg_body, from_email, [email_address], css)

        # Log notification
        CandidateNotification.objects.create(
            sender=sender,
            from_email=from_email,
            candidate=candidate,
            competition_step=competition_step,
            subject=subject,
            body=msg_body,
            is_test=is_test,
            date=datetime.datetime.now()
        )

    def notify_candidates(self, request, step_url):

        params = {}

        if not request.GET.get('ids', None):
            raise Http404

        ids = get_safe_ids(request)
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        candidates = Candidate.objects.filter(pk__in=ids)

        if request.method == "POST":
            form = CandidateNotificationForm(request.POST)
            if form.is_valid():
                subject = form.cleaned_data["subject"]
                body = form.cleaned_data["body"]
                from_email = form.cleaned_data["from_email"]
                cc_me = form.cleaned_data['cc_me']

                if 'send_message' in request.POST:
                    send_to = [candidate.composer.user.email for candidate in candidates]
                    is_test = False
                else:
                    send_to = [from_email]
                    is_test = True

                ccs = []
                if cc_me:
                    ccs = [from_email]
                
                # Send the message to each candidate
                for (email_address, candidate) in zip(send_to, candidates):
                    msg_body = evaluate_notification_message(body, candidate, active_step)

                    self.notify_candidate(
                        subject, msg_body, from_email, email_address, request.user, candidate, active_step,
                        is_test, ccs
                    )

                messages.success(request, "A message has been send to {0}".format(','.join(send_to)))

                if is_test:
                    # Reload the form with same data
                    params["form"] = form
                    params["candidates"] = candidates
                    params["active_step"] = active_step
                    return render(request, 'admin/notify_candidates.html', params)
                else:
                    # Redirect to 'follow results'
                    url = reverse('competition-admin:step_follow_results', args=[active_step.url])
                    return redirect(url)
        else:
            current_site = Site.objects.get(id=settings.SITE_ID)
            default_body = DEFAULT_CANDIDATE_NOTIFICATION_BODY.format(active_step.url)

            default_data = {
                "from_email": active_step.competition.email,
                "subject": '%s / %s evaluations' % (active_step.competition.title, active_step.name),
                "body": default_body
            }
            form = CandidateNotificationForm(initial=default_data)

        params["form"] = form
        params["candidates"] = candidates
        params["active_step"] = active_step

        return render(request, 'admin/notify_candidates.html', params)

    def follow_evaluations(self, request, step_url):

        params = {}
        active_step = self.get_active_competition_step(request, step_url)

        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        if request.method == 'POST':  # If the form has been submitted...
            if request.POST.__contains__("_jury_member"):
                selected = request.POST.getlist("_jury_member")
                redirect_url = reverse('competition-admin:step_notify_jury_members', args=[active_step.url])
                redirect_url += "?ids={0}".format(",".join(selected))
                return redirect(redirect_url)
            else:
                messages.error(request, _("Please select a jury member"))

        # Get all jury members for this step
        jury_members = active_step.get_jury_members()
        # Construct results
        ordering = request.GET.get('order', '')
        order_values = ['total', 'to_process', 'in_progress', 'completed']
        for index, elt in enumerate(order_values):
            if elt == ordering:
                # reverse order
                order_values[index] = '-' + elt
        columns = [
            '<a href="?order=none">Jury member</a>',
            'Email',
            '<a href="?order={0}">Total</a>'.format(order_values[0]),
            '<a href="?order={0}">To process</a>'.format(order_values[1]),
            '<a href="?order={0}">In progress</a>'.format(order_values[2]),
            '<a href="?order={0}">Completed</a>'.format(order_values[3]),
            'Notified'
        ]
        lines = []
        for jm in jury_members:
            total = Evaluation.objects.filter(competition_step=active_step, jury_member=jm).count()
            to_process = Evaluation.objects.filter(
                competition_step=active_step, jury_member=jm, status__url="to_process"
            ).count()
            in_progress = Evaluation.objects.filter(
                competition_step=active_step, jury_member=jm, status__url="in_progress"
            ).count()
            completed = Evaluation.objects.filter(
                competition_step=active_step, jury_member=jm,status__url="completed"
            ).count()
            url = reverse('competition-admin:step_jury_members', args=[active_step.url, jm.id])
            jury_member_link = '<a href="{0}">{1} {2}</a>'.format(
                url, jm.user.first_name, jm.user.last_name
            )
            line = {}
            line["jury_member_id"] = jm.id
            items = []
            items.append({'val': jury_member_link})
            items.append({'val': jm.user.email})
            items.append({'val': total, 'class': 'numeric'})
            items.append({'val': '{0}/{1}'.format(to_process, total), 'class': 'to_process numeric'})
            items.append({'val': '{0}/{1}'.format(in_progress, total), 'class': 'in_progress numeric'})
            items.append({'val': '{0}/{1}'.format(completed, total),'class': 'completed numeric'})
            # Have we notified this jury members ?
            notification_count = JuryMemberNotification.objects.filter(jury_member=jm,competition_step=active_step).count()
            if notification_count > 0:
                items.append({'val': "Yes (%s)" % notification_count, 'class': 'notified'})
            else:
                items.append({'val': "No", 'class': 'not_notified'})
            line["items"] = items
            lines.append(line)

        def sort_total(line, index, reversed):
            value = int(line["items"][index]['val'])
            if reversed:
                return -value
            else:
                return value

        def sort_sub_total(line, index, reversed):
            value = int(line["items"][index]['val'].split('/')[0])
            if reversed:
                return -value
            else:
                return value

        if ordering:
            reverse_oder = ordering[0] == '-'
            if reverse_oder:
                ordering = ordering[1:]

        if ordering == 'total':
            lines = sorted(lines, key=lambda line: sort_total(line, 2, reverse_oder))
        if ordering == 'to_process':
            lines = sorted(lines, key=lambda line: sort_sub_total(line, 3, reverse_oder))
        if ordering == 'in_progress':
            lines = sorted(lines, key=lambda line: sort_sub_total(line, 4, reverse_oder))
        if ordering == 'completed':
            lines = sorted(lines, key=lambda line: sort_sub_total(line, 5, reverse_oder))

        params["columns"] = [mark_safe(column) for column in columns]
        params["lines"] = lines
        params["active_step"] = active_step
        params['has_permission'] = self.has_permission(request)
        return render(request, 'admin/follow_evaluations.html', params)

    def step_settings(self, request, step_url):

        active_step = self.get_active_competition_step(request, step_url)

        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        if request.method == 'POST':  # If the form has been submitted...
            form = CompetitionStepSettingsForm(request.POST, instance=active_step)
            if form.is_valid():
                form.save()
                messages.success(request, _('The settings have been saved'))
                # Refresh the current page
                return HttpResponseRedirect(request.path)
        else:
            form = CompetitionStepSettingsForm(instance=active_step)

        context = {
            "form": form,
            "active_step": active_step,
        }
        return render(request, 'admin/step_settings.html', context)

    def edit_jury_member(self, request, id):
        context = {}
        jury_member = JuryMember.objects.get(id=id)
        if request.method == 'POST':  # If the form has been submitted...
            form = JuryMemberEditForm(request.POST)
            if form.is_valid():  # All validation rules pass
                # Update
                jury_member.user.first_name = form.cleaned_data["first_name"]
                jury_member.user.last_name = form.cleaned_data["last_name"]
                jury_member.user.email = form.cleaned_data["email"]
                jury_member.user.save()
                jury_member.save()
                # Redirect to jury member list
                return redirect(reverse('competition-admin:show_jury_members'))
        else:
            data = {
                'email': jury_member.user.email,
                'first_name': jury_member.user.first_name,
                'last_name': jury_member.user.last_name,
            }
            form = JuryMemberEditForm(data)

        context["form"] = form
        context["jury_member"] = jury_member
        return render(request, 'admin/edit_jury_member.html', context)

    def add_jury_member(self, request):
        params = {}

        if request.method == 'POST':  # If the form has been submitted...
            form = CreateJuryMemberForm(request.POST)
            if form.is_valid():  # All validation rules pass
                email = form.cleaned_data["email"]

                # Create individual profile for the jury member if not already exist
                profile = get_profile_from_email(email)
                if not profile:
                    user = User.objects.create_user(
                        form.cleaned_data["username"],
                        email,
                        form.cleaned_data["password"]
                    )
                    user.first_name = form.cleaned_data["first_name"]
                    user.last_name = form.cleaned_data["last_name"]
                    Individual.objects.create(user=user)

                else:
                    user = profile.user
                    user.username = form.cleaned_data["username"]

                # Set staff status
                user.is_staff = True
                user.save()

                # Create jury member
                jm = JuryMember()
                jm.user = user
                jm.save()

                # Associate jury member to current competition
                jmc = JuryMemberCompetition()
                jmc.jury_member = jm
                jmc.competition = self.get_active_competition(request)
                jmc.save()

                # Redirect to jury member list
                return redirect(reverse('competition-admin:show_jury_members'))

            else:
                email = form.get_profile_email()
                if email:
                    profile = get_profile_from_email(email)
                    if profile:
                        safe_email = quote(email)
                        url = reverse('competition-admin:make_jury_members')
                        return redirect("{0}?email={1}".format(url, safe_email))

        else:
            form = CreateJuryMemberForm()

        params["form"] = form
        return render(request, 'admin/add_jury_member.html', params)

    def make_jury_member(self, request):
        params = {}

        if request.method == 'POST':  # If the form has been submitted...
            form = MakeJuryMemberForm(request.POST)
            if form.is_valid():  # All validation rules pass
                profile = form.cleaned_data["profile_email"]
                user = profile.user

                # Set staff status
                user.is_staff = True
                user.save()

                # Create jury member
                try:
                    jury_member = JuryMember.objects.get(user=user)
                except JuryMember.DoesNotExist:
                    jury_member = JuryMember()
                    jury_member.user = user
                    jury_member.save()

                # Associate jury member to current competition
                active_competition = self.get_active_competition(request)
                queryset = JuryMemberCompetition.objects.filter(
                    jury_member=jury_member, competition=active_competition
                )
                if not queryset.exists():
                    competition_jury_member = JuryMemberCompetition()
                    competition_jury_member.jury_member = jury_member
                    competition_jury_member.competition = active_competition
                    competition_jury_member.save()

                # Redirect to jury member list
                return redirect(reverse('competition-admin:show_jury_members'))
        else:
            initial = {
                'profile_email': request.GET.get('email', ''),
            }
            form = MakeJuryMemberForm(initial=initial)

        params["form"] = form
        return render(request, 'admin/make_jury_member.html', params)

    def follow_results_single_note(self, request, step_url):
        try:
            return self.do_follow_results_single_note(request, step_url)
        except (RuntimeError, ObjectDoesNotExist) as exc:
            return render(request, 'admin/runtime_error.html', {'exc': exc})

    def do_follow_results_single_note(self, request, step_url):

        params = {}
        errors = []

        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        if request.method == 'POST':  # If the form has been submitted...

            if "run_action" in request.POST:
                # Run selected action
                # SHOULD BE IMPROVED TO BE MORE DYNAMIC
                action = request.POST["action"]
                referer = "."
                selected = request.POST.getlist("_candidate")
                if len(selected) > 0:
                    if action == "manage_groups_for_candidates":
                        return redirect("../../../candidates/manage_groups?ids=%s&referer=%s" % (
                            ",".join(selected), referer)
                        )
                    elif action == "manage_steps":
                        return redirect(
                            "../../../candidates/manage_steps?ids=%s&referer=%s" % (",".join(selected), referer)
                        )
                    else:
                        raise RuntimeError("Unhandled action")
                else:
                    raise RuntimeError("No candidates have been selected")
            else:
                # Save note changes
                evaluations = Evaluation.objects.filter(competition_step=active_step,status__url="completed")
                existing_notes = {}
                for evaluation in evaluations:
                    evaluation_note = evaluation.get_unique_note()
                    existing_notes[evaluation_note.id] = evaluation_note.value
                # Get posted notes
                posted_notes = {}
                for key in list(request.POST.keys()):
                    if key.startswith("note_"):
                        id = int(key[5:])
                        val = request.POST[key]
                        # If we need validation, we must write it here !
                        # Example :
                        # if not validate(val):
                        #    errors.append(key)
                        posted_notes[id] = val
                # Apply changes
                if not errors:
                    changed_notes = {}
                    for key,val in list(posted_notes.items()):
                        if existing_notes[key] != val:
                            changed_notes[key] = val
                            evaluation_note = EvaluationNote.objects.get(id=key)
                            evaluation_note.set_value(val)
                    # Display summary
                    params["changed_notes"] = EvaluationNote.objects.filter(pk__in=list(changed_notes.keys()))
                    qs_args = request.GET.copy()
                    if "edit" in qs_args:
                        del qs_args["edit"]
                    params["list_url"] = "?%s" % "&".join(["%s=%s"%(key,val) for key,val in list(qs_args.items())])
                    return render(request, 'admin/change_results_done.html',params)

        edit_mode = request.GET.__contains__("edit")
        params["edit_mode"] = edit_mode
        # Handle sorting
        sorted_col_index = -1
        sort_direction = None
        if request.GET.__contains__("o"):
            sorted_col_index = int(request.GET["o"])
            sort_direction = request.GET["ot"]
            params["sorted_col_index"] = sorted_col_index
            params["sort_direction"] = sort_direction
        #
        # Construct columns
        #
        columns = ['Last name', 'First name', 'ID']
        EXPORT_EXTRA_COLUMNS = ['Gender', 'Birth date', 'Age', 'Citizenship']
        if request.GET.__contains__("export"):
            columns.extend(EXPORT_EXTRA_COLUMNS)
        columns.append("Groups")
        # Add a column for each jury member
        jury_members = active_step.get_jury_members()
        for jury_member in jury_members:
            if jury_member.user.first_name:
                first_name_letter = jury_member.user.first_name[0].upper() + ". "
            else:
                first_name_letter = ''
            columns.append("%s%s" % (first_name_letter, jury_member.user.last_name))
        columns.append('Choices')
        columns.append('Recommendations')
        columns.append('Candidate notified')
        ####
        ### SPECIFIC (to be improved / should be dynamic)
        ####
        if active_step.competition.url == "ircam-cursus1":
            if active_step.url == "pre-jury":
                columns.append("Average")
            if active_step.url == "jury":
                columns.append("Average")
        ####
        ### END - SPECIFIC
        ####
        #
        # Construct lines
        #
        lines = []

        selected_candidate_group = None
        selected_attr = None
        selected_reco = None

        candidate_filters = {}
        if "cg" in request.GET:
            selected_candidate_group = int(request.GET["cg"])
            candidate_filters["groups__id"] = selected_candidate_group

        if "attr" in request.GET:
            selected_attr = int(request.GET["attr"])
            candidate_filters["attributes__id"] = selected_attr

        if "reco" in request.GET:
            selected_reco = int(request.GET["reco"])
            candidate_filters["evaluation__recommendation"] = selected_reco

        for candidate in active_step.get_candidates(candidate_filters, order_by="composer__user__last_name"):
            cells = []

            profile = get_profile(candidate.composer.user)
            gender = getattr(profile, 'gender', None)
            birth_date = getattr(profile, 'birth_date', None)
            citizenship = getattr(profile, 'citizenship', None)

            cells.append({'value': candidate.composer.user.last_name})
            cells.append({'value': candidate.composer.user.first_name})
            cells.append({'value': candidate.id})

            if request.GET.__contains__("export"):
                composer_gender = ""
                if gender:
                    composer_gender = gender.name
                cells.append({'value': composer_gender })
                cells.append({'value': birth_date.strftime("%d/%m/%Y") if birth_date else ''})
                cells.append({'value': get_age(birth_date) if birth_date else ''})
                cells.append({'value': citizenship.name if citizenship else ''})
            cells.append({'value': ", ".join([group.name for group in candidate.groups.all()])})
            for jury_member in jury_members:
                evaluations = Evaluation.objects.filter(
                    competition_step=active_step, candidate=candidate, jury_member=jury_member
                )
                item = {}
                if len(evaluations) == 1:
                    evaluation = evaluations[0]
                    if evaluation.status.url == "completed":
                        result = evaluation.get_unique_note().value
                    elif evaluation.status.url == "in_progress":
                        result = "in progress"
                    elif evaluation.status.url == "to_process":
                        result = "to process"
                    else:
                        raise RuntimeError("Unhandled evaluation status : s%" % evaluation.status.url)
                    item["value"] = result
                    item["editable"] = True
                    item["eval_key"] = "note_%s" % (evaluation.id)
                    item["completed"] = (evaluation.status.url=="completed")
                    item["class"] = "evaluation %s" % evaluation.status.url
                    if item["eval_key"] in errors:
                        item["class"] += " error"
                    # If evaluation is complete, prepare edit_html
                    # (we render the corresponding evaluation field widget)
                    if evaluation.status.url == "completed":
                        ef = evaluation.get_unique_evaluation_field()
                        ev = EvaluationNote.objects.get(evaluation=evaluation,key=ef.key)
                        item["edit_html"] = ev.get_edit_html()
                else:
                    item["value"] = ""
                    item["class"] = "empty"
                cells.append(item)

            cells.append({
                'value': candidate.filters_str()
            })

            cells.append({
                'value': candidate.recommendations_str()
            })

            cells.append({
                'value': 'yes' if CandidateNotification.objects.filter(
                    candidate=candidate, competition_step=active_step, is_test=False
                ).exists() else 'no'
            })

            ####
            ### SPECIFIC (to be improved / should be dynamic)
            ####
            if active_step.competition.url == "ircam-cursus1":
                if active_step.url == "pre-jury":
                    item = {}
                    # Compute average (No = 0, Maybe = 0.5, Yes = 1)
                    evaluations = Evaluation.objects.filter(competition_step=active_step,candidate=candidate)
                    average = ""
                    try:
                        cpt = 0
                        sum = 0
                        for ev in evaluations:
                            ev = evaluation.get_unique_note().value
                            if ev == "Yes":
                                sum += 20
                            elif ev == "Maybe":
                                sum += 10
                            elif ev == "No":
                                sum += 0
                            else:
                                raise RuntimeError("Unhandled case")
                            cpt += 1
                        average = "%.2f" % (sum / cpt)
                    except:
                        average = "N/A"
                    item["value"] = average
                    item["class"] = "average"
                    cells.append(item)
                if active_step.url == "jury":
                    item = {}
                    # Compute average
                    evaluations = Evaluation.objects.filter(competition_step=active_step,candidate=candidate)
                    average = ""
                    try:
                        cpt = 0
                        sum = 0
                        for ev in evaluations:
                            sum += float(ev.get_unique_note().value)
                            cpt += 1
                        average = "%.2f" % (sum / cpt)
                    except:
                        average = "N/A"
                    item["value"] = average
                    item["class"] = "average"
                    cells.append(item)

            ####
            ### END - SPECIFIC
            ####
            line = {}
            line["candidate_id"] = candidate.id
            line["cells"] = cells
            lines.append(line)

        #
        # Sort lines (if applicable)
        #
        def sort_col_func(line, col_index):
            # Try to sort by numeric value. Some of them might be strings
            value = line["cells"][col_index]["value"]
            try:
                int_value = int(value)
                return '{0:06}'.format(int_value)
            except ValueError:
                if isinstance(value, str):
                    return value.lower()
                else:
                    return str(value).lower()

        if sorted_col_index > 0:
            if request.GET.__contains__("export") and sorted_col_index >= 3:
                sorted_col_index += len(EXPORT_EXTRA_COLUMNS)
            lines = sorted(
                lines,
                key=lambda line: sort_col_func(line, sorted_col_index),
                reverse=(sort_direction == "desc")
            )

        # Prepare candidate groups filter
        qs = {}
        if "ot" in request.GET:
            qs["ot"] = request.GET["ot"]
        if "o" in request.GET:
            qs["o"] = request.GET["o"]
        if "edit" in request.GET:
            qs["edit"] = request.GET["edit"]
        #default_url = ["?%s=%s" % (key, value) for (key, value) in qs.items()]
        default_url = "".join(["?%s=%s" % (key, value) for (key, value) in list(qs.items())])

        for key_word in ('cg', 'reco', 'attr'):
            if key_word in request.GET:
                qs[key_word] = request.GET[key_word]

        candidate_groups = []
        candidate_groups.append(
            {"url": default_url, "name": "------------", "selected": (selected_candidate_group == None)}
        )
        for obj in CandidateGroup.objects.filter(competition=active_step.competition):
            keys = qs.copy()
            keys['cg'] = obj.id
            url = "?%s" % "&".join(["%s=%s" % (key, value) for (key, value) in list(keys.items())])
            candidate_groups.append({"url": url, "name": obj.name, "selected": (obj.id == selected_candidate_group)})
        params["candidate_groups"] = candidate_groups

        filter_attrs = CompetitionAttributeValue.objects.filter(
            attribute__competition=active_step.competition,
            is_filter=True
        )

        for key_word in ('reco', 'attr'):
            selected_value = locals().get('selected_'+key_word, None)
            select_ctrl = [
                {"url": default_url, "name": "------------", "selected": (selected_value == None)}
            ]
            for obj in filter_attrs:
                keys = qs.copy()
                keys[key_word] = obj.id
                url = "?%s" % "&".join(["%s=%s" % (key, value) for (key, value) in list(keys.items())])
                select_ctrl.append(
                    {"url": url, "name": obj.value, "selected": (obj.id == selected_value)}
                )
            params["select_"+key_word] = select_ctrl

        params["columns"] = columns
        params["lines"] = lines
        params["active_step"] = active_step
        if selected_candidate_group:
            params["candidate_group"] = selected_candidate_group

        qs_args = {}
        qs_args["edit"] = 1
        if "ot" in request.GET:
            qs_args["ot"] = request.GET["ot"]
        if "o" in request.GET:
            qs_args["o"] = request.GET["o"]
        if "cg" in request.GET:
            qs_args["cg"] = request.GET["cg"]
        params["edit_url"] = "?{0}".format("&".join(["{0}={1}".format(key, val) for key, val in list(qs_args.items())]))
        params["export_url"] = "?export&{0}".format(
            "&".join(["{0}={1}".format(key, val) for key, val in list(qs_args.items())])
        )

        if "export" in request.GET:
            wb = get_excel_workbook(columns, lines)
            return get_excel_response(wb)

        if errors:
            params["error_message"] = "Note should be integers."

        return render(request, 'admin/follow_results_single_note.html', params)

    def show_evaluations(self, request):
        params = {
            'has_permission': self.has_permission(request),
        }
        return render(request, 'admin/evaluations.html', params)

    def follow_results_multi_notes(self, request, step_url):
        context = {}
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        context["active_step"] = active_step
        candidate_proxy_model = get_candidate_in_step_proxy_model_class(active_step)

        # As a security, we force the manager, even if it is declared in the class
        # If we don't, a bugs occurs when switch between competitions (candidate mismatch !)
        mgr = get_candidate_in_step_manager(active_step)()
        mgr.contribute_to_class(candidate_proxy_model, "objects")
        candidate_proxy_model._meta.default_manager = mgr
        candidate_step_results_admin_class = get_candidate_step_results_admin(active_step, True)
        return candidate_step_results_admin_class(candidate_proxy_model, self).changelist_view(request, context)

    def follow_results(self, request, step_url):
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        result_list_field_count = EvaluationField.objects.filter(
            competition_step=active_step, display_in_result_list=True
        ).count()

        if result_list_field_count > 1:
            return self.follow_results_multi_notes(request, step_url)
        elif result_list_field_count == 1:
            return self.follow_results_single_note(request, step_url)
        else:
            return self.follow_results_multi_notes(request, step_url)

    def show_candidate_results(self, request, step_url, id):
        """
        Shows the detailed evaluations of the specified candidate
        for the specified competition step in active competition
        """
        params = {}
        active_step = self.get_active_competition_step(request, step_url)
        if not active_step:
            return HttpResponseRedirect(reverse('competition-admin:index'))

        candidate = Candidate.objects.get(id=id)
        params["active_step"] = active_step
        params["candidate"] = candidate
        edit_mode = "edit" in request.GET

        if request.method == 'POST': # If the form has been submitted...
            # Save changes
            for key in list(request.POST.keys()):
                if key.startswith("note_"):
                    val = request.POST[key]
                    id = int(key[5:])
                    evaluation_note = EvaluationNote.objects.get(id=id)
                    evaluation_note.set_value(val)
            # Switch off edit mode
            edit_mode = False

        params["evaluations"] = Evaluation.objects.filter(
            competition_step=active_step, candidate=candidate
        )
        params["edit_mode"] = edit_mode

        return render(request, 'admin/candidate_results.html', params)

    def manage_steps_for_candidates(self,request):
        params = {}
        ids = get_safe_ids(request)
        candidates = Candidate.objects.filter(id__in=ids)
        referer = request.GET["referer"]
        params["candidates"] = candidates
        errors = []

        if request.method == 'POST':  # If the form has been submitted...
            new_group = None
            action_choice = None
            if request.POST.__contains__("action_choice"):
                action_choice = int(request.POST["action_choice"])
                if action_choice == 1:
                    # Add to selected step
                    if request.POST.__contains__("step_add"):
                        step_add = request.POST["step_add"]
                    if not step_add:
                        errors.append(_("Please select a step"))
                elif action_choice == 2:
                    # Remove from selected step
                    if request.POST.__contains__("step_remove"):
                        step_remove = request.POST["step_remove"]
                    if not step_remove:
                        errors.append(_("Please select a step"))
            else:
                errors.append("Please select a choice")
            if not errors:
                competition = self.get_active_competition(request)
                if action_choice == 1:
                    # Add selected candidates to step
                    step = CompetitionStep.objects.filter(competition=competition, name=step_add)[0]
                    for candidate in candidates:
                        if step not in candidate.steps.all():
                            candidate.steps.add(step)
                            candidate.save()
                elif action_choice == 2:
                    # Remove selected candidates from step
                    step = CompetitionStep.objects.filter(competition=competition, name=step_remove)[0]
                    for candidate in candidates:
                        if step in candidate.steps.all():
                            candidate.steps.remove(step)
                            candidate.save()
                else:
                    raise RuntimeError("Unhandled case")
                return redirect(referer)

        return render(request, 'admin/manage_candidate_steps.html', params)

    def remove_candidate_from_competition(self, request):
        params = {}
        ids = get_safe_ids(request)
        candidates = Candidate.objects.filter(id__in=ids)
        referer = request.GET["referer"]
        params["candidates"] = candidates
        errors = []

        if request.method == 'POST':  # If the form has been submitted...
            if 'yes-do-it' in request.POST:
                competition = self.get_active_competition(request)
                if not request.user.is_superuser:
                    manager = get_object_or_404(CompetitionManager, user=request.user)
                    if manager not in competition.managed_by.all():
                        raise PermissionDenied

                for candidate in candidates:
                    candidate.delete()

                return redirect(referer)

        return render(request, 'admin/remove_candidate_from_competition.html', params)

    def manage_steps_for_jury_members(self,request):
        params = {}
        ids = get_safe_ids(request)
        jury_members = JuryMember.objects.filter(id__in=ids)
        referer = request.GET["referer"]
        params["jury_members"] = jury_members
        errors = []

        if request.method == 'POST': # If the form has been submitted...
            new_group= None
            action_choice = None
            if request.POST.__contains__("action_choice"):
                action_choice = int(request.POST["action_choice"])
                if action_choice == 1:
                    # Add to selected step
                    if request.POST.__contains__("step_add"):
                        step_add = request.POST["step_add"]
                    if not step_add:
                        errors.append(_("Please select a step"))

                elif action_choice == 2:
                    # Remove from selected step
                    if request.POST.__contains__("step_remove"):
                        step_remove = request.POST["step_remove"]
                    if not step_remove:
                        errors.append(_("Please select a step"))
            else:
                errors.append("Please select a choice")
            if not errors:
                if action_choice == 1:
                    # Add selected jury members to step
                    competition = self.get_active_competition(request)
                    step = CompetitionStep.objects.filter(competition=competition, name=step_add)[0]
                    for jury_member in jury_members:
                        jmc_exists = JuryMemberCompetition.objects.filter(
                            competition=competition, jury_member=jury_member
                        ).exists()
                        if not jmc_exists:
                            jmc = JuryMemberCompetition()
                            jmc.jury_member = jury_member
                            jmc.competition = competition
                            jmc.save()

                        if step not in jury_member.steps.all():
                            jury_member.steps.add(step)
                            jury_member.save()

                elif action_choice == 2:
                    # Remove selected jury_members from step
                    step = CompetitionStep.objects.filter(
                        competition=self.get_active_competition(request),name=step_remove
                    )[0]
                    for jury_member in jury_members:
                        if step in jury_member.steps.all():
                            jury_member.steps.remove(step)
                            jury_member.save()
                        if jury_member.steps.filter(competition=step.competition).count() == 0:
                            JuryMemberCompetition.objects.filter(
                                competition=step.competition, jury_member=jury_member
                            ).delete()
                else:
                    raise RuntimeError("Unhandled case")
                return redirect(referer)

        return render(request, 'admin/manage_jury_members_steps.html', params)

    def manage_groups_for_jury_members(self,request):
        params = {}
        ids = get_safe_ids(request)
        jury_members = JuryMember.objects.filter(id__in=ids)
        referer = request.GET["referer"]
        params["jury_members"] = jury_members
        active_competition = self.get_active_competition(request)
        if not active_competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))
        params["groups"] = JuryMemberGroup.objects.filter(competition=active_competition)
        errors = []
        if request.method == 'POST': # If the form has been submitted...
            new_group= None
            existing_group = None
            action_choice = None
            if request.POST.__contains__("action_choice"):
                action_choice = int(request.POST["action_choice"])
                if action_choice == 1:
                    # Create new group
                    if request.POST.__contains__("new_group"):
                        new_group = request.POST["new_group"]
                    if not new_group:
                        errors.append(_("Please specify a name for the new group"))
                elif action_choice == 2:
                    # Add to existing group
                    if request.POST.__contains__("existing_group_add"):
                        existing_group_add = request.POST["existing_group_add"]
                    if not existing_group_add:
                        errors.append(_("Please select an existing group"))
                elif action_choice == 3:
                    # Add to existing group
                    if request.POST.__contains__("existing_group_remove"):
                        existing_group_remove = request.POST["existing_group_remove"]
                    if not existing_group_remove:
                        errors.append(_("Please select an existing group"))
                else:
                    raise RuntimeError("Unhandled case")
            else:
                errors.append(_("Please select an action to perform"))
            if not errors:
                # Add to jury member to groups and redirect to referer
                the_group = None
                if action_choice == 1:
                    the_group = JuryMemberGroup()
                    the_group.name = new_group
                    the_group.competition = active_competition
                    the_group.save()
                    for jury_member in jury_members:
                        if the_group not in jury_member.groups.all():
                            jury_member.groups.add(the_group)
                            jury_member.save()
                elif action_choice == 2:
                    the_group = JuryMemberGroup.objects.filter(
                        competition=active_competition, name=existing_group_add
                    )[0]
                    # Add the group to jury members
                    for jury_member in jury_members:
                        if the_group not in jury_member.groups.all():
                            jury_member.groups.add(the_group)
                            jury_member.save()
                elif action_choice == 3:
                    the_group = JuryMemberGroup.objects.filter(
                        competition=active_competition, name=existing_group_remove
                    )[0]
                    # Add the group to candidates
                    for jury_member in jury_members:
                        if the_group in jury_member.groups.all():
                            jury_member.groups.remove(the_group)
                            jury_member.save()
                else:
                    raise RuntimeError("Unhandled case")
                return redirect(referer)
        params["errors"] = errors
        return render(request, 'admin/manage_jury_groups.html', params)

    def edit_candidate_document(self, request, id):
        params = {}
        candidate = Candidate.objects.get(id=id)

        id_key = request.GET.get("key", None)
        if id_key:
            key = request.GET["key"][len("id_"):]
        else:
            key = request.POST.get("key", None)

        if not key:
            raise Http404

        field_definition = None
        if candidate.competition.dynamic_application_form:
            field_class = ApplicationFormFieldDefinition
            try:
                field_definition = field_class.objects.get(
                    parent__parent=candidate.competition.dynamic_application_form, name=key
                )
            except (field_class.DoesNotExist, field_class.MultipleObjectsReturned) as err:
                pass

        if CandidateDocument.objects.filter(candidate=candidate, key=key).exists():
            # We are editing an existing instance
            instance = CandidateDocument.objects.get(candidate=candidate, key=key)
            edit_mode = True
        else:
            # We are adding a new document
            instance = CandidateDocument(candidate=candidate, key=key)
            edit_mode = False
            initial = {'key': key}
        candidate_document_form_class = get_candidate_document_modelform(candidate, field_definition)
        if request.method == 'POST':
            form = candidate_document_form_class(request.POST, instance=instance)
            if form.is_valid():
                file = form.cleaned_data.get('file')

                if edit_mode and not file:
                    instance.delete()
                else:
                    form.instance.is_admin = True
                    if not edit_mode:
                        form.instance.candidate = candidate
                    form.save()

                    # update extra_files
                    for index in range(field_definition.extra_files):
                        field_name = 'extra_file_{0}'.format(index)
                        extra_file = form.extra_files[index] if index < len(form.extra_files) else None
                        extra_file_name = form.cleaned_data.get(field_name)
                        if extra_file:
                            if not extra_file_name:
                                extra_file.delete()
                            else:
                                extra_file.file = extra_file_name
                                extra_file.save()
                        else:
                            if extra_file_name:
                                CandidateDocumentExtraFile.objects.create(document=instance, file=extra_file_name)

                    return redirect(reverse('competition-admin:edit_candidate', args=[id]))
        else:
            if edit_mode:
                form = candidate_document_form_class(instance=instance)
            else:
                form = candidate_document_form_class(initial=initial)
        params["candidate"] = candidate
        params["form"] = form
        params["key"] = key

        return render(request, 'admin/edit_candidate_document.html', params)

    def edit_candidate_biographic_element(self, request, id):
        params = {}
        candidate = Candidate.objects.get(id=id)
        key = request.GET["key"][len("id")+1:]
        if CandidateBiographicElement.objects.filter(candidate=candidate, key=key).exists():
            # We are editing an existing instance
            instance = CandidateBiographicElement.objects.get(candidate=candidate, key=key)
            edit_mode = True
        else:
            # We are adding a new document
            instance = CandidateBiographicElement(candidate=candidate, key=key)
            edit_mode = False
            initial = {'key': key}

        candidate_biographic_element_form = get_candidate_biographic_element_modelform(candidate, edit_mode)
        if request.method == 'POST':
            form = candidate_biographic_element_form(request.POST, instance=instance)
            if form.is_valid():
                form.instance.is_admin = True
                if not edit_mode:
                    form.instance.candidate = candidate
                form.save()
                return redirect(reverse('competition-admin:edit_candidate', args=[id]))
        else:
            if edit_mode:
                form = candidate_biographic_element_form(instance=instance)
            else:
                form = candidate_biographic_element_form(initial=initial)
        params["candidate"] = candidate
        params["form"] = form
        return render(request, 'admin/edit_candidate_biographic_element.html', params)

    def edit_candidate_media(self, request, id):
        params = {}
        candidate = Candidate.objects.get(id=id)
        if not 'key' in request.GET:
            raise Http404
        key = request.GET["key"][len("id") + 1:]
        if CandidateMedia.objects.filter(candidate=candidate, key=key).exists():
            # We are editing an existing instance
            instance = CandidateMedia.objects.get(candidate=candidate, key=key)
            edit_mode = True
        else:
            # We are adding a new media
            instance = CandidateMedia(candidate=candidate, key=key)
            edit_mode = False
            initial = {'key': key}

        field_type = ''
        # Try to get the type of field
        if candidate.competition.dynamic_application_form:
            field_class = ApplicationFormFieldDefinition
            try:
                field_def = field_class.objects.get(
                    parent__parent=candidate.competition.dynamic_application_form, name=key
                )
                field_type = field_def.kind.name
            except (field_class.DoesNotExist, field_class.MultipleObjectsReturned) as err:
                pass

        candidate_media_form_base_class = get_candidate_media_modelform(candidate, edit_mode)

        class candidate_media_form_class(candidate_media_form_base_class):
            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                self.field_type = field_type
                patch_media_fields(self.fields, field_type)

            def clean_link(self):
                link = self.cleaned_data['link']
                if link and self.field_type in ('VideoYouTubeField', 'YouTubeField'):
                    if not get_youtube_code(link):
                        raise ValidationError(_('This is not a valid YouTube url'))
                return link

            def clean_audio(self):
                audio = self.cleaned_data['audio']
                if audio:
                    if os.path.splitext(audio)[-1].lower() == '.mp3':
                        target_dir = get_candidate_folder(candidate)
                        full_path = os.path.join(settings.MEDIA_ROOT, target_dir, audio)
                        if not is_mp3_valid(full_path):
                            raise ValidationError(_('This is not a valid mp3'))
                return self._clean_file('audio')

        if request.method == 'POST':
            form = candidate_media_form_class(request.POST, instance=instance)
            if form.is_valid():
                title = form.cleaned_data.get('title')
                if edit_mode and not title:
                    instance.delete()
                else:
                    form.instance.is_admin = True
                    if not edit_mode:
                        form.instance.candidate = candidate
                    form.save()
                return redirect(reverse('competition-admin:edit_candidate', args=[id]))
        else:
            if edit_mode:
                form = candidate_media_form_class(instance=instance)
            else:
                form = candidate_media_form_class(initial=initial)
        params["candidate"] = candidate
        params["form"] = form
        return render(request, 'admin/edit_candidate_media.html', params)

    def manage_candidates_to_competition_step(self, request):
        competition = self.get_active_competition(request)
        if not competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))
        params = {}
        ids = get_safe_ids(request)
        candidates = Candidate.objects.filter(competition=competition, id__in=ids)
        params["candidates"] = candidates

        if request.method == "POST":
            form = AddRemoveCompetitionStepForm(competition, request.POST)
            if form.is_valid():
                if form.cleaned_data['add_or_remove'] == "add":
                    competition_step = form.cleaned_data['competition_step']
                    for candidate in candidates:
                        if competition_step not in candidate.steps.all():
                            candidate.steps.add(competition_step)
                            candidate.save()
                            if competition_step.notify_association_by_email and competition_step.notification_email:
                                send_mail(
                                    competition_step.competition.name,
                                    competition_step.notification_email,
                                    competition_step.competition.email,
                                    [candidate.composer.user.email]
                                )

                elif form.cleaned_data['add_or_remove'] == "remove":
                    competition_step = form.cleaned_data['competition_step']
                    for candidate in candidates:
                        if competition_step in candidate.steps.all():
                            candidate.steps.remove(competition_step)
                            candidate.save()
                return redirect(reverse('competition-admin:step_manage_allocations', args=[competition_step.url]))
        else:
            form = AddRemoveCompetitionStepForm(competition)

        params["competition"] = competition
        params["form"] = form

        return render(request, 'admin/manage_candidates_to_competition_step.html', params)

    def manage_jury_members_to_competition_step(self, request):
        competition = self.get_active_competition(request)
        if not competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))
        params = {}
        ids = get_safe_ids(request)
        jury_members = JuryMember.objects.filter(id__in=ids)
        params["jury_members"] = jury_members

        if request.method == "POST":
            form = AddRemoveCompetitionStepForm(competition, request.POST)
            if form.is_valid():
                competition_step = form.cleaned_data['competition_step']

                if form.cleaned_data['add_or_remove'] == "add":
                    for jury_member in jury_members:

                        competition_jury_members = JuryMemberCompetition.objects.filter(
                            competition=competition, jury_member=jury_member
                        )
                        if not competition_jury_members.exists():
                            JuryMemberCompetition.objects.create(
                                jury_member=jury_member,
                                competition=competition
                            )
                        if competition_step not in jury_member.steps.all():
                            jury_member.steps.add(competition_step)
                            jury_member.save()

                elif form.cleaned_data['add_or_remove'] == "remove":

                    for jury_member in jury_members:
                        if competition_step in jury_member.steps.all():
                            jury_member.steps.remove(competition_step)
                            jury_member.save()
                        if jury_member.steps.filter(competition=competition_step.competition).count() == 0:
                            JuryMemberCompetition.objects.filter(
                                competition=competition_step.competition, jury_member=jury_member
                            ).delete()

                return redirect(reverse('competition-admin:step_manage_allocations', args=[competition_step.url]))
        else:
            form = AddRemoveCompetitionStepForm(competition)

        params["competition"] = competition
        params["form"] = form

        return render(request, 'admin/manage_jury_members_to_competition_step.html', params)

    def manage_groups_for_candidates(self,request):
        params = {}
        ids = get_safe_ids(request)
        competition = self.get_active_competition(request)
        if not competition:
            return HttpResponseRedirect(reverse('competition-admin:index'))
        candidates = Candidate.objects.filter(id__in=ids)
        referer = request.GET["referer"]
        params["candidates"] = candidates
        params["groups"] = CandidateGroup.objects.filter(competition=competition)
        errors = []
        if request.method == 'POST':  # If the form has been submitted...
            new_group= None
            existing_group = None
            action_choice = None
            if request.POST.__contains__("action_choice"):
                action_choice = int(request.POST["action_choice"])
                if action_choice==1:
                    # Create new group
                    if request.POST.__contains__("new_group"):
                        new_group = request.POST["new_group"]
                    if not new_group:
                        errors.append(_("Please specify a name for the new group"))
                elif action_choice==2:
                    # Add to existing group
                    if request.POST.__contains__("existing_group_add"):
                        existing_group_add = request.POST["existing_group_add"]
                    if not existing_group_add:
                        errors.append(_("Please select an existing group"))
                elif action_choice==3:
                    # Add to existing group
                    if request.POST.__contains__("existing_group_remove"):
                        existing_group_remove = request.POST["existing_group_remove"]
                    if not existing_group_remove:
                        errors.append(_("Please select an existing group"))
                else:
                    raise RuntimeError("Unhandled case")
            else:
                errors.append(_("Please select an action to perform"))
            if not errors:
                # Add to candidates to groups and redirect to referer
                the_group = None
                if action_choice == 1:
                    the_group = CandidateGroup()
                    the_group.name = new_group
                    the_group.competition = competition
                    the_group.save()
                    for candidate in candidates:
                        if the_group not in candidate.groups.all():
                            candidate.groups.add(the_group)
                            candidate.save()
                elif action_choice == 2:
                    the_group = CandidateGroup.objects.filter(
                        competition=competition, name=existing_group_add
                    )[0]
                    # Add the group to candidates
                    for candidate in candidates:
                        if the_group not in candidate.groups.all():
                            candidate.groups.add(the_group)
                            candidate.save()
                elif action_choice == 3:
                    the_group = CandidateGroup.objects.filter(
                        competition=competition, name=existing_group_remove
                    )[0]
                    # Add the group to candidates
                    for candidate in candidates:
                        if the_group in candidate.groups.all():
                            candidate.groups.remove(the_group)
                            candidate.save()
                else:
                    raise RuntimeError("Unhandled case")
                return redirect(referer)
        params["errors"] = errors
        return render(request, 'admin/manage_candidate_groups.html',params)

    def show_step(self, request, step_url):
        # Redirect to first tab
        # TODO
        return redirect('%s/allocations/' % request.META["PATH_INFO"])

    def get_urls(self):

        urls = super(CompetitionAdminSite, self).get_urls()

        my_urls = [
            url(r'^admin', super(CompetitionAdminSite, self).index),
            # This url doesn't use competition_admin_view wrapper
            url(r'^select_competition/(?P<id>.*)', self.select_competition, name='select_competition'),
            # The following urls use competition_admin_view wrapper (they need an active competition)
            url(r'^test', self.show_test),
            # Infos
            url(r'^infos/', self.show_informations, name="show_informations"),
            # Candidates
            url(
                r'^candidates/manage_candidates_to_competition_step',
                self.manage_candidates_to_competition_step,
                name="manage_candidates_to_competition_step",
            ),
            url(r'^candidates/manage_groups', self.manage_groups_for_candidates, name='manage_groups_candidates'),
            url(r'^candidates/manage_steps', self.manage_steps_for_candidates, name='manage_steps_candidates'),
            url(
                r'^candidates/remove_candidate_from_competition',
                self.remove_candidate_from_competition,
                name='remove_candidate_from_competition'
            ),
            url(
                r'^candidates/(?P<id>.+)/edit_document/',
                self.edit_candidate_document,
                name="edit_candidate_document"
            ),
            url(
                r'^candidates/(?P<id>.+)/edit_biographicelement/',
                self.edit_candidate_biographic_element,
                name="edit_candidate_biographic_element"
            ),
            url(
                r'^candidates/(?P<id>.+)/edit_media/',
                self.edit_candidate_media,
                name="edit_candidate_media"
            ),
            url(r'^candidates/(?P<id>.+)/$', self.edit_candidate, name="edit_candidate"),
            url(r'^candidates/', self.show_candidates, name="show_candidates"),
            url(r'^application_drafts/', self.show_application_drafts, name='show_application_drafts'),
            # Jury members
            url(
                r'^jury_members/manage_jury_members_to_competition_step',
                self.manage_jury_members_to_competition_step,
                name="manage_jury_members_to_competition_step",
            ),
            url(r'^jury_members/manage_groups', self.manage_groups_for_jury_members),
            url(r'^jury_members/manage_steps', self.manage_steps_for_jury_members, name='jury_manage_steps'),
            url(r'^jury_members/add', self.add_jury_member, name='add_jury_member'),
            url(r'^jury_members/make', self.make_jury_member, name='make_jury_members'),
            url(r'^jury_members/(?P<id>.+)/$', self.edit_jury_member),
            url(r'^jury_members/', self.show_jury_members, name='show_jury_members'),
            # Evaluations
            url(r'^evaluations/', self.show_evaluations, name='show_evaluations'),
            # Switch competition
            url(r'^switch/', self.switch_competition, name="switch_competition"),
        ]

        # Steps
        my_urls += [
            url(
                r'^step/(?P<step_url>.+)/allocations/associate_jury_members',
                self.associate_jury_members,
                name='step_associate_jury_members'
            ),
            url(
                r'^step/(?P<step_url>.+)/allocations/',
                self.manage_allocations,
                name='step_manage_allocations'
            ),
            url(
                r'^step/(?P<step_url>.+)/jury_members/(?P<id>.+)/$',
                self.show_competition_step_jury_member,
                name='step_jury_members'
            ),
            url(
                r'^step/(?P<step_url>.+)/notify_jury_members/$',
                self.notify_jury_members,
                name='step_notify_jury_members'
            ),
            url(
                r'^step/(?P<step_url>.+)/notify_candidates/$',
                self.notify_candidates,
                name='step_notify_candidates'
            ),
            url(
                r'^step/(?P<step_url>.+)/evaluations/',
                self.follow_evaluations,
                name='step_follow_evaluations'
            ),
            url(
                r'^step/(?P<step_url>.+)/settings/',
                self.step_settings,
                name='step_settings'
            ),
            url(
                r'^step/(?P<step_url>.+)/results/(?P<id>.+)/$',
                self.show_candidate_results,
                name='step_candidate_results'
            ),
            url(r'^step/(?P<step_url>.+)/results/', self.follow_results, name='step_follow_results'),
            # Don't move this up ! Beware of regex evaluation precedence...
            url(
                r'^step/(?P<step_url>.*)',
                self.show_step,
                name="competition_step"
            ),
        ]

        return my_urls + urls
