# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-06-27 10:06
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_reference', '0010_competitioncategory'),
        ('ulysses_competitions', '0040_competition_closed_message'),
    ]

    operations = [
        migrations.AddField(
            model_name='competition',
            name='category',
            field=models.ForeignKey(blank=True, default=None, help_text='Users, interested by this category, will received an email when the competition is published', null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.CompetitionCategory', verbose_name='category'),
        ),
    ]
