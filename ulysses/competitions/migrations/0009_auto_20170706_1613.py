# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-07-06 16:13


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_competitions','0008_auto_20170623_1746'),
    ]

    operations = [
        migrations.AlterField(
            model_name='competition',
            name='presentation',
            field=models.TextField(help_text='competition summary', verbose_name='summary'),
        ),
    ]
