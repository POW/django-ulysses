# -*- coding: utf-8 -*-

from datetime import datetime, date, timedelta, time

from django.db.models import Q
from django.utils.text import slugify

from .models import (
    Competition, CompetitionStep, EvaluationField, DynamicApplicationForm, ApplicationFormFieldSetDefinition,
    ApplicationFormFieldDefinition, CompetitionAttribute, CompetitionAttributeValue, CompetitionManager
)


def get_visible_competitions(user=None, allow_draft=False):
    """returns a query of open competitions"""
    public_status = ["published", "open", "jury", "closed"]
    draft_status = ["draft", "new"]
    queryset = Competition.objects.exclude(
        archived=True
    )
    if allow_draft or (user and user.is_superuser):
        # superuser can access  public and draft
        queryset = queryset.filter(status__name__in=public_status + draft_status)
    elif user and user.is_authenticated:
        # manager can access public and his own drafts
        queryset = queryset.filter(
            Q(status__name__in=public_status) |
            Q(status__name__in=draft_status, managed_by__user=user)
        )
    else:
        # others can access public
        queryset = queryset.filter(status__name__in=public_status)
    return queryset.order_by('order_index', 'publication_date')


def get_homepage_competitions():
    """These competitions are shown on homepage"""
    draft_status = "draft"
    queryset = Competition.objects.exclude(status__name=draft_status).filter(show_on_homepage=True)
    return queryset.order_by('order_index', 'publication_date')[:4]


def get_competition_unique_name(competition):
    counter = 1
    while True:
        title = competition.title + " " + str(counter)
        url = slugify(title)
        if not Competition.objects.filter(Q(title=title) | Q(url=url)).exists():
            return title
        counter += 1
        if counter > 100:
            break


def get_form_unique_name(form):
    counter = 1
    while True:
        name = form.name + " " + str(counter)
        if not DynamicApplicationForm.objects.filter(name=name).exists():
            return name
        counter += 1
        if counter > 100:
            break


def is_competition_admin(user, competition=None):
    if competition:
        return competition.managed_by.filter(user=user).exists()
    else:
        return CompetitionManager.objects.filter(user=user).exists()


def clone_competition(competition):
    """Clone a competition"""

    title = get_competition_unique_name(competition)
    if not title:
        return None

    form_name = None
    if competition.dynamic_application_form:
        form_name = get_form_unique_name(competition.dynamic_application_form)
        if not form_name:
            return None

    url = slugify(title)

    publication_date = datetime.combine(
        date.today(), competition.publication_date.time()
    ) + timedelta(days=1)

    opening_date = publication_date + (competition.opening_date - competition.publication_date)
    closing_date = publication_date + (competition.closing_date - competition.publication_date)
    result_date = publication_date + (competition.result_date - competition.publication_date)
    unpublish_date = publication_date + (competition.unpublish_date - competition.publication_date)

    clone = Competition.objects.create(
        url=url,
        title=title,
        email=competition.email,
        subtitle=competition.subtitle,
        presentation=competition.presentation,
        application_email_header=competition.application_email_header,
        jury_guidelines=competition.jury_guidelines,
        order_index=competition.order_index,
        organized_by=competition.organized_by,
        image=competition.image,
        small_image=competition.small_image,
        publication_date=publication_date,
        opening_date=opening_date,
        closing_date=closing_date,
        result_date=result_date,
        unpublish_date=unpublish_date,
        use_dynamic_form=competition.use_dynamic_form,
        display_candidate_name=competition.display_candidate_name,
        archived=False,
        application_form_class=competition.application_form_class,
        allow_access_to_jury_history=competition.allow_access_to_jury_history,
        show_on_homepage=False,
        is_external_call=competition.is_external_call,
        external_call_url=competition.external_call_url,
    )

    for partner in competition.partners.all():
        clone.partners.add(partner)

    for managed_by in competition.managed_by.all():
        clone.managed_by.add(managed_by)

    for profile_mandatory_field in competition.profile_mandatory_fields.all():
        clone.profile_mandatory_fields.add(profile_mandatory_field)

    clone.save()

    # clone Steps
    for step in competition.competitionstep_set.all():
        step_closing_date = datetime.combine(step.closing_date, time.min)
        closing_date = publication_date + (step_closing_date - competition.publication_date)
        step_clone = CompetitionStep.objects.create(
            competition=clone,
            name=step.name,
            url=step.url,
            order_index=step.order_index,
            is_open=False,
            closing_date=closing_date.date(),
            candidate_attribute_jury_filter=step.candidate_attribute_jury_filter,
            is_recommendation_required=step.is_recommendation_required,
            can_access_evaluations=step.can_access_evaluations
        )

        for field in step.evaluationfield_set.all():
            EvaluationField.objects.create(
                competition_step=step_clone,
                key=field.key,
                label=field.label,
                weight=field.weight,
                field_type=field.field_type,
                widget_type=field.widget_type,
                required=field.required,
                principal=field.principal,
                use_in_stats=field.use_in_stats,
                display_in_result_list=field.display_in_result_list,
                field_extra_args=field.field_extra_args,
                order_index=field.order_index,
                is_note=field.is_note
            )

    # clone dynamic_application_form
    if competition.dynamic_application_form:
        form_clone = DynamicApplicationForm.objects.create(
            name=form_name,
            clean_method=competition.dynamic_application_form.clean_method
        )
        for fieldset in competition.dynamic_application_form.applicationformfieldsetdefinition_set.all():
            fieldset_clone = ApplicationFormFieldSetDefinition.objects.create(
                parent=form_clone,
                name=fieldset.name,
                legend=fieldset.legend,
                description=fieldset.description,
                classes=fieldset.classes,
                order=fieldset.order
            )

            for field in fieldset.applicationformfielddefinition_set.all():
                if field.competition_attribute:
                    attr_clone = CompetitionAttribute.objects.create(key=field.name, competition=clone)
                    for attr_value in field.competition_attribute.competitionattributevalue_set.all():
                        CompetitionAttributeValue.objects.create(
                            attribute=attr_clone,
                            key=attr_value.key,
                            acronym=attr_value.acronym,
                            value=attr_value.value,
                            is_filter=attr_value.is_filter,
                            hide_recommendation=attr_value.hide_recommendation,
                            order=attr_value.order
                        )
                else:
                    attr_clone = None

                ApplicationFormFieldDefinition.objects.create(
                    parent=fieldset_clone,
                    kind=field.kind,
                    name=field.name,
                    label=field.label,
                    required=field.required,
                    help_text=field.help_text,
                    competition_attribute=attr_clone,
                    is_visible_by_jury=field.is_visible_by_jury,
                    grand_parent=form_clone,
                    maximum_length=field.maximum_length,
                    order=fieldset.order
                )

        clone.dynamic_application_form = form_clone
        clone.save()

    return clone
