
import datetime
import pickle
import os
from os import listdir
from os.path import isfile, join

from django.conf import settings
from django.db.models import Q

from ulysses.composers.models import Composer
from ulysses.generic.emails import send_email

from .fields import (
    ApplicationElementMultipleValueField, ApplicationTextElementField, ApplicationHtmlElementField,
    ApplicationBooleanElementField
)
from .models import (
    CompetitionAttributeValue, Candidate, CandidateBiographicElement, CandidateDocument, CandidateMedia,
    ApplicationElement, CandidateAttributeValue, TemporaryDocument, get_user_temporary_folder, TemporaryMedia,
    ApplicationDraft, DraftBiographicElement, DraftDocument, DraftMedia
)


def create_application(form, user, data, competition):
    try:
        # Create candidate
        defaults = {
            'competition': competition,
            'composer': Composer.get_from_user(user),
            'id_in_competition': competition.get_unique_candidate_id()
        }
        try:
            candidate = Candidate.objects.get(
                Q(competition=competition, composer=Composer.get_from_user(user)) |
                Q(id_in_competition=competition.get_unique_candidate_id(), competition=competition)
            )
            for key, value in list(defaults.items()):
                setattr(candidate, key, value)
            candidate.save()
        except Candidate.DoesNotExist:
            new_values = {'application_date': datetime.datetime.now()}
            new_values.update(defaults)
            candidate = Candidate(**new_values)
            candidate.save()
        
        application_summary = ""
        # Create biographic elements
        biographic_element_keys = form.get_biographic_element_keys()
        if biographic_element_keys:
            application_summary += "<div class='BiographicElements'>Biographic details</div>"
            for key in biographic_element_keys:
                key_val = data[key]
                if key_val:
                    biographic_element = CandidateBiographicElement()
                    biographic_element.key = key
                    biographic_element.candidate = candidate
                    biographic_element.title = form.get_field_definition_from_name(key).label
                    biographic_element.text = data[key]
                    biographic_element.save()
                    line1 = "<div class='BiographicElement'><span class='Title'>{0}</span>:".format(
                        biographic_element.title
                    )
                    line2 = "<span class='Text'>{0}</span></div>".format(biographic_element.text)
                    application_summary += line1 + line2
        # Duplicate documents
        document_keys = form.get_document_keys()
        if document_keys:
            application_summary += "<div class='Documents'>Documents</div>"
            for key in document_keys:
                key_val = data[form.get_hidden_key(key)]
                if key_val:
                    try:
                        temporary_document = TemporaryDocument.objects.get(id=key_val)
                    except TemporaryDocument.DoesNotExist:
                        raise Exception("TemporaryDocument.DoesNotExist. {0}: {1}".format(key, key_val))
                    document = temporary_document.move_as_candidate_element(candidate)
                    line1 = "<div class='Document'><span class='Title'>{0}</span> ".format(document.title)
                    line2 = "(<span class='FileName'>{0}</span>)</div>".format(document.file)
                    application_summary += line1 + line2
        # Duplicate medias
        media_keys = form.get_media_keys()
        if media_keys:
            application_summary += "<div class='Medias'>Medias</div>"
            for key in media_keys :
                key_val = data[form.get_hidden_key(key)]
                if key_val:
                    temporary_media = TemporaryMedia.objects.get(id=key_val)
                    media = temporary_media.move_as_candidate_element(candidate)
                    application_summary += "<div class='Media'><span class='Title'>%s</span></div>" % (media.title)
        # Create application elements
        elements_keys = form.get_elements_keys()
        if elements_keys:
            application_summary += "<div class='Elements'>Elements</div>"
            for key in elements_keys:
                element = ApplicationElement()
                element.candidate = candidate
                element.key = key
                field_definition = form.get_field_definition_from_name(key)
                element.label = field_definition.label
                element_str = ""
                element.multiple_values = False
                if type(field_definition) is ApplicationElementMultipleValueField:
                    value_list = data.getlist(key)
                    element.value = pickle.dumps(value_list)
                    element.multiple_values = True
                    element_str = "%s" % value_list
                elif type(field_definition) is ApplicationTextElementField:
                    element.value = data[key]
                    element_str = element.value
                elif type(field_definition) is ApplicationHtmlElementField:
                    element.value = data[key]
                    element_str = element.value
                elif type(field_definition) is ApplicationBooleanElementField:
                    element.value = data[key]
                    element_str = str(element.value)
                else:
                    raise RuntimeError("Unhandled case")
                element.save()
                application_summary += "<div class='Element'><span class='Label'>%s :</span><span class='Value'>%s</span></div>" % (element.label,element_str)
        # Create candidate attributes values
        for key in form.get_attribute_value_keys():
            field_definition = form.get_field_definition_from_name(key)
            competition_attribute_qs = CompetitionAttributeValue.objects.filter(
                attribute=field_definition.competition_attribute, key=data[key]
            )
            if competition_attribute_qs.exists():
                cav = CandidateAttributeValue()
                cav.candidate = candidate
                cav.attribute = key
                cav.value = CompetitionAttributeValue.objects.get(
                    attribute=field_definition.competition_attribute, key=data[key]
                )
                cav.save()
                line1 = "<div class='AttributeValue'><span class='Label'>{0} :</span>".format(field_definition.label)
                line2 = "<span class='Value'>{0}</span></div>".format(cav.value.value)
                application_summary += line1 + line2
        # Create candidate attributes multiple values
        for key in form.get_attribute_multiple_value_keys():
            field_definition = form.get_field_definition_from_name(key)
            value_list = data.getlist(key)
            cpt = 1
            for val in value_list:
                cav = CandidateAttributeValue()
                cav.candidate = candidate
                cav.attribute = "%s_%s" % (key, cpt)
                cav.value = CompetitionAttributeValue.objects.get(
                    attribute=field_definition.competition_attribute, key=val
                )
                cav.save()
                line1 = "<div class='AttributeValue'><span class='Label'>{0} :</span>".format(field_definition.label)
                line2 = "<span class='Value'>{0}</span></div>".format(cav.value.value)
                application_summary += line1 + line2
                cpt += 1
        # Save application summary
        candidate.application_summary = application_summary
        candidate.save()
        # Send confirmation mail

        subject = "Your application to {0}".format(competition.title)
        context_dict = {
            'candidate': candidate,
            'competition': competition,
        }

        cc_list = []
        sender = settings.CONTACT_EMAIL
        if competition.email:
            sender = competition.email
            cc_list.append(competition.email)

        send_email(
            subject,
            'emails/application_confirmation.html',
            context_dict,
            [candidate.composer.user.email],
            sender=sender,
            cc_list=cc_list
        )

        # Delete temporary documents for this competition and composer
        TemporaryDocument.objects.filter(composer=candidate.composer,competition=competition).delete()
        # Clean 'temp/<username>' directory
        user_temp_dir = get_user_temporary_folder(user.username)
        for f in listdir(user_temp_dir):
            full_path = join(user_temp_dir,f)
            if isfile(full_path):
                os.remove(full_path)
        # Delete application draft, if any
        if ApplicationDraft.objects.filter(composer=candidate.composer,competition=competition).exists():
            draft = ApplicationDraft.objects.get(composer=candidate.composer,competition=competition)
            DraftMedia.objects.filter(draft=draft).delete()
            DraftDocument.objects.filter(draft=draft).delete()
            DraftBiographicElement.objects.filter(draft=draft).delete()
            draft.delete()
    except:
        # If something wrong occurred, delete candidate & associates elements,
        # if any (we don't want to leave it in a intermediate state)
        if Candidate.objects.filter(competition=competition,composer=Composer.get_from_user(user)).exists():
            candidate = Candidate.objects.get(competition=competition,composer=Composer.get_from_user(user))
            CandidateDocument.objects.filter(candidate=candidate).delete()
            CandidateMedia.objects.filter(candidate=candidate).delete()
            CandidateBiographicElement.objects.filter(candidate=candidate).delete()
            ApplicationElement.objects.filter(candidate=candidate).delete()
            candidate.delete()
        # Re-raise the error
        raise

