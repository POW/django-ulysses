# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import OrganizeCallView, show_call_guidelines, CompetitionXlsExportView, upload_call_file


app_name = 'competitions'


urlpatterns = [
    url(r'announce-your-call/contact-us/$', OrganizeCallView.as_view(), name='organize_call'),
    url(r'organize-call/(?P<id>\d+)/$', OrganizeCallView.as_view(), name='change_organize_call'),
    url(r'announce-your-call/$', show_call_guidelines, name='show_call_guidelines'),
    url(r'export-to-xls/(?P<competition_id>\d+)/$', CompetitionXlsExportView.as_view(), name='export_xls_competition'),
    url(r'upload_call_file/$', upload_call_file, name='upload_call_file')
]
