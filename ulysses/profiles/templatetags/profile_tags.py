# -*- coding: utf-8 -*-

from django import template
from django.utils.safestring import mark_safe

from sorl.thumbnail import get_thumbnail

from ulysses.middleware import get_request
from ulysses.social.utils import is_bookmarked_by, is_work_bookmarked_by, is_followed_by

from ..models import ProfileVisibility
from ..utils import get_profile, get_profile_or_anonymous

register = template.Library()


def _extract_if_node_args(parser, token):
    """utility for if else endif type of tags"""
    nodelist_true = parser.parse(('else', 'endif'))
    token = parser.next_token()
    if token.contents == 'else':
        nodelist_false = parser.parse(('endif',))
        parser.delete_first_token()
    else:
        nodelist_false = template.NodeList()
    return nodelist_true, nodelist_false


class IfCanSeeNode(template.Node):
    """Do something if can see the field"""

    def __init__(self, profile, field_name, nodelist_true, nodelist_false):
        self.profile = profile
        self.field_name = field_name
        self.nodelist_true = nodelist_true
        self.nodelist_false = nodelist_false

    def __iter__(self):
        for node in self.nodelist_true:
            yield node
        for node in self.nodelist_false:
            yield node

    def _get_arg_value(self, context, arg_value):
        if "'" == arg_value[0] or '"' == arg_value[0]:
            return arg_value.strip("'").strip("'")
        else:
            return context.get(arg_value)

    def _check_condition(self, context):
        """check condition of the if"""
        request = context.get('request')
        profile = self._get_arg_value(context, self.profile)
        field_name = self._get_arg_value(context, self.field_name)
        if request:
            return ProfileVisibility.can_see(profile, field_name, request.user)
        raise Exception("Unkown request: Is django request context_processor installed?")

    def render(self, context):
        """to html"""
        if self._check_condition(context):
            return self.nodelist_true.render(context)
        else:
            return self.nodelist_false.render(context)


@register.tag
def if_can_see(parser, token):
    """Do something if user can see field of a given profile"""
    args = token.split_contents()
    if len(args) < 3:
        raise Exception("Invalid tag usage. It should be {% if_can_see profile 'field_name' %}{% endif %}")
    profile = args[1]
    field_name = args[2]
    nodelist_true, nodelist_false = _extract_if_node_args(parser, token)
    return IfCanSeeNode(profile, field_name, nodelist_true, nodelist_false)


@register.filter
def visibility_value(profile, field_name):
    """returns the name of visibility for a given profile"""
    return ProfileVisibility.get_visibility_name(profile, field_name)


@register.filter
def profile(user):
    """returns the name of visibility for a given profile"""
    return get_profile(user)


@register.filter
def profile_or_anonymous(user):
    """returns the name of visibility for a given profile"""
    return get_profile_or_anonymous(user)


@register.filter
def is_follower(member):
    """returns true if the given profile is bookmarked by the logged-in user"""
    request = get_request()
    if request.user.is_authenticated:
        return is_followed_by(member.user, request.user)
    return False


@register.filter
def is_request_user(member):
    """returns true if the given profile is bookmarked by the logged-in user"""
    request = get_request()
    if request.user.is_authenticated:
        return member.user == request.user
    return False


@register.filter
def is_favorite_member(member):
    """returns true if the given profile is bookmarked by the logged-in user"""
    request = get_request()
    if request.user.is_authenticated:
        return is_bookmarked_by(member.user, request.user)
    return False


@register.filter
def is_favorite_work(work):
    """returns true if the given work is bookmarked by the logged-in user"""
    request = get_request()
    if request.user.is_authenticated:
        return is_work_bookmarked_by(work, request.user)
    return False


@register.filter
def as_avatar(image):
    """returns true if the given work is bookmarked by the logged-in user"""
    profile = get_profile(get_request().user)
    css_class = ""
    url = ""
    if not image:
        css_class = 'no-avatar'
    else:
        try:
            url = get_thumbnail(image, "100x100", crop='center').url
        except:
            pass
    return mark_safe('<img src="{0}" class="member-avatar {1}" alt="{2}" />'.format(url, css_class, profile.name))
