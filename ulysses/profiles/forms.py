# -*- coding: utf-8 -*-

from datetime import datetime, date

from django import forms
from django.contrib.auth.models import User
from django.template.defaultfilters import date as date_to_str
from django.utils.dateformat import DateFormat
from django.utils.translation import ugettext_lazy as _, ugettext as __

from ulysses.generic.forms import BaseForm, BaseModelForm, ItemsSearchForm, FileUploadForm
from ulysses.generic.widgets import HtmlWidget
from ulysses.reference.models import Country, Continent, CompetitionCategory

from .fields import MultipleUserChoiceField, MultipleNetworksChoiceField
from .models import (
    Individual, Organization, Interest, OrganizationType, Role, Citizenship,
    OrganizationMembershipRequest, ContactsGroup, ComposerInstrument, IndividualUrl, OrganizationUrl
)
from .widgets import ProfileImageControl


def validate_url_max_length(value):
    if len(value) > 250:
        raise forms.ValidationError( _('URL max length is 250 characters'))


class ProfileBaseModelForm(BaseModelForm):
    """Base class for forms"""

    def __init__(self, only_fields=None, *args, **kwargs):
        """
        Constructor
        Args:
            only_fields: a list of field names. If specified, only these fields are visible
            *args:
            **kwargs:
        """
        super(ProfileBaseModelForm, self).__init__(*args, **kwargs)
        if only_fields:
            for field_name, field in list(self.fields.items()):
                if field_name not in only_fields:
                    multiple_hidden_names = (
                        "interests", "networks", "organization_types", "roles", 'instruments'
                    )
                    if field_name in multiple_hidden_names:
                        field.widget = forms.MultipleHiddenInput()
                    else:
                        field.widget = forms.HiddenInput()

    def save(self, **kwargs):
        if self.instance:
            self.instance.address2 = self.instance.address2 or ''
            self.instance.phone2 = self.instance.phone2 or ''
        return super().save()


class EditIndividualProfileForm(FileUploadForm):
    """Edit an individual profile"""
    first_name = forms.CharField(required=True, max_length=30)
    last_name = forms.CharField(required=True, max_length=30)
    link1 = forms.URLField(required=False, label=_('Website or social link 1'), validators=[validate_url_max_length])
    link2 = forms.URLField(required=False, label=_('Website or social link 2'), validators=[validate_url_max_length])
    birth_date = forms.CharField(
        required=False, label=_('Date of birth'),
        widget=forms.TextInput(attrs={'class': 'birth-datepicker', 'autocomplete': 'off'}),
        help_text=_('format: YYYY-MM-DD (ie: 1980-12-25)')
    )

    class Meta:
        model = Individual
        fields = (
            'photo', 'first_name', 'last_name', 'country', 'city', 'citizenship', 'dual_citizenship', 'birth_date',
            'roles', 'instruments', 'interests', 'organizations', 'biography', 'link1', 'link2',
        )
        upload_fields = ('photo',)
        upload_fields_args = {
            'photo': {
                'extensions': 'png|jpg',
                'label': '',
                'upload_url_name': 'profiles:upload_profile_image',
                'widget_class': ProfileImageControl,
            },
        }

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance')
        urls = [url.url for url in instance.urls()]
        link1 = urls[0] if len(urls) > 0 else ''
        link2 = urls[1] if len(urls) > 1 else ''

        date_as_str = ''
        if instance.birth_date:
            date_format = DateFormat(instance.birth_date)
            date_as_str = date_format.format("Y-m-d")

        kwargs['initial'] = {
            'last_name': instance.user.last_name,
            'first_name': instance.user.first_name,
            'link1': link1,
            'link2': link2,
            'birth_date': date_as_str
        }
        super(EditIndividualProfileForm, self).__init__(*args, **kwargs)
        self.fields['photo_media'].label = __("Profile picture")
        self.fields['biography'].widget = HtmlWidget()
        self.fields['country'].label = __('Country of residence')
        self.fields['roles'].label = _('Occupation')
        self.fields['instruments'].label = _('Instruments played')
        self.fields['interests'].label = _('Main interests')
        self.fields['organizations'].label = _('Organization')

    def save(self, **kwargs):
        individual = super(EditIndividualProfileForm, self).save()
        individual.user.first_name = self.cleaned_data['first_name'].strip()
        individual.user.last_name = self.cleaned_data['last_name'].strip()
        individual.user.save()

        individual.individualurl_set.all().delete()
        link1 = self.cleaned_data.get('link1')
        link2 = self.cleaned_data.get('link2')
        if link1:
            IndividualUrl.objects.create(individual=individual, url=link1)

        if link2:
            IndividualUrl.objects.create(individual=individual, url=link2)

        return individual

    def clean_birth_date(self):
        value = self.cleaned_data['birth_date']
        if value:
            try:
                elements = [int(elt) for elt in value.split('-')]
                if len(elements) != 3:
                    raise ValueError
                year, month, day = elements
                return date(year, month, day)
            except (IndexError, ValueError, TypeError) as err:
                raise forms.ValidationError(_('Invalid date format {0}. Use YYYY-MM-DD').format(value))


class EditOrganizationProfileForm(FileUploadForm):
    """Edit an organization profile"""
    networks = MultipleNetworksChoiceField(required=False)
    link1 = forms.URLField(required=False, label=_('Website or social link 1'), validators=[validate_url_max_length])
    link2 = forms.URLField(required=False, label=_('Website or social link 2'), validators=[validate_url_max_length])

    class Meta:
        model = Organization
        fields = (
            'logo', 'name', 'organization_types', 'description', 'city', 'country', 'interests', 'networks',
            "organization_types",
        )
        upload_fields = ('logo',)
        upload_fields_args = {
            'logo': {
                'extensions': 'png|jpg',
                'label': '',
                'upload_url_name': 'profiles:upload_profile_image',
                'widget_class': ProfileImageControl,
            },
        }

    def __init__(self, *args, **kwargs):
        instance = kwargs.get('instance')
        urls = [url.url for url in instance.urls()]
        link1 = urls[0] if len(urls) > 0 else ''
        link2 = urls[1] if len(urls) > 1 else ''
        kwargs['initial'] = {
            'link1': link1,
            'link2': link2,
        }
        super(EditOrganizationProfileForm, self).__init__(*args, **kwargs)
        self.fields['logo_media'].label = __("Profile picture")
        self.fields['description'].widget = HtmlWidget()

    def save(self, **kwargs):
        organization = super(EditOrganizationProfileForm, self).save()
        organization.user.first_name = ''
        organization.user.last_name = self.cleaned_data['name'].strip()[:30]
        organization.user.save()

        organization.organizationurl_set.all().delete()
        link1 = self.cleaned_data.get('link1')
        link2 = self.cleaned_data.get('link2')
        if link1:
            OrganizationUrl.objects.create(organization=organization, url=link1)

        if link2:
            OrganizationUrl.objects.create(organization=organization, url=link2)

        return organization


class EditIndividualPrivateInfoForm(ProfileBaseModelForm):
    """Edit an organization profile"""

    class Meta:
        model = Individual
        fields = (
            'gender', 'address1', 'address2', 'zipcode', 'phone1', 'phone2',
        )
        labels = {
            'gender': _('My pronouns'),
        }
   


class EditOrganizationPrivateInfoForm(ProfileBaseModelForm):
    """Edit an organization profile"""

    class Meta:
        model = Organization
        fields = (
            'address1', 'address2', 'zipcode', 'phone1', 'phone2',
        )


class ProfileSearchForm(ItemsSearchForm):
    """search for a profile"""
    activity = forms.ChoiceField(
        required=False, label=_('Occupation')
    )
    instruments = forms.ModelChoiceField(
        queryset=ComposerInstrument.objects.all(), required=False, label=_('Instruments')
    )
    interests = forms.ModelChoiceField(
        queryset=Interest.objects.all(), required=False, label=_('Interests')
    )
    country = forms.ChoiceField(required=False, label=_('Reside in'))
    citizenship = forms.ModelChoiceField(
        queryset=Citizenship.objects.all(), required=False, label=_('Citizenship')
    )
    age = forms.CharField(
        required=False, label=_('Born after'),
        widget=forms.TextInput(attrs={'class': 'select-datepicker', 'readonly': 'readonly'})
    )

    def __init__(self, *args, **kwargs):
        super(ProfileSearchForm, self).__init__(*args, **kwargs)

        activity_choices = [('', '')]
        for organization_type in OrganizationType.objects.all():
            activity_choices.append(
                (organization_type.id, organization_type.name)
            )
        for role in Role.objects.all():
            activity_choices.append(
                (-role.id, role.name)
            )
        self.fields['activity'].choices = activity_choices

        continent_choices = []
        for continent in Continent.objects.all():
            continent_choices.append(
                (-continent.id, continent.name)
            )
            country_choices = []
        for country in Country.objects.all():
            country_choices.append(
                (country.id, country.name)
            )
        self.fields['country'].choices = [
            ('', '', ),
            (_('Continents'), continent_choices),
            (_('Countries'), country_choices),
        ]
        self._post_init()

    def _do_get_label(self, field_label, key, item):
        if key == 'age':
            return '{0}: {1}'.format(field_label, self.to_value(item))
        elif key == 'country':
            if item > 0:
                return Country.objects.get(id=item).name
            else:
                return Continent.objects.get(id=-item).name
        else:
            return field_label

    def to_value(self, item):
        for key, value in list(item.items()):
            if key == 'age':
                try:
                    return date_to_str(datetime.fromtimestamp(value / 1000).date())
                except (ValueError, TypeError):
                    return value
            else:
                return value
        return ""


class OrganizationMembershipRequestForm(BaseModelForm):

    class Meta:
        model = OrganizationMembershipRequest
        fields = ['organization']

    def __init__(self, *args, **kwargs):
        super(OrganizationMembershipRequestForm, self).__init__(*args, **kwargs)
        self.fields['organization'].required = True


class AcceptMembershipRequestForm(BaseForm):
    accepted = forms.CharField(widget=forms.HiddenInput(), required=True)

    def __init__(self, instance=None, *args, **kwargs):
        self.instance = instance
        super(AcceptMembershipRequestForm, self).__init__(*args, **kwargs)


class RemoveMembershipForm(BaseForm):
    """select an individual to remove from an organization"""
    individual = forms.IntegerField(required=True, widget=forms.HiddenInput())
    organization = forms.IntegerField(required=True, widget=forms.HiddenInput())


class AddUrlForm(BaseForm):
    """add an url"""
    url = forms.URLField(required=True, label=_('URL'))


class EditContactsGroupForm(BaseModelForm):
    """add a group of contacts"""
    users = MultipleUserChoiceField(label=_('Contacts'))

    class Meta:
        model = ContactsGroup
        fields = ('name', 'users')

    def __init__(self, *args, **kwargs):
        super(EditContactsGroupForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            self.fields['users'].set_initial(self.instance.users.all())


class ProfilePreferencesForm(BaseForm):
    """profile performance"""
    accept_newsletter = forms.BooleanField(
        label=_('Accept newsletter'),
        required=False,
        widget=forms.Select(choices=((True, _('Yes')), (False, _('No')))),
        help_text=_('You will receive the ULYSSES Platform newsletter')
    )
    accept_messages_digest = forms.BooleanField(
        label=_('Accept message digest'),
        required=False,
        widget=forms.Select(choices=((True, _('Yes')), (False, _('No')))),
        help_text=_('You will be notified by email when you receive a message on ULYSSES Platform')
    )
    competition_categories = forms.ModelMultipleChoiceField(
        CompetitionCategory.objects.filter(is_any=False),
        required=False,
        label=_('Competition categories'),
        widget=forms.CheckboxSelectMultiple(),
        help_text=_('You will be notified by email when a competition of the selected categories are published')
    )


class EmailModificationForm(BaseForm):
    """email modification address"""
    email = forms.EmailField(
        required=True,
        label=_('New email address'),
        help_text=_(
            'Your email address is needed to sign in the ULYSSES Platform')
    )

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email).count() > 0:
            raise forms.ValidationError(_('This email is already used by another account'))
        return email
