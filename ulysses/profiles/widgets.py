# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.urls import reverse_lazy

from ulysses.generic.widgets import AutocompleteAutocreateWidget, FileUploadControl

from .models import Network


class UserOrExtraAutocompleteWidget(AutocompleteAutocreateWidget):
    """An autocomplete widget for choosing or adding works"""
    model = User
    autocomplete_url = reverse_lazy("profiles:members_autocomplete")

    def __init__(self, text_field, *args, **kwargs):
        super(UserOrExtraAutocompleteWidget, self).__init__(*args, **kwargs)
        self.text_field = text_field


class NetworkAutocompleteWidget(AutocompleteAutocreateWidget):
    """An autocomplete widget for choosing or adding networks"""
    model = Network


class ProfileImageControl(FileUploadControl):
    template_name = "widgets/profile_image_control.html"
