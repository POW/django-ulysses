# -*- coding: utf-8 -*-

from haystack import indexes

from .models import Individual, Organization


class IndividualIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)
    content_auto = indexes.EdgeNgramField(model_attr='name')

    def get_model(self):
        return Individual

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(is_enabled=True, user__is_active=True)


class OrganizationIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)
    content_auto = indexes.EdgeNgramField(model_attr='name')

    def get_model(self):
        return Organization

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(is_enabled=True, user__is_active=True)

