"""
A two-step (registration followed by activation) workflow, implemented
by storing an activation key in a model and emailing the key to the
user.

This workflow is provided primarily for backwards-compatibility with
existing installations; new installs of django-registration should
look into the HMAC activation workflow in registration.backends.hmac.

"""

from django.contrib import messages
from django.contrib.auth import logout
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.template.loader import render_to_string
from django.views.generic import TemplateView, RedirectView

from django_registration.backends.activation.views import ActivationView as BaseActivationView
from django_registration.backends.activation.views import RegistrationView as BaseRegistrationView
from django_registration.exceptions import ActivationError

from ulysses.generic.emails import send_email
from ulysses.profiles.utils import get_profile
from ulysses.web.models import NewsletterSubscription


class RegistrationView(BaseRegistrationView):

    def send_activation_email(self, user):
        """
        Send the activation email. The activation key is the username,
        signed using TimestampSigner.

        """
        self.request.session['register_email'] = user.email
        activation_key = self.get_activation_key(user)
        context = self.get_email_context(activation_key)
        context["user"] = user
        subject = render_to_string('emails/profile_activation_subject.txt', context)
        # Force subject to a single line to avoid header-injection issues.
        subject = ''.join(subject.splitlines())
        send_email(subject, 'emails/profile_activation.html', context, [user.email])


class ActivationView(BaseActivationView):
    """
    """
    def activate(self, *args, **kwargs):
        user = super().activate(*args, **kwargs)
        profile = get_profile(user)
        if profile.ulysses_newsletter:
            NewsletterSubscription.objects.create(
                email=profile.user.email, at_registration=True
            )
        messages.success(self.request, _('Your account has been successfully activated. Please sign in.'))
        return user

    def get_user(self, username):
        """
        Given the verified username, look up and return the
        corresponding user account if it exists, or raising
        ``ActivationError`` if it doesn't.

        """
        try:
            return super().get_user(username)
        except ActivationError as err:
            if err.code == "already_activated":
                text = _(
                    'This activation link has been already used or has expired. Please try to sign in or re-register.'
                )
                messages.error(self.request, text)
            raise err

    def get_success_url(self, user):
        return reverse('login')


class RegistrationCompleteView(TemplateView):
    template_name = 'registration/registration_complete.html'

    def get_context_data(self, **kwargs):
        context = super(RegistrationCompleteView, self).get_context_data(**kwargs)
        if 'register_email' in self.request.session:
            register_email = self.request.session['register_email']
            context['register_email'] = register_email
        return context


class LogoutView(RedirectView):

    def get(self, request, *args, **kwargs):
        logout(request)
        messages.success(
            request,
            _('You are now logged out.')
        )
        return HttpResponseRedirect(reverse('web:home'))
