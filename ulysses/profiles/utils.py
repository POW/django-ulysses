# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.db.models import Q

from .models import Organization, Individual, AnonymousProfile, OrganizationUrl, IndividualUrl


def get_profile(user):
    """returns the profile associated with the current user"""
    if user is None or user.is_anonymous:
        return None
    try:
        return user.individual
    except Individual.DoesNotExist:
        try:
            return user.organization
        except Organization.DoesNotExist:
            return None


def get_profile_or_anonymous(user):
    """returns the profile associated with the current user"""
    if isinstance(user, User):
        profile = get_profile(user)
    else:
        profile = AnonymousProfile(user)
    return profile


def get_active_members_queryset(only_with_photos=False, only_with_works=False, prefetch=False, queryset=None):
    """

    Returns: queryset of active members users

    """
    if queryset is None:
        queryset = User.objects.all()

    queryset = queryset.filter(is_active=True).filter(
        Q(individual__isnull=False) | Q(organization__isnull=False),
        Q(individual__is_enabled=True) | Q(organization__is_enabled=True)
    )

    if only_with_photos:
        queryset = queryset.exclude(
            (Q(individual__isnull=False) & Q(individual__photo='')) |
            (Q(organization__isnull=False) & Q(organization__logo=''))
        )

    if only_with_works:
        queryset = queryset.exclude(work__isnull=True)

    if prefetch:
        queryset = queryset.prefetch_related('individual', 'organization')

    return queryset


def get_member(member_class, member_id):
    """
    Returns: user or Null if not found
    """
    try:
        return member_class.objects.get(id=member_id, user__is_active=True, is_enabled=True)
    except member_class.DoesNotExist:
        return None


def get_active_organizations_queryset(prefetch=False):
    """return active organization members"""
    queryset = User.objects.filter(is_active=True).filter(
        Q(organization__isnull=False, organization__is_enabled=True)
    )
    if prefetch:
        queryset = queryset.prefetch_related('organization')

    return queryset


def get_active_individuals_queryset(prefetch=False):
    """return active organization members"""
    queryset = User.objects.filter(is_active=True).filter(
        Q(individual__isnull=False, individual__is_enabled=True)
    )
    if prefetch:
        queryset = queryset.prefetch_related('individual')

    return queryset


def get_members(limit_to=None, order_by=None, only_with_photos=False, only_with_works=False):
    """
    get registered active members

    Args:
        limit_to: if set, limit to the numbers of members

    Returns: list of members ordered by date_joined

    """
    # get the active members
    members = get_active_members_queryset(only_with_photos=only_with_photos, only_with_works=only_with_works)

    if order_by is None:
        order_by = ['-date_joined']

    members = members.order_by(*order_by)

    if limit_to:
        # Take limit_to members order by login date, and random
        members = members[:limit_to]

    return members


def get_homepage_members():
    """8 members, random sorting, only with photos and works"""
    return get_members(8, '?', True, True)


def get_members_count():
    """
    Returns: the number of active members
    """
    return get_active_members_queryset().count()


def get_individuals_count():
    """
    Returns: the number of active members
    """
    return get_active_individuals_queryset().count()


def get_organizations_count():
    """
    Returns: the number of active members
    """
    return get_active_organizations_queryset().count()


def get_profile_from_email(email):
    """
    :param email: address email
    :return: first matching profile if a profile correspond to this address email, None if not
    """
    for profile_model in (Individual, Organization):
        profiles = profile_model.objects.filter(user__email=email)
        if profiles.count():
            return profiles[0]
    return None


def switch_profile(old_profile):
    """turn an individual into organization or organization to individual"""
    if old_profile.is_individual():
        new_profile = Organization(user=old_profile.user)
        new_profile.description = old_profile.biography
        new_profile.logo = old_profile.photo
        new_profile.name = old_profile.name
    else:
        new_profile = Individual(user=old_profile.user)
        new_profile.biography = old_profile.description
        new_profile.photo = old_profile.logo
        words = old_profile.name.split(' ')
        if len(words) > 1:
            new_profile.user.first_name = words[0]
            new_profile.user.last_name = ''.join(words[1:])
        else:
            new_profile.user.last_name = old_profile.name
        new_profile.user.save()

    new_profile.country = old_profile.country
    new_profile.city = old_profile.city
    new_profile.is_enabled = old_profile.is_enabled
    new_profile.ulysses_label = old_profile.ulysses_label
    new_profile.has_already_login = old_profile.has_already_login
    new_profile.save()

    # Keep interests
    for interest in old_profile.interests.all():
        new_profile.interests.add(interest)
    new_profile.save()

    # Keep urls
    for url in old_profile.urls():
        if old_profile.is_individual():
            OrganizationUrl.objects.create(organization=new_profile, url=url.url)
        else:
            IndividualUrl.objects.create(individual=new_profile, url=url.url)

    old_profile.delete()

    return new_profile
