# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from ulysses.super_admin.sites import super_admin_site

from .models import (
    OrganizationType, Interest, Organization, OrganizationMember, OrganizationUrl, Individual, Network, IndividualUrl,
    Role, UserViews, OrganizationMembershipRequest, ContactsGroup, WelcomeMessage, ComposerInstrument, EmailModification
)
from .utils import switch_profile


def switch_individual_to_organization(modeladmin, request, queryset):
    if queryset.count() != 1:
        messages.error(request, 'Please select one and only one profile')
    else:
        individual = queryset[0]
        organization = switch_profile(individual)
        url = reverse('admin:ulysses_profiles_organization_change', args=[organization.id])
        return HttpResponseRedirect(url)


switch_individual_to_organization.short_description = _('Switch to organization')


def switch_organization_to_individual(modeladmin, request, queryset):
    if queryset.count() != 1:
        messages.error(request, 'Please select one and only one profile')
    else:
        organization = queryset[0]
        individual = switch_profile(organization)
        url = reverse('admin:ulysses_profiles_individual_change', args=[individual.id])
        return HttpResponseRedirect(url)


switch_organization_to_individual.short_description = _('Switch to individual')


class OrganizationTypeAdmin(admin.ModelAdmin):
    list_display = ("name", "order", )
    list_editable = ("order", )
    search_fields = ("name", )


class ComposerInstrumentAdmin(admin.ModelAdmin):
    list_display = ("__str__", "name", 'profiles_count')
    list_editable = ("name", )
    search_fields = ("name", )


class RoleAdmin(admin.ModelAdmin):
    list_display = ("name", "order", )
    list_editable = ("order", )
    search_fields = ("name", )


class InterestAdmin(admin.ModelAdmin):
    list_display = ("name", "order", "user_counts")
    list_editable = ("order",)
    search_fields = ("name",)


class NetworkAdmin(admin.ModelAdmin):
    list_display = ("name", )
    search_fields = ("name",)


class OrganizationUrlInline(admin.TabularInline):
    model = OrganizationUrl


class OrganizationAdmin(admin.ModelAdmin):
    list_display = ("name", "user", "country", "networks_str", "ulysses_label", "is_enabled", )
    search_fields = ("name", 'user__email', )
    list_filter = ["is_enabled", "ulysses_label", 'networks', 'country', ]
    raw_id_fields = ('user', )
    inlines = [OrganizationUrlInline, ]
    actions = [switch_organization_to_individual]


class OrganizationMemberAdmin(admin.ModelAdmin):
    list_display = ("user", "organization", )
    search_fields = ("organization__name", )


class IndividualUrlInline(admin.TabularInline):
    model = IndividualUrl


class IndividualAdmin(admin.ModelAdmin):
    list_display = ("user", "country", "ulysses_label", "is_enabled", )
    search_fields = ("user__email", "user__last_name", )
    list_filter = ("is_enabled", "ulysses_label", "country", )
    raw_id_fields = ('user', )
    actions = [switch_individual_to_organization]
    inlines = [IndividualUrlInline, ]


class UserViewsAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'count')
    search_fields = ("user__email", "user__last_name", )


class OrganizationMembershipRequestAdmin(admin.ModelAdmin):
    list_display = ('id', 'organization', 'individual', 'accepted', )
    list_filter = ('accepted', )
    search_fields = ("individual__user__email", "individual__user__last_name", )
    date_hierarchy = 'datetime'


class ContactsGroupAdmin(admin.ModelAdmin):
    list_display = ('name', 'owner', )


class WelcomeMessageAdmin(admin.ModelAdmin):
    list_display = ('subject', 'order', 'recipient_type', )
    list_editable = ('order',  'recipient_type', )


class EmailModificationAdmin(admin.ModelAdmin):
    list_display = ("email", "user", "confirmed")
    list_filter = ('confirmed', )
    search_fields = ("email", )


# Global admin registration
super_admin_site.register(OrganizationType, OrganizationTypeAdmin)
super_admin_site.register(Interest, InterestAdmin)
super_admin_site.register(Organization, OrganizationAdmin)
super_admin_site.register(OrganizationMember, OrganizationMemberAdmin)
super_admin_site.register(Individual, IndividualAdmin)
super_admin_site.register(Network, NetworkAdmin)
super_admin_site.register(Role, RoleAdmin)
super_admin_site.register(UserViews, UserViewsAdmin)
super_admin_site.register(OrganizationMembershipRequest, OrganizationMembershipRequestAdmin)
super_admin_site.register(ContactsGroup, ContactsGroupAdmin)
super_admin_site.register(WelcomeMessage, WelcomeMessageAdmin)
super_admin_site.register(ComposerInstrument, ComposerInstrumentAdmin)
super_admin_site.register(EmailModification, EmailModificationAdmin)
