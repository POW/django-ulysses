# -*- coding: utf-8 -*-

from datetime import datetime

from django.db.models import Q
from django.core.management.base import BaseCommand
from django.utils.translation import ugettext as _

from ulysses.competitions.models import Competition, CompetitionCategory
from ulysses.profiles.models import Individual, Organization
from ulysses.generic.emails import send_email


class Command(BaseCommand):
    """send a summary of all emails"""
    help = "send an email to inform users about competitions publication"

    def handle(self, *args, **options):
        verbosity = options.get('verbosity', 1)
        now = datetime.now()
        # We notify competition when open and not when published
        open_competitions = Competition.objects.filter(
            opening_date__lte=now,
            closing_date__gt=now
        )
        unnotified_competitions = open_competitions.filter(
            publication_notified=False,
        )
        # get the categories of the unnotified
        category_ids = list(unnotified_competitions.values_list('category__id', flat=True))
        # The any category means that it must be notified to all profiles who follow at least one category
        any_ids = [cat.id for cat in CompetitionCategory.objects.filter(is_any=True)]
        all_category_ids = category_ids + any_ids
        unnotified_competition_ids = list(unnotified_competitions.values_list('id', flat=True))
        has_any_competitions = unnotified_competitions.filter(category__is_any=True).exists()
        # Mark unnotified as notified
        unnotified_competitions.update(publication_notified=True)
        if has_any_competitions:
            # If a competition is marked as Any. We sent it to all users who have registred to a category
            all_categories = CompetitionCategory.objects.all()
        else:
            # If no competition is marked as Any. Only to users who have registred to the competition categories
            all_categories = CompetitionCategory.objects.filter(id__in=all_category_ids)
        for profile_class in (Individual, Organization):
            profiles = profile_class.objects.filter(
                user__is_active=True, is_enabled=True, competition_categories__in=all_categories
            ).distinct()
            for profile in profiles:
                # for every profile to notified
                profile_categories = profile.competition_categories.all()
                profile_categories_ids = [cat.id for cat in profile_categories]
                all_profile_category_ids = profile_categories_ids + any_ids
                # get the list of competition this profile is interested in
                competitions_to_be_notified = open_competitions.filter(
                    id__in=unnotified_competition_ids, category__id__in=all_profile_category_ids
                )
                competitions_count = competitions_to_be_notified.count()
                if competitions_count > 0:
                    if profile_categories.count() > 1:
                        categories = list(profile_categories)
                        names = ', '.join([cat.name for cat in categories[:-1]])
                        names += ' and ' + categories[-1].name
                        category_str = 'new {0} calls are published'.format(names)
                    else:
                        category = profile_categories[0]
                        category_str = 'a new {0} call is published'.format(category.name)
                    if verbosity:
                        print(
                            "send email to", profile.user.email, ':', competitions_to_be_notified.count(),
                            'competition(s) published'
                        )
                    send_email(
                        _('{0} been published').format(
                            _('A new competition has') if competitions_count == 1 else _('New competitions have')
                        ),
                        'emails/competition_published.html',
                        {
                            'competitions': competitions_to_be_notified,
                            'profile': profile,
                            'category_str': category_str,
                        },
                        [profile.user.email]
                    )
