# -*- coding: utf-8 -*-


import pycountry

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.community.models import CommunityUser
from ulysses.profiles.models import (
    Individual, Organization, IndividualUrl, Role, OrganizationUrl, Interest, OrganizationType, ComposerInstrument
)
from ulysses.reference.models import Country
from ulysses.works.models import Instrument


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "create profiles"

    def get_country(self, code):
        country = pycountry.countries.get(alpha_2=code)
        try:
            return Country.objects.get(name=country.name)
        except Country.DoesNotExist:
            print("Country does'nt exist:", country.name, code)
            return None
        except Country.MultipleObjectsReturned:
            print("duplicate Country:", country.name, code)
            return Country.objects.filter(name=country.name)[0]

    def set_individual_data(self, individual, community_user):
        individual.birth_date = individual.birth_date or community_user.birth_date
        individual.biography = community_user.about_me

        if not individual.country and community_user.country:
            individual.country = self.get_country(community_user.country)

        individual.is_enabled = True

        individual.save()
        individual.user.last_login = community_user.last_login_date

        for url in (community_user.web_site, community_user.facebook, community_user.twitter):
            try:
                IndividualUrl.objects.get_or_create(individual=individual, url=url)
            except Exception as msg:
                print("Create IndividualUrl failed:", msg)

        if community_user.occupation:
            role = Role.objects.get_or_create(
                community_id=community_user.occupation.id, name=community_user.occupation.label
            )[0]
            individual.roles.add(role)
            individual.save()

    def set_organization_data(self, organization, community_user):
        organization.description = community_user.about_me

        if not organization.country and community_user.country:
            organization.country = self.get_country(community_user.country)

        organization.is_enabled = True

        organization.save()

        organization.user.last_login = community_user.last_login_date
        organization.user.save()

        for url in (community_user.web_site, community_user.facebook, community_user.twitter):
            if url:
                try:
                    OrganizationUrl.objects.get_or_create(organization=organization, url=url)
                except Exception as msg:
                    print("Create organizationUrl failed:", msg)

        for org_type in community_user.organization_types.all():
            organization_type, is_new = OrganizationType.objects.get_or_create(name=org_type.label)
            if is_new:
                organization_type.community_id = org_type.id
                organization_type.save()
            organization.organization_types.add(organization_type)
        organization.save()

    def set_interests(self, profile, community_interests):
        for community_interest in community_interests:
            interest, is_new = Interest.objects.get_or_create(name=community_interest.label)
            if is_new:
                print("#create:", community_interest)
            profile.interests.add(interest)
        profile.save()

    def set_instruments(self, profile, community_instruments):
        for community_instrument in community_instruments:
            instrument, is_new = ComposerInstrument.objects.get_or_create(name=community_instrument.label)
            if is_new:
                print("#create:", community_instrument)
            profile.instruments.add(instrument)
        profile.save()

    def handle(self, *args, **options):

        # user_emails = CommunityUser.objects.exclude(email='').values('email').distinct()
        #
        # print user_emails.count(), '==', CommunityUser.objects.exclude(email='').count()
        #
        # for data in user_emails:
        #
        #     if CommunityUser.objects.filter(email=data['email']).count() > 2:
        #         print ">", email, CommunityUser.objects.filter(email=data['email']).count()
        #
        # return

        community_users = CommunityUser.objects.exclude(email="")

        for community_user in community_users:

            individual = organization = None
            user_queryset = User.objects.filter(email=community_user.email)

            if user_queryset.count() > 0:
                user = user_queryset.order_by('-last_login')[0]

                if Organization.objects.filter(user=user).count():
                    organization = Organization.objects.get(user=user)
                    self.set_organization_data(organization, community_user)

                    Individual.objects.filter(user=user).delete()

                elif Individual.objects.filter(user=user).count():
                    individual = Individual.objects.get(user=user)

                    if community_user.profile and community_user.profile.label == "Organization":

                        organization_name = ' '.join([community_user.first_name, community_user.last_name])

                        # On bascule les utilisateurs en organization
                        if Organization.objects.filter(user=user).exists():

                            # Do not create new user
                            organization = Organization.objects.filter(user=user)[0]
                            self.set_organization_data(organization, community_user)

                        else:

                            organization = Organization.objects.create(user=user)
                            organization.name = organization_name
                            organization.city = individual.city
                            organization.country = individual.country

                            organization.save()

                            self.set_organization_data(organization, community_user)

                        for individual_url in IndividualUrl.objects.filter(individual=individual):
                            if individual_url.url:
                                OrganizationUrl.objects.create(organization=organization, url=individual_url.url)

                        individual.delete()
                        individual = None

                    else:

                        self.set_individual_data(individual, community_user)
                        self.set_instruments(individual, community_instruments=community_user.instruments.all())

                else:
                    user_queryset.delete()

            if not (organization or individual):

                password = User.objects.make_random_password()
                username = 'user_{0}_{1}'.format(User.objects.count() + 1, community_user.email)[:30]

                user = User.objects.create_user(
                    username=username, email=community_user.email, password=password,
                    first_name=community_user.first_name, last_name=community_user.last_name,
                    date_joined=community_user.creation_date, last_login=community_user.last_login_date, is_active=True
                )

                if community_user.profile and community_user.profile.label == "Organization":

                    organization_name = ' '.join([community_user.first_name, community_user.last_name])

                    organization = Organization.objects.create(user=user, name=organization_name)
                    self.set_organization_data(organization, community_user)

                else:

                    individual = Individual.objects.create(user=user)
                    self.set_individual_data(individual, community_user)

            profile = organization if organization is not None else individual
            # Force l'activation des comptes Community
            profile.is_enabled = True
            profile.save()
            self.set_interests(profile, community_user.interests.all())
