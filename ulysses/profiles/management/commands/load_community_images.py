# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User

from ulysses.community.models import CommunityUser
from ulysses.community.utils import download_ftp, copy_media
from ulysses.profiles.utils import get_profile


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "create profiles"

    def handle(self, *args, **options):

        community_users = CommunityUser.objects.exclude(email="")

        for index, community_user in enumerate(community_users):

            if community_user.photo:

                try:
                    user = User.objects.get(email=community_user.email)
                except User.MultipleObjectsReturned:
                    user = User.objects.filter(email=community_user.email)[0]
                profile = get_profile(user)

                dir_name = 'organization_logos' if profile.is_organization() else 'individual_photos'

                file_name = copy_media(community_user.photo_storage_path, dir_name, community_user.photo)
                if file_name:
                    if profile.is_organization():
                        profile.logo = file_name
                    else:
                        profile.photo = file_name
                    profile.save()

