# -*- coding: utf-8 -*-



from django.core.management.base import BaseCommand

from ulysses.composers.models import is_composer
from ulysses.competitions.models import Competition
from ulysses.profiles.models import Organization, OrganizationUrl


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "create profiles"

    def handle(self, *args, **options):

        organizations = {}

        print('**** create organizations', Organization.objects.count())

        managers = {}

        for competition in Competition.objects.all().order_by('publication_date'):

            org_name = competition.organized_by.name

            for mgr in competition.managed_by.all().order_by('user__date_joined'):
                email = mgr.user.email
                if email and not is_composer(mgr.user) and org_name not in organizations and email not in managers:
                    organizations[org_name] = (mgr.user, competition.organized_by)
                    managers[email] = org_name

        print('**$$', Competition.objects.count(), len(organizations), len(managers))
        print('**>', organizations)
        print('**#', managers)

        for name, (user, partner) in list(organizations.items()):
            print(">", name, user, partner)
            organization = Organization.objects.create(
                user=user,
                name=name,
                logo=partner.logo,
            )
            if partner.url:
                OrganizationUrl.objects.create(url=partner.url, organization=organization)
