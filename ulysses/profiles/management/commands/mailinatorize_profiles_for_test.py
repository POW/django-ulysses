# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand
from django.contrib.auth.models import User


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "Change all emails except tests"

    def handle(self, *args, **options):

        print("deprecated")

        # users = User.objects.exclude(email="")
        # test_emails = [
        #     'ljean@apidev.fr',
        #     'ceciledrencourt@gmail.com',
        #     'florence.grappin@ircam.fr',
        #     'guillaume.pellerin@ircam.fr',
        #     'jurytest@mailinator.com',
        #     'admintest@mailinator.com',
        #     'cecile.drencourt@ircam.fr',
        #     'luc.jean@apidev.fr',
        #     'contact@apidev.fr',
        # ]
        #
        # for user in users:
        #     if user.email not in test_emails:
        #         user.email = u'user-{0}@mailinator.com'.format(user.id)
        #         user.save()
        #     else:
        #         print u"Keep", user.email
