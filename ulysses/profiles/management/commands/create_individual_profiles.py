# -*- coding: utf-8 -*-



from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.composers.models import is_composer, Composer
from ulysses.competitions.models import CompetitionManager, Competition
from ulysses.profiles.models import Individual, Organization, IndividualUrl, OrganizationMember


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "create profiles"

    def handle(self, *args, **options):

        emails = User.objects.exclude(email='').values('email').distinct()

        total_emails = emails.count()
        print('# create', total_emails, "individual profiles")

        for index, data in enumerate(emails):

            if index % 500 == 0:
                print(index, "/", total_emails)

            email = data['email']

            composer_user_queryset = User.objects.filter(email=email, composer__isnull=False)

            if composer_user_queryset.count() > 0:
                user = composer_user_queryset.order_by('-last_login')[0]
                composer = Composer.objects.get(user=user)

                individual = Individual.objects.get_or_create(user=user)[0]
                individual.birth_date = composer.birth_date
                individual.citizenship = composer.citizenship
                individual.city = composer.city
                individual.country = composer.country
                individual.save()
                if composer.website:
                    IndividualUrl.objects.get_or_create(individual=individual, url=composer.website)

            else:

                is_organization = False
                user = User.objects.filter(email=email).order_by('-last_login')[0]

                competition_manager_queryset = CompetitionManager.objects.filter(user=user)
                if competition_manager_queryset.count():

                    for competition in Competition.objects.filter(managed_by__user=user):

                        if competition.organized_by:
                            organization_name = competition.organized_by.name
                            try:
                                organization = Organization.objects.get(name=organization_name)
                                if organization.user == user:
                                    is_organization = True
                                else:
                                    membership = OrganizationMember.objects.get_or_create(
                                        organization=organization, user=user
                                    )[0]
                                    membership.confirmed = True
                                    membership.save()
                            except Organization.DoesNotExist:
                                pass

                if not is_organization:
                    Individual.objects.get_or_create(user=user)
