# -*- coding: utf-8 -*-



from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.community.models import CommunityUser
from ulysses.profiles.models import (
    Individual, Organization
)



class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "create profiles"

    def handle(self, *args, **options):

        community_users = CommunityUser.objects.exclude(email="")

        for community_user in community_users:

            user_queryset = User.objects.filter(email=community_user.email)

            if user_queryset.count() > 0:
                user = user_queryset.order_by('-last_login')[0]

                if Organization.objects.filter(user=user).count():
                    organization = Organization.objects.get(user=user)
                    organization.is_enabled = True
                    organization.save()

                elif Individual.objects.filter(user=user).count():
                    individual = Individual.objects.get(user=user)
                    individual.is_enabled = True
                    individual.save()

