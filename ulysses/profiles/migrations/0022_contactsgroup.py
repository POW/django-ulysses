# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-12 18:15


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ulysses_profiles','0021_auto_20170411_1426'),
    ]

    operations = [
        migrations.CreateModel(
            name='ContactsGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=100, verbose_name='name')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='owner')),
                ('users', models.ManyToManyField(blank=True, related_name='_contactsgroup_users_+', to=settings.AUTH_USER_MODEL, verbose_name='users')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Group of contacts',
                'verbose_name_plural': 'Groups of contacts',
            },
        ),
    ]
