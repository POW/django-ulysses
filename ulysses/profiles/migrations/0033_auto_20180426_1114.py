# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-04-26 11:14


from django.db import migrations


def forward(apps, schema_editor):
    composer_instrument_class = apps.get_model("ulysses_profiles", "ComposerInstrument")
    # work_instrument_class = apps.get_model("ulysses_works", "ComposerInstrument")
    individual_class = apps.get_model("ulysses_profiles", "Individual")

    for individual in individual_class.objects.filter(instruments__isnull=False):
        for instrument in individual.instruments.all():
            composer_instrument = composer_instrument_class.objects.get_or_create(name=instrument.name)[0]
            individual.composer_instruments.add(composer_instrument)
        individual.instruments.clear()
        individual.save()


def backward(apps, schema_editor):
    composer_instrument_class = apps.get_model("ulysses_profiles", "ComposerInstrument")
    work_instrument_class = apps.get_model("works", "ComposerInstrument")
    individual_class = apps.get_model("ulysses_profiles", "Individual")

    for individual in individual_class.objects.filter(instruments__isnull=False):
        for instrument in individual.composer_instruments.all():
            work_instrument = work_instrument_class.objects.get_or_create(name=instrument.name)[0]
            individual.instruments.add(work_instrument)
        individual.composer_instruments.clear()
        individual.save()
    composer_instrument_class.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0032_auto_20180426_1113'),
    ]

    operations = [
        migrations.RunPython(forward, backward)
    ]
