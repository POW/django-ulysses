# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2016-12-08 16:04


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ulysses_reference', '0002_auto_20161208_1600'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Individual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('birth_date', models.DateField(blank=True, default=None, null=True)),
                ('photo', models.ImageField(blank=True, null=True, upload_to='organization_logos', verbose_name='logo')),
                ('biography', models.TextField(blank=True, verbose_name='biography')),
                ('citizenship', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Citizenship', verbose_name='citizenship')),
                ('city', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.City', verbose_name='City')),
                ('country', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Country', verbose_name='country')),
            ],
            options={
                'ordering': ['user__email'],
                'verbose_name': 'Individual',
                'verbose_name_plural': 'Individuals',
            },
        ),
        migrations.CreateModel(
            name='Interest',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='name')),
                ('order', models.IntegerField(default=0)),
                ('community_id', models.IntegerField(default=0, verbose_name='community id')),
            ],
            options={
                'ordering': ['order', 'name'],
                'verbose_name': 'Interest',
                'verbose_name_plural': 'Interests',
            },
        ),
        migrations.CreateModel(
            name='Organization',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='name')),
                ('description', models.TextField(blank=True, verbose_name='description')),
                ('logo', models.ImageField(blank=True, null=True, upload_to='organization_logos', verbose_name='logo')),
                ('city', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.City', verbose_name='City')),
                ('country', models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Country', verbose_name='country')),
                ('interests', models.ManyToManyField(blank=True, to='ulysses_profiles.Interest')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Organization',
                'verbose_name_plural': 'Organizations',
            },
        ),
        migrations.CreateModel(
            name='OrganizationMember',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('confirmed', models.BooleanField(default=False, help_text='the organization has confirmed the membership', verbose_name='confirmed')),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ulysses_profiles.Organization')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ['organization__name'],
                'verbose_name': 'Organization member',
                'verbose_name_plural': 'Organization members',
            },
        ),
        migrations.CreateModel(
            name='OrganizationType',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='name')),
                ('order', models.IntegerField(default=0)),
                ('community_id', models.IntegerField(default=0, verbose_name='community id')),
            ],
            options={
                'ordering': ['order', 'name'],
                'verbose_name': 'Organization type',
                'verbose_name_plural': 'Organization types',
            },
        ),
        migrations.CreateModel(
            name='OrganizationUrl',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('url', models.URLField(verbose_name='URL')),
                ('order', models.IntegerField(default=0)),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ulysses_profiles.Organization')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'Organization url',
                'verbose_name_plural': 'Organization urls',
            },
        ),
        migrations.AddField(
            model_name='organization',
            name='organization_types',
            field=models.ManyToManyField(blank=True, to='ulysses_profiles.OrganizationType'),
        ),
        migrations.AddField(
            model_name='organization',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
        migrations.AddField(
            model_name='individual',
            name='interests',
            field=models.ManyToManyField(blank=True, to='ulysses_profiles.Interest'),
        ),
        migrations.AddField(
            model_name='individual',
            name='user',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='user'),
        ),
    ]
