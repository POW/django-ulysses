# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-04-26 12:19


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0034_remove_individual_instruments'),
    ]

    operations = [
        migrations.RenameField(
            model_name='individual',
            old_name='composer_instruments',
            new_name='instruments',
        ),
    ]
