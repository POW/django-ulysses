# -*- coding: utf-8 -*-



from django.db import migrations


def forwards_func(apps, schema_editor):
    """create profiles models from ulysses.community model"""
    community_organization_type_model = apps.get_model("ulysses_community", "OrganizationType")
    organization_type_model = apps.get_model("ulysses_profiles", "OrganizationType")

    community_interest_model = apps.get_model("ulysses_profiles", "Interest")
    interest_model = apps.get_model("ulysses_profiles", "Interest")

    model_mapping = [
        (community_organization_type_model, organization_type_model),
        (community_interest_model, interest_model),
    ]

    for from_model, to_model in model_mapping:
        for obj in from_model.objects.all():
            if to_model.objects.filter(name=obj.label).count() == 0:
                to_model.objects.create(name=obj.label, order=obj.order, community_id=obj.id)


def reverse_func(apps, schema_editor):
    """delete everything created byt forward func"""
    organization_type_model = apps.get_model("ulysses_profiles", "OrganizationType")
    interest_model = apps.get_model("ulysses_profiles", "Interest")

    managed_models = [
        organization_type_model, interest_model,
    ]

    for the_model in managed_models:
        the_model.objects.delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0001_initial'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func),
    ]
