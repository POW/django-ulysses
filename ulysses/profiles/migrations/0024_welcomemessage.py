# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-19 10:59


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0023_auto_20170613_1451'),
    ]

    operations = [
        migrations.CreateModel(
            name='WelcomeMessage',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('subject', models.CharField(max_length=200, verbose_name='subject')),
                ('recipient_type', models.IntegerField(choices=[(1, 'All profiles'), (2, 'Individuals'), (3, 'Organizations')], verbose_name='recipients type')),
                ('text', models.TextField(verbose_name='text')),
                ('order', models.PositiveIntegerField(default=1, unique=True, verbose_name='order')),
            ],
            options={
                'ordering': ['order'],
                'verbose_name': 'Welcome message',
                'verbose_name_plural': 'Welcome messages',
            },
        ),
    ]
