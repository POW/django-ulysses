# -*- coding: utf-8 -*-
# Generated by Django 1.10.8 on 2019-06-27 10:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_reference', '0010_competitioncategory'),
        ('ulysses_profiles', '0044_auto_20190517_0951'),
    ]

    operations = [
        migrations.AddField(
            model_name='individual',
            name='competition_categories',
            field=models.ManyToManyField(blank=True, help_text='The user receives an email when a competition of this category is published', to='ulysses_reference.CompetitionCategory', verbose_name='competition categories'),
        ),
        migrations.AddField(
            model_name='organization',
            name='competition_categories',
            field=models.ManyToManyField(blank=True, help_text='The user receives an email when a competition of this category is published', to='ulysses_reference.CompetitionCategory', verbose_name='competition categories'),
        ),
    ]
