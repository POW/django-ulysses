# -*- coding: utf-8 -*-



from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0002_from_community'),
    ]

    operations = [
        migrations.AlterField(
            model_name='individual',
            name='city',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='city'),
        ),
        migrations.AlterField(
            model_name='organization',
            name='city',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='city'),
        ),
    ]
