# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2018-09-27 14:40


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles', '0038_auto_20180927_1436'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='individual',
            name='more_info',
        ),
    ]
