# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-12 22:44


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0009_individual_more_info'),
    ]

    operations = [
        migrations.AddField(
            model_name='individual',
            name='is_enabled',
            field=models.BooleanField(default=False, verbose_name='is enabled'),
        ),
        migrations.AddField(
            model_name='organization',
            name='is_enabled',
            field=models.BooleanField(default=False, verbose_name='is enabled'),
        ),
        migrations.AlterField(
            model_name='profilevisibility',
            name='visibility',
            field=models.IntegerField(choices=[(1, 'Everyone'), (2, 'Members'), (3, 'Friends'), (4, 'Only me')], default=4, verbose_name='visibility'),
        ),
    ]
