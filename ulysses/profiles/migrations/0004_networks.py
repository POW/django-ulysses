# -*- coding: utf-8 -*-



from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_profiles','0003_city_as_char'),
    ]

    operations = [
        migrations.CreateModel(
            name='Network',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, unique=True, verbose_name='name')),
            ],
            options={
                'ordering': ['name'],
                'verbose_name': 'Network',
                'verbose_name_plural': 'Networks',
            },
        ),
        migrations.AddField(
            model_name='organization',
            name='networks',
            field=models.ManyToManyField(blank=True, to='ulysses_profiles.Network'),
        ),
    ]
