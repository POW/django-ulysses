# -*- coding: utf-8 -*-

import uuid
import unicodedata

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models import Q, Sum
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext

from sorl.thumbnail import get_thumbnail
# from dja.models import RegistrationProfile

from ulysses.reference.models import Country, Citizenship, Gender, CompetitionCategory
from ulysses.utils import get_age
from ulysses.works.utils.querysets import get_works_queryset
from ulysses.works.models import WorkViews, Work


class OrganizationType(models.Model):
    """Type of the organization : music academy, music festival, pluridisciplinary festival, ensemble ..."""
    name = models.CharField(verbose_name=_("name"), max_length=200, unique=True)
    order = models.IntegerField(default=0)
    community_id = models.IntegerField(default=0, verbose_name=_("community id"))

    class Meta:
        verbose_name = _('Organization type')
        verbose_name_plural = _('Organization types')
        ordering = ['order', 'name']

    def __str__(self):
        return self.name


class Interest(models.Model):
    """Type of the organization : music academy, music festival, pluridisciplinary festival, ensemble ..."""
    name = models.CharField(verbose_name=_("name"), max_length=200, unique=True)
    order = models.IntegerField(default=0)
    community_id = models.IntegerField(default=0, verbose_name=_("community id"))

    def user_counts(self):
        return self.individual_set.count() + self.organization_set.count()

    class Meta:
        verbose_name = _('Type of Activity')
        verbose_name_plural = _('Types of Activity')
        ordering = ['order', 'name']

    def __str__(self):
        return self.name


class Network(models.Model):
    """A network of organizations"""
    name = models.CharField(verbose_name=_("name"), max_length=200, unique=True)

    class Meta:
        verbose_name = _('Network')
        verbose_name_plural = _('Networks')
        ordering = ['name']

    def __str__(self):
        return self.name


class ComposerInstrument(models.Model):
    name = models.CharField(max_length=100, verbose_name=_("name"))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']
        verbose_name = _("Instrument")
        verbose_name_plural = _("Instruments")

    def profiles(self):
        return Individual.objects.filter(instruments=self)

    def profiles_count(self):
        return self.profiles().count()

    def save(self, *args, **kwargs):
        ret = super(ComposerInstrument, self).save(*args, **kwargs)
        if self.id:
            siblings = ComposerInstrument.objects.filter(name=self.name).exclude(id=self.id)
            for sibling in siblings:
                for individual in sibling.profiles():
                    individual.instruments.remove(sibling)
                    individual.instruments.add(self)
                    individual.save()
            siblings.delete()
        return ret


class BaseProfile(models.Model):
    """Base class for organization and individual"""
    user = models.OneToOneField(User, verbose_name=_("user"), on_delete=models.CASCADE)

    country = models.ForeignKey(
        Country, verbose_name=_("country"), blank=True, null=True, default=None, on_delete=models.SET_NULL
    )
    city = models.CharField(verbose_name=_("city"), max_length=200, blank=True, default='')

    address1 = models.CharField(verbose_name=_("address (1)"), max_length=200, blank=True, default='')
    address2 = models.CharField(verbose_name=_("address (2)"), max_length=200, blank=True, null=True)
    zipcode = models.CharField(verbose_name=_("zip code"), max_length=10, blank=True, default='')
    phone1 = models.CharField(verbose_name=_("phone (1)"), max_length=100, blank=True, default='')
    phone2 = models.CharField(verbose_name=_("phone (2)"), max_length=100, blank=True, null=True)
    interests = models.ManyToManyField(Interest, blank=True)
    is_enabled = models.BooleanField(default=False, verbose_name=_('is enabled'))
    ulysses_label = models.BooleanField(default=False, verbose_name=_('Ulysses label'), db_index=True)
    has_already_login = models.BooleanField(
        default=False, verbose_name=_('Has already login'), help_text=_('True if a user has login at least once')
    )
    ulysses_newsletter = models.BooleanField(verbose_name=_('Ulysses newsletter'), default=False)
    accept_messages_digest = models.BooleanField(verbose_name=_('Accept message digest'), default=True)
    competition_categories = models.ManyToManyField(
        CompetitionCategory, blank=True, verbose_name=_('competition categories'),
        help_text=_('The user receives an email when a competition of this category is published')
    )

    @property
    def favorites_count(self):
        return self.user.followed_set.count()

    def interests_str(self):
        return ', '.join([interest.name for interest in self.interests.all()])

    def competition_categories_str(self):
        return ', '.join([elt.name for elt in self.competition_categories.all()])

    def works_participations(self):
        return get_works_queryset(queryset=self.user.work_as_participant_set.all())

    def composed_works(self):
        return get_works_queryset(queryset=self.user.work_as_composer_set.all())

    def public_works(self):
        return self.user.work_set.exclude(visibility=Work.VISIBILITY_PRIVATE)

    def works_count(self):
        return self.public_works().count()

    def works_views(self):
        works = get_works_queryset(queryset=self.user.work_set.exclude(visibility=Work.VISIBILITY_PRIVATE))
        return WorkViews.objects.filter(work__in=works).aggregate(views=Sum('count'))['views'] or 0

    def followers_count(self):
        return User.objects.filter(follower_set__member_user=self.user).count()

    def following_count(self):
        return User.objects.filter(followed_set__follower=self.user).count()

    def events_collaborations(self):

        events = {}

        def _add_to_events(events_dict, event, role):
            if event.id in events_dict:
                events[event.id].role += ', ' + role
            else:
                event.role = role
                events[event.id] = event

        for event in self.user.organized_event_set.all():
            _add_to_events(events, event, ugettext('Organizator'))

        for event in self.user.event_set.all():
            if event.organizators.count() == 0:
                # Do not add owner if there are some organizators set
                _add_to_events(events, event, ugettext('Organizator'))

        for event in self.user.performed_event_set.all():
            _add_to_events(events, event, ugettext('Performer'))

        events_as_collaborator = []
        for collaboration in self.user.eventcollaborator_set.all():
            event = collaboration.event
            event.role = collaboration.role.name
            _add_to_events(events, event, collaboration.role.name)

        # Sorted events = Most recents first
        return list(reversed(sorted(list(events.values()), key=lambda event: event.start_date)))

    def _get_profile_setup_level_fields(self):
        """returns list of fields and dict of (field, min length)"""
        raise NotImplementedError

    def profile_setup_level(self):
        """percentage of fields with a value"""
        fields, min_lengths = self._get_profile_setup_level_fields()
        score = 0
        for field in fields:
            value = getattr(self, field)
            if callable(value):
                value = value()
            if value:
                score += 1
                if field in min_lengths:
                    if len(value) > min_lengths[field]:
                        score += 1
        return int(round(100 * score / (len(min_lengths) + len(fields)), 0))

    def profile_setup_level_status(self):
        value = self.profile_setup_level()
        if value < 33:
            return "profile-setup-low"
        if value < 50:
            return "profile-setup-medium"
        if value < 75:
            return "profile-setup-high"
        return "profile-setup-full"

    @property
    def location(self):
        if self.country:
            if self.city:
                return '{0} ({1})'.format(self.city, self.country.name)
            else:
                return self.country.name
        else:
            return self.city

    @property
    def email(self):
        return self.user.email

    @property
    def title(self):
        return self.name

    @property
    def website(self):
        urls = self.urls()
        if urls.count():
            return urls[0].url
        return ""

    class Meta:
        abstract = True


class Organization(BaseProfile):
    """An organization"""

    name = models.CharField(verbose_name=_("name"), max_length=200)
    description = models.TextField(verbose_name=_("description"), blank=True)
    organization_types = models.ManyToManyField(OrganizationType, blank=True, verbose_name=_('type'))
    logo = models.ImageField(
        verbose_name=_("logo"), upload_to="organization_logos", blank=True, null=True, max_length=200,
        help_text=_('Supported formats: JPG, PNG. 100MB max.')
    )
    networks = models.ManyToManyField(Network, blank=True)
    ulysses_label = models.BooleanField(default=False, verbose_name=_('Ulysses partner'), db_index=True)
    contact_name = models.CharField(max_length=100, verbose_name=_('Contact name'), blank=True, default='')

    def save(self, **kwargs):
        self.user.last_name = self.name.strip()[:30]
        self.user.first_name = ''
        self.user.save()
        return super(Organization, self).save(**kwargs)

    def is_organization(self):
        return True

    def is_individual(self):
        return False

    def is_anonymous(self):
        return False

    @property
    def name_for_sort(self):
        return self.name.upper()

    @property
    def name_as_candidate(self):
        return self.name.upper()

    @property
    def subtitle(self):
        organization_types = [org_type.name for org_type in self.organization_types.all()]
        return ', '.join(organization_types)

    def about(self):
        organization_types = [org_type.name for org_type in self.organization_types.all()]
        if len(organization_types) > 2:
            organization_types = organization_types[:2] + ["..."]
        return ', '.join(organization_types)

    def _get_profile_setup_level_fields(self):
        return [
            'description', 'networks_str', 'interests_str', 'location', 'logo', 'organization_types_str',
            'urls',
        ], {
            'description': 100
        }

    def get_visibility_fields(self):
        """returns the field who have visibility access"""
        return ['description', 'networks', 'interests', 'location']

    def networks_str(self):
        return ', '.join([network.name for network in self.networks.all()])

    def organization_types_str(self):
        return ', '.join([org_type.name for org_type in self.organization_types.all()])

    def get_absolute_url(self):
        """url of the public page"""
        return reverse('profiles:organization_profile', args=[self.id])

    def get_avatar(self, size='100x100'):
        """returns a small image"""
        if self.logo:
            try:
                return get_thumbnail(self.logo, size, crop='center').url
            except:
                pass
        return '{0}img/default-organization_{1}.png'.format(settings.STATIC_URL, size)

    def get_facebook_avatar(self):
        return self.get_avatar(size='300x300')

    def has_avatar(self):
        if self.logo:
            return True
        else:
            return False

    def get_avatar_class(self):
        if self.logo:
            return ''
        else:
            return 'no-avatar'

    def get_avatar_medium(self):
        return self.get_avatar(size='300x300')

    def individuals(self):
        """members of the organization"""
        return self.individual_set

    def urls(self):
        """urls"""
        return self.organizationurl_set.all()

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ['name']

    def __str__(self):
        return self.name


class OrganizationUrl(models.Model):
    """Urls of an organization : main site, facebook, twitter..."""
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    url = models.URLField(verbose_name=_('URL'), max_length=250)

    class Meta:
        verbose_name = _('Organization url')
        verbose_name_plural = _('Organization urls')

    def __str__(self):
        return self.url


class OrganizationMember(models.Model):
    """A member is attached to the organization but doesn't have possibility to update the organization"""
    organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    confirmed = models.BooleanField(
        default=False, verbose_name=_("confirmed"),
        help_text=_('the organization has confirmed the membership')
    )

    class Meta:
        verbose_name = _('Organization member')
        verbose_name_plural = _('Organization members')
        ordering = ['organization__name']

    def __str__(self):
        return '{0} / {1}'.format(self.organization.name, self.user.email)


class Role(models.Model):
    """Role played by an individual. It can be composer, performer, designer ..."""
    name = models.CharField(verbose_name=_("name"), max_length=200, unique=True)
    order = models.IntegerField(default=0)
    community_id = models.IntegerField(default=0, verbose_name=_("community id"))

    class Meta:
        verbose_name = _('Role')
        verbose_name_plural = _('Roles')
        ordering = ['order', 'name']

    def __str__(self):
        return self.name


class AnonymousProfile(object):

    def __init__(self, name=None):
        self.name = name or ugettext("Anonymous")

    def get_avatar(self, size="100x100"):
        """returns a small image"""
        return '{0}img/default-individual_{1}.png'.format(settings.STATIC_URL, size)

    def is_anonymous(self):
        return True

    def get_avatar_class(self):
        return False

    def get_avatar_class(self):
        return ''

    def get_avatar_medium(self):
        return self.get_avatar(size='300x300')

    def get_absolute_url(self):
        return None


class Individual(BaseProfile):
    """More information about a  individual user"""
    birth_date = models.DateField(default=None, blank=True, null=True)
    photo = models.ImageField(
        verbose_name=_("logo"), upload_to="individual_photos", blank=True, null=True, max_length=200,
        help_text=_('Supported formats: JPG, PNG. 100MB max.')
    )
    biography = models.TextField(verbose_name=_("biography"), blank=True)
    citizenship = models.ForeignKey(
        Citizenship, verbose_name=_("citizenship"), blank=True, null=True, default=None, related_name='+',
        on_delete=models.SET_NULL
    )
    dual_citizenship = models.ForeignKey(
        Citizenship, verbose_name=_("dual citizenship"), blank=True, null=True, default=None, related_name='+',
        on_delete=models.SET_NULL
    )
    roles = models.ManyToManyField(Role, blank=True, verbose_name=_('roles'))

    instruments = models.ManyToManyField(
        ComposerInstrument, verbose_name=_("instruments"), blank=True
    )
    organizations = models.ManyToManyField(Organization, blank=True, verbose_name=_('organizations'))
    gender = models.ForeignKey(
        Gender, blank=True, default=None, null=True, verbose_name=_('Pronouns'), on_delete=models.SET_NULL
    )

    class Meta:
        verbose_name = _('Individual')
        verbose_name_plural = _('Individuals')
        ordering = ['user__email', ]

    def save(self, **kwargs):
        self.user.last_name = self.user.last_name.strip()
        self.user.first_name = self.user.first_name.strip()
        self.user.save()
        return super(Individual, self).save(**kwargs)

    def _get_profile_setup_level_fields(self):
        return [
            'biography', 'birth_date', 'citizenship', 'interests_str', 'location', 'roles_str',
            'instruments_str', 'urls', 'photo',
        ], {
            'biography': 100
        }

    def get_visibility_fields(self):
        """returns the field who have visibility access"""
        return [
            'biography', 'birth_date', 'citizenship', 'interests', 'location', 'roles',
            'instruments',
        ]

    def is_organization(self):
        return False

    def is_individual(self):
        return True

    @property
    def subtitle(self):
        return self.roles_str()

    @property
    def name(self):
        return '{0} {1}'.format(self.user.first_name.strip(), self.user.last_name.strip().upper())

    @property
    def last_name(self):
        return self.user.last_name

    @property
    def first_name(self):
        return self.user.first_name

    @property
    def name_for_sort(self):
        return '{0} {1}'.format(self.user.last_name.strip(), self.user.first_name.strip()).upper()

    @property
    def name_as_candidate(self):
        return '{0}, {1}'.format(self.user.last_name.strip().upper(), self.user.first_name)

    def pending_membership_requests(self):
        return OrganizationMembershipRequest.objects.filter(individual=self, accepted=False)

    def about(self):
        items = [
            role.name for role in self.roles.all()
        ] + [
            organization.name for organization in self.organizations.all()
        ]
        if len(items) > 2:
            items = items[:2] + ["..."]
        return ', '.join(items)

    def has_avatar(self):
        if self.photo:
            return True
        return False

    def get_avatar_class(self):
        if self.photo:
            return ''
        else:
            return 'no-avatar'

    def get_avatar(self, size="100x100"):
        """returns a small image"""
        if self.photo:
            try:
                return get_thumbnail(self.photo, size, crop='center').url
            except:
                pass
        return '{0}img/default-individual_{1}.png'.format(settings.STATIC_URL, size)

    def get_avatar_medium(self):
        return self.get_avatar(size='300x300')

    def get_facebook_avatar(self):
        return self.get_avatar(size='300x300')

    def get_absolute_url(self):
        """url of the public page"""
        return reverse('profiles:individual_profile', args=[self.id])

    def citizenship_str(self):
        citizenships = []
        if self.citizenship:
            citizenships.append(self.citizenship.name)
        if self.dual_citizenship:
            citizenships.append(self.dual_citizenship.name)
        return ', '.join(citizenships)

    def roles_str(self):
        return ', '.join([role.name for role in self.roles.all()])

    def instruments_str(self):
        return ', '.join([instrument.name for instrument in self.instruments.all()])

    def organizations_str(self):
        return ', '.join([organization.name for organization in self.organizations.all()])

    def urls(self):
        """urls"""
        return self.individualurl_set.all()

    def __str__(self):
        return '{0}'.format(self.user.email)

    @property
    def age(self):
        if self.birth_date:
            return get_age(self.birth_date)


class IndividualUrl(models.Model):
    """Urls of an individual : main site, facebook, twitter..."""
    individual = models.ForeignKey(Individual, on_delete=models.CASCADE)
    url = models.URLField(verbose_name=_('URL'), max_length=250)

    class Meta:
        verbose_name = _('Individual url')
        verbose_name_plural = _('Individual urls')

    def __str__(self):
        return self.url


class ProfileVisibility(models.Model):
    """"""
    EVERYONE = 1
    MEMBERS = 2
    FRIENDS = 3
    ONLY_ME = 4

    VISIBILITY_CHOICES = (
        (EVERYONE, _("Everyone")),
        (MEMBERS, _("Members")),
        (FRIENDS, _("Followers")),
        (ONLY_ME, _("Only me")),
    )

    visibility = models.IntegerField(choices=VISIBILITY_CHOICES, default=EVERYONE, verbose_name=_('visibility'))

    field_name = models.CharField(max_length=100, verbose_name=_('field name'), db_index=True)

    profile_models = Q(app_label='profiles', model='individual') | Q(app_label='profiles', model='organization')
    profile = GenericForeignKey(ct_field='profile_ct', fk_field='profile_id')
    profile_ct = models.ForeignKey(
        ContentType, limit_choices_to=profile_models, verbose_name=_('profile content type'), on_delete=models.CASCADE
    )
    profile_id = models.PositiveIntegerField(verbose_name=_('profile id'), db_index=True)

    class Meta:
        verbose_name = _('Profile visibility')
        verbose_name_plural = _('Profile visibilities')

    def __str__(self):
        return '{0} / {1}'.format(self.profile, self.field_name)

    @classmethod
    def get_visibility(cls, profile, field_name):
        """returns the visibility for a given field on a given profile"""
        profile_visibility = cls.get_visibility_obj(profile, field_name)
        if profile_visibility:
            return profile_visibility.visibility
        else:
            return cls.EVERYONE

    @classmethod
    def get_visibility_obj(cls, profile, field_name):
        """returns the visibility for a given field on a given profile"""
        content_type = ContentType.objects.get_for_model(profile)
        try:
            return cls.objects.get(
                profile_ct=content_type, profile_id=profile.id, field_name=field_name
            )
        except cls.DoesNotExist:
            return None

    @classmethod
    def get_visibility_name(cls, profile, field_name):
        """returns the visibility name  for a given field on a given profile"""
        visibility = cls.get_visibility(profile, field_name)
        choices_dict = dict(ProfileVisibility.VISIBILITY_CHOICES)
        return choices_dict.get(visibility, "?")

    @classmethod
    def set_visibility(cls, profile, field_name, visibility):
        """returns the visibility for a given field on a given profile"""
        content_type = ContentType.objects.get_for_model(profile)
        try:
            obj = cls.objects.get(
                profile_ct=content_type, profile_id=profile.id, field_name=field_name
            )
            obj.visibility = visibility
            obj.save()
        except cls.DoesNotExist:
            cls.objects.create(
                profile_ct=content_type, profile_id=profile.id, field_name=field_name, visibility=visibility
            )

    @classmethod
    def can_see(cls, profile, field_name, user):
        """does the user can see the field of the profile"""
        visibiilty = cls.get_visibility(profile, field_name)
        return visibiilty.is_visible_by(user)

    def is_visible_by(self, user):
        """does the user can see the field of the profile"""
        if self.visibility == ProfileVisibility.EVERYONE:
            return True
        elif self.visibility == ProfileVisibility.MEMBERS:
            return user.is_authenticated
        elif self.visibility == ProfileVisibility.FRIENDS:
            # At the moment i am my only friend
            return user == self.profile.user
        else:
            return user == self.profile.user


class UserViews(models.Model):
    """Log the numbers of views by other users in order to calculate popularity"""
    user = models.OneToOneField(User, verbose_name=_('user'), on_delete=models.CASCADE)
    count = models.IntegerField(default=0, verbose_name=_('views count'))

    def __str__(self):
        return '{0} {1}'.format(self.user.last_name, self.user.first_name)

    class Meta:
        verbose_name = _('User views')
        verbose_name_plural = _('Use views')
        ordering = ['-count']


class OrganizationMembershipRequest(models.Model):
    organization = models.ForeignKey(Organization, verbose_name=_('organization'), on_delete=models.CASCADE)
    individual = models.ForeignKey(Individual, verbose_name=_('individual'), on_delete=models.CASCADE)
    accepted = models.BooleanField(default=False, verbose_name=_('accepted'))
    datetime = models.DateTimeField(auto_now=True, verbose_name=_('datetime'))

    def __str__(self):
        return '{0} by {1}'.format(self.organization.name, self.individual.name)

    class Meta:
        verbose_name = _('organization membership request')
        verbose_name_plural = _('organization membership requests')
        ordering = ['-datetime']


class ContactsGroup(models.Model):
    """a group of contacts"""
    owner = models.ForeignKey(User, verbose_name=_('owner'), on_delete=models.CASCADE)
    name = models.CharField(max_length=100, verbose_name=_('name'))
    users = models.ManyToManyField(User, blank=True, related_name='+', verbose_name=_('users'))

    def __str__(self):
        return '{0}'.format(self.name)

    class Meta:
        verbose_name = _('Group of contacts')
        verbose_name_plural = _('Groups of contacts')
        ordering = ['name', ]


class WelcomeMessage(models.Model):
    """A message send to user on first login"""

    MESSAGE_FOR_ALL = 1
    MESSAGE_FOR_INDIVIDUALS = 2
    MESSAGE_FOR_ORGANIZATIONS = 3

    RECIPIENT_TYPE_CHOICES = (
        (MESSAGE_FOR_ALL, _('All profiles')),
        (MESSAGE_FOR_INDIVIDUALS, _('Individuals')),
        (MESSAGE_FOR_ORGANIZATIONS, _('Organizations')),

    )

    subject = models.CharField(max_length=200, verbose_name=_('subject'))
    recipient_type = models.IntegerField(verbose_name=_('recipients type'), choices=RECIPIENT_TYPE_CHOICES)
    text = models.TextField(verbose_name=_('text'))
    order = models.PositiveIntegerField(verbose_name=_('order'), default=1, unique=True)

    class Meta:
        verbose_name = _('Welcome message')
        verbose_name_plural = _('Welcome messages')
        ordering = ['order']

    def __str__(self):
        return self.subject


class EmailModification(models.Model):
    """A message send to user on first login"""

    user = models.ForeignKey(User, verbose_name=_('user'), on_delete=models.CASCADE)
    email = models.EmailField()
    datetime = models.DateTimeField(auto_now=True, verbose_name=_('datetime'))
    confirmed = models.BooleanField(default=False)
    uuid = models.CharField(max_length=100, blank=True, default='', db_index=True)

    class Meta:
        verbose_name = _('Email modification')
        verbose_name_plural = _('Email modifications')
        ordering = ['-datetime']

    def __str__(self):
        return self.email

    def save(self, *args, **kwargs):
        """save"""
        super(EmailModification, self).save(*args, **kwargs)
        if not self.uuid:
            name = '{0}-{1}-email-modification-{1}'.format(
                settings.SECRET_KEY, self.id, self.email
            )
            ascii_token = unicodedata.normalize('NFKD', name).encode("ascii", 'ignore')
            self.uuid = uuid.uuid5(uuid.NAMESPACE_URL, '{0}'.format(ascii_token))
            return super(EmailModification, self).save()
