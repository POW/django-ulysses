"""
Views for creating, editing and viewing user profiles.
"""
from django.http import Http404
from django.utils.translation import ugettext as _
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from ulysses.social.models import Following, FeedItem
from ulysses.social.utils import is_followed_by, is_bookmarked_by, get_all_feeds
from ulysses.generic.views import DetailView, PaginationMixin
from ulysses.utils import get_back_url_for_search

from ..models import Individual, Organization, UserViews
from ..utils import get_profile, get_member


class PublicProfileView(PaginationMixin, DetailView):
    """Show the personal space"""
    model_class = None
    default_back_url = reverse_lazy('profiles:members')
    load_more_pagination = True

    def get_context_data(self, **kwargs):
        profile_id = self.kwargs.get('profile_id', 0)
        profile = get_member(self.model_class, profile_id)
        if not profile:
            raise Http404

        user_views = UserViews.objects.get_or_create(user=profile.user)[0]
        user_views.count += 1
        user_views.save()

        context = super(PublicProfileView, self).get_context_data(**kwargs)

        is_me = profile.user == self.request.user
        follow_warning = ''

        if is_me:
            follow_warning = _("You can't follow yourself")

        if self.request.user.is_authenticated:
            feed_items = get_all_feeds(FeedItem.objects.filter(owner=profile.user))
            # Pagination
            feed_items = self.paginate(feed_items, 3)
        else:
            feed_items = None

        context.update(
            {
                'is_me': is_me,
                'followed': is_followed_by(profile.user, self.request.user),
                'follow_warning': follow_warning,
                'profile': profile,
                'feed_items': feed_items,
                'followers_count': Following.objects.filter(member_user=profile.user).count(),
                "search_back_url": get_back_url_for_search(self.request, "profiles:members"),
                'in_bookmarks': is_bookmarked_by(profile.user, self.request.user),
            }
        )
        return context


class IndividualPublicProfileView(PublicProfileView):
    """view individual profile"""
    template_name = "profiles/individual_profile.html"
    model_class = Individual


class OrganizationPublicProfileView(PublicProfileView):
    """view organization profile"""
    template_name = "profiles/organization_profile.html"
    model_class = Organization


class CurrentUserProfile(RedirectView):
    """view organization profile"""
    def get_redirect_url(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            profile = get_profile(self.request.user)
            if profile:
                return profile.get_absolute_url()
        raise Http404
