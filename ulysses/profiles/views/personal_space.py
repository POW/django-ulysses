# -*- coding: utf-8 -*-
"""
Views for creating, editing and viewing user profiles.
"""
import os

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.auth.views import auth_logout
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.core.files.base import File
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import View

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from sorl.thumbnail import get_thumbnail

from ulysses.competitions.models import ApplicationDraft, Candidate
from ulysses.composers.models import Composer, BiographicElement, Document, Media
from ulysses.events.models import UserEventAlert
from ulysses.generic.emails import send_email
from ulysses.generic.forms import ConfirmForm
from ulysses.middleware import get_request
from ulysses.profiles.serializers import ProfileSerializer
from ulysses.social.utils import get_bookmarked_works, get_bookmarked_members
from ulysses.utils import upload_helper_ex
from ulysses.web.models import NewsletterSubscription
from ulysses.works.models import Work

from ..forms import (
    EditIndividualProfileForm, EditOrganizationProfileForm, EditIndividualPrivateInfoForm,
    EditOrganizationPrivateInfoForm, EditContactsGroupForm, EmailModificationForm, ProfilePreferencesForm
)
from ..models import ContactsGroup, Individual, Organization, EmailModification
from ..utils import get_profile, switch_profile
from ..views.base import ProfileTemplateView, ProfilePopupFormView, ProfileFormView, LoginRequiredMixin


class PersonalSpaceView(ProfileTemplateView):
    """Show the personal space"""
    template_name = "personal_space/main.html"

    def get(self, *args, **kwargs):
        tab = self.request.GET.get('tab')
        if tab == "messages":
            return HttpResponseRedirect(reverse('social:messages'))
        elif tab == 'data':
            return HttpResponseRedirect(reverse('profiles:personal_space') + "?tab=portfolio")
        else:
            return super(PersonalSpaceView, self).get(*args, **kwargs)

    def get_context_data(self, **kwargs):
        """update the context with url kwargs"""
        context = super(PersonalSpaceView, self).get_context_data(**kwargs)

        # Already in context by inheriting from PersonalSpaceView
        profile = context.get('profile')

        context['tab'] = tab = self.request.GET.get('tab') or 'works'
        if profile:
            # Calls
            if tab == 'applications':
                # Applications
                context["draft_applications"] = ApplicationDraft.objects.filter(
                    composer__user=profile.user
                ).order_by('-creation_date')

                context["submitted_applications"] = Candidate.objects.filter(
                    composer__user=profile.user
                ).order_by('-application_date')

            if tab == 'bookmarks':
                context['bookmarked_works'] = get_bookmarked_works(profile.user)
                context['bookmarked_members'] = get_bookmarked_members(profile.user)
                context['event_alerts'] = UserEventAlert.objects.filter(user=profile.user)

            if tab == 'portfolio':
                try:
                    composer = Composer.get_from_user(profile.user)
                    context["biographic_details"] = BiographicElement.objects.filter(composer=composer).order_by(
                        '-creation_date'
                    )
                    context["documents"] = Document.objects.filter(composer=composer).order_by('-creation_date')
                except Composer.DoesNotExist:
                    pass

            if tab == 'works':
                context['works'] = Work.objects.filter(owner=profile.user)

        return context


class EditProfileView(ProfileFormView):
    """edit the profile of the current user"""
    template_name = "personal_space/edit_profile.html"
    profile = None

    def get_context_data(self, **kwargs):
        """update the context with url kwargs"""
        context = super(EditProfileView, self).get_context_data(**kwargs)
        context.update(self.kwargs)
        return context

    def get_form_kwargs(self):
        """returns a dictionary passed as kwargs to the __init__ of the form_class"""
        kwargs = super(EditProfileView, self).get_form_kwargs()

        individual, organization = self.get_profile()
        kwargs.update(
            {
                'instance': individual or organization,
            }
        )
        return kwargs

    def get_form_class(self):
        """returns the form class to use"""
        individual, organization = self.get_profile()
        if individual:
            self.profile = individual
            return EditIndividualProfileForm
        else:
            self.profile = organization
            return EditOrganizationProfileForm

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(self.profile.get_absolute_url())


class EditPrivateProfileView(EditProfileView):
    """edit the profile of the current user"""
    template_name = "personal_space/edit_private_info.html"

    def get_form_class(self):
        """returns the form class to use"""
        individual, organization = self.get_profile()
        if individual:
            self.profile = individual
            return EditIndividualPrivateInfoForm
        else:
            self.profile = organization
            return EditOrganizationPrivateInfoForm


def get_profile_image_directory(user):
    """Get the path where to store file """

    profile = get_profile(user)

    if profile.is_individual():
        model_field = Individual.photo.field
    else:
        model_field = Organization.logo.field

    upload_to = model_field.upload_to
    if callable(upload_to):
        upload_to = upload_to(profile, '')

    folders = (upload_to, )
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def make_avatar_upload_callback(target_filename, target_path):
    url = get_thumbnail(File(open(target_path)), "100x100", crop='center').url
    return target_filename, url


@csrf_exempt
@login_required
def upload_profile_image(request):
    target_dir = get_profile_image_directory(request.user)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    return upload_helper_ex(request, absolute_target_dir, make_avatar_upload_callback)


class EditContactsGroupView(ProfilePopupFormView):
    template_name = "personal_space/edit_contacts_group.html"
    form_class = EditContactsGroupForm

    def get_form_kwargs(self):
        """returns a dictionary passed as kwargs to the __init__ of the form_class"""
        kwargs = super(EditContactsGroupView, self).get_form_kwargs()

        user = self.user

        if 'id' in self.kwargs:
            instance = get_object_or_404(ContactsGroup, id=self.kwargs['id'])
            if instance.owner != user:
                raise PermissionDenied
        else:
            instance = ContactsGroup(owner=user)

        kwargs.update(
            {
                'instance': instance,
            }
        )
        return kwargs

    def form_valid(self, form):
        obj = form.save()
        url = reverse('profiles:personal_space') + '?tab=contacts&contacts_group={0}'.format(obj.id)
        return HttpResponseRedirect(url)


class DeleteContactsGroupView(ProfilePopupFormView):
    """edit the profile of the current user"""
    template_name = "personal_space/delete_contacts_group.html"
    form_class = ConfirmForm

    def get_contacts_group(self):
        instance = get_object_or_404(ContactsGroup, id=self.kwargs['id'])
        if instance.owner != self.user:
            raise PermissionDenied
        return instance

    def get_context_data(self, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""
        context = super(DeleteContactsGroupView, self).get_context_data(**kwargs)
        context['contacts_group'] = self.get_contacts_group()
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            obj = self.get_contacts_group()
            obj.delete()
        url = reverse('profiles:personal_space') + '?tab=contacts'
        return HttpResponseRedirect(url)


# class ContactsGroupUserPagination(PageNumberPagination):
#     page_size = 25
#     page_size_query_param = 'page_size'
#     max_page_size = 100


class ContactsGroupUserView(ListAPIView):
    """View received messages"""
    queryset = User.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]
#    pagination_class = ContactsGroupUserPagination

    def get_queryset(self):
        """returns messages of current user"""
        request = get_request()
        group_id = self.kwargs['group_id']
        contacts_group = get_object_or_404(ContactsGroup, id=group_id)
        if contacts_group.owner != request.user:
            raise PermissionDenied
        return contacts_group.users.all().order_by('last_name')


class SwitchProfileView(ProfilePopupFormView):
    """if organization > individual. If individual > organization"""
    template_name = "personal_space/switch_profile.html"
    form_class = ConfirmForm

    def get_context_data(self, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""
        context = super(SwitchProfileView, self).get_context_data(**kwargs)
        context['profile'] = get_profile(self.request.user)
        return context

    def form_valid(self, form):
        old_profile = get_profile(self.request.user)
        if form.cleaned_data['confirm']:
            new_profile = switch_profile(old_profile)
            return HttpResponseRedirect(new_profile.get_absolute_url())
        return HttpResponseRedirect(old_profile.get_absolute_url())


class DeleteProfileView(ProfilePopupFormView):
    """delete the profile"""
    template_name = "personal_space/delete_profile.html"
    form_class = ConfirmForm

    def get_context_data(self, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""
        context = super(DeleteProfileView, self).get_context_data(**kwargs)
        context['profile'] = get_profile(self.request.user)
        return context

    def form_valid(self, form):
        profile = get_profile(self.request.user)
        if form.cleaned_data['confirm']:
            auth_logout(self.request)
            profile_email = profile.user.email
            NewsletterSubscription.objects.filter(email=profile_email).update(deleted_account=True)
            # Delete the user
            profile.user.delete()
            messages.success(self.request, _('Your account has been successfully deleted'))
            return HttpResponseRedirect(reverse("web:home"))
        return HttpResponseRedirect(profile.get_absolute_url())


class ProfilePreferencesView(ProfilePopupFormView):
    """delete the profile"""
    template_name = "personal_space/profile_preferences.html"
    form_class = ProfilePreferencesForm

    def get_form_kwargs(self, *args, **kwargs):
        form_kwargs = super(ProfilePreferencesView, self).get_form_kwargs(*args, **kwargs)
        profile = get_profile(self.request.user)
        form_kwargs['initial'] = {
            'accept_messages_digest': profile.accept_messages_digest,
            'accept_newsletter': profile.ulysses_newsletter,
            'competition_categories': profile.competition_categories.filter(is_any=False),
        }
        return form_kwargs

    def get_context_data(self, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""
        context = super(ProfilePreferencesView, self).get_context_data(**kwargs)
        context['profile'] = get_profile(self.request.user)
        return context

    def form_valid(self, form):
        profile = get_profile(self.request.user)
        ulysses_newsletter_before = profile.ulysses_newsletter
        profile.accept_messages_digest = form.cleaned_data['accept_messages_digest']
        profile.ulysses_newsletter = form.cleaned_data['accept_newsletter']
        profile.competition_categories.clear()
        for category in form.cleaned_data['competition_categories']:
            profile.competition_categories.add(category)
        profile.save()
        if ulysses_newsletter_before != profile.ulysses_newsletter:
            NewsletterSubscription.objects.create(
                email=profile.email,
                unsubscribe=not profile.ulysses_newsletter
            )
        messages.success(
            self.request,
            _("Your preferences have been successfully updated.")
        )
        return HttpResponseRedirect(reverse("profiles:personal_space"))


class ModifyEmailView(ProfilePopupFormView):
    """delete the profile"""
    template_name = "personal_space/modify_email.html"
    form_class = EmailModificationForm

    def get_context_data(self, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""
        context = super(ModifyEmailView, self).get_context_data(**kwargs)
        context['profile'] = get_profile(self.request.user)
        return context

    def form_valid(self, form):
        profile = get_profile(self.request.user)
        email_modification = EmailModification.objects.create(
            email=form.cleaned_data['email'],
            user=profile.user
        )

        send_email(
            _('Modification of your email address'),
            'emails/email_modification.html',
            {'email_modification': email_modification},
            [email_modification.email]
        )

        messages.success(
            self.request,
            _("To confirm your new email address please click on the activation link we send you via email.")
        )
        return HttpResponseRedirect(reverse("profiles:personal_space"))


class ConfirmEmailModificationView(View, LoginRequiredMixin):
    """confirm the modification of email"""

    def get(self, request, **kwargs):
        """returns a dictionary passed as kwargs to the template for rendering"""

        uuid = self.kwargs.get('uuid')
        if not uuid:
            raise Http404

        email_modification = get_object_or_404(EmailModification, uuid=uuid)
        profile = get_profile(request.user)
        if profile and profile.user == email_modification.user:
            if email_modification.confirmed:
                messages.error(self.request, _('The email modification has already been confirmed'))
            else:
                if User.objects.filter(email=email_modification.email).count() > 0:
                    messages.error(self.request, _('The new email is already used by another account'))
                else:
                    profile.user.email = email_modification.email
                    profile.user.save()
                    email_modification.confirmed = True
                    email_modification.save()
                    messages.success(self.request, _('Your email address has been successfully updated.'))
            return HttpResponseRedirect(reverse("profiles:personal_space"))
        else:
            raise PermissionDenied
