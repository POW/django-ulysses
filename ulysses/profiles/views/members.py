# -*- coding: utf-8 -*-

from datetime import datetime
import xlwt

from django.db.models import Q, Count
from django.utils.translation import ugettext as _

from ulysses.generic.views import ListBasePageView, XlsExportView
from ulysses.profiles.forms import ProfileSearchForm
from ulysses.profiles.utils import get_members, get_profile


class MembersPageView(ListBasePageView):
    """Displays the list of members"""
    template_name = 'profiles/members.html'

    ITEMS_COUNTER_STEP = 16
    ROWS_LG_LIMIT = 4  # By default only 4 lines on small screens

    ORDER_BY_CHOICES = [
        (ListBasePageView.DATE, _('Newest')),
        (ListBasePageView.ALPHABETICAL, _('A-Z')),
        (ListBasePageView.FOLLOWERS, _('Followers')),
        (ListBasePageView.ULYSSES_LABElS, _('Endorsed by <img src="/static/img/ulysses_network_transparent.png" />')),
    ]

    def get_ordering(self, order_by):
        """returns the ordering fields"""
        ordering = ['-date_joined']

        if order_by == self.DATE:
            ordering = ['-date_joined']

        elif order_by == self.ALPHABETICAL:
            ordering = ['last_name', 'first_name']

        elif order_by == self.FOLLOWERS:
            ordering = ['-followers_count', ]

        elif order_by == self.ULYSSES_LABElS:
            ordering = ['-followers_count', ]

        return ordering

    def get_queryset(self):
        return get_members()

    def get_filter_form_class(self):
        return ProfileSearchForm

    def _do_filter_items(self, queryset, field, values):
        if field == 'age':
            for value in values:
                try:
                    value = datetime.fromtimestamp(value / 1000).date()
                except (ValueError, TypeError):
                    value = None
                if value is not None:
                    queryset = queryset.filter(
                        (Q(individual__isnull=False) & Q(individual__birth_date__gte=value))
                    )

        if field == 'country':
            country_values = []
            continent_values = []
            for value in values:
                if value > 0:
                    country_values.append(value)
                else:
                    continent_values.append(-value)
            queryset = queryset.filter(
                (Q(individual__isnull=False) & Q(individual__country__in=country_values)) |
                (Q(organization__isnull=False) & Q(organization__country__in=country_values)) |
                (Q(individual__isnull=False) & Q(individual__country__continent__in=continent_values)) |
                (Q(organization__isnull=False) & Q(organization__country__continent__in=continent_values))
            )

        if field == 'activity':
            role_values = [-value for value in values if value < 0]  # negative values -> roles
            org_type_values = [value for value in values if value > 0]  # positive values -> organization_types

            queryset = queryset.filter(
                (Q(individual__isnull=False) & Q(individual__roles__in=role_values)) |
                (Q(organization__isnull=False) & Q(organization__organization_types__in=org_type_values))
            )

        if field == 'instruments':
            queryset = queryset.filter(
                (Q(individual__isnull=False) & Q(individual__instruments__in=values))
            )

        if field == 'citizenship':
            queryset = queryset.filter(
                (Q(individual__isnull=False) &
                 (Q(individual__citizenship__in=values) | Q(individual__dual_citizenship__in=values)))
            )

        if field == 'interests':
            queryset = queryset.filter(
                (Q(individual__isnull=False) & Q(individual__interests__in=values)) |
                (Q(organization__isnull=False) & Q(organization__interests__in=values))
            )

        return queryset

    def order_items(self, queryset, order_by, ordering):
        if order_by == ListBasePageView.ULYSSES_LABElS:
            queryset = queryset.filter(
                (Q(individual__isnull=False) & Q(individual__ulysses_label=True)) |
                (Q(organization__isnull=False) & Q(organization__ulysses_label=True))
            )

        if '-followers_count' in ordering:
            queryset = queryset.annotate(followers_count=Count('followed_set'))

        return super(MembersPageView, self).order_items(queryset, order_by, ordering)


class MembersXlsExportView(XlsExportView):
    only_superuser = True

    @staticmethod
    def field(profile, field):
        attr = getattr(profile, field)
        if callable(attr):
            attr = attr()
        return str(attr)

    def do_fill_workbook(self, workbook):
        """implement it in base class"""
        sheet = workbook.add_sheet(_("Members"))
        line_index = 0

        header_style = xlwt.XFStyle()
        header_style.pattern = xlwt.Pattern()
        header_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        header_style.pattern.pattern_fore_colour = 22  # light gray

        disabled_style = xlwt.XFStyle()
        disabled_style.pattern = xlwt.Pattern()
        disabled_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        disabled_style.pattern.pattern_fore_colour = 23  # dark gray

        profile_fields = (
            'email', 'country', 'city', 'address1', 'address2', 'zipcode', 'phone1', 'phone2',
            'ulysses_label', 'has_already_login', 'ulysses_newsletter', 'interests_str',
            'competition_categories_str'
        )

        individual_fields = (
            'birth_date', 'citizenship', 'dual_citizenship', 'roles_str', 'instruments_str', 'organizations_str',
            'gender',
        )

        organization_fields = (
            'organization_types_str', 'networks_str', 'ulysses_label', 'contact_name',
        )

        all_fields = profile_fields + individual_fields + organization_fields

        self.write_cell(sheet, line_index, 0, _('type'), style=header_style)
        self.write_cell(sheet, line_index, 1, _('name'), style=header_style)
        self.write_cell(sheet, line_index, 2, _('first name'), style=header_style)
        base_column_index = 3
        individual_base_column_index = base_column_index + len(profile_fields)
        organization_base_column_index = individual_base_column_index + len(individual_fields)

        for index, field in enumerate(all_fields):
            self.write_cell(sheet, line_index, base_column_index + index, field.replace('_str', ''), style=header_style)

        line_index += 1
        for member in get_members(order_by=['id']):
            profile = get_profile(member)
            if profile.is_organization():
                self.write_cell(sheet, line_index, 0, _('Organization'))
                self.write_cell(sheet, line_index, 1, profile.name)
                self.write_cell(sheet, line_index, 2, '')
            else:
                self.write_cell(sheet, line_index, 0, _('Individual'))
                self.write_cell(sheet, line_index, 1, profile.last_name)
                self.write_cell(sheet, line_index, 2, profile.first_name)

            for index, field in enumerate(profile_fields):
                self.write_cell(sheet, line_index, base_column_index + index, self.field(profile, field))

            for index, field in enumerate(individual_fields):
                if profile.is_individual():
                    self.write_cell(sheet, line_index, individual_base_column_index + index, self.field(profile, field))
                else:
                    self.write_cell(sheet, line_index, individual_base_column_index + index, '', style=disabled_style)

            for index, field in enumerate(organization_fields):
                if profile.is_organization():
                    self.write_cell(
                        sheet, line_index, organization_base_column_index + index, self.field(profile, field)
                    )
                else:
                    self.write_cell(sheet, line_index, organization_base_column_index + index, '', style=disabled_style)

            line_index += 1
