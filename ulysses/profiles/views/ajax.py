# -*- coding: utf-8 -*-

from django.db.models import Q
from django.db.models.functions import Lower

from ulysses.generic.views import AjaxView

from ..utils import get_active_members_queryset, get_profile, get_active_organizations_queryset


class MembersAutocompleteView(AjaxView):
    """returns the members matching the request term"""

    def get_queryset(self):
        return get_active_members_queryset(prefetch=True)

    def get_json_data(self):
        """for autocomplete"""
        term = self.request.GET.get('term', '')
        members = []
        members_users_queryset = self.get_queryset()

        words = term.split(" ")
        for word in words:
            members_users_queryset = members_users_queryset.filter(
                Q(last_name__istartswith=word) | Q(first_name__istartswith=word)
            )

        if term:
            members_users_queryset = members_users_queryset.order_by(Lower('last_name'), Lower('first_name'))[:20]

            members = [
                {'id': user.id, 'name': get_profile(user).name}
                for user in members_users_queryset
            ]
        return members


class OrganizationAutocompleteView(MembersAutocompleteView):
    """returns the organizations matching the request term"""

    def get_queryset(self):
        return get_active_organizations_queryset(prefetch=True)
