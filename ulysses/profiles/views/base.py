# -*- coding: utf-8 -*-

from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import LoginView as BaseLoginView
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.urls import resolve
from django.views.generic import FormView, TemplateView

from ulysses.generic.views import ulysses_popup_redirect
from ulysses.events.models import Event
from ulysses.web.forms import EmailAuthForm
from ulysses.works.models import Work


from ..models import Individual, Organization


class LoginRequiredMixin(object):
    """Base class if login is required"""

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        self.user = request.user
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class ProfileMixin(object):
    """Base class for profiles"""
    user = None
    individual = None
    organization = None

    def get_profile(self):
        """Get the profile"""
        if not self.individual and not self.organization:
            try:
                self.individual = Individual.objects.get(user=self.user)
            except Individual.DoesNotExist:
                self.organization = get_object_or_404(Organization, user=self.user)

        return self.individual, self.organization

    def get_profile_context(self):
        """get the context used for rendering the template"""
        individual, organization = self.get_profile()
        return {'individual': individual, 'organization': organization, 'profile': individual or organization}


class ProfileTemplateView(LoginRequiredMixin, ProfileMixin, TemplateView):
    """Show the personal space"""

    def get_context_data(self, **kwargs):
        context = super(ProfileTemplateView, self).get_context_data(**kwargs)
        context.update(self.get_profile_context())
        return context


class ProfileFormView(LoginRequiredMixin, ProfileMixin, FormView):
    """Show the personal space"""

    def get_context_data(self, **kwargs):
        context = super(ProfileFormView, self).get_context_data(**kwargs)
        context.update(self.get_profile_context())
        return context


class ProfilePopupFormView(ProfileFormView):
    """edit the profile of the current user"""
    user = None

    @method_decorator(ulysses_popup_redirect)
    def dispatch(self, request, *args, **kwargs):
        """Manage close of the colorbox popup"""
        self.user = request.user
        return super(ProfilePopupFormView, self).dispatch(request, *args, **kwargs)


class LoginView(BaseLoginView):
    form_class = EmailAuthForm
    template_name = 'registration/login.html'
    redirect_authenticated_user = True

    def get_context_data(self, **kwargs):
        """custom login : add OpenGraph info for works"""
        context = super().get_context_data(**kwargs)
        next_url = self.request.GET.get('next')
        if next_url:
            try:
                resolved = resolve(next_url)
            except:
                resolved = None
            if resolved:
                if resolved.url_name == 'view_work' and resolved.namespaces == ['works']:
                    try:
                        work = Work.objects.get(id=resolved.kwargs.get('id', 0))
                        if work.visibility != Work.VISIBILITY_PRIVATE:
                            context['next_obj'] = work
                    except Work.DoesNotExist:
                        pass
                elif resolved.url_name == 'view_event' and resolved.namespaces == ['events']:
                    try:
                        event = Event.objects.get(id=resolved.kwargs.get('id', 0))
                        context['next_obj'] = event
                    except Event.DoesNotExist:
                        pass
        return context
