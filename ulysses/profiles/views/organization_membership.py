# -*- coding: utf-8 -*-
"""
Views for creating, editing and viewing user profiles.
"""

from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, Http404
from django.shortcuts import get_object_or_404
from django.urls import reverse

from ulysses.profiles.utils import get_profile

from ..forms import OrganizationMembershipRequestForm, AcceptMembershipRequestForm, RemoveMembershipForm
from ..models import OrganizationMembershipRequest, Individual, Organization
from ..views.base import ProfilePopupFormView


class OrganizationMembershipRequestView(ProfilePopupFormView):
    """edit the profile of the current user"""
    template_name = "personal_space/request_organization_membership.html"
    form_class = OrganizationMembershipRequestForm

    def get_form_kwargs(self):
        """returns a dictionary passed as kwargs to the __init__ of the form_class"""
        kwargs = super(OrganizationMembershipRequestView, self).get_form_kwargs()
        profile = get_profile(self.user)
        if not profile.is_individual():
            raise Http404

        instance = OrganizationMembershipRequest(individual=profile)
        kwargs.update(
            {
                'instance': instance,
            }
        )
        return kwargs

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse('profiles:personal_space'))


class AcceptMembershipRequestView(ProfilePopupFormView):
    """edit the profile of the current user"""
    template_name = "personal_space/accept_organization_membership.html"
    form_class = AcceptMembershipRequestForm

    def get_form_kwargs(self):
        """returns a dictionary passed as kwargs to the __init__ of the form_class"""
        kwargs = super(AcceptMembershipRequestView, self).get_form_kwargs()
        instance = get_object_or_404(
            OrganizationMembershipRequest,
            id=self.kwargs['membership_request_id'], organization__user=self.user
        )
        kwargs.update(
            {
                'instance': instance,
                'initial': {'accepted': 'accepted'}
            }
        )
        return kwargs

    def form_valid(self, form):
        instance = form.instance

        profile, organization = self.get_profile()
        if organization != instance.organization:
            raise PermissionDenied

        profile = instance.individual

        if not instance.accepted:
            # Mark as processed
            instance.accepted = True
            instance.save()

            # Make the member as organization member
            profile.organizations.add(organization)
            profile.save()

        return HttpResponseRedirect(profile.get_absolute_url())


class RemoveOrganizationMembershipView(ProfilePopupFormView):
    """remove the individual as organization member"""
    template_name = "personal_space/remove_organization_membership.html"
    form_class = RemoveMembershipForm
    individual = None
    organization = None
    user_profile = None

    def get_user_profile(self, request):
        if not self.user_profile:
            self.user_profile = get_profile(request.user)
        return self.user_profile

    def get_individual(self):
        if not self.individual:
            self.individual = get_object_or_404(Individual, id=self.kwargs['individual_id'])
        return self.individual

    def get_organization(self):
        if not self.organization:
            self.organization = get_object_or_404(Organization, id=self.kwargs['organization_id'])
        return self.organization

    def dispatch(self, request, *args, **kwargs):
        individual = self.get_individual()
        organization = self.get_organization()

        if organization not in individual.organizations.all():
            raise PermissionDenied

        if request.user.is_authenticated:
            profile = self.get_user_profile(request)
            if not (profile == organization or profile == individual):
                # Only the organization or the individual can remove the membership
                raise PermissionDenied

        return super(RemoveOrganizationMembershipView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        """returns a dictionary passed as kwargs to the __init__ of the form_class"""
        kwargs = super(RemoveOrganizationMembershipView, self).get_form_kwargs()
        individual = self.get_individual()
        organization = self.get_organization()
        kwargs.update(
            {
                'initial': {'individual': individual.id, 'organization': organization.id},
            }
        )
        return kwargs

    def get_context_data(self, **kwargs):
        context = super(RemoveOrganizationMembershipView, self).get_context_data(**kwargs)
        context['individual'] = self.get_individual()
        context['organization'] = self.get_organization()
        return context

    def form_valid(self, form):
        """remove the membership"""
        individual = get_object_or_404(Individual, id=form.cleaned_data['individual'])
        organization = get_object_or_404(Organization, id=form.cleaned_data['organization'])
        # Remove the individual as member of organization
        individual.organizations.remove(organization)
        individual.save()
        self.get_user_profile(self.request)
        return HttpResponseRedirect(individual.get_absolute_url())
