# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.generic.fields import MultipleElementChoiceField

from .utils import get_active_members_queryset, get_active_organizations_queryset, get_profile
from .widgets import UserOrExtraAutocompleteWidget, NetworkAutocompleteWidget


class MultipleUserChoiceField(MultipleElementChoiceField):
    """Autocomplete select between active members"""

    def __init__(self, queryset=None, *args, **kwargs):
        if queryset is None:
            queryset = User.objects.none()
        super(MultipleUserChoiceField, self).__init__(queryset=queryset, *args, **kwargs)

    def label_from_instance(self, instance):
        """Get the label for a given user"""
        profile = get_profile(instance)
        if profile:
            return profile.name
        return instance.email

    def get_element_queryset(self):
        return get_active_members_queryset()


class MultipleOrganizationChoiceField(MultipleUserChoiceField):
    """Autocomplete select between active organizations"""

    def get_element_queryset(self):
        return get_active_organizations_queryset()


class MultipleUsersOrExtraChoiceField(forms.CharField):
    """"""
    def __init__(self, text_field, *args, **kwargs):

        if 'widget' not in kwargs:
            kwargs['widget'] = UserOrExtraAutocompleteWidget(
                placeholder_text=_('Enter a member name'), text_field=text_field
            )
        super(MultipleUsersOrExtraChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class MultipleNetworksChoiceField(forms.CharField):
    """Autocomplete select between networks. Can create new ones"""

    def __init__(self, *args, **kwargs):
        if 'widget' not in kwargs:
            kwargs['widget'] = NetworkAutocompleteWidget(
                placeholder_text=_('Enter a network'),
                help_text=_('enter the network, press entry and save')
            )
        super(MultipleNetworksChoiceField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value