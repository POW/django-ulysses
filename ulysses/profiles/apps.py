# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.profiles'
    label = 'ulysses_profiles'
    verbose_name = "Ulysses Profiles"
