# -*- coding: utf-8 -*-

import factory
import datetime

from django.contrib.auth.models import User
from django.core.files.base import ContentFile

from ulysses.reference.factories import CitizenshipFactory, CountryFactory, GenderFactory
from .models import (
    Organization, Individual, Interest, OrganizationType, Network, Role, ContactsGroup, ComposerInstrument
)


class ComposerInstrumentFactory(factory.DjangoModelFactory):
    class Meta:
        model = ComposerInstrument

    name = factory.Sequence(lambda n: 'instrument-{0}'.format(n))


class ProfileUserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    email = factory.Sequence(lambda n: 'user{0}@toto.com'.format(n))
    username = factory.Sequence(lambda n: 'user{0}'.format(n))
    first_name = factory.Sequence(lambda n: 'first-name{0}'.format(n))
    last_name = factory.Sequence(lambda n: 'last-name{0}'.format(n))
    password = "1234"

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        password = kwargs.pop('password', None)
        user = super(ProfileUserFactory, cls)._create(model_class, *args, **kwargs)
        if password:
            user.set_password(password)
            user.save()
        return user


class InterestFactory(factory.DjangoModelFactory):
    class Meta:
        model = Interest

    name = factory.Sequence(lambda n: 'interest_{0}'.format(n))
    order = factory.Sequence(lambda n: n)


class NetworkFactory(factory.DjangoModelFactory):
    class Meta:
        model = Network

    name = factory.Sequence(lambda n: 'network_{0}'.format(n))


class OrganizationTypesFactory(factory.DjangoModelFactory):
    class Meta:
        model = OrganizationType

    name = factory.Sequence(lambda n: 'org_type_{0}'.format(n))
    order = factory.Sequence(lambda n: n)


class RolesFactory(factory.DjangoModelFactory):
    class Meta:
        model = Role

    name = factory.Sequence(lambda n: 'role_{0}'.format(n))
    order = factory.Sequence(lambda n: n)


class OrganizationFactory(factory.DjangoModelFactory):
    class Meta:
        model = Organization

    user = factory.SubFactory(ProfileUserFactory)
    name = factory.Sequence(lambda n: 'organization_{0}'.format(n))
    country = factory.SubFactory(CountryFactory)
    city = factory.Sequence(lambda n: 'Orgcity_{0}'.format(n))
    address1 = 'AAAAAAAAAAAAA'
    address2 = 'BP BBBBBBBB'
    zipcode = 'CCCCCCCCCC'
    phone1 = "07.05.04.03.02"
    phone2 = "07.05.04.03.01"
    is_enabled = True

    logo = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.ImageField()._make_data(
                {'width': 100, 'height': 100}
            ),
            'example.jpg'
        )
    )

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        organization = super(OrganizationFactory, cls)._create(model_class, *args, **kwargs)
        organization.user.last_name = organization.name
        organization.user.first_name = ''
        organization.user.save()
        return organization


class IndividualFactory(factory.DjangoModelFactory):
    class Meta:
        model = Individual

    user = factory.SubFactory(ProfileUserFactory)
    birth_date = datetime.date(1981, 1, 1)
    citizenship = factory.SubFactory(CitizenshipFactory)
    city = factory.Sequence(lambda n: 'Indcity_{0}'.format(n))
    country = factory.SubFactory(CountryFactory)
    gender = factory.SubFactory(GenderFactory)
    address1 = 'XXXXXXXXXXXX'
    address2 = 'BP YYYYYYY'
    zipcode = 'ZZZZZZ'
    phone1 = "06.05.04.03.02"
    phone2 = "06.05.04.03.01"
    is_enabled = True

    photo = factory.LazyAttribute(
        lambda _: ContentFile(
            factory.django.ImageField()._make_data(
                {'width': 100, 'height': 100}
            ),
            'example.jpg'
        )
    )

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        last_name = kwargs.pop('last_name', '')
        first_name = kwargs.pop('first_name', '')
        organizations = kwargs.pop('organizations', [])
        instruments = kwargs.pop('instruments', [])

        individual = super(IndividualFactory, cls)._create(model_class, *args, **kwargs)

        if last_name or last_name:
            individual.user.last_name = last_name
            individual.user.first_name = first_name
            individual.user.save()

        for organization in organizations:
            individual.organizations.add(organization)

        for instrument in instruments:
            individual.instruments.add(instrument)

        individual.save()

        return individual


class ContactsGroupFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'contact-group-{0}'.format(n))

    class Meta:
        model = ContactsGroup

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Get the user of IndividualFactory
        owner = kwargs.pop('owner', None) or IndividualFactory.create().user
        kwargs['owner'] = owner
        group = super(ContactsGroupFactory, cls)._create(model_class, *args, **kwargs)

        # users = kwargs.get('users', [])
        # for user in users:
        #     group.users.add(user)
        # group.save()

        return group

    @factory.post_generation
    def users(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for user in extracted:
                self.users.add(user)
            self.save()
