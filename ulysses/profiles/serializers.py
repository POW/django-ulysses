# -*- coding: utf-8 -*-

from rest_framework.serializers import Serializer

from .utils import get_profile


class ProfileSerializer(Serializer):

    def to_representation(self, user):
        profile = get_profile(user)
        if profile:
            return {
                'id': user.id,
                'name': profile.name,
                'url': profile.get_absolute_url(),
                'avatar': profile.get_avatar(),
                'thumbnail': profile.get_avatar(),
                'about': profile.about(),
            }
