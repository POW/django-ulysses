# -*- coding: utf-8 -*-

from django.urls import reverse

from rest_framework import status

from ulysses.generic.tests import BaseTestCase, APITestCase, assert_popup_redirects

from ..factories import (
    OrganizationFactory, IndividualFactory
)
from ..models import ContactsGroup


class ContactsGroupTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_add_contacts_group(self):
        """it should add group"""

        profile = IndividualFactory.create()

        self.client.login(email=profile.user.email, password="1234")

        url = reverse('profiles:add_contacts_group')

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(0, ContactsGroup.objects.count())

    def test_view_add_anonymous(self):
        """it should add group"""

        url = reverse('profiles:add_contacts_group')
        response = self.client.get(url, follow=True)
        assert_popup_redirects(
            response,
            reverse('login') + '?next={0}'.format(url)
        )

    def test_add_contacts_group(self):
        """it should add group"""

        profile = IndividualFactory.create()

        individual1 = IndividualFactory.create()
        individual2 = IndividualFactory.create()
        individual3 = IndividualFactory.create()
        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        self.client.login(email=profile.user.email, password="1234")

        url = reverse('profiles:add_contacts_group')

        data = {
            'name': 'Friends',
            'users': [individual1.user.id, individual2.user.id, organization1.user.id, organization2.user.id]
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(1, ContactsGroup.objects.count())
        group = ContactsGroup.objects.all()[0]

        assert_popup_redirects(
            response,
            reverse('profiles:personal_space') + '?tab=contacts&contacts_group={0}'.format(group.id)
        )

        self.assertEqual(group.name, data['name'])
        self.assertEqual(group.users.count(), 4)
        for id_ in data['users']:
            self.assertEqual(group.users.filter(id=id_).count(), 1)
        self.assertEqual(group.owner, profile.user)

    def test_add_contacts_group_anonymous(self):
        """it should add group"""

        profile = IndividualFactory.create()

        individual1 = IndividualFactory.create()
        individual2 = IndividualFactory.create()
        individual3 = IndividualFactory.create()
        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        url = reverse('profiles:add_contacts_group')

        data = {
            'name': 'Friends',
            'users': [individual1.user.id, individual2.user.id, organization1.user.id, organization2.user.id]
        }

        response = self.client.post(url, data=data, follow=True)

        assert_popup_redirects(
            response,
            reverse('login') + '?next={0}'.format(url)
        )

        self.assertEqual(0, ContactsGroup.objects.count())

    def test_view_edit_contacts_group(self):
        """it should add group"""

        profile = IndividualFactory.create()

        self.client.login(email=profile.user.email, password="1234")

        group = ContactsGroup.objects.create(name='Clients', owner=profile.user)

        url = reverse('profiles:edit_contacts_group', args=[group.id])

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, ContactsGroup.objects.count())

    def test_edit_contacts_group(self):
        """it should add group"""

        profile = IndividualFactory.create()

        individual1 = IndividualFactory.create()
        individual2 = IndividualFactory.create()
        individual3 = IndividualFactory.create()
        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        self.client.login(email=profile.user.email, password="1234")

        group = ContactsGroup.objects.create(name='Clients', owner=profile.user)
        group.users.add(individual1.user, organization3.user)
        group.save()

        url = reverse('profiles:edit_contacts_group', args=[group.id])

        data = {
            'name': 'Friends',
            'users': [individual1.user.id, individual2.user.id, organization1.user.id, organization2.user.id]
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(1, ContactsGroup.objects.count())
        group = ContactsGroup.objects.all()[0]

        assert_popup_redirects(
            response,
            reverse('profiles:personal_space') + '?tab=contacts&contacts_group={0}'.format(group.id)
        )

        self.assertEqual(group.name, data['name'])
        self.assertEqual(group.users.count(), 4)
        for id_ in data['users']:
            self.assertEqual(group.users.filter(id=id_).count(), 1)
        self.assertEqual(group.owner, profile.user)

    def test_edit_contacts_group_other(self):
        """it should add group"""

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        individual1 = IndividualFactory.create()
        individual2 = IndividualFactory.create()
        individual3 = IndividualFactory.create()
        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        self.client.login(email=profile.user.email, password="1234")

        group = ContactsGroup.objects.create(name='Clients', owner=profile2.user)
        group.users.add(individual1.user, organization3.user)
        group.save()

        url = reverse('profiles:edit_contacts_group', args=[group.id])

        data = {
            'name': 'Friends',
            'users': [individual1.user.id, individual2.user.id, organization1.user.id, organization2.user.id]
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(403, response.status_code)

        self.assertEqual(1, ContactsGroup.objects.count())
        group = ContactsGroup.objects.all()[0]

        self.assertNotEqual(group.name, data['name'])
        self.assertEqual(group.users.count(), 2)
        self.assertEqual(group.owner, profile2.user)


class ContactsOfGroupTest(APITestCase):

    def test_get_contacts(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        group = ContactsGroup.objects.create(name='Clients', owner=profile1.user)
        group.users.add(profile2.user)
        group.save()

        url = reverse('profiles:contacts_of_group', args=[group.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(1, len(response.data))
        self.assertEqual(profile2.user.id, response.data[0]['id'])

    def test_get_contacts_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        group = ContactsGroup.objects.create(name='Clients', owner=profile3.user)
        group.users.add(profile2.user)
        group.save()

        url = reverse('profiles:contacts_of_group', args=[group.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_contacts_unknown(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        group = ContactsGroup.objects.create(name='Clients', owner=profile3.user)
        group.users.add(profile2.user)
        group.save()

        url = reverse('profiles:contacts_of_group', args=[group.id + 1])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_404_NOT_FOUND, response.status_code)

    def test_get_contacts_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name='Clients', owner=profile3.user)
        group.users.add(profile2.user)
        group.save()

        url = reverse('profiles:contacts_of_group', args=[group.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)
