# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.urls import reverse
from django.utils import formats

from ulysses.generic.tests import BaseTestCase

from ulysses.works.factories import WorkFactory
from ulysses.works.models import Work
from ulysses.social.factories import FollowingFactory, FeedItemFactory

from ..factories import OrganizationFactory, IndividualFactory, InterestFactory, NetworkFactory, RolesFactory
from ..models import ProfileVisibility, Individual, Organization, UserViews


class PublicProfileTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_organization_profile(self):
        """it should show profile page"""

        organization = OrganizationFactory.create()

        url = reverse('profiles:organization_profile', args=[organization.id])
        self.assertEqual(url, organization.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, organization.name)
        organization = Organization.objects.get(id=organization.id)
        self.assertEqual(UserViews.objects.get(user=organization.user).count, 1)

    def test_view_organization_inactive(self):
        """it should show profile page"""

        organization = OrganizationFactory.create()
        organization.user.is_active = False
        organization.user.save()

        url = reverse('profiles:organization_profile', args=[organization.id])
        self.assertEqual(url, organization.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        self.assertEqual(UserViews.objects.filter(user=organization.user).count(), 0)

    def test_view_individual_profile(self):
        """it should show profile page"""

        individual = IndividualFactory.create()

        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, individual.user.last_name.upper())
        self.assertContains(response, individual.user.first_name)

        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(UserViews.objects.get(user=individual.user).count, 1)

    def test_view_individual_inactive(self):
        """it should show profile page"""

        individual = IndividualFactory.create()
        individual.user.is_active = False
        individual.user.save()

        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        self.assertEqual(UserViews.objects.filter(user=individual.user).count(), 0)

    def test_view_individual_profile_update_popularity(self):
        """it should show profile page"""

        individual = IndividualFactory.create()
        UserViews.objects.create(user=individual.user, count=5)

        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, individual.user.last_name.upper())
        self.assertContains(response, individual.user.first_name)

        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(UserViews.objects.get(user=individual.user).count, 6)

    def test_view_individual_profile_unknown(self):
        """it should display 404"""

        url = reverse('profiles:individual_profile', args=[1])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_organization_profile_unknown(self):
        """it should display 404"""

        url = reverse('profiles:organization_profile', args=[1])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_posts_on_profile(self):
        """it should show profile page"""

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile3.user.email, password="1234"))

        FollowingFactory.create(member_user=profile2.user, follower=profile.user)

        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile2.user)
        FeedItemFactory.create()

        url = reverse('profiles:individual_profile', args=[profile.id])
        self.assertEqual(url, profile.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, profile.user.last_name.upper())
        self.assertContains(response, profile.user.first_name)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(3, len(soup.select('.community-news .item')))

    def test_view_posts_on_profile_anonymous(self):
        """it should show profile page"""

        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        FollowingFactory.create(member_user=profile2.user, follower=profile.user)

        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile.user)
        FeedItemFactory.create(owner=profile2.user)
        FeedItemFactory.create()

        url = reverse('profiles:individual_profile', args=[profile.id])
        self.assertEqual(url, profile.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, profile.user.last_name.upper())
        self.assertContains(response, profile.user.first_name)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select('.community-news .item')))

    def test_view_max_posts_on_profile(self):
        """it should show profile page"""

        profile = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile3.user.email, password="1234"))

        FeedItemFactory.create_batch(5, owner=profile.user)

        url = reverse('profiles:organization_profile', args=[profile.id])
        self.assertEqual(url, profile.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, profile.user.last_name)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(3, len(soup.select('.community-news .item')))

    def test_view_max_followers_on_profile_page2(self):
        """it should show profile page"""
        profile = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile3.user.email, password="1234"))
        FeedItemFactory.create_batch(5, owner=profile.user)
        url = reverse('profiles:organization_profile', args=[profile.id])
        self.assertEqual(url, profile.get_absolute_url())
        response = self.client.get(url, data={'page': 5})  # page 2 view all
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, profile.user.last_name)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(5, len(soup.select('.community-news .item')))


class PublicFieldsTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_biography(self):
        """it should show profile page with biography"""

        individual = IndividualFactory.create(biography="This is my life")

        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, individual.user.last_name.upper())
        self.assertContains(response, individual.user.first_name)
        self.assertContains(response, individual.biography)

    def test_fields_visibility(self):
        """it should not show private fields on page"""

        interest = InterestFactory.create()

        individual = IndividualFactory.create(biography="About me and myself")
        individual.interests.add(interest)
        individual.save()

        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        birth_date = formats.date_format(individual.birth_date, "DATE_FORMAT")

        self.assertContains(response, individual.user.last_name.upper())
        self.assertContains(response, individual.user.first_name)
        self.assertContains(response, individual.biography)
        self.assertContains(response, individual.city)
        self.assertContains(response, individual.country.name)
        self.assertContains(response, interest.name)
        self.assertContains(response, birth_date)
        self.assertContains(response, individual.citizenship)
        self.assertNotContains(response, individual.gender)
        self.assertNotContains(response, individual.address1)
        self.assertNotContains(response, individual.address2)
        self.assertNotContains(response, individual.zipcode)
        self.assertNotContains(response, individual.phone1)
        self.assertNotContains(response, individual.phone2)

    def test_view_works(self):
        individual = IndividualFactory.create(biography="About me and myself")
        individual.save()
        work0 = WorkFactory.create(composer=individual.user)
        work1 = WorkFactory.create(owner=individual.user)
        work2 = WorkFactory.create()
        work2.participants.add(individual.user)
        work2.save()
        work3 = WorkFactory.create()
        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())
        self.client.login(email=individual.user.email, password="1234")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertFalse(content.find(work0.get_absolute_url()) >= 0)
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_view_works_private(self):
        individual2 = IndividualFactory.create()
        individual = IndividualFactory.create(biography="About me and myself")
        individual.save()
        work1 = WorkFactory.create(owner=individual.user)
        work2 = WorkFactory.create(owner=individual.user, visibility=Work.VISIBILITY_PRIVATE)
        work3 = WorkFactory.create()
        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())
        self.client.login(email=individual2.user.email, password="1234")
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        content = str(soup.select('.ulysses-content')[0])
        self.assertTrue(content.find(work1.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work2.get_absolute_url()) >= 0)
        self.assertFalse(content.find(work3.get_absolute_url()) >= 0)

    def test_view_works_anonymous(self):
        individual = IndividualFactory.create(biography="About me and myself")
        individual.save()
        work1 = WorkFactory.create(owner=individual.user, exclude_from_playlist=True)
        work2 = WorkFactory.create(owner=individual.user, exclude_from_playlist=True)
        work3 = WorkFactory.create(exclude_from_playlist=True)
        url = reverse('profiles:individual_profile', args=[individual.id])
        self.assertEqual(url, individual.get_absolute_url())
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, work1.get_absolute_url())
        self.assertContains(response, work2.get_absolute_url())
        self.assertNotContains(response, work3.get_absolute_url())


class OrganizationVisibilityTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_description(self):
        """it should show profile page with description"""

        organization = OrganizationFactory.create(description="About my organization")

        url = reverse('profiles:organization_profile', args=[organization.id])
        self.assertEqual(url, organization.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, organization.name)
        self.assertContains(response, organization.description)

    def test_fields_visibility(self):
        """it should not show private fields on page"""

        interest = InterestFactory.create()
        network = NetworkFactory.create()

        organization = OrganizationFactory.create(description="About my organization")
        organization.interests.add(interest)
        organization.networks.add(network)
        organization.save()

        for field_name in organization.get_visibility_fields():
            ProfileVisibility.set_visibility(organization, field_name, ProfileVisibility.ONLY_ME)

        url = reverse('profiles:organization_profile', args=[organization.id])
        self.assertEqual(url, organization.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select('.visibility-info')
        self.assertEqual(len(tags), 0)

        self.assertContains(response, organization.name)
        self.assertContains(response, organization.description)
        self.assertContains(response, organization.city)
        self.assertContains(response, organization.country.name)
        self.assertContains(response, interest.name)
        self.assertContains(response, network.name)
        self.assertNotContains(response, organization.address1)
        self.assertNotContains(response, organization.address2)
        self.assertNotContains(response, organization.zipcode)
        self.assertNotContains(response, organization.phone1)
        self.assertNotContains(response, organization.phone2)


class CurrentProfileTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_organization_profile(self):
        """it should show profile page"""

        profile = OrganizationFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:current_user_profile')

        response = self.client.get(url)
        self.assertRedirects(response, profile.get_absolute_url())

    def test_view_individual_profile(self):
        """it should show profile page"""

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:current_user_profile')

        response = self.client.get(url)
        self.assertRedirects(response, profile.get_absolute_url())

    def test_view_anonymous(self):
        """it should display 404"""

        url = reverse('profiles:current_user_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_no_profile(self):
        """it should display 404"""

        profile = IndividualFactory.create()
        user = profile.user
        profile.delete()

        self.assertTrue(self.client.login(email=user.email, password="1234"))

        url = reverse('profiles:current_user_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
