# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core import mail
from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ..factories import OrganizationFactory, IndividualFactory
from ..models import EmailModification


class EmailModificationTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_form_as_org(self):
        profile = OrganizationFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(EmailModification.objects.count(), 0)

    def test_view_form_as_individual(self):
        profile = IndividualFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(EmailModification.objects.count(), 0)

    def test_view_form_as_anonymous(self):
        url = reverse('profiles:modify_email')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + "?next=" + url)
        self.assertEqual(EmailModification.objects.count(), 0)

    def test_change_email_as_individual(self):
        profile = IndividualFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        data = {'email': 'my-new-email@toto.fr'}
        response = self.client.post(url, data)
        assert_popup_redirects(response, reverse('profiles:personal_space'))

        # Check that the request has been stored
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.email, data['email'])
        self.assertNotEqual(email_modification.uuid, '')

        # Check a confirmation email has been sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [data['email']])
        confirm_url = '{0}{1}'.format(
            Site.objects.get_current(),
            reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        )
        self.assertTrue(email.body.find(confirm_url) >= 0)

        # Check the user email has not be changed yet
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, data['email'])

    def test_change_email_as_organization(self):
        profile = OrganizationFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        data = {'email': 'my-new-email@toto.fr'}
        response = self.client.post(url, data)
        assert_popup_redirects(response, reverse('profiles:personal_space'))

        # Check that the request has been stored
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.email, data['email'])
        self.assertNotEqual(email_modification.uuid, '')

        # Check a confirmation email has been sent
        self.assertEqual(len(mail.outbox), 1)
        email = mail.outbox[0]
        self.assertEqual(email.to, [data['email']])
        confirm_url = '{0}{1}'.format(
            Site.objects.get_current(),
            reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        )
        self.assertTrue(email.body.find(confirm_url) >= 0)

        # Check the user email has not be changed yet
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, data['email'])

    def test_change_email_existing(self):
        profile = OrganizationFactory.create()
        profile2 = IndividualFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        data = {'email': profile2.user.email}
        response = self.client.post(url, data)
        self.assertEqual(200, response.status_code)

        # Check that the request has been stored
        self.assertEqual(EmailModification.objects.count(), 0)

        # Check a confirmation email has been sent
        self.assertEqual(len(mail.outbox), 0)

    def test_change_email_missing_value(self):
        profile = OrganizationFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        data = {'email': ''}
        response = self.client.post(url, data)
        self.assertEqual(200, response.status_code)

        # Check that the request has been stored
        self.assertEqual(EmailModification.objects.count(), 0)

        # Check a confirmation email has been sent
        self.assertEqual(len(mail.outbox), 0)

    def test_change_email_wrong_value(self):
        profile = OrganizationFactory.create()
        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:modify_email')
        data = {'email': 'hello'}
        response = self.client.post(url, data)
        self.assertEqual(200, response.status_code)

        # Check that the request has been stored
        self.assertEqual(EmailModification.objects.count(), 0)

        # Check a confirmation email has been sent
        self.assertEqual(len(mail.outbox), 0)

    def test_change_email_as_anonymous(self):
        url = reverse('profiles:modify_email')
        data = {'email': 'my-new-email@toto.fr'}
        response = self.client.post(url, data)
        assert_popup_redirects(response, reverse('login') + "?next=" + url)
        self.assertEqual(EmailModification.objects.count(), 0)


class ConfirmEmailModificationTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_change_email_as_individual(self):
        profile = IndividualFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile.user,
            email='new-email@toto.fr',
            confirmed=False
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        # Check that the request has been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, True)

        # Check the email has been changed
        user = User.objects.get(id=profile.user.id)
        self.assertEqual(user.email, email_modification.email)

    def test_change_email_as_organization(self):
        profile = OrganizationFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile.user,
            email='new-email@toto.fr',
            confirmed=False
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        # Check that the request has been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, True)

        # Check the email has been changed
        user = User.objects.get(id=profile.user.id)
        self.assertEqual(user.email, email_modification.email)

    def test_change_email_as_other(self):
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile2.user,
            email='new-email@toto.fr',
            confirmed=False
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        # Check that the request has not been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile2.user)
        self.assertEqual(email_modification.confirmed, False)

        # Check the email hasn't been changed
        user = User.objects.get(id=profile2.user.id)
        self.assertNotEqual(user.email, email_modification.email)

    def test_change_email_as_anonymous(self):
        profile = IndividualFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile.user,
            email='new-email@toto.fr',
            confirmed=False
        )

        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        # Check that the request has not been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, False)

        # Check the email hasn't been changed
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, email_modification.email)

    def test_change_email_already_confirmed(self):
        profile = IndividualFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile.user,
            email='new-email@toto.fr',
            confirmed=True
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        # Check that the request has not been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, True)

        # Check the email hasn't been changed
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, email_modification.email)

    def test_change_email_already_exists(self):
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        email_modification = EmailModification.objects.create(
            user=profile.user,
            email=profile2.user.email,
            confirmed=False
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=[email_modification.uuid])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('profiles:personal_space'))

        # Check that the request has not been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, False)

        # Check the email hasn't been changed
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, email_modification.email)

    def test_change_email_wrong_token(self):
        profile = IndividualFactory.create()

        EmailModification.objects.create(
            user=profile.user,
            email='new-email@toto.fr',
            confirmed=False
        )

        self.client.login(email=profile.user.email, password="1234")
        url = reverse('profiles:confirm_email_modification', args=['azerty'])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

        # Check that the request has not been updated
        self.assertEqual(EmailModification.objects.count(), 1)
        email_modification = EmailModification.objects.all()[0]
        self.assertEqual(email_modification.user, profile.user)
        self.assertEqual(email_modification.confirmed, False)

        # Check the email hasn't been changed
        user = User.objects.get(id=profile.user.id)
        self.assertNotEqual(user.email, email_modification.email)
