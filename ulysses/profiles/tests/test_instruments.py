# -*- coding: utf-8 -*-

from ulysses.generic.tests import BaseTestCase

from ..factories import IndividualFactory, ComposerInstrumentFactory
from ..models import Individual, ComposerInstrument


class InstrumentTest(BaseTestCase):

    def test_reassign_instruments(self):

        instrument1 = ComposerInstrumentFactory.create(name="violin")
        instrument2 = ComposerInstrumentFactory.create(name="guitar")
        instrument3 = ComposerInstrumentFactory.create(name="violon")

        individual1 = IndividualFactory.create(instruments=[instrument1, instrument2])
        individual2 = IndividualFactory.create(instruments=[instrument1])
        individual3 = IndividualFactory.create(instruments=[instrument1, instrument3])
        individual4 = IndividualFactory.create(instruments=[instrument3, instrument2])

        instrument3.name = instrument1.name
        instrument3.save()

        self.assertEqual(ComposerInstrument.objects.count(), 2)
        self.assertEqual(ComposerInstrument.objects.filter(id=instrument1.id).count(), 0)
        self.assertEqual(ComposerInstrument.objects.filter(id=instrument2.id).count(), 1)
        self.assertEqual(ComposerInstrument.objects.filter(id=instrument3.id).count(), 1)

        self.assertEqual(Individual.objects.count(), 4)
        individual1 = Individual.objects.get(id=individual1.id)
        individual2 = Individual.objects.get(id=individual2.id)
        individual3 = Individual.objects.get(id=individual3.id)
        individual4 = Individual.objects.get(id=individual4.id)

        self.assertEqual(list(individual1.instruments.all()), [instrument2, instrument3])
        self.assertEqual(list(individual2.instruments.all()), [instrument3])
        self.assertEqual(list(individual3.instruments.all()), [instrument3])
        self.assertEqual(list(individual4.instruments.all()), [instrument2, instrument3])
