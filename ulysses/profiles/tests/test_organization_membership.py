# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ..factories import (
    OrganizationFactory, IndividualFactory
)
from ..models import OrganizationMembershipRequest, Individual


class RequestOrganizationMembershipTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_request_as_organization(self):
        """It should not be allowed"""

        profile = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:request_membership')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())

    def test_view_request_as_individual(self):
        """It should show form for selecting an organization"""

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:request_membership')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select("select#id_organization")))
        self.assertEqual(1 + 2, len(soup.select("select#id_organization option")))
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())

    def test_view_request_as_anonymous(self):
        """It should not be allowed"""

        url = reverse('profiles:request_membership')

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())

    def test_post_request_as_organization(self):
        """It should not be allowed"""

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        profile = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:request_membership')

        response = self.client.post(url, data={'organization': organization2})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())

    def test_post_request_as_individual(self):
        """It should create a request and send an email to the organization"""

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:request_membership')

        response = self.client.post(url, data={'organization': organization2.id})
        self.assertEqual(response.status_code, 200)
        assert_popup_redirects(response, reverse('profiles:personal_space'))
        self.assertEqual(1, OrganizationMembershipRequest.objects.count())
        membership_request = OrganizationMembershipRequest.objects.all()[0]
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization2)
        self.assertEqual(membership_request.accepted, False)
        self.assertEqual(membership_request.datetime.date(), date.today())
        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_post_request_as_invalid_organization(self):
        """It should create a request and send an email to the organization"""

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        profile = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:request_membership')

        response = self.client.post(url, data={'organization': 'bla'})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('.field-error')))
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_post_request_as_anonymous(self):
        """It should not be allowed"""
        url = reverse('profiles:request_membership')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))
        self.assertEqual(0, OrganizationMembershipRequest.objects.count())


class AcceptOrganizationMembershipTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_accept_request_as_individual(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_accept_request_as_organization(self):
        """It should show form for selecting an organization"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select("input#id_accepted")))
        self.assertEqual("accepted", soup.select("input#id_accepted")[0].get("value"))
        self.assertEqual("hidden", soup.select("input#id_accepted")[0].get("type"))
        self.assertEqual(1, OrganizationMembershipRequest.objects.count())
        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization)
        self.assertEqual(membership_request.accepted, False)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_view_accept_request_already_accepted(self):
        """It should show form for selecting an organization"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=True
        )

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_accept_request_as_other_organization(self):
        """It should show form for selecting an organization"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        self.assertTrue(self.client.login(email=organization2.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_accept_request_as_anonymous(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

    def test_accept_request_as_organization(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.post(url, data={'accepted': 'accepted'})
        self.assertEqual(response.status_code, 200)
        assert_popup_redirects(response, profile.get_absolute_url())

        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization)
        self.assertEqual(membership_request.accepted, True)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 1)
        self.assertTrue(organization in profile.organizations.all())

    def test_accept_request_as_other_organization(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization2, accepted=False
        )

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.post(url, data={'accepted': 'accepted'})
        self.assertEqual(response.status_code, 404)

        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization2)
        self.assertEqual(membership_request.accepted, False)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_accept_request_as_individual(self):
        """It should cretae a request and send an email to the organization"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.post(url, data={'accepted': 'accepted'})
        self.assertEqual(response.status_code, 404)

        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization)
        self.assertEqual(membership_request.accepted, False)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_accept_request_as_anonymous(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=False
        )

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.post(url, data={'accepted': 'accepted'})
        self.assertEqual(response.status_code, 200)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization)
        self.assertEqual(membership_request.accepted, False)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)

    def test_accept_request_already_accepted(self):
        """It should not be allowed"""

        profile = IndividualFactory.create()
        organization = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()

        membership_request = OrganizationMembershipRequest.objects.create(
            individual=profile, organization=organization, accepted=True
        )

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:accept_membership', args=[membership_request.id])

        response = self.client.post(url, data={'accepted': 'accepted'})
        self.assertEqual(response.status_code, 200)
        assert_popup_redirects(response, profile.get_absolute_url())

        membership_request = OrganizationMembershipRequest.objects.get(id=membership_request.id)
        self.assertEqual(membership_request.individual, profile)
        self.assertEqual(membership_request.organization, organization)
        self.assertEqual(membership_request.accepted, True)
        self.assertEqual(membership_request.datetime.date(), date.today())

        profile = Individual.objects.get(id=profile.id)
        self.assertEqual(profile.organizations.count(), 0)


class RemoveOrganizationMembershipTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_as_individual(self):
        """It should show popup"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(1, len(soup.select("input#id_individual")))
        self.assertEqual(str(individual.id), soup.select("input#id_individual")[0].get("value"))
        self.assertEqual("hidden", soup.select("input#id_individual")[0].get("type"))

        self.assertEqual(1, len(soup.select("input#id_organization")))
        self.assertEqual(str(organization.id), soup.select("input#id_organization")[0].get("value"))
        self.assertEqual("hidden", soup.select("input#id_organization")[0].get("type"))

        self.assertNotContains(response, individual.name)
        self.assertContains(response, organization.name)

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_view_as_organization(self):
        """It should show popup"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(1, len(soup.select("input#id_individual")))
        self.assertEqual(str(individual.id), soup.select("input#id_individual")[0].get("value"))
        self.assertEqual("hidden", soup.select("input#id_individual")[0].get("type"))

        self.assertEqual(1, len(soup.select("input#id_organization")))
        self.assertEqual(str(organization.id), soup.select("input#id_organization")[0].get("value"))
        self.assertEqual("hidden", soup.select("input#id_organization")[0].get("type"))

        self.assertContains(response, individual.name)
        self.assertNotContains(response, organization.name)

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_view_as_non_member(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [])

    def test_view_non_member_as_organization(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [])

    def test_view_as_anonymous(self):
        """It should redirect to login"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_view_as_other_individual(self):
        """It should show error"""

        individual = IndividualFactory.create()
        other_individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=other_individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_view_as_other_organization(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()
        other_organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=other_organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_post_as_individual(self):
        """It should show popup"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id})
        assert_popup_redirects(response, individual.get_absolute_url())

        self.assertEqual(list(individual.organizations.all()), [])

    def test_post_as_organization(self):
        """It should show popup"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        assert_popup_redirects(response, individual.get_absolute_url())

        self.assertEqual(list(individual.organizations.all()), [])

    def test_post_as_non_member(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [])

    def test_post_non_member_as_organization(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        self.assertTrue(self.client.login(email=organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [])

    def test_post_as_anonymous(self):
        """It should redirect to login"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_post_as_other_individual(self):
        """It should show error"""

        individual = IndividualFactory.create()
        other_individual = IndividualFactory.create()
        organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=other_individual.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [organization])

    def test_post_as_other_organization(self):
        """It should show error"""

        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()
        other_organization = OrganizationFactory.create()

        individual.organizations.add(organization)
        individual.save()

        self.assertTrue(self.client.login(email=other_organization.user.email, password="1234"))

        url = reverse('profiles:remove_organization_membership', args=[individual.id, organization.id])

        response = self.client.post(url, {'individual': individual.id, 'organization': organization.id}, follow=True)
        self.assertEqual(response.status_code, 403)

        self.assertEqual(list(individual.organizations.all()), [organization])
