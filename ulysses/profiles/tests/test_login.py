# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.composers.factories import UserFactory
from ulysses.events.factories import EventFactory
from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.profiles.models import Individual, Organization, WelcomeMessage
from ulysses.social.models import Message, MessageRecipient
from ulysses.works.factories import WorkFactory, Work


class EmailAuthTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_login(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)

    def test_view_login_next_work(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, exclude_from_playlist=True)
        response = self.client.get(url, data={'next': work.get_absolute_url()})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertContains(response, work.title)

    def test_view_login_next_work_member(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        work = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, exclude_from_playlist=True)
        response = self.client.get(url, data={'next': work.get_absolute_url()})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertContains(response, work.title)

    def test_view_login_next_event(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        event = EventFactory.create()
        response = self.client.get(url, data={'next': event.get_absolute_url()})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertContains(response, event.title)

    def test_view_login_next_unknown(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        event = EventFactory.create()
        response = self.client.get(url, data={'next': event.get_absolute_url() + 'xxx'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertNotContains(response, event.title)

    def test_view_login_next_work_invalid(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        work = WorkFactory.create(visibility=Work.VISIBILITY_PUBLIC, exclude_from_playlist=True)
        invalid_url = work.get_absolute_url().replace(str(work.id), '99999')
        response = self.client.get(url, data={'next': invalid_url})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertNotContains(response, work.title)

    def test_view_login_next_work_private(self):
        """it should display login form"""

        url = reverse('login')
        self.client.get(url)

        work = WorkFactory.create(visibility=Work.VISIBILITY_PRIVATE)
        response = self.client.get(url, data={'next': work.get_absolute_url()})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        field_names = ('email', 'password', )
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        self.assertNotContains(response, work.title)

    def test_login_with_email_individual(self):
        """it should login_with_email"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user, is_enabled=True)

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data, follow=True)
        individual = Individual.objects.get(id=individual.id)
        self.assertRedirects(response, reverse("profiles:personal_space"))
        self.assertEqual(individual.is_enabled, True)
        self.assertEqual(individual.has_already_login, True)

    def test_login_not_the_first_time(self):
        """it should login_with_email"""
        url = reverse('login')

        WelcomeMessage.objects.all().delete()  # Delete message created by migrations

        welcome_message2 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_INDIVIDUALS, subject="Hello", text='Individual', order=2
        )
        welcome_message1 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ALL, subject="Hello all", text='Nice to see you', order=1
        )
        welcome_message3 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ORGANIZATIONS, subject="Helloorg", text='Org', order=3
        )

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user, is_enabled=True, has_already_login=True)

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data, follow=True)
        individual = Individual.objects.get(id=individual.id)
        self.assertRedirects(response, reverse("web:home"))
        self.assertEqual(individual.is_enabled, True)
        self.assertEqual(individual.has_already_login, True)
        self.assertEqual(Message.objects.count(), 0)

    def test_login_with_email_individual_enable(self):
        """it should login_with_email"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user, is_enabled=False)

        WelcomeMessage.objects.all().delete()  # Delete message created by migrations

        welcome_message2 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_INDIVIDUALS, subject="Hello", text='Individual', order=2
        )
        welcome_message1 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ALL, subject="Hello all", text='Nice to see you', order=1
        )
        welcome_message3 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ORGANIZATIONS, subject="Helloorg", text='Org', order=3
        )

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data, follow=True)
        self.assertRedirects(response, reverse("profiles:personal_space"))
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(individual.is_enabled, True)
        self.assertEqual(individual.has_already_login, True)

        messages = Message.objects.all()
        self.assertEqual(2, messages.count())

        message1 = messages[0]
        self.assertEqual(message1.subject, welcome_message1.subject)
        self.assertEqual(message1.messagereply_set.count(), 1)
        self.assertEqual(message1.messagereply_set.all()[0].body, welcome_message1.text)
        self.assertEqual(message1.sender, None)
        self.assertEqual(
            [msg_rec.user for msg_rec in MessageRecipient.objects.filter(message=message1).all()],
            [user]
        )

        message2 = messages[1]
        self.assertEqual(message2.subject, welcome_message2.subject)
        self.assertEqual(message2.messagereply_set.count(), 1)
        self.assertEqual(message2.messagereply_set.all()[0].body, welcome_message2.text)
        self.assertEqual(message2.sender, None)
        self.assertEqual(
            [msg_rec.user for msg_rec in MessageRecipient.objects.filter(message=message1).all()],
            [user]
        )

    def test_login_with_email_organization(self):
        """it should login_with_email"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        organization = OrganizationFactory.create(user=user, is_enabled=True)

        WelcomeMessage.objects.all().delete()  # Delete message created by migrations

        welcome_message2 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_INDIVIDUALS, subject="Hello", text='Individual', order=2
        )
        welcome_message1 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ALL, subject="Hello all", text='Nice to see you', order=1
        )
        welcome_message3 = WelcomeMessage.objects.create(
            recipient_type=WelcomeMessage.MESSAGE_FOR_ORGANIZATIONS, subject="Helloorg", text='Org', order=3
        )

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data, follow=True)
        self.assertRedirects(response, reverse("profiles:personal_space"))
        organization = Organization.objects.get(id=organization.id)
        self.assertEqual(organization.is_enabled, True)
        self.assertEqual(organization.has_already_login, True)

        messages = Message.objects.all()
        self.assertEqual(2, messages.count())

        message1 = messages[0]
        self.assertEqual(message1.subject, welcome_message1.subject)
        self.assertEqual(message1.messagereply_set.count(), 1)
        self.assertEqual(message1.messagereply_set.all()[0].body, welcome_message1.text)
        self.assertEqual(message1.sender, None)
        self.assertEqual(
            [msg_rec.user for msg_rec in MessageRecipient.objects.filter(message=message1).all()],
            [user]
        )

        message2 = messages[1]
        self.assertEqual(message2.subject, welcome_message3.subject)
        self.assertEqual(message2.messagereply_set.count(), 1)
        self.assertEqual(message2.messagereply_set.all()[0].body, welcome_message3.text)
        self.assertEqual(message2.sender, None)
        self.assertEqual(
            [msg_rec.user for msg_rec in MessageRecipient.objects.filter(message=message2).all()],
            [user]
        )

    def test_login_with_email_organization_enable(self):
        """it should login_with_email"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        organization = OrganizationFactory.create(user=user, is_enabled=False)

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data, follow=True)
        self.assertRedirects(response, reverse("profiles:personal_space"))
        organization = Organization.objects.get(id=organization.id)
        self.assertEqual(organization.is_enabled, True)
        self.assertEqual(organization.has_already_login, True)

    def test_login_wrong_email(self):
        """it should show an error"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user)

        post_data = {
            'email': "william@dalton.com",
            'password': password,
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select(".errorlist.nonfield")))
        self.assertEqual(1, len(soup.select(".errorlist")))
        self.assertEqual(1, len(soup.select('.field-with-error input[name=email]')))

        field_names = ('email', 'password',)
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(individual.has_already_login, False)

    def test_login_wrong_password(self):
        """it should show an error"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user, is_enabled=True)

        post_data = {
            'email': email,
            'password': "234",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select(".errorlist.nonfield")))
        self.assertEqual(1, len(soup.select(".errorlist")))
        self.assertEqual(1, len(soup.select('.field-with-error input[name=password]')))

        field_names = ('email', 'password',)
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(individual.is_enabled, True)

    def test_login_wrong_password_enable(self):
        """it should show an error"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=True)
        individual = IndividualFactory.create(user=user, is_enabled=False)

        post_data = {
            'email': email,
            'password': "234",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select(".errorlist.nonfield")))
        self.assertEqual(1, len(soup.select(".errorlist")))
        self.assertEqual(1, len(soup.select('.field-with-error input[name=password]')))

        field_names = ('email', 'password',)
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(individual.is_enabled, False)

    def test_inactive_user(self):
        """it should show an error"""
        url = reverse('login')

        email = 'joe@dalton.fr'
        password = "123"
        user = UserFactory.create(email=email, password=password, is_active=False)
        IndividualFactory.create(user=user)

        post_data = {
            'email': email,
            'password': password,
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select(".errorlist.nonfield")))
        self.assertEqual(1, len(soup.select(".errorlist")))
        self.assertEqual(1, len(soup.select('.field-with-error input[name=email]')))

        field_names = ('email', 'password',)
        for field_name in field_names:
            self.assertEqual(len(soup.select("form.ulysses-form #id_" + field_name)), 1)
