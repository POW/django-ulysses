# -*- coding: utf-8 -*-

import json

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ..factories import OrganizationFactory, IndividualFactory


class MembersAutocompleteTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_autocomplete_empty(self):
        """it should return a json list"""

        url = reverse('profiles:members_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual([], json_data)

    def test_autocomplete_inactive(self):
        """it should return a json list"""

        individual1 = IndividualFactory.create(last_name='Allard', first_name='Bernard')
        individual1.user.is_active = False
        individual1.user.save()

        url = reverse('profiles:members_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual([], json_data)

    def test_autocomplete_name(self):
        """it should return a json list"""

        individual1 = IndividualFactory.create(last_name='Allard', first_name='Bernard')
        individual2 = IndividualFactory.create(last_name='Allard', first_name='Alain')
        individual3 = IndividualFactory.create(last_name='Bernard', first_name='Alain')
        individual4 = IndividualFactory.create(last_name='Durand', first_name='Maurice')

        organization1 = OrganizationFactory.create(name='ASSE')
        organization2 = OrganizationFactory.create(name='IRCAM')

        url = reverse('profiles:members_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(4, len(json_data))

        self.assertEqual(
            json_data,
            [
                {'id': elt.user.id, 'name': elt.name}
                for elt in [individual2, individual1, organization1, individual3]
            ]
        )

    def test_autocomplete_two_words(self):
        """it should return a json list"""

        individual1 = IndividualFactory.create(last_name='Allard', first_name='Bernard')
        individual2 = IndividualFactory.create(last_name='Allard', first_name='Alain')
        individual3 = IndividualFactory.create(last_name='Bernard', first_name='Alain')
        individual4 = IndividualFactory.create(last_name='Durand', first_name='Maurice')

        organization1 = OrganizationFactory.create(name='ASSE')
        organization2 = OrganizationFactory.create(name='IRCAM')

        url = reverse('profiles:members_autocomplete')

        response = self.client.get(url, data={'term': 'Bernard Al'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(2, len(json_data))

        self.assertEqual(
            json_data,
            [
                {'id': elt.user.id, 'name': elt.name}
                for elt in [individual1, individual3]
            ]
        )

    def test_autocomplete_limit(self):
        """it should return a json list"""

        for index in range(25):
            individual1 = IndividualFactory.create(last_name='Allard{0}'.format(index))

        url = reverse('profiles:members_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(20, len(json_data))


class OrganizationAutocompleteTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_autocomplete_empty(self):
        """it should return a json list"""

        url = reverse('profiles:organizations_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual([], json_data)

    def test_autocomplete_name(self):
        """it should return a json list"""

        individual1 = IndividualFactory.create(last_name='Allard', first_name='Bernard')
        individual2 = IndividualFactory.create(last_name='Allard', first_name='Alain')
        individual3 = IndividualFactory.create(last_name='Bernard', first_name='Alain')
        individual4 = IndividualFactory.create(last_name='Durand', first_name='Maurice')

        organization1 = OrganizationFactory.create(name='ASSE')
        organization2 = OrganizationFactory.create(name='IRCAM')

        url = reverse('profiles:organizations_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(1, len(json_data))

        self.assertEqual(
            json_data,
            [
                {'id': elt.user.id, 'name': elt.name}
                for elt in [organization1]
            ]
        )

    def test_autocomplete_two_words(self):
        """it should return a json list"""

        individual1 = IndividualFactory.create(last_name='Allard', first_name='Bernard')
        individual2 = IndividualFactory.create(last_name='Allard', first_name='Alain')
        individual3 = IndividualFactory.create(last_name='Bernard', first_name='Alain')
        individual4 = IndividualFactory.create(last_name='Durand', first_name='Maurice')

        organization1 = OrganizationFactory.create(name='Allez les verts')
        organization2 = OrganizationFactory.create(name='IRCAM')

        url = reverse('profiles:organizations_autocomplete')

        response = self.client.get(url, data={'term': 'Allez les'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(0, len(json_data))

    def test_autocomplete_limit(self):
        """it should return a json list"""

        for index in range(25):
            OrganizationFactory.create(name='Allez{0}'.format(index))

        url = reverse('profiles:organizations_autocomplete')

        response = self.client.get(url, data={'term': 'A'})
        self.assertEqual(response.status_code, 200)

        json_data = json.loads(response.content)

        self.assertEqual(20, len(json_data))
