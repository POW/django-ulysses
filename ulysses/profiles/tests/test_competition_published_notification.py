# -*- coding: utf-8 -*-

from django.core.management import call_command
from django.core import mail
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase

from ulysses.reference.factories import CompetitionCategoryFactory
from ulysses.competitions.factories import CompetitionFactory
from ulysses.competitions.models import Competition

from ..factories import IndividualFactory, OrganizationFactory


class NotificationTest(BaseTestCase):

    def assertTextInHTML(self, text, email):
        self.assertEqual(email.alternatives[0][1], "text/html")
        self.assertTrue(email.alternatives[0][0].find(text) >= 0)

    def assertTextNotInHTML(self, text, email):
        self.assertEqual(email.alternatives[0][1], "text/html")
        self.assertTrue(email.alternatives[0][0].find(text) < 0)

    def test_notify_competition_open(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = OrganizationFactory.create()
        category = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category, open=True)
        profile1.competition_categories.add(category)
        profile1.save()
        profile3.competition_categories.add(category)
        profile3.save()
        call_command('notify_competition_published',  verbosity=0)
        self.assertEqual(len(mail.outbox), 2)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new competition has been published'))
        self.assertTextInHTML(competition.get_absolute_url(), digest_email)
        digest_email = mail.outbox[1]
        self.assertEqual(digest_email.to, [profile3.user.email])
        self.assertEqual(digest_email.subject, _('A new competition has been published'))
        self.assertTextInHTML(competition.get_absolute_url(), digest_email)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, True)

    def test_notify_several_competition_open(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        competition1 = CompetitionFactory.create(category=category, open=True)
        competition2 = CompetitionFactory.create(category=category, open=True)
        competition3 = CompetitionFactory.create(open=True)
        competition4 = CompetitionFactory.create(category=category, draft=True)
        profile1.competition_categories.add(category)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New competitions have been published'))
        # Check html content
        self.assertTextInHTML(competition1.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition2.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition4.get_absolute_url(), digest_email)
        # Check if status has been updated correctly
        competition1 = Competition.objects.get(id=competition1.id)
        self.assertEqual(competition1.publication_notified, True)
        competition2 = Competition.objects.get(id=competition2.id)
        self.assertEqual(competition2.publication_notified, True)
        competition3 = Competition.objects.get(id=competition3.id)
        self.assertEqual(competition3.publication_notified, True)
        competition4 = Competition.objects.get(id=competition4.id)
        self.assertEqual(competition4.publication_notified, False)
        # re-run command : it should not send email
        mail.outbox = []
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)

    def test_notify_subscribed_several_categories(self):
        profile1 = IndividualFactory.create()
        category1 = CompetitionCategoryFactory.create()
        category2 = CompetitionCategoryFactory.create()
        category3 = CompetitionCategoryFactory.create()
        competition1 = CompetitionFactory.create(category=category1, open=True)
        competition2 = CompetitionFactory.create(category=category2, open=True)
        competition3 = CompetitionFactory.create(open=True)
        competition4 = CompetitionFactory.create(category=category1, draft=True)
        competition5 = CompetitionFactory.create(category=category1, open=True)
        competition6 = CompetitionFactory.create(category=category3, open=True)
        profile1.competition_categories.add(category1)
        profile1.competition_categories.add(category2)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New competitions have been published'))
        # Check html content
        self.assertTextInHTML(competition1.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition2.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition4.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition5.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition6.get_absolute_url(), digest_email)
        # Check if status has been updated correctly
        competition1 = Competition.objects.get(id=competition1.id)
        self.assertEqual(competition1.publication_notified, True)
        competition2 = Competition.objects.get(id=competition2.id)
        self.assertEqual(competition2.publication_notified, True)
        competition3 = Competition.objects.get(id=competition3.id)
        self.assertEqual(competition3.publication_notified, True)
        competition4 = Competition.objects.get(id=competition4.id)
        self.assertEqual(competition4.publication_notified, False)
        competition5 = Competition.objects.get(id=competition5.id)
        self.assertEqual(competition5.publication_notified, True)
        competition6 = Competition.objects.get(id=competition6.id)
        self.assertEqual(competition6.publication_notified, True)
        # re-run command : it should not send email
        mail.outbox = []
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)

    def test_notify_competition_open_not_following(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category, open=True)
        # profile1.competition_categories.add(category)
        # profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, True)

    def test_notify_competition_open_other_category(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        category2 = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category2, open=True)
        profile1.competition_categories.add(category)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, True)

    def test_notify_competition_draft_category(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category, draft=True)
        profile1.competition_categories.add(category)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, False)

    def test_notify_competition_published_category(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category, published=True)
        profile1.competition_categories.add(category)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, False)

    def test_notify_competition_already_notified(self):
        profile1 = IndividualFactory.create()
        category = CompetitionCategoryFactory.create()
        competition = CompetitionFactory.create(category=category, open=True, publication_notified=True)
        profile1.competition_categories.add(category)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        competition = Competition.objects.get(id=competition.id)
        self.assertEqual(competition.publication_notified, True)

    def test_notify_competition_any_open(self):
        profile1 = IndividualFactory.create()
        any_category = CompetitionCategoryFactory.create(is_any=True)
        category1 = CompetitionCategoryFactory.create(is_any=False)
        competition1 = CompetitionFactory.create(category=any_category, open=True)
        profile1.competition_categories.add(category1)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new competition has been published'))
        # Check html content
        self.assertTextInHTML(competition1.get_absolute_url(), digest_email)
        cat_names = 'a new {0} call is published'.format(category1.name)
        self.assertTextInHTML(cat_names, digest_email)
        # Check if status has been updated correctly
        competition1 = Competition.objects.get(id=competition1.id)
        self.assertEqual(competition1.publication_notified, True)
        # re-run command : it should not send email
        mail.outbox = []
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)

    def test_dont_notify_competition_any_if_not_following(self):
        profile1 = IndividualFactory.create()
        any_category = CompetitionCategoryFactory.create(is_any=True)
        competition1 = CompetitionFactory.create(category=any_category, open=True)
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        # Check if status has been updated correctly
        competition1 = Competition.objects.get(id=competition1.id)
        self.assertEqual(competition1.publication_notified, True)
        self.assertEqual(len(mail.outbox), 0)

    def test_notify_subscribed_several_categories_any(self):
        profile1 = IndividualFactory.create()
        any_category = CompetitionCategoryFactory.create(is_any=True)
        category1 = CompetitionCategoryFactory.create(order=1)
        category2 = CompetitionCategoryFactory.create(order=2)
        category3 = CompetitionCategoryFactory.create(order=3)
        competition1 = CompetitionFactory.create(category=any_category, open=True)
        competition2 = CompetitionFactory.create(category=category2, open=True)
        competition3 = CompetitionFactory.create(open=True)
        competition4 = CompetitionFactory.create(category=any_category, draft=True)
        competition5 = CompetitionFactory.create(category=category1, open=True)
        competition6 = CompetitionFactory.create(category=category3, open=True)
        profile1.competition_categories.add(category1)
        profile1.competition_categories.add(category2)
        profile1.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New competitions have been published'))
        # Check html content
        cat_names = 'new {0} and {1} calls are published'.format(category1.name, category2.name)
        self.assertTextInHTML(cat_names, digest_email)
        self.assertTextInHTML(competition1.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition1.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition2.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition4.get_absolute_url(), digest_email)
        self.assertTextInHTML(competition5.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(competition6.get_absolute_url(), digest_email)
        # Check if status has been updated correctly
        competition1 = Competition.objects.get(id=competition1.id)
        self.assertEqual(competition1.publication_notified, True)
        competition2 = Competition.objects.get(id=competition2.id)
        self.assertEqual(competition2.publication_notified, True)
        competition3 = Competition.objects.get(id=competition3.id)
        self.assertEqual(competition3.publication_notified, True)
        competition4 = Competition.objects.get(id=competition4.id)
        self.assertEqual(competition4.publication_notified, False)
        competition5 = Competition.objects.get(id=competition5.id)
        self.assertEqual(competition5.publication_notified, True)
        competition6 = Competition.objects.get(id=competition6.id)
        self.assertEqual(competition6.publication_notified, True)
        # re-run command : it should not send email
        mail.outbox = []
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)

    def test_notify_several_profiles_any(self):
        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        any_category = CompetitionCategoryFactory.create(is_any=True)
        category1 = CompetitionCategoryFactory.create()
        category2 = CompetitionCategoryFactory.create()
        competition1 = CompetitionFactory.create(category=any_category, open=True)
        competition2 = CompetitionFactory.create(category=any_category, open=True)
        profile1.competition_categories.add(category1)
        profile1.competition_categories.add(category2)
        profile1.save()
        profile2.competition_categories.add(category2)
        profile2.save()
        call_command('notify_competition_published', verbosity=0)
        self.assertEqual(len(mail.outbox), 2)
        digest_emails = mail.outbox
        dests = [email.to[0] for email in digest_emails]
        self.assertTrue(profile1.user.email in dests)
        self.assertTrue(profile2.user.email in dests)
        self.assertFalse(profile3.user.email in dests)