# -*- coding: utf-8 -*-

import xlrd
from io import BytesIO

from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory, OrganizationFactory, ProfileUserFactory


class MembersExcelTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    @staticmethod
    def _read_excel(response):
        data_io = BytesIO(response.content)
        workbook = xlrd.open_workbook(file_contents=data_io.getvalue())
        worksheet = workbook.sheet_by_index(0)
        values = []
        line_index = 1  # ignore header
        while True:
            try:
                value1 = worksheet.cell(line_index, 0).value
                value2 = worksheet.cell(line_index, 1).value
                value3 = worksheet.cell(line_index, 2).value
                value4 = worksheet.cell(line_index, 3).value
            except IndexError:
                value1 = value2 = value3 = value4 = None
            if value1:
                values.append([value1, value2, value3, value4])
                line_index += 1
            else:
                break
        return values

    def test_export_no_members(self):
        """it should export empty file"""
        # individual = IndividualFactory.create(is_superuser=True)
        user = ProfileUserFactory.create(is_superuser=True)
        self.client.login(email=user.email, password="1234")
        url = reverse('profiles:export_members_to_excel')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lines = self._read_excel(response)
        self.assertEqual(len(lines), 0)

    def test_export_members_anonymous(self):
        """it should raise error"""
        # self.client.login(email=user.email, password="1234")
        url = reverse('profiles:export_members_to_excel')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_export_members_not_superuser(self):
        """it should raise error"""
        # individual = IndividualFactory.create(is_superuser=True)
        user = ProfileUserFactory.create(is_staff=True)
        self.client.login(email=user.email, password="1234")
        url = reverse('profiles:export_members_to_excel')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_export_members(self):
        """it should export members"""
        individual = IndividualFactory.create()
        organization = OrganizationFactory.create()
        user = ProfileUserFactory.create(is_superuser=True)
        self.client.login(email=user.email, password="1234")
        url = reverse('profiles:export_members_to_excel')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        lines = self._read_excel(response)
        self.assertEqual(len(lines), 2)
        self.assertEqual(lines[0], [_('Individual'), individual.last_name, individual.first_name, individual.email])
        self.assertEqual(
            lines[1], [_('Organization'), organization.name, '', organization.email]
        )
