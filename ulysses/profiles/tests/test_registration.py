# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import os

from django.contrib.auth.models import User
from django.core import mail
from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase

from ulysses.composers.factories import UserFactory
from ulysses.profiles.models import Organization, Individual
from ulysses.profiles.factories import IndividualFactory


class BaseRegistrationTest(BaseTestCase):

    def setUp(self):
        super(BaseRegistrationTest, self).setUp()
        os.environ['RECAPTCHA_DISABLE'] = 'True'

    def tearDown(self):
        try:
            del os.environ['RECAPTCHA_DISABLE']
        except KeyError:
            pass
        super(BaseRegistrationTest, self).tearDown()


class RegistrationTest(BaseRegistrationTest):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_register(self):
        """it should display register form"""

        url = reverse('registration_register')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        user_type_options = [
            tag["value"]
            for tag in soup.select("select#id_user_type option")
        ]

        self.assertEqual(user_type_options, ["individual", "organization"])

        field_names = (
            'last_name', 'first_name', 'organization_name', 'contact_name', 'password1', 'password2', 'username',
            'newsletter'
        )
        for field_name in field_names:
            self.assertEqual(len(soup.select("#id_" + field_name)), 1)

    def test_register_as_organization(self):
        """it should create user for an organization"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'organization',
            'organization_name': 'Dalton',
            'contact_name': 'Joe Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': True,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.last_name, post_data['organization_name'])
        self.assertEqual(user.first_name, '')
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 1)
        organization = Organization.objects.all()[0]
        self.assertEqual(organization.user, user)
        self.assertEqual(organization.ulysses_newsletter, True)
        self.assertEqual(organization.contact_name, post_data['contact_name'])

        response = self.client.get(reverse('registration_complete'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, post_data["email"])

    def test_register_as_individual(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        individual = Individual.objects.all()[0]
        self.assertEqual(individual.user, user)
        self.assertEqual(individual.ulysses_newsletter, False)
        self.assertEqual(Individual.objects.all()[0].user.is_active, False)

    def test_register_as_suspicious_individual(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(Individual.objects.all()[0].user, user)
        self.assertEqual(Individual.objects.all()[0].user.is_active, False)

    def test_register_as_organization_no_name(self):
        """it should show an error"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'organization',
            'organization_name': '',
            'contact_name': 'Joe Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_as_organization_no_contact_name(self):
        """it should show an error"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'organization',
            'organization_name': 'Dalton',
            'contact_name': '',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_as_individual_no_name(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'first_name': '',
            'last_name': '',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(2, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_as_organization_no_email(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'organization',
            'organization_name':'Dalton',
            'contact_name': 'Joe Dalton',
            'username': '',
            'email': '',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_as_individual_no_email(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'first_name': 'Joe',
            'last_name': 'Dalton',
            'username': '',
            'email': '',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_invalid_password1(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'A88776655'  # it should have a lowercase letter

        post_data = {
            'user_type': 'individual',
            'first_name': 'Joe',
            'last_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_invalid_password2(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'a7982bcd'  # missing uppercase

        post_data = {
            'user_type': 'individual',
            'first_name': 'Joe',
            'last_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_invalid_password3(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'Poseidon'  # missing number

        post_data = {
            'user_type': 'individual',
            'first_name': 'Joe',
            'last_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_invalid_password4(self):
        """it should show an error"""
        url = reverse('registration_register')

        password = 'Poseid7'  # it should be 8 characters long

        post_data = {
            'user_type': 'individual',
            'first_name': 'Joe',
            'last_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_existing_email(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        email = 'dalton@dalton.com'
        UserFactory.create(email=email)

        post_data = {
            'user_type': 'organization',
            'organization_name': 'Dalton',
            'contact_name': 'Joe Dalton',
            'username': '',
            'email': email,
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(1, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_very_long_name(self):
        """it should create user even if email is longer than 3O chars (limit for username)"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton' * 5 + "1",
            'first_name': 'Joe',
            'username': '',
            'email': 'joe@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        self.assertTrue(len(post_data['last_name']) > 30)
        response = self.client.post(url, post_data)
        # Check user
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_very_long_organization_name(self):
        """it should create user even if email is longer than 3O chars (limit for username)"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'organization',
            'name': 'Dalton' * 5 + "1",
            'contact_name': 'Joe D',
            'username': '',
            'email': 'joe@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        self.assertTrue(len(post_data['name']) > 30)
        response = self.client.post(url, post_data)
        # Check user
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_very_long_firstname(self):
        """it should create user even if email is longer than 3O chars (limit for username)"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe' * 10 + "1",
            'username': '',
            'email': 'joe@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        self.assertTrue(len(post_data['first_name']) > 30)
        response = self.client.post(url, post_data)
        # Check user
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_very_long_email(self):
        """it should create user even if email is longer than 3O chars (limit for username)"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'this-is-the-email-of-the-dalton-company-so-please-be-carefulr@dalton.com',
            'password1': password,
            'password2': password,
            'recaptcha_response_field': 'PASSED'
        }

        self.assertTrue(len(post_data['email']) > 30)
        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])
        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Individual.objects.all()[0].user, user)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_no_user_type(self):
        """it should create user even if email is longer than 3O chars (limit for username)"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            # 'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'joe@dalton.com',
            'password1': password,
            'password2': password,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)

    def test_register_no_captcha(self):
        """it should create user for an individual"""
        del os.environ['RECAPTCHA_DISABLE']

        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'dalton@dalton.com',
            'newsletter': False,
            'password1': password,
            'password2': '',
        }

        response = self.client.post(url, post_data)

        # Check user
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".field-error")))
        self.assertEqual(0, User.objects.count())
        self.assertEqual(mail.outbox, [])
        self.assertEqual(Individual.objects.count(), 0)
        self.assertEqual(Organization.objects.count(), 0)


class ActivationTest(BaseRegistrationTest):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_register_as_individual(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(Individual.objects.all()[0].user, user)

        activation_key = response.context["activation_key"]
        url = reverse('registration_activate', args=[activation_key])

        self.assertTrue(mail.outbox[0].body.find(url) >= 0)

        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('login'))
        user = User.objects.get(id=user.id)
        self.assertEqual(user.is_active, True)

        message = list(response.context['messages'])[0]
        self.assertEqual(message.tags, "success")
        self.assertTrue(_('Your account has been successfully activated. Please sign in.') in message.message)

    def test_register_as_suspicious_individual(self):
        """WARNING: We do not anymore consider these users has suspicious"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Dalton',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(Individual.objects.all()[0].user, user)

        activation_key = response.context["activation_key"]
        url = reverse('registration_activate', args=[activation_key])
        self.assertTrue(mail.outbox[0].body.find(url) >= 0)

        # This is now OK : The users with same value for last-name and first-name are not supicious anymore
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('login'))
        user = User.objects.get(id=user.id)
        self.assertEqual(user.is_active, True)

    def test_register_as_individual_wrong_key(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(Individual.objects.all()[0].user, user)

        activation_key = response.context["activation_key"]
        # reverse
        wrong_key = activation_key[::-1]
        url = reverse('registration_activate', args=[wrong_key])

        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(id=user.id)
        self.assertEqual(user.is_active, False)

    def test_register_as_individual_twice(self):
        """it should create user for an individual"""
        url = reverse('registration_register')
        password = 'Poseidon-1234'

        post_data = {
            'user_type': 'individual',
            'last_name': 'Dalton',
            'first_name': 'Joe',
            'username': '',
            'email': 'dalton@dalton.com',
            'password1': password,
            'password2': '',
            'newsletter': False,
            'recaptcha_response_field': 'PASSED'
        }

        response = self.client.post(url, post_data)
        self.assertRedirects(response, reverse('registration_complete'))

        # Check user
        user = User.objects.get(email=post_data["email"])
        self.assertEqual(user.is_active, False)
        self.assertNotEqual(user.username, '')
        self.assertEqual(user.first_name, post_data['first_name'])
        self.assertEqual(user.last_name, post_data['last_name'])
        self.assertEqual(mail.outbox[0].to, [post_data["email"]])

        self.assertEqual(Individual.objects.count(), 1)
        self.assertEqual(Organization.objects.count(), 0)
        self.assertEqual(Individual.objects.all()[0].user, user)

        activation_key = response.context["activation_key"]
        url = reverse('registration_activate', args=[activation_key])

        self.assertTrue(mail.outbox[0].body.find(url) >= 0)

        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('login'))
        user = User.objects.get(id=user.id)
        self.assertEqual(user.is_active, True)

        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        user = User.objects.get(id=user.id)
        self.assertEqual(user.is_active, True)
        self.assertContains(response, "window.location = '{0}';".format(reverse('login')))

        response = self.client.get(reverse('login'))
        self.assertEqual(response.status_code, 200)
        message = list(response.context['messages'])[0]
        self.assertEqual(message.tags, "error")
        text = _('This activation link has been already used or has expired. Please try to sign in or re-register.')
        self.assertTrue(text in message.message)


class LogoutTest(BaseRegistrationTest):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_logout(self):
        """it should create user for an individual"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")

        url = reverse('logout')
        response = self.client.get(url, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertRedirects(response, reverse('web:home'))

        user = response.context['user']
        self.assertEqual(user.is_authenticated, False)

        message = list(response.context['messages'])[0]
        self.assertEqual(message.tags, "success")
        self.assertTrue(_('You are now logged out.') in message.message)
