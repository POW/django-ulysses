# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ulysses.competitions.factories import CompetitionFactory, CandidateFactory
from ulysses.composers.factories import (
    ComposerFactory, DocumentFactory as ComposerDocumentFactory, BiographicElementFactory
)
from ulysses.reference.factories import CitizenshipFactory, CompetitionCategoryFactory
from ulysses.events.models import Event, MemberCalendarItem
from ulysses.events.factories import EventFactory, MemberCalendarItemFactory, UserEventAlertFactory
from ulysses.social.models import (
    MemberRecommendation, WorkRecommendation, WorkBookmark, Following, MemberPost, FeedItem, Message, Document
)
from ulysses.social.factories import (
    WorkRecommendationFactory, WorkBookmarkFactory, FollowingFactory, MemberRecommendationFactory, MemberPostFactory,
    FeedItemFactory, DocumentFactory, MessageFactory, MemberBookmarkFactory
)
from ulysses.works.models import Work
from ulysses.works.factories import WorkFactory
from ulysses.web.models import NewsletterSubscription

from ..factories import (
    OrganizationFactory, IndividualFactory, ProfileUserFactory, OrganizationTypesFactory, InterestFactory,
)
from ..models import (
    Organization, Individual, OrganizationUrl, IndividualUrl, OrganizationMember
)


class PersonalSpaceTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_profile_as_organization(self):
        """it should display organization page on personal space"""

        organization = OrganizationFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:personal_space')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, organization.name)
        self.assertContains(response, organization.user.email)

    def test_view_profile_as_individual(self):
        """it should display individual page on personal space"""

        individual = IndividualFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:personal_space')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, individual.user.first_name)
        self.assertContains(response, individual.user.last_name.upper())
        self.assertContains(response, individual.user.email)

    def test_view_profile_as_anonymous(self):
        """it should display login form"""
        url = reverse('profiles:personal_space')
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_profile_as_no_profile_user(self):
        """it should display login form"""
        user = ProfileUserFactory.create()
        self.client.login(email=user.email, password="1234")
        url = reverse('profiles:personal_space')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class EditProfileTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def _get_initial_data(self, url):
        """get the form and extract initial value"""
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        hiddens = [(field['name'], field.get('value')) for field in soup.select("input[type=hidden]")]
        data = {}
        for field_name, field_value in hiddens:
            if field_value is not None:
                data[field_name] = field_value
        data.pop("csrfmiddlewaretoken")
        return data

    def test_view_organization_form(self):
        """it should show the form with the request fields"""

        organization = OrganizationFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(soup.select("#id_name")[0]["type"], "text")
        self.assertEqual(soup.select("#id_name")[0]["value"], organization.name)
        self.assertEqual(len(soup.select("#id_last_name")), 0)
        self.assertEqual(len(soup.select("#id_first_name")), 0)

    def test_view_organization_private_form(self):
        """it should show the form with the request fields"""

        organization = OrganizationFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:edit_private_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(soup.select("#id_address1")[0]["type"], "text")
        self.assertEqual(soup.select("#id_address1")[0]["value"], organization.address1)
        self.assertEqual(soup.select("#id_address2")[0]["type"], "text")
        self.assertEqual(soup.select("#id_address2")[0]["value"], organization.address2)
        self.assertEqual(soup.select("#id_zipcode")[0]["type"], "text")
        self.assertEqual(soup.select("#id_zipcode")[0]["value"], organization.zipcode)
        self.assertEqual(soup.select("#id_phone1")[0]["type"], "text")
        self.assertEqual(soup.select("#id_phone1")[0]["value"], organization.phone1)
        self.assertEqual(soup.select("#id_phone2")[0]["type"], "text")
        self.assertEqual(soup.select("#id_phone2")[0]["value"], organization.phone2)

    def test_view_individual_form(self):
        """it should show the form with the request fields"""

        individual = IndividualFactory.create()
        url1 = IndividualUrl.objects.create(individual=individual, url="http://www.toto.fr")
        url2 = IndividualUrl.objects.create(individual=individual, url="http://www.titi.fr")

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(soup.select("#id_last_name")[0]["type"], "text")
        self.assertEqual(soup.select("#id_last_name")[0]["value"], individual.user.last_name)
        self.assertEqual(soup.select("#id_first_name")[0]["type"], "text")
        self.assertEqual(soup.select("#id_first_name")[0]["value"], individual.user.first_name)
        self.assertEqual(soup.select("#id_link1")[0]["value"], url1.url)
        self.assertEqual(soup.select("#id_link2")[0]["value"], url2.url)
        self.assertEqual(len(soup.select("#id_description")), 0)
        self.assertEqual(len(soup.select("#id_name")), 0)

    def test_view_individual_private_form(self):
        """it should show the form with the request fields"""

        individual = IndividualFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_private_profile')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(soup.select("#id_address1")[0]["type"], "text")
        self.assertEqual(soup.select("#id_address1")[0]["value"], individual.address1)
        self.assertEqual(soup.select("#id_address2")[0]["type"], "text")
        self.assertEqual(soup.select("#id_address2")[0]["value"], individual.address2)
        self.assertEqual(soup.select("#id_zipcode")[0]["type"], "text")
        self.assertEqual(soup.select("#id_zipcode")[0]["value"], individual.zipcode)
        self.assertEqual(soup.select("#id_phone1")[0]["type"], "text")
        self.assertEqual(soup.select("#id_phone1")[0]["value"], individual.phone1)
        self.assertEqual(soup.select("#id_phone2")[0]["type"], "text")
        self.assertEqual(soup.select("#id_phone2")[0]["value"], individual.phone2)

    def test_edit_organization(self):
        """it should change the name"""

        organization = OrganizationFactory.create()
        org_type1 = OrganizationTypesFactory.create()
        org_type2 = OrganizationTypesFactory.create()
        org_type3 = OrganizationTypesFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        data = {
            'name': 'New name of my organization',
            'organization_types': [org_type1.id, org_type3.id],
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, organization.get_absolute_url())

        organization = Organization.objects.get(id=organization.id)

        self.assertEqual(data['name'], organization.name)
        self.assertEqual([org_type1, org_type3], list(organization.organization_types.all()))
        self.assertEqual(data['name'], organization.user.last_name)
        self.assertEqual('', organization.user.first_name)

    def test_edit_organization_name_too_long(self):
        """it should not change the name"""

        organization = OrganizationFactory.create()
        org_type1 = OrganizationTypesFactory.create()
        org_type2 = OrganizationTypesFactory.create()
        org_type3 = OrganizationTypesFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        data = {
            'name': 'A' * 31,
            'organization_types': [org_type1.id, org_type3.id],
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, organization.get_absolute_url())

        organization = Organization.objects.get(id=organization.id)

        self.assertEqual(data['name'], organization.name)
        self.assertEqual([org_type1, org_type3], list(organization.organization_types.all()))
        self.assertEqual('A' * 30, organization.user.last_name)
        self.assertEqual('', organization.user.first_name)

    def test_edit_private_organization(self):
        """it should change the name"""

        organization = OrganizationFactory.create()

        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:edit_private_profile')

        data = {
            'address1': '1 rue',
            'address2': '',
            'zipcode': '42810',
            'phone1': '1234',
            'phone2': '+33456'
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, organization.get_absolute_url())

        organization = Organization.objects.get(id=organization.id)

        self.assertEqual(data['address1'], organization.address1)
        self.assertEqual(data['address2'], organization.address2)
        self.assertEqual(data['zipcode'], organization.zipcode)
        self.assertEqual(data['phone1'], organization.phone1)
        self.assertEqual(data['phone2'], organization.phone2)

    def test_edit_organization_anonymous(self):
        """it should change the name"""

        organization = OrganizationFactory.create()

        url = reverse('profiles:edit_profile')

        data = {
            'name': 'New name of my organization',
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

        organization = Organization.objects.get(id=organization.id)

        self.assertNotEqual(data['name'], organization.name)

    def test_edit_organization_private_anonymous(self):
        """it should change the name"""

        organization = OrganizationFactory.create()

        url = reverse('profiles:edit_private_profile')

        data = {
            'address1': '1 rue',
            'address2': '',
            'zipcode': '42810',
            'phone1': '1234',
            'phone2': '+33456'
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

        organization = Organization.objects.get(id=organization.id)

        self.assertNotEqual(data['address1'], organization.address1)

    def test_edit_individual(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        url1 = IndividualUrl.objects.create(individual=individual, url="http://www.toto.fr")
        url2 = IndividualUrl.objects.create(individual=individual, url="http://www.titi.fr")

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        data = {
            'last_name': 'Vidual',
            'first_name': 'Indy',
            'birth_date': date(1982, 2, 20),
            'organizations': [organization1.id, organization2.id],
            'citizenship': citizenship1.id,
            'link1': 'http://www.hello.fr',
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, individual.get_absolute_url())

        individual = Individual.objects.get(id=individual.id)

        self.assertEqual(data['last_name'], individual.user.last_name)
        self.assertEqual(data['first_name'], individual.user.first_name)
        self.assertEqual(data['birth_date'], individual.birth_date)
        self.assertEqual(2, individual.organizations.count())
        self.assertEqual(citizenship1, individual.citizenship)
        self.assertEqual(None, individual.dual_citizenship)
        self.assertEqual([data['link1']], [url.url for url in individual.urls()])

    def test_edit_individual_minimum(self):
        """it should not change the name"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")
        url = reverse('profiles:edit_profile')
        data = {
            'last_name': 'A' * 30,
            'first_name': 'B' * 30,
            'birth_date': date(1982, 2, 20),
        }
        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, individual.get_absolute_url())
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(data['last_name'], individual.user.last_name)
        self.assertEqual(data['first_name'], individual.user.first_name)

    def test_edit_individual_last_name_too_long(self):
        """it should not change the name"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")
        url = reverse('profiles:edit_profile')
        data = {
            'last_name': 'A' * 31,
            'first_name': 'Indy',
            'birth_date': date(1982, 2, 20),
        }
        response = self.client.post(url, data=data, follow=False)
        self.assertEqual(response.status_code, 200)
        individual = Individual.objects.get(id=individual.id)
        self.assertNotEqual(data['last_name'], individual.user.last_name)
        self.assertNotEqual(data['first_name'], individual.user.first_name)

    def test_edit_individual_first_name_too_long(self):
        """it should not change the name"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")
        url = reverse('profiles:edit_profile')
        data = {
            'last_name': 'Hello',
            'first_name': 'A' * 31,
            'birth_date': date(1982, 2, 20),
        }
        response = self.client.post(url, data=data, follow=False)
        self.assertEqual(response.status_code, 200)
        individual = Individual.objects.get(id=individual.id)
        self.assertNotEqual(data['last_name'], individual.user.last_name)
        self.assertNotEqual(data['first_name'], individual.user.first_name)

    def test_edit_individual_link_too_long(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        url1 = IndividualUrl.objects.create(individual=individual, url="http://www.toto.fr")

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_profile')
        long_query_param = 'a' * 250

        data = {
            'last_name': 'Vidual',
            'first_name': 'Indy',
            'birth_date': date(1982, 2, 20),
            'organizations': [organization1.id, organization2.id],
            'citizenship': citizenship1.id,
            'link1': 'https://www.toto.fr?t={0}'.format(long_query_param),
        }

        response = self.client.post(url, data=data, follow=False)
        self.assertEqual(response.status_code, 200)

        individual = Individual.objects.get(id=individual.id)
        self.assertEqual([url1.url], [url.url for url in individual.urls()])

    def test_edit_individual_link_not_too_long(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        url1 = IndividualUrl.objects.create(individual=individual, url="http://www.toto.fr")

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_profile')
        long_query_param = 'a' * 200

        data = {
            'last_name': 'Vidual',
            'first_name': 'Indy',
            'birth_date': date(1982, 2, 20),
            'organizations': [organization1.id, organization2.id],
            'citizenship': citizenship1.id,
            'link1': 'https://www.toto.fr?t={0}'.format(long_query_param),
        }

        response = self.client.post(url, data=data, follow=False)
        self.assertRedirects(response, individual.get_absolute_url())

        individual = Individual.objects.get(id=individual.id)
        self.assertEqual([data['link1']], [url.url for url in individual.urls()])

    def test_edit_private_individual(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_private_profile')

        data = {
            'address1': '1 rue',
            'address2': '',
            'zipcode': '42810',
            'phone1': '1234',
            'phone2': '+33456'
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, individual.get_absolute_url())

        individual = Individual.objects.get(id=individual.id)

        self.assertEqual(data['address1'], individual.address1)
        self.assertEqual(data['address2'], individual.address2)
        self.assertEqual(data['zipcode'], individual.zipcode)
        self.assertEqual(data['phone1'], individual.phone1)
        self.assertEqual(data['phone2'], individual.phone2)

    def test_edit_private_individual_anonymous(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        url = reverse('profiles:edit_private_profile')

        data = {
            'address1': '1 rue',
            'address2': '',
            'zipcode': '42810',
            'phone1': '1234',
            'phone2': '+33456'
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

        individual = Individual.objects.get(id=individual.id)

        self.assertNotEqual(data['address1'], individual.address1)

    def test_edit_individual_dual_citizenship(self):
        """it should change the name"""

        individual = IndividualFactory.create()

        organization1 = OrganizationFactory.create()
        organization2 = OrganizationFactory.create()
        organization3 = OrganizationFactory.create()

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()

        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:edit_profile')

        data = {
            'last_name': 'Vidual',
            'first_name': 'Indy',
            'birth_date': date(1982, 2, 20),
            'organizations': [organization1.id, organization2.id],
            'citizenship': citizenship1.id,
            'dual_citizenship': citizenship2.id,
        }

        response = self.client.post(url, data=data, follow=True)
        self.assertRedirects(response, individual.get_absolute_url())

        individual = Individual.objects.get(id=individual.id)

        self.assertEqual(data['last_name'], individual.user.last_name)
        self.assertEqual(data['first_name'], individual.user.first_name)
        self.assertEqual(data['birth_date'], individual.birth_date)
        self.assertEqual(2, individual.organizations.count())
        self.assertEqual(citizenship1, individual.citizenship)
        self.assertEqual(citizenship2, individual.dual_citizenship)


class EditProfilePreferencesTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_organization_form(self):
        """it should show the form with the request fields"""
        organization = OrganizationFactory.create()
        self.client.login(email=organization.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        any_category = CompetitionCategoryFactory.create(is_any=True)
        category1 = CompetitionCategoryFactory.create()
        category2 = CompetitionCategoryFactory.create()
        category3 = CompetitionCategoryFactory.create()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, category1.name)
        self.assertContains(response, category2.name)
        self.assertContains(response, category3.name)
        self.assertNotContains(response, any_category.name)

    def test_view_individual_form(self):
        """it should show the form with the request fields"""
        individual = IndividualFactory.create()
        any_category = CompetitionCategoryFactory.create(is_any=True)
        category1 = CompetitionCategoryFactory.create()
        category2 = CompetitionCategoryFactory.create()
        category3 = CompetitionCategoryFactory.create()
        self.client.login(email=individual.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, category1.name)
        self.assertContains(response, category2.name)
        self.assertContains(response, category3.name)
        self.assertNotContains(response, any_category.name)

    def test_edit_organization(self):
        """it should change the name"""
        organization = OrganizationFactory.create()
        category1 = CompetitionCategoryFactory.create(order=1)
        category2 = CompetitionCategoryFactory.create(order=2)
        category3 = CompetitionCategoryFactory.create(order=3)
        self.client.login(email=organization.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        data = {
            'accept_newsletter': True,
            'accept_messages_digest': True,
            'competition_categories': [category1.id, category3.id],
        }
        response = self.client.post(url, data=data, follow=True)
        assert_popup_redirects(response, reverse('profiles:personal_space'))
        organization = Organization.objects.get(id=organization.id)
        self.assertEqual(data['accept_newsletter'], organization.ulysses_newsletter)
        self.assertEqual(data['accept_messages_digest'], organization.accept_messages_digest)
        self.assertEqual([elt.id for elt in organization.competition_categories.all()], [category1.id, category3.id])

    def test_edit_individual(self):
        """it should change the name"""
        individual = IndividualFactory.create()
        category1 = CompetitionCategoryFactory.create(order=1)
        category2 = CompetitionCategoryFactory.create(order=2)
        category3 = CompetitionCategoryFactory.create(order=3)
        self.client.login(email=individual.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        data = {
            'accept_newsletter': True,
            'accept_messages_digest': True,
            'competition_categories': [category1.id, category3.id],
        }
        response = self.client.post(url, data=data, follow=True)
        assert_popup_redirects(response, reverse('profiles:personal_space'))
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(data['accept_newsletter'], individual.ulysses_newsletter)
        self.assertEqual(data['accept_messages_digest'], individual.accept_messages_digest)
        self.assertEqual([elt.id for elt in individual.competition_categories.all()], [category1.id, category3.id])

    def test_edit_organization_anonymous(self):
        """it should change the name"""
        organization = OrganizationFactory.create()
        category1 = CompetitionCategoryFactory.create(order=1)
        category2 = CompetitionCategoryFactory.create(order=2)
        category3 = CompetitionCategoryFactory.create(order=3)
        # self.client.login(email=organization.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        data = {
            'accept_newsletter': True,
            'accept_messages_digest': True,
            'competition_categories': [category1.id, category3.id],
        }
        response = self.client.post(url, data=data, follow=True)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))
        organization = Organization.objects.get(id=organization.id)
        self.assertEqual(organization.competition_categories.count(), 0)

    def test_edit_individual_anonymous(self):
        """it should change the name"""
        individual = IndividualFactory.create()
        category1 = CompetitionCategoryFactory.create(order=1)
        category2 = CompetitionCategoryFactory.create(order=2)
        category3 = CompetitionCategoryFactory.create(order=3)
        # self.client.login(email=organization.user.email, password="1234")
        url = reverse('profiles:profile_preferences')
        data = {
            'accept_newsletter': True,
            'accept_messages_digest': True,
            'competition_categories': [category1.id, category3.id],
        }
        response = self.client.post(url, data=data, follow=True)
        assert_popup_redirects(response, reverse('login') + "?next={0}".format(url))
        individual = Individual.objects.get(id=individual.id)
        self.assertEqual(individual.competition_categories.count(), 0)


class PersonalSpaceCallsTest(BaseTestCase):
    """Calls Tab of the personal space"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_view_no_calls(self):
        """it should display calls tab with no competitions"""

        organization = OrganizationFactory.create()
        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:personal_space') + "?tab=applications"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")

        self.assertEqual(1, len(soup.select(".applications-tab")))

    def test_view_competitions_as_candidate(self):
        """it should display competitions tab with competitions"""

        individual = IndividualFactory.create(biography="About me")
        self.client.login(email=individual.user.email, password="1234")

        comp1 = CompetitionFactory.create(open=True, title='Abcd')
        comp2 = CompetitionFactory.create(open=True, title='Efgh')
        comp3 = CompetitionFactory.create(open=True, title='Ijkl')

        composer = ComposerFactory.create(user=individual.user)
        for comp in (comp1, comp2):
            CandidateFactory.create(composer=composer, competition=comp)

        url = reverse('profiles:personal_space') + "?tab=applications"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")

        self.assertEqual(1, len(soup.select(".applications-tab")))
        self.assertContains(response, comp1.title)
        self.assertContains(response, comp2.title)
        self.assertNotContains(response, comp3.title)

    def test_view_bookmarks(self):
        """it should display calls tab with bookmarks"""

        organization = OrganizationFactory.create()
        self.client.login(email=organization.user.email, password="1234")

        url = reverse('profiles:personal_space') + "?tab=bookmarks"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")

        self.assertEqual(1, len(soup.select(".bookmarks-tab")))
        self.assertEqual(0, len(soup.select(".list-item .member-item")))
        self.assertEqual(0, len(soup.select(".works-list .item")))
        self.assertEqual(0, len(soup.select(".event_alerts-list .event_alerts-list-item")))

    def test_view_bookmarks_empty(self):
        """it should display calls tab without any bookmarks"""

        profile = IndividualFactory.create()
        self.client.login(email=profile.user.email, password="1234")

        work1 = WorkFactory.create()
        work2 = WorkFactory.create()
        work3 = WorkFactory.create()
        WorkBookmarkFactory.create(work=work1, member_user=profile.user)
        WorkBookmarkFactory.create(work=work2)

        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        MemberBookmarkFactory.create(member_user=profile2.user, bookmarked_by=profile.user)
        MemberBookmarkFactory.create(member_user=profile.user, bookmarked_by=profile3.user)

        alert1 = UserEventAlertFactory.create(user=profile.user)
        alert2 = UserEventAlertFactory.create()

        url = reverse('profiles:personal_space') + "?tab=bookmarks"

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select(".bookmarks-tab")))
        self.assertEqual(1, len(soup.select(".list-item .member-item")))
        self.assertContains(response, profile2.name)
        self.assertNotContains(response, profile3.name)

        self.assertEqual(1, len(soup.select(".works-list .item")))
        text = str(soup.select(".works-list .item"))
        self.assertTrue(text.find(work1.title) >= 0)
        self.assertTrue(text.find(work2.title) < 0)
        self.assertTrue(text.find(work3.title) < 0)

        self.assertEqual(1, len(soup.select(".event_alerts-list .event_alerts-list-item")))
        text = str(soup.select(".event_alerts-list .event_alerts-list-item"))
        self.assertTrue(text.find(alert1.label) >= 0)
        self.assertTrue(text.find(alert1.get_absolute_url()) >= 0)
        self.assertTrue(text.find(alert2.label) < 0)


class PersonalSpacePortfolioTest(BaseTestCase):
    """Calls Tab of the personal space"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_view_portfolio(self):
        """it should display documents in portfolio"""
        individual = IndividualFactory.create()
        composer = ComposerFactory.create(user=individual.user)
        self.client.login(email=individual.user.email, password="1234")

        ComposerDocumentFactory.create(composer=composer)
        BiographicElementFactory.create(composer=composer)

        url = reverse('profiles:personal_space') + "?tab=portfolio"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")

        documents = soup.select('.personal-space-documents-section')[0]
        self.assertEqual(1, len(documents.select('.portfolio-line')))

        biographies = soup.select('.personal-space-bio-section')[0]
        self.assertEqual(1, len(biographies.select('.portfolio-line')))

    def test_view_portfolio_empty(self):
        """it should display calls tab without any bookmarks"""
        individual = IndividualFactory.create()
        composer = ComposerFactory.create(user=individual.user)
        self.client.login(email=individual.user.email, password="1234")

        other_composer = ComposerFactory.create()
        ComposerDocumentFactory.create(composer=other_composer)
        BiographicElementFactory.create(composer=other_composer)

        url = reverse('profiles:personal_space') + "?tab=portfolio"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")

        documents = soup.select('.personal-space-documents-section')[0]
        self.assertEqual(0, len(documents.select('.portfolio-line')))

        biographies = soup.select('.personal-space-bio-section')[0]
        self.assertEqual(0, len(biographies.select('.portfolio-line')))

    def test_view_portfolio_not_a_composer(self):
        """it should display calls tab without any bookmarks"""
        individual = IndividualFactory.create()
        self.client.login(email=individual.user.email, password="1234")

        url = reverse('profiles:personal_space') + "?tab=portfolio"
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")

        documents = soup.select('.personal-space-documents-section')[0]
        self.assertEqual(0, len(documents.select('.portfolio-line')))

        biographies = soup.select('.personal-space-bio-section')[0]
        self.assertEqual(0, len(biographies.select('.portfolio-line')))


class SwitchProfileUrlTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_switch_organization(self):
        """it should delete an url"""

        ircam_url = "https://www.ircam.fr/"
        organization = OrganizationFactory.create()
        user = organization.user

        OrganizationUrl.objects.create(organization=organization, url=ircam_url)

        interest = InterestFactory.create()
        organization.interests.add(interest)
        organization.save()

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:switch_profile')

        data = {
            'confirm': 'confirm',
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(0, Organization.objects.count())
        self.assertEqual(1, Individual.objects.count())
        individual = Individual.objects.all()[0]
        self.assertEqual(individual.user, user)

        assert_popup_redirects(response, individual.get_absolute_url())

        self.assertEqual(0, OrganizationUrl.objects.count())
        self.assertEqual(1, IndividualUrl.objects.filter(individual=individual, url=ircam_url).count())
        self.assertEqual(1, individual.interests.count())

    def test_switch_individual(self):
        """it should delete an url"""

        ircam_url = "https://www.ircam.fr/"
        individual = IndividualFactory.create()
        user = individual.user

        IndividualUrl.objects.create(individual=individual, url=ircam_url)

        interest = InterestFactory.create()
        individual.interests.add(interest)
        individual.save()

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:switch_profile')

        data = {
            'confirm': 'confirm',
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(0, Individual.objects.count())
        self.assertEqual(1, Organization.objects.count())
        organization = Organization.objects.all()[0]
        self.assertEqual(organization.user, user)

        assert_popup_redirects(response, organization.get_absolute_url())

        self.assertEqual(0, IndividualUrl.objects.count())
        self.assertEqual(1, OrganizationUrl.objects.filter(organization=organization, url=ircam_url).count())
        self.assertEqual(1, organization.interests.count())

    def test_view_switch_organization(self):
        """it should delete an url"""

        ircam_url = "https://www.ircam.fr/"
        organization = OrganizationFactory.create()
        user = organization.user

        OrganizationUrl.objects.create(organization=organization, url=ircam_url)

        interest = InterestFactory.create()
        organization.interests.add(interest)
        organization.save()

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:switch_profile')

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, Organization.objects.count())
        self.assertEqual(0, Individual.objects.count())

    def test_view_switch_individual(self):
        """it should delete an url"""

        individual = IndividualFactory.create()
        user = individual.user

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:switch_profile')

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(0, Organization.objects.count())
        self.assertEqual(1, Individual.objects.count())

    def test_view_switch_anonymous(self):
        """it should delete an url"""

        organization = OrganizationFactory.create()

        url = reverse('profiles:switch_profile')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + '?next={0}'.format(url))

        self.assertEqual(1, Organization.objects.count())
        self.assertEqual(0, Individual.objects.count())


class DeleteProfileTest(BaseTestCase):
    """Public profile of a member"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_delete_organization(self):
        """it should delete an url"""

        organization = OrganizationFactory.create()
        user = organization.user

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:delete_profile')

        data = {
            'confirm': 'confirm',
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(0, Organization.objects.count())

        assert_popup_redirects(response, reverse('web:home'))

    def test_delete_individual(self):
        """it should delete an url"""

        individual = IndividualFactory.create()
        user = individual.user

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:delete_profile')

        data = {
            'confirm': 'confirm',
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(0, Individual.objects.count())

        assert_popup_redirects(response, reverse('web:home'))

    def test_view_delete_profile(self):
        """it should delete an url"""

        organization = OrganizationFactory.create()
        user = organization.user

        self.client.login(email=user.email, password="1234")

        url = reverse('profiles:delete_profile')

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, Organization.objects.count())

    def test_view_delete_anonymous(self):
        """it should delete an url"""

        organization = OrganizationFactory.create()

        url = reverse('profiles:delete_profile')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login') + '?next={0}'.format(url))

        self.assertEqual(1, Organization.objects.count())
        self.assertEqual(0, Individual.objects.count())

    def test_delete_dependencies(self):
        """it should delete profile"""

        individual = IndividualFactory.create()
        user = individual.user

        self.client.login(email=user.email, password="1234")

        organization = OrganizationFactory.create()

        work_as_owner = WorkFactory.create(owner=user)
        work_as_composer = WorkFactory.create(composer=user)
        work_as_performer = WorkFactory.create(performers=[individual])
        work_as_participant = WorkFactory.create(participants=[individual])
        bookmarked_work = WorkFactory.create()
        WorkBookmarkFactory.create(work=bookmarked_work, member_user=user)
        recommended_work = WorkFactory.create()
        WorkRecommendationFactory.create(work=recommended_work, recommended_by=user)

        followed_member = IndividualFactory.create()
        FollowingFactory.create(member_user=followed_member.user, follower=user)
        recommended_member = IndividualFactory.create()
        MemberRecommendationFactory.create(member_user=recommended_member.user, recommended_by=user)

        event_as_owner = EventFactory.create(owner=user)
        event_as_performer = EventFactory.create(performers=[individual])
        event_as_collaborator = EventFactory.create(collaborators=[individual])
        event_as_organizator = EventFactory.create(organizators=[individual])
        bookmarked_event = EventFactory.create()
        MemberCalendarItemFactory.create(event=bookmarked_event, user=user)

        NewsletterSubscription.objects.create(email=user.email)

        post_as_owner = MemberPostFactory.create(owner=user)

        feed_as_owner = FeedItemFactory.create(owner=user)

        doc_as_owner = DocumentFactory.create(owner=user)
        doc_as_allowed = DocumentFactory.create(allowed_users=[user])

        msg_as_sender = MessageFactory.create(sender=user)
        msg_as_recipient = MessageFactory.create(recipients=[user])

        OrganizationMember.objects.create(organization=organization, user=individual.user, confirmed=True)

        url = reverse('profiles:delete_profile')

        data = {
            'confirm': 'confirm',
        }

        response = self.client.post(url, data=data, follow=True)

        self.assertEqual(0, Individual.objects.filter(id=user.id).count())

        assert_popup_redirects(response, reverse('web:home'))

        self.assertEqual(0, Work.objects.filter(id=work_as_owner.id).count())
        self.assertEqual(0, Following.objects.all().count())
        self.assertEqual(0, MemberRecommendation.objects.all().count())
        self.assertEqual(0, WorkBookmark.objects.all().count())
        self.assertEqual(0, WorkRecommendation.objects.all().count())
        self.assertEqual(0, Event.objects.filter(id=event_as_owner.id).count())
        self.assertEqual(0, MemberCalendarItem.objects.all().count())
        self.assertEqual(0, MemberPost.objects.filter(id=post_as_owner.id).count())
        self.assertEqual(0, FeedItem.objects.filter(id=feed_as_owner.id).count())
        self.assertEqual(0, OrganizationMember.objects.all().count())
        self.assertEqual(0, Document.objects.filter(id=doc_as_owner.id).count())
        self.assertEqual(0, Message.objects.filter(id=msg_as_sender.id).count())
        self.assertEqual(1, NewsletterSubscription.objects.count())
        subscription = NewsletterSubscription.objects.all()[0]
        self.assertEqual(subscription.deleted_account, True)

        self.assertEqual(1, Work.objects.filter(id=work_as_composer.id).count())
        self.assertEqual(1, Work.objects.filter(id=work_as_performer.id).count())
        self.assertEqual(1, Work.objects.filter(id=work_as_participant.id).count())
        self.assertEqual(1, Work.objects.filter(id=bookmarked_work.id).count())
        self.assertEqual(1, Work.objects.filter(id=recommended_work.id).count())
        self.assertEqual(1, Organization.objects.filter(id=organization.id).count())
        self.assertEqual(1, Individual.objects.filter(id=followed_member.id).count())
        self.assertEqual(1, Individual.objects.filter(id=recommended_member.id).count())
        self.assertEqual(1, Event.objects.filter(id=event_as_collaborator.id).count())
        self.assertEqual(1, Event.objects.filter(id=event_as_performer.id).count())
        self.assertEqual(1, Event.objects.filter(id=event_as_organizator.id).count())
        self.assertEqual(1, Event.objects.filter(id=bookmarked_event.id).count())
        self.assertEqual(1, Document.objects.filter(id=doc_as_allowed.id).count())
        self.assertEqual(1, Message.objects.filter(id=msg_as_recipient.id).count())
