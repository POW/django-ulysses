# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, datetime

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import (
    IndividualFactory, OrganizationFactory, InterestFactory, OrganizationTypesFactory, RolesFactory, CitizenshipFactory,
    ComposerInstrumentFactory
)
from ulysses.reference.factories import CountryFactory
from ulysses.social.factories import FollowingFactory


class MembersTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]
    PAGE_SIZE = 32
    PAGE_SIZE_SM = 16

    def test_view_members_no_members(self):
        """it should display members"""

        url = reverse('profiles:members')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 0)

    def test_view_members_anonymous(self):
        """it should display members"""

        individuals = IndividualFactory.create_batch(6)
        organizations = OrganizationFactory.create_batch(5)

        url = reverse('profiles:members')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 11)

        for individual in individuals:
            self.assertContains(response, individual.get_absolute_url())

        for organization in organizations:
            self.assertContains(response, organization.get_absolute_url())

    def test_view_members_inactive(self):
        """it should display members"""

        individual = IndividualFactory.create()
        individual.user.is_active = False
        individual.user.save()

        url = reverse('profiles:members')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 0)

    def test_view_members_anonymous_many_members(self):
        """it should display members"""

        IndividualFactory.create_batch(60)
        OrganizationFactory.create_batch(60)

        url = reverse('profiles:members')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), self.PAGE_SIZE)
        self.assertEqual(len(soup.select(".hide-on-md .list-item")), self.PAGE_SIZE_SM)

    def test_view_members_anonymous_many_members_page_2(self):
        """it should display members"""

        IndividualFactory.create_batch(self.PAGE_SIZE * 2)
        OrganizationFactory.create_batch(self.PAGE_SIZE * 2)

        url = reverse('profiles:members')

        response = self.client.get(url, data={'counter': 'items:32$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 32)
        self.assertEqual(len(soup.select(".hide-on-md .list-item")), 0)

    def test_view_members_as_member(self):
        """it should display members"""

        me_as_individual = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=me_as_individual.user.email, password=1234))

        individuals = IndividualFactory.create_batch(5)
        organizations = OrganizationFactory.create_batch(6)

        url = reverse('profiles:members')

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 12)

        self.assertContains(response, me_as_individual.get_absolute_url())

        for individual in individuals:
            self.assertContains(response, individual.get_absolute_url())

        for organization in organizations:
            self.assertContains(response, organization.get_absolute_url())

    def test_search_by_individual_by_country(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        individuals_in = IndividualFactory.create_batch(5)
        individuals_out = IndividualFactory.create_batch(9)

        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        other_country = CountryFactory.create()

        for index, individual in enumerate(individuals_in):
            individual.country = country1 if index % 2 == 0 else country2
            individual.save()

        for index, individual in enumerate(individuals_out[:4]):
            individual.country = other_country
            individual.save()

        filters = 'country:{0}$country:{1}$'.format(country1.id, country2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_by_organization_by_country(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        orgs_in = OrganizationFactory.create_batch(5)
        orgs_out = OrganizationFactory.create_batch(9)

        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        other_country = CountryFactory.create()

        for index, org in enumerate(orgs_in):
            org.country = country1 if index % 2 == 0 else country2
            org.save()

        for index, org in enumerate(orgs_out[:4]):
            org.country = other_country
            org.save()

        filters = 'country:{0}$country:{1}$'.format(country1.id, country2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_by_individual_by_interest(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        individuals_in = IndividualFactory.create_batch(5)
        individuals_out = IndividualFactory.create_batch(9)

        interest1 = InterestFactory.create()
        interest2 = InterestFactory.create()
        other_interest = InterestFactory.create()

        for index, individual in enumerate(individuals_in):
            if index % 3 in (0, 1, ):
                individual.interests.add(interest1)
            if index % 3 in (0, 2, ):
                individual.interests.add(interest2)
            individual.save()

        for index, individual in enumerate(individuals_out[:4]):
            individual.interests.add(other_interest)
            individual.save()

        filters = 'interests:{0}$interests:{1}$'.format(interest1.id, interest2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_by_organization_by_interest(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        orgs_in = OrganizationFactory.create_batch(5)
        orgs_out = OrganizationFactory.create_batch(9)

        interest1 = InterestFactory.create()
        interest2 = InterestFactory.create()
        other_interest = InterestFactory.create()

        for index, org in enumerate(orgs_in):
            if index % 3 in (0, 1,):
                org.interests.add(interest1)
            if index % 3 in (0, 2,):
                org.interests.add(interest2)
            org.save()

        for index, org in enumerate(orgs_out[:4]):
            org.interests.add(other_interest)
            org.save()

        filters = 'interests:{0}$interests:{1}$'.format(interest1.id, interest2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_organization_by_type(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        orgs_in = OrganizationFactory.create_batch(5)
        orgs_out = OrganizationFactory.create_batch(9)

        type1 = OrganizationTypesFactory.create()
        type2 = OrganizationTypesFactory.create()
        other_type = OrganizationTypesFactory.create()

        for index, org in enumerate(orgs_in):
            if index % 3 in (0, 1,):
                org.organization_types.add(type1)
            if index % 3 in (0, 2,):
                org.organization_types.add(type2)
            org.save()

        for index, org in enumerate(orgs_out[:4]):
            org.organization_types.add(other_type)
            org.save()

        filters = 'activity:{0}$activity:{1}$'.format(type1.id, type2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_individual_by_role(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in = IndividualFactory.create_batch(5)
        ind_out = IndividualFactory.create_batch(9)

        role1 = RolesFactory.create()
        role2 = RolesFactory.create()
        other_role = RolesFactory.create()

        for index, ind in enumerate(ind_in):
            if index % 3 in (0, 1,):
                ind.roles.add(role1)
            if index % 3 in (0, 2,):
                ind.roles.add(role2)
            ind.save()

        for index, ind in enumerate(ind_out[:4]):
            ind.roles.add(other_role)
            ind.save()

        filters = 'activity:{0}$activity:{1}$'.format(-role1.id, -role2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_individual_by_citizenship(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in = IndividualFactory.create_batch(5)
        ind_out = IndividualFactory.create_batch(9)

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()
        other_citizenship = CitizenshipFactory.create()

        for index, ind in enumerate(ind_in):
            if index % 3 in (0, 1,):
                ind.citizenship = citizenship1
            if index % 3 in (2,):
                ind.citizenship = citizenship2
            ind.save()

        for index, ind in enumerate(ind_out[:4]):
            ind.citizenship = other_citizenship
            ind.save()

        filters = 'citizenship:{0}$citizenship:{1}$'.format(citizenship1.id, citizenship2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 5)

    def test_search_individual_with_dual_citizenship(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()
        other_citizenship = CitizenshipFactory.create()

        ind1 = IndividualFactory.create(citizenship=citizenship1)
        ind2 = IndividualFactory.create(citizenship=other_citizenship, dual_citizenship=citizenship1)
        ind3 = IndividualFactory.create(citizenship=citizenship1, dual_citizenship=citizenship2)
        ind4 = IndividualFactory.create(citizenship=citizenship2)
        out1 = IndividualFactory.create()
        out2 = IndividualFactory.create(citizenship=other_citizenship)

        filters = 'citizenship:{0}$citizenship:{1}$'.format(citizenship1.id, citizenship2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)
        self.assertContains(response, ind1.name)
        self.assertContains(response, ind2.name)
        self.assertContains(response, ind3.name)
        self.assertContains(response, ind4.name)
        self.assertNotContains(response, out1.name)
        self.assertNotContains(response, out2.name)

    def test_search_individual_by_dual_citizenship(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        citizenship1 = CitizenshipFactory.create()
        citizenship2 = CitizenshipFactory.create()
        other_citizenship = CitizenshipFactory.create()

        ind1 = IndividualFactory.create(citizenship=citizenship1)
        ind2 = IndividualFactory.create(citizenship=other_citizenship, dual_citizenship=citizenship1)
        ind3 = IndividualFactory.create(citizenship=citizenship1, dual_citizenship=citizenship2)
        ind4 = IndividualFactory.create(citizenship=citizenship2)
        out1 = IndividualFactory.create()
        out2 = IndividualFactory.create(citizenship=other_citizenship)

        filters = 'citizenship:{0}$citizenship:{1}$'.format(citizenship1.id, citizenship2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)
        self.assertContains(response, ind1.get_absolute_url())
        self.assertContains(response, ind2.get_absolute_url())
        self.assertContains(response, ind3.get_absolute_url())
        self.assertContains(response, ind4.get_absolute_url())
        self.assertNotContains(response, out1.get_absolute_url())
        self.assertNotContains(response, out2.get_absolute_url())

    def test_search_multi_criteria(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        interest1 = InterestFactory.create()
        country1 = CountryFactory.create()

        organization1 = OrganizationFactory.create()
        organization1.interests.add(interest1)
        organization1.country = country1
        organization1.save()

        organization2 = OrganizationFactory.create()
        organization2.interests.add(interest1)
        organization2.save()

        organization3 = OrganizationFactory.create()
        organization3.country = country1
        organization3.save()

        organization4 = OrganizationFactory.create()

        individual1 = IndividualFactory.create()
        individual1.interests.add(interest1)
        individual1.country = country1
        individual1.save()

        individual2 = IndividualFactory.create()
        individual2.interests.add(interest1)
        individual2.save()

        individual3 = IndividualFactory.create()
        individual3.country = country1
        individual3.save()

        individual4 = IndividualFactory.create()

        filters = 'interests:{0}$country:{1}$'.format(interest1.id, country1.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 2)
        self.assertContains(response, individual1.get_absolute_url())
        self.assertContains(response, organization1.get_absolute_url())

    def test_search_page_1(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        individuals_in = IndividualFactory.create_batch(self.PAGE_SIZE + 2)
        country1 = CountryFactory.create()

        for index, individual in enumerate(individuals_in):
            individual.country = country1
            individual.save()

        filters = 'country:{0}$'.format(country1.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), self.PAGE_SIZE)

    def test_search_page_2(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        individuals_in = IndividualFactory.create_batch(self.PAGE_SIZE + 2)
        country1 = CountryFactory.create()

        for index, individual in enumerate(individuals_in):
            individual.country = country1
            individual.save()

        filters = 'country:{0}$'.format(country1.id)
        response = self.client.get(url, data={'filters': filters, 'counter': 'items:48$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), self.PAGE_SIZE + 2)

    def test_search_individual_ulysses_label(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        individual1 = IndividualFactory.create(ulysses_label=True)
        individual2 = IndividualFactory.create(ulysses_label=True)
        individual3 = IndividualFactory.create(ulysses_label=False)

        response = self.client.get(url, data={'orders': 'ulysses:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 2)

        self.assertContains(response, individual1.get_absolute_url())
        self.assertContains(response, individual2.get_absolute_url())
        self.assertNotContains(response, individual3.get_absolute_url())

    def test_search_organization_ulysses_partner(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        organization1 = OrganizationFactory.create(ulysses_label=True)
        organization2 = OrganizationFactory.create(ulysses_label=True)
        organization3 = OrganizationFactory.create(ulysses_label=False)

        response = self.client.get(url, data={'orders': 'ulysses:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 2)

        self.assertContains(response, organization1.get_absolute_url())
        self.assertContains(response, organization2.get_absolute_url())
        self.assertNotContains(response, organization3.get_absolute_url())

    def test_search_by_instruments(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ins1 = ComposerInstrumentFactory.create()
        ins2 = ComposerInstrumentFactory.create()
        ins3 = ComposerInstrumentFactory.create()

        orgs_out = OrganizationFactory.create_batch(5)
        ind_in = IndividualFactory.create_batch(3)
        ind_out = IndividualFactory.create_batch(4)

        for index, ind in enumerate(ind_in):
            if index % 3 in (0, 1,):
                ind.instruments.add(ins1)
            if index % 3 in (0, 2,):
                ind.instruments.add(ins2)
            ind.save()

        for index, ind in enumerate(ind_out[:2]):
            ind.instruments.add(ins3)
            ind.save()

        filters = 'instruments:{0}$instruments:{1}$'.format(ins1.id, ins2.id)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), len(ind_in))

    def test_search_individual_by_birth_date(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in1 = IndividualFactory.create(birth_date=date(1975, 2, 20))
        ind_in2 = IndividualFactory.create(birth_date=date(1990, 7, 7))
        ind_out1 = IndividualFactory.create(birth_date=date(1970, 7, 7))
        ind_in3 = IndividualFactory.create(birth_date=date(2001, 7, 7))

        response = self.client.get(
            url, data={'birth_date': '1972-02-10#1992-02-13', 'user_type': 'individual'}
        )

        born_after = int(datetime(1972, 10, 2).strftime('%s')) * 1000
        filters = 'age:{0}$'.format(born_after)
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 3)

        self.assertContains(response, ind_in1.get_absolute_url())
        self.assertContains(response, ind_in2.get_absolute_url())
        self.assertNotContains(response, ind_out1.get_absolute_url())
        self.assertContains(response, ind_in3.get_absolute_url())

    def test_search_individual_strange(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in1 = IndividualFactory.create(birth_date=date(1975, 2, 20))
        ind_in2 = IndividualFactory.create(birth_date=date(1990, 7, 7))
        ind_in3 = IndividualFactory.create(birth_date=date(1970, 7, 7))
        ind_in4 = IndividualFactory.create(birth_date=date(2001, 7, 7))

        filters = 'blabla:12$'
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        self.assertContains(response, ind_in1.get_absolute_url())
        self.assertContains(response, ind_in2.get_absolute_url())
        self.assertContains(response, ind_in3.get_absolute_url())
        self.assertContains(response, ind_in4.get_absolute_url())

    def test_search_individual_invalid1(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in1 = IndividualFactory.create(birth_date=date(1975, 2, 20))
        ind_in2 = IndividualFactory.create(birth_date=date(1990, 7, 7))
        ind_in3 = IndividualFactory.create(birth_date=date(1970, 7, 7))
        ind_in4 = IndividualFactory.create(birth_date=date(2001, 7, 7))

        filters = 'age:abcd$'
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        self.assertContains(response, ind_in1.get_absolute_url())
        self.assertContains(response, ind_in2.get_absolute_url())
        self.assertContains(response, ind_in3.get_absolute_url())
        self.assertContains(response, ind_in4.get_absolute_url())

    def test_search_individual_invalid2(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind_in1 = IndividualFactory.create(birth_date=date(1975, 2, 20))
        ind_in2 = IndividualFactory.create(birth_date=date(1990, 7, 7))
        ind_in3 = IndividualFactory.create(birth_date=date(1970, 7, 7))
        ind_in4 = IndividualFactory.create(birth_date=date(2001, 7, 7))

        filters = 'blabla'
        response = self.client.get(url, data={'filters': filters})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        self.assertContains(response, ind_in1.get_absolute_url())
        self.assertContains(response, ind_in2.get_absolute_url())
        self.assertContains(response, ind_in3.get_absolute_url())
        self.assertContains(response, ind_in4.get_absolute_url())


class MembersOrderingTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', ]
    PAGE_SIZE = 32

    def test_search_order_by_alphabetical(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        response = self.client.get(url, data={'orders': 'alpha:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind3, ind2, ind4, ind1]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions should be already sorted
        self.assertEqual(sorted(positions), positions)

    def test_search_order_by_date_joined(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        ind1.user.date_joined = datetime(2016, 7, 7)
        ind2.user.date_joined = datetime.now()
        ind3.user.date_joined = datetime(2015, 11, 27)
        ind4.user.date_joined = datetime(2017, 2, 20)
        for ind in [ind2, ind4, ind1, ind3]:
            ind.user.save()

        response = self.client.get(url, data={'orders': 'date_joined:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind2, ind4, ind1, ind3]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions sohould be already sorted
        self.assertEqual(sorted(positions), positions)

    def test_search_order_by_followers(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        FollowingFactory.create(member_user=ind2.user)
        FollowingFactory.create(member_user=ind2.user)
        FollowingFactory.create(member_user=ind2.user)

        FollowingFactory.create(member_user=ind1.user)
        FollowingFactory.create(member_user=ind1.user)

        FollowingFactory.create(member_user=ind4.user)

        response = self.client.get(url, data={'orders': 'followers:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind2, ind1, ind4, ind3]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions should be already sorted
        self.assertEqual(sorted(positions), positions)

    def test_order_by_default(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        ind1.user.date_joined = datetime(2016, 7, 7)
        ind2.user.date_joined = datetime.now()
        ind3.user.date_joined = datetime(2015, 11, 27)
        ind4.user.date_joined = datetime(2017, 2, 20)
        for ind in [ind2, ind4, ind1, ind3]:
            ind.user.save()

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind2, ind4, ind1, ind3]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions sohould be already sorted
        self.assertEqual(sorted(positions), positions)

    def test_order_by_error1(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        ind1.user.date_joined = datetime(2016, 7, 7)
        ind2.user.date_joined = datetime.now()
        ind3.user.date_joined = datetime(2015, 11, 27)
        ind4.user.date_joined = datetime(2017, 2, 20)
        for ind in [ind2, ind4, ind1, ind3]:
            ind.user.save()

        response = self.client.get(url, data={'orders': 'blabla:1$'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind2, ind4, ind1, ind3]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions sohould be already sorted
        self.assertEqual(sorted(positions), positions)

    def test_order_by_error2(self):
        """it should display members matching the criteria"""

        url = reverse('profiles:members')

        ind1 = IndividualFactory.create(last_name='Doeb', first_name='John')
        ind2 = IndividualFactory.create(last_name='Dicaprio', first_name='Leo')
        ind3 = IndividualFactory.create(last_name='Damon', first_name='Matt')
        ind4 = IndividualFactory.create(last_name='Doea', first_name='Arnold')

        ind1.user.date_joined = datetime(2016, 7, 7)
        ind2.user.date_joined = datetime.now()
        ind3.user.date_joined = datetime(2015, 11, 27)
        ind4.user.date_joined = datetime(2017, 2, 20)
        for ind in [ind2, ind4, ind1, ind3]:
            ind.user.save()

        response = self.client.get(url, data={'orders': 'followers'})
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".list-item")), 4)

        positions = []
        for ind in [ind2, ind4, ind1, ind3]:
            self.assertContains(response, ind.get_absolute_url())
            positions.append(soup.text.find(ind.get_absolute_url()))
        # Make sur that the members are displayed in right order : The positions sohould be already sorted
        self.assertEqual(sorted(positions), positions)
