# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import ajax, members, organization_membership, personal_space, public_profiles


app_name = "profiles"


urlpatterns = [
    url(r'members-autocomplete/$', ajax.MembersAutocompleteView.as_view(), name="members_autocomplete"),
    url(r'organizations-autocomplete/$', ajax.OrganizationAutocompleteView.as_view(), name="organizations_autocomplete"),

    url(r'^$', personal_space.PersonalSpaceView.as_view(), name='personal_space'),
    url(r'^edit-profile/$', personal_space.EditProfileView.as_view(), name='edit_profile'),
    url(r'^edit-private-profile-info/$', personal_space.EditPrivateProfileView.as_view(), name='edit_private_profile'),
    url(r'^upload-profile-image/$', personal_space.upload_profile_image, name='upload_profile_image'),
    url(
        r'^add-contacts-group/$',
        personal_space.EditContactsGroupView.as_view(),
        name="add_contacts_group"
    ),
    url(
        r'^edit-contacts-group/(?P<id>\d+)/$',
        personal_space.EditContactsGroupView.as_view(),
        name="edit_contacts_group"
    ),
    url(
        r'^delete-contacts-group/(?P<id>\d+)/$',
        personal_space.DeleteContactsGroupView.as_view(),
        name="delete_contacts_group"
    ),
    url(
        r'contacts-of-group/(?P<group_id>\d+)/$',
        personal_space.ContactsGroupUserView.as_view(),
        name="contacts_of_group"
    ),
    url(
        r'switch-profile/$',
        personal_space.SwitchProfileView.as_view(),
        name="switch_profile"
    ),
    url(
        r'delete-profile/$',
        personal_space.DeleteProfileView.as_view(),
        name="delete_profile"
    ),
    url(
        r'profile-preferences/$',
        personal_space.ProfilePreferencesView.as_view(),
        name="profile_preferences"
    ),
    url(r'^view-profile/$', public_profiles.CurrentUserProfile.as_view(), name='current_user_profile'),
    url(
        r'^individual/(?P<profile_id>\d+)/$',
        public_profiles.IndividualPublicProfileView.as_view(),
        name='individual_profile'
    ),
    url(
        r'^organization/(?P<profile_id>\d+)/$',
        public_profiles.OrganizationPublicProfileView.as_view(),
        name='organization_profile'
    ),

    url(r'^members/', members.MembersPageView.as_view(), name="members"),
    url(r'^export-members-to-excel/', members.MembersXlsExportView.as_view(), name="export_members_to_excel"),

    url(
        r'^request-membership/',
        organization_membership.OrganizationMembershipRequestView.as_view(),
        name="request_membership"
    ),
    url(
        r'^accept-membership/(?P<membership_request_id>\d+)/$',
        organization_membership.AcceptMembershipRequestView.as_view(),
        name="accept_membership"
    ),
    url(
        r'^remove-organization-membership/(?P<individual_id>\d+)/(?P<organization_id>\d+)/$',
        organization_membership.RemoveOrganizationMembershipView.as_view(),
        name="remove_organization_membership"
    ),
    url(
        r'^modify-email/$',
        personal_space.ModifyEmailView.as_view(),
        name="modify_email"
    ),
    url(
        r'^confirm-email-modification/(?P<uuid>[\w\d-]+)/$',
        personal_space.ConfirmEmailModificationView.as_view(),
        name="confirm_email_modification"
    ),
]
