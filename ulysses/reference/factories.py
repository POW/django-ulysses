# -*- coding: utf-8 -*-

import factory

from .models import Citizenship, Country, Gender, CompetitionCategory, Continent


class CitizenshipFactory(factory.DjangoModelFactory):
    class Meta:
        model = Citizenship

    name = factory.Sequence(lambda n: 'citizenship-{0}'.format(n))


class CountryFactory(factory.DjangoModelFactory):
    class Meta:
        model = Country

    name = factory.Sequence(lambda n: 'country-{0}'.format(n))


class ContinentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Continent

    name = factory.Sequence(lambda n: 'continent-{0}'.format(n))
    order = factory.Sequence(lambda n: n)


class GenderFactory(factory.DjangoModelFactory):
    class Meta:
        model = Gender

    name = "male"


class CompetitionCategoryFactory(factory.DjangoModelFactory):
    class Meta:
        model = CompetitionCategory

    name = factory.Sequence(lambda n: 'competition-category-{0}'.format(n))
    is_any = False
