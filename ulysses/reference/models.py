# -*- coding: utf-8 -*-

import pycountry

from django.db import models
from django.utils.translation import ugettext_lazy as _


class Citizenship(models.Model):
    """citizenship"""
    name = models.CharField(max_length=200, verbose_name=_('name'))

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name', ]


def get_code_from_country(name):
    try:
        return pycountry.countries.get(name=name).alpha_2.lower()
    except KeyError:
        return ''


class Country(models.Model):
    """A country"""
    name = models.CharField(max_length=200, verbose_name=_('name'))
    code = models.CharField(max_length=10, verbose_name=_('code'), default='', blank=True, db_index=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "country"
        verbose_name_plural = "countries"
        ordering = ['name', ]

    def continents(self):
        if self.id:
            return ", ".join([elt.name for elt in Continent.objects.filter(countries=self)])
        return ''


class Gender(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=200)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "pronoun"
        ordering = ['-name', ]


class Continent(models.Model):
    """A continent"""
    name = models.CharField(max_length=200, verbose_name=_('name'))
    order = models.IntegerField()
    countries = models.ManyToManyField(Country, blank=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "continent"
        verbose_name_plural = "continents"
        ordering = ['order', 'name']

    def countries_count(self):
        return self.countries.count()

    def migration_code(self):
        if self.id:
            code = '"{0}": [\n'.format(self.name)
            for country in self.countries.all():
                code += '    u"{0}",\n'.format(country.name)
            code += "],\n"
            return code
        return ""


class CompetitionCategory(models.Model):
    name = models.CharField(max_length=200, unique=True)
    order = models.IntegerField(default=0)
    is_any = models.BooleanField(default=False)

    class Meta:
        ordering = ['order', 'name', ]
        verbose_name = _("competition category")
        verbose_name_plural = _("competition categories")

    def __str__(self):
        return self.name
