# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.reference'
    label = 'ulysses_reference'
    verbose_name = "Ulysses Reference"
