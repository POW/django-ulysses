# -*- coding: utf-8 -*-



from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_reference', '0002_auto_20161208_1600'),
        ('ulysses_profiles','0003_city_as_char'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='city',
            name='country',
        ),
        migrations.DeleteModel(
            name='City',
        ),
    ]
