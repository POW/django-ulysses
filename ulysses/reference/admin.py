# -*- coding: utf-8 -*-

from django.contrib import admin

from ulysses.super_admin.sites import super_admin_site

from .models import Citizenship, Country, Continent, CompetitionCategory, Gender


class CountryAdmin(admin.ModelAdmin):
    list_display = ("name", "continents", )
    search_fields = ("name", )


class CitizenshipAdmin(admin.ModelAdmin):
    search_fields = ("name", )


class ContinentAdmin(admin.ModelAdmin):
    list_display = ("name", "order", "countries_count")
    filter_horizontal = ("countries", )
    readonly_fields = ('migration_code', )


class CompetitionCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "order", "is_any")
    list_editable = ("order", "is_any")


# Global admin registration
super_admin_site.register(Country, CountryAdmin)
super_admin_site.register(Citizenship, CitizenshipAdmin)
super_admin_site.register(Continent, ContinentAdmin)
super_admin_site.register(CompetitionCategory, CompetitionCategoryAdmin)
super_admin_site.register(Gender)