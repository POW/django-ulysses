# -*- coding: utf-8 -*-

from ulysses.generic.widgets import NameAutocompleteBaseWidget

from .models import EventRole


class EventRoleAutocompleteWidget(NameAutocompleteBaseWidget):
    """An autocomplete widget for choosing or adding tags"""
    tag_model = EventRole
