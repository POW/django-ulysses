# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views import (
    AddEventView, EditEventView, upload_event_file, EventView, DeleteEventView, EventListView, CreateEventsAlertView,
    GetFiltersView, DeleteEventAlertView
)


app_name = "events"


urlpatterns = [
    url(r'^view/(?P<id>\d+)/$', EventView.as_view(), name='view_event'),
    url(r'^edit/(?P<id>\d+)/$', EditEventView.as_view(), name='edit_event'),
    url(r'^delete/(?P<id>\d+)/$', DeleteEventView.as_view(), name='delete_event'),
    url(r'^add/$', AddEventView.as_view(), name='add_event'),
    url(r'^list/$', EventListView.as_view(), name='events_list'),
    url(r'^create-events-alert/$', CreateEventsAlertView.as_view(), name='create_events_alert'),
    url(r'^get-filters/$', GetFiltersView.as_view(), name='get_filters'),
    url(r'^upload-file/$', upload_event_file, name='upload_event_file'),
    url(r'^upload-file/(?P<user_id>\d+)/$', upload_event_file, name='upload_event_file'),
    url(r'^delete-alert/(?P<id>\d+)/$', DeleteEventAlertView.as_view(), name='delete_event_alert'),
]
