# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.events'
    label = 'ulysses_events'
    verbose_name = "Ulysses Events"
