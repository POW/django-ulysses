# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-14 00:04


from django.db import migrations


def create_collaborators(apps, schema_editor):
    """set country code from pycountries"""
    event_class = apps.get_model("ulysses_events", "Event")
    role_class = apps.get_model("ulysses_events", "EventRole")
    collaborator_class = apps.get_model("ulysses_events", "EventCollaborator")

    partner_role = role_class.objects.create(name='Partner')
    producer_role = role_class.objects.create(name='Producer')

    for event in event_class.objects.all():

        for partner in event.partners.all():
            collaborator_class.objects.create(event=event, role=partner_role, user=partner)

        for producer in event.producers.all():
            collaborator_class.objects.create(event=event, role=producer_role, user=producer)


def delete_collaborators(apps, schema_editor):
    role_class = apps.get_model("ulysses_events", "EventRole")
    collaborator_class = apps.get_model("ulysses_events", "EventCollaborator")
    collaborator_class.objects.all().delete()
    role_class.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_events', '0013_auto_20170413_1733'),
    ]

    operations = [
        migrations.RunPython(create_collaborators, delete_collaborators),
    ]
