# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-01 15:09


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_events', '0018_event_extra_works'),
    ]

    operations = [
        migrations.AddField(
            model_name='event',
            name='extra_performers',
            field=models.CharField(blank=True, default='', help_text='Members which are not existing on the platform', max_length=500, verbose_name='extra performes'),
        ),
    ]
