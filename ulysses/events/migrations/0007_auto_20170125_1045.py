# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-25 10:45


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_events', '0006_event_ulysses_label'),
    ]

    operations = [
        migrations.AlterField(
            model_name='event',
            name='ulysses_label',
            field=models.BooleanField(db_index=True, default=False, verbose_name='Ulysses label'),
        ),
    ]
