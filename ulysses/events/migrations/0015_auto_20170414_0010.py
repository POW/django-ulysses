# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-14 00:10


from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_events', '0014_auto_20170414_0004'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='event',
            name='partners',
        ),
        migrations.RemoveField(
            model_name='event',
            name='producers',
        ),
    ]
