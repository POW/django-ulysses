# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-24 17:02


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_reference', '0003_remove_city'),
        ('ulysses_events', '0002_auto_20170123_1755'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='event',
            options={'ordering': ['start_date', 'end_date'], 'verbose_name': 'event', 'verbose_name_plural': 'events'},
        ),
        migrations.AddField(
            model_name='event',
            name='city',
            field=models.CharField(blank=True, default=b'', max_length=100, verbose_name='City'),
        ),
        migrations.AddField(
            model_name='event',
            name='country',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Country', verbose_name='Country'),
        ),
        migrations.AddField(
            model_name='event',
            name='location',
            field=models.CharField(blank=True, default=b'', max_length=100, verbose_name='Location'),
        ),
    ]
