# -*- coding: utf-8 -*-
# Generated by Django 1.10.3 on 2017-01-24 20:59


from django.db import migrations, models
import django.db.models.deletion


def create_specialities(apps, schema_editor):
    """set country code from pycountries"""
    artistic_speciality_class = apps.get_model("ulysses_events", "ArtisticSpeciality")
    artistic_speciality_class.objects.create(name='Young audience', order_index=1)
    artistic_speciality_class.objects.create(name='Theatre', order_index=2)
    artistic_speciality_class.objects.create(name='Film', order_index=3)
    artistic_speciality_class.objects.create(name='Dance', order_index=4)
    artistic_speciality_class.objects.create(name='Philsosphy', order_index=5)
    artistic_speciality_class.objects.create(name='Litterature', order_index=6)
    artistic_speciality_class.objects.create(name='Science', order_index=7)
    artistic_speciality_class.objects.create(name='Visual art', order_index=8)
    artistic_speciality_class.objects.create(name='Other', order_index=100)


def delete_specialities(apps, schema_editor):
    artistic_speciality_class = apps.get_model("ulysses_events", "ArtisticSpeciality")
    artistic_speciality_class.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_works','0006_auto_20170120_1700'),
        ('ulysses_events', '0003_auto_20170124_1702'),
    ]

    operations = [
        migrations.CreateModel(
            name='ArtisticSpeciality',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=200, verbose_name='name')),
                ('order_index', models.IntegerField(default=0, verbose_name='order index')),
            ],
            options={
                'verbose_name': 'artistic speciality',
                'verbose_name_plural': 'artistic specialities',
                'ordering': ['order_index', 'name'],
            },
        ),
        migrations.AddField(
            model_name='event',
            name='keywords',
            field=models.ManyToManyField(blank=True, to='ulysses_works.Keyword', verbose_name='keywords'),
        ),
        migrations.AddField(
            model_name='event',
            name='music_and',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_events.ArtisticSpeciality', verbose_name='Music and'),
        ),
        migrations.RunPython(create_specialities, delete_specialities),
    ]
