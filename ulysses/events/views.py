# -*- coding: utf-8 -*-

import os.path
import json

from datetime import datetime
from decimal import Decimal, ConversionSyntax

from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.contrib.sites.models import Site
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template.loader import render_to_string
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from icalendar import Calendar, Event as CalendarEvent

from ulysses.generic.directories import upload_file, EVENTS_DIRECTORY
from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import (
    DetailView, LoginRequiredPopupFormView, redirect_to_login, ListBasePageView, FilterPageMixin, AjaxView
)
from ulysses.social.models import FeedItem, MemberPost

from .forms import EventForm, EventSearchForm, SaveEventSearchForm
from .models import Event, UserEventAlert
from .utils import get_upcoming_events_queryset, filter_events


@csrf_exempt
def upload_event_file(request, user_id=0):
    return upload_file(request, EVENTS_DIRECTORY, user_id)


def _to_ical_event(event):
    ical_event = CalendarEvent()
    ical_event.add('summary', event.title)
    ical_event.add('description', event.description)
    ical_event.add('dtstart', event.start_date)
    if event.end_date:
        ical_event.add('dtend', event.end_date)
    ical_event.add('uid', str(event.id))
    ical_event.add('last-modified', event.update_date)
    return ical_event


class EventListView(ListBasePageView):
    """Displays the list of events"""
    template_name = 'events/events_list.html'

    ITEMS_COUNTER_STEP = 18
    ROWS_LG_LIMIT = 3  # By default only 3 lines on small screens

    ORDER_BY_CHOICES = []

    def get_ordering(self, order_by):
        """returns the ordering fields"""
        ordering = ['start_date', 'end_date']
        return ordering

    def get_queryset(self):
        return get_upcoming_events_queryset()

    def get_filter_form_class(self):
        return EventSearchForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        ical_url = self.request.path + "?as=ical"
        filter_args = ''
        for key, val in self.request.GET.items():
            filter_args += '&' + key + '=' + val
        context['ical_url'] = ical_url + filter_args
        context['filter_args'] = filter_args
        return context

    def _do_filter_items(self, queryset, field, values):
        return filter_events(queryset, field, values)

    def get_ical_events(self):
        """add data to template context"""
        filters = self.get_filters()
        order_by = self.get_order_by()

        # items
        queryset = self.get_queryset()
        queryset = self.get_filtered_items(queryset, filters)
        ordering = self.get_ordering(order_by)
        queryset = self.order_items(queryset, order_by, ordering)

        return queryset

    def get(self, *args, **kwargs):
        if self.request.GET.get('as') == 'ical':
            cal = Calendar()
            site = Site.objects.get_current()
            cal.add('prodid', '-//{0} //{1}//'.format(_('Ulysses platform'), site.domain))
            cal.add('version', '2.0')
            events = self.get_ical_events()
            for event in events:
                cal.add_component(_to_ical_event(event))
            response = HttpResponse(content_type="text/calendar")
            response.write(cal.to_ical())
            response['Content-Disposition'] = 'attachment; filename=ulysses.ics'
            return response
        else:
            return super().get(*args, **kwargs)


class EventView(DetailView):
    """View an existing work"""
    template_name = 'events/view_event.html'
    event = None

    def dispatch(self, request, *args, **kwargs):
        self.event = get_object_or_404(Event, id=self.kwargs['id'])
        if self.event.visibility != Event.VISIBILITY_PUBLIC and self.request.user.is_anonymous:
                return redirect_to_login(self.request)
        return super(EventView, self).dispatch(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(EventView, self).get_context_data(**kwargs)
        context['event'] = self.event
        events = get_upcoming_events_queryset().exclude(id=self.event.id)[:4]
        context['events'] = events
        return context

    def get(self, *args, **kwargs):
        if self.request.GET.get('as') == 'ical':
            if not self.event.start_date:
                raise Http404
            cal = Calendar()
            site = Site.objects.get_current()
            cal.add('prodid', '-//{0} //{1}//'.format(_('Ulysses platform'), site.domain))
            cal.add('version', '2.0')
            cal.add_component(_to_ical_event(self.event))
            response = HttpResponse(content_type="text/calendar")
            response.write(cal.to_ical())
            response['Content-Disposition'] = 'attachment; filename=ulysses.ics'
            return response
        else:
            return super().get(*args, **kwargs)


class ChangeEventBaseView(LoginRequiredPopupFormView):
    form_class = EventForm
    event = None

    def get_event(self):
        raise NotImplementedError

    def get_form(self, form_class=None):
        form = super(ChangeEventBaseView, self).get_form(form_class=form_class)
        return form

    def get_form_kwargs(self):
        form_kwargs = super(ChangeEventBaseView, self).get_form_kwargs() or {}
        self.event = self.get_event()
        form_kwargs['instance'] = self.event
        return form_kwargs

    def on_saved(self, event):
        pass

    def form_valid(self, form):
        event = form.save()
        self.on_saved(event)
        return HttpResponseRedirect(reverse('events:view_event', args=[event.id]))


class AddEventView(ChangeEventBaseView):
    """Add a new work"""
    template_name = 'events/add_event.html'

    def get_event(self):
        return Event(
            owner=self.request.user,
            creation_date=datetime.now(),
            update_date=datetime.now(),
        )

    def on_saved(self, event):
        message_text = _('Your event has been successfully added to the Community Feed.')
        messages.success(self.request, message_text)

        # Create a FeedItem to see that posts in the feeds
        content_type = ContentType.objects.get_for_model(event)
        lookup = dict(
            owner=event.owner,
            content_type=content_type,
            object_id=event.id,
            is_original_post=True,
            tag=MemberPost.COMMUNITY_NEWS_POST
        )

        try:
            feed = FeedItem.objects.get(**lookup)
        except FeedItem.DoesNotExist:
            feed = FeedItem(**lookup)
            feed.feed_datetime = datetime.now()

        feed.post_text = event.title
        feed.save()


class EditEventView(ChangeEventBaseView):
    """Edit an existing work"""
    template_name = 'events/edit_event.html'
    form_class = EventForm

    def get_event(self):
        event = get_object_or_404(Event, id=self.kwargs['id'])
        if event.owner != self.request.user:
            raise PermissionDenied
        return event

    def get_context_data(self, **kwargs):
        context = super(EditEventView, self).get_context_data(**kwargs)
        context['event'] = self.event
        return context

    def on_saved(self, event):
        message_text = _('Your event has been successfully updated.')
        messages.success(self.request, message_text)


class DeleteEventView(LoginRequiredPopupFormView):
    template_name = 'events/popup_delete_event.html'
    form_class = ConfirmForm
    event = None

    def get_form_kwargs(self):
        form_kwargs = super(DeleteEventView, self).get_form_kwargs()
        # Delete an existing document
        self.event = get_object_or_404(Event, id=self.kwargs['id'])
        if self.event.owner != self.request.user:
            # Check user permission :
            # Owners will delete the doc
            raise PermissionDenied
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeleteEventView, self).get_context_data(**kwargs)
        context['instance'] = self.event
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            # delete member post
            self.event.delete()

            message_text = _('Your event has been successfully deleted from the Community Feed.')
            messages.success(self.request, message_text)

        return HttpResponseRedirect(reverse("social:community_news"))


class CreateEventsAlertView(LoginRequiredPopupFormView, FilterPageMixin):
    template_name = 'events/popup_create_events_alert.html'
    form_class = SaveEventSearchForm

    def get_context_data(self, **kwargs):
        """add data to template context"""
        context = super().get_context_data(**kwargs)
        filters = self.get_filters('p_')
        if not filters:
            # If no filters defined : set All events by default
            filters = [{'event_type': -1}]
        data = {
            'filters': filters,
        }
        # filters
        filter_form_class = self.form_class
        if filter_form_class:
            filter_form = filter_form_class(initial={'filter': json.dumps(data)})
            filter_fields = self.init_form(filters, filter_form_class, data, 'p_')
            filter_form.fields['label'].initial = self.get_label(filter_fields)
        else:
            filter_fields = []
            filter_form = None
        context['filter_fields'] = filter_fields
        existing_form = context.get('form', None)
        if existing_form:
            context['form_errors'] = existing_form.non_field_errors
        context['form'] = filter_form
        return context

    def form_valid(self, form):
        event_filter = form.cleaned_data['filter']
        label = form.cleaned_data['label']
        elts = json.loads(event_filter)
        patched_filters = []
        for event_filter in elts['filters']:
            for key, value in event_filter.items():
                patched_filters.append({key.replace('p_', ''): value})
        UserEventAlert.objects.create(
            user=self.request.user,
            label=label,
            event_filter=json.dumps({'filters': patched_filters}),
            creation_datetime=datetime.now(),
        )
        message_text = _('Your alert has been successfully created.')
        messages.success(self.request, message_text)
        return HttpResponseRedirect(reverse('profiles:personal_space') + '?tab=bookmarks')


class GetFiltersView(AjaxView, FilterPageMixin):
    login_required = True

    def get_json_data(self):
        # popup fields are prefixed with a p_ to avoid mismatch between popup and main page
        filters = self.get_filters('p_')
        data = {
            'filters': filters,
        }
        filter_fields = self.init_form(filters, EventSearchForm, data)
        label = self.get_label(filter_fields)
        html = render_to_string(
            "controls/_filter_fields_labels.html",
            {'filter_fields': filter_fields, 'popup_url': reverse('events:get_filters')},
        )
        return {'html': html, 'label': label}


class DeleteEventAlertView(LoginRequiredPopupFormView):
    template_name = 'events/popup_delete_event_alert.html'
    form_class = ConfirmForm
    event_alert = None

    def get_form_kwargs(self):
        form_kwargs = super().get_form_kwargs()
        self.event_alert = get_object_or_404(UserEventAlert, id=self.kwargs['id'])
        if self.event_alert.user != self.request.user:
            # Check user permission :
            raise PermissionDenied
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['instance'] = self.event_alert
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            # delete member post
            self.event_alert.delete()

            message_text = _('Your alert has been successfully deleted.')
            messages.success(self.request, message_text)

        return HttpResponseRedirect(reverse('profiles:personal_space') + '?tab=bookmarks')