# -*- coding: utf-8 -*-

from datetime import datetime

from django.core.management.base import BaseCommand
from django.utils.translation import ugettext as _

from ulysses.generic.emails import send_email
from ulysses.events.models import UserEventAlert, EventNotification
from ulysses.events.utils import get_alert_upcoming_events
from ulysses.profiles.models import Individual, Organization


class Command(BaseCommand):
    """send a summary of all emails"""
    help = ""
    
    def handle(self, *args, **options):
        verbosity = options.get('verbosity', 1)
        now = datetime.now()

        for profile_class in (Individual, Organization):
            profiles = profile_class.objects.filter(
                user__is_active=True, is_enabled=True, user__usereventalert__isnull=False
            )
            for profile in profiles:
                events_to_notify = []

                for event_alert in UserEventAlert.objects.filter(user=profile.user):
                    alert_events = get_alert_upcoming_events(event_alert)
                    events_to_notify.extend(alert_events)

                if events_to_notify:
                    events_to_notify = sorted(set(events_to_notify), key=lambda elt: (elt.start_date, elt.end_date))
                    if verbosity:
                        print(
                            "send email to", profile.user.email, ':', len(events_to_notify), 'events(s) added'
                        )

                    send_email(
                        _('{0} been added').format(
                            _('A new event has') if len(events_to_notify) == 1 else _('New events have')
                        ),
                        'emails/events_added.html',
                        {'events': events_to_notify, 'profile': profile},
                        [profile.user.email]
                    )

                    for event in events_to_notify:
                        EventNotification.objects.create(event=event, user=profile.user, creation_datetime=now)
