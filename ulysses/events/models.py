# -*- coding: utf-8 -*-

from datetime import time
import json

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.db.models.signals import post_delete
from django.dispatch import receiver
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _, ugettext

from sorl.thumbnail import get_thumbnail

from ulysses.middleware import get_request
from ulysses.generic.directories import DirectoryName, EVENTS_DIRECTORY
from ulysses.generic.models import NameIndexBaseModel
from ulysses.utils import text_overview, date_to_str

from ulysses.profiles.utils import get_profile
from ulysses.reference.models import Country
from ulysses.works.models import Work, Keyword


def events_directory(*args):
    return DirectoryName(EVENTS_DIRECTORY)(*args)


class EventType(NameIndexBaseModel):
    """A type of event : Concert, Conference, """

    class Meta:
        ordering = ['order_index', 'name']
        verbose_name = _('event type')
        verbose_name_plural = _('event types')

    def __str__(self):
        return self.name


class ArtisticSpeciality(NameIndexBaseModel):

    class Meta:
        ordering = ['order_index', 'name']
        verbose_name = _('artistic speciality')
        verbose_name_plural = _('artistic specialities')


class EventRole(NameIndexBaseModel):

    class Meta:
        ordering = ['order_index', 'name']
        verbose_name = _('Event role')
        verbose_name_plural = _('Event roles')


class EventCollaborator(models.Model):
    event = models.ForeignKey("Event", on_delete=models.CASCADE)
    user = models.ForeignKey(User, blank=True, default=None, null=True, on_delete=models.CASCADE)
    role = models.ForeignKey(EventRole, blank=True, default=None, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=500, blank=True, default='')

    def is_anonymous(self):
        return False

    class Meta:
        verbose_name = _('Event collaborator')
        verbose_name_plural = _('Event collaborators')


class Collaborator(object):
    def __init__(self, user, role):
        self.user = user
        self.role = role

    def is_anonymous(self):
        return not isinstance(self.user, User)


class PerformerCollaborator(Collaborator):
    def __init__(self, user):
        super(PerformerCollaborator, self).__init__(user, _('Performer'))


class Event(models.Model):
    """An event"""

    VISIBILITY_PUBLIC = 1
    VISIBILITY_MEMBER = 3

    VISIBILITY_CHOICES = (
        (VISIBILITY_PUBLIC, _('Public')),
        (VISIBILITY_MEMBER, _('Ulysses network')),
    )

    title = models.CharField(max_length=200, verbose_name=_('Event name'), db_index=True)
    creation_date = models.DateTimeField(verbose_name=_("creation date"), auto_now_add=True)
    update_date = models.DateTimeField(verbose_name=_("last update date"), blank=True, null=True, auto_now=True)

    owner = models.ForeignKey(User, verbose_name=_('owner'), on_delete=models.CASCADE)
    picture = models.ImageField(
        verbose_name=_('Photo'), upload_to=events_directory, blank=True, default=None, null=True,
        max_length=200
    )
    description = models.TextField(blank=True, default='', verbose_name=_('Description'), max_length=1000)
    description_backup = models.TextField(blank=True, default='', verbose_name=_('Description backup'))
    works = models.ManyToManyField(Work, blank=True, verbose_name=_('Works'))
    extra_works = models.CharField(
        max_length=500, blank=True, verbose_name=_('extra works'), default='',
        help_text=_('Works which are not existing on the platform')
    )
    start_date = models.DateTimeField(verbose_name=_('Starts'), blank=True, default=None, null=True)
    end_date = models.DateTimeField(verbose_name=_('Ends'), blank=True, default=None, null=True)
    event_type = models.ForeignKey(
        EventType, verbose_name=_('Category'), blank=True, default=None, null=True, on_delete=models.SET_NULL
    )
    performers = models.ManyToManyField(
        User, blank=True, verbose_name=_('Performer(s) Names'), related_name='performed_event_set'
    )
    extra_performers = models.CharField(
        max_length=500, blank=True, verbose_name=_('extra performes'), default='',
        help_text=_('Members which are not existing on the platform')
    )
    organizators = models.ManyToManyField(
        User, blank=True, verbose_name=_('Organized By)'), related_name='organized_event_set'
    )
    members_involved = models.ManyToManyField(
        User, blank=True, verbose_name=_('Members involved'), related_name='involved_events_set'
    )

    premiere = models.BooleanField(default=False, verbose_name=_('Premiere'))
    link = models.URLField(verbose_name=_('link'), blank=True, default=None, null=True)
    event_venue = models.CharField(verbose_name=_('Event venue'), default='', blank=True, max_length=200)
    # location = gpl_latitude#gps_longitude#country_code
    location = models.CharField(verbose_name=_('Location'), default='', blank=True, max_length=100)
    city = models.CharField(verbose_name=_('City'), default='', blank=True, max_length=100)
    country = models.ForeignKey(
        Country, verbose_name=_('Country'), default=None, blank=True, null=True, on_delete=models.SET_NULL
    )
    keywords = models.ManyToManyField(Keyword, blank=True, verbose_name=_('keywords'))
    music_and = models.ForeignKey(
        ArtisticSpeciality, blank=True, null=True, default=None, verbose_name=_('Music and'), on_delete=models.SET_NULL
    )
    ulysses_label = models.BooleanField(default=False, verbose_name=_('Ulysses label'), db_index=True)
    place_name = models.CharField(
        verbose_name=_('Event location (concert hall, theatre...)'), default='', blank=True, max_length=100
    )
    price = models.CharField(verbose_name=_('price(s)'), default='', blank=True, max_length=200)
    booking_link = models.URLField(verbose_name=_('booking link'), blank=True, default=None, null=True)
    visibility = models.IntegerField(
        verbose_name=_('visibility'), default=VISIBILITY_PUBLIC, choices=VISIBILITY_CHOICES
    )

    class Meta:
        verbose_name = _('event')
        verbose_name_plural = _('events')
        ordering = ['start_date', 'end_date']

    def __str__(self):
        return self.title

    def latitude(self):
        return self.location.split("#")[0]

    def longitude(self):
        return self.location.split("#")[1]

    @property
    def name(self):
        return self.title

    @property
    def favorites_count(self):
        return MemberCalendarItem.objects.filter(event=self, is_set=True).count()

    @property
    def summary(self):
        return text_overview(self.description, 30)

    def organization_str(self):
        if self.organizators.count():
            return ', '.join(
                [get_profile(organizator).name for organizator in self.organizators.all()]
            )
        else:
            return get_profile(self.owner).name

    def get_collaborations(self):

        # collaborators
        collaborators = list(EventCollaborator.objects.filter(event=self, user__isnull=False))

        # anonymous (just a name. no user) collaborators
        for anonymous_collaboration in EventCollaborator.objects.filter(event=self, user__isnull=True):
            for anonymous_collaborator in anonymous_collaboration.name.split("##"):
                if anonymous_collaborator:
                    collaborators.append(
                        Collaborator(anonymous_collaborator, anonymous_collaboration.role)
                    )

        # performers
        for user in self.performers.all():
            collaborators.append(PerformerCollaborator(user))

        # anonymous (just a name. no user) performers
        for name in self.extra_performers.split("##"):
            if name:
                collaborators.append(PerformerCollaborator(name))

        return collaborators

    def place(self):
        value = ''
        if self.city and self.country:
            value = '{0} ({1})'.format(self.city, self.country.name)
        elif self.city:
            value = self.city
        elif self.country:
            value = self.country.name
        if value and self.place_name:
            value = '{0}, {1}'.format(self.place_name, value)
        elif self.place_name:
            value = self.place_name
        return value

    def get_absolute_url(self):
        return reverse('events:view_event', args=[self.id])

    def val_to_date(self, date_value):
        return date_to_str(date_value, show_hour_if_midnight=False)

    def in_member_calendar(self, member_user=None):
        """Returns True if the event is in the current user calendar of favorites"""
        if not member_user:
            member_user = get_request().user

        if member_user and member_user.is_authenticated:
            return MemberCalendarItem.objects.filter(event=self, user=member_user, is_set=True).exists()

        return False

    def date_str(self):
        if self.start_date:
            if self.end_date:
                return ugettext('{0} to {1}').format(
                    self.val_to_date(self.start_date),
                    self.val_to_date(self.end_date),
                )
            else:
                return self.val_to_date(self.start_date)
        return ""

    def cal_time_str(self):
        if self.start_date and self.start_date.time() != time.min:
            return ugettext('{0.hour}:{0.minute:02}').format(self.start_date)
        return ""

    def cal_date_str(self):
        if self.start_date:
            if self.end_date:
                if self.start_date.month == self.end_date.month:
                    return ugettext('{0} - {1}').format(
                        self.start_date.day, self.end_date.day,
                    )
                else:
                    return ugettext('{0:02}/{1:02} - {2:02}/{3:02}').format(
                        self.start_date.day, self.start_date.month,
                        self.end_date.day, self.end_date.month,
                    )
            else:
                return '{0}'.format(self.start_date.day)
        return ""

    def keywords_str(self):
        return ", ".join([keyword.name for keyword in self.keywords.all()])

    def get_thumbnail(self):
        return self.get_image("400x300")

    def get_image_fb(self):
        return self.get_image("1200x630")

    def get_medium(self):
        return self.get_image("600")

    def get_large_image(self):
        return self.get_image("1200x240")

    def get_image(self, size='400x400', crop='center'):
        if self.picture:
            try:
                return get_thumbnail(self.picture, size, crop=crop).url
            except:
                pass
        return '{0}img/img-default-event_{1}.png'.format(settings.STATIC_URL, size)

    def get_square_medium_image(self):
        """image on dashboard"""
        return self.get_image("400x400", crop="center")

    def all_works(self):
        list_of_works = list(self.works.all())
        for extra_work in self.extra_works.split("##"):
            if extra_work:  # avoid any empty value
                list_of_works.append(
                    {
                        'is_extra': True,
                        'title': extra_work,
                    }
                )
        return list_of_works


# Delete feeditem related to the deleted event
@receiver(post_delete)
def delete_feeditem(sender, instance, **kwargs):
    if sender != Event:
        return
    try:
        # import here to avoid circular dependency
        from ulysses.social.models import FeedItem
        feeditem = FeedItem.objects.get(
            content_type=ContentType.objects.get_for_model(Event), 
            object_id=instance.id, 
            is_deleted=False
        )
        feeditem.is_deleted = True
        feeditem.save()
    except FeedItem.DoesNotExist:
        pass


class MemberCalendarItem(models.Model):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    is_set = models.BooleanField(default=False, verbose_name=_('is_set'))
    creation_datetime = models.DateTimeField(verbose_name=_('creation datetime'))

    class Meta:
        verbose_name = _('member calendar item')
        verbose_name_plural = _('member calendar items')
        ordering = ['event__start_date', 'event__end_date']

    def __str__(self):
        return '{0} - {1}'.format(self.event, self.user)


class UserEventAlert(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event_filter = models.TextField()
    label = models.TextField()
    creation_datetime = models.DateTimeField(verbose_name=_('creation datetime'))

    class Meta:
        verbose_name = _('user event alert')
        verbose_name_plural = _('user event alerts')
        ordering = ['creation_datetime']

    def __str__(self):
        return '{0} - {1}'.format(self.user, self.label)

    def load_filters(self):
        try:
            return json.loads(self.event_filter)
        except:
            return {}

    def get_event_filters(self):
        event_filter = self.load_filters()
        return event_filter.get('filters', [])

    def query_string(self):
        try:
            event_filter = self.load_filters()
            query_string = ''
            for query_key, query_filters in event_filter.items():
                query_value = ''
                for filter_elt in query_filters:
                    for filter_key, filter_value in filter_elt.items():
                        query_value += '{0}:{1}$'.format(filter_key, filter_value)
                query_string += '&{0}={1}'.format(query_key, query_value)
            return query_string
        except:
            return ''

    def get_absolute_url(self):
        return reverse('events:events_list') + '?' + self.query_string()


class EventNotification(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    creation_datetime = models.DateTimeField(verbose_name=_('creation datetime'))

    class Meta:
        verbose_name = _('event notification')
        verbose_name_plural = _('event notifications')
        ordering = ['-creation_datetime']

    def __str__(self):
        return '{0} - {1}'.format(self.user, self.event)
