# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects
from ulysses.profiles.factories import IndividualFactory
from ulysses.reference.factories import CountryFactory
from ulysses.works.factories import KeywordFactory

from ..factories import EventFactory
from ..models import Event, EventRole, EventType


class EventBaseTestCase(BaseTestCase):

    def setUp(self):
        super(EventBaseTestCase, self).setUp()
        CountryFactory.create(name="France", code='fr')


class EventDetailTest(EventBaseTestCase):

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user, visibility=Event.VISIBILITY_MEMBER)
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_public_event_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user, visibility=Event.VISIBILITY_PUBLIC)
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event1.title)
        self.assertNotContains(response, reverse('events:edit_event', args=[event1.id]))

    def test_view_as_me(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event1.title)
        self.assertContains(response, reverse('events:edit_event', args=[event1.id]))

    def test_view_as_other(self):
        """view """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile2.user)
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event1.title)
        self.assertNotContains(response, reverse('events:edit_event', args=[event1.id]))

    def test_view_as_anonymous_missing(self):
        """view """
        url = reverse('events:view_event', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_view_as_logged_missing(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:view_event', args=[1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class AddEventTest(EventBaseTestCase):

    def test_view_as_anonymous(self):
        """view """
        url = reverse('events:add_event')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

    def test_view_as_user(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:add_event')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select('form.colorbox-form')))

    def test_post_as_anonymous(self):
        """post """
        url = reverse('events:add_event')
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(0, queryset.count())
        assert_popup_redirects(response, reverse('login'))

    def test_post_as_user(self):
        """post """
        url = reverse('events:add_event')
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
            'visibility': Event.VISIBILITY_MEMBER,
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertEqual(event.title, data['title'])
        self.assertEqual(event.owner, profile.user)
        self.assertEqual(event.visibility, Event.VISIBILITY_MEMBER)
        assert_popup_redirects(response, reverse('events:view_event', args=[event.id]))

    def test_post_keywords(self):
        """post """
        url = reverse('events:add_event')
        keyword1 = KeywordFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'keywords': '##'.join([str(keyword1.id), 'GREAT']),
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertEqual(event.title, data['title'])
        self.assertEqual(event.owner, profile.user)
        self.assertEqual(event.keywords.count(), 2)
        self.assertEqual(event.keywords.filter(id=keyword1.id).count(), 1)
        self.assertEqual(event.keywords.filter(name='GREAT').count(), 1)
        self.assertEqual(event.visibility, Event.VISIBILITY_PUBLIC)
        assert_popup_redirects(response, reverse('events:view_event', args=[event.id]))

    def test_post_missing_title(self):
        """post """
        url = reverse('events:add_event')
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".errorlist")))
        queryset = Event.objects.all()
        self.assertEqual(0, queryset.count())

    def test_post_missing_start_date(self):
        """post """
        url = reverse('events:add_event')
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Hello',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".errorlist")))
        queryset = Event.objects.all()
        self.assertEqual(0, queryset.count())

    def test_add_with_members_involved(self):
        """post """
        profile = IndividualFactory.create()
        participant1 = IndividualFactory.create()
        participant2 = IndividualFactory.create()
        participant3 = IndividualFactory.create()
        role1 = EventRole.objects.get(name='Producer')
        url = reverse('events:add_event')
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
            'members_involved': [participant1.user.id, participant2.user.id],
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        assert_popup_redirects(response, reverse('events:view_event', args=[event.id]))
        self.assertEqual(event.title, data['title'])
        self.assertEqual(event.owner, profile.user)
        self.assertEqual(event.members_involved.count(), 2)


class EditEventTest(EventBaseTestCase):

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

    def test_view_as_user(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select('form.colorbox-form')))

    def test_view_as_other(self):
        """post """
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile2.user)
        url = reverse('events:edit_event', args=[event1.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_post_as_anonymous(self):
        """post """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
            'visibility': Event.VISIBILITY_MEMBER,
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertNotEqual(event.title, data['title'])
        self.assertEqual(event.visibility, Event.VISIBILITY_PUBLIC)
        assert_popup_redirects(response, reverse('login'))

    def test_post_as_user(self):
        """post """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
            'visibility': Event.VISIBILITY_MEMBER,
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertEqual(event.title, data['title'])
        self.assertEqual(event.owner, profile.user)
        self.assertEqual(event.visibility, Event.VISIBILITY_MEMBER)
        assert_popup_redirects(response, reverse('events:view_event', args=[event.id]))

    def test_post_default_visibility(self):
        """post """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertEqual(event.title, data['title'])
        self.assertEqual(event.owner, profile.user)
        self.assertEqual(event.visibility, Event.VISIBILITY_PUBLIC)
        assert_popup_redirects(response, reverse('events:view_event', args=[event.id]))

    def test_post_missing_title(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:edit_event', args=[event1.id])
        data = {
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select(".errorlist")))
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertNotEqual(event.title, '')

    def test_post_as_other(self):
        """post """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        profile2 = IndividualFactory.create()
        event1 = EventFactory.create(owner=profile2.user)
        url = reverse('events:edit_event', args=[event1.id])
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        data = {
            'title': 'Obladi-Oblada',
            'start_date': '2017-05-30',
            'event_type': EventType.objects.all()[0].id,
            'description': 'Text',
            'picture_media': "event.png",
            'event_venue': 'Paris (France)',
            'city_name': 'Paris',
            'country_name': 'France',
            'country': 'fr',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)
        queryset = Event.objects.all()
        self.assertEqual(1, queryset.count())
        event = queryset[0]
        self.assertNotEqual(event.title, data['title'])
        self.assertNotEqual(event.owner, profile.user)


class DeleteEventTest(BaseTestCase):

    def test_view_delete_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, Event.objects.count())

    def test_view_delete_as_other_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create()
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(1, Event.objects.count())

    def test_view_delete_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, Event.objects.count())

    def test_view_delete_invalid(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id + 1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(1, Event.objects.count())

    def test_post_delete_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(0, Event.objects.count())

    def test_post_delete_as_other_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create()
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(1, Event.objects.count())

    def test_post_delete_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, Event.objects.count())

    def test_post_delete_invalid(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event = EventFactory.create(owner=profile.user)
        url = reverse('events:delete_event', args=[event.id + 1])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(1, Event.objects.count())
