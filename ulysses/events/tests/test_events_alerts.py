# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, timedelta
import json

from django.template.defaultfilters import date as date_to_str
from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects
from ulysses.profiles.factories import IndividualFactory
from ulysses.reference.factories import CountryFactory, ContinentFactory

from ..models import UserEventAlert
from ..factories import EventTypeFactory, UserEventAlertFactory


class GetFiltersTest(BaseTestCase):

    def test_view_empty_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        url = reverse('events:get_filters')
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login'))

    def test_view_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertEqual(data['html'], '\n')
        self.assertEqual(data['label'], '')

    def test_view_event_filter_category(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:get_filters')
        response = self.client.get(url, data={'filters': 'event_type:{0}$'.format(category1.id)})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(data['html'].find(category1.name) >= 0)
        self.assertEqual(data['label'], category1.name)

    def test_view_event_filter_all_categories(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:get_filters')
        response = self.client.get(url, data={'filters': 'event_type:-1$'})
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(data['html'].find(_('All events')) >= 0)
        self.assertEqual(data['label'], _('All events'))

    def test_view_filter_event_date(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        date1_value = int(date1.strftime('%s')) * 1000
        response = self.client.get(
            url, data={'filters': 'event_date:{0}$'.format(date1_value)}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        label = _('Date: ') + date_to_str(date1)
        self.assertTrue(data['html'].find(label) >= 0)
        self.assertEqual(data['label'], label)

    def test_view_filter_event_country(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        response = self.client.get(
            url, data={'filters': 'event_country:{0}$'.format(country1.id)}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        label = country1.name
        self.assertTrue(data['html'].find(label) >= 0)
        self.assertEqual(data['label'], label)

    def test_view_filter_event_continent(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        continent1 = ContinentFactory.create()
        continent2 = ContinentFactory.create()
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        country3 = CountryFactory.create()
        continent1.countries.add(country1)
        continent1.countries.add(country3)
        continent1.save()
        continent2.countries.add(country2)
        continent2.save()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        response = self.client.get(
            url, data={'filters': 'event_country:-{0}$'.format(continent1.id)}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        label = continent1.name
        self.assertTrue(data['html'].find(label) >= 0)
        self.assertEqual(data['label'], label)

    def test_view_filter_event_location(self):
        """view """
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--20'
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:get_filters')
        response = self.client.get(
            url, data={'filters': 'event_location:{0}$'.format(ste_filter)}
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        label = _('{0} km{1} around ').format(20, 's') + 'Saint-Étienne (FR)'
        self.assertTrue(data['html'].find(label) >= 0)
        self.assertEqual(data['label'], label)

    def test_view_several_values(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        country1 = CountryFactory.create()
        url = reverse('events:get_filters')
        response = self.client.get(
            url,
            data={
                'filters': 'event_type:{0}$event_type:{1}$event_country:{2}$'.format(
                    category1.id, category2.id, country1.id
                )
            }
        )
        self.assertEqual(response.status_code, 200)
        data = json.loads(response.content)
        self.assertTrue(data['html'].find(category1.name) >= 0)
        self.assertTrue(data['html'].find(category2.name) >= 0)
        self.assertTrue(data['html'].find(country1.name) >= 0)
        self.assertEqual(data['label'], ', '.join([category1.name, category2.name, country1.name]))


class CreateAlertTest(BaseTestCase):

    def test_view_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:create_events_alert')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(soup.select('#id_label')[0].get('value', ''), _('All events'))
        self.assertEqual(soup.select('#id_filter')[0].get('value', ''), '{"filters": [{"event_type": -1}]}')
        self.assertEqual(UserEventAlert.objects.count(), 0)

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        url = reverse('events:create_events_alert')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(UserEventAlert.objects.count(), 0)

    def test_view_event_filter_category_all(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        response = self.client.get(url, data={'filters': 'event_type:-1$'})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(soup.select('#id_label')[0]['value'], _('All events'))
        self.assertEqual(
            soup.select('#id_filter')[0]['value'],
            '{"filters": [{"event_type": -1}]}'
        )
        self.assertEqual(UserEventAlert.objects.count(), 0)

    def test_view_event_filter_category(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        response = self.client.get(url, data={'filters': 'event_type:{0}$'.format(category1.id)})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(soup.select('#id_label')[0]['value'], category1.name)
        self.assertEqual(
            soup.select('#id_filter')[0]['value'],
            '{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        self.assertEqual(UserEventAlert.objects.count(), 0)

    def test_post_create_alert(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        data = {
            'filter': '{"filters": [{"event_type": ' + str(category1.id) + '}]}',
            'label': category1.name,
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('profiles:personal_space') + '?tab=bookmarks')
        self.assertEqual(UserEventAlert.objects.count(), 1)
        alert = UserEventAlert.objects.all()[0]
        self.assertEqual(alert.user, profile.user)
        self.assertEqual(alert.event_filter, data['filter'])
        self.assertEqual(alert.label, data['label'])
        self.assertEqual(alert.creation_datetime.date(), date.today())

    def test_post_create_alert_all(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        data = {
            'filter': '{"filters": [{"event_type": -1}]}',
            'label': 'All',
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('profiles:personal_space') + '?tab=bookmarks')
        self.assertEqual(UserEventAlert.objects.count(), 1)
        alert = UserEventAlert.objects.all()[0]
        self.assertEqual(alert.user, profile.user)
        self.assertEqual(alert.event_filter, data['filter'])
        self.assertEqual(alert.label, data['label'])
        self.assertEqual(alert.creation_datetime.date(), date.today())

    def test_post_create_alert_modified(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        data = {
            'filter': '{"filters": [{"p_event_type": ' + str(category1.id) + '}]}',
            'label': category1.name,
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('profiles:personal_space') + '?tab=bookmarks')
        self.assertEqual(UserEventAlert.objects.count(), 1)
        alert = UserEventAlert.objects.all()[0]
        self.assertEqual(alert.user, profile.user)
        self.assertEqual(alert.event_filter, '{"filters": [{"event_type": ' + str(category1.id) + '}]}')
        self.assertEqual(alert.label, data['label'])
        self.assertEqual(alert.creation_datetime.date(), date.today())

    def test_post_create_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        data = {
            'filter': '{"filters": [{"event_type": ' + str(category1.id) + '}]}',
            'label': category1.name,
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(UserEventAlert.objects.count(), 0)

    def test_post_create_alert_empty(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        url = reverse('events:create_events_alert')
        data = {
            'filters': '',
            'label': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        self.assertEqual(UserEventAlert.objects.count(), 0)


class EventAlertUrlTest(BaseTestCase):

    def test_event_alert_url(self):
        alert = UserEventAlertFactory.create(event_filter='{"filters":[{"event_type":1}]}')
        self.assertEqual(alert.get_absolute_url(), reverse('events:events_list') + '?&filters=event_type:1$')

    def test_event_alert_url_several_args(self):
        alert = UserEventAlertFactory.create(
            event_filter='{"filters":[{"event_type":1},{"event_country":-5}],"orders":[{"date":1}],"counter":[]}'
        )
        self.assertEqual(
            alert.get_absolute_url(),
            reverse('events:events_list') + '?&filters=event_type:1$event_country:-5$&orders=date:1$&counter='
        )

    def test_event_alert_invalid_url(self):
        alert = UserEventAlertFactory.create(event_filter='{toto}')
        self.assertEqual(alert.get_absolute_url(), reverse('events:events_list') + '?')


class DeleteAlertTest(BaseTestCase):

    def test_view_delete_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_view_delete_as_other_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create()
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_view_delete_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_view_delete_invalid(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id + 1])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_post_delete_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, reverse('profiles:personal_space') + '?tab=bookmarks')
        self.assertEqual(0, UserEventAlert.objects.count())

    def test_post_delete_as_other_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create()
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 403)
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_post_delete_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        # self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, UserEventAlert.objects.count())

    def test_post_delete_invalid(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        alert = UserEventAlertFactory.create(user=profile.user)
        url = reverse('events:delete_event_alert', args=[alert.id + 1])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(1, UserEventAlert.objects.count())
