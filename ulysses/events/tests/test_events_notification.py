# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django.core.management import call_command
from django.core import mail
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase

from ulysses.events.factories import EventFactory, EventTypeFactory, UserEventAlertFactory
from ulysses.events.models import EventNotification
from ulysses.profiles.factories import IndividualFactory
from ulysses.reference.factories import CountryFactory, ContinentFactory


class NotificationTest(BaseTestCase):

    def assertTextInHTML(self, text, email):
        self.assertEqual(email.alternatives[0][1], "text/html")
        self.assertTrue(email.alternatives[0][0].find(text) >= 0)

    def assertTextNotInHTML(self, text, email):
        self.assertEqual(email.alternatives[0][1], "text/html")
        self.assertTrue(email.alternatives[0][0].find(text) < 0)

    def test_notify_event_added(self):
        profile1 = IndividualFactory.create()
        category1 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event1.title, digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())

    def test_notify_event_several_users(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=category2)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        UserEventAlertFactory.create(
            user=profile2.user, event_filter='{"filters": [{"event_type": ' + str(category2.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 2)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event2.get_absolute_url(), digest_email)
        digest_email = mail.outbox[1]
        self.assertEqual(digest_email.to, [profile2.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event1.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile2.user, event=event2).count())

    def test_notify_event_several_events(self):
        profile1 = IndividualFactory.create()
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=category2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category2.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_only_future_events(self):
        profile1 = IndividualFactory.create()
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() - timedelta(days=1), event_type=category1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=category2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category2.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextNotInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_only_once(self):
        profile1 = IndividualFactory.create()
        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=category2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=category1)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category1.id) + '}]}'
        )
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(category2.id) + '}]}'
        )
        EventNotification.objects.create(user=profile1.user, event=event1, creation_datetime=datetime.now())
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextNotInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_several_args(self):
        profile1 = IndividualFactory.create()
        cat1 = EventTypeFactory.create()
        cat2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=cat2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_type": ' + str(cat1.id) + '}, {"event_type": ' + str(cat2.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_same_several_filters(self):
        profile1 = IndividualFactory.create()
        cat1 = EventTypeFactory.create()
        cat2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=cat2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_type": ' + str(cat1.id) + '},{"event_type": ' + str(cat2.id) + '}]}'
        )
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_type": ' + str(cat2.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_all_event_types(self):
        profile1 = IndividualFactory.create()
        cat1 = EventTypeFactory.create()
        cat2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=cat2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_type": -1}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_no_matching_events(self):
        profile1 = IndividualFactory.create()
        cat1 = EventTypeFactory.create()
        cat2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() - timedelta(days=1), event_type=cat1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2), event_type=cat2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), event_type=cat1)
        EventNotification.objects.create(user=profile1.user, event=event3, creation_datetime=datetime.now())
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_type": ' + str(cat1.id) + '}]}'
        )

        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_no_date(self):
        profile1 = IndividualFactory.create()
        cat1 = EventTypeFactory.create()
        event1 = EventFactory.create(event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_type": ' + str(cat1.id) + '}]}'
        )

        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event1).count())

    def test_notify_event_country(self):
        profile1 = IndividualFactory.create()
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), country=country1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), country=country2)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_country": ' + str(country1.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event2.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())

    def test_notify_event_continent(self):
        profile1 = IndividualFactory.create()
        continent1 = ContinentFactory.create()
        continent2 = ContinentFactory.create()
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        country3 = CountryFactory.create()
        continent1.countries.add(country1)
        continent1.countries.add(country3)
        continent1.save()
        continent2.countries.add(country2)
        continent2.save()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), country=country1)
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), country=country2)
        event3 = EventFactory.create(start_date=datetime.now() + timedelta(days=1), country=country1)
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_country": ' + str(- continent1.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event2.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())

    def test_notify_event_date(self):
        profile1 = IndividualFactory.create()
        event1 = EventFactory.create(start_date=datetime.now() + timedelta(days=1))
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=2))
        date1_value = int(event1.start_date.date().strftime('%s')) * 1000
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_date": ' + str(date1_value) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event2.get_absolute_url(), digest_email)
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event2).count())

    def test_notify_event_location(self):
        profile1 = IndividualFactory.create()
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--20'
        ste_bellevue = '45.41600079999999#4.393281000000002#FR'
        ste_center = '45.439695#4.387177899999983#FR'
        paris = '48.85661400000001#2.3522219000000177#FR'
        date1 = date=datetime.now() + timedelta(days=1)
        event1 = EventFactory.create(start_date=date1, location=paris)
        event2 = EventFactory.create(start_date=date1, location=ste_center)
        event3 = EventFactory.create(start_date=date1, location=ste_bellevue)
        event4 = EventFactory.create(start_date=date1, location='')
        UserEventAlertFactory.create(
            user=profile1.user, event_filter='{"filters": [{"event_location":"' + ste_filter + '"}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('New events have been added'))
        self.assertTextNotInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextInHTML(event3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event4.get_absolute_url(), digest_email)
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event3).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event4).count())

    def test_notify_event_type_and_location(self):
        profile1 = IndividualFactory.create()
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--20'
        ste_bellevue = '45.41600079999999#4.393281000000002#FR'
        ste_center = '45.439695#4.387177899999983#FR'
        paris = '48.85661400000001#2.3522219000000177#FR'
        date1 = date = datetime.now() + timedelta(days=1)
        cat1 = EventTypeFactory.create()
        cat2 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=date1, location=paris, event_type=cat1)
        event2 = EventFactory.create(start_date=date1, location=ste_center, event_type=cat1)
        event3 = EventFactory.create(start_date=date1, location=ste_bellevue, event_type=cat2)
        event4 = EventFactory.create(start_date=date1, location='', event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [{"event_location":"' + ste_filter + '"},{"event_type":' + str(cat1.id) + '}]}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile1.user.email])
        self.assertEqual(digest_email.subject, _('A new event has been added'))
        self.assertTextNotInHTML(event1.get_absolute_url(), digest_email)
        self.assertTextInHTML(event2.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event3.get_absolute_url(), digest_email)
        self.assertTextNotInHTML(event4.get_absolute_url(), digest_email)
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event1).count())
        self.assertEqual(1, EventNotification.objects.filter(user=profile1.user, event=event2).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event3).count())
        self.assertEqual(0, EventNotification.objects.filter(user=profile1.user, event=event4).count())

    def test_notify_invalid_alert(self):
        profile1 = IndividualFactory.create()
        date1 = date = datetime.now() + timedelta(days=1)
        cat1 = EventTypeFactory.create()
        event1 = EventFactory.create(start_date=date1, event_type=cat1)
        UserEventAlertFactory.create(
            user=profile1.user,
            event_filter='{"filters": [}'
        )
        call_command('notify_new_events', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
