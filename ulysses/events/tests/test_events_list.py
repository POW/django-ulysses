# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, timedelta

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory
from ulysses.reference.factories import CountryFactory, ContinentFactory

from ..factories import EventFactory, EventTypeFactory


class EventsListTest(BaseTestCase):

    def test_view_empty_as_anonymous(self):
        """view """
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_empty_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_as_anonymous(self):
        """view """
        profile = IndividualFactory.create()
        event1 = EventFactory.create(start_date=date.today())
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event1.get_absolute_url())
        self.assertContains(response, event1.title)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select(".list-item")))

    def test_view_as_user(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date.today())
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, event1.get_absolute_url())
        self.assertContains(response, event1.title)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(1, len(soup.select(".list-item")))
        self.assertContains(response, url + '?as=ical')

    def test_view_only_future_events(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date.today())
        event2 = EventFactory.create(start_date=date.today() + timedelta(days=1))
        event3 = EventFactory.create(start_date=date.today() - timedelta(days=1))
        event4 = EventFactory.create(start_date=date.today() - timedelta(days=1), end_date=date.today())
        event5 = EventFactory.create(
            start_date=date.today() + timedelta(days=1), end_date=date.today() + timedelta(days=5)
        )
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event4, event1, event2, event5]  # order by date
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)
        positions = [soup.select('a[href="{0}"]'.format(event.get_absolute_url() for event in expected))]
        self.assertEqual(positions, sorted(positions))  # make sure events are displayed in order

    def test_view_no_future_events(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date.today() - timedelta(days=1))
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(0, len(soup.select(".list-item")))
        self.assertContains(response, url + '?as=ical')

    def test_view_pagination(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        in_events = [EventFactory.create(start_date=date.today() + timedelta(days=i)) for i in range(36)]
        # out by pagination
        [EventFactory.create(start_date=date.today() + timedelta(days=len(in_events) + i)) for i in range(3)]
        url = reverse('events:events_list')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(len(in_events), len(soup.select(".list-item")))
        for event in in_events:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_event_filter_category(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()

        event1 = EventFactory.create(start_date=date.today(), event_type=category1)
        event2 = EventFactory.create(start_date=date.today(), event_type=category1)
        event3 = EventFactory.create(start_date=date.today(), event_type=category2)
        event4 = EventFactory.create(start_date=date.today(), event_type=None)

        url = reverse('events:events_list')
        response = self.client.get(url, data={'filters': 'event_type:{0}$'.format(category1.id)})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event1, event2]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)
        self.assertContains(
            response, url + '?as=ical&amp;filters=event_type:{0}$'.format(category1.id)
        )

    def test_view_event_filter_categories(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()

        event1 = EventFactory.create(start_date=date.today(), event_type=category1)
        event2 = EventFactory.create(start_date=date.today(), event_type=category1)
        event3 = EventFactory.create(start_date=date.today(), event_type=category2)
        event4 = EventFactory.create(start_date=date.today(), event_type=None)

        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_type:{0}$event_type:{1}$'.format(category1.id, category2.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event1, event2, event3]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_event_filter_categories_none(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()
        category3 = EventTypeFactory.create()

        event1 = EventFactory.create(start_date=date.today(), event_type=category1)
        event2 = EventFactory.create(start_date=date.today(), event_type=category2)

        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_type:{0}$'.format(category3.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(0, len(soup.select(".list-item")))

    def test_view_filter_event_date(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        date2 = date.today() + timedelta(days=5)
        date3 = date.today() + timedelta(days=15)

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1)
        event2 = EventFactory.create(start_date=date1 + timedelta(days=1))
        event3 = EventFactory.create(start_date=date2)
        event4 = EventFactory.create(
            start_date=date1 - timedelta(days=1), end_date=date1 + timedelta(days=1)
        )
        event5 = EventFactory.create(start_date=date3, end_date=date3 + timedelta(days=1))
        url = reverse('events:events_list')
        date1_value = int(date1.strftime('%s')) * 1000
        response = self.client.get(
            url, data={'filters': 'event_date:{0}$'.format(date1_value)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event4, event1]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_dates(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        date2 = date.today() + timedelta(days=5)
        date3 = date.today() + timedelta(days=15)

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1)
        event2 = EventFactory.create(start_date=date1 + timedelta(days=1))
        event3 = EventFactory.create(start_date=date2)
        event4 = EventFactory.create(
            start_date=date1 - timedelta(days=1), end_date=date1 + timedelta(days=1)
        )
        event5 = EventFactory.create(start_date=date3, end_date=date3 + timedelta(days=1))
        url = reverse('events:events_list')
        date1_value = int(date1.strftime('%s')) * 1000
        date2_value = int(date2.strftime('%s')) * 1000
        response = self.client.get(
            url, data={'filters': 'event_date:{0}$event_date:{1}$'.format(date1_value, date2_value)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event3, event4, event1,]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_dates_none(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1 + timedelta(days=1))
        url = reverse('events:events_list')
        date1_value = int(date1.strftime('%s')) * 1000
        response = self.client.get(
            url, data={'filters': 'event_date:{0}$'.format(date1_value)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(0, len(soup.select(".list-item")))

    def test_view_filter_event_country(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, country=country1)
        event2 = EventFactory.create(start_date=date1, country=country1)
        event3 = EventFactory.create(start_date=date1, country=country2)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_country:{0}$'.format(country1.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event1, event2]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_countries(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        country3 = CountryFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, country=country1)
        event2 = EventFactory.create(start_date=date1, country=country1)
        event3 = EventFactory.create(start_date=date1, country=country2)
        event4 = EventFactory.create(start_date=date1, country=country3)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_country:{0}$event_country:{1}$'.format(country1.id, country2.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event1, event2, event3]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_countries_empty(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        country3 = CountryFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, country=country1)
        event2 = EventFactory.create(start_date=date1, country=country1)
        event3 = EventFactory.create(start_date=date1, country=country2)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_country:{0}$'.format(country3.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(0, len(soup.select(".list-item")))

    def test_view_filter_event_continent(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        continent1 = ContinentFactory.create()
        continent2 = ContinentFactory.create()
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        country3 = CountryFactory.create()
        continent1.countries.add(country1)
        continent1.countries.add(country3)
        continent1.save()
        continent2.countries.add(country2)
        continent2.save()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, country=country1)
        event2 = EventFactory.create(start_date=date1, country=country1)
        event3 = EventFactory.create(start_date=date1, country=country2)
        event4 = EventFactory.create(start_date=date1, country=country3)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_country:-{0}$'.format(continent1.id)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event1, event2, event4]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_location(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--20'
        ste_bellevue = '45.41600079999999#4.393281000000002#FR'
        ste_center = '45.439695#4.387177899999983#FR'
        paris = '48.85661400000001#2.3522219000000177#FR'

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, location=paris)
        event2 = EventFactory.create(start_date=date1, location=ste_center)
        event3 = EventFactory.create(start_date=date1, location=ste_bellevue)
        event4 = EventFactory.create(start_date=date1, location='')
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_location:{0}$'.format(ste_filter)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event2, event3]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_location_exact(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--0'
        ste_bellevue = '45.41600079999999#4.393281000000002#FR'
        ste_center = '45.439695#4.387177899999983#FR'
        paris = '48.85661400000001#2.3522219000000177#FR'

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, location=paris)
        event2 = EventFactory.create(start_date=date1, location=ste_center)
        event3 = EventFactory.create(start_date=date1, location=ste_bellevue)
        event4 = EventFactory.create(start_date=date1, location='')
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_location:{0}$'.format(ste_filter)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        expected = [event2]
        self.assertEqual(len(expected), len(soup.select(".list-item")))
        for event in expected:
            self.assertContains(response, event.get_absolute_url())
            self.assertContains(response, event.title)

    def test_view_filter_event_location_none(self):
        """view """
        date1 = date.today() + timedelta(days=10)
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--20'
        paris = '48.85661400000001#2.3522219000000177#FR'

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, location=paris)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={'filters': 'event_location:{0}$'.format(ste_filter)}
        )
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(0, len(soup.select(".list-item")))
