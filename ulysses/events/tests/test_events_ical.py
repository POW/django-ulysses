# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django.urls import reverse

from icalendar import Calendar
import pytz
from rest_framework import status

from ulysses.generic.tests import BaseTestCase
from ulysses.profiles.factories import IndividualFactory
from ulysses.reference.factories import CountryFactory

from ..factories import EventFactory, EventTypeFactory


class IcalBaseTestCase(BaseTestCase):

    @staticmethod
    def _ical_to_datetime(ical_value, utc=False, tzid=""):
        ical_dt = None
        if ical_value:
            try:
                ical_dt = ical_value.to_ical().decode("utf-8")
            except Exception as err:
                ical_dt = ical_value

        if ical_dt:
            try:
                if 'T' in ical_dt:
                    y, mo, d, h, mi, s = (
                        ical_dt[:4], ical_dt[4:6], ical_dt[6:8], ical_dt[9:11], ical_dt[11:13], ical_dt[13:15]
                    )
                else:
                    y, mo, d, h, mi, s = ical_dt[:4], ical_dt[4:6], ical_dt[6:8], 0, 0, 0
                y, mo, d, h, mi, s = tuple([int(x) for x in (y, mo, d, h, mi, s)])
                if utc:
                    if ('Z' == ical_dt[-1]) or (tzid == ""):
                        if h == 0 and mi == 0 and s == 0:
                            dt = datetime(y, mo, d, h, mi, s)
                        else:
                            dt = datetime(y, mo, d, h, mi, s, tzinfo=pytz.utc)
                    else:
                        try:
                            tz = pytz.timezone(tzid)
                            dt = datetime(y, mo, d, h, mi, s)
                            dt = tz.localize(dt)
                        except:
                            dt = datetime(y, mo, d, h, mi, s)
                else:
                    dt = datetime(y, mo, d, h, mi, s)
                return dt
            except Exception:
                raise
        return None

    @staticmethod
    def _get_events_from_ical(content):
        calendar = Calendar.from_ical(content)
        ical_events = []
        for component in calendar.walk():
            if component.name == "VEVENT":
                ical_event = {}
                ical_event['uid'] = component.get('uid', "")
                ical_event['summary'] = component.get('summary', "")
                ical_event['description'] = component.get('description', "")
                ical_event['dtstart'] = IcalBaseTestCase._ical_to_datetime(component.get('dtstart', ""))
                ical_event['dtend'] = IcalBaseTestCase._ical_to_datetime(component.get('dtend', ""))
                ical_event['last_modified'] = IcalBaseTestCase._ical_to_datetime(component.get('last-modified', ""))
                ical_events.append(ical_event)
        return ical_events


class EventsICalTest(IcalBaseTestCase):

    def test_get_events_empty(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        EventFactory.create(start_date=datetime.now() - timedelta(days=1))  # past event
        EventFactory.create()  # no date
        url = reverse('events:events_list')
        response = self.client.get(url, data={'as': 'ical'})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        ical_events = self._get_events_from_ical(response.content)
        self.assertEqual(0, len(ical_events))

    def test_get_events_ical(self):
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=datetime.now())
        event2 = EventFactory.create(start_date=datetime.now() + timedelta(days=1))
        event3 = EventFactory.create(start_date=datetime.now() - timedelta(days=1))
        event4 = EventFactory.create(start_date=datetime.now() - timedelta(days=1), end_date=datetime.now())
        event5 = EventFactory.create(
            start_date=datetime.now() + timedelta(days=1), end_date=datetime.now() + timedelta(days=5)
        )
        url = reverse('events:events_list')
        response = self.client.get(url, data={'as': 'ical'})
        self.assertEqual(response.status_code, 200)
        expected = [event4, event1, event2, event5]
        ical_events = self._get_events_from_ical(response.content)
        self.assertEqual(len(expected), len(ical_events))
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_get_ical_filter_categories(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))

        category1 = EventTypeFactory.create()
        category2 = EventTypeFactory.create()

        event1 = EventFactory.create(start_date=datetime.now(), event_type=category1)
        event2 = EventFactory.create(start_date=datetime.now(), event_type=category1)
        event3 = EventFactory.create(start_date=datetime.now(), event_type=category2)
        event4 = EventFactory.create(start_date=datetime.now(), event_type=None)
        expected = [event1, event2, event3]

        url = reverse('events:events_list')
        response = self.client.get(
            url, data={
                'filters': 'event_type:{0}$event_type:{1}$'.format(category1.id, category2.id),
                'as': 'ical',
            }
        )
        self.assertEqual(response.status_code, 200)
        ical_events = self._get_events_from_ical(response.content)
        self.assertEqual(len(expected), len(ical_events))
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_view_filter_event_date(self):
        """view """
        date1 = datetime.now() + timedelta(days=10)
        date2 = datetime.now() + timedelta(days=5)
        date3 = datetime.now() + timedelta(days=15)

        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1)
        event2 = EventFactory.create(start_date=date1 + timedelta(days=1))
        event3 = EventFactory.create(start_date=date2)
        event4 = EventFactory.create(
            start_date=date1 - timedelta(days=1), end_date=date1 + timedelta(days=1)
        )
        event5 = EventFactory.create(start_date=date3, end_date=date3 + timedelta(days=1))
        url = reverse('events:events_list')
        date1_value = int(date1.strftime('%s')) * 1000
        response = self.client.get(
            url, data={
                'filters': 'event_date:{0}$'.format(date1_value),
                'as': 'ical',
            }
        )
        self.assertEqual(response.status_code, 200)
        expected = [event4, event1]
        ical_events = self._get_events_from_ical(response.content)
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_get_ical_filter_event_country(self):
        """view """
        date1 = datetime.now() + timedelta(days=10)
        country1 = CountryFactory.create()
        country2 = CountryFactory.create()
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, country=country1)
        event2 = EventFactory.create(start_date=date1, country=country1)
        event3 = EventFactory.create(start_date=date1, country=country2)
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={
                'filters': 'event_country:{0}$'.format(country1.id),
                'as': 'ical',
            }
        )
        self.assertEqual(response.status_code, 200)
        expected = [event1, event2]
        ical_events = self._get_events_from_ical(response.content)
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_get_ical_filter_event_location_exact(self):
        """view """
        date1 = datetime.now() + timedelta(days=10)
        ste_filter = 'Saint-Étienne--45,439695--4,387177899999983--FR--0'
        ste_bellevue = '45.41600079999999#4.393281000000002#FR'
        ste_center = '45.439695#4.387177899999983#FR'
        paris = '48.85661400000001#2.3522219000000177#FR'
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(start_date=date1, location=paris)
        event2 = EventFactory.create(start_date=date1, location=ste_center)
        event3 = EventFactory.create(start_date=date1, location=ste_bellevue)
        event4 = EventFactory.create(start_date=date1, location='')
        expected = [event2]
        url = reverse('events:events_list')
        response = self.client.get(
            url, data={
                'filters': 'event_location:{0}$'.format(ste_filter),
                'as': 'ical',
            }
        )
        self.assertEqual(response.status_code, 200)
        ical_events = self._get_events_from_ical(response.content)
        self.assertEqual(len(expected), len(ical_events))
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_get_ical_no_pagination(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        in_events = [EventFactory.create(start_date=datetime.now() + timedelta(days=i)) for i in range(36)]
        # out by pagination
        [EventFactory.create(start_date=datetime.now() + timedelta(days=len(in_events) + i)) for i in range(3)]
        url = reverse('events:events_list')
        response = self.client.get(url, data={'as': 'ical'})
        self.assertEqual(response.status_code, 200)
        ical_events = self._get_events_from_ical(response.content)
        self.assertEqual(39, len(ical_events))


class EventDetailICalTest(IcalBaseTestCase):

    def test_view_ical(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile.user, start_date=datetime.now())
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url, data={'as': 'ical'})
        self.assertEqual(response.status_code, 200)
        ical_events = self._get_events_from_ical(response.content)
        expected = [event1]
        self.assertEqual(len(expected), len(ical_events))
        for event, ical_event in zip(expected, ical_events):
            self.assertEqual(ical_event['summary'], event.title)
            self.assertEqual(ical_event['dtstart'], event.start_date.replace(microsecond=0))
            if event.end_date:
                self.assertEqual(ical_event['dtend'], event.end_date.replace(microsecond=0))
            self.assertEqual(ical_event['description'], event.description)
            self.assertEqual(ical_event['uid'], str(event.id))
            self.assertEqual(ical_event['last_modified'], event.update_date.replace(microsecond=0))

    def test_view_ical_no_start_date(self):
        """view """
        profile = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile.user)
        url = reverse('events:view_event', args=[event1.id])
        response = self.client.get(url, data={'as': 'ical'})
        self.assertEqual(response.status_code, 404)
