# -*- coding: utf-8 -*-

from datetime import datetime, date, time, timedelta
from decimal import Decimal, ConversionSyntax

from django.db.models import Q

from ulysses.events.models import Event, EventNotification
from ulysses.generic.utils import haversine
from ulysses.generic.views import FilterPageMixin

from .forms import EventSearchForm


def get_events_queryset(only_with_photos=False):
    """

    Returns: queryset of events

    """
    queryset = Event.objects.all()

    if only_with_photos:
        queryset = queryset.exclude(picture__isnull=True)

    return queryset


def get_forthcoming_events_queryset():
    """

    Returns: queryset of events in future

    """
    today_datetime = datetime.combine(date.today(), time.min)
    queryset = Event.objects.filter(
        Q(start_date__gte=today_datetime, end_date__isnull=True) |
        Q(end_date__gte=today_datetime, end_date__isnull=False)
    )
    if queryset.count() == 0:
        queryset = Event.objects.all()

    return queryset


def get_upcoming_events_queryset(take_time_into_account=False):
    """
    Returns: queryset of events in future
    """
    if take_time_into_account:
        today_datetime = datetime.now()
    else:
        today_datetime = datetime.combine(date.today(), time.min)

    queryset = Event.objects.filter(
        Q(start_date__gte=today_datetime, end_date__isnull=True) |
        Q(end_date__gte=today_datetime, end_date__isnull=False)
    ).order_by('start_date')

    return queryset


def get_events(limit_to=None, order_by=None, only_with_photos=False, queryset=None):
    """
    get events

    Args:
        limit_to: if set, limit to the numbers of events

    Returns: list of events

    """
    # get events
    if queryset is None:
        events = get_events_queryset(only_with_photos=only_with_photos)
    else:
        events = queryset

    if order_by is None:
        order_by = ['start_date']

    events = events.order_by(*order_by)

    if limit_to:
        # Take limit_to events
        events = events[:limit_to]

    return events


def get_forthcoming_events(limit_to=None, order_by=None):
    """
        get future events

        Args:
            limit_to: if set, limit to the numbers of events

        Returns: list of events

    """
    return get_events(limit_to=limit_to, order_by=None, queryset=get_forthcoming_events_queryset())


def get_events_count():
    """
    Returns: the number of events
    """
    return get_events_queryset().count()


def get_forthcoming_events_count():
    """
    Returns: the number of events in future
    """
    return get_forthcoming_events_queryset().count()


def get_events_in_range(from_date, to_date, queryset=None):
    """
    return All events in range :
    - no end and start in range
    - start and end in the range
    - ends in the range
    - starts in the range
    - starts before and ends after the range
    """

    if queryset is None:
        queryset = Event.objects

    # Get the next day of end_date to include all events of the last day (event at 30/06/2017 21:00)
    if isinstance(to_date, datetime):
        to_end_date = to_date.date()
    else:
        to_end_date = to_date
        to_end_date = datetime.combine(to_end_date, time.min) + timedelta(days=1)

    lookup1 = {'end_date__isnull': True}
    if from_date:
        lookup1['start_date__gte'] = from_date
    if to_date:
        lookup1['start_date__lt'] = to_end_date  # to_end_date is the day after to_date so don't include it

    lookup2 = {'end_date__isnull': False}
    if from_date:
        lookup2['end_date__gte'] = from_date
    if to_date:
        lookup2['start_date__lt'] = to_end_date  # to_end_date is the day after to_date so don't include it

    return queryset.filter(Q(**lookup1)| Q(**lookup2))


def filter_events(queryset, field, values):

    if field == 'event_type':
        if -1 in values:
            queryset = queryset.all()
        else:
            queryset = queryset.filter(event_type__in=values)

    if field == 'event_date':
        original_queryset = queryset
        event_date_queryset = None
        event_date_queryset_is_set = False
        for value in values:
            value = datetime.fromtimestamp(value / 1000).date()
            if not event_date_queryset_is_set:
                event_date_queryset_is_set = True
                event_date_queryset = get_events_in_range(value, value, original_queryset)
            else:
                event_date_queryset |= get_events_in_range(value, value, original_queryset)
        if event_date_queryset_is_set:
            queryset = event_date_queryset

    if field == 'event_location':
        matching_events = []
        for value in values:
            value_items = value.split('--')
            city, lat, long, country, radius = None, None, None, None, 1
            if len(value_items) >= 3:
                city, lat, long = value_items[:3]
            if len(value_items) >= 5:
                try:
                    radius = int(value_items[4]) or 1
                except (ValueError, TypeError):
                    radius = 1
            if lat is not None and long is not None:
                filter_lat = Decimal(lat.replace(',', '.'))
                filter_long = Decimal(long.replace(',', '.'))
                for event in queryset.exclude(location=''):
                    try:
                        event_lat = Decimal(event.latitude())
                        event_long = Decimal(event.longitude())
                        distance = haversine(event_long, event_lat, filter_long, filter_lat)
                        if distance < radius:
                            matching_events.append(event.id)
                    except ConversionSyntax:
                        pass
        queryset = queryset.filter(id__in=matching_events)

    if field == 'event_country':
        country_values = []
        continent_values = []
        for value in values:
            if value > 0:
                country_values.append(value)
            else:
                continent_values.append(-value)
        queryset = queryset.filter(
            Q(country__in=country_values) |
            Q(country__continent__in=continent_values)
        )
    return queryset


class EventFilter(FilterPageMixin):

    def _do_filter_items(self, queryset, field, values):
        return filter_events(queryset, field, values)


def get_alert_upcoming_events(event_alert):
    filters = event_alert.get_event_filters()
    if filters:
        data = {'filters': filters}
        page_filter = EventFilter()
        page_filter.init_form(filters, EventSearchForm, data)
        events = page_filter.get_filtered_items(get_upcoming_events_queryset(), filters)
        already_notified = EventNotification.objects.filter(user=event_alert.user, event__in=events).values('event')
        return events.exclude(id__in=already_notified)
    else:
        return Event.objects.none()
