# -*- coding: utf-8 -*-

from django.contrib.admin import ModelAdmin, TabularInline

from ulysses.super_admin.sites import super_admin_site

from .models import (
    Event, EventType, MemberCalendarItem, EventRole, EventCollaborator, UserEventAlert, EventNotification
)


class EventCollaboratorInline(TabularInline):
    model = EventCollaborator
    fields = ('role', 'user')
    raw_id_fields = ('user', )


class EventAdmin(ModelAdmin):
    list_display = (
        'title', 'owner', 'start_date', 'end_date', 'event_type', 'city', 'country', "ulysses_label", 'visibility',
    )
    date_hierarchy = 'start_date'
    list_filter = ("ulysses_label", 'event_type', 'country', 'visibility', )
    raw_id_fields = ('organizators', 'performers', 'works', 'owner', 'members_involved', )
    inlines = [EventCollaboratorInline]
    search_fields = ('title', )


class MemberCalendarItemAdmin(ModelAdmin):
    list_display = ('id', 'event', 'user', 'is_set', 'creation_datetime')
    list_filter = ('is_set', )
    raw_id_fields = ('user', 'event', )
    date_hierarchy = 'creation_datetime'


class EventRoleAdmin(ModelAdmin):
    list_display = ("name", "order_index", )
    list_editable = ("order_index", )
    search_fields = ("name", )


class UserEventAlertAdmin(ModelAdmin):
    list_display = ('label', 'user', )
    raw_id_fields = ('user',)
    date_hierarchy = 'creation_datetime'


class EventNotificationAdmin(ModelAdmin):
    list_display = ('user', 'event', 'creation_datetime')
    raw_id_fields = ('user', 'event')
    date_hierarchy = 'creation_datetime'


super_admin_site.register(EventRole, EventRoleAdmin)
super_admin_site.register(Event, EventAdmin)
super_admin_site.register(EventType)
super_admin_site.register(MemberCalendarItem, MemberCalendarItemAdmin)
super_admin_site.register(UserEventAlert, UserEventAlertAdmin)
super_admin_site.register(EventNotification, EventNotificationAdmin)
