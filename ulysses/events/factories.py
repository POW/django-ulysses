# -*- coding: utf-8 -*-

from datetime import datetime
import factory

from ulysses.profiles.factories import IndividualFactory

from .models import Event, EventType, MemberCalendarItem, EventRole, EventCollaborator, UserEventAlert


class EventTypeFactory(factory.DjangoModelFactory):
    class Meta:
        model = EventType

    name = factory.Sequence(lambda n: 'event-type-{0}'.format(n))
    order_index = factory.Sequence(lambda n: n)


class EventRoleFactory(factory.DjangoModelFactory):
    class Meta:
        model = EventRole

    name = factory.Sequence(lambda n: 'event-role-{0}'.format(n))


class EventFactory(factory.DjangoModelFactory):
    class Meta:
        model = Event

    title = factory.Sequence(lambda n: 'event-{0}'.format(n))

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        owner = kwargs.get('owner', None)

        performers = kwargs.pop('performers', [])
        collaborators = kwargs.pop('collaborators', [])
        organizators = kwargs.pop('organizators', [])

        if not owner:
            individual = IndividualFactory.create()
            kwargs['owner'] = individual.user

        event = super(EventFactory, cls)._create(model_class, *args, **kwargs)

        role = EventRoleFactory.create()
        for collaborator in collaborators:
            EventCollaborator.objects.create(event=event, user=collaborator.user, role=role)

        for organizator in organizators:
            event.organizators.add(organizator.user)

        for performer in performers:
            event.performers.add(performer.user)

        return event


class MemberCalendarItemFactory(factory.DjangoModelFactory):
    class Meta:
        model = MemberCalendarItem

    creation_datetime = datetime.now()


class UserEventAlertFactory(factory.DjangoModelFactory):
    class Meta:
        model = UserEventAlert

    label = factory.Sequence(lambda n: 'alert-{0}'.format(n))
    creation_datetime = datetime.now()
    event_filter = '{"filters":[{"event_type":1}}'

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        user = kwargs.get('user', None)

        if not user:
            individual = IndividualFactory.create()
            kwargs['user'] = individual.user

        obj = super(UserEventAlertFactory, cls)._create(model_class, *args, **kwargs)

        return obj
