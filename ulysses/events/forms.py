# -*- coding: utf-8 -*-

from datetime import datetime

from django.template.defaultfilters import date as date_to_str
from django.utils.translation import ugettext_lazy as _, ugettext

import floppyforms.__future__ as forms

from ulysses.generic.forms import FileUploadForm, ItemsSearchForm
from ulysses.generic.widgets import DateTimePicker

from ulysses.profiles.fields import MultipleUserChoiceField
from ulysses.reference.models import Country, Continent
from ulysses.works.fields import MultipleKeywordsChoiceField

from .models import Event, EventType


class EventForm(FileUploadForm):
    """add or edit a work"""
    instance = None
    members_involved = MultipleUserChoiceField(required=False, label=_('Members involved'))
    keywords = MultipleKeywordsChoiceField(required=False, label=_('Keywords'))
    event_venue = forms.CharField(
        required=True,
        label=_('Event venue'),
        widget=forms.TextInput(attrs={'placeholder': _('Type in the name of the event venue')})
    )
    country_name = forms.CharField(
        widget=forms.TextInput(attrs={'readonly': 'readonly'})
    )
    country = forms.CharField(widget=forms.HiddenInput(), required=True)

    class Meta:
        model = Event
        fields = (
            'title', 'picture', 'event_type', 'start_date', 'end_date',
            'event_venue', 'place_name', 'city', 'country', 'country_name', 'location',
            'price', 'description', 'link', 'members_involved', 'keywords', 'visibility',
        )
        upload_fields = ('picture',)
        upload_fields_args = {
            'picture': {'extensions': 'png|jpg', 'label': _("Photo"), 'upload_url_name': 'events:upload_event_file'},
        }
        widgets = {
            'start_date': DateTimePicker(),
            'end_date': DateTimePicker(),
            'place_name': forms.TextInput(attrs={'readonly': 'readonly'}),
            'city': forms.TextInput(attrs={'readonly': 'readonly'}),
            'location': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        self.instance = kwargs.get('instance', None)
        initial = kwargs.get('initial', None) or {}
        if self.instance and self.instance.country:
            initial['country'] = self.instance.country.code.upper()
            initial['country_name'] = self.instance.country.name
            kwargs['initial'] = initial
        super(EventForm, self).__init__(*args, **kwargs)

        for field in ('title', 'picture_media', 'event_type', 'start_date', 'description', ):
            self.fields[field].required = True
        self.fields['visibility'].required = False

        self.fields['members_involved'].widget.attrs['data-placeholder'] = ugettext(
            'Type in a member or leave the field empty'
        )
        if self.instance and self.instance.id:
            self.fields['members_involved'].set_initial(self.instance.members_involved.all())

    def clean_end_date(self):
        start_date = self.cleaned_data.get('start_date', None)
        end_date = self.cleaned_data['end_date']
        if end_date:
            if not start_date:
                raise forms.ValidationError(ugettext('A start date is expected'))
            if end_date < start_date:
                raise forms.ValidationError(ugettext('The end date must be after the start date'))
        return end_date

    def clean_country(self):
        country = self.cleaned_data.get('country')
        if country:
            try:
                return Country.objects.get(code=country.lower())
            except Country.DoesNotExist:
                raise forms.ValidationError(ugettext('Unknown country'))


class EventSearchBaseForm(ItemsSearchForm):

    def init_countries(self, field_name):
        continent_choices = []
        for continent in Continent.objects.all():
            continent_choices.append(
                (-continent.id, continent.name)
            )
        country_choices = []
        for country in Country.objects.all():
            country_choices.append(
                (country.id, country.name)
            )
        self.fields[field_name].choices = [
            ('', '',),
            (_('Continents'), continent_choices),
            (_('Countries'), country_choices),
        ]

    def _do_get_label(self, field_label, key, item):

        if key == 'event_date':
            return '{0}: {1}'.format(field_label, self.to_value(item))

        elif key == 'event_location':
            return self.to_value(item)

        elif key == 'event_country':
            if item > 0:
                return Country.objects.get(id=item).name
            else:
                return Continent.objects.get(id=-item).name

        elif key == 'event_type' and item and item.get('event_type') == -1:
            return ugettext('All events')

        else:
            return str(field_label)

    def to_value(self, item):
        for key, value in list(item.items()):
            if key == 'event_date':
                return date_to_str(datetime.fromtimestamp(value / 1000).date())

            elif key == 'event_location':
                value_items = value.split('--')
                city, lat, long, country, radius = None, None, None, None, None
                if len(value_items) >= 3:
                    city, lat, long = value_items[:3]
                if len(value_items) >= 4:
                    country = value_items[3]
                    if city and country:
                        city += ' ({0})'.format(country)
                if len(value_items) >= 5:
                    try:
                        radius = int(value_items[4])
                    except (ValueError, TypeError):
                        radius = 0
                if not city:
                    return ''
                if radius:
                    return _('{0} km{1} around ').format(radius, 's' if radius > 1 else '') + city
                else:
                    return city

            else:
                return value
        return ""


class EventSearchForm(EventSearchBaseForm):
    """search for a events"""

    event_type = forms.ModelChoiceField(
        queryset=EventType.objects.all(), required=False, label=_('Category')
    )
    event_date = forms.CharField(
        required=False, label=_('Date'),
        widget=forms.TextInput(attrs={'class': 'event-select-datepicker', 'readonly': 'readonly'})
    )
    event_country = forms.ChoiceField(required=False, label=_('Country'))

    event_location = forms.CharField(
        required=False, label=_('Location'),
        widget=forms.TextInput(attrs={'class': 'event-location-picker', 'readonly': 'readonly'})
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_countries('event_country')
        self._post_init()


class SaveEventSearchForm(EventSearchBaseForm):
    label = forms.CharField(required=False, widget=forms.HiddenInput())
    p_event_type = forms.ModelChoiceField(
        queryset=EventType.objects.all(), required=False, label=_('Category')
    )
    p_event_date = forms.CharField(
        required=False, label=_('Date'),
        widget=forms.TextInput(attrs={'class': 'popup-select-datepicker', 'readonly': 'readonly'})
    )
    p_event_country = forms.ChoiceField(required=False, label=_('Country'))
    p_event_location = forms.CharField(
        required=False, label=_('Location'),
        widget=forms.TextInput(attrs={'class': 'p_event-location-picker', 'readonly': 'readonly'})
    )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.init_countries('p_event_country')
        self._post_init()
        # Add all events
        event_type_choices = [self.fields['p_event_type'].choices[0]]
        event_type_choices += [(-1, ugettext('All events'))]
        event_type_choices += self.fields['p_event_type'].choices[1:]
        self.fields['p_event_type'].choices = event_type_choices

    def clean(self):
        cleaned_data = super().clean()
        if not cleaned_data.get('label', ''):
            raise forms.ValidationError(_('Please enter at least one filter'))
        return cleaned_data
