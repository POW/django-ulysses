# -*- coding: utf-8 -*-

from datetime import date, datetime
import json
import os.path
import mimetypes
import xlwt

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.contrib.auth.views import redirect_to_login as django_redirect_to_login
from django.core.exceptions import PermissionDenied
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.http import (
    HttpResponse, HttpResponseRedirect, Http404, HttpResponseNotFound, HttpResponseForbidden
)
from django.shortcuts import resolve_url
from django.urls import reverse
from django.utils.dateformat import DateFormat
from django.utils.decorators import method_decorator
from django.utils.six.moves.urllib.parse import urlparse
from django.utils.encoding import smart_str
from django.views.generic import FormView, View, TemplateView

from ulysses.utils import ChunkedFile, get_safe_sheet_name


def ulysses_popup_redirect(view_func):
    """manage redirection : close the coorbox. Very useful for form-based views"""
    def wrapper(request, *args, **kwargs):
        try:
            response = view_func(request, *args, **kwargs)
        except Http404:
            return HttpResponseNotFound()
        except PermissionDenied:
            return HttpResponseForbidden()
        except Exception as msg:
            raise
        if response.status_code == 302:
            script = '<script>$.colorbox.close(); loadUrl("{0}"); $("body").trigger("colorboxRedirect");</script>'
            script = script.format(response['Location'])
            return HttpResponse(script)
        elif response.status_code != 200:
            return HttpResponse(status=response.status_code)
        else:
            return response
    return wrapper


class PopupFormView(FormView):
    """Base class for popup form : redirect if success"""
    template_name = "colorbox/popup_form_base.html"
    form_url = ""
    title = ""
    form_class = None
    success_url = ""
    staff_only = False

    def get_context_data(self, **kwargs):
        """update template context"""
        context = super(PopupFormView, self).get_context_data(**kwargs)
        context['form_url'] = self.get_form_url()
        context['title'] = self.get_title()
        return context

    def get_form_url(self):
        """get url for submitting the form"""
        return self.form_url

    def get_title(self):
        """get title"""
        return self.title

    @method_decorator(ulysses_popup_redirect)
    def dispatch(self, *args, **kwargs):
        """Manage permissions and use decorator for redirection"""
        if self.staff_only:
            if not self.request.user.is_staff:
                raise PermissionDenied
        return super(PopupFormView, self).dispatch(*args, **kwargs)


class PopupView(TemplateView):
    """Base class for popup form : redirect if success"""
    template_name = "colorbox/popup_base.html"
    title = ""

    def get_context_data(self, **kwargs):
        """update template context"""
        context = super(PopupView, self).get_context_data(**kwargs)
        context['title'] = self.get_title()
        return context

    def get_title(self):
        """get title"""
        return self.title


class ProfileFormBaseView(FormView):
    """A view which requires login"""
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProfileFormBaseView, self).dispatch(request, *args, **kwargs)


class ProfileTemplateView(TemplateView):
    """A view which requires login"""
    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(ProfileTemplateView, self).dispatch(request, *args, **kwargs)


class DetailView(TemplateView):
    """Add a back"""
    model_class = None
    default_back_url = None

    def get_context_data(self, **kwargs):

        context = super(DetailView, self).get_context_data(**kwargs)
        back_url = self.request.session.pop('back_url', '')
        # force reverse_lazy execution
        default_back_url = '{0}'.format(self.default_back_url)

        if default_back_url not in back_url:
            back_url = default_back_url

        context.update({'back_url': back_url,})
        return context


class LoadMorePaginator(Paginator):
    """cumulate all pages started from 0"""
    load_more = True

    def page(self, number):
        """
        Returns a Page object for the given 1-based page number.
        """
        number = self.validate_number(number)
        bottom = (number - 1) * self.per_page
        top = bottom + self.per_page
        if top + self.orphans >= self.count:
            top = self.count
        return self._get_page(self.object_list[0:top], number, self)


class PaginationMixin(object):
    page = None
    _GET = None
    load_more_pagination = False

    @property
    def GET(self):
        if self._GET is None:
            self._GET = dict(self.request.GET)
        return self._GET

    def get_page(self):
        """returns page"""
        if self.page is None:
            if self.GET.pop('page', None):
                self.page = self.request.GET.get('page')
            else:
                self.page = 1
        return self.page

    def paginate(self, items, count):
        """returns paginated items"""
        page = self.get_page()
        if self.load_more_pagination:
            paginator = LoadMorePaginator(items, count)
        else:
            paginator = Paginator(items, count)
        try:
            items = paginator.page(page)
        except PageNotAnInteger:
            # If page is not an integer, deliver first page.
            items = paginator.page(1)
        except EmptyPage:
            # If page is out of range (e.g. 9999), deliver last page of results.
            items = paginator.page(paginator.num_pages)
        return items


class SearchFormView(FormView, PaginationMixin):
    """search form"""
    order_form = None
    has_order_by = False

    def get_order_form_class(self):
        """Returns a form for ordering items"""
        # To be inherited in order to support ordering of items
        return None

    def get_form_kwargs(self):
        """Returns the keyword arguments for instantiating the form."""
        kwargs = {
            'initial': self.get_initial(),
            'prefix': self.get_prefix(),
        }

        self.get_page()

        # Try to create an order form to order items in the search form
        order_form_class = self.get_order_form_class()
        if order_form_class and 'order_by' in self.GET:
            self.order_form = order_form_class(data=self.request.GET)
            self.GET.pop('order_by')
            self.has_order_by = True
            self.order_form.is_valid()

        if self.request.method in ('POST', 'PUT') or (self.request.method == 'GET' and self.GET):
            kwargs.update({
                'data': self.request.POST or self.request.GET,
                'files': self.request.FILES,
            })
        return kwargs

    def get(self, request, *args, **kwargs):
        """Process GET requests"""
        if self.GET:
            # Store the query for getting back to the search results
            self.request.session['back_url'] = self.request.get_full_path()
            # If parameters are passed as GET, process the request like a POST
            return super(SearchFormView, self).post(request, *args, **kwargs)
        else:
            return super(SearchFormView, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(SearchFormView, self).get_context_data(**kwargs)
        if self.has_order_by or self.GET:
            context['get_querystr'] = '&'.join(
                [''] + [
                    '{0}={1}'.format(key, value)
                    for key, value in list(self.request.GET.items()) if key != 'page' and value
                ]
            )

        order_form_class = self.get_order_form_class()
        if order_form_class and self.order_form is None:
            self.order_form = order_form_class()

        context['order_form'] = self.order_form

        return context


class AjaxView(View):
    """returns Json content"""
    login_required = False

    def dispatch(self, request, *args, **kwargs):
        if self.login_required and request.user.is_anonymous:
            return HttpResponseRedirect(reverse('login'))
        return super().dispatch(request, *args, **kwargs)

    def get_json_data(self):
        """Override and return data to dump"""
        return []

    def get(self, *args, **kwargs):
        """handle request"""
        json_data = self.get_json_data()
        json_text = json.dumps(json_data)
        return HttpResponse(json_text, content_type='application/json')


class LoginRequiredPopupFormView(PopupFormView):
    user = None

    @method_decorator(ulysses_popup_redirect)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            return HttpResponseRedirect(reverse('login'))
        self.user = request.user
        return super().dispatch(request, *args, **kwargs)


class LoginRequiredPopupView(PopupView):
    user = None

    @method_decorator(ulysses_popup_redirect)
    def dispatch(self, request, *args, **kwargs):
        if request.user.is_anonymous:
            return HttpResponseRedirect(reverse('login'))
        self.user = request.user
        return super(LoginRequiredPopupView, self).dispatch(request, *args, **kwargs)


class PrivateDownloadView(View):
    """
    protected download for documents.
    Media files can not be accessed from file url and must pass through this verification
    """
    as_attachment = True

    def get_file(self, request, **kwargs):
        """Override this method to the requested file and check permissions"""
        raise NotImplemented

    def build_response(self, file):
        """return a HttpResponse for a file"""

        file_name = os.path.split(file.name)[-1]

        # Try to get the content type
        content_type = mimetypes.MimeTypes().guess_type(file_name)[0]
        if content_type is None:
            content_type = "application/octet-stream"

        if settings.DEBUG:
            # Debug mode : Put the whole file here
            try:
                response = HttpResponse(ChunkedFile(file), content_type=content_type)
            except (ValueError, IOError) as err:
                raise Http404(str(err))
        else:
            # Set nginx header
            response = HttpResponse(content_type=content_type)
            try:
                response['X-Accel-Redirect'] = file.url
            except (ValueError, IOError) as err:
                raise Http404(str(err))

        if self.as_attachment:
            response['Content-Disposition'] = smart_str('attachment; filename={0}'.format(file_name))

        file_size = None
        try:
            file_size = file.size
        except FileNotFoundError:
            pass
        if file_size is not None:
            response['Content-Length'] = file.size

        return response

    def get(self, request, **kwargs):
        """handle HTTP GET query : get a file and returns it"""
        return self.build_response(self.get_file(request, **kwargs))


class FilterPageMixin(object):
    filter_form = None

    @staticmethod
    def _extract_from_get(get_value, prefix=''):
        values = []
        if get_value:
            for item in get_value.split('$'):
                if item:
                    try:
                        key, value = item.split(':')
                    except ValueError:
                        key, value = None, None
                    if value:
                        try:
                            value = int(value)
                        except (ValueError, TypeError):
                            pass
                    if key:
                        if prefix:
                            # When used in a popup (ie create event alerts), we can not reuse the ids several times
                            # Because it can cause some problems to js (datepicker for example)
                            # We need to prefix the fields with a prefix and to remove it when handling the queries
                            key = key.replace(prefix, '')
                        values.append({key: value})
        return values

    def get_filters(self, prefix=''):
        return self._extract_from_get(self.request.GET.get('filters'), prefix)

    def init_form(self, filters, filter_form_class, data, prefix=''):
        # filters
        if filter_form_class:
            self.filter_form = filter_form_class(initial={'filter': json.dumps(data)})
            filter_fields = [
                {
                    'name': list(filter_item.keys())[0],
                    'value': self.filter_form.to_value(filter_item),
                    'label': self.filter_form.get_label(filter_item, prefix),
                    'raw_value': self.filter_form.to_raw_value(filter_item),
                }
                for filter_item in filters
            ]
        else:
            filter_fields = []
            self.filter_form = None
        return filter_fields

    def get_filtered_items(self, queryset, filters):
        """handle search form"""
        filter_elements = [list(elt.items())[0] for elt in filters]
        regrouped_fields = {}
        for field, value in filter_elements:
            if field in regrouped_fields:
                regrouped_fields[field].append(value)
            else:
                regrouped_fields[field] = [value]

        for field, values in list(regrouped_fields.items()):
            queryset = self._do_filter_items(queryset, field, values)

        return queryset.distinct()

    def _do_filter_items(self, queryset, field, values):
        return queryset

    @staticmethod
    def get_label(filter_fields):
        return ', '.join([elt['label'] for elt in filter_fields])


class ListBasePageView(TemplateView, FilterPageMixin):

    VIEWS = 'views'
    RECOMMENDATIONS = 'recommendations'
    DATE = 'date'
    ALPHABETICAL = 'alpha'
    FOLLOWERS = 'followers'
    ULYSSES_LABElS = 'ulysses'

    ITEMS_COUNTER_STEP = 20
    ROWS_LG_LIMIT = 0  # limit the number of lines shown on small screen
    ORDER_BY_CHOICES = []
    DEFAULT_ORDERING = DATE

    def get_counter(self):
        counter = self._extract_from_get(self.request.GET.get('counter'))
        if len(counter):
            return list(counter[0].values())[0]
        else:
            if self.ROWS_LG_LIMIT:
                return self.ITEMS_COUNTER_STEP * 2
            else:
                return self.ITEMS_COUNTER_STEP

    def get_lg_limit(self):
        counter = self._extract_from_get(self.request.GET.get('counter'))
        if len(counter):
            return 0
        else:
            return self.ROWS_LG_LIMIT

    def get_order_by(self):
        orders = self._extract_from_get(self.request.GET.get('orders'))
        if len(orders):
            return list(orders[0].keys())[0]
        else:
            return self.DEFAULT_ORDERING

    def get_filter_form_class(self):
        raise NotImplemented

    def get_queryset(self):
        raise NotImplemented

    def get_ordering(self, order_by):
        raise NotImplemented

    def order_items(self, queryset, order_by, ordering):
        return queryset.order_by(*ordering)

    def get_context_data(self, **kwargs):
        """add data to template context"""
        context = super(ListBasePageView, self).get_context_data(**kwargs)
        counter = self.get_counter()
        filters = self.get_filters()
        order_by = self.get_order_by()

        data = {
            'filters': filters,
            'orders': [{order_by: 1}],
            'counter': [{'items': counter}],
        }

        # filters
        filter_form_class = self.get_filter_form_class()
        filter_fields = self.init_form(filters, filter_form_class, data)

        context['order_by_value'] = order_by
        context['order_bys'] = self.ORDER_BY_CHOICES
        context['filter_fields'] = filter_fields
        context['form'] = self.filter_form

        # items
        queryset = self.get_queryset()
        queryset = self.get_filtered_items(queryset, filters)
        ordering = self.get_ordering(order_by)
        queryset = self.order_items(queryset, order_by, ordering)

        # Pagination
        context['items_count'] = queryset.count()
        queryset = queryset[:counter]
        context['items'] = queryset
        context['hide_load_more'] = queryset.count() == context['items_count']
        context['load_more_step'] = self.ITEMS_COUNTER_STEP
        context['lg_limit'] = self.get_lg_limit()

        return context


class XlsExportMixin(object):
    doc_name = 'ulysses.xls'
    _col_widths = None
    _line_heights = None
    _default_style = None
    _error_style = None
    max_width = 10000
    truncated_sign = '[TRUNCATED] '

    def get_default_style(self):
        """
        * Colour index
        8 through 63. 0 = Black, 1 = White, 2 = Red, 3 = Green, 4 = Blue, 5 = Yellow, 6 = Magenta,
        7 = Cyan, 16 = Maroon, 17 = Dark Green, 18 = Dark Blue, 19 = Dark Yellow , almost brown),
        20 = Dark Magenta, 21 = Teal, 22 = Light Gray, 23 = Dark Gray, the list goes on... sty
        * Borders
        borders.left, borders.right, borders.top, borders.bottom
        May be: NO_LINE, THIN, MEDIUM, DASHED, DOTTED, THICK, DOUBLE, HAIR, MEDIUM_DASHED,
        THIN_DASH_DOTTED, MEDIUM_DASH_DOTTED, THIN_DASH_DOT_DOTTED, MEDIUM_DASH_DOT_DOTTED,
        SLANTED_MEDIUM_DASH_DOTTED, or 0x00 through 0x0D.
        borders = xlwt.Borders()
        borders.left = xlwt.Borders.THIN
        borders.right = xlwt.Borders.THIN
        borders.top = xlwt.Borders.THIN
        borders.bottom = xlwt.Borders.THIN
        borders.left_colour = 0x00
        borders.right_colour = 0x00
        borders.top_colour = 0x00
        borders.bottom_colour = 0x00
        style.borders = borders
        * Fonts
        style.font = xlwt.Font()
        style.font.height = 8 * 20
        style.font.colour_index = 22
        * Alignment
        style.alignment = xlwt.Alignment()
        style.alignment.horz = xlwt.Alignment.HORZ_LEFT
        style.alignment.vert = xlwt.Alignment.VERT_CENTER
        * Pattern
        May be: NO_PATTERN, SOLID_PATTERN, or 0x00 through 0x12
        style.pattern = xlwt.Pattern()
        style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        style.pattern.pattern_fore_colour = 23
        """
        style = xlwt.XFStyle()
        return style

    def _calculate_size(self, ws, line, column, value):
        col_widths = [len(value_lines) for value_lines in '{0}'.format(value).split("\n")]
        line_height = (len(col_widths) * 240) if len(col_widths) > 1 else 0
        width = 1500 + max(col_widths) * 220
        width = min(width, self.max_width)
        if width > self._col_widths.get(column, 0):
            self._col_widths[column] = width
            ws.col(column).width = width
        if line_height > self._line_heights.get(line, 0):
            self._line_heights[line] = line_height
            ws.row(line).height_mismatch = True
            ws.row(line).height = line_height

    def get_value(self, value):

        if isinstance(value, str) and len(value) > 32767:
            truncated_length = 32767 - len(self.truncated_sign)
            value = self.truncated_sign + value[:truncated_length]

        elif isinstance(value, date):
            date_format = DateFormat(value)
            return date_format.format("d/m/y").capitalize()

        elif isinstance(value, datetime):
            date_format = DateFormat(value)
            return date_format.format("d/m/y H:i").capitalize()

        return value

    def write_cell(self, sheet, line, column, value, *args, **kwargs):
        value = self.get_value(value)
        if isinstance(value, str) and self.truncated_sign in value:
            style = self._error_style
        else:
            style = kwargs.pop('style', None) or self._default_style
        ret = sheet.write(line, column, value, style, *args, **kwargs)
        self._calculate_size(sheet, line, column, value)
        return ret

    def write_merge(self, sheet, line1, line2, column1, column2, value, *args, **kwargs):
        value = self.get_value(value)
        style = kwargs.pop('style', None) or self._default_style
        ret = sheet.write_merge(line1, line2, column1, column2, value, style, *args, **kwargs)
        self._calculate_size(sheet, line1, column1, value)
        return ret

    def get_doc_name(self):
        return self.doc_name

    @staticmethod
    def get_safe_sheet_name(name):
        return get_safe_sheet_name(name)

    def build_response(self, workbook):
        response = HttpResponse(content_type="application/ms-excel")
        response['Content-Disposition'] = 'attachment; filename={0}'.format(self.get_doc_name())
        workbook.save(response)
        return response


class XlsExportView(View, XlsExportMixin):
    only_staff = True
    only_superuser = False

    def __init__(self, *args, **kwargs):
        super(XlsExportView, self).__init__(*args, **kwargs)
        self._default_style = self.get_default_style()
        self._error_style = xlwt.XFStyle()
        self._error_style.pattern = xlwt.Pattern()
        self._error_style.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
        self._error_style.pattern.pattern_fore_colour = 5

    def dispatch(self, *args, **kwargs):
        if not self.check_permission():
            raise PermissionDenied()
        return super(XlsExportView, self).dispatch(*args, **kwargs)

    def check_permission(self):
        if self.only_superuser:
            return self.request.user.is_superuser
        if self.only_staff:
            return self.request.user.is_staff
        return True

    def do_fill_workbook(self, workbook):
        """implement it in base class"""
        pass

    def get(self, *args, **kwargs):
        workbook = xlwt.Workbook()
        self._col_widths = {}
        self._line_heights = {}
        self.do_fill_workbook(workbook)
        return self.build_response(workbook)


def redirect_to_login(request):
    path = request.build_absolute_uri()
    resolved_login_url = resolve_url(settings.LOGIN_URL)
    # If the login url is the same scheme and net location then just
    # use the path as the "next" url.
    login_scheme, login_netloc = urlparse(resolved_login_url)[:2]
    current_scheme, current_netloc = urlparse(path)[:2]
    if ((not login_scheme or login_scheme == current_scheme) and
            (not login_netloc or login_netloc == current_netloc)):
        path = request.get_full_path()
    return django_redirect_to_login(
        path, resolved_login_url, REDIRECT_FIELD_NAME
    )
