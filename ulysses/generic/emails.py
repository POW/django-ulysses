# -*- coding: utf-8 -*-

from django.conf import settings
from django.core.mail import get_connection, EmailMultiAlternatives, EmailMessage
from django.template.loader import get_template

from ulysses.utils import dehtml


def send_email(subject, template_name, context, dests, sender=None, cc_list=None):
    """Send an HTML email"""
    emails = []
    connection = get_connection()
    from_email = sender if sender else settings.DEFAULT_FROM_EMAIL
    for address in dests:
        context['email_address'] = address
        the_template = get_template(template_name)
        html_text = the_template.render(context)
        text = dehtml(html_text)
        email = EmailMultiAlternatives(subject, text, from_email, [address], cc=cc_list if cc_list else [])
        email.attach_alternative(html_text, "text/html")
        emails.append(email)
    return connection.send_messages(emails)


def send_text_email(subject, msg_body, from_email, tos, ccs=None):
    """Send text email"""
    email = EmailMessage(
        subject,
        msg_body,
        from_email if from_email else settings.DEFAULT_FROM_EMAIL,
        tos,
        cc=ccs if ccs is not None else [],
    )
    email.send()
