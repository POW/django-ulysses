# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class NameIndexBaseModel(models.Model):
    """Base class for model with name and index"""
    name = models.CharField(max_length=200, verbose_name=_('name'))
    order_index = models.IntegerField(default=0, verbose_name=_('order index'))

    class Meta:
        abstract = True

    def save(self, **kwargs):
        if self.order_index == 0:
            self.order_index = self.__class__.objects.count()
        return super(NameIndexBaseModel, self).save(**kwargs)

    def __str__(self):
        return self.name
