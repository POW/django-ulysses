# -*- coding: utf-8 -*-

from datetime import date, datetime, time
import re

from django.contrib.auth.models import User
from django.utils import formats
from django.utils.safestring import mark_safe

from tinymce.settings import JS_URL as TINYMCE_JS_URL
from tinymce.widgets import TinyMCE
import floppyforms.__future__ as forms

from ulysses.profiles.utils import get_profile


mce_font_style = 'bold,italic,underline'
mce_list = 'bullist,numlist'
mce_align = 'justifyleft,justifycenter,justifyright,justifyfull'
mce_indent = 'outdent,indent'
mce_copy_paste = 'cut,copy,paste'
mce_undo = 'undo,redo'
mce_link = 'link,unlink'
mce_format = 'formatselect,removeformat'
mce_char = 'charmap'
mce_paste = 'pastetext,pasteword,selectall'


def mce_line(*args):
    return ',|,'.join(args)


DEFAULT_TINCY_MCE_ATTRS = {
    'data-theme_advanced_statusbar_location': 'bottom',
    'data-theme_advanced_buttons1': mce_line(mce_font_style, mce_format, mce_copy_paste, mce_paste, mce_undo),
    'data-theme_advanced_buttons2': mce_line(mce_list, mce_indent, mce_align, mce_link, mce_char),
    'data-theme_advanced_buttons3': '',
    'data-height': '100px'
}


MINI_TINCY_MCE_ATTRS = {
    'data-theme_advanced_statusbar_location': 'bottom',
    'data-theme_advanced_buttons1': mce_font_style,
    'data-theme_advanced_buttons2': '',
    'data-theme_advanced_buttons3': '',
    'data-height': '100px'
}


class FileUploadControl(forms.TextInput):
    initial_image = None
    template_name = "widgets/fileupload_control.html"

    def __init__(self, upload_url, allowed_extensions='', no_help=False, *args, **kwargs):
        self.upload_url = upload_url
        self.allowed_extensions = allowed_extensions
        self.no_help = no_help
        super(FileUploadControl, self).__init__(*args, **kwargs)

    def get_context(self, name, value, attrs=None):
        context = super(FileUploadControl, self).get_context(name, value, attrs=attrs)
        context['upload_url'] = self.upload_url
        context['allowed_extensions'] = self.allowed_extensions
        context['initial_image'] = self.initial_image
        if not self.no_help:
            context['allowed_extensions_label'] = ', '.join([elt.upper() for elt in self.allowed_extensions.split('|')])
        return context


class DateTimePicker(forms.TextInput):
    template_name = "widgets/date_time_picker.html"
    format = None

    def format_value(self, value):
        return formats.localize_input(value, self.format or formats.get_format('DATETIME_INPUT_FORMATS')[0])

    def get_context(self, name, value, attrs=None):

        context = super(DateTimePicker, self).get_context(name, value, attrs=attrs)
        if value and isinstance(value, datetime):
            date_value = formats.localize_input(
                value.date(), self.format or formats.get_format('DATE_INPUT_FORMATS')[0]
            )
            time_value = formats.localize_input(
                value.time(), self.format or formats.get_format('TIME_INPUT_FORMATS')[0]
            )
            if value.time() == time.min:
                context['allday'] = True

            context['date_value'] = date_value
            context['time_value'] = time_value

        elif value and isinstance(value, str):

            try:
                the_date, the_time = value.split(" ")
            except ValueError:
                the_date = value
                the_time = time.min

            context['date_value'] = the_date
            context['time_value'] = the_time
            if the_time == time.min or the_time == "" or the_time == "00:00:00":
                context['allday'] = True

        else:
            context['allday'] = True

        return context


class DateRangeInput(forms.TextInput):
    template_name = "widgets/date_range_input.html"

    def format_value(self, value):
        if value:
            date_range = [val or '' for val in value]
            str_value = '{0}#{1}'.format(*date_range)
            return str_value
        return value

    def item_to_date(self, date_value):
        try:
            digits = [int(x) for x in date_value.split('-')]
            if len(digits):
                while len(digits) < 3:
                    digits.append(1)
            return date(*digits[:3])
        except ValueError:
            return None

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value and '#' in value:
            items = value.split("#")
            from_value = self.item_to_date(items[0])
            to_value = self.item_to_date(items[1])
            if from_value or to_value:
                return [from_value, to_value]
        return []

    def get_context(self, name, value, attrs=None):
        context = super(DateRangeInput, self).get_context(name, value, attrs=attrs)
        if value:
            items = value
            if items[0] and isinstance(items[0], date):
                context['from_value'] = formats.localize_input(
                    items[0], formats.get_format('DATE_INPUT_FORMATS')[0]
                )
            if items[1] and isinstance(items[1], date):
                context['to_value'] = formats.localize_input(
                    items[1], formats.get_format('DATE_INPUT_FORMATS')[0]
                )
        return context


class IntRangeInput(forms.TextInput):
    template_name = "widgets/int_range_input.html"

    def format_value(self, value):
        if value:
            int_range = [val or '' for val in value]
            str_value = '{0}#{1}'.format(*int_range)
            return str_value
        return value

    def item_to_int(self, int_value):
        try:
            return int(int_value)
        except ValueError:
            return None

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value and '#' in value:
            items = value.split("#")
            from_value = self.item_to_int(items[0])
            to_value = self.item_to_int(items[1])
            if from_value or to_value:
                return [from_value, to_value]
        return []

    def get_context(self, name, value, attrs=None):
        context = super(IntRangeInput, self).get_context(name, value, attrs=attrs)
        if value:
            items = value
            if items[0] and isinstance(items[0], int):
                context['from_value'] = items[0]
            if items[1] and isinstance(items[1], int):
                context['to_value'] = items[1]
        return context


class OnMapLocationInput(forms.TextInput):
    template_name = "widgets/osm_map.html"

    def __init__(self, city_name_field=None, country_name_field=None, *args, **kwargs):
        self.city_name_field = city_name_field or 'city'
        self.country_name_field = country_name_field or 'country'
        super(OnMapLocationInput, self).__init__(*args, **kwargs)

    class Media:
        css = {
            'all': ('https://unpkg.com/leaflet@0.7.3/dist/leaflet.css', )
        }
        js = ('https://unpkg.com/leaflet@0.7.3/dist/leaflet.js', )

    def get_context(self, name, value, attrs=None):
        context = super(OnMapLocationInput, self).get_context(name, value, attrs=attrs)
        if value and '#' in value:
            items = value.split("#")
            context['lat'] = items[0]
            context['lng'] = items[1]
            if len(items) > 2:
                context['country_code'] = value.split("#")[2]
        context['city_name_field'] = self.city_name_field
        context['country_name_field'] = self.country_name_field
        return context


class TagAutocompleteBaseWidget(forms.TextInput):
    template_name = "widgets/tag_input.html"
    tag_model = None
    field_name = 'name'
    case_sensitive = False

    def __init__(self, available_tags=None, *args, **kwargs):
        available_tags = available_tags or self.tag_model.objects.all()
        self.available_names = [getattr(tag, self.field_name) for tag in available_tags]
        super(TagAutocompleteBaseWidget, self).__init__(*args, **kwargs)
        if self.tag_model is None:
            raise RuntimeError(
                'TagAutocompleteBaseWidget is a base class: Create a child class and define tag_model attribute'
            )

    def get_context(self, name, value, attrs=None):
        context = super(TagAutocompleteBaseWidget, self).get_context(name, value, attrs=attrs)
        context['available_names'] = self.available_names
        tags = []
        for tag_id in value:
            try:
                tag_obj = self.tag_model.objects.get(id=tag_id)
                tags.append(getattr(tag_obj, self.field_name))
            except self.tag_model.DoesNotExist:
                pass
        context['value'] = ' '.join(tags)
        return context

    def _get_tag_lookup(self, tag_value, create=False):
        """lookup for getting a tag. This can be overridden"""
        field_name = self.field_name
        if not create and not self.case_sensitive:
            field_name += "__iexact"
        return {
            field_name: tag_value
        }

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value:
            tags = value.split(" ")
            cleaned_tags = []
            for tag in tags:
                stripped_tag = tag.strip()
                if stripped_tag:
                    try:
                        lookup = self._get_tag_lookup(stripped_tag)
                        tag_obj = self.tag_model.objects.get(**lookup)
                    except self.tag_model.DoesNotExist:
                        lookup = self._get_tag_lookup(stripped_tag, create=True)
                        tag_obj = self.tag_model.objects.create(**lookup)
                    cleaned_tags.append(tag_obj)
            return cleaned_tags
        return []


class NameAutocompleteBaseWidget(forms.TextInput):
    template_name = "widgets/name_input.html"
    tag_model = None
    field_name = 'name'
    case_sensitive = False

    def __init__(self, available_tags=None, *args, **kwargs):
        available_tags = available_tags or self.tag_model.objects.all()
        self.available_names = [getattr(tag, self.field_name) for tag in available_tags]
        super(NameAutocompleteBaseWidget, self).__init__(*args, **kwargs)
        if self.tag_model is None:
            raise RuntimeError(
                'NameAutocompleteBaseWidget is a base class: Create a child class and define tag_model attribute'
            )

    def get_context(self, name, value, attrs=None):
        context = super(NameAutocompleteBaseWidget, self).get_context(name, value, attrs=attrs)
        context['available_names'] = self.available_names
        context['value'] = value
        return context

    def _get_tag_lookup(self, tag_value, create=False):
        """lookup for getting a tag. This can be overridden"""
        field_name = self.field_name
        if not create and not self.case_sensitive:
            field_name += "__iexact"
        return {
            field_name: tag_value
        }

    def value_from_datadict(self, data, files, name):
        value = data.get(name, None)
        if value:
            try:
                lookup = self._get_tag_lookup(value)
                tag_obj = self.tag_model.objects.get(**lookup)
            except self.tag_model.DoesNotExist:
                lookup = self._get_tag_lookup(value, create=True)
                tag_obj = self.tag_model.objects.create(**lookup)
            except self.tag_model.MultipleObjectsReturned:
                lookup = self._get_tag_lookup(value)
                tag_obj = self.tag_model.objects.filter(**lookup)[0]

            return tag_obj

        return ""


class SimpleHtmlWidget(TinyMCE):

    def __init__(self, *args, **kwargs):
        super(SimpleHtmlWidget, self).__init__(*args, **kwargs)

    def _media(self):
        js = [
            TINYMCE_JS_URL,
            # 'django_tinymce/jquery-1.9.1.min.js',  # Jquery is already loaded. It must not be imported twice
            'js/tinymce_callbacks.js',
            'js/init_tinymce2.js',
        ]
        return forms.Media(js=js)
    media = property(_media)


class AutocompleteAutocreateWidget(forms.TextInput):
    """An autocomplete widget for choosing or adding tags"""
    template_name = "widgets/autocomplete_autocreate_input.html"
    model = None
    autocomplete_url = None
    text_field = None  # Name of a field in charge of "text" values, If none : create new objects with name
    separator = "##"

    def __init__(self, *args, **kwargs):
        self.placeholder_text = kwargs.pop('placeholder_text', "")
        self.help_text = kwargs.pop('help_text', "")
        super(AutocompleteAutocreateWidget, self).__init__(*args, **kwargs)
        self.all_items = self.model.objects.all()

    @staticmethod
    def as_name(item):
        if isinstance(item, User):
            profile = get_profile(item)
            if profile:
                return profile.name
            else:
                return '{0}'.format(item)
        else:
            return item.name

    def get_context(self, name, value, attrs=None):
        context = super(AutocompleteAutocreateWidget, self).get_context(name, value, attrs=attrs)

        # extract form prefix if widget is used in a formset
        formset_prefix = ""
        prefix_match = re.match("(?P<prefix>form\-\d+\-)(?P<raw_name>.+)", name)
        if prefix_match:
            formset_prefix = prefix_match.group("prefix")

        context['all_items'] = self.all_items
        if value:
            context['value_ids'] = self.separator.join([str(item.id) for item in value])
            context['value'] = [(item.id, self.as_name(item)) for item in value]
        else:
            context['value_ids'] = ""
            context['value'] = []
        context['placeholder_text'] = self.placeholder_text
        context['help_text'] = self.help_text
        if self.text_field:
            context['text_field'] = formset_prefix + self.text_field
        context['separator'] = self.separator
        context["autocomplete_url"] = self.autocomplete_url
        return context

    def value_from_datadict(self, data, files, name):

        value = data.get(name, None)
        item_ids = []
        if value:
            for elt in value.split(self.separator):
                elt = elt.strip()
                if elt:
                    try:
                        elt_id = int(elt)
                    except ValueError:
                        elt_id = None

                    if elt_id is None and self.text_field is None:
                        new_keyword = self.model.objects.get_or_create(name=elt)[0]
                        elt_id = new_keyword.id

                    item_ids.append(elt_id)
        return list(self.model.objects.filter(id__in=item_ids))


class HtmlWidget(forms.Textarea):

    def __init__(self, maximum_length=0, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {}
        attrs['class'] = 'form-control tinymce'
        attrs.update(MINI_TINCY_MCE_ATTRS)
        super(HtmlWidget, self).__init__(attrs, *args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        html = super(HtmlWidget, self).render(name, value, attrs, **kwargs)
        button_html = ''
        button_html += '<div class="tinymce-word-counter" id="word_counter_id_{0}">0</div>'.format(
            name,
        )
        output = mark_safe('<div class="html-widget">{0}{1}</div>'.format(button_html, html))
        return output


class StaticLabelWidget(forms.TextInput):
    """This is a dummy field which allows to insert text in the form"""
    has_error = False

    def __init__(self, label, *args, **kwargs):
        self.label = label
        super(StaticLabelWidget, self).__init__(*args, **kwargs)

    def render(self, name, value, attrs=None, **kwargs):
        label_html = '<div class="form-static-label {1}">{0}</div>'.format(
            self.label, 'form-static-label-error' if self.has_error else ''
        )
        output = mark_safe(label_html)
        return output
