# -*- coding: utf-8 -*-

import os

from django.conf import settings

from ulysses.middleware import get_request
from ulysses.utils import upload_helper

EVENTS_DIRECTORY = "events"
CALLS_DIRECTORY = "calls"
WORKS_DIRECTORY = "works"


def get_user_directory(parent_directory, user_id):
    """Get the path where to store file """
    folders = (parent_directory, str(user_id))
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)
    return os.path.sep.join(folders)


class DirectoryName:

    def __init__(self, parent_directory):
        self.parent_directory = parent_directory

    def __call__(self, instance, file_name):
        """return where to upload file"""
        if instance and getattr(instance, 'owner', None):
            user_id = instance.owner.id
        else:
            request = get_request()
            user_id = request.user.id if (request and request.user and request.user.id) else '0'
        user_directory = get_user_directory(self.parent_directory, user_id)
        return os.path.join(user_directory, file_name)


def upload_file(request, parent_directory, user_id=0):
    if not user_id:
        user_id = request.user.id or '0'
    target_dir = get_user_directory(parent_directory, user_id)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    return upload_helper(request, absolute_target_dir)