# -*- coding: utf-8 -*-

import logging
import os.path
import shutil
import re

from django.conf import settings
from django.test import TestCase
from django.test.utils import override_settings
from django.core.management import call_command

import haystack
from rest_framework.test import APITestCase


def get_haystack_test_connections():
    haystack_settings = dict(settings.HAYSTACK_CONNECTIONS)
    haystack_settings['default']['INDEX_NAME'] = 'ulysses_unittest_index'
    return haystack_settings


def get_unit_test_media_root():
    """return unit testing_media root"""
    if not os.path.exists(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)

    if '_unit_tests' not in settings.MEDIA_ROOT:
        unit_test_media_root = os.path.join(settings.MEDIA_ROOT, '_unit_tests')
        if not os.path.exists(unit_test_media_root):
            os.mkdir(unit_test_media_root)
    else:
        unit_test_media_root = settings.MEDIA_ROOT

    settings.PERSONAL_FILES_ROOT = os.path.join(unit_test_media_root, 'composers')

    # Absolute filesystem path to the directory that will hold admin files.
    settings.ADMIN_FILES_ROOT = os.path.join(unit_test_media_root, 'admin')

    # Absolute filesystem path to the directory that will hold candidates files.
    settings.CANDIDATE_FILES_ROOT = os.path.join(unit_test_media_root, 'candidates')

    # Absolute filesystem path to the directory that will hold temporary files.
    settings.TEMPORARY_FILES_ROOT = os.path.join(unit_test_media_root, 'temp')

    # Absolute filesystem path to the directory that will hold draft files.
    settings.DRAFT_FILES_ROOT = os.path.join(unit_test_media_root, 'draft')

    return unit_test_media_root


@override_settings(
    MEDIA_ROOT=get_unit_test_media_root(),
    HAYSTACK_CONNECTIONS=get_haystack_test_connections(),
    HAYSTACK_SIGNAL_PROCESSOR=''
)
class BaseTestCase(TestCase):
    def _clean_files(self):
        if '_unit_tests' in settings.MEDIA_ROOT:
            try:
                shutil.rmtree(settings.MEDIA_ROOT)
            except OSError:
                pass
        else:
            raise Exception("Warning! wrong media root for unit-testing")

    def setUp(self):
        haystack.connections.reload('default')
        logging.disable(logging.CRITICAL)
        self._clean_files()
        get_unit_test_media_root()  # force to recreate _unit_test

    def tearDown(self):
        call_command('clear_index', interactive=False, verbosity=0)
        logging.disable(logging.NOTSET)
        self._clean_files()


@override_settings(
    MEDIA_ROOT=get_unit_test_media_root(),
    HAYSTACK_CONNECTIONS=get_haystack_test_connections(),
    HAYSTACK_SIGNAL_PROCESSOR=''
)
class BaseAPITestCase(APITestCase):
    def _clean_files(self):
        if '_unit_tests' in settings.MEDIA_ROOT:
            try:
                shutil.rmtree(settings.MEDIA_ROOT)
            except OSError:
                pass
        else:
            raise Exception("Warning! wrong media root for unit-testing")

    def setUp(self):
        haystack.connections.reload('default')
        logging.disable(logging.CRITICAL)
        self._clean_files()

    def tearDown(self):
        call_command('clear_index', interactive=False, verbosity=0)
        logging.disable(logging.NOTSET)
        self._clean_files()


def assert_popup_redirects(response, url):
    """assert the colorbox response redirects to the right page"""
    if response.status_code != 200:
        raise Exception("colobox Response didn't redirect as expected: Response code was {0} (expected 200)".format(
            response.status_code)
        )

    script = '<script>$.colorbox.close(); loadUrl("{0}"); $("body").trigger("colorboxRedirect");</script>'
    expected_content = script.format(url)
    content = response.content.decode('utf-8')
    if content != expected_content:
        redirect_url = re.match('<script>.*loadUrl\("(?P<redirect>.*)"\);.*</script>', content)
        if redirect_url:
            raise Exception("Don't redirect to {0} but to {1}".format(url, redirect_url.groups('redirect')))
        else:
            raise Exception("Don't redirect to {0}: {1}".format(url, content[:30]))
