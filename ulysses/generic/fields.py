# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup

from django.utils.translation import ugettext

import floppyforms.__future__ as forms

from .widgets import DateRangeInput, IntRangeInput, SimpleHtmlWidget


class MultipleElementChoiceField(forms.ModelMultipleChoiceField):
    """"""
    has_initial = False

    def __init__(self, *args, **kwargs):
        super(MultipleElementChoiceField, self).__init__(*args, **kwargs)

    def set_initial(self, queryset):
        """Set the initial value"""
        self.initial = queryset
        self.queryset = queryset
        has_initial = True

    def get_element_queryset(self):
        raise NotImplemented

    def _check_values(self, value):
        """
        Given a list of possible PK values, returns a QuerySet of the
        corresponding objects. Raises a ValidationError if a given value is
        invalid (not a valid PK, not in the queryset, etc.)
        """
        key = self.to_field_name or 'pk'
        # deduplicate given values to avoid creating many querysets or
        # requiring the database backend deduplicate efficiently.
        try:
            value = frozenset(value)
        except TypeError:
            # list of lists isn't hashable, for example
            raise forms.ValidationError(
                self.error_messages['list'],
                code='list',
            )
        for pk in value:
            try:
                self.queryset.filter(**{key: pk})
            except (ValueError, TypeError):
                raise forms.ValidationError(
                    self.error_messages['invalid_pk_value'],
                    code='invalid_pk_value',
                    params={'pk': pk},
                )
        queryset = self.get_element_queryset().filter(**{'%s__in' % key: value})

        if not self.has_initial:
            # Force init value
            self.queryset = queryset

        return queryset


class DateRangeField(forms.CharField):

    def __init__(self, *args, **kwargs):
        if kwargs.get('widget', None) is None:
            kwargs['widget'] = DateRangeInput()
        super(DateRangeField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class IntRangeField(forms.CharField):

    def __init__(self, *args, **kwargs):
        if kwargs.get('widget', None) is None:
            kwargs['widget'] = IntRangeInput()
        super(IntRangeField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        return value


class SimpleHtmlField(forms.CharField):

    buttons = "bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,link,|,bullist,numlist,|,undo,redo"
    buttons += ",|,removeformat,|,formatselect"

    mce_attrs = {
        'mode': 'textareas',
        'theme': "advanced",
        'height': "400",
        'width': "100%",
        'theme_advanced_buttons1': buttons,
        'theme_advanced_buttons2': "",
        'theme_advanced_buttons3': "",
        'theme_advanced_blockformats': "p,h3,h4",
        'theme_advanced_toolbar_location': "top",
        'theme_advanced_toolbar_align': "left",
        'theme_advanced_statusbar_location': "bottom",
        'theme_advanced_resizing': True,
        'setup': "setupInitPlainText",
        'paste_preprocess': "pastePreprocessHandleLinebreaks",
    }

    def __init__(self, *args, **kwargs):
        if kwargs.get('widget', None) is None:
            kwargs['widget'] = SimpleHtmlWidget(mce_attrs=self.mce_attrs)
        super(SimpleHtmlField, self).__init__(*args, **kwargs)

    def to_python(self, value):
        allowed_tags = ['p', 'h3', 'h4', 'em', 'strong', 'ul', 'li', 'ol', 'span', 'a']

        if value:
            soup = BeautifulSoup(value, "html.parser")
            for tag in soup.find_all():
                if tag.name and tag.name not in allowed_tags:
                    raise forms.ValidationError(
                        ugettext('Invalid HTML code: Tag {0} is not allowed').format(tag.name)
                    )
        return value
