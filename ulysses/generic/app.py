# -*- coding: utf-8 -*-

from django.apps.config import AppConfig as DangoBaseAppConfig


class BaseAppConfig(DangoBaseAppConfig):
    pass
