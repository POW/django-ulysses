# -*- coding: utf-8 -*-

import collections
import os.path

from django.conf import settings
from django.forms.forms import BoundField
from django.forms.widgets import SelectMultiple
from django.urls import reverse
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _

import floppyforms.__future__ as forms

from ulysses.utils import is_mp3_valid
from .widgets import FileUploadControl


class Fieldset(object):

    def __init__(self, form, name, boundfields, legend='', classes='', description=''):
        self.form = form
        self.boundfields = boundfields
        if legend is None:
            legend = name
        self.legend = legend and mark_safe(legend)
        self.classes = classes
        self.description = mark_safe(description)
        self.name = name

    def __iter__(self):
        for field in self.boundfields:
            yield field


class FormWithFieldsetMixin(object):

    def get_fieldsets(self):
        """return the list of fieldsets"""
        return self.Meta.fieldsets

    @property
    def fieldsets(self):
        for fieldset_name, fieldset_attrs in self.get_fieldsets():

            fields = []
            for field_name in fieldset_attrs.get('fields', None) or []:
                if field_name in self.fields:
                    fields.append(
                        BoundField(self, self.fields[field_name], field_name)
                    )

            yield Fieldset(
                form=self,
                name=fieldset_name,
                boundfields=fields,
                legend=fieldset_attrs.get('legend', ''),
                classes=fieldset_attrs.get('classes', ''),
                description=fieldset_attrs.get('description', ''),
            )


class BootstrapFormMixin(object):

    def _patch_fields(self):

        for field_name in self.fields:
            field = self.fields[field_name]

            if 'class' in field.widget.attrs:
                val = field.widget.attrs['class']
                field.widget.attrs['class'] = val + " form-control"
            else:
                field.widget.attrs['class'] = "form-control"

            if isinstance(field.widget, (SelectMultiple, forms.SelectMultiple )):
                field.widget.attrs['data-placeholder'] = "Select option(s)"


class BaseForm(forms.Form, BootstrapFormMixin):
    """Base form with bootstrap style"""

    def __init__(self, *args, **kwargs):
        super(BaseForm, self).__init__(*args, **kwargs)
        self._patch_fields()


class BaseModelForm(forms.ModelForm, BootstrapFormMixin):
    """Base model form with bootstrap style"""

    def __init__(self, *args, **kwargs):
        super(BaseModelForm, self).__init__(*args, **kwargs)
        self._patch_fields()


class BetterForm(FormWithFieldsetMixin, forms.Form, BootstrapFormMixin):
    """Base class inherit from Bootstrap and form-utils BetterForm"""

    def __init__(self, *args, **kwargs):
        super(BetterForm, self).__init__(*args, **kwargs)
        self._patch_fields()


class FileUploadForm(BaseModelForm):
    """Base model form with file-upload file fields"""

    def __init__(self, *args, **kwargs):
        super(FileUploadForm, self).__init__(*args, **kwargs)

        instance = kwargs.get('instance', None)
        fields_order = []
        remove_fields = []
        media_field_names = self.get_upload_fields()
        if media_field_names:
            # Transform file fields to upload fields
            for field_name in list(self.fields):
                if field_name in media_field_names:

                    # Replace every FileField to a CharField which will be used a uploadjs input
                    media_field_name = self.get_upload_field_name(field_name)

                    # Store Ordering of fields
                    fields_order.append(media_field_name)

                    widget_args = self.get_upload_field_args(field_name)

                    upload_url_name = widget_args['upload_url_name']
                    allowed_extensions = widget_args['extensions']
                    no_help = widget_args.get('no_help', False)
                    widget_class = widget_args.get('widget_class')

                    # Create a CharField for storing the file name
                    # The CharField as the same args than the corresponding FileField
                    label = field_name
                    model_field = self._meta.model._meta.get_field(field_name)
                    required = not model_field.blank
                    help_text = model_field.help_text
                    try:
                        label = model_field.verbose_name
                    except AttributeError:
                        pass
                    # TODO for MP3 check
                    media_field = forms.CharField(
                        label=label,
                        required=required,
                        widget=widget_class(
                            upload_url=reverse(upload_url_name),
                            allowed_extensions=allowed_extensions,
                            no_help=no_help
                        ),
                        max_length=200,
                        help_text=help_text
                    )
                    if 'mp3' in allowed_extensions:
                        _media_field_clean = media_field.clean
                        patched_field_name = field_name

                        def patched_clean(value):
                            if value:
                                if os.path.splitext(value)[1].lower() == '.mp3':
                                    full_value = self.get_relative_file_path(patched_field_name, value)
                                    full_value = os.path.join(settings.MEDIA_ROOT, full_value)
                                    if not is_mp3_valid(full_value):
                                        raise forms.ValidationError(_('It seems that this file is not a valid mp3'))
                            return _media_field_clean(value)

                        media_field.clean = patched_clean

                    # Try to load initial value
                    instance_value = getattr(instance, field_name, None)
                    if instance_value:
                        setattr(media_field.widget, 'initial_image', instance_value)
                        media_field.initial = os.path.split(instance_value.name)[-1]

                    self.fields[media_field_name] = media_field

                    # Add the original FileField to the remove list
                    remove_fields.append(field_name)
                else:
                    # Store Ordering of fields
                    fields_order.append(field_name)

        # Remove the original FileFields
        for field_name in remove_fields:
            self.fields.pop(field_name)

        if fields_order:
            # reorder the fields to keep the original fields list
            fields = collections.OrderedDict()
            for field_name in fields_order:
                fields[field_name] = self.fields[field_name]
            self.fields = fields

    def get_upload_fields(self):
        """returns the list of fields to uploadify"""
        return getattr(self.Meta, 'upload_fields', None)

    def get_upload_field_name(self, field_name):
        """returns the name of the CharField"""
        return field_name + '_media'

    def get_relative_file_path(self, field_name, file_name):
        """returns the relative file path for the new file"""
        model_field = self._meta.model._meta.get_field(field_name)
        upload_to = model_field.upload_to
        if callable(upload_to):
            upload_to = upload_to(self.instance, file_name)
        else:
            upload_to = os.path.join(upload_to, file_name)
        return upload_to

    def copy_file(self, instance, field_name):
        """copy the filename as FileField"""
        upload_field_name = self.get_upload_field_name(field_name)
        file_name = self.cleaned_data.get(upload_field_name)
        if file_name:
            file_relative_path = self.get_relative_file_path(field_name, file_name)
            setattr(instance, field_name, file_relative_path)
        else:
            setattr(instance, field_name, '')

    def get_upload_field_args(self, field_name):
        """list of patch fields for initialization of js code"""
        if field_name in self.get_upload_fields():
            if hasattr(self.Meta, 'upload_fields_args') and field_name in self.Meta.upload_fields_args:
                attrs = self.Meta.upload_fields_args[field_name]
            else:
                attrs = {}

            return {
                'name': field_name,
                'media_field_name': self.get_upload_field_name(field_name),
                'extensions': attrs.get('extensions', ''),
                'label': attrs.get('label', field_name),
                'upload_url_name': attrs['upload_url_name'],
                'no_help': attrs.get('no_help', False),
                'widget_class': attrs.get('widget_class', None) or FileUploadControl
            }

    def save(self, *args, **kwargs):
        """Save file and update file"""
        instance = super(FileUploadForm, self).save(*args, **kwargs)
        for field_name in self.get_upload_fields():
            self.copy_file(instance, field_name)
        instance.save()
        return instance


class ConfirmForm(BaseForm):
    confirm = forms.CharField(required=False, initial='confirm', widget=forms.HiddenInput())

    def clean_confirm(self):
        text = self.cleaned_data['confirm']
        if text == 'confirm':
            return True
        return False


class ItemsSearchForm(forms.Form):
    """search for a profile"""

    # , widget=forms.HiddenInput()) -> cause troubles
    filter = forms.CharField(required=False, widget=forms.TextInput(attrs={'style': 'display: none;'}))

    def _post_init(self):

        for field in self.fields:
            try:
                choices = self.fields[field].choices
            except AttributeError:
                choices = None

            label = self.fields[field].label
            if label:
                label = label + '   \u25BE'
            if choices:
                choices = [list(elt) for elt in self.fields[field].choices]
                choices[0][1] = label
                self.fields[field].choices = choices
            else:
                if label:
                    self.fields[field].widget.attrs['value'] = label
                    self.fields[field].widget.attrs['placeholder'] = label

    def get_label(self, item, prefix=''):
        for key, value in list(item.items()):
            if prefix:
                key = prefix + key
            try:
                field = self.fields[key]
            except KeyError:
                continue
            try:
                choices = field.choices
            except AttributeError:
                choices = None
            if choices:
                for choice in choices:
                    if type(choice[1]) is list:
                        for sub_elt in choice[1]:
                            if sub_elt[0] == value:
                                return sub_elt[1]
                    else:
                        if choice[0] == value:
                            return choice[1]
            return self._do_get_label(field.label, key.replace(prefix, ''), item)
        return ""

    def _do_get_label(self, field_label, key, item):
        return field_label

    def to_raw_value(self, item):
        for key, value in list(item.items()):
            return value
        return ""

    def to_value(self, item):
        for key, value in list(item.items()):
            return value
        return ""
