# -*- coding: utf-8 -*-

import re
from math import radians, cos, sin, asin, sqrt

from django.contrib.sites.models import Site
from django.urls import resolve
from django.utils.safestring import mark_safe

from ulysses.utils import dehtml, get_youtube_code, get_soundcloud_code
from ulysses.middleware import get_request

from ulysses.works.models import Work


def safe_html(text, allow_attachments=False, player=False, allow_links_preview=False):
    """remove html tags except accept links and linebreaks"""
    safe_text = dehtml(text, allow_spaces=True, allow_html_chars=True)
    site = Site.objects.get_current()

    # create link if urls in message
    def replace_url(gr):
        url = gr.group(0)
        prefix_http = 'http://{0}'.format(site.domain)
        prefix_https = 'https://{0}'.format(site.domain)
        found, href_url = False, ''
        for prefix in [prefix_http, prefix_https]:
            if url.find(prefix) == 0:
                found = True
                href_url = url.replace(prefix, '')  # empty href_url may be a valid link
                break
        if found:
            args = ''
            additional_html = ''
            try:
                resolved = resolve(href_url)
                if resolved:
                    if resolved.url_name == 'accept_membership' and resolved.namespaces == ['profiles']:
                        args = 'class="colorbox-form"'

                    elif player and resolved.url_name == 'view_work' and resolved.namespaces == ['works']:
                        try:
                            work = Work.objects.get(id=resolved.kwargs.get('id', 0))
                            if work.is_public() and work.audio:
                                html = '<br />{0}<br /><audio controls="controls">'.format(work.title)
                                html += '<source src="{0}" />'.format(work.audio_url)
                                html += '</audio>'
                                additional_html = html
                                args = 'target="_blank"'
                        except Work.DoesNotExist:
                            pass
            except Exception:
                pass
            link = '<a href="{0}" {1}>{2}</a>'.format(href_url, args, url)
            return link + additional_html
        else:
            if allow_links_preview:
                youtube_code = get_youtube_code(url)
                if youtube_code:
                    args = 'allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen'
                    size = 'width="100%" height="200"'
                    link = '<a href="{0}" target="_blank">{0}</a>'.format(url)
                    embed_url = 'https://www.youtube.com/embed/{0}'.format(youtube_code)
                    return '{0}<br /><iframe src="{1}" frameborder="0" {2} {3}></iframe>'.format(
                        link, embed_url, size, args
                    )
                else:
                    soundcloud_code = get_soundcloud_code(url)
                    if soundcloud_code:
                        link = '<a href="{0}" target="_blank">{0}</a>'.format(url)
                        args = 'scrolling="no" frameborder="no" allow="autoplay"'
                        size = 'width="100%" height="200"'
                        soundcloud_url = 'https%3A//api.soundcloud.com/tracks/{0}'.format(soundcloud_code)
                        url_args = '&color=%23ff5500&auto_play=false&hide_related=false&show_comments=true&' + \
                                   'show_user=true&show_reposts=false&show_teaser=true&visual=true'
                        embed_url = 'https://w.soundcloud.com/player/?url={0}{1}'.format(soundcloud_url, url_args)
                        return '{0}<br /><iframe src="{1}" {2} {3}></iframe>'.format(
                            link, embed_url, size, args
                        )
            return '<a href="{0}" target="_blank">{0}</a>'.format(url)

    # create link if urls in message
    def replace_attachments(gr):
        request = get_request()
        link = gr.group('link')
        try:
            url, name = link.split(':')[:2]
        except IndexError:
            url = name = ''
        if url and name:
            prefix = 'http{0}://{1}'.format('s' if request.is_secure() else '', site.domain)
            return '<a href="{0}{1}" target="_blank">{2}</a>'.format(prefix, url, name)
        else:
            return link

    safe_text = re.sub(r"(https?://[\S]+)", replace_url, safe_text, 0)
    if allow_attachments:
        safe_text = re.sub(r"attachment://(?P<link>[\S]+)", replace_attachments, safe_text, 0)

    # replace \n by html linebreaks
    safe_text = safe_text.replace('\n', '<br />')

    return mark_safe(safe_text)


def url_to_link(url):
    return safe_html(url, allow_attachments=False)


def haversine(lon1, lat1, lon2, lat2):
    """
    Calculate the great circle distance between two points
    on the earth (specified in decimal degrees)
    See: https://stackoverflow.com/a/42687012/117092
    """
    # convert decimal degrees to radians
    lon1, lat1, lon2, lat2 = map(radians, [lon1, lat1, lon2, lat2])

    # haversine formula
    dlon = lon2 - lon1
    dlat = lat2 - lat1
    angle = sin(dlat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dlon / 2) ** 2
    center = 2 * asin(sqrt(angle))
    radius = 6371  # Radius of earth in kilometers. Use 3956 for miles
    return center * radius
