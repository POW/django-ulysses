# -*- coding: utf-8 -*-

from django.contrib.auth.models import User

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination

from ulysses.middleware import get_request

from ulysses.profiles.serializers import ProfileSerializer
from ulysses.works.models import Work
from ulysses.works.serializers import WorkSerializer

from ..models import Following


class ProfilePagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class FavoriteMembersView(ListAPIView):
    """View received messages"""
    queryset = User.objects.all()
    serializer_class = ProfileSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = ProfilePagination

    def get_queryset(self):
        """returns messages of current user"""
        queryset = super(FavoriteMembersView, self).get_queryset()
        request = get_request()
        # The users followed by current user
        my_followings = Following.objects.filter(follower=request.user)
        queryset = queryset.filter(followed_set__in=my_followings).order_by('-followed_set__follow_datetime')
        return queryset


class WorkPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class FavoriteWorksView(ListAPIView):
    """View received messages"""
    queryset = Work.objects.all()
    serializer_class = WorkSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = WorkPagination

    def get_queryset(self):
        """returns messages of current user"""
        queryset = super(FavoriteWorksView, self).get_queryset()
        request = get_request()
        # The works recommended by current user
        queryset = queryset.filter(bookmarks__member_user=request.user).order_by(
            '-bookmarks__datetime'
        )
        return queryset
