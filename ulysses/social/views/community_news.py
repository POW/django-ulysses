# -*- coding: utf-8 -*-

from datetime import datetime, date

from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _

from ulysses.events.utils import get_upcoming_events_queryset
from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView, ListBasePageView

from ..forms import ShareElementForm, EditShareElementForm
from ..models import FeedItem
from ..shortcuts import get_feed_content, get_content_object
from ..utils import get_all_feeds, filter_following


class CommunityNewsView(ListBasePageView):
    template_name = 'social/community_news.html'

    ITEMS_COUNTER_STEP = 16
    ROWS_LG_LIMIT = 0

    EVERYONE = 'everyone'
    FOLLOWING = 'following'
    DEFAULT_ORDERING = EVERYONE

    ORDER_BY_CHOICES = [
        (EVERYONE, _('Everyone\'s activity')),
        (FOLLOWING, _('Everyone I follow')),
    ]

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(CommunityNewsView, self).dispatch(request, *args, **kwargs)

    def get_ordering(self, order_by):
        """returns the ordering fields"""
        ordering = ['-update_datetime']
        return ordering

    def get_queryset(self):
        return get_all_feeds()

    def get_filter_form_class(self):
        return None

    def order_items(self, queryset, order_by, ordering):
        if order_by == self.FOLLOWING:
            queryset = filter_following(queryset, self.request.user)
        else:
            queryset = get_all_feeds(no_works=True)
        return super(CommunityNewsView, self).order_items(queryset, order_by, ordering)

    def get_context_data(self, **kwargs):
        context = super(CommunityNewsView, self).get_context_data(**kwargs)

        items = context['items']

        # Mark displayed items as read if not read
        for feed_item in items:

            if feed_item.content_object is None:
                feed_item.is_deleted = True
                feed_item.save()

            if not feed_item.is_read:
                feed_item.is_read = True
                feed_item.save()

        context['items'] = [elt for elt in items if not elt.is_deleted]

        events = get_upcoming_events_queryset(take_time_into_account=True)[:4]
        context['events'] = events
        no_upcoming_events = False
        if events.count():
            if events[0].end_date:
                no_upcoming_events = events[0].end_date.date() < date.today()
            elif events[0].start_date:
                no_upcoming_events = events[0].start_date.date() < date.today()
            else:
                no_upcoming_events = True
        context['no_upcoming_events'] = no_upcoming_events

        return context


class ShareElementView(LoginRequiredPopupFormView):
    template_name = 'social/popup_share_element.html'
    form_class = ShareElementForm
    content_object = None

    def get_content_type(self):
        content_type_id = self.kwargs['content_type']
        return ContentType.objects.get(id=content_type_id)

    def get_content_object(self):
        if self.content_object is None:
            self.content_object = get_content_object(self.kwargs['content_type'], self.kwargs['object_id'])
        return self.content_object

    def get_form_kwargs(self):
        form_kwargs = super(ShareElementView, self).get_form_kwargs()

        content_object = self.get_content_object()
        tag = getattr(content_object, 'post_type', '')

        initial_data = {
            'content_type': self.kwargs['content_type'],
            'object_id': self.kwargs['object_id'],
            'tag': tag,
        }
        parent_id = self.kwargs.get('feed_id')
        if parent_id:
            initial_data['parent'] = parent_id

        form_kwargs.update({
            'initial': initial_data,
            'instance': FeedItem(owner=self.request.user, feed_datetime=datetime.now()),
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(ShareElementView, self).get_context_data(**kwargs)
        context.update(self.kwargs)

        try:
            content_type_id = int(self.kwargs['content_type'])
        except ValueError:
            content_type_id = 0
        is_repost = (content_type_id == ContentType.objects.get_for_model(FeedItem).id)
        if is_repost:
            #  Repost : show original content
            feed_item = self.get_content_object()
            parent = feed_item.parent or feed_item
            context['content'] = get_feed_content(parent.content_type.id, parent.object_id)
        else:
            context['content'] = get_feed_content(self.kwargs['content_type'], self.kwargs['object_id'])

        return context

    def form_valid(self, form):
        feed = form.save()
        if feed.content_type.model == "memberpost":
            feed.post_text = feed.content_object.text
            feed.save()

        if feed.content_type == ContentType.objects.get_for_model(FeedItem) and feed.parent:
            # Repost
            feed.parent.update_datetime = datetime.now()
            feed.parent.save()
        return HttpResponseRedirect(reverse('social:community_news'))


class EditShareElementView(LoginRequiredPopupFormView):
    template_name = 'social/popup_edit_share.html'
    form_class = EditShareElementForm
    content_object = None
    instance = None

    def get_instance(self):
        if not self.instance:
            self.instance = get_object_or_404(FeedItem, id=self.kwargs['feed_id'])
        return self.instance

    def get_form_kwargs(self):
        form_kwargs = super(EditShareElementView, self).get_form_kwargs()

        instance = self.get_instance()
        if instance.owner != self.request.user:
            raise PermissionDenied

        form_kwargs.update({
            'instance': instance,
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(EditShareElementView, self).get_context_data(**kwargs)
        instance = self.get_instance()
        context['instance'] = instance

        if instance.content_type == ContentType.objects.get_for_model(FeedItem) and instance.parent:
            context['content'] = get_feed_content(instance.parent.content_type.id, instance.parent.object_id)
        else:
            context['content'] = get_feed_content(instance.content_type.id, instance.object_id)
        return context

    def form_valid(self, form):
        feed = form.save()
        if feed.content_type == ContentType.objects.get_for_model(FeedItem) and feed.parent:
            feed = feed.parent
            feed.parent.feed_datetime = datetime.now()
            feed.parent.save()
        return HttpResponseRedirect(reverse('social:community_news'))


class DeleteShareElementView(LoginRequiredPopupFormView):
    template_name = 'social/popup_delete_share.html'
    form_class = ConfirmForm
    content_object = None
    instance = None

    def get_instance(self):
        if not self.instance:
            self.instance = get_object_or_404(FeedItem, id=self.kwargs['feed_id'])
        return self.instance

    def get_form_kwargs(self):
        form_kwargs = super(DeleteShareElementView, self).get_form_kwargs()

        instance = self.get_instance()
        if instance.owner != self.request.user:
            raise PermissionDenied

        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeleteShareElementView, self).get_context_data(**kwargs)
        instance = self.get_instance()
        context['instance'] = instance
        context['content'] = get_feed_content(instance.content_type.id, instance.object_id)
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            # delete member post
            self.instance.is_deleted = True
            self.instance.save()
        return HttpResponseRedirect(reverse('social:community_news'))
