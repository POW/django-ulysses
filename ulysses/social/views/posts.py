# -*- coding: utf-8 -*-

from datetime import datetime
import json
import os.path

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.contenttypes.models import ContentType
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView, ProfileTemplateView
from ulysses.utils import upload_helper

from ..forms import MemberPostForm
from ..models import MemberPost, get_members_post_directory, FeedItem


@csrf_exempt
@login_required
def upload_post_image(request, user_id=0):
    if user_id == 0:
        user_id = request.user.id
    target_dir = get_members_post_directory(user_id)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    return upload_helper(request, absolute_target_dir)


class MemberPostFormView(LoginRequiredPopupFormView):
    template_name = 'social/popup_member_post.html'
    form_class = MemberPostForm
    instance = None

    def get_instance(self):
        if not self.instance:
            post_id = self.kwargs.get('post_id', None)
            if post_id:
                self.instance = get_object_or_404(MemberPost, id=post_id)
        return self.instance

    def get_form_kwargs(self):
        form_kwargs = super(MemberPostFormView, self).get_form_kwargs()

        instance = self.get_instance()
        if instance:
            if instance.owner != self.request.user:
                raise PermissionDenied
        else:
            instance = MemberPost(owner=self.request.user, post_datetime=datetime.now())
        form_kwargs['instance'] = instance
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(MemberPostFormView, self).get_context_data(**kwargs)
        instance = self.get_instance()
        if instance:
            context['instance'] = instance
        return context

    def form_valid(self, form):
        member_post = form.save()

        is_new = self.instance is None
        if is_new:
            message_text = _('Your post has been successfully added to the Community Feed.')
        else:
            message_text = _('Your post has been successfully updated.')
        messages.success(self.request, message_text)

        content_type = ContentType.objects.get_for_model(member_post)

        if is_new:
            # Create a FeedItem to see that posts in the feeds
            lookup = dict(
                owner=member_post.owner,
                content_type=content_type,
                object_id=member_post.id,
                is_original_post=True
            )

            try:
                feed = FeedItem.objects.get(**lookup)
            except FeedItem.DoesNotExist:
                feed = FeedItem(**lookup)
                feed.feed_datetime = datetime.now()

            feed.tag = member_post.post_type
            feed.post_text = member_post.text
            feed.save()

            share_feed_queryset = FeedItem.objects.filter(
                content_type=content_type, object_id=member_post.id
            ).exclude(id=feed.id)

            # Update feed tags
            for shared_feed in share_feed_queryset:
                shared_feed.tag = member_post.post_type
                shared_feed.save()

        # notify_updated_post(member_post, share_feed_queryset)

        return HttpResponseRedirect(reverse('social:community_news'))


class MemberPostPostedView(ProfileTemplateView):
    template_name = 'social/member_post_posted.html'

    def get_context_data(self, **kwargs):
        context = super(MemberPostPostedView, self).get_context_data(**kwargs)
        member_post = get_object_or_404(MemberPost, id=self.kwargs['id'], owner=self.request.user)
        context['member_post'] = member_post
        return context


class DeletePostView(LoginRequiredPopupFormView):
    template_name = 'social/popup_delete_post.html'
    form_class = ConfirmForm
    member_post = None

    def get_form_kwargs(self):
        form_kwargs = super(DeletePostView, self).get_form_kwargs()
        # Delete an existing document
        self.member_post = get_object_or_404(MemberPost, id=self.kwargs['post_id'])
        if self.member_post.owner != self.request.user:
            # Check user permission :
            # Owners will delete the doc
            raise PermissionDenied
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeletePostView, self).get_context_data(**kwargs)
        context['instance'] = self.member_post
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            # delete member post
            self.member_post.is_deleted = True
            self.member_post.save()

            # delete main feed
            main_feed = self.member_post.main_feed()
            main_feed.is_deleted = True
            main_feed.save()

            message_text = _('Your post has been successfully deleted from the Community Feed.')
            messages.success(self.request, message_text)

        return HttpResponseRedirect(reverse("social:community_news"))


@login_required
def mark_notifications_as_read(request, id=0):
    queryset = FeedItem.objects.filter(
        is_read=False, tag=FeedItem.NOTIFICATION, id__lte=id, owner=request.user
    )
    queryset.update(is_read=True)
    return HttpResponse(json.dumps({'ok': 1}), content_type="application/json")
