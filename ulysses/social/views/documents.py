# -*- coding: utf-8 -*-

import os.path

from django.conf import settings
from django.core.exceptions import PermissionDenied
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from rest_framework.generics import ListAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.pagination import PageNumberPagination

from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView, PrivateDownloadView
from ulysses.middleware import get_request
from ulysses.utils import upload_helper

from ..forms import DocumentForm
from ..models import Document, DocumentAccess, get_user_documents_directory
from ..serializers import DocumentSerializer
from ..utils import get_user_documents


@csrf_exempt
def upload_document_file(request, user_id=0):
    if user_id == 0:
        user_id = request.user.id
    target_dir = get_user_documents_directory(user_id)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    return upload_helper(request, absolute_target_dir)


class DocumentFormView(LoginRequiredPopupFormView):
    template_name = 'social/popup_share_document.html'
    form_class = DocumentForm

    def get_form_kwargs(self):
        form_kwargs = super(DocumentFormView, self).get_form_kwargs()

        document_id = self.kwargs.get('document_id', None)
        if document_id:
            # Edit an existing document
            document = get_object_or_404(Document, id=document_id)
            if document.owner != self.request.user:
                raise PermissionDenied
        else:
            # Add a new document
            document = Document(owner=self.request.user)

        form_kwargs.update({
            'instance': document,
            'initial': {},
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DocumentFormView, self).get_context_data(**kwargs)
        context['document_id'] = self.kwargs.get('document_id', None)
        return context

    def form_valid(self, form):
        form.save()
        return HttpResponseRedirect(reverse("profiles:personal_space") + '?tab=documents')


class DocumentPagination(PageNumberPagination):
    page_size = 10
    page_size_query_param = 'page_size'
    max_page_size = 100


class SharedDocumentListView(ListAPIView):
    """View other documents"""
    serializer_class = DocumentSerializer
    permission_classes = [IsAuthenticated]
    pagination_class = DocumentPagination

    def get_queryset(self):
        """returns messages of current user"""
        request = get_request()

        if request.user.is_anonymous:
            queryset = Document.objects.none()
        else:
            # Get user documents
            queryset = get_user_documents(request.user)

        return queryset


class UserDocumentListView(SharedDocumentListView):
    """View documents shared bu current user"""

    def get_queryset(self):
        """returns messages of current user"""
        request = get_request()
        queryset = Document.objects.filter(owner=request.user)
        return queryset

    def get(self, request, *args, **kwargs):
        """returns list of messages. Mark them as read"""
        response = super(UserDocumentListView, self).get(request, *args, **kwargs)
        queryset_ids = [elt.id for elt in self.get_queryset()]
        DocumentAccess.objects.filter(document__in=queryset_ids, user=request.user).update(is_read=True)
        return response


class DeleteDocumentFormView(LoginRequiredPopupFormView):
    template_name = 'social/popup_delete_document.html'
    form_class = ConfirmForm
    document = None

    def get_form_kwargs(self):
        form_kwargs = super(DeleteDocumentFormView, self).get_form_kwargs()
        # Delete an existing document
        self.document = get_object_or_404(Document, id=self.kwargs['document_id'])
        if self.document.owner != self.request.user and not self.document.is_allowed(self.request.user):
            # Check user permission :
            # Owners will delete the doc
            # user whith acess will remove their own access
            raise PermissionDenied
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeleteDocumentFormView, self).get_context_data(**kwargs)
        context['document'] = self.document
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            if self.document.owner == self.request.user:
                self.document.delete()
            else:
                document_access = DocumentAccess.objects.get_or_create(
                    document=self.document, user=self.request.user
                )[0]
                document_access.is_read = True
                document_access.is_deleted = True
                document_access.save()
        return HttpResponseRedirect(reverse("profiles:personal_space") + '?tab=documents')


class DocumentDownloadView(PrivateDownloadView):
    """
    private download for documents.
    Media files can not be accessed from file url and must pass through this verification
    """

    def get_file(self, request, **kwargs):
        """
        get document from id and check user can acess it
        returns the file of the document
        """
        doc = get_object_or_404(Document, id=kwargs['id'])

        # Check permissions
        if not doc.is_allowed(request.user):
            raise PermissionDenied

        return doc.file
