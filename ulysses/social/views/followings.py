# -*- coding: utf-8 -*-

import json

from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect, HttpResponse
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext_lazy

from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination

from ulysses.profiles.serializers import ProfileSerializer
from ulysses.profiles.utils import get_profile

from ..forms import FollowMemberForm
from ..models import Following
from ..notifications import notify_following
from ..views import ActionOnMemberBaseFormView, MemberPopupBaseView


class FollowMemberView(ActionOnMemberBaseFormView):
    template_name = 'social/popup_follow_member.html'
    form_class = FollowMemberForm

    def get_context_data(self, **kwargs):
        context = super(FollowMemberView, self).get_context_data(**kwargs)
        #
        profile = context['profile']
        context['follow'] = Following.objects.filter(follower=self.user, member_user=profile.user).exists()
        return context

    def form_valid(self, form):
        member_user = form.cleaned_data['member_user']

        lookup = dict(member_user=member_user, follower=self.user)

        if Following.objects.filter(**lookup).exists():
            Following.objects.filter(**lookup).delete()
        else:
            following = Following.objects.create(**lookup)
            notify_following(following)

        redirect_url = self.request.META.get('HTTP_REFERER')
        if not redirect_url:
            redirect_url = get_profile(member_user).get_absolute_url()

        return HttpResponseRedirect(redirect_url)


class FollowersPagination(PageNumberPagination):
    page_size = 9
    page_size_query_param = 'page_size'
    max_page_size = 100


class FollowersView(ListAPIView):
    """REst API View : The users who are following the current user"""
    queryset = User.objects.all()
    serializer_class = ProfileSerializer
    pagination_class = FollowersPagination

    def get_queryset(self):
        """returns the users who are following the current user"""
        queryset = super(FollowersView, self).get_queryset()
        user_followers = Following.objects.filter(member_user__id=self.kwargs.get('user_id'))
        queryset = queryset.filter(follower_set__in=user_followers).order_by('-follower_set__follow_datetime')
        return queryset


class FollowersListView(MemberPopupBaseView):
    template_name = 'social/popup_followers_list.html'
    title = ugettext_lazy('Followers')

    def get_context_data(self, **kwargs):
        context = super(FollowersListView, self).get_context_data(**kwargs)
        profile = context['profile']
        print(profile.user)
        context['followings'] = Following.objects.filter(member_user=profile.user)
        return context


class FollowingListView(MemberPopupBaseView):
    template_name = 'social/popup_following_list.html'
    title = ugettext_lazy('Following')

    def get_context_data(self, **kwargs):
        context = super(FollowingListView, self).get_context_data(**kwargs)
        profile = context['profile']
        context['followings'] = Following.objects.filter(follower=profile.user)
        return context


@login_required
def follow_member_ajax(request, user_id):
    follower = request.user
    member_user = get_object_or_404(User, id=user_id)
    lookup = dict(member_user=member_user, follower=follower)
    if Following.objects.filter(**lookup).exists():
        Following.objects.filter(**lookup).delete()
        following = None
    else:
        following = Following.objects.create(**lookup)
        notify_following(following)
    return HttpResponse(
        json.dumps({'following': following is not None}),
        content_type="application/json"
    )