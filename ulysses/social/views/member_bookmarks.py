# -*- coding: utf-8 -*-

from django.http import HttpResponseRedirect

from ulysses.profiles.utils import get_profile

from ..forms import BookmarkMemberForm
from ..models import MemberBookmark
from ..views import ActionOnMemberBaseFormView


class BookmarkMemberView(ActionOnMemberBaseFormView):
    template_name = 'social/popup_bookmark_member.html'
    form_class = BookmarkMemberForm

    def get_context_data(self, **kwargs):
        context = super(BookmarkMemberView, self).get_context_data(**kwargs)
        profile = context['profile']
        context['in_bookmarks'] = MemberBookmark.objects.filter(
            member_user=profile.user, bookmarked_by=self.user
        ).exists()
        return context

    def form_valid(self, form):
        member_user = form.cleaned_data['member_user']
        lookup = dict(member_user=member_user, bookmarked_by=self.user)
        if MemberBookmark.objects.filter(**lookup).exists():
            MemberBookmark.objects.filter(**lookup).delete()
        else:
            MemberBookmark.objects.create(**lookup)
        redirect_url = self.request.META.get('HTTP_REFERER')
        if not redirect_url:
            redirect_url = get_profile(member_user).get_absolute_url()
        return HttpResponseRedirect(redirect_url)
