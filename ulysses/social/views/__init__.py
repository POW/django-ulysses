# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.http import Http404
from django.shortcuts import get_object_or_404

from ulysses.profiles.utils import get_profile

from ulysses.generic.views import LoginRequiredPopupFormView, LoginRequiredPopupView


def _get_profile(user_id):
    user = get_object_or_404(User, id=user_id)
    profile = get_profile(user)
    if not profile:
        raise Http404
    return profile


class ActionOnMemberBaseFormView(LoginRequiredPopupFormView):

    def get_form_kwargs(self):
        form_kwargs = super(ActionOnMemberBaseFormView, self).get_form_kwargs()
        form_kwargs.update({
            'done_by': self.user,
            'initial': {'member_user': self.kwargs['id'], },
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(ActionOnMemberBaseFormView, self).get_context_data(**kwargs)
        context['profile'] = _get_profile(self.kwargs['id'])
        return context


class MemberPopupBaseView(LoginRequiredPopupView):

    def get_context_data(self, **kwargs):
        context = super(MemberPopupBaseView, self).get_context_data(**kwargs)
        context['profile'] = _get_profile(self.kwargs['id'])
        return context
