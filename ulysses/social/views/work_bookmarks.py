# -*- coding: utf-8 -*-

from django.http import Http404, HttpResponseRedirect
from django.shortcuts import get_object_or_404

from ulysses.generic.views import LoginRequiredPopupFormView

from ulysses.profiles.utils import get_profile
from ulysses.works.models import Work

from ..forms import WorkBookmarkForm
from ..models import WorkBookmark


class BookmarkWorkView(LoginRequiredPopupFormView):
    template_name = 'social/popup_bookmark_work.html'
    form_class = WorkBookmarkForm

    def get_form_kwargs(self):
        form_kwargs = super(BookmarkWorkView, self).get_form_kwargs()
        form_kwargs.update({
            'initial': {'work': self.kwargs['work_id'], },
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(BookmarkWorkView, self).get_context_data(**kwargs)
        profile = get_profile(self.request.user)
        if not profile:
            raise Http404
        context['profile'] = profile
        work = get_object_or_404(Work, id=self.kwargs['work_id'])
        context['work'] = work
        context['in_bookmarks'] = WorkBookmark.objects.filter(work=work, member_user=profile.user).exists()
        return context

    def form_valid(self, form):
        profile = get_profile(self.request.user)
        work = get_object_or_404(Work, id=self.kwargs['work_id'])
        lookup = dict(member_user=profile.user, work=work)
        if WorkBookmark.objects.filter(**lookup).exists():
            WorkBookmark.objects.filter(**lookup).delete()
        else:
            WorkBookmark.objects.create(**lookup)

        redirect_url = self.request.META.get('HTTP_REFERER')
        if not redirect_url:
            redirect_url = work.get_absolute_url()

        return HttpResponseRedirect(redirect_url)
