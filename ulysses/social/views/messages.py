# -*- coding: utf-8 -*-

from datetime import datetime
import os.path

from django.db import transaction
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.db.models import Q
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt

from rest_framework.views import Response
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.permissions import IsAuthenticated

from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView, ProfileTemplateView, PrivateDownloadView
from ulysses.middleware import get_request
from ulysses.utils import full_path_url

from ulysses.profiles.utils import get_profile
from ulysses.utils import handle_upload
from ulysses.works.models import Work
from ulysses.web.models import FocusOn

from ..forms import ComposeMessageForm, MessageSubjectForm, AddParticipantsForm
from ..models import (
    Message, MessageRecipient, MessageReply, get_members_attachments_directory, attachments_directory
)
from ..serializers import MessageReplySerializer, PostMessageReplySerializer
from ..utils import get_message, get_discussions


class ComposeMessageView(LoginRequiredPopupFormView):
    template_name = 'social/popup_compose_message.html'
    form_class = ComposeMessageForm
    compose_message = False
    write_to = False
    reply_to = False

    def get_profile(self):
        user_id = self.kwargs.get('id')
        if user_id:
            user = get_object_or_404(User, id=user_id)
            profile = get_profile(user)
            if not profile:
                raise Http404
            return profile

    def get_work(self):
        work_id = self.kwargs.get('work_id')
        if work_id:
            return get_object_or_404(Work, id=work_id)

    def get_focus_on(self):
        focus_on_id = self.kwargs.get('focus_on_id')
        if focus_on_id:
            return get_object_or_404(FocusOn, id=focus_on_id)

    def get_message(self, message_id):
        try:
            return Message.objects.get(id=message_id)
        except Message.DoesNotExist:
            pass

    def get_form_kwargs(self):
        form_kwargs = super(ComposeMessageView, self).get_form_kwargs()
        message_id = self.kwargs.get('message_id')

        recipients = None

        if message_id:
            # Reply to  a message?
            message = self.get_message(message_id)
            if message:
                recipient_ids = [
                    recipient.id for recipient in message.recipients.exclude(id=self.request.user.id)
                ]
                if message.sender != self.request.user:
                    recipient_ids = [message.sender.id] + recipient_ids
                recipients = User.objects.filter(id__in=recipient_ids)
        else:
            # New message
            profile = self.get_profile()
            if profile:
                recipients = User.objects.filter(id=profile.user.id)

        work = self.get_work()
        if work:
            initial_subject = work.title
        else:
            focus_on = self.get_focus_on()
            if focus_on:
                initial_subject = focus_on.title
            else:
                initial_subject = self.request.GET.get('subject', '')

        form_kwargs.update({
            'initial': {
                'subject': initial_subject,
            },
            'recipients': recipients,
        })
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(ComposeMessageView, self).get_context_data(**kwargs)
        profile = self.get_profile()
        context['profile'] = profile
        message_id = self.kwargs.get('message_id')

        if self.write_to:
            context['share_elt_url'] = reverse('social:write_to', args=[profile.user.id])
        elif self.reply_to and message_id:
            context['share_elt_url'] = reverse('social:reply_to', args=[message_id])
        else:
            share_urls = [
                (self.get_work(), 'social:share_work'),
                (self.get_focus_on(), 'social:share_focus_on'),
            ]
            for elt, view_name in share_urls:
                if elt:
                    context['elt'] = elt
                    context['share_elt_url'] = reverse(view_name, args=[elt.id])
                    break
        return context

    def form_valid(self, form):
        existing_message = None
        # Check if a conversation already exists with this member
        sender = self.request.user
        recipients = form.cleaned_data['recipients']
        if not self.compose_message and not self.reply_to and len(recipients) == 1:
            recipient = recipients[0]
            # look for an existing message with this recipient
            sender_messages = Message.objects.filter(messagerecipient__user=sender)
            existing_messages = sender_messages.filter(
                messagerecipient__user=recipient
            ).order_by('-updated_datetime')

            for current_message in existing_messages:
                other_recipients = current_message.recipients.exclude(messagerecipient__user__in=[sender, recipient])
                if other_recipients.count() == 0:
                    # We just manage the conversation between only 2 users
                    # Not the group conversations

                    # ignore deleted messages
                    if MessageRecipient.objects.filter(message=current_message, is_deleted=True).count() == 0:
                        existing_message = current_message
                        break

        elif self.reply_to:
            existing_message = self.get_message(self.kwargs.get('message_id'))

        if existing_message:
            # If there is already an existing message: no need to create a new one
            message = existing_message
            if not message.updated_datetime:
                message.updated_datetime = datetime.now()
                message.save()
        else:
            message = form.save(commit=False)
            message.sender = self.request.user
            message.sent_datetime = datetime.now()
            message.updated_datetime = datetime.now()
            message.save()
            MessageRecipient.objects.create(message=message, user=message.sender)
            for recipient in form.cleaned_data['recipients']:
                MessageRecipient.objects.create(message=message, user=recipient)

        for elt in [self.get_work(), self.get_focus_on()]:
            if elt:
                message.updated_datetime = datetime.now()
                message.save()
                MessageReply.objects.create(
                    message=message,
                    sender=self.request.user,
                    sent_datetime=datetime.now(),
                    body=full_path_url(elt.get_absolute_url())
                )
                break

        return HttpResponseRedirect(reverse('social:messages_init', args=[message.id]))


class MessageBoxRefreshView(ProfileTemplateView):
    template_name = 'social/_discussions.html'

    def get_discussions(self):
        return get_discussions(self.request.user)

    def get_context_data(self, **kwargs):
        context = super(MessageBoxRefreshView, self).get_context_data(**kwargs)
        discussions = self.get_discussions()
        context['discussions'] = discussions
        return context


class MessageBoxView(MessageBoxRefreshView):
    template_name = 'social/message_box.html'

    def get_context_data(self, **kwargs):
        context = super(MessageBoxView, self).get_context_data(**kwargs)
        message_id = self.kwargs.get('message_id')
        discussions = context['discussions']
        if not message_id:
            if discussions.count() > 0:
                message_id = discussions[0].id
            else:
                message_id = 0
        context['init_message'] = message_id
        return context


class MessageReplyView(ListAPIView):
    """View sent messages"""
    queryset = MessageReply.objects.all()
    serializer_class = MessageReplySerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        """returns messages of current user"""
        queryset = super(MessageReplyView, self).get_queryset()
        request = get_request()
        message = get_message(self.kwargs['message_id'], request.user)
        queryset = queryset.filter(message=message).order_by('sent_datetime')

        # If user has exit the discussion. He can only view replies before his exit
        exit_datetime = None
        with transaction.atomic():
            recipients = MessageRecipient.objects.select_for_update().filter(message=message)
            for recipient in recipients:
                if recipient.exit_datetime:
                    if exit_datetime is None:
                        exit_datetime = recipient.exit_datetime
                    else:
                        exit_datetime = max(exit_datetime, recipient.exit_datetime)
            if exit_datetime:
                queryset = queryset.filter(
                    sent_datetime__lte=exit_datetime
                )

            # the message has been read
            recipients.filter(user=request.user).update(is_read=True)
        return queryset


class PostMessageReplyView(CreateAPIView):
    serializer_class = PostMessageReplySerializer
    permission_classes = [IsAuthenticated]

    def create(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)

        message = get_message(self.kwargs['message_id'], request.user)
        user_in_recipients_queryset = MessageRecipient.objects.filter(
            message=message, user=request.user, exit_datetime__isnull=True
        )
        if user_in_recipients_queryset.count() == 0:
            raise PermissionDenied

        body = serializer.validated_data['body']
        if body:
            MessageReply.objects.create(
                message=message,
                sender=request.user,
                sent_datetime=datetime.now(),
                body=body
            )
            message.updated_datetime = datetime.now()
            message.save()

            with transaction.atomic():
                MessageRecipient.objects.select_for_update().filter(message=message).exclude(
                    Q(user=request.user) | Q(exit_datetime__isnull=False)
                ).update(is_read=False)

        replies = MessageReply.objects.filter(message=message).order_by('sent_datetime')

        response_serializer = MessageReplySerializer(instance=replies, many=True)

        return Response(response_serializer.data)


class DeleteMessageFormView(LoginRequiredPopupFormView):
    template_name = 'social/popup_delete_message.html'
    form_class = ConfirmForm
    message = None

    def get_form_kwargs(self):
        form_kwargs = super(DeleteMessageFormView, self).get_form_kwargs()
        # Delete an existing message
        self.message = get_message(self.kwargs['message_id'], self.request.user)
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeleteMessageFormView, self).get_context_data(**kwargs)
        context['message'] = self.message
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            with transaction.atomic():
                recipients = MessageRecipient.objects.select_for_update().filter(
                    message=self.message, user=self.request.user
                )
                for recipient in recipients:
                    recipient.is_deleted = True
                    recipient.save()
                    recipient_profile = get_profile(recipient.user)
                    MessageReply.objects.create(
                        message=self.message,
                        sender=self.request.user,
                        sent_datetime=datetime.now(),
                        body=_(u'{0} left the conversation'.format(recipient_profile.name)),
                        is_action=True
                    )
        return HttpResponseRedirect(reverse("social:messages"))


class ExitMessageFormView(LoginRequiredPopupFormView):
    template_name = 'social/popup_exit_message.html'
    form_class = ConfirmForm
    message = None

    def get_form_kwargs(self):
        form_kwargs = super(ExitMessageFormView, self).get_form_kwargs()
        self.message = get_message(self.kwargs['message_id'], self.request.user)
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(ExitMessageFormView, self).get_context_data(**kwargs)
        context['message'] = self.message
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            with transaction.atomic():
                recipients = MessageRecipient.objects.select_for_update().filter(
                    message=self.message, user=self.request.user
                )
                for recipient in recipients:
                    recipient_profile = get_profile(recipient.user)
                    MessageReply.objects.create(
                        message=self.message,
                        sender=self.request.user,
                        sent_datetime=datetime.now(),
                        body=_(u'{0} left the conversation'.format(recipient_profile.name)),
                        is_action=True
                    )
                    recipient.exit_datetime = datetime.now()
                    recipient.save()
        return HttpResponseRedirect(reverse("social:messages"))


class DuplicateDiscussion(LoginRequiredPopupFormView):
    template_name = 'social/popup_duplicate_message.html'
    form_class = MessageSubjectForm
    message = None

    def get_form_kwargs(self):
        form_kwargs = super(DuplicateDiscussion, self).get_form_kwargs()
        self.message = get_message(self.kwargs['message_id'], self.request.user)
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DuplicateDiscussion, self).get_context_data(**kwargs)
        context['message'] = self.message
        return context

    def form_valid(self, form):
        subject = form.cleaned_data['subject']

        new_message = Message.objects.create(
            sender=self.request.user,
            subject=subject,
            sent_datetime=datetime.now(),
            updated_datetime=datetime.now()
        )

        for recipient in MessageRecipient.objects.filter(message=self.message):
            MessageRecipient.objects.create(
                message=new_message, user=recipient.user, is_deleted=False
            )

        return HttpResponseRedirect(reverse('social:messages_init', args=[new_message.id]))


class AddParticipantsToDiscussion(LoginRequiredPopupFormView):
    template_name = 'social/popup_add_participants_message.html'
    form_class = AddParticipantsForm
    message = None

    def get_form_kwargs(self):
        form_kwargs = super(AddParticipantsToDiscussion, self).get_form_kwargs()
        self.message = get_message(self.kwargs['message_id'], self.request.user)
        form_kwargs['instance'] = self.message
        user_ids = [user.id for user in self.message.get_recipients()]
        form_kwargs['recipients'] = User.objects.filter(id__in=user_ids)
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(AddParticipantsToDiscussion, self).get_context_data(**kwargs)
        context['message'] = self.message
        return context

    def form_valid(self, form):

        previous_recipients = self.message.get_recipients()

        new_recipients = list(form.cleaned_data['recipients']) + [self.request.user]
        with transaction.atomic():
            for recipient in previous_recipients:
                if recipient not in new_recipients:
                    MessageRecipient.objects.select_for_update().filter(
                        message=self.message, user=recipient
                    ).update(is_deleted=True)

                    recipient_profile = get_profile(recipient)
                    MessageReply.objects.create(
                        message=self.message,
                        sender=self.request.user,
                        sent_datetime=datetime.now(),
                        body=_(u'{0} has been removed'.format(recipient_profile.name)),
                        is_action=True
                    )

            for recipient in new_recipients:
                show_info = False
                try:
                    message_recipient = MessageRecipient.objects.select_for_update().get(
                        message=self.message, user=recipient
                    )
                    if message_recipient.is_deleted:
                        message_recipient.is_deleted = False
                        message_recipient.save()
                        show_info = True

                except MessageRecipient.DoesNotExist:
                    MessageRecipient.objects.create(
                        message=self.message, user=recipient
                    )
                    show_info = True

                except MessageRecipient.MultipleObjectsReturned:
                    MessageRecipient.objects.select_for_update().filter(
                        message=self.message, user=recipient
                    ).delete()
                    MessageRecipient.objects.create(
                        message=self.message, user=recipient
                    )
                    show_info = True

                if show_info:
                    recipient_profile = get_profile(recipient)
                    MessageReply.objects.create(
                        message=self.message,
                        sender=self.request.user,
                        sent_datetime=datetime.now(),
                        body=_(u'{0} joined the conversation'.format(recipient_profile.name)),
                        is_action=True
                    )
        return HttpResponseRedirect(reverse('social:messages_init', args=[self.message.id]))


class UpdateDiscussionSubject(LoginRequiredPopupFormView):
    template_name = 'social/popup_update_message_subject.html'
    form_class = MessageSubjectForm
    message = None

    def get_form_kwargs(self):
        form_kwargs = super(UpdateDiscussionSubject, self).get_form_kwargs()
        self.message = get_message(self.kwargs['message_id'], self.request.user)
        if self.message.sender != self.request.user:
            raise PermissionDenied
        form_kwargs['instance'] = self.message
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(UpdateDiscussionSubject, self).get_context_data(**kwargs)
        context['message'] = self.message
        return context

    def form_valid(self, form):
        form.save()
        MessageReply.objects.create(
            message=self.message,
            sender=self.request.user,
            sent_datetime=datetime.now(),
            body=_('discussion has been renamed'),
            is_action=True
        )
        return HttpResponseRedirect(reverse('social:messages_init', args=[self.message.id]))


@csrf_exempt
def upload_message_attachment(request, message_id=0):
    message = get_message(message_id, request.user)
    target_dir = get_members_attachments_directory(request.user.id)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    upload_data = handle_upload(request, absolute_target_dir)
    target_filename = upload_data['filename']

    # Create a new attachment
    reply = MessageReply.objects.create(
        message=message,
        sender=request.user,
        sent_datetime=datetime.now(),
        body=''
    )

    attachment_url = reverse('social:reply_attachment', args=[reply.id])
    reply.attachment = attachments_directory(reply, target_filename)
    reply.body = _('attached attachment://{0}:{1}').format(attachment_url, reply.attachment_filename)
    reply.save()

    message.updated_datetime = datetime.now()
    message.save()
    with transaction.atomic():
        MessageRecipient.objects.select_for_update().filter(message=message).update(is_read=False)

    return HttpResponse(target_filename)


class AttachmentDownloadView(PrivateDownloadView):
    """
    private download for attachments.
    """

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get_file(self, request, **kwargs):
        """
        get document from id and check user can acess it
        returns the file of the document
        """
        reply = get_object_or_404(MessageReply, id=kwargs['reply_id'])

        # Check permissions
        get_message(reply.message.id, request.user)

        return reply.attachment


class DeleteAttachmentView(LoginRequiredPopupFormView):
    template_name = 'social/popup_delete_attachment.html'
    form_class = ConfirmForm
    reply = None

    def get_form_kwargs(self):
        form_kwargs = super(DeleteAttachmentView, self).get_form_kwargs()
        # Delete an existing message
        self.reply = get_object_or_404(MessageReply, id=self.kwargs['reply_id'], attachment__isnull=False)
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(DeleteAttachmentView, self).get_context_data(**kwargs)
        context['reply'] = self.reply
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            if self.reply.message.sender != self.request.user or not self.reply.message.is_group:
                raise PermissionDenied
            self.reply.delete()
        return HttpResponseRedirect(reverse("social:messages"))
