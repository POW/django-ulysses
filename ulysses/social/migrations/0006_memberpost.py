# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-09 15:42


from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import ulysses.social.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('ulysses_social','0005_remove_message_is_read'),
    ]

    operations = [
        migrations.CreateModel(
            name='MemberPost',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('post_type', models.CharField(choices=[(b'community-news', 'Community News'), (b'looking-for', 'Looking For')], max_length=20, verbose_name='post type')),
                ('post_datetime', models.DateTimeField(auto_now=True)),
                ('text', models.TextField(verbose_name='text')),
                ('image', models.ImageField(blank=True, default=None, null=True, upload_to=ulysses.social.models.members_post_directory, verbose_name='image')),
                ('owner', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL, verbose_name='member user')),
            ],
            options={
                'ordering': ['-post_datetime'],
                'verbose_name': 'Member post',
                'verbose_name_plural': 'Member posts',
            },
        ),
    ]
