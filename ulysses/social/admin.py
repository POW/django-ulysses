# -*- coding: utf-8 -*-

from django.contrib import admin

from ulysses.super_admin.sites import super_admin_site

from .models import (
    MemberRecommendation, WorkRecommendation, Message, Following, MemberPost, FeedItem, PostTag, MessageRecipient,
    MemberBookmark
)


class MemberRecommendationAdmin(admin.ModelAdmin):
    list_display = ('member_user', 'recommended_by', 'recommendation_datetime', )
    date_hierarchy = 'recommendation_datetime'
    raw_id_fields = ['member_user', 'recommended_by']
    search_fields = ['recommended_by__email']


class WorkRecommendationAdmin(admin.ModelAdmin):
    list_display = ('work', 'recommended_by', 'recommendation_datetime', )
    date_hierarchy = 'recommendation_datetime'
    raw_id_fields = ['work', 'recommended_by']
    search_fields = ['recommended_by__email']


class FollowingAdmin(admin.ModelAdmin):
    list_display = ('member_user', 'follower', 'follow_datetime', )
    date_hierarchy = 'follow_datetime'
    raw_id_fields = ['member_user', 'follower']
    search_fields = ['follower__email']


class MemberBookmarkAdmin(admin.ModelAdmin):
    list_display = ('member_user', 'bookmarked_by', 'bookmark_datetime', )
    date_hierarchy = 'bookmark_datetime'
    raw_id_fields = ['member_user', 'bookmarked_by']
    search_fields = ['bookmarker__email']


class MessageRecipientInline(admin.TabularInline):
    model = MessageRecipient


class MessageAdmin(admin.ModelAdmin):
    list_display = ('sender', 'recipients_list', 'subject', 'sent_datetime', )
    date_hierarchy = 'sent_datetime'
    raw_id_fields = ['recipients', 'sender']
    search_fields = ['sender__email']
    inlines = [MessageRecipientInline]


class MemberPostAdmin(admin.ModelAdmin):
    list_display = ('owner', 'post_datetime', 'post_type', )
    date_hierarchy = 'post_datetime'
    list_filter = ('post_type', )
    raw_id_fields = ['owner', ]
    search_fields = ['owner__email']


class FeedItemAdmin(admin.ModelAdmin):
    list_display = ('owner', 'feed_datetime', 'tag', 'content_type', )
    date_hierarchy = 'feed_datetime'
    list_filter = ('tag', 'content_type', )
    raw_id_fields = ['owner', 'parent', 'sender', ]
    search_fields = ['owner__email']


class PostTagAdmin(admin.ModelAdmin):
    list_display = ('name', 'creation_date', 'posts_count', )
    date_hierarchy = 'creation_date'
    search_fields = ['name', ]


super_admin_site.register(MemberRecommendation, MemberRecommendationAdmin)
super_admin_site.register(WorkRecommendation, WorkRecommendationAdmin)
super_admin_site.register(Message, MessageAdmin)
super_admin_site.register(Following, FollowingAdmin)
super_admin_site.register(MemberPost, MemberPostAdmin)
super_admin_site.register(FeedItem, FeedItemAdmin)
super_admin_site.register(PostTag, PostTagAdmin)
super_admin_site.register(MemberBookmark, MemberBookmarkAdmin)
