# -*- coding: utf-8 -*-

from datetime import datetime

from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.profiles.utils import get_profile

from .models import FeedItem


def create_notification(sender, recipient, text, obj, parent_tag=''):
    """create a notification"""
    content_type = ContentType.objects.get_for_model(obj)

    return FeedItem.objects.create(
        feed_datetime=datetime.now(),
        tag=FeedItem.NOTIFICATION,
        owner=recipient,
        sender=sender,
        content_type=content_type,
        object_id=obj.id,
        text=text,
        is_read=False,
        parent_tag=parent_tag
    )


def notify_new_object(owner, obj, share_label):
    """create a notification"""
    content_type = ContentType.objects.get_for_model(obj)

    return FeedItem.objects.create(
        feed_datetime=datetime.now(),
        owner=owner,
        content_type=content_type,
        object_id=obj.id,
        text='',
        is_read=False,
        share_label=share_label
    )


def notify_event_participation(event, participant):
    """create a message in order to inform a user that he as been added to an event participant"""

    if event.owner != participant:
        create_notification(
            event.owner,
            participant,
            _("You have been added to the '{0}' event participants".format(event)),
            event
        )


def notify_following(following):
    """inform that the user received a recommendation"""
    follower_profile = get_profile(following.follower)

    # Notify the followed member
    create_notification(
        following.follower,
        following.member_user,
        _('{0} is now following you'.format(follower_profile.name)),
        follower_profile
    )


def notify_work_bookmark(work_bookmark):
    """create a message in order to inform that his work received a recommendation"""
    profile = get_profile(work_bookmark.member_user)
    work = work_bookmark.work
    recipients_list = [work.owner]
    if work.composer and work.composer != work.owner:
        recipients_list.append(work.composer)

    # Do not notify user if he bookmarked his own work
    try:
        recipients_list.remove(work_bookmark.member_user)
    except ValueError:
        pass

    # Add a Notification for the member --> Indicate something added in his selection
    create_notification(
        work_bookmark.member_user,
        work_bookmark.member_user,
        _('You have bookmarked {0}'.format(work.title)),
        work,
        parent_tag=FeedItem.SELECTION
    )

    # Notify all people who are involved in this work that someone has bookmarked it
    for recipient in recipients_list:
        create_notification(
            work_bookmark.member_user,
            recipient,
            _('Your work {0} has been bookmarked by {1}'.format(work.title, profile.name)),
            work,
            parent_tag=FeedItem.SELECTION
        )


def notify_work_created(work):
    """create a message in order to inform that his work received a recommendation"""
    if work.is_public():
        notify_new_object(
            work.owner,
            work,
            _('added a work')
        )


def notify_event_bookmark(member_calendar_item):
    """create a message in order to inform that his work received a recommendation"""
    profile = get_profile(member_calendar_item.user)
    event = member_calendar_item.event
    recipients_list = [event.owner]

    for collaboration in set(event.get_collaborations()):
        if collaboration.user != event.owner:
            recipients_list.append(collaboration.user)

    # Do not notify user if he bookmarked his own work
    try:
        recipients_list.remove(member_calendar_item.user)
    except ValueError:
        pass

    # Add a Notification for the member --> Indicate something added in his selection
    create_notification(
        member_calendar_item.user,
        member_calendar_item.user,
        _('You have bookmarked {0}'.format(event.title)),
        event,
        parent_tag=FeedItem.SELECTION
    )

    # Notify all people who are involved in this event that someone has bookmarked it
    for recipient in recipients_list:
        create_notification(
            member_calendar_item.user,
            recipient,
            _('Your event {0} has been bookmarked by {1}'.format(event.title, profile.name)),
            event,
            parent_tag=FeedItem.SELECTION
        )


def notify_event_created(event):
    """create a message in order to inform that his work received a recommendation"""
    notify_new_object(
        event.owner,
        event,
        _('shared an event')
    )


def notify_membership_request(membership_request):
    """inform that the user has requested to be member of an organization"""
    organization = membership_request.organization
    profile = membership_request.individual
    site = Site.objects.get_current()

    text = _('{0} would like to be listed in the members of {1}. Accept? http://{2}{3}'.format(
        profile.name, organization.name,
        site.domain,
        reverse('profiles:accept_membership', args=[membership_request.id])
    ))

    return create_notification(
        profile.user,
        organization.user,
        text,
        profile
    )


def notify_membership_accepted(membership_request):
    """inform a user when he has been accepted as member of an organization"""
    organization = membership_request.organization
    profile = membership_request.individual

    return create_notification(
        organization.user,
        profile.user,
        _('You have been accepted as member of {0}'.format(organization.name)),
        organization
    )


def notify_membership_removed(profile, individual, organization):
    """inform a user when he has been removed from members of an organization"""

    if profile == individual:
        # Done by the individual
        recipient = organization
    else:
        # Done by the organization
        recipient = individual

    text = _('{0} is now detached from your organization {1}'.format(individual.name, organization.name))

    return create_notification(
        profile.user,
        recipient.user,
        text,
        profile
    )


def get_shared_content_owners(feed):
    """return list of owners of a content: who must be notified"""
    owners = []
    for owner in getattr(feed.content_object, 'owners', []):
        owners.append(owner)

    if not owners:
        owner = getattr(feed.content_object, 'owner', None)
        if not owner:
            # profiles
            owner = getattr(feed.content_object, 'user', None)
        if owner:
            owners.append(owner)

    # Do not notify the feed owner if he is in the list
    if feed.owner in owners:
        owners.remove(feed.owner)

    return owners


def get_notification_text(feed, updated=False, shared_by=None, repost=False):
    """get the text for notifiation"""
    # convert some of content types
    content_type_name = {
        'individual': _('profile'),
        'organization': _('profile'),
    }.get(feed.content_type.name.lower(), feed.content_type.name.lower())

    if shared_by is None:
        shared_by = feed.owner

    shared_by_profile = get_profile(shared_by)

    # Build text
    name = getattr(feed.content_object, 'name', '') or getattr(feed.content_object, 'title', '')

    verb = _('reposted') if repost else _('shared')
    noun = _('repost') if repost else _('share')

    if updated:
        text = _('{1} has updated the {2} of your {0}').format(content_type_name, shared_by_profile.name, noun)
    else:
        text = _('{1} {2} your {0}').format(content_type_name, shared_by_profile.name, verb)

    if name and content_type_name != _('profile'):
        text += ': ' + name

    return text


def notify_share_content(feed, updated=False, shared_by=None):
    """inform a user when his content has been shared"""

    shared_by = shared_by if shared_by else feed.owner
    text = get_notification_text(feed, updated=updated, shared_by=shared_by)

    for owner in get_shared_content_owners(feed):

        if owner and owner != shared_by:
            create_notification(
                shared_by if shared_by else owner,
                owner,
                text,
                feed,
                parent_tag=feed.tag
            )


def notify_updated_post(post, shared_feeds):
    """inform a user when a shared post has been modified"""
    recipients = [feed_item.owner for feed_item in shared_feeds]

    for recipient in recipients:
        create_notification(
            post.owner,
            recipient,
            _('One of your shared posts has been modified'),
            post
        )


def notify_repost(feed, updated=False):
    """inform a user when a shared post has been reposted"""

    text = get_notification_text(feed.parent, updated=updated, shared_by=feed.owner, repost=True)

    # Notify owner of the original post
    if feed.owner != feed.parent.owner:
        create_notification(
            feed.owner,
            feed.parent.owner,
            text,
            feed.parent
        )

    # Notify owners of the original content
    notify_share_content(feed.parent, updated=updated, shared_by=feed.owner)
