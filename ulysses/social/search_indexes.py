# -*- coding: utf-8 -*-

from haystack import indexes

from .models import MemberPost


class MemberPostIndex(indexes.SearchIndex, indexes.Indexable):
    """haystack"""
    text = indexes.CharField(document=True, use_template=True)

    def get_model(self):
        return MemberPost

    def index_queryset(self, using=None):
        """Used when the entire index for model is updated."""
        return self.get_model().objects.filter(is_deleted=False)
