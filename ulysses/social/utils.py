# -*- coding: utf-8 -*-

from datetime import datetime
from itertools import chain

from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import User
from django.core.exceptions import PermissionDenied
from django.db.models import Q
from django.shortcuts import get_object_or_404

from ulysses.profiles.models import WelcomeMessage
from ulysses.events.models import Event
from ulysses.works.models import Work

from .models import (
    Following, MemberPost, FeedItem, Message, MessageRecipient, Document, ContactsGroup, WorkBookmark, MessageReply,
    MemberBookmark
)


def is_followed_by(user, follower):
    """returns True if the user is followed by another"""
    if follower.is_authenticated and follower != user:
        return Following.objects.filter(member_user=user, follower=follower).exists()
    return False


def is_bookmarked_by(user, bookmarked_by):
    """returns True if the user is followed by another"""
    if bookmarked_by.is_authenticated:
        return MemberBookmark.objects.filter(member_user=user, bookmarked_by=bookmarked_by).exists()
    return False


def is_work_bookmarked_by(work, member_user):
    """returns True is work recommended by a user"""
    return WorkBookmark.objects.filter(work=work, member_user=member_user).exists()


def get_bookmarked_works(user):
    """returns the list of works bookmarked by the given user"""
    return Work.objects.filter(bookmarks__member_user=user).distinct()


def get_user_followed(user):
    """returns the list of users followed by the given user"""
    return User.objects.filter(followed_set__follower=user)


def get_bookmarked_members(user):
    """returns the list of users bookmarked by the given user"""
    return User.objects.filter(bookmarked_set__bookmarked_by=user)


def get_user_followers(user):
    """returns the list of users following the given user"""
    return User.objects.filter(follower_set__member_user=user)


def get_user_posts(user, post_type):
    """returns the list of post"""

    # returns all my and one sent of user I am following "community news" and also all "looking-for"
    queryset = MemberPost.objects.filter(
        Q(owner__in=get_user_followed(user)) | Q(owner=user) | Q(post_type=MemberPost.LOOKING_FOR_POST)
    ).filter(post_type=post_type)
    return queryset


def get_user_feeds(user, post_type=None):
    """returns the list of feed items"""

    followed_users = get_user_followed(user)

    feed_item_content_type = ContentType.objects.get_for_model(FeedItem)

    # returns:
    # - all my and followed members "community news"
    # - all "looking-for"
    # - reposts (not the repost itself but the posts which has been reposted by my followed members)
    # - favorites
    queryset = FeedItem.objects.filter(
        is_deleted=False
    ).filter(
        Q(owner__in=get_user_followed(user)) | Q(owner=user) |
        Q(tag=MemberPost.COMMUNITY_NEWS_POST) | Q(tag=MemberPost.LOOKING_FOR_POST) |
        (Q(feeditem__content_type=feed_item_content_type, feeditem__owner__in=followed_users))
    )

    queryset = queryset.exclude(
        Q(tag=FeedItem.NOTIFICATION) & ~Q(owner=user)  # Notifications : only if it concerns the current user
    )

    # Reposts : don't show the repost itself (but keep notifications on a FeedItem (post shared)
    queryset = queryset.exclude(
        Q(content_type=feed_item_content_type) & ~Q(tag=FeedItem.NOTIFICATION)
    )

    if post_type == MemberPost.LOOKING_FOR_POST:
        queryset = queryset.filter(tag=MemberPost.LOOKING_FOR_POST)

    elif post_type == MemberPost.COMMUNITY_NEWS_POST:
        queryset = queryset.exclude(tag=MemberPost.LOOKING_FOR_POST)

    return queryset.distinct().order_by('-update_datetime')


def get_all_feeds(queryset=None, only_works=False, no_works=False):
    """returns the list of feed items"""
    if only_works:
        allowed_models = (Work, )
    elif no_works:
        allowed_models = (Event, MemberPost, )
    else:
        allowed_models = (Event, MemberPost, Work,)

    allowed_content_types = [
        ContentType.objects.get_for_model(model_class) for model_class in allowed_models
    ]
    if queryset is None:
        queryset = FeedItem.objects.all()
    queryset = queryset.filter(
        Q(tag=MemberPost.COMMUNITY_NEWS_POST) | Q(tag=MemberPost.LOOKING_FOR_POST),
        content_type__in=allowed_content_types,
        parent__isnull=True,
        is_deleted=False
    )
    return queryset.distinct().order_by('-update_datetime')


def filter_following(queryset, user):
    feed_item_content_type = ContentType.objects.get_for_model(FeedItem)

    followed_users = get_user_followed(user)

    return queryset.filter(
        Q(owner__in=get_user_followed(user)) | Q(owner=user) |
        (Q(feeditem__content_type=feed_item_content_type, feeditem__owner__in=followed_users))
    ).distinct().order_by('-update_datetime')


def get_homepage_feeds():
    """returns the list of feed items"""
    return get_all_feeds(no_works=True)


def get_community_feeds(user=None):
    """returns the list of feed items"""
    # only_posts = ContentType.objects.objects.get_for_model(MemberPost)
    queryset = FeedItem.objects.filter(
        tag=MemberPost.COMMUNITY_NEWS_POST
    ).exclude(is_deleted=True)
    return queryset


def create_message(sender, recipients, subject, text):
    """create a notification"""

    message = Message.objects.create(
        sent_datetime=datetime.now(),
        sender=sender,
        subject=subject,
        updated_datetime=datetime.now()
        #body=text,
    )

    MessageReply.objects.create(
        message=message,
        sent_datetime=datetime.now(),
        sender=sender,
        body=text
    )

    if sender:
        MessageRecipient.objects.create(user=sender, message=message)

    for recipient in recipients:
        MessageRecipient.objects.create(user=recipient, message=message)

    return message


def create_welcome_messages(profile):
    """send a welcome message to all new users after first login"""

    if profile.is_individual():
        expected_types = (WelcomeMessage.MESSAGE_FOR_ALL, WelcomeMessage.MESSAGE_FOR_INDIVIDUALS)
    else:
        expected_types = (WelcomeMessage.MESSAGE_FOR_ALL, WelcomeMessage.MESSAGE_FOR_ORGANIZATIONS)

    # Create messages in reverse order to get it in right order in the message box
    for welcome_message in WelcomeMessage.objects.filter(recipient_type__in=expected_types).order_by('-order'):

        create_message(
            sender=None, recipients=[profile.user], subject=welcome_message.subject, text=welcome_message.text
        )


def get_user_documents(user):
    """return list of documents of a user"""

    # Ge the user followed by current user
    followed_users = get_user_followed(user)
    groups = ContactsGroup.objects.filter(users=user)

    queryset = Document.objects.filter(
        # Q(access_type=Document.ACCESS_ALL) |
        # Q(access_type=Document.ACCESS_MEMBERS) |
        Q(access_type=Document.ACCESS_GROUP, allowed_group__in=groups) |
        Q(access_type=Document.ACCESS_FOLLOWERS, owner__in=followed_users) |
        Q(access_type=Document.ACCESS_DEFINED, allowed_users=user)
    ).exclude(owner=user).exclude(documentaccess__is_deleted=True).distinct()

    return queryset


def get_message(message_id, user):
    if user.is_anonymous:
        raise PermissionDenied
    message = get_object_or_404(Message, id=message_id)
    if MessageRecipient.objects.filter(message=message, user=user, is_deleted=False).count() == 0:
        raise PermissionDenied
    return message


def get_notifications(user):
    """
    :param user:
    :return: unread notifications + Last 4 read notifications
    """
    base_queryset = FeedItem.objects.filter(
        owner=user,
        tag=FeedItem.NOTIFICATION,
        is_deleted=False,
    ).exclude(
        Q(parent_tag=MemberPost.LOOKING_FOR_POST) | (Q(parent_tag=FeedItem.SELECTION) & Q(tag=FeedItem.NOTIFICATION))
    )

    queryset1 = base_queryset.filter(is_read=False).order_by('-feed_datetime')

    queryset2 = base_queryset.filter(is_read=True).order_by('-feed_datetime')[:4]

    return list(chain(queryset1, queryset2))


def get_discussions(user, only_unread=False):
    receptions = MessageRecipient.objects.filter(is_deleted=False, user=user)
    if only_unread:
        receptions = receptions.filter(is_read=False)
    return Message.objects.filter(
        id__in=[reception.message.id for reception in receptions]
    ).distinct().order_by('-updated_datetime')
