# -*- coding: utf-8 -*-

from ulysses.generic.widgets import TagAutocompleteBaseWidget

from ulysses.social.models import PostTag


class PostTagAutocompleteWidget(TagAutocompleteBaseWidget):
    """An autocomplete widget for choosing or adding tags"""
    tag_model = PostTag
