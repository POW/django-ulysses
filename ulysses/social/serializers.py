# -*- coding: utf-8 -*-

from rest_framework.serializers import ModelSerializer, CharField

from ulysses.profiles.serializers import ProfileSerializer

from ulysses.middleware import get_request
from ulysses.utils import dehtml

from .models import Document, DocumentAccess, MessageReply


class DocumentSerializer(ModelSerializer):
    owner = ProfileSerializer(read_only=True)
    get_allowed_users = ProfileSerializer(read_only=True, many=True)
    get_absolute_url = CharField(read_only=True)
    access_type_label = CharField(read_only=True)
    name = CharField(read_only=True)
    icon = CharField(read_only=True)

    class Meta:
        model = Document
        fields = (
            'id', 'get_absolute_url', 'owner', 'get_allowed_users', 'datetime', 'description',
            'access_type_label', 'name', 'icon'
        )

    def is_document_read(self, request, instance):
        try:
            doc_access = DocumentAccess.objects.get(document=instance, user=request.user)
            if not doc_access.is_read:
                doc_access.is_read = True
                doc_access.save()
                return False
            return True
        except DocumentAccess.DoesNotExist:
            DocumentAccess.objects.create(document=instance, user=request.user, is_read=True)
            return False

    def to_representation(self, instance):
        request = get_request()
        data = super(DocumentSerializer, self).to_representation(instance)
        data['is_read'] = self.is_document_read(request, instance)
        data['get_delete_url'] = instance.get_delete_url()
        if instance.owner == request.user:
            data['get_edit_url'] = instance.get_edit_url()
        else:
            data['get_edit_url'] = None
        return data


class MessageReplySerializer(ModelSerializer):
    sender = ProfileSerializer(read_only=True)
    html_body = CharField(read_only=True)

    class Meta:
        model = MessageReply
        fields = (
            'id', 'sender', 'sent_datetime', 'html_body', 'send_by_me', 'sent_on', 'sent_by', 'is_group',
            'is_attachment', 'is_action'
        )


class PostMessageReplySerializer(ModelSerializer):

    class Meta:
        model = MessageReply
        fields = (
            'body',
        )

    def validate_body(self, value):
        return dehtml(value)
