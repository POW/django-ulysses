# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import re

from django.contrib.auth.models import User
from django.utils.translation import ugettext as _, ugettext_lazy as _l

import floppyforms.__future__ as forms

from ulysses.generic.forms import BaseForm, BaseModelForm, FileUploadForm
from ulysses.middleware import get_request

from ulysses.profiles.fields import MultipleUserChoiceField
from ulysses.profiles.models import ContactsGroup
from ulysses.works.models import Work

from .models import (
    MemberRecommendation, WorkRecommendation, Message, MemberPost, FeedItem, Document, DocumentAccess, PostTag
)
from .widgets import PostTagAutocompleteWidget


class ActionOnMemberBaseForm(forms.Form):
    """Base form for action on members : follow, recommend"""
    member_user = forms.IntegerField(required=True, widget=forms.HiddenInput())
    action_yourself_error = None
    allow_action_on_yourself = False

    def __init__(self, done_by=None, *args, **kwargs):
        self.done_by = done_by
        super(ActionOnMemberBaseForm, self).__init__(*args, **kwargs)

    def clean_member_user(self):
        user_id = self.cleaned_data.get('member_user', None)
        if not user_id:
            raise forms.ValidationError(_('Unknown user'))
        try:
            return User.objects.get(id=user_id)
        except User.DoesNotExist:
            raise forms.ValidationError(_('Unknown user'))

    def clean(self):
        ret = super(ActionOnMemberBaseForm, self).clean()
        member_user = self.cleaned_data.get('member_user', None)
        if not self.allow_action_on_yourself and member_user == self.done_by:
            raise forms.ValidationError(self.action_yourself_error)
        return ret


class RecommendMemberForm(ActionOnMemberBaseForm):
    """recommend a member"""
    action_yourself_error = _l("You can't recommend yourself")

    def clean(self):
        ret = super(RecommendMemberForm, self).clean()

        member_user = self.cleaned_data.get('member_user', None)
        if MemberRecommendation.objects.filter(member_user=member_user, recommended_by=self.done_by).exists():
            raise forms.ValidationError(_("A recommendation already exists"))

        return ret


class FollowMemberForm(ActionOnMemberBaseForm):
    """follow a member"""
    action_yourself_error = _l("You can't follow yourself")


class BookmarkMemberForm(ActionOnMemberBaseForm):
    allow_action_on_yourself = True


class RecommendWorkForm(forms.Form):
    """recommend a work"""
    work = forms.IntegerField(required=True, widget=forms.HiddenInput())

    def __init__(self, done_by=None, *args, **kwargs):
        self.done_by = done_by
        super(RecommendWorkForm, self).__init__(*args, **kwargs)

    def clean_work(self):
        work_id = self.cleaned_data.get('work', None)
        if not work_id:
            raise forms.ValidationError(_('Unknown work'))
        try:
            return Work.objects.get(id=work_id)
        except Work.DoesNotExist:
            raise forms.ValidationError(_('Unknown user'))

    def clean(self):
        ret = super(RecommendWorkForm, self).clean()

        work = self.cleaned_data.get('work', None)

        if WorkRecommendation.objects.filter(work=work, recommended_by=self.done_by).exists():
            raise forms.ValidationError(_("A recommendation already exists"))

        return ret


class WorkBookmarkForm(forms.Form):
    """bookmark a work"""
    work = forms.IntegerField(required=True, widget=forms.HiddenInput())

    def clean_work(self):
        work_id = self.cleaned_data.get('work', None)
        if not work_id:
            raise forms.ValidationError(_('Unknown work'))
        try:
            return Work.objects.get(id=work_id)
        except Work.DoesNotExist:
            raise forms.ValidationError(_('Unknown user'))


class ComposeMessageForm(BaseModelForm):
    """compose a message"""
    recipients = MultipleUserChoiceField(required=False, label=_('Contacts'))

    class Meta:
        model = Message
        fields = ('recipients', 'subject',)

    def __init__(self, recipients=None, *args, **kwargs):
        super(ComposeMessageForm, self).__init__(*args, **kwargs)

        if recipients:
            self.fields['recipients'].set_initial(recipients)

    def clean_recipients(self):
        recipients = self.cleaned_data['recipients']
        if not recipients:
            raise forms.ValidationError(_('This fields is required'))
        return recipients


class AddParticipantsForm(BaseModelForm):
    """compose a message"""
    recipients = MultipleUserChoiceField(required=False, label=_('Recipients'))

    class Meta:
        model = Message
        fields = ('recipients', )

    def __init__(self, recipients=None, *args, **kwargs):
        super(AddParticipantsForm, self).__init__(*args, **kwargs)

        if recipients:
            self.fields['recipients'].set_initial(recipients)

    def clean_recipients(self):
        recipients = self.cleaned_data['recipients']
        if not recipients:
            raise forms.ValidationError(_('This fields is required'))
        return recipients


class MessageSubjectForm(BaseModelForm):
    """compose a message"""

    class Meta:
        model = Message
        fields = ('subject',)


class MemberPostForm(FileUploadForm):
    """post a Community News or a Looking for"""

    class Meta:
        model = MemberPost
        fields = ('text', 'image', 'iframe', 'tags', )
        upload_fields = ('image', )
        upload_fields_args = {
            'image': {'extensions': 'png|jpg', 'label': _("Image"), 'upload_url_name': 'social:upload_post_image'},
        }

    def __init__(self, *args, **kwargs):
        super(MemberPostForm, self).__init__(*args, **kwargs)
        self.fields['iframe'].label = ''
        self.fields['tags'].widget = PostTagAutocompleteWidget()
        self.fields['tags'].label = _('Keywords')

    def is_souncloud(self, url):
        if re.match('https://w.soundcloud.com/player/.*', url):
            return '<iframe width="100%" height="450" scrolling="no" frameborder="no" src="{0}"></iframe>'.format(url)

    def is_youtube(self, url):
        if re.match('https://www.youtube.com/.*', url) or re.match('https://youtu.be/.*', url):
            return '<iframe width="100%" height="450" scrolling="no" frameborder="0" src="{0}"></iframe>'.format(url)

    def clean_iframe(self):
        iframe_code = self.cleaned_data['iframe']
        if iframe_code:
            soup = BeautifulSoup(iframe_code, 'html.parser')
            iframe_tags = soup.select('iframe')
            if len(iframe_tags) != 1:
                raise forms.ValidationError(_('Invalid code'))
            iframe_tag = iframe_tags[0]
            url = iframe_tag.get('src')
            for callback in [self.is_youtube, self.is_souncloud]:
                content = callback(url)
                if content:
                    return content
            raise forms.ValidationError(_('This embed content is not supported'))
        return ''

    def clean_tags(self):
        """return tags. Only allowed for looking-for posts"""
        tags = self.cleaned_data['tags']
        return tags


class ShareElementForm(BaseModelForm):
    """share an element"""

    class Meta:
        model = FeedItem
        fields = ('content_type', 'object_id', 'tag', 'parent', 'text', )
        widgets = {
            'content_type': forms.HiddenInput(),
            'object_id': forms.HiddenInput(),
            'tag': forms.HiddenInput(),
            'parent': forms.HiddenInput(),
        }

    def __init__(self, *args, **kwargs):
        super(ShareElementForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = ''


class EditShareElementForm(BaseModelForm):
    """share an element"""

    class Meta:
        model = FeedItem
        fields = ('text', )

    def __init__(self, *args, **kwargs):
        super(EditShareElementForm, self).__init__(*args, **kwargs)
        self.fields['text'].label = ''


class ShareElementFilterForm(BaseForm):
    """filter items on the community news page"""

    filter = forms.ChoiceField(
        required=False,
        choices=(
            ('', _('All')),
            (MemberPost.COMMUNITY_NEWS_POST, _('Community News')),
            (MemberPost.LOOKING_FOR_POST, _('Looking For')),
            (FeedItem.NOTIFICATION, _('Notifications')),
        )
    )


class DocumentForm(FileUploadForm):
    """share a document"""

    allowed_users = MultipleUserChoiceField(required=True, label=_('Members'))

    class Meta:
        model = Document
        fields = ('file', 'access_type', 'allowed_users', 'allowed_group', 'description', )
        upload_fields = ('file', )
        upload_fields_args = {
            'file': {'extensions': '', 'label': _("Document"), 'upload_url_name': 'social:upload_document_file'},
        }

    def __init__(self, *args, **kwargs):
        super(DocumentForm, self).__init__(*args, **kwargs)
        self.fields['allowed_users'].required = False
        if self.instance and self.instance.id and self.instance.access_type == Document.ACCESS_DEFINED:
            self.fields['allowed_users'].set_initial(self.instance.allowed_users.all())
        else:
            self.fields['allowed_users'].set_initial(User.objects.none())

        request = get_request()
        self.fields['allowed_group'].queryset = ContactsGroup.objects.filter(owner=request.user)

    def _save_m2m(self):
        if self.cleaned_data['access_type'] == Document.ACCESS_DEFINED:
            for user in self.cleaned_data['allowed_users']:
                DocumentAccess.objects.get_or_create(user=user, document=self.instance)


class PostReplyForm(forms.Form):
    body = forms.CharField(
        widget=forms.TextInput(attrs={'placeholder': _('Type your message'), 'class': 'message-body-input'})
    )
