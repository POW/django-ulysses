# -*- coding: utf-8 -*-

from django.contrib.contenttypes.models import ContentType
from django.shortcuts import get_object_or_404, Http404
from django.template.loader import get_template
from django.utils.safestring import mark_safe
from django.utils.translation import ugettext as _

from ulysses.middleware import get_request


def get_content_object(content_type_id, object_id):
    """returns an object from content_type data"""
    content_type = ContentType.objects.get(id=content_type_id)
    model_class = content_type.model_class()
    return get_object_or_404(model_class, id=object_id)


def get_feed_content(content_type_id, object_id, short_content=False, feed_item=None):
    """returns the content of an object for displaying it the community feed"""
    content_type = ContentType.objects.get(id=content_type_id)
    model_class = content_type.model_class()
    request = get_request()
    try:
        content_object = get_object_or_404(model_class, id=object_id)
        context_data = {
            'content_object': content_object,
            'short_content': short_content,
            'feed_item': feed_item,
            'user': request.user if request else None,
        }
        template = get_template('social/share/{0}.html'.format(content_type.model))
        return template.render(context_data)
    except Http404:
        return mark_safe('<p class="content-missing">{0}</p>'.format(_('Content missing')))
