# -*- coding: utf-8 -*-

from django.conf.urls import url

from .views.community_news import CommunityNewsView, ShareElementView, EditShareElementView, DeleteShareElementView
from .views.documents import (
    upload_document_file, DocumentFormView, SharedDocumentListView, UserDocumentListView, DeleteDocumentFormView,
    DocumentDownloadView
)
from .views.favorites import FavoriteMembersView, FavoriteWorksView
from .views.followings import (
    FollowMemberView, FollowersView, FollowersListView, FollowingListView, follow_member_ajax
)
from .views.messages import (
    ComposeMessageView, DeleteMessageFormView, MessageBoxView, MessageReplyView, PostMessageReplyView,
    upload_message_attachment, AttachmentDownloadView, DuplicateDiscussion, UpdateDiscussionSubject,
    AddParticipantsToDiscussion, MessageBoxRefreshView, ExitMessageFormView, DeleteAttachmentView
)
from .views.posts import (
    MemberPostFormView, MemberPostPostedView, upload_post_image, DeletePostView, mark_notifications_as_read
)
from .views.work_bookmarks import BookmarkWorkView
from .views.member_bookmarks import BookmarkMemberView


app_name = "social"


urlpatterns = [

    # Following
    url(r'follow-member/(?P<id>\d+)/$', FollowMemberView.as_view(), name="follow_member"),
    url(r'followers/(?P<user_id>\d+)/$', FollowersView.as_view(), name="followers"),
    url(r'bookmark-member/(?P<id>\d+)/$', BookmarkMemberView.as_view(), name="bookmark_member"),
    url(r'followers-list/(?P<id>\d+)/$', FollowersListView.as_view(), name="followers_list"),
    url(r'following-list/(?P<id>\d+)/$', FollowingListView.as_view(), name="following_list"),
    url(r'follow-member-ajax/(?P<user_id>\d+)/$', follow_member_ajax, name="follow_member_ajax"),

    # Messages
    url(r'^messages/$', MessageBoxView.as_view(), name="messages"),
    url(r'^refresh-messages/$', MessageBoxRefreshView.as_view(), name="refresh_messages"),
    url(r'^messages/(?P<message_id>\d+)/$', MessageBoxView.as_view(), name="messages_init"),
    url(r'^message-thread/(?P<message_id>\d+)/$', MessageReplyView.as_view(), name="message_thread"),
    url(r'^post-message/(?P<message_id>\d+)/$', PostMessageReplyView.as_view(), name="post_message"),
    url(r'^post-message-attachment/(?P<message_id>\d+)/$', upload_message_attachment, name="post_message_attachment"),
    url(r'^get-message-attachment/(?P<reply_id>\d+)/$', AttachmentDownloadView.as_view(), name="reply_attachment"),
    url(r'^delete-attachment/(?P<reply_id>\d+)/$', DeleteAttachmentView.as_view(), name="delete_attachment"),

    url(r'^compose-message/$', ComposeMessageView.as_view(compose_message=True), name="compose_message"),
    url(r'^write-to/(?P<id>\d+)/$', ComposeMessageView.as_view(write_to=True), name="write_to"),
    url(r'^share-work/(?P<work_id>\d+)/$', ComposeMessageView.as_view(), name="share_work"),
    url(r'^share-focus-on/(?P<focus_on_id>\d+)/$', ComposeMessageView.as_view(), name="share_focus_on"),
    url(r'^reply-to/(?P<message_id>\d+)/$', ComposeMessageView.as_view(reply_to=True), name="reply_to"),
    url(
        r'^compose-content-message/(?P<content_type_id>\d+)/(?P<element_id>\d+)/$',
        ComposeMessageView.as_view(),
        name="compose_content_message"
    ),
    url(r'^delete-message/(?P<message_id>\d+)/$', DeleteMessageFormView.as_view(), name="delete_message"),
    url(r'^exit-message/(?P<message_id>\d+)/$', ExitMessageFormView.as_view(), name="exit_message"),
    url(r'^duplicate-message/(?P<message_id>\d+)/$', DuplicateDiscussion.as_view(), name="duplicate_message"),
    url(r'^update-subject/(?P<message_id>\d+)/$', UpdateDiscussionSubject.as_view(), name="update_message_subject"),
    url(
        r'^add-participants-to_discussion/(?P<message_id>\d+)/$',
        AddParticipantsToDiscussion.as_view(),
        name="add_participants_to_discussion"
    ),

    # Posts
    url(r'upload-post-image/$', upload_post_image, name="upload_post_image"),
    url(r'upload-post-image/(?P<user_id>\d+)/$', upload_post_image, name="upload_post_image"),
    url(r'post-member-post/(?P<post_type>[\w-]+)/$', MemberPostFormView.as_view(), name="post_member_post"),
    url(r'edit-member-post/(?P<post_id>\d+)/$', MemberPostFormView.as_view(), name="edit_member_post"),
    url(r'member-post-posted/(?P<id>\d+)/$', MemberPostPostedView.as_view(), name="member_post_posted"),
    url(r'delete-post/(?P<post_id>\d+)/$', DeletePostView.as_view(), name="delete_member_post"),

    # Community news
    url(r'community-news/$', CommunityNewsView.as_view(), name="community_news"),
    url(
        r'share-element/(?P<content_type>\d+)/(?P<object_id>\d+)/$',
        ShareElementView.as_view(),
        name="share_element"
    ),
    url(
        r'share-element/(?P<content_type>\d+)/(?P<object_id>\d+)/(?P<feed_id>\d+)/$',
        ShareElementView.as_view(),
        name="share_element"
    ),
    url(r'edit-share/(?P<feed_id>\d+)/$', EditShareElementView.as_view(), name="edit_share"),
    url(r'delete-share/(?P<feed_id>\d+)/$', DeleteShareElementView.as_view(), name="delete_share"),

    # Favorites
    url(r'favorite-members/$', FavoriteMembersView.as_view(), name="favorite_members"),
    url(r'favorite-works/$', FavoriteWorksView.as_view(), name="favorite_works"),

    # Bookmarks
    url(r'bookmark-work/(?P<work_id>\d+)/$', BookmarkWorkView.as_view(), name="bookmark_work"),

    # Documents
    url(r'upload-document-file/$', upload_document_file, name="upload_document_file"),
    url(r'user-documents-list/$', UserDocumentListView.as_view(), name="user_document_list"),
    url(r'shared-documents-list/$', SharedDocumentListView.as_view(), name="shared_document_list"),
    url(r'share-document/$', DocumentFormView.as_view(), name="share_document"),
    url(r'edit-document/(?P<document_id>\d+)/$', DocumentFormView.as_view(), name="edit_document"),
    url(r'delete-document/(?P<document_id>\d+)/$', DeleteDocumentFormView.as_view(), name="delete_document"),
    url(r'download-document/(?P<id>\d+)/$', DocumentDownloadView.as_view(), name="download_document"),

    # Notifications
    url(
        r'mark-notifications-as-read/(?P<id>\d+)/$',
        mark_notifications_as_read,
        name="mark_notifications_as_read"
    ),


]
