# -*- coding: utf-8 -*-

from datetime import datetime

import factory

from ulysses.profiles.factories import ProfileUserFactory, IndividualFactory
from ulysses.works.factories import WorkFactory

from .models import (
    MemberRecommendation, WorkRecommendation, Message, Following, MessageRecipient, MemberPost, FeedItem, Document,
    DocumentAccess, PostTag, WorkBookmark, MessageReply, MessageDigest, MemberBookmark
)


class MemberRecommendationFactory(factory.DjangoModelFactory):

    class Meta:
        model = MemberRecommendation

    member_user = None
    recommended_by = None
    recommendation_datetime = datetime.now()

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        # Get the user of IndividualFactory
        member_user = kwargs.pop('member_user', None) or IndividualFactory.create().user
        recommended_by = kwargs.pop('recommended_by', None) or IndividualFactory.create().user
        kwargs.update(
            {
                'member_user': member_user,
                'recommended_by': recommended_by,
            }
        )
        return super(MemberRecommendationFactory, cls)._create(model_class, *args, **kwargs)


class WorkRecommendationFactory(factory.DjangoModelFactory):

    class Meta:
        model = WorkRecommendation

    work = factory.SubFactory(WorkFactory)
    recommended_by = factory.SubFactory(ProfileUserFactory)
    recommendation_datetime = datetime.now()


class WorkBookmarkFactory(factory.DjangoModelFactory):

    class Meta:
        model = WorkBookmark

    work = factory.SubFactory(WorkFactory)
    member_user = factory.SubFactory(ProfileUserFactory)
    datetime = datetime.now()


class MessageFactory(factory.DjangoModelFactory):

    class Meta:
        model = Message

    sender = factory.SubFactory(ProfileUserFactory)
    sent_datetime = datetime.now()
    subject = factory.Sequence(lambda n: 'subject-{0}'.format(n))
    body = factory.Sequence(lambda n: 'body-{0}'.format(n))

    @factory.post_generation
    def recipients(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            for recipient in extracted:
                MessageRecipient.objects.create(
                    user=recipient,
                    message=self
                )

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        last_digest_datetime = kwargs.pop("last_digest_datetime", None)
        message = super(MessageFactory, cls)._create(model_class, *args, **kwargs)
        MessageRecipient.objects.create(
            user=message.sender,
            message=message
        )
        if last_digest_datetime:
            MessageDigest.objects.create(message=message, last_digest_datetime=last_digest_datetime)
        return message


class MessageReplyFactory(factory.DjangoModelFactory):

    class Meta:
        model = MessageReply

    sender = factory.SubFactory(ProfileUserFactory)
    sent_datetime = datetime.now()
    body = factory.Sequence(lambda n: 'body-{0}'.format(n))
    message = factory.SubFactory(MessageFactory)


class AttachmentFactory(factory.DjangoModelFactory):
    class Meta:
        model = MessageReply

    sender = factory.SubFactory(ProfileUserFactory)
    sent_datetime = datetime.now()
    body = ''
    message = factory.SubFactory(MessageFactory)
    attachment = factory.django.FileField()


class FollowingFactory(factory.DjangoModelFactory):

    class Meta:
        model = Following

    member_user = factory.SubFactory(ProfileUserFactory)
    follower = factory.SubFactory(ProfileUserFactory)
    follow_datetime = datetime.now()


class MemberPostFactory(factory.DjangoModelFactory):

    class Meta:
        model = MemberPost

    owner = factory.SubFactory(ProfileUserFactory)
    post_datetime = factory.Sequence(lambda n: datetime.now())
    text = factory.Sequence(lambda n: 'post-text-{0}'.format(n))
    post_type = MemberPost.COMMUNITY_NEWS_POST

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        post = super(MemberPostFactory, cls)._create(model_class, *args, **kwargs)
        FeedItemFactory.create(
            owner=post.owner,
            content_object=post,
            tag=post.post_type,
            is_original_post=True,
            feed_datetime=post.post_datetime,
            parent=None,
            is_deleted=False
        )
        return post


class FeedItemFactory(factory.DjangoModelFactory):

        class Meta:
            model = FeedItem

        owner = factory.SubFactory(ProfileUserFactory)
        feed_datetime = factory.Sequence(lambda n: datetime.now())
        content_object = factory.SubFactory(MemberPostFactory)
        tag = MemberPost.COMMUNITY_NEWS_POST

        @classmethod
        def _create(cls, model_class, *args, **kwargs):
            feed = super(FeedItemFactory, cls)._create(model_class, *args, **kwargs)
            # # MemberPost already have a FeedItem associated. Mark it as a parent
            # if feed.content_type.model_class() == MemberPost:
            #     feed.parent = feed.content_object.main_feed()
            #     feed.save()
            return feed


class DocumentFactory(factory.DjangoModelFactory):

        class Meta:
            model = Document

        owner = factory.SubFactory(ProfileUserFactory)
        datetime = datetime.now()
        description = factory.Sequence(lambda n: 'description-{0}'.format(n))
        access_type = Document.ACCESS_MEMBERS
        file = factory.django.FileField()

        @factory.post_generation
        def allowed_users(self, create, extracted, **kwargs):
            if not create:
                # Simple build, do nothing.
                return

            if extracted:
                for allowed_user in extracted:
                    DocumentAccess.objects.create(user=allowed_user, document=self, is_read=False)
                self.save()


class PostTagFactory(factory.DjangoModelFactory):
    name = factory.Sequence(lambda n: 'post-tag-{0}'.format(n))

    class Meta:
        model = PostTag


class MemberBookmarkFactory(factory.DjangoModelFactory):
    member_user = factory.SubFactory(ProfileUserFactory)
    bookmarked_by = factory.SubFactory(ProfileUserFactory)
    bookmark_datetime = factory.Sequence(lambda n: datetime.now())

    class Meta:
        model = MemberBookmark
