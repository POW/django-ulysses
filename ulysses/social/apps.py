# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.social'
    label = 'ulysses_social'
    verbose_name = "Ulysses Social"
