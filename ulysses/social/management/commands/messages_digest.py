
from datetime import datetime
from django.db import transaction
from django.db.models import Q, F
from django.core.management.base import BaseCommand
from django.utils.translation import ugettext as _

from ulysses.profiles.models import Individual, Organization
from ulysses.social.models import Message, MessageRecipient, MessageReply, MessageDigest
from ulysses.generic.emails import send_email


class Command(BaseCommand):
    """send a summary of all emails"""
    help = ""
    
    def handle(self, *args, **options):

        verbosity = options.get('verbosity', 1)

        messages_digest = Message.objects.filter(
            Q(messagedigest__last_digest_datetime__isnull=True) |
            Q(updated_datetime__isnull=False, messagedigest__last_digest_datetime__lte=F('updated_datetime')),
            is_deleted=False
        )
        last_digest_datetime = datetime.now()

        for profile_class in (Individual, Organization):

            profiles = profile_class.objects.filter(
                accept_messages_digest=True, user__is_active=True, is_enabled=True
            )

            for profile in profiles:

                as_unread_recipients = MessageRecipient.objects.filter(
                    user=profile.user, is_read=False, is_deleted=False
                )
                messages = messages_digest.filter(
                    messagerecipient__in=as_unread_recipients
                )
                all_replies = []
                for message in messages:
                    message_replies = MessageReply.objects.filter(
                        message=message, is_action=False
                    ).exclude(
                        sender=profile.user,
                    )
                    if message.last_digest_datetime:
                        message_replies = message_replies.filter(
                            sent_datetime__gte=message.last_digest_datetime
                        )
                    all_replies.extend(message_replies.distinct())

                replies_count = len(all_replies)
                if replies_count:
                    if verbosity:
                        print("send email to", profile.user.email, replies_count)
                    send_email(
                        _(u'You have {0} new message{1}'.format(
                            replies_count, 's' if replies_count > 1 else ''
                        )),
                        'emails/messages_digest.html',
                        {'replies': all_replies, 'profile': profile},
                        [profile.user.email]
                    )

                    try:
                        message_digest = MessageDigest.objects.get(message=message)
                        message_digest.last_digest_datetime = last_digest_datetime
                        message_digest.save()
                    except MessageDigest.DoesNotExist:
                        MessageDigest.objects.create(
                            message=message, last_digest_datetime=last_digest_datetime
                        )
