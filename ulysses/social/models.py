# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import datetime
import os
import os.path
import re

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _
from django.utils.http import urlquote_plus

from sorl.thumbnail import get_thumbnail

from ulysses.competitions.models import Competition
from ulysses.events.models import Event
from ulysses.generic.utils import safe_html
from ulysses.middleware import get_request
from ulysses.profiles.models import ContactsGroup
from ulysses.profiles.utils import get_profile, Individual, Organization
from ulysses.works.models import Work
from ulysses.utils import dehtml
from ulysses.web.models import FocusOn
from ulysses.web.templatetags.ulysses_utils import message_datetime

from .shortcuts import get_feed_content


def get_user_documents_directory(user_id):
    """Get the path where to store file """

    docs_directory = "docs"
    user_directory = '{0}'.format(user_id)

    folders = (docs_directory, user_directory)
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def get_user_attachments_directory(user_id):
    """Get the path where to store file """

    docs_directory = "attachments"
    user_directory = '{0}'.format(user_id)

    folders = (docs_directory, user_directory)
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def documents_directory(instance, file_name):
    """return where to upload file"""
    if instance and instance.owner:
        user_id = instance.owner.id
    else:
        request = get_request()
        user_id = request.user.id if request else 0
    user_docs_directory = get_user_documents_directory(user_id)
    return os.path.join(user_docs_directory, file_name)


def attachments_directory(instance, file_name):
    """return where to upload file"""
    if instance and instance.sender:
        user_id = instance.sender.id
    else:
        request = get_request()
        user_id = request.user.id if request else 0
    user_attachments_directory = get_user_attachments_directory(user_id)
    return os.path.join(user_attachments_directory, file_name)


class BaseRecommendation(models.Model):
    """Base class"""
    recommended_by = models.ForeignKey(User, verbose_name=_('member user'), on_delete=models.CASCADE)
    recommendation_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class MemberRecommendation(BaseRecommendation):
    """A user is recommended by another user"""
    member_user = models.ForeignKey(
        User, verbose_name=_('member user'), related_name='received_recommendations', on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('Member recommendation')
        verbose_name_plural = _('Member recommendations')
        ordering = ['-recommendation_datetime']

    def __str__(self):
        return '{0} recommended by {1}'.format(self.member_user, self.recommended_by)


class WorkRecommendation(BaseRecommendation):
    """A work is recommended by a user"""
    work = models.ForeignKey(
        Work, verbose_name=_('work'), related_name='received_recommendations', on_delete=models.CASCADE
    )

    class Meta:
        verbose_name = _('Work recommendation')
        verbose_name_plural = _('Work recommendations')
        ordering = ['-recommendation_datetime']

    def __str__(self):
        return '{0} recommended by {1}'.format(self.work, self.recommended_by)


class WorkBookmark(models.Model):
    """A work is bookmarked by a user"""
    work = models.ForeignKey(Work, verbose_name=_('work'), related_name='bookmarks', on_delete=models.CASCADE)
    member_user = models.ForeignKey(User, verbose_name=_('user'), on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Work bookmark')
        verbose_name_plural = _('Work bookmarks')
        ordering = ['-datetime']

    def __str__(self):
        return '{0} in bookmarks of {1}'.format(self.work, self.member_user)


class MessageRecipient(models.Model):
    user = models.ForeignKey(User, verbose_name=_('recipient'), on_delete=models.CASCADE)
    message = models.ForeignKey("Message", verbose_name=_('message'), on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False, verbose_name=_('is read'))
    is_deleted = models.BooleanField(default=False, verbose_name=_('is deleted'))
    exit_datetime = models.DateTimeField(
        blank=True, default=None, null=True, help_text=_('If user has left the discussion')
    )


class Message(models.Model):
    """A message between two members"""
    # If sender is NULL, the message is sent by the platform
    sender = models.ForeignKey(
        User, verbose_name=_('sender'), related_name='sent_messages', blank=True, null=True, on_delete=models.CASCADE
    )
    recipients = models.ManyToManyField(
        User, verbose_name=_('recipients'), related_name='received_messages', through=MessageRecipient
    )
    subject = models.CharField(max_length=200, blank=True, default='', verbose_name=_('subject'))
    body = models.TextField(blank=True, default='', verbose_name=_('body'))
    sent_datetime = models.DateTimeField(blank=True, default=None, null=True)
    updated_datetime = models.DateTimeField(blank=True, default=None, null=True)
    is_deleted = models.BooleanField(default=False, verbose_name=_('is deleted'))

    def get_delete_url(self):
        return reverse('social:delete_message', args=[self.id])

    def is_sent_by_ulysses(self):
        return self.sender is None

    def is_owner(self):
        return self.sender == get_request().user

    def is_participant(self):
        """is the member participating to the discussion"""
        user = get_request().user
        return MessageRecipient.objects.filter(
            user=user, message=self, is_deleted=False, exit_datetime__isnull=True
        ).exists()

    def get_recipients(self):
        request = get_request()
        message_recipients = MessageRecipient.objects.filter(
            message=self, is_deleted=False,  exit_datetime__isnull=True
        )
        if request.user:
            message_recipients = message_recipients.exclude(user=request.user)
        return [recipient.user for recipient in message_recipients]

    def participants(self):
        if self.is_group:
            message_recipients = MessageRecipient.objects.filter(
                message=self, is_deleted=False, exit_datetime__isnull=True
            )
            return sorted(
                set([recipient.user for recipient in message_recipients]),
                key=lambda user: get_profile(user).name_for_sort
            )
        return []

    def attachments(self):
        replies = self.messagereply_set.exclude(attachment='')
        return [
            {
                'filename': reply.attachment_filename,
                'sent_on': reply.sent_on,
                'is_image': os.path.splitext(reply.attachment.name)[-1] in ('.jpg', '.png'),
                'url': reverse('social:reply_attachment', args=[reply.id]),
                'delete_url': reverse('social:delete_attachment', args=[reply.id]),
            }
            for reply in replies
            if reply.attachment
        ]

    @property
    def recipients_list(self):
        recipients = [get_profile(recipient) for recipient in self.get_recipients()]
        return ', '.join([profile.name for profile in recipients if profile])

    @property
    def html_body(self):
        # Remove html code.
        return safe_html(self.body)

    def is_read(self):
        request = get_request()
        try:
            return MessageRecipient.objects.get(message=self, user=request.user).is_read
        except MessageRecipient:
            return True

    @property
    def is_group(self):
        return MessageRecipient.objects.filter(message=self).exclude(user=self.sender).count() > 1

    @property
    def reply_url(self):
        if self.sender:
            subject = urlquote_plus("Re: {0}".format(self.subject))
            return reverse('social:reply_to', args=[self.id]) + "?subject={0}".format(subject)

    def get_last_message(self):
        replies = [reply.body for reply in self.messagereply_set.all()[:1]]
        if len(replies):
            return replies[0]
        return ""

    def get_discussion_contact(self):
        if self.is_group:
            # group
            return {
                'avatar': '{0}icons/group-white.svg'.format(settings.STATIC_URL),
                'name': self.subject,
                'class': 'icon',
                'subtitle': self.recipients_list,
                'subject': self.get_last_message(),
            }
        else:
            request = get_request()
            if self.sender == request.user:
                try:
                    recipient = self.get_recipients()[0]
                    profile = get_profile(recipient)
                except IndexError:
                    profile = get_profile(self.sender)
                return {
                    'avatar': profile.get_avatar(),
                    'name': profile.name,
                    'class': 'avatar',
                    'subtitle': profile.subtitle,
                    'avatar_class': 'member-avatar ' + profile.get_avatar_class(),
                    'subject': self.get_last_message(),
                    'url': profile.get_absolute_url(),
                }
            else:
                if self.sender:
                    profile = get_profile(self.sender)
                    return {
                        'avatar': profile.get_avatar(),
                        'name': profile.name,
                        'class': 'avatar',
                        'subtitle': profile.subtitle,
                        'avatar_class': 'member-avatar ' + profile.get_avatar_class(),
                        'subject': self.get_last_message(),
                        'url': profile.get_absolute_url(),
                    }
                else:
                    return {
                        'avatar': '{0}icons/ulysses.svg'.format(settings.STATIC_URL),
                        'name': 'Ulysses',
                        'class': 'icon',
                        'subtitle': '',
                        'subject': self.get_last_message(),
                    }

    @property
    def discussion_contact(self):
        data = self.get_discussion_contact()
        return data

    @property
    def last_digest_datetime(self):
        try:
            return self.messagedigest.last_digest_datetime
        except MessageDigest.DoesNotExist:
            return None

    @last_digest_datetime.setter
    def set_last_digest_datetime(self, last_digest_datetime):
        try:
            self.messagedigest.last_digest_datetime = last_digest_datetime
            self.messagedigest.save()
        except MessageDigest.DoesNotExist:
            MessageDigest.objects.create(message=self, last_digest_datetime=last_digest_datetime)

    class Meta:
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')
        ordering = ['-sent_datetime']

    def __str__(self):
        return '{0} from {1}'.format(self.subject, self.sender)


class MessageDigest(models.Model):
    message = models.OneToOneField(Message, on_delete=models.CASCADE)
    last_digest_datetime = models.DateTimeField()


class MessageReply(models.Model):
    """A message between two members"""
    message = models.ForeignKey(Message, verbose_name=_('message'), on_delete=models.CASCADE)
    sender = models.ForeignKey(
        User, verbose_name=_('sender'), related_name='sent_replies', blank=True, null=True, on_delete=models.CASCADE
    )
    body = models.TextField(blank=True, default='', verbose_name=_('body'))
    sent_datetime = models.DateTimeField(blank=True, default=None, null=True)
    is_first = models.BooleanField(default=False)
    attachment = models.FileField(
        verbose_name=_('attachment'), upload_to=attachments_directory, max_length=200,
        blank=True, default=None, null=True
    )
    is_action = models.BooleanField(
        default=False, help_text=_('If True, this message is created automatically after action (rename group, ... )')
    )
    html_body_cache = models.TextField(blank=True, default='', verbose_name='html body')

    class Meta:
        verbose_name = _('Message reply')
        verbose_name_plural = _('Message replies')
        ordering = ['-sent_datetime']

    def __str__(self):
        return '{0} from {1}'.format(self.message.subject, self.sender)

    @property
    def is_attachment(self):
        return bool(self.attachment)

    @property
    def html_body(self):
        # Not the best choice. But it allows to keep existing messages working
        if not self.html_body_cache:
            self.html_body_cache = safe_html(
                self.body,  # text body
                allow_attachments=self.is_attachment,  # attachemets
                player=True,  # HTML player for works
                allow_links_preview=True  # Youtube and SoundCloud
            )
            self.save()
        return self.html_body_cache

    @property
    def summary(self):
        # Remove html code.
        text = dehtml(safe_html(self.body, allow_attachments=False))
        if len(text) > 60:
            text = text[:60] + "..."
        return text

    @property
    def sent_on(self):
        return message_datetime(self.sent_datetime, True)

    @property
    def is_group(self):
        return self.message.is_group

    @property
    def sent_by(self):
        request = get_request()
        profile = get_profile(self.sender)
        if profile and request and profile.user != request.user:
            return profile.name
        return ''

    @property
    def send_by_me(self):
        request = get_request()
        if request:
            return request.user == self.sender
        return False

    @property
    def attachment_filename(self):
        if self.attachment:
            filename = os.path.split(self.attachment.name)[-1]
            # Try to remove suffix
            result = re.match('.+(?P<suffix>_\d+)\.[\w\d]+$', filename)
            if result:
                suffix = str(result.group('suffix'))
                filename = filename.replace(suffix, '')
            return filename
        return ""

    def save(self, *args, **kwargs):
        if not self.id:
            self.is_first = self.message.messagereply_set.count() == 0
        return super(MessageReply, self).save(*args, **kwargs)


class Following(models.Model):
    """A member follow another one"""

    member_user = models.ForeignKey(
        User, verbose_name=_('member user'), related_name='followed_set', on_delete=models.CASCADE
    )
    follower = models.ForeignKey(
        User, verbose_name=_('follower'), related_name='follower_set', on_delete=models.CASCADE
    )
    follow_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Member following')
        verbose_name_plural = _('Member followings')
        ordering = ['-follow_datetime']

    def __str__(self):
        return '{0} followed by {1}'.format(self.member_user, self.follower)


class MemberBookmark(models.Model):
    """A member bookmarked another one"""

    member_user = models.ForeignKey(
        User, verbose_name=_('bookmarked user'), related_name='bookmarked_set', on_delete=models.CASCADE
    )
    bookmarked_by = models.ForeignKey(
        User, verbose_name=_('bookmarker'), related_name='bookmarker_set', on_delete=models.CASCADE
    )
    bookmark_datetime = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = _('Member bookmark')
        verbose_name_plural = _('Member bookmarks')
        ordering = ['-bookmark_datetime']

    def __str__(self):
        return '{0} bookmarked by {1}'.format(self.member_user, self.bookmarked_by)


def get_members_post_directory(user_id):
    """Get the path where to store file """

    posts_directory = "posts"
    user_directory = '{0}'.format(user_id)

    folders = (posts_directory, user_directory)
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def get_members_attachments_directory(user_id):
    """Get the path where to store file """

    posts_directory = "attachments"
    user_directory = '{0}'.format(user_id)

    folders = (posts_directory, user_directory)
    full_path = settings.MEDIA_ROOT
    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def members_post_directory(instance, file_name):
    """return where to upload file"""
    if instance and instance.owner:
        user_id = instance.owner.id
    else:
        request = get_request()
        user_id = request.user.id if request else 0
    user_posts_directory = get_members_post_directory(user_id)
    return os.path.join(user_posts_directory, file_name)


class PostTag(models.Model):
    """a tag which can be selected for Looking-For posts"""
    name = models.CharField(max_length=100, verbose_name=_('name'), db_index=True)
    creation_date = models.DateTimeField(auto_created=True, verbose_name=_('creation date'), default=datetime.now)

    class Meta:
        verbose_name = _('Post tag')
        verbose_name_plural = _('Post tags')
        ordering = ['name']

    @property
    def posts_count(self):
        return self.memberpost_set.filter(is_deleted=False).count()

    def __str__(self):
        return self.name


class MemberPost(models.Model):
    """A member follow another one"""

    COMMUNITY_NEWS_POST = 'community-news'
    LOOKING_FOR_POST = 'looking-for'
    POST_TYPE_CHOICE = (
        (COMMUNITY_NEWS_POST, _('Community News')),
        (LOOKING_FOR_POST, _('Looking For')),
    )

    post_type = models.CharField(
        max_length=20, choices=POST_TYPE_CHOICE, verbose_name=_('post type'), default=COMMUNITY_NEWS_POST
    )
    owner = models.ForeignKey(User, verbose_name=_('member user'), on_delete=models.CASCADE)
    post_datetime = models.DateTimeField()
    text = models.TextField(blank=False, verbose_name=_('text'))
    image = models.ImageField(
        blank=True, default=None, null=True, verbose_name=_('image'), upload_to=members_post_directory, max_length=200
    )
    iframe = models.TextField(
        blank=True, default='', verbose_name=_('iframe'),
        help_text=_('Copy the embed code of a Youtube video or Soundcloud track')
    )
    is_deleted = models.BooleanField(default=False, verbose_name=_('is deleted'))
    tags = models.ManyToManyField(PostTag, blank=True, verbose_name=_('tags'))

    @property
    def title(self):
        return self.text.replace('\n', ' ')

    @property
    def name(self):
        return self.title

    def is_souncloud(self):
        return self._check_iframe_url('https://w.soundcloud.com/player/.*')

    def is_youtube(self, url):
        return self._check_iframe_url('https://www.youtube.com/.*')

    def _check_iframe_url(self, regex):
        if self.iframe:
            soup = BeautifulSoup(self.iframe, 'html.parser')
            iframe_tags = soup.select('iframe')
            if len(iframe_tags) == 1:
                iframe_tag = iframe_tags[0]
                url = iframe_tag.get('src')
                return re.match(regex, url)
        return False

    class Meta:
        verbose_name = _('Member post')
        verbose_name_plural = _('Member posts')
        ordering = ['-post_datetime']

    def __str__(self):
        return 'posted by {0} on {1}'.format(self.owner, self.post_datetime)

    def main_feed(self):
        content_type = ContentType.objects.get_for_model(self)
        try:
            return FeedItem.objects.filter(
                owner=self.owner, content_type=content_type, object_id=self.id
            ).order_by('feed_datetime')[0]
        except IndexError:
            return None

    @property
    def html_text(self):
        return safe_html(self.text)

    def get_thumbnail(self, size="600"):
        """returns a small image"""
        if self.image:
            try:
                return get_thumbnail(self.image, size).url
            except:
                pass


class FeedItem(models.Model):
    """An information shared in the community feed"""
    NOTIFICATION = 'notification'
    SELECTION = 'selection'

    owner = models.ForeignKey(User, verbose_name=_('member user'), on_delete=models.CASCADE)
    feed_datetime = models.DateTimeField()
    update_datetime = models.DateTimeField(default=None, null=True, blank=True)

    content_type = models.ForeignKey(ContentType, db_index=True, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(db_index=True)
    content_object = GenericForeignKey('content_type', 'object_id')
    tag = models.CharField(max_length=20, default='', blank=True, verbose_name='tag', db_index=True)
    text = models.TextField(
        blank=True, default='', verbose_name=_('text'), help_text=_('Something you want to say about this post')
    )
    parent = models.ForeignKey(
        "FeedItem", blank=True, null=True, default=None, verbose_name=_("parent"), on_delete=models.CASCADE
    )
    post_text = models.TextField(blank=True, verbose_name=_('post text'), default='')
    is_original_post = models.BooleanField(default=False, verbose_name=_('is original post'))
    is_deleted = models.BooleanField(default=False, verbose_name=_('is deleted'))
    sender = models.ForeignKey(
        User, verbose_name=_('sender'), related_name='sent_notifications', default=None, blank=True, null=True,
        on_delete=models.CASCADE
    )
    is_read = models.BooleanField(default=False, verbose_name=_('is read'))
    parent_tag = models.CharField(max_length=20, default='', blank=True, verbose_name='parent tag', db_index=True)
    share_label = models.CharField(max_length=200, default='', blank=True, verbose_name='share action')

    class Meta:
        verbose_name = _('Feed')
        verbose_name_plural = _('Feeds')
        ordering = ['-update_datetime']

    def __str__(self):
        return 'shared by {0} on {1}'.format(self.owner, self.feed_datetime)

    def save(self, **kwargs):
        if not self.update_datetime:
            self.update_datetime = self.feed_datetime
        return super(FeedItem, self).save(**kwargs)

    def posted_by(self):
        owner = getattr(self.content_object, 'owner', None)
        if owner:
            return get_profile(owner)

    def show_shared_by(self):
        if self.parent:
            return True
        else:
            return self.content_type.model_class() != MemberPost

    def shared_by(self):
        return get_profile(self.owner)

    def get_content(self):
        return get_feed_content(self.content_type.id, self.object_id, short_content=False, feed_item=self)

    def short_content(self):
        return get_feed_content(self.content_type.id, self.object_id, short_content=True, feed_item=self)

    def can_be_external(self):
        return self.content_type.model_class() in (Competition, Event, Individual, Organization, FocusOn)

    def is_selection(self):
        return self.parent_tag == FeedItem.SELECTION and self.owner == self.sender

    @property
    def title(self):
        content_object_class = self.content_type.model_class()
        if content_object_class in (Competition, Event, FocusOn):
            return self.content_object.title
        elif content_object_class in (Individual, Organization, ):
            return self.content_object.name

    @property
    def html_text(self):
        return safe_html(self.text)

    @property
    def post_html_text(self):
        return safe_html(self.post_text)

    def get_absolute_url(self):
        if self.can_be_external():
            return self.content_object.get_absolute_url()

    def get_item_url(self):
        return reverse('social:community_news') + '?filter={0}#{1}'.format(self.tag or 'community-news', self.id)

    def is_memberpost(self):
        return self.content_type.model == 'memberpost'

    def post_owner(self):
        """Returns the owner of the shared post"""
        if self.is_original_post:
            return self.content_object.owner

    def repost_content_type_id(self):
        return ContentType.objects.get_for_model(FeedItem).id

    def get_reposts(self):
        """returns comments on a feed"""
        return FeedItem.objects.filter(
            is_deleted=False, content_type__id=self.repost_content_type_id(), object_id=self.id, parent=self,
        ).order_by('feed_datetime')

    @property
    def is_notification(self):
        return self.tag == FeedItem.NOTIFICATION


class Document(models.Model):
    """Document"""

    ACCESS_ALL = 1
    ACCESS_MEMBERS = 2
    ACCESS_FOLLOWERS = 3
    ACCESS_DEFINED = 4
    ACCESS_USER = 5
    ACCESS_GROUP = 6

    ACCESS_CHOICES = (
        # (ACCESS_ALL, _(u'All')),
        # (ACCESS_MEMBERS, _(u'Members')),
        (ACCESS_USER, _('Private')),
        (ACCESS_FOLLOWERS, _('My Followers')),
        (ACCESS_DEFINED, _('Selected contacts')),
        (ACCESS_GROUP, _('Contacts of group')),
    )

    owner = models.ForeignKey(User, verbose_name=_('member user'), on_delete=models.CASCADE)
    datetime = models.DateTimeField(auto_now=True)
    file = models.FileField(
        verbose_name=_('file'), upload_to=documents_directory, max_length=200
    )
    description = models.TextField(blank=True, default='', verbose_name=_('description'))
    access_type = models.IntegerField(verbose_name=_('access type'), default=ACCESS_USER, choices=ACCESS_CHOICES)
    allowed_users = models.ManyToManyField(
        User, verbose_name=_('allowed users'), blank=True, through="ulysses_social.DocumentAccess", related_name='+'
    )
    allowed_group = models.ForeignKey(
        ContactsGroup, default=None, blank=True, null=True, verbose_name=_('Allowed group'), on_delete=models.CASCADE,
        help_text=_('The members in this group will be allowed to access the document')
    )

    class Meta:
        verbose_name = _('Document')
        verbose_name_plural = _('Documents')
        ordering = ['-datetime']

    def __str__(self):
        return '{0}'.format(self.file.name)

    def get_absolute_url(self):
        return reverse('social:download_document', args=[self.id])

    def get_edit_url(self):
        return reverse('social:edit_document', args=[self.id])

    def get_delete_url(self):
        return reverse('social:delete_document', args=[self.id])

    def access_type_label(self):
        return self.get_access_type_display()

    def icon(self):
        icon = 'fichier-autre.png'

        ext = os.path.splitext(self.file.name)[-1]

        if ext in ('.xls', '.xlsx', ):
            icon = 'fichier-excel.png'

        elif ext in ('.doc', '.docx', ):
            icon = 'fichier-word.png'

        elif ext in ('.pdf', ):
            icon = 'fichier-pdf.png'

        if ext in ('.mp3', '.wav'):
            icon = 'fichier-score.png'

        return '{0}img/{1}'.format(settings.STATIC_URL, icon)

    def name(self):
        return os.path.split(self.file.name)[-1]

    def get_allowed_users(self):
        """return queryset of User allowed to access this document"""
        if self.access_type == Document.ACCESS_USER:
            return User.objects.none()

        elif self.access_type == Document.ACCESS_DEFINED:
            return self.allowed_users.all().exclude(id=self.owner.id).exclude(id=self.owner.id)

        elif self.access_type == Document.ACCESS_FOLLOWERS:
            profile = get_profile(self.owner)
            if profile:
                return User.objects.filter(follower_set__member_user=self.owner).exclude(id=self.owner.id)
            else:
                return User.objects.none()

        elif self.access_type == Document.ACCESS_GROUP:
            profile = get_profile(self.owner)
            if profile:
                return self.allowed_group.users.all().exclude(id=self.owner.id)
            else:
                return User.objects.none()

        # elif self.access_type in (Document.ACCESS_MEMBERS, Document.ACCESS_ALL):
        #     return get_active_members_queryset().exclude(id=self.owner.id)
        return User.objects.none()

    def is_allowed(self, user):
        """returns True if given user is allowed to access the document"""
        if user.is_authenticated:
            if user == self.owner:
                return True
            else:
                return self.get_allowed_users().filter(id=user.id).exists()
        else:
            return False  # self.access_type == Document.ACCESS_ALL


class DocumentAccess(models.Model):
    """Inform a user that someone shared a document with you"""
    document = models.ForeignKey(Document, verbose_name=_('document'), on_delete=models.CASCADE)
    user = models.ForeignKey(User, verbose_name=_('user'), related_name='+', on_delete=models.CASCADE)
    is_read = models.BooleanField(default=False, verbose_name=_('is read'))
    is_deleted = models.BooleanField(default=False, verbose_name=_('is deleted'))

    class Meta:
        verbose_name = _('Document access')
        verbose_name_plural = _('Document accesses')

    def __str__(self):
        return '{0} - {1}'.format(self.document, self.user)
