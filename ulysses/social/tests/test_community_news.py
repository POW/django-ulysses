# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta, time

from django.contrib.contenttypes.models import ContentType
from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ulysses.competitions.factories import CompetitionFactory
from ulysses.competitions.models import CompetitionManager
from ulysses.events.factories import EventFactory
from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.web.factories import FocusOnFactory
from ulysses.works.factories import WorkFactory

from ..factories import MemberPostFactory, FeedItemFactory, FollowingFactory
from ..models import FeedItem, MemberPost


class CommunityNewsTest(BaseTestCase):

    def test_view_anonymous(self):
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))

    def test_view_empty(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 0)

    def test_view_all_items(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        my_post = MemberPostFactory.create(owner=user)
        other_post = MemberPostFactory.create()
        url = reverse('social:community_news')
        response = self.client.get(url, data={'orders': 'everyone:1$'})
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 2)
        self.assertContains(response, my_post.text)
        self.assertContains(response, other_post.text)
        self.assertEqual(len(soup.select(".load-more")), 0)

    def test_view_all_items_default(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        my_post = MemberPostFactory.create(owner=user)
        other_post = MemberPostFactory.create()
        # works are not shown
        work1 = WorkFactory.create()
        FeedItemFactory.create(content_object=work1)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 2)
        community_news_content = str(soup.select(".community-news"))
        self.assertContains(response, my_post.text)
        self.assertContains(response, other_post.text)
        self.assertFalse(community_news_content.find(work1.title) >= 0)
        self.assertEqual(len(soup.select(".load-more")), 0)

    def test_view_following_items(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        individual2 = IndividualFactory.create()
        user2 = individual2.user
        FollowingFactory.create(member_user=user2, follower=user)
        following_post = MemberPostFactory.create(owner=user2)
        other_post = MemberPostFactory.create()
        following_work = WorkFactory.create(owner=user2)
        FeedItemFactory.create(content_object=following_work, owner=user2)
        other_work = WorkFactory.create()
        FeedItemFactory.create(content_object=other_work)
        url = reverse('social:community_news')
        response = self.client.get(url, data={'orders': 'following:1$'})
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 3)
        community_news_content = str(soup.select(".community-news"))
        self.assertContains(response, following_post.text)
        self.assertContains(response, following_work.title)
        self.assertTrue(community_news_content.find(following_work.title) >= 0)
        self.assertFalse(community_news_content.find(other_work.title) >= 0)
        self.assertNotContains(response, other_post.text)
        self.assertEqual(len(soup.select(".load-more")), 0)

    def test_view_pagination_items(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        for index in range(20):
            MemberPostFactory.create(owner=user)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 16)
        self.assertEqual(len(soup.select(".load-more")), 1)

    def test_view_pagination_items_page2(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        for index in range(20):
            member_post = MemberPostFactory.create(owner=user)
        url = reverse('social:community_news')
        response = self.client.get(url, data={'counter': 'items:32$'})
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".community-news")), 1)
        self.assertEqual(len(soup.select(".community-news .list-item")), 20)
        self.assertEqual(len(soup.select(".load-more")), 0)

    def test_view_next_events(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        future_date = datetime.now() + timedelta(days=1)
        today_date = datetime.combine(date.today(), time.max)
        past_date = datetime.now() - timedelta(days=1)
        event1 = EventFactory.create(start_date=future_date)
        event2 = EventFactory.create(start_date=today_date)
        event3 = EventFactory.create(start_date=past_date)
        event4 = EventFactory.create(start_date=None)
        event5 = EventFactory.create(start_date=past_date, end_date=future_date)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 3)
        self.assertContains(response, 'Upcoming events')
        self.assertContains(response, event1.title)
        self.assertContains(response, event2.title)
        self.assertNotContains(response, event3.title)
        self.assertNotContains(response, event4.title)
        self.assertContains(response, event5.title)

    def test_view_today_events(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        later_today = datetime.now() + timedelta(hours=1)
        earlier_today = datetime.now() - timedelta(hours=1)
        earlier_today2 = datetime.now() - timedelta(hours=2)
        event1 = EventFactory.create(start_date=later_today)
        event2 = EventFactory.create(start_date=earlier_today)
        event3 = EventFactory.create(start_date=earlier_today, end_date=later_today)
        event4 = EventFactory.create(start_date=earlier_today2, end_date=earlier_today)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 2)
        self.assertContains(response, 'Upcoming events')
        self.assertContains(response, event1.title)
        self.assertContains(response, event3.title)
        self.assertNotContains(response, event2.title)
        self.assertNotContains(response, event4.title)

    def test_view_finish_today_events(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        later_today = datetime.now() + timedelta(hours=1)
        earlier_today = datetime.now() - timedelta(hours=1)
        past_date = datetime.now() - timedelta(days=1)
        event1 = EventFactory.create(start_date=past_date, end_date=later_today)
        event2 = EventFactory.create(start_date=past_date, end_date=earlier_today)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 1)
        self.assertContains(response, 'Upcoming events')
        self.assertContains(response, event1.title)
        self.assertNotContains(response, event2.title)

    def test_view_finished_today_events(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        later_today = datetime.now() + timedelta(hours=1)
        earlier_today = datetime.now() - timedelta(hours=1)
        past_date = datetime.now() - timedelta(days=1)
        event1 = EventFactory.create(start_date=past_date, end_date=earlier_today)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 0)
        self.assertContains(response, 'Upcoming events')

    def test_view_all_types(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        work1 = WorkFactory.create()
        FeedItemFactory.create(content_object=work1)
        future_date = datetime.now() + timedelta(days=1)
        event1 = EventFactory.create(start_date=future_date)  # create a future event for the upcoming events
        event2 = EventFactory.create(start_date=None)  # shared -> should be in timeline and not in the upcoming events
        FeedItemFactory.create(content_object=event2)
        other_post = MemberPostFactory.create()
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 1)
        self.assertContains(response, work1.title)
        self.assertContains(response, other_post.text)
        self.assertContains(response, event2.title)

    def test_view_no_next_events(self):
        # Create a active user
        individual = IndividualFactory.create()
        user = individual.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        past_date = datetime.now() - timedelta(days=1)
        event1 = EventFactory.create(start_date=past_date)
        event2 = EventFactory.create(start_date=None)
        url = reverse('social:community_news')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".upcoming-events")), 1)
        self.assertEqual(len(soup.select(".upcoming-events .event-summary")), 0)
        self.assertNotContains(response, event1.title)
        self.assertNotContains(response, event2.title)


class ShareElementTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def test_view_member_share_own_element(self):
        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))
        member_post = MemberPostFactory.create(owner=user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = member_post.main_feed()
        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_content_type")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.content_type.id), tag.get('value', ''))
        tags = soup.select("input#id_object_id")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.object_id), tag.get('value', ''))
        tags = soup.select("input#id_tag")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(MemberPost.COMMUNITY_NEWS_POST, tag.get('value', ''))
        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action'))
        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_member_share_other_element(self):
        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_content_type")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.content_type.id), tag.get('value', ''))

        tags = soup.select("input#id_object_id")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.object_id), tag.get('value', ''))

        tags = soup.select("input#id_tag")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(MemberPost.COMMUNITY_NEWS_POST, tag.get('value', ''))

        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_member_share_looking_for(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_content_type")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.content_type.id), tag.get('value', ''))

        tags = soup.select("input#id_object_id")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.object_id), tag.get('value', ''))

        tags = soup.select("input#id_tag")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(MemberPost.LOOKING_FOR_POST, tag.get('value', ''))

        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_member_share_parent(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id, feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_content_type")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.content_type.id), tag.get('value', ''))

        tags = soup.select("input#id_object_id")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.object_id), tag.get('value', ''))

        tags = soup.select("input#id_tag")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(MemberPost.LOOKING_FOR_POST, tag.get('value', ''))

        tags = soup.select("input#id_parent")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('hidden', tag.get('type', ''))
        self.assertEqual(str(feed_item.id), tag.get('value', ''))

        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_member_share_element_missing(self):
        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user)
        feed_item = FeedItemFactory.create(owner=user, content_object=member_post)

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id + 1])
        response = self.client.get(url)
        self.assertEqual(404, response.status_code)

    def test_view_share_anonymous(self):
        member_post = MemberPostFactory.create(post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_share_own_post(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(2, share_qs.count())

        self.assertEqual(1, share_qs.exclude(id=feed_item.id).count())
        new_feed_item = share_qs.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(new_feed_item.text, "")
        self.assertEqual(new_feed_item.parent, None)

    def test_share_other_post(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=profile2.user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        self.assertEqual(2, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).count())

        self.assertEqual(1, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).exclude(id=feed_item.id).count())
        new_feed_item = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.sender, None)
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(new_feed_item.text, "")
        self.assertEqual(new_feed_item.parent, None)

    def test_share_looking_for(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.LOOKING_FOR_POST,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        notif_qs = FeedItem.objects.filter(tag=FeedItem.NOTIFICATION)
        self.assertEqual(0, notif_qs.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(2, share_qs.count())

        self.assertEqual(1, share_qs.exclude(id=feed_item.id).count())
        new_feed_item = share_qs.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.LOOKING_FOR_POST)
        self.assertEqual(new_feed_item.text, "")
        self.assertEqual(new_feed_item.parent, None)

    def test_share_looking_for_text(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.LOOKING_FOR_POST,
            'text': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        notif_qs = FeedItem.objects.filter(tag=FeedItem.NOTIFICATION)
        self.assertEqual(0, notif_qs.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(2, share_qs.count())

        self.assertEqual(1, share_qs.exclude(id=feed_item.id).count())
        new_feed_item = share_qs.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.LOOKING_FOR_POST)
        self.assertEqual(new_feed_item.text, 'Hello')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_post_text(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Cool!',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        notif_qs = FeedItem.objects.filter(tag=FeedItem.NOTIFICATION)
        self.assertEqual(0, notif_qs.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(2, share_qs.count())

        self.assertEqual(1, share_qs.exclude(id=feed_item.id).count())
        new_feed_item = share_qs.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(new_feed_item.text, 'Cool!')
        self.assertEqual(new_feed_item.parent, None)
        self.assertEqual(new_feed_item.post_text, member_post.text)

    def test_share_post_parent(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Cool!',
            'parent': feed_item.id,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())

        notif_qs = FeedItem.objects.filter(tag=FeedItem.NOTIFICATION)
        self.assertEqual(0, notif_qs.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(2, share_qs.count())

        self.assertEqual(1, share_qs.exclude(id=feed_item.id).count())
        new_feed_item = share_qs.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(new_feed_item.text, 'Cool!')
        self.assertEqual(new_feed_item.parent, feed_item)
        self.assertEqual(new_feed_item.post_text, member_post.text)

    def test_share_anonymous(self):

        profile1 = IndividualFactory.create()
        user = profile1.user

        member_post = MemberPostFactory.create(owner=user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': '',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_share_event(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        event = EventFactory.create()
        content_type = ContentType.objects.get_for_model(event)

        url = reverse('social:share_element', args=[content_type.id, event.id])

        data = {
            'content_type': content_type.id,
            'object_id': event.id,
            'text': 'Nice event!',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(1, share_qs.count())
        new_feed_item = share_qs[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, event)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Nice event!')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_work(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        work = WorkFactory.create()
        content_type = ContentType.objects.get_for_model(work)

        url = reverse('social:share_element', args=[content_type.id, work.id])

        data = {
            'content_type': content_type.id,
            'object_id': work.id,
            'text': 'Good work!',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())

        share_qs = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(1, share_qs.count())
        new_feed_item = share_qs[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, work)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Good work!')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_own_work(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        work = WorkFactory.create(owner=profile1.user)
        content_type = ContentType.objects.get_for_model(work)

        url = reverse('social:share_element', args=[content_type.id, work.id])

        data = {
            'content_type': content_type.id,
            'object_id': work.id,
            'text': 'Good work!',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())

        self.assertEqual(1, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).count())
        new_feed_item = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, work)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Good work!')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_focus_on(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        focus_on = FocusOnFactory.create()
        content_type = ContentType.objects.get_for_model(focus_on)

        url = reverse('social:share_element', args=[content_type.id, focus_on.id])

        data = {
            'content_type': content_type.id,
            'object_id': focus_on.id,
            'text': 'Keep focus!',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())
        new_feed_item = FeedItem.objects.all()[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, focus_on)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Keep focus!')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_post_iframe(self):

        profile1 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        member_post = MemberPostFactory.create(owner=user)
        feed_item = member_post.main_feed()

        url = reverse('social:share_element', args=[feed_item.content_type.id, feed_item.object_id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Cool!',
            'iframe': '',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        self.assertEqual(1, FeedItem.objects.exclude(id=feed_item.id).count())
        new_feed_item = FeedItem.objects.exclude(id=feed_item.id)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, member_post)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(new_feed_item.text, 'Cool!')
        self.assertEqual(new_feed_item.parent, None)
        self.assertEqual(new_feed_item.post_text, member_post.text)

    def test_share_individual(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        content_type = ContentType.objects.get_for_model(profile2)

        url = reverse('social:share_element', args=[content_type.id, profile2.id])

        data = {
            'content_type': content_type.id,
            'object_id': profile2.id,
            'text': 'Best profile',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).count())
        new_feed_item = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, profile2)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Best profile')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_organization(self):

        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        content_type = ContentType.objects.get_for_model(profile2)

        url = reverse('social:share_element', args=[content_type.id, profile2.id])

        data = {
            'content_type': content_type.id,
            'object_id': profile2.id,
            'text': 'Best profile',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).count())
        new_feed_item = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, profile2)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'Best profile')
        self.assertEqual(new_feed_item.parent, None)

    def test_share_competition(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        manager1 = CompetitionManager.objects.create(user=profile1.user)
        manager2 = CompetitionManager.objects.create(user=profile2.user)

        competition = CompetitionFactory.create(open=True, title='Abcd')
        competition.managed_by.add(manager1, manager2)
        competition.save()

        user = profile1.user
        self.assertTrue(self.client.login(email=user.email, password="1234"))

        content_type = ContentType.objects.get_for_model(competition)

        url = reverse('social:share_element', args=[content_type.id, competition.id])

        data = {
            'content_type': content_type.id,
            'object_id': competition.id,
            'text': 'call me',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(0, MemberPost.objects.count())

        feed_item_queryset = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION)
        self.assertEqual(1, feed_item_queryset.count())
        new_feed_item = feed_item_queryset[0]
        self.assertEqual(new_feed_item.owner, user)
        self.assertEqual(new_feed_item.content_object, competition)
        self.assertEqual(new_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(new_feed_item.tag, '')
        self.assertEqual(new_feed_item.text, 'call me')
        self.assertEqual(new_feed_item.parent, None)

class EditFeedItemTest(BaseTestCase):

    def test_view_edit_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='look at this')
        url = reverse('social:edit_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')

        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(feed_item.text, tag.text)

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(feed_item.text, updated_feed.text)

    def test_view_edit_looking_for(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)

        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='look at this')

        url = reverse('social:edit_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')

        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(feed_item.text, tag.text)

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(feed_item.text, updated_feed.text)

    def test_view_edit_share_of_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='Look at this')
        url = reverse('social:edit_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(feed_item.text, tag.text)
        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action'))
        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(feed_item.text, updated_feed.text)

    def test_view_edit_anonymous(self):
        profile1 = IndividualFactory.create()
        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='Look at this')
        url = reverse('social:edit_share', args=[feed_item.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

    def test_view_edit_shared_by_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile2.user, content_object=post, text='Look at this')

        url = reverse('social:edit_share', args=[feed_item.id])

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_edit_community_news(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:edit_share', args=[feed_item.id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Updated text'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]
        self.assertEqual(updated_feed_item.owner, profile1.user)
        self.assertEqual(updated_feed_item.content_object, feed_item.content_object)
        self.assertEqual(updated_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(updated_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(updated_feed_item.text, data['text'])
        self.assertEqual(updated_feed_item.parent, feed_item.parent)

    def test_edit_share_of_other_post(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:edit_share', args=[feed_item.id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Updated text'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).count())
        share_queryset = FeedItem.objects.exclude(tag=FeedItem.NOTIFICATION).exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]
        self.assertEqual(updated_feed_item.owner, profile1.user)
        self.assertEqual(updated_feed_item.content_object, feed_item.content_object)
        self.assertEqual(updated_feed_item.feed_datetime.date(), date.today())
        self.assertEqual(updated_feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(updated_feed_item.text, data['text'])
        self.assertEqual(updated_feed_item.parent, feed_item.parent)

    def test_edit_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:edit_share', args=[feed_item.id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Updated text'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]

        self.assertEqual(feed_item.id, updated_feed_item.id)

        self.assertNotEqual(updated_feed_item.text, data['text'])
        self.assertEqual(updated_feed_item.text, feed_item.text)

    def test_edit_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile2.user, content_object=post)

        url = reverse('social:edit_share', args=[feed_item.id])

        data = {
            'content_type': feed_item.content_type.id,
            'object_id': feed_item.object_id,
            'tag': MemberPost.COMMUNITY_NEWS_POST,
            'text': 'Updated text'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(403, response.status_code)

        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]

        self.assertEqual(feed_item.id, updated_feed_item.id)

        self.assertNotEqual(updated_feed_item.text, data['text'])
        self.assertEqual(updated_feed_item.text, feed_item.text)


class DeleteFeedItemTest(BaseTestCase):

    def test_view_delete_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='look at this')
        url = reverse('social:delete_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(updated_feed.is_deleted, False)

    def test_view_delete_looking_for(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)

        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='look at this')

        url = reverse('social:delete_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(feed_item.text, updated_feed.text)
        self.assertEqual(updated_feed.is_deleted, False)

    def test_view_delete_share_of_other(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='Look at this')

        url = reverse('social:delete_share', args=[feed_item.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(updated_feed.is_deleted, False)

    def test_view_edit_anonymous(self):
        profile1 = IndividualFactory.create()

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post, text='Look at this')

        url = reverse('social:delete_share', args=[feed_item.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(updated_feed.is_deleted, False)

    def test_view_delete_shared_by_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.LOOKING_FOR_POST)
        feed_item = FeedItemFactory.create(owner=profile2.user, content_object=post, text='Look at this')

        url = reverse('social:delete_share', args=[feed_item.id])

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        updated_feed = FeedItem.objects.get(id=feed_item.id)
        self.assertEqual(updated_feed.is_deleted, False)

    def test_delete_community_news(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:delete_share', args=[feed_item.id])

        data = {
            'confirm': 'confirm'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]
        self.assertEqual(updated_feed_item.is_deleted, True)

    def test_delete_share_of_other_post(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:delete_share', args=[feed_item.id])

        data = {
            'confirm': 'confirm'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('social:community_news'))

        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]
        self.assertEqual(updated_feed_item.is_deleted, True)

    def test_delete_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        post = MemberPostFactory.create(owner=profile2.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile1.user, content_object=post)

        url = reverse('social:delete_share', args=[feed_item.id])

        data = {
            'confirm': 'confirm'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(200, response.status_code)

        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]

        self.assertEqual(updated_feed_item.is_deleted, False)

    def test_delete_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        post = MemberPostFactory.create(owner=profile1.user, post_type=MemberPost.COMMUNITY_NEWS_POST)
        feed_item = FeedItemFactory.create(owner=profile2.user, content_object=post)

        url = reverse('social:delete_share', args=[feed_item.id])

        data = {
            'confirm': 'confirm'
        }
        response = self.client.post(url, data=data)

        self.assertEqual(403, response.status_code)

        self.assertEqual(2, FeedItem.objects.count())
        share_queryset = FeedItem.objects.exclude(id=post.main_feed().id)
        self.assertEqual(1, share_queryset.count())
        updated_feed_item = share_queryset[0]

        self.assertEqual(feed_item.id, updated_feed_item.id)

        self.assertEqual(updated_feed_item.is_deleted, False)
