# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ulysses.profiles.factories import IndividualFactory
from ulysses.works.factories import WorkFactory

from ..factories import WorkBookmarkFactory
from ..models import WorkBookmark, FeedItem


class WorkBookmarkTest(BaseTestCase):

    def test_view_bookmark_work(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, work.title)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag['action'])
        tags = soup.select("input#id_work")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("hidden", tag['type'])
        self.assertEqual(str(work.id), tag['value'])
        self.assertEqual(0, WorkBookmark.objects.count())

    def test_bookmark_work(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.post(url, data={'work': work.id})
        assert_popup_redirects(response, work.get_absolute_url())
        self.assertEqual(1, WorkBookmark.objects.count())
        bookmark = WorkBookmark.objects.all()[0]
        self.assertEqual(bookmark.work, work)
        self.assertEqual(bookmark.member_user, profile1.user)
        self.assertEqual(bookmark.datetime.date(), date.today())

    def test_bookmark_own_work(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create(owner=profile1.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.post(url, data={'work': work.id})
        assert_popup_redirects(response, work.get_absolute_url())
        self.assertEqual(1, WorkBookmark.objects.count())
        bookmark = WorkBookmark.objects.all()[0]
        self.assertEqual(bookmark.work, work)
        self.assertEqual(bookmark.member_user, profile1.user)
        self.assertEqual(bookmark.datetime.date(), date.today())

    def test_bookmark_work_composer(self):
        profile1 = IndividualFactory.create()
        composer_profile = IndividualFactory.create()
        work = WorkFactory.create(composer=composer_profile.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.post(url, data={'work': work.id})
        assert_popup_redirects(response, work.get_absolute_url())
        self.assertEqual(1, WorkBookmark.objects.count())
        bookmark = WorkBookmark.objects.all()[0]
        self.assertEqual(bookmark.work, work)
        self.assertEqual(bookmark.member_user, profile1.user)
        self.assertEqual(bookmark.datetime.date(), date.today())

    def test_bookmark_as_anonymous(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create()
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.post(url, data={'work': work.id})
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(0, WorkBookmark.objects.count())

    def test_bookmark_unknown(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[222])
        response = self.client.post(url, data={'work': 222})
        self.assertEqual(response.status_code, 404)
        self.assertEqual(0, WorkBookmark.objects.count())

    def test_bookmark_existing(self):
        profile1 = IndividualFactory.create()
        work = WorkFactory.create()
        WorkBookmarkFactory.create(work=work, member_user=profile1.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:bookmark_work', args=[work.id])
        response = self.client.post(url, data={'work': work.id})
        assert_popup_redirects(response, work.get_absolute_url())
        self.assertEqual(0, WorkBookmark.objects.count())
