# -*- coding: utf-8 -*-

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.works.factories import WorkFactory

from ..factories import WorkBookmarkFactory
from ..models import Following


class FavoriteMembersTest(APITestCase):

    def test_get_favorite_members_empty(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_members')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_get_favorite_members(self):

        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        profile4 = IndividualFactory.create()
        IndividualFactory.create()

        Following.objects.create(member_user=profile2.user, follower=profile1.user)
        Following.objects.create(member_user=profile1.user, follower=profile2.user)
        Following.objects.create(member_user=profile3.user, follower=profile1.user)
        Following.objects.create(member_user=profile1.user, follower=profile4.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_members')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

        results = response.data['results']
        self.assertEqual(len(results), 2)

        followed_profiles = [profile3, profile2]

        for (profile, elt) in zip(followed_profiles, results):
            self.assertEqual(profile.user.id, elt['id'])
            self.assertEqual(profile.name, elt['name'])
            self.assertEqual(profile.get_absolute_url(), elt['url'])
            self.assertEqual(profile.get_avatar(), elt['avatar'])

    def test_get_favorite_members_anonymous(self):
        profile1 = IndividualFactory.create()

        url = reverse('social:favorite_members')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_favorite_members_pagination(self):

        profile1 = IndividualFactory.create()
        for index in range(11):
            profile2 = IndividualFactory.create()
            Following.objects.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_members')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 11)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(
            response.data['next'],
            'http://testserver{0}?page=2'.format(reverse('social:favorite_members'))
        )

        results = response.data['results']

        self.assertEqual(len(results), 10)

    def test_get_favorite_members_pagination_page2(self):

        profile1 = IndividualFactory.create()
        for index in range(11):
            profile2 = IndividualFactory.create()
            Following.objects.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_members') + "?page=2"

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 11)
        self.assertEqual(
            response.data['previous'],
            'http://testserver{0}'.format(reverse('social:favorite_members'))
        )
        self.assertEqual(response.data['next'], None)

        results = response.data['results']

        self.assertEqual(len(results), 1)


class FavoriteWorksTest(APITestCase):

    def test_get_favorite_works_empty(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_works')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_get_favorite_works(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        work1 = WorkFactory.create()
        work2 = WorkFactory.create()
        work3 = WorkFactory.create()
        work4 = WorkFactory.create()

        WorkBookmarkFactory.create(work=work1, member_user=profile1.user)
        WorkBookmarkFactory.create(work=work1, member_user=profile2.user)
        WorkBookmarkFactory.create(work=work2, member_user=profile1.user)
        WorkBookmarkFactory.create(work=work3, member_user=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_works')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

        results = response.data['results']
        self.assertEqual(len(results), 2)

        followed_works = [work2, work1]

        for (work, elt) in zip(followed_works, results):
            self.assertEqual(work.id, elt['id'])
            self.assertEqual(work.title, elt['title'])
            self.assertEqual(work.get_absolute_url(), elt['url'])
            self.assertEqual(work.get_thumbnail(), elt['thumbnail'])

    def test_get_favorite_works_anonymous(self):

        url = reverse('social:favorite_works')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)

    def test_get_favorite_works_pagination(self):

        profile1 = IndividualFactory.create()
        for index in range(11):
            work = WorkFactory.create()
            WorkBookmarkFactory.create(work=work, member_user=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_works')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 11)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(
            response.data['next'],
            'http://testserver{0}?page=2'.format(reverse('social:favorite_works'))
        )

        results = response.data['results']

        self.assertEqual(len(results), 10)

    def test_get_favorite_works_pagination_page2(self):

        profile1 = IndividualFactory.create()
        for index in range(11):
            work = WorkFactory.create()
            WorkBookmarkFactory.create(work=work, member_user=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:favorite_works') + "?page=2"

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 11)
        self.assertEqual(
            response.data['previous'],
            'http://testserver{0}'.format(reverse('social:favorite_works'))
        )
        self.assertEqual(response.data['next'], None)

        results = response.data['results']

        self.assertEqual(len(results), 1)
