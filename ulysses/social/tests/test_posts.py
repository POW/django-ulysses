# -*- coding: utf-8 -*-

from datetime import date

from bs4 import BeautifulSoup

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects
from ulysses.profiles.factories import IndividualFactory

from ..models import MemberPost, FeedItem, PostTag
from ..factories import MemberPostFactory, PostTagFactory


class PostMemberPostTest(BaseTestCase):

    def test_view_member_post_community_news(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('', tag.text)
        tags = soup.select("textarea#id_iframe")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('', tag.text)
        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(0, FeedItem.objects.count())

    def test_view_compose_anonymous(self):
        url = reverse('social:post_member_post', args=['community-news'])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(0, FeedItem.objects.count())

    def test_post_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'text': 'How are you?',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertEqual(member_post.iframe, '')
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_post_message_anonymous(self):
        profile1 = IndividualFactory.create()
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'text': 'How are you?',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(0, FeedItem.objects.count())
        assert_popup_redirects(response, reverse('login'))

    def test_post_missing_text(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'text': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(0, FeedItem.objects.count())
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('ul.errorlist li')))

    def test_post_community_news_youtube(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'text': 'How are you?',
            'iframe': '<div style="position:relative;height:0;padding-bottom:56.25%">'
                      '<iframe src="https://www.youtube.com/embed/gl3manXIeBI?ecver=2" width="640" height="360"'
                      ' frameborder="0" style="position:absolute;width:100%;height:100%;left:0" allowfullscreen>'
                      '</iframe></div>'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertNotEqual(member_post.iframe, '')
        soup = BeautifulSoup(member_post.iframe, 'html.parser')
        self.assertEqual(1, len(soup.select('iframe[src=https://www.youtube.com/embed/gl3manXIeBI?ecver=2]')))
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_post_community_news_soundcloud(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'text': 'How are you?',
            'iframe': '<iframe width="100%" height="450" scrolling="no" frameborder="no" '
                      'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/290414336&amp;'
                      'auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;'
                      'show_reposts=false&amp;visual=true"></iframe>'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertNotEqual(member_post.iframe, '')
        soup = BeautifulSoup(member_post.iframe, 'html.parser')
        self.assertEqual(1, len(soup.select('iframe')))
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_post_community_my_iframe(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        data = {
            'post_type': 'community-news',
            'text': 'How are you?',
            'iframe': '<iframe width="100%" height="450" scrolling="no" frameborder="no" '
                      'src="http://blabla.com"></iframe>'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(0, MemberPost.objects.count())
        self.assertEqual(0, FeedItem.objects.count())
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('ul.errorlist li')))

    def test_post_community_tags(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_member_post', args=['community-news'])
        PostTagFactory.create(name='Hello')
        PostTagFactory.create(name='RED')
        data = {
            'text': 'How are you?',
            'tags': 'News Hello Red'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertEqual(member_post.iframe, '')
        self.assertEqual(member_post.tags.count(), 3)
        self.assertEqual(PostTag.objects.filter(name="News").count(), 1)
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())


class EditMemberPostTest(BaseTestCase):

    def test_view_edit_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:edit_member_post', args=[post.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(reverse('social:edit_member_post', args=[post.id]), tag.get('action'))
        tags = soup.select("textarea#id_text")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(post.text, tag.text)
        tags = soup.select("textarea#id_iframe")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('', tag.text)
        self.assertEqual(1, MemberPost.objects.count())
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_edit_post_anonymous(self):
        profile1 = IndividualFactory.create()
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:edit_member_post', args=[post.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

    def test_view_edit_post_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile2.user)
        url = reverse('social:edit_member_post', args=[post.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_edit_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:edit_member_post', args=[post.id])
        data = {
            'text': 'How are you?',
            'iframe': '<iframe width="100%" height="450" scrolling="no" frameborder="no" '
                      'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/290414336&amp;'
                      'auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;'
                      'show_reposts=false&amp;visual=true"></iframe>'
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertNotEqual(member_post.iframe, '')
        soup = BeautifulSoup(member_post.iframe, 'html.parser')
        self.assertEqual(1, len(soup.select('iframe')))
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_edit_tags(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user)
        tag1 = PostTagFactory.create(name="Hello")
        tag2 = PostTagFactory.create(name="Word")
        tag3 = PostTagFactory.create(name="Submarine")
        post.tags.add(tag1, tag2)
        url = reverse('social:edit_member_post', args=[post.id])
        data = {
            'text': 'How are you?',
            'iframe': '',
            'tags': 'Hello Yellow Submarine',
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.text, data['text'])
        self.assertEqual(member_post.post_datetime.date(), date.today())
        self.assertEqual(member_post.iframe, '')
        self.assertEqual(member_post.tags.count(), 3)
        for word in data['tags'].split(' '):
            self.assertEqual(member_post.tags.filter(name=word).count(), 1)
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_edit_anonymous(self):
        profile1 = IndividualFactory.create()
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:edit_member_post', args=[post.id])
        data = {
            'text': 'How are you?',
            'iframe': '<iframe width="100%" height="450" scrolling="no" frameborder="no" '
                      'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/290414336&amp;'
                      'auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;'
                      'show_reposts=false&amp;visual=true"></iframe>'
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertNotEqual(member_post.text, data['text'])
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())

    def test_edit_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile2.user)
        url = reverse('social:edit_member_post', args=[post.id])
        data = {
            'post_type': 'community-news',
            'text': 'How are you?',
            'iframe': '<iframe width="100%" height="450" scrolling="no" frameborder="no" '
                      'src="https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/290414336&amp;'
                      'auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;'
                      'show_reposts=false&amp;visual=true"></iframe>'
        }
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile2.user)
        self.assertNotEqual(member_post.text, data['text'])
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile2.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())


class DeleteMemberPostTest(BaseTestCase):

    def test_delete_member_post(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:delete_member_post', args=[post.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.is_deleted, False)
        self.assertEqual(1, FeedItem.objects.count())

    def test_view_delete_post_anonymous(self):
        profile1 = IndividualFactory.create()
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:delete_member_post', args=[post.id])
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.is_deleted, False)

    def test_view_delete_post_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile2.user)
        url = reverse('social:delete_member_post', args=[post.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.is_deleted, False)

    def test_delete_community_news(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:delete_member_post', args=[post.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('social:community_news'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.is_deleted, True)
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())
        self.assertEqual(feed_item.is_deleted, True)

    def test_delete_anonymous(self):
        profile1 = IndividualFactory.create()
        post = MemberPostFactory.create(owner=profile1.user)
        url = reverse('social:delete_member_post', args=[post.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile1.user)
        self.assertEqual(member_post.is_deleted, False)
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile1.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())
        self.assertEqual(feed_item.is_deleted, False)

    def test_delete_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        post = MemberPostFactory.create(owner=profile2.user)
        url = reverse('social:delete_member_post', args=[post.id])
        data = {
            'confirm': 'confirm',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(403, response.status_code)
        self.assertEqual(1, MemberPost.objects.count())
        member_post = MemberPost.objects.all()[0]
        self.assertEqual(member_post.id, post.id)
        self.assertEqual(member_post.post_type, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(member_post.owner, profile2.user)
        self.assertEqual(member_post.is_deleted, False)
        self.assertEqual(1, FeedItem.objects.count())
        feed_item = FeedItem.objects.all()[0]
        self.assertEqual(feed_item.owner, profile2.user)
        self.assertEqual(feed_item.content_object, member_post)
        self.assertEqual(feed_item.tag, MemberPost.COMMUNITY_NEWS_POST)
        self.assertEqual(feed_item.feed_datetime.date(), date.today())
        self.assertEqual(feed_item.is_deleted, False)
