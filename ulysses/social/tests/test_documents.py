# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date
from os.path import abspath, dirname, join as join_path, split as split_path

from django.test.utils import override_settings

from rest_framework import status
from rest_framework.reverse import reverse

from ulysses.generic.tests import BaseTestCase, BaseAPITestCase, assert_popup_redirects

from ulysses.profiles.factories import IndividualFactory
from ulysses.profiles.models import Individual, ContactsGroup

from ..factories import DocumentFactory, FollowingFactory
from ..models import Document, DocumentAccess


class UserDocumentTest(BaseAPITestCase):

    def test_read_documents_empty(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:user_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_read_documents(self):

        profile1 = IndividualFactory.create()

        document1 = DocumentFactory.create(owner=profile1.user)
        document2 = DocumentFactory.create(owner=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:user_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        results = response.data['results']

        self.assertEqual(len(results), 2)

        self.assertEqual([document2.id, document1.id], [elt['id'] for elt in results])

        for (document, elt) in zip([document2, document1], results):
            self.assertEqual(document.id, elt['id'])
            self.assertEqual(document.description, elt['description'])
            self.assertEqual(False, elt['is_read'])
            self.assertEqual(document.get_absolute_url(), elt['get_absolute_url'])
            self.assertEqual(document.get_delete_url(), elt['get_delete_url'])
            self.assertEqual(document.get_edit_url(), elt['get_edit_url'])

    def test_read_documents_anonymous(self):
        profile1 = IndividualFactory.create()

        url = reverse('social:user_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)


class SharedUserDocumentTest(BaseAPITestCase):

    def test_read_documents_empty(self):

        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:shared_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_read_documents(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group1 = ContactsGroup.objects.create(name='A', owner=profile2.user)
        group1.users.add(profile1.user)
        group1.save()

        group2 = ContactsGroup.objects.create(name='B', owner=profile2.user)

        in_document2 = DocumentFactory.create(
            access_type=Document.ACCESS_GROUP, allowed_group=group1, owner=profile2.user
        )
        in_document3 = DocumentFactory.create(access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user])
        in_document4 = DocumentFactory.create(access_type=Document.ACCESS_FOLLOWERS)

        DocumentAccess.objects.create(document=in_document2, user=profile1.user, is_read=True)

        FollowingFactory.create(member_user=in_document4.owner, follower=profile1.user)

        out_document1 = DocumentFactory.create(access_type=Document.ACCESS_DEFINED)
        out_document2 = DocumentFactory.create(access_type=Document.ACCESS_FOLLOWERS)
        out_document3 = DocumentFactory.create(access_type=Document.ACCESS_USER)
        out_document4 = DocumentFactory.create(
            access_type=Document.ACCESS_GROUP, allowed_group=group2, owner=profile2.user
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:shared_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        results = response.data['results']

        self.assertEqual(len(results), 3)

        self.assertEqual(
            [in_document4.id, in_document3.id, in_document2.id, ],
            [elt['id'] for elt in results]
        )

        for (document, elt) in zip([in_document4, in_document3, in_document2, ], results):
            self.assertEqual(document.id, elt['id'])
            self.assertEqual(document.description, elt['description'])
            self.assertEqual(document == in_document2, elt['is_read'])
            self.assertEqual(document.get_absolute_url(), elt['get_absolute_url'])
            self.assertEqual(document.get_delete_url(), elt['get_delete_url'])
            self.assertEqual(None, elt['get_edit_url'])

    def test_read_deleted_documents(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group1 = ContactsGroup.objects.create(name='A', owner=profile2.user)
        group1.users.add(profile1.user)
        group1.save()

        group2 = ContactsGroup.objects.create(name='B', owner=profile2.user)

        in_document2 = DocumentFactory.create(
            access_type=Document.ACCESS_GROUP, allowed_group=group1, owner=profile2.user
        )
        in_document3 = DocumentFactory.create(access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user])
        in_document4 = DocumentFactory.create(access_type=Document.ACCESS_FOLLOWERS)

        DocumentAccess.objects.create(document=in_document2, user=profile1.user, is_read=True)

        FollowingFactory.create(member_user=in_document4.owner, follower=profile1.user)

        out_document1 = DocumentFactory.create(access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user])
        out_document2 = DocumentFactory.create(access_type=Document.ACCESS_FOLLOWERS)
        out_document3 = DocumentFactory.create(access_type=Document.ACCESS_USER)
        out_document4 = DocumentFactory.create(
            access_type=Document.ACCESS_GROUP, allowed_group=group2, owner=profile2.user
        )

        doc_access = DocumentAccess.objects.get(document=out_document1, user=profile1.user)
        doc_access.is_deleted = True
        doc_access.save()

        FollowingFactory.create(member_user=out_document2.owner, follower=profile1.user)
        DocumentAccess.objects.create(document=out_document2, user=profile1.user, is_deleted=True)
        DocumentAccess.objects.create(document=out_document3, user=profile1.user, is_deleted=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:shared_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        results = response.data['results']

        self.assertEqual(len(results), 3)

        self.assertEqual(
            [in_document4.id, in_document3.id, in_document2.id],
            [elt['id'] for elt in results]
        )

        for (document, elt) in zip([in_document4, in_document3, in_document2,], results):
            self.assertEqual(document.id, elt['id'])
            self.assertEqual(document.description, elt['description'])
            self.assertEqual(document == in_document2, elt['is_read'])
            self.assertEqual(document.get_absolute_url(), elt['get_absolute_url'])

    def test_shared_document_anonymous(self):
        profile1 = IndividualFactory.create()

        url = reverse('social:shared_document_list')

        response = self.client.get(url)
        self.assertEqual(status.HTTP_403_FORBIDDEN, response.status_code)


class ShareDocumentTest(BaseTestCase):

    def test_view_share_document(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_allowed_users")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_file_media")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual('', tag.get('value', ''))

        tags = soup.select("textarea#id_description")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual('', tag.get('value', ''))

        self.assertEqual(0, Document.objects.count())

    def test_view_share_document_anonymous(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        url = reverse('social:share_document')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(0, Document.objects.count())

    def test_view_edit_document(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        document = DocumentFactory.create(
            owner=profile1.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile2.user]
        )

        url = reverse('social:edit_document', args=[document.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_allowed_users")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([profile2.user.id], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_file_media")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual(split_path(document.file.name)[-1], tag.get('value', ''))

        tags = soup.select("textarea#id_description")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(document.description, tag.text)

        self.assertEqual(1, Document.objects.count())
        doc = Document.objects.get(id=document.id)
        self.assertEqual(doc.description, document.description)

    def test_view_edit_other_document(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        document = DocumentFactory.create(owner=profile2.user)

        url = reverse('social:edit_document', args=[document.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

        self.assertEqual(1, Document.objects.count())
        doc = Document.objects.get(id=document.id)
        self.assertEqual(doc.description, document.description)
        self.assertEqual(doc.owner, profile2.user)

    def test_share_document_me(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_USER,
            'description': 'Just for me',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': []
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(0, document.get_allowed_users().count())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])

    def test_share_document_followers(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        FollowingFactory.create(member_user=profile1.user, follower=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_FOLLOWERS,
            'description': '',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': []
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(1, document.get_allowed_users().count())
        self.assertTrue(profile2.user in document.get_allowed_users().all())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])

    def test_share_document_defined(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_DEFINED,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': [profile3.user.id]
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(1, document.get_allowed_users().count())
        self.assertTrue(profile3.user in document.get_allowed_users().all())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])

    def test_share_document_contacts(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name='A', owner=profile1.user)
        group.users.add(profile2.user)
        group.save()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_GROUP,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_group': group.id,
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(1, document.get_allowed_users().count())
        self.assertTrue(profile2.user in document.get_allowed_users().all())
        self.assertFalse(profile3.user in document.get_allowed_users().all())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])
        self.assertEqual(document.allowed_group, group)

    def test_share_document_contacts_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name='A', owner=profile2.user)
        group.users.add(profile2.user)
        group.save()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_GROUP,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_group': group.id,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(0, Document.objects.count())

        self.assertEqual(1, len(soup.select('ul.errorlist li')))

        self.assertEqual(0, Document.objects.count())

    def test_share_document_anonymous(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_FOLLOWERS,
            'description': '',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': []
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(0, Document.objects.count())

    def test_post_message_missing_file(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_DEFINED,
            'description': 'For my friends only',
            'file': '',
            'file_media': '',
            'allowed_users': [profile3.user.id]
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(0, Document.objects.count())

        self.assertEqual(1, len(soup.select('ul.errorlist li')))

    def test_post_message_missing_type(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:share_document')

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': [profile3.user.id]
        }
        response = self.client.post(url, data=data)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(0, Document.objects.count())

        self.assertEqual(1, len(soup.select('ul.errorlist li')))

    def test_edit_document_defined(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        document = DocumentFactory.create(owner=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:edit_document', args=[document.id])

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_DEFINED,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': [profile3.user.id]
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(1, document.get_allowed_users().count())
        self.assertTrue(profile3.user in document.get_allowed_users().all())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])

    def test_edit_document_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        description = 'my document'
        document = DocumentFactory.create(owner=profile2.user, description=description)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:edit_document', args=[document.id])

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_DEFINED,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_users': [profile3.user.id]
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 403)

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile2.user)
        self.assertEqual(document.description, description)

    def test_edit_document_contacts(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        description = 'my document'
        document = DocumentFactory.create(owner=profile1.user, description=description)

        group = ContactsGroup.objects.create(name='A', owner=profile1.user)
        group.users.add(profile2.user)
        group.save()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:edit_document', args=[document.id])

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join_path(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'access_type': Document.ACCESS_GROUP,
            'description': 'For my friends only',
            'file': file_to_upload,
            'file_media': 'sample-doc.pdf',
            'allowed_group': group.id,
        }
        response = self.client.post(url, data=data)

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(1, document.get_allowed_users().count())
        self.assertTrue(profile2.user in document.get_allowed_users().all())
        self.assertFalse(profile3.user in document.get_allowed_users().all())
        self.assertEqual(document.description, data['description'])
        self.assertEqual(document.datetime.date(), date.today())
        self.assertEqual(document.access_type, data['access_type'])
        self.assertEqual(document.allowed_group, group)


class DeleteDocumentTest(BaseTestCase):

    def test_view_delete_document(self):

        profile1 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))

        self.assertEqual(1, Document.objects.count())
        document = Document.objects.all()[0]

        self.assertEqual(document.id, document0.id)
        self.assertEqual(document.owner, profile1.user)
        self.assertEqual(0, document.get_allowed_users().count())

    def test_view_delete_document_other(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user, access_type=Document.ACCESS_DEFINED)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.get(url)

        self.assertEqual(403, response.status_code)

        self.assertEqual(1, Document.objects.count())

    def test_view_delete_shared_document(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.get(url)

        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        doc_access = DocumentAccess.objects.get(document=document0, user=profile1.user)
        self.assertEqual(doc_access.is_deleted, False)

    def test_view_delete_shared_group(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name='A', owner=profile2.user)
        group.users.add(profile1.user)
        group.save()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_GROUP, allowed_group=group
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.get(url)

        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        self.assertEqual(0, DocumentAccess.objects.filter(document=document0, user=profile1.user).count())

    def test_view_delete_shared_followers(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_FOLLOWERS
        )
        FollowingFactory.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.get(url)

        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        self.assertEqual(0, DocumentAccess.objects.filter(document=document0, user=profile1.user).count())

    def test_delete_document(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile1.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile2.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(0, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

    def test_delete_document_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user, access_type=Document.ACCESS_DEFINED)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(response.status_code, 403)

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

    def test_delete_shared_document(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        doc_access = DocumentAccess.objects.get(document=document0, user=profile1.user)
        self.assertEqual(doc_access.is_deleted, True)

    def test_delete_shared_contacts_document(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name='A', owner=profile2.user)
        group.users.add(profile1.user)
        group.save()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_GROUP, allowed_group=group
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        doc_access = DocumentAccess.objects.get(document=document0, user=profile1.user)
        self.assertEqual(doc_access.is_deleted, True)

    def test_delete_shared_followers(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_FOLLOWERS
        )
        FollowingFactory.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())

        doc_access = DocumentAccess.objects.get(document=document0, user=profile1.user)
        self.assertEqual(doc_access.is_deleted, True)

    def test_delete_document_twice(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        document0.delete()

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(404, response.status_code)

    def test_delete_document_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user)

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        assert_popup_redirects(response, reverse('login'))

    def test_delete_document_no_confirm(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile1.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile2.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:delete_document', args=[document0.id])

        response = self.client.post(url, data={'confirm': ''})

        assert_popup_redirects(response, reverse('profiles:personal_space') + "?tab=documents")

        self.assertEqual(1, Document.objects.count())

        self.assertEqual(2, Individual.objects.count())


@override_settings(DEBUG=False)
class DownloadDocumentTest(BaseTestCase):

    def test_download_document_as_owner1(self):

        profile1 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile1.user, access_type=Document.ACCESS_DEFINED)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_as_owner2(self):

        profile1 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile1.user, access_type=Document.ACCESS_FOLLOWERS)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_as_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user]
        )

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_document_defined(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile1.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_not_in_defined(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_DEFINED, allowed_users=[profile3.user]
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_document_follower(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user, access_type=Document.ACCESS_FOLLOWERS)

        FollowingFactory.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_not_in_follower(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user, access_type=Document.ACCESS_FOLLOWERS)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_document_contacts(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name="Test", owner=profile2.user)
        group.users.add(profile1.user)
        group.save()

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_GROUP, allowed_group=group
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_not_in_contacts(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        group = ContactsGroup.objects.create(name="Test", owner=profile2.user)

        document0 = DocumentFactory.create(
            owner=profile2.user, access_type=Document.ACCESS_GROUP, allowed_group=group
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_document_user(self):
        profile1 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile1.user, access_type=Document.ACCESS_USER)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)

    def test_download_document_not_user(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        document0 = DocumentFactory.create(owner=profile2.user, access_type=Document.ACCESS_USER)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(403, response.status_code)

    def test_download_document_filename(self):
        profile1 = IndividualFactory.create()

        data = 'abcdef'

        document0 = DocumentFactory.create(
            owner=profile1.user, access_type=Document.ACCESS_USER,
            file__filename='doc.pdf', file__data=data
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)
        self.assertEqual(response['Content-Type'], 'application/pdf')
        self.assertEqual(response['Content-Disposition'], 'attachment; filename=doc.pdf')
        self.assertEqual(response['Content-Length'], str(len(data)))

    def test_download_document_unknown_type(self):
        profile1 = IndividualFactory.create()

        data = 'abcdef'

        document0 = DocumentFactory.create(
            owner=profile1.user, access_type=Document.ACCESS_USER,
            file__filename='doc.zzz', file__data=data
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:download_document', args=[document0.id])

        self.assertEqual(url, document0.get_absolute_url())

        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(response['X-Accel-Redirect'], document0.file.url)
        self.assertEqual(response['Content-Type'], 'application/octet-stream')
        self.assertEqual(response['Content-Disposition'], 'attachment; filename=doc.zzz')
        self.assertEqual(response['Content-Length'], str(len(data)))
