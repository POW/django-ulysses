# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, datetime, timedelta
from os.path import abspath, dirname, join

from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site

from rest_framework.reverse import reverse

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects, BaseAPITestCase

from ulysses.profiles.factories import IndividualFactory
from ulysses.web.factories import FocusOnFactory
from ulysses.works.factories import WorkFactory, Work

from ..factories import MemberPostFactory, MessageFactory, MessageReplyFactory, AttachmentFactory
from ..models import Message, MessageRecipient, MessageReply


class ComposeMessageTest(BaseTestCase):

    def test_view_compose_message(self):

        profile1 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:compose_message')
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual('', tag.get('value', ''))

        self.assertEqual(0, Message.objects.count())

    def test_view_write_to(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag['action'])

        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([profile2.user.id], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual('', tag.get('value', ''))

        self.assertEqual(0, Message.objects.count())

    def test_view_reply_to(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], subject="hello",
            sent_datetime=datetime.now() - timedelta(days=1)
        )
        url = reverse('social:reply_to', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag['action'])

        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([profile2.user.id], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual('', tag.get('value', ''))

        self.assertEqual(1, Message.objects.count())

    def test_view_share_work(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        work = WorkFactory.create()
        url = reverse('social:share_work', args=[work.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual(work.title, tag.get('value', ''))

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action', ''))

        self.assertEqual(0, Message.objects.count())

    def test_view_share_focus_on(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        focus_on = FocusOnFactory.create()
        url = reverse('social:share_focus_on', args=[focus_on.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])

        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual(focus_on.title, tag.get('value', ''))

        tags = soup.select("form")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual(url, tag.get('action', ''))

        self.assertEqual(0, Message.objects.count())

    def test_view_compose_anonymous(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        url = reverse('social:compose_message')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(0, Message.objects.count())

    def test_create_message(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:compose_message')

        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile1.user)
        self.assertEqual(message_recipient.is_read, False)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

    def test_compose_message_with_existing(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:compose_message')
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], subject="hello",
            sent_datetime=datetime.now() - timedelta(days=1)
        )
        MessageRecipient.objects.filter(message=message1).update(is_read=True)
        MessageReplyFactory.create(message=message1, sender=profile1.user)

        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        queryset = Message.objects.exclude(id=message1.id)
        self.assertEqual(1, queryset.count())
        message = queryset[0]
        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, '')
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())
        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)
        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 0)

    def test_reply_to_existing_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        message = MessageFactory.create(
            sender=profile2.user, recipients=[profile1.user], subject="hello",
            sent_datetime=datetime.now() - timedelta(days=1)
        )
        url = reverse('social:reply_to', args=[message.id])

        MessageRecipient.objects.filter(message=message).update(is_read=True)
        existing_reply1 = MessageReplyFactory.create(message=message, sender=profile2.user)
        existing_reply2 = MessageReplyFactory.create(message=message, sender=profile1.user)

        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile2.user)
        self.assertEqual(message.subject, 'hello')
        self.assertEqual(message.sent_datetime.date(), date.today() - timedelta(days=1))
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)

        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 2)
        replies = replies.exclude(id=existing_reply1.id).exclude(id=existing_reply2.id)
        self.assertEqual(replies.count(), 0)

    def test_create_message_group(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:compose_message')

        data = {
            'recipients': [profile2.user.id, profile3.user.id],
            'subject': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 3)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile1.user)
        self.assertEqual(message_recipient.is_read, False)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile3.user)
        self.assertEqual(message_recipient.is_read, False)

        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 0)

    def test_share_work(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        work = WorkFactory.create()
        url = reverse('social:share_work', args=[work.id])

        data = {
            'recipients': [profile2.user.id],
            'subject': work.title,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile1.user)
        self.assertEqual(message_recipient.is_read, False)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 1)
        reply = replies.all()[0]
        site = Site.objects.get_current()
        self.assertEqual(reply.body, 'http://' + site.domain + work.get_absolute_url())

    def test_share_work_existing_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        work = WorkFactory.create()
        url = reverse('social:share_work', args=[work.id])

        message = MessageFactory.create(
            sender=profile2.user, recipients=[profile1.user], subject="hello",
            sent_datetime=datetime.now() - timedelta(days=1)
        )
        MessageRecipient.objects.filter(message=message).update(is_read=True)
        existing_reply = MessageReplyFactory.create(message=message, sender=profile2.user)

        data = {
            'recipients': [profile2.user.id],
            'subject': work.title,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile2.user)
        self.assertEqual(message.subject, 'hello')
        self.assertEqual(message.sent_datetime.date(), date.today() - timedelta(days=1))
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)

        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 2)
        replies = replies.exclude(id=existing_reply.id)
        self.assertEqual(replies.count(), 1)
        existing_reply = replies.all()[0]
        site = Site.objects.get_current()
        self.assertEqual(existing_reply.body, 'http://' + site.domain + work.get_absolute_url())

    def test_share_focus_on(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        focus_on = FocusOnFactory.create()
        url = reverse('social:share_focus_on', args=[focus_on.id])

        data = {
            'recipients': [profile2.user.id],
            'subject': focus_on.title,
        }
        response = self.client.post(url, data=data)

        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]

        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))

        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())

        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile1.user)
        self.assertEqual(message_recipient.is_read, False)
        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 1)
        reply = replies.all()[0]
        site = Site.objects.get_current()
        self.assertEqual(reply.body, 'http://' + site.domain + focus_on.get_absolute_url())

    def test_write_to(self):
        """no existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        queryset = Message.objects.all()
        self.assertEqual(1, queryset.count())
        message = queryset[0]
        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())
        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)
        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 0)

    def test_write_to_new_contact(self):
        """no existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile3.user]
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        queryset = Message.objects.exclude(id=message1.id)
        self.assertEqual(1, queryset.count())
        message = queryset[0]
        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())
        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)
        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 0)

    def test_write_to_existing_contact(self):
        """existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=datetime.now() - timedelta(days=1)
        )
        message2 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=datetime.now()
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        self.assertEqual(1, Message.objects.filter(id=message1.id).count())
        self.assertEqual(1, Message.objects.filter(id=message2.id).count())
        assert_popup_redirects(response, reverse('social:messages_init', args=[message2.id]))

    def test_write_to_existing_contact_deleted(self):
        """existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=datetime.now() - timedelta(days=1)
        )
        recipient = MessageRecipient.objects.get(message=message1, user=profile2.user)
        recipient.is_deleted = True
        recipient.save()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        self.assertEqual(1, Message.objects.filter(id=message1.id).count())
        queryset = Message.objects.exclude(id=message1.id)
        self.assertEqual(queryset.count(), 1)
        message2 = queryset[0]
        assert_popup_redirects(response, reverse('social:messages_init', args=[message2.id]))

    def test_write_to_existing_contact_in_group(self):
        """existing message with this member but in a group"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user, profile3.user]
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        queryset = Message.objects.exclude(id=message1.id)
        self.assertEqual(1, queryset.count())
        message = queryset[0]
        assert_popup_redirects(response, reverse('social:messages_init', args=[message.id]))
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual(message.subject, data['subject'])
        self.assertEqual(message.sent_datetime.date(), date.today())
        self.assertEqual(message.updated_datetime.date(), date.today())
        self.assertEqual(MessageRecipient.objects.filter(message=message).count(), 2)
        MessageRecipient.objects.get(message=message, user=profile1.user)
        MessageRecipient.objects.get(message=message, user=profile2.user)
        replies = MessageReply.objects.filter(message=message)
        self.assertEqual(replies.count(), 0)

    def test_write_to_existing_contact_some_in_group(self):
        """existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user, profile3.user], updated_datetime=datetime.now()
        )
        message2 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=datetime.now() - timedelta(days=1)
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile2.user.id])
        data = {
            'recipients': [profile2.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        self.assertEqual(1, Message.objects.filter(id=message1.id).count())
        self.assertEqual(1, Message.objects.filter(id=message2.id).count())
        assert_popup_redirects(response, reverse('social:messages_init', args=[message2.id]))

    def test_write_to_myself(self):
        """existing message with this member"""
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=datetime.now()
        )
        message2 = MessageFactory.create(
            sender=profile1.user, recipients=[profile1.user], updated_datetime=datetime.now() - timedelta(days=1)
        )
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:write_to', args=[profile1.user.id])
        data = {
            'recipients': [profile1.user.id],
            'subject': '',
        }
        response = self.client.post(url, data=data)
        self.assertEqual(2, Message.objects.count())
        self.assertEqual(1, Message.objects.filter(id=message1.id).count())
        self.assertEqual(1, Message.objects.filter(id=message2.id).count())
        assert_popup_redirects(response, reverse('social:messages_init', args=[message2.id]))


class PostMessageReplyTest(BaseAPITestCase):

    def test_post_message_contact(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.data))

        self.assertEqual(1, MessageReply.objects.count())
        self.assertEqual(1, MessageReply.objects.filter(message=message).count())
        reply = MessageReply.objects.all()[0]
        self.assertEqual(reply.sender, profile1.user)
        self.assertEqual(reply.body, data['body'])
        self.assertEqual(reply.sent_datetime.date(), date.today())

        message = Message.objects.get(id=message.id)
        self.assertEqual(message.updated_datetime.date(), date.today())

        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

    def test_post_message_existing(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        reply = MessageReplyFactory.create(message=message, sender=profile2.user)

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(2, len(response.data))

        self.assertEqual(2, MessageReply.objects.count())
        self.assertEqual(2, MessageReply.objects.filter(message=message).count())
        self.assertEqual(1, MessageReply.objects.filter(message=message, sender=profile1.user).count())
        reply = MessageReply.objects.filter(message=message, sender=profile1.user)[0]
        self.assertEqual(reply.sender, profile1.user)
        self.assertEqual(reply.body, data['body'])
        self.assertEqual(reply.sent_datetime.date(), date.today())

        message = Message.objects.get(id=message.id)
        self.assertEqual(message.updated_datetime.date(), date.today())

        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

    def test_post_message_reply(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile1.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.data))

        self.assertEqual(1, MessageReply.objects.count())
        self.assertEqual(1, MessageReply.objects.filter(message=message).count())
        reply = MessageReply.objects.all()[0]
        self.assertEqual(reply.sender, profile1.user)
        self.assertEqual(reply.body, data['body'])
        self.assertEqual(reply.sent_datetime.date(), date.today())

        message = Message.objects.get(id=message.id)
        self.assertEqual(message.updated_datetime.date(), date.today())

        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

    def test_post_message_not_in_recipients(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile3.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 403)

        self.assertEqual(0, MessageReply.objects.count())

    def test_post_message_has_left(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile3.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True, exit_datetime=datetime.now())

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 403)

        self.assertEqual(0, MessageReply.objects.count())

    def test_post_message_anonymous(self):

        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile3.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 403)

        self.assertEqual(0, MessageReply.objects.count())

    def test_post_message_group(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user, profile3.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message', args=[message.id])

        data = {
            'body': 'Hello',
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, len(response.data))

        self.assertEqual(1, MessageReply.objects.count())
        self.assertEqual(1, MessageReply.objects.filter(message=message).count())
        reply = MessageReply.objects.all()[0]
        self.assertEqual(reply.sender, profile1.user)
        self.assertEqual(reply.body, data['body'])
        self.assertEqual(reply.sent_datetime.date(), date.today())

        message = Message.objects.get(id=message.id)
        self.assertEqual(message.updated_datetime.date(), date.today())

        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

        message_recipient = MessageRecipient.objects.get(message=message, user=profile3.user)
        self.assertEqual(message_recipient.is_read, False)


class MessageThreadTest(BaseAPITestCase):

    def test_get_message_content_empty(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, [])

    def test_get_message_not_found(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id + 1000])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 404)

    def test_get_message_anonymous(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_get_message_not_recipient(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile3.user, recipients=[profile2.user])

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 403)

    def test_get_message_reply(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        reply = MessageReplyFactory.create(message=message, sender=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['html_body'], reply.body)

    def test_get_message_replies(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        reply1 = MessageReplyFactory.create(
            message=message, sender=profile2.user, sent_datetime=datetime(2018, 9, 6, 12, 0)
        )
        reply2 = MessageReplyFactory.create(
            message=message, sender=profile2.user, sent_datetime=datetime(2018, 9, 6, 9, 0)
        )
        reply3 = MessageReplyFactory.create(
            message=message, sender=profile2.user, sent_datetime=datetime(2018, 9, 6, 15, 0)
        )
        reply4 = MessageReplyFactory.create(
            message=message, sender=profile2.user, sent_datetime=datetime(2018, 9, 5, 18, 0)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 4)
        self.assertEqual(response.data[0]['html_body'], reply4.body)
        self.assertEqual(response.data[1]['html_body'], reply2.body)
        self.assertEqual(response.data[2]['html_body'], reply1.body)
        self.assertEqual(response.data[3]['html_body'], reply3.body)

    def test_get_message_replies_has_left(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        recipient = MessageRecipient.objects.get(user=profile2.user)
        recipient.exit_datetime = datetime(2018, 9, 7, 14, 0)
        recipient.save()
        reply1 = MessageReplyFactory.create(
            message=message, sender=profile1.user, sent_datetime=datetime(2018, 9, 6, 12, 0)
        )
        reply2 = MessageReplyFactory.create(
            message=message, sender=profile1.user, sent_datetime=datetime(2018, 9, 6, 9, 0)
        )
        reply3 = MessageReplyFactory.create(
            message=message, sender=profile1.user, sent_datetime=datetime(2018, 9, 6, 15, 0)
        )
        reply4 = MessageReplyFactory.create(
            message=message, sender=profile1.user, sent_datetime=datetime(2018, 9, 8, 18, 0)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 3)
        self.assertEqual(response.data[0]['html_body'], reply2.body)
        self.assertEqual(response.data[1]['html_body'], reply1.body)
        self.assertEqual(response.data[2]['html_body'], reply3.body)

    def test_get_message_reply_group(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user, profile3.user])
        reply = MessageReplyFactory.create(message=message, sender=profile3.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['html_body'], reply.body)

    def test_get_message_reply_group_from_me(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user, profile3.user])
        reply = MessageReplyFactory.create(message=message, sender=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data[0]['html_body'], reply.body)

    def test_get_message_reply_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        message2 = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        MessageReplyFactory.create(message=message2)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message1.id])
        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 0)

    def test_get_message_reply_player(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        work = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER)
        work_full_url = 'http://' + site.domain + work.get_absolute_url()
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this work {0}".format(work_full_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        player_html = '<audio controls="controls"><source src="{0}" /></audio>'.format(work.audio_url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data[0]['html_body'],
            'Listen this work <a href="{0}" target="_blank">{1}</a><br />{2}<br />{3}'.format(
                work.get_absolute_url(), work_full_url, work.title, player_html
            )
        )

    def test_get_message_reply_player_private_work(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        work = WorkFactory.create(visibility=Work.VISIBILITY_PRIVATE)
        work_full_url = 'http://' + site.domain + work.get_absolute_url()
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this work {0}".format(work_full_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data[0]['html_body'],
            'Listen this work <a href="{0}" >{1}</a>'.format(work.get_absolute_url(), work_full_url)
        )

    def test_get_message_reply_player_no_audio(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        work = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, no_audio=True)
        work_full_url = 'http://' + site.domain + work.get_absolute_url()
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this work {0}".format(work_full_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data[0]['html_body'],
            'Listen this work <a href="{0}" >{1}</a>'.format(work.get_absolute_url(), work_full_url)
        )

    def test_get_message_reply_youtube(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        youtube_url = 'https://www.youtube.com/watch?v=8yDKRE2-mOs'
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this sound {0}".format(youtube_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertTrue('iframe' in response.data[0]['html_body'])

    def test_get_message_reply_youtube_short(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        youtube_url = 'https://youtu.be/8yDKRE2-mOs'
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this sound {0}".format(youtube_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertTrue('iframe' in response.data[0]['html_body'])

    def test_get_message_reply_soundcloud(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        site = Site.objects.get_current()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])
        soundcloud_url = 'https://soundcloud.com/lennonstella/bad?in=lennonstella/sets/love-me-1158'
        MessageReplyFactory.create(
            message=message, sender=profile2.user, body="Listen this sound {0}".format(soundcloud_url)
        )

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:message_thread', args=[message.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data), 1)
        self.assertTrue('iframe' in response.data[0]['html_body'])


class ComposeContentMessageTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def _do_test_view_compose_message(self, element):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        content_type = ContentType.objects.get_for_model(element)
        url = reverse('social:compose_content_message', args=[content_type.id, element.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])
        tags = soup.select("input#id_subject")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("text", tag['type'])
        self.assertEqual(element.title, tag.get('value', ''))
        self.assertEqual(0, Message.objects.count())

    def test_compose_message_member_post(self):
        profile1 = IndividualFactory.create()
        owner = IndividualFactory.create()
        element = MemberPostFactory.create(owner=owner.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        content_type = ContentType.objects.get_for_model(element)
        url = reverse('social:compose_content_message', args=[content_type.id, element.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("select#id_recipients")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual([], [int(sub_tag["value"]) for sub_tag in tag.select('option')])
        self.assertEqual(0, Message.objects.count())


class DeleteMessageTest(BaseTestCase):

    def test_view_delete_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_view_exit_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        url = reverse('social:exit_message', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))

    def test_view_delete_message_as_sender(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(1, len(soup.select('form[action={0}]'.format(url))))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_view_delete_message_as_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile3.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(403, response.status_code)
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_view_delete_message_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_delete_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.post(url, {'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('social:messages'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, True)
        self.assertEqual(
            MessageReply.objects.filter(message=message1, sender=profile2.user, is_action=True).count(),
            1
        )

    def test_exit_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile2.user.email, password="1234"))
        url = reverse('social:exit_message', args=[message1.id])
        response = self.client.post(url, {'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('social:messages'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        recipient = MessageRecipient.objects.get(message=message1, user=profile2.user)
        self.assertEqual(recipient.is_deleted, False)
        self.assertNotEqual(recipient.exit_datetime, None)
        self.assertEqual(
            MessageReply.objects.filter(message=message1, sender=profile2.user, is_action=True).count(),
            1
        )

    def test_delete_message_as_sender(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.post(url, {'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('social:messages'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile1.user).is_deleted, True)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_delete_message_as_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile3.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(403, response.status_code)
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_delete_message_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.post(url, {'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)

    def test_delete_message_no_confirm(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message1 = MessageFactory.create(recipients=[profile2.user], sender=profile1.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_message', args=[message1.id])
        response = self.client.post(url, {'confirm': ''})
        self.assertEqual(200, response.status_code)
        assert_popup_redirects(response, reverse('social:messages'))
        self.assertEqual(1, Message.objects.count())
        message = Message.objects.all()[0]
        self.assertEqual(message.id, message1.id)
        self.assertEqual(message.sender, profile1.user)
        self.assertEqual([profile1.user, profile2.user], list(message.recipients.all().order_by('id')))
        self.assertEqual(message.is_deleted, False)
        self.assertEqual(MessageRecipient.objects.get(message=message1, user=profile2.user).is_deleted, False)


class AttachmentTest(BaseAPITestCase):

    def test_post_attachment(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message_attachment', args=[message.id])

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join(base_dir, "fixtures", 'sample-doc.pdf')
        uploaded_file = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'Filedata': uploaded_file
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 200)

        message = Message.objects.get(id=message.id)
        self.assertEqual(message.updated_datetime.date(), date.today())

        message_recipient = MessageRecipient.objects.get(message=message, user=profile2.user)
        self.assertEqual(message_recipient.is_read, False)

        self.assertEqual(
            MessageReply.objects.filter(message=message, sender=profile1.user).count(),
            1
        )

    def test_post_attachment_as_other(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user])

        MessageRecipient.objects.filter(message=message).update(is_read=True)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message_attachment', args=[message.id])

        base_dir = abspath(dirname(__file__))
        sample_doc_path = join(base_dir, "fixtures", 'sample-doc.pdf')
        uploaded_file = open(sample_doc_path, 'r', encoding='latin-1')

        data = {
            'Filedata': uploaded_file
        }
        response = self.client.post(url, data=data)

        self.assertEqual(response.status_code, 403)

        message = Message.objects.get(id=message.id)
        self.assertEqual(
            MessageReply.objects.filter(message=message, sender=profile1.user).count(),
            0
        )

    def test_post_attachment_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile1.user])
        MessageRecipient.objects.filter(message=message).update(is_read=True)
        # self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:post_message_attachment', args=[message.id])
        base_dir = abspath(dirname(__file__))
        sample_doc_path = join(base_dir, "fixtures", 'sample-doc.pdf')
        uploaded_file = open(sample_doc_path, 'r', encoding='latin-1')
        data = {
            'Filedata': uploaded_file
        }
        response = self.client.post(url, data=data)
        self.assertEqual(response.status_code, 403)
        message = Message.objects.get(id=message.id)
        self.assertEqual(
            MessageReply.objects.filter(message=message, sender=profile1.user).count(),
            0
        )

    def test_view_delete_attachment(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        attachment = AttachmentFactory.create(message=message, sender=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 1)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_delete_attachment(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user, profile3.user])

        attachment = AttachmentFactory.create(message=message, sender=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(response.status_code, 200)
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 0)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_delete_attachment_one_to_one(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile1.user, recipients=[profile2.user])

        attachment = AttachmentFactory.create(message=message, sender=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(response.status_code, 403)
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 1)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_delete_attachment_non_admin(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile1.user, profile3.user])

        attachment = AttachmentFactory.create(message=message, sender=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(response.status_code, 403)
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 1)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_delete_attachment_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()

        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile3.user])

        attachment = AttachmentFactory.create(message=message, sender=profile2.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])

        response = self.client.post(url, data={'confirm': 'confirm'})

        self.assertEqual(response.status_code, 403)
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 1)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_delete_attachment_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile1.user])
        attachment = AttachmentFactory.create(message=message, sender=profile2.user)
        # self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:delete_attachment', args=[attachment.id])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, reverse('login'))
        self.assertEqual(MessageReply.objects.filter(id=attachment.id).count(), 1)
        self.assertEqual(Message.objects.filter(id=message.id).count(), 1)

    def test_download_attachments(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile1.user])
        attachment = AttachmentFactory.create(message=message, sender=profile2.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:reply_attachment', args=[attachment.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_download_attachments_other(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile3.user])
        attachment = AttachmentFactory.create(message=message, sender=profile2.user)
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:reply_attachment', args=[attachment.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 403)

    def test_download_attachments_anonymous(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        message = MessageFactory.create(sender=profile2.user, recipients=[profile2.user, profile1.user])
        attachment = AttachmentFactory.create(message=message, sender=profile2.user)
        # self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:reply_attachment', args=[attachment.id])
        response = self.client.get(url)
        self.assertRedirects(response, reverse('login') + "?next={0}".format(url))
