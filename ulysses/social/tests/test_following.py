# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date

from django.urls import reverse
from django.utils.translation import ugettext as _

from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase

from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ulysses.profiles.factories import IndividualFactory, OrganizationFactory

from ..factories import FollowingFactory
from ..models import Following, FeedItem


class FollowMemberTest(BaseTestCase):

    def test_view_follow_individual_member(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:follow_member', args=[profile2.user.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, _("Do you want to follow"))
        self.assertContains(response, profile2.name)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_member_user")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("hidden", tag['type'])
        self.assertEqual(str(profile2.user.id), tag['value'])
        self.assertEqual(0, Following.objects.count())

    def test_view_followed_individual_member(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        FollowingFactory.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:follow_member', args=[profile2.user.id])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertContains(response, _("Do you want to stop following"))

        self.assertContains(response, profile2.name)
        soup = BeautifulSoup(response.content, 'html.parser')
        tags = soup.select("input#id_member_user")
        self.assertEqual(1, len(tags))
        tag = tags[0]
        self.assertEqual("hidden", tag['type'])
        self.assertEqual(str(profile2.user.id), tag['value'])

        self.assertEqual(1, Following.objects.count())
        following = Following.objects.all()[0]
        self.assertEqual(following.member_user, profile2.user)
        self.assertEqual(following.follower, profile1.user)

    def test_follow_individual_member(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:follow_member', args=[profile2.user.id])

        response = self.client.post(url, data={'member_user': profile2.user.id})

        assert_popup_redirects(response, profile2.get_absolute_url())

        self.assertEqual(1, Following.objects.count())
        following = Following.objects.all()[0]
        self.assertEqual(following.member_user, profile2.user)
        self.assertEqual(following.follower, profile1.user)
        self.assertEqual(following.follow_datetime.date(), date.today())

    def test_follow_organization_member(self):
        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:follow_member', args=[profile2.user.id])
        response = self.client.post(url, data={'member_user': profile2.user.id})
        assert_popup_redirects(response, profile2.get_absolute_url())
        self.assertEqual(1, Following.objects.count())
        following = Following.objects.all()[0]
        self.assertEqual(following.member_user, profile2.user)
        self.assertEqual(following.follower, profile1.user)
        self.assertEqual(following.follow_datetime.date(), date.today())

    def test_organization_follow_individual_member(self):

        profile1 = OrganizationFactory.create()
        profile2 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:follow_member', args=[profile2.user.id])

        response = self.client.post(url, data={'member_user': profile2.user.id})

        assert_popup_redirects(response, profile2.get_absolute_url())

        self.assertEqual(1, Following.objects.count())
        following = Following.objects.all()[0]
        self.assertEqual(following.member_user, profile2.user)
        self.assertEqual(following.follower, profile1.user)
        self.assertEqual(following.follow_datetime.date(), date.today())

    def test_follow_myself(self):
        profile1 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
        url = reverse('social:follow_member', args=[profile1.user.id])
        response = self.client.post(url, data={'member_user': profile1.user.id})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(
            [tag.text for tag in soup.select('ul.errorlist li')],
            [_("You can't follow yourself")]
        )
        self.assertEqual(0, Following.objects.count())

    def test_follow_as_anonymous(self):

        profile2 = IndividualFactory.create()

        url = reverse('social:follow_member', args=[profile2.user.id])

        response = self.client.post(url, data={'member_user': profile2.user.id})

        assert_popup_redirects(response, reverse('login'))

        self.assertEqual(0, Following.objects.count())

    def test_follow_unknown(self):

        profile1 = IndividualFactory.create()

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:follow_member', args=[profile1.user.id + 222])

        response = self.client.post(url, data={'member_user': profile1.user.id + 222})

        self.assertEqual(response.status_code, 404)

        self.assertEqual(0, Following.objects.count())

    def test_follow_existing(self):

        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        FollowingFactory.create(member_user=profile2.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:follow_member', args=[profile2.user.id])

        response = self.client.post(url, data={'member_user': profile2.user.id})
        self.assertEqual(response.status_code, 200)

        self.assertEqual(0, Following.objects.count())


class FollowersMembersTest(APITestCase):

    def test_get_followers_empty(self):

        profile1 = IndividualFactory.create()
        url = reverse('social:followers', args=[profile1.user.id])

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_get_followers_empty_anonymous(self):

        profile1 = IndividualFactory.create()
        url = reverse('social:followers', args=[profile1.user.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        data = {
            'results': [],
            'count': 0,
            'previous': None,
            'next': None,
        }

        self.assertEqual(data, response.data)

    def test_get_followers_anonymous(self):

        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        profile4 = IndividualFactory.create()
        IndividualFactory.create()

        Following.objects.create(member_user=profile1.user, follower=profile2.user)
        Following.objects.create(member_user=profile2.user, follower=profile1.user)
        Following.objects.create(member_user=profile1.user, follower=profile3.user)
        Following.objects.create(member_user=profile4.user, follower=profile1.user)

        url = reverse('social:followers', args=[profile1.user.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

        results = response.data['results']
        self.assertEqual(len(results), 2)

        followed_profiles = [profile3, profile2]

        for (profile, elt) in zip(followed_profiles, results):
            self.assertEqual(profile.user.id, elt['id'])
            self.assertEqual(profile.name, elt['name'])
            self.assertEqual(profile.get_absolute_url(), elt['url'])
            self.assertEqual(profile.get_avatar(), elt['avatar'])

    def test_get_followers(self):

        profile1 = IndividualFactory.create()
        profile2 = OrganizationFactory.create()
        profile3 = IndividualFactory.create()
        profile4 = IndividualFactory.create()
        IndividualFactory.create()

        Following.objects.create(member_user=profile1.user, follower=profile2.user)
        Following.objects.create(member_user=profile2.user, follower=profile1.user)
        Following.objects.create(member_user=profile1.user, follower=profile3.user)
        Following.objects.create(member_user=profile4.user, follower=profile1.user)

        self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))

        url = reverse('social:followers', args=[profile1.user.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 2)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(response.data['next'], None)

        results = response.data['results']
        self.assertEqual(len(results), 2)

        followed_profiles = [profile3, profile2]

        for (profile, elt) in zip(followed_profiles, results):
            self.assertEqual(profile.user.id, elt['id'])
            self.assertEqual(profile.name, elt['name'])
            self.assertEqual(profile.get_absolute_url(), elt['url'])
            self.assertEqual(profile.get_avatar(), elt['avatar'])

    def test_get_followers_pagination(self):

        profile1 = IndividualFactory.create()
        for index in range(10):
            profile2 = IndividualFactory.create()
            Following.objects.create(member_user=profile1.user, follower=profile2.user)

        url = reverse('social:followers', args=[profile1.user.id])

        response = self.client.get(url)
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 10)
        self.assertEqual(response.data['previous'], None)
        self.assertEqual(
            response.data['next'],
            'http://testserver{0}?page=2'.format(url)
        )

        results = response.data['results']

        self.assertEqual(len(results), 9)

    def test_get_followers_pagination_page2(self):

        profile1 = IndividualFactory.create()
        for index in range(10):
            profile2 = IndividualFactory.create()
            Following.objects.create(member_user=profile1.user, follower=profile2.user)

        url = reverse('social:followers', args=[profile1.user.id])

        response = self.client.get(url, data={'page': 2})
        self.assertEqual(status.HTTP_200_OK, response.status_code)

        self.assertEqual(response.data['count'], 10)
        self.assertEqual(
            response.data['previous'],
            'http://testserver{0}'.format(url)
        )
        self.assertEqual(response.data['next'], None)

        results = response.data['results']

        self.assertEqual(len(results), 1)
