# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from django.core.management import call_command
from django.core import mail
from django.utils.translation import ugettext as _

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory

from ..factories import MessageFactory, MessageReplyFactory
from ..models import MessageRecipient


class MessageDigestTest(BaseTestCase):

    def test_message_digest_first_digest(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=None
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile2.user.email])
        self.assertEqual(digest_email.subject, _('You have 1 new message'))

    def test_message_digest_new_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=500)
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)

        recipient = MessageRecipient.objects.get(user=profile2.user, message=message)
        recipient.is_deleted = False
        recipient.is_read = False
        recipient.save()

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile2.user.email])
        self.assertEqual(digest_email.subject, _('You have 1 new message'))

    def test_message_digest_several_messages(self):
        profile1 = IndividualFactory.create()
        profile3 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=500)
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)
        MessageReplyFactory.create(
            message=message, sender=profile1.user, sent_datetime=sent_datetime + timedelta(seconds=10)
        )

        message2 = MessageFactory.create(
            sender=profile3.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message2, sender=profile3.user, sent_datetime=sent_datetime)
        MessageReplyFactory.create(
            message=message2, sender=profile3.user, sent_datetime=sent_datetime + timedelta(seconds=10)
        )

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile2.user.email])
        self.assertEqual(digest_email.subject, _('You have 4 new messages'))

    def test_message_digest_no_new_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=100)
        sent_datetime = now - timedelta(seconds=500)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 0)

    def test_message_digest_actions(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=500)
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime, is_action=True)

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 0)

    def test_message_digest_has_left_discussion(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=500)
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)

        recipient = MessageRecipient.objects.get(user=profile2.user, message=message)
        recipient.is_deleted = True
        recipient.save()

        call_command('messages_digest', verbosity=0)

        self.assertEqual(len(mail.outbox), 0)

    def test_message_digest_several_calls_message(self):
        profile1 = IndividualFactory.create()
        profile2 = IndividualFactory.create()

        now = datetime.now()
        last_digest_datetime = now - timedelta(seconds=500)
        sent_datetime = now - timedelta(seconds=100)

        message = MessageFactory.create(
            sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
            last_digest_datetime=last_digest_datetime
        )
        MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)

        recipient = MessageRecipient.objects.get(user=profile2.user, message=message)
        recipient.is_deleted = False
        recipient.is_read = False
        recipient.save()

        call_command('messages_digest', verbosity=0)
        self.assertEqual(len(mail.outbox), 1)
        digest_email = mail.outbox[0]
        self.assertEqual(digest_email.to, [profile2.user.email])
        self.assertEqual(digest_email.subject, _('You have 1 new message'))

        mail.outbox = []  # empty the mailbox
        call_command('messages_digest', verbosity=0)
        self.assertEqual(len(mail.outbox), 0)
    #
    # def test_message_digest_deadlock(self):
    #     profile1 = IndividualFactory.create()
    #     profile2 = IndividualFactory.create()
    #
    #     self.assertTrue(self.client.login(email=profile1.user.email, password="1234"))
    #
    #     now = datetime.now()
    #     last_digest_datetime = now - timedelta(seconds=500)
    #     sent_datetime = now - timedelta(seconds=100)
    #
    #     for i in range(100):
    #         profile_n = IndividualFactory.create()
    #         message = MessageFactory.create(
    #             sender=profile1.user, recipients=[profile_n.user], updated_datetime=sent_datetime,
    #             last_digest_datetime=last_digest_datetime
    #         )
    #         MessageReplyFactory.create(message=message, sender=profile1.user, sent_datetime=sent_datetime)
    #         recipient = MessageRecipient.objects.get(user=profile_n.user, message=message)
    #         recipient.is_deleted = False
    #         recipient.is_read = False
    #         recipient.save()
    #
    #     new_messages = []
    #     for i in range(100):
    #         new_messages.append(
    #             MessageFactory.create(
    #                 sender=profile1.user, recipients=[profile2.user], updated_datetime=sent_datetime,
    #                 last_digest_datetime=last_digest_datetime
    #             )
    #         )
    #
    #     from multiprocessing import Process
    #     import time
    #
    #     def call_messages_digest():
    #         call_command('messages_digest', interactive=False, verbosity=2)
    #
    #     call_messages_digest_process = Process(target=call_messages_digest)
    #
    #     for index, new_message in enumerate(new_messages):
    #
    #         print("> post message")
    #         url = reverse('social:post_message', args=[new_message.id])
    #         data = {
    #             'body': 'New message',
    #         }
    #         response = self.client.post(url, data=data)
    #         self.assertEqual(response.status_code, 200)
    #
    #         if index == 0:
    #             call_messages_digest_process.start()
    #         time.sleep(0.05)
    #
    #     call_messages_digest_process.join()
