# -*- coding: utf-8 -*-

from functools import update_wrapper

from django.contrib.admin.sites import AdminSite
from django.contrib.auth.views import redirect_to_login
from django.core.exceptions import PermissionDenied
from django.urls import reverse
from django.views.decorators.cache import never_cache
from django.views.decorators.csrf import csrf_protect


class BaseAdminSite(AdminSite):
    """
    Base class for Ulysses admin sites
    - admin is meant for competitions admin
    - super-admin is meant for Ulysses platform staff
    """

    def admin_view(self, view, cacheable=False, extra_handler=None):
        """Main wrapper for displaying any admin view"""

        def inner(request, *args, **kwargs):

            # redirect to login in not logged
            if not request.user.is_authenticated:
                return redirect_to_login(
                    request.get_full_path(),
                    reverse('admin:login', current_app=self.name)
                )

            # Permission Denied if logged but not authorized to access
            if not self.has_permission(request):
                raise PermissionDenied

            if '/super-admin/' in request.path and not request.user.is_superuser:
                raise PermissionDenied

            # Makes possible to extend this in sub-methods
            # for example manage competitions switch in admin site
            if extra_handler:
                response = extra_handler(request)
                if response:
                    return response

            # Ok process the view
            return view(request, *args, **kwargs)

        # Call update_wrapper helper function
        if not cacheable:
            inner = never_cache(inner)
        # We add csrf_protect here so this function can be used as a utility
        # function for any view, without having to repeat 'csrf_protect'.
        if not getattr(view, 'csrf_exempt', False):
            inner = csrf_protect(inner)
        return update_wrapper(inner, view)
