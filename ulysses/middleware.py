# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from threading import current_thread

from django.http import HttpResponse


class RequestNotFound(Exception):
    """exception"""
    pass


class RequestManager(object):
    """
    get django request from anywhere
    copied from https://github.com/ljean/coop_cms
    """
    _shared = {}

    def __init__(self):
        """his is a Borg"""
        self.__dict__ = RequestManager._shared

    def _get_request_dict(self):
        """request dict"""
        if not hasattr(self, '_request'):
            self._request = {}  # pylint: disable=attribute-defined-outside-init
        return self._request

    def clean(self):
        """clean"""
        if hasattr(self, '_request'):
            del self._request

    def get_request(self):
        """return request"""
        _requests = self._get_request_dict()
        the_thread = current_thread()
        if the_thread not in _requests:
            raise RequestNotFound("Request not found: make sure that middleware is installed")
        return _requests[the_thread]

    def set_request(self, request):
        """set request"""
        _requests = self._get_request_dict()
        _requests[current_thread()] = request


class RequestMiddleware:
    """middleware for request"""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        """middleware is called for every request"""
        RequestManager().set_request(request)
        return self.get_response(request)


def get_request():
    try:
        return RequestManager().get_request()
    except RequestNotFound:
        return None


class UlyssesPlayerMiddleware(object):
    """middleware for request"""

    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        response = self.get_response(request)
        return self.process_response(request, response)

    def process_exception(self, request, exception):
        ulysses_player = request.META.get('HTTP_X_ULYSSES_PLAYER')
        if ulysses_player:
            html = '<div class="ulysses-content">{0}</div>'.format(exception)
            return HttpResponse(html)

    def process_response(self, request, response):
        ulysses_player = request.META.get('HTTP_X_ULYSSES_PLAYER')
        if ulysses_player:
            if getattr(response, 'content'):
                soup = BeautifulSoup(response.content, 'html.parser')
                html = soup.select('.ulysses-content')
                if len(html) > 0:
                    html = '<div class="ulysses-content">{0}</div>'.format(html[0])
                    return HttpResponse(html)
            if response.status_code in (301, 302):
                return HttpResponse(
                    '<script>loadUrl("{0}");</script>'.format(response['Location'])
                )
            if response.status_code in (404, 403, 401):
                # Redirect to the page. It will stop the player
                return HttpResponse(
                    "<script>window.location = '{0}';</script>".format(request.path)
                )
        return response
