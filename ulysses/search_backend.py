# -*- coding: utf-8 -*-

from django.contrib.auth.models import User

from haystack.backends.elasticsearch_backend import (
    ElasticsearchSearchBackend, ElasticsearchSearchQuery, ElasticsearchSearchEngine
)
from haystack.signals import RealtimeSignalProcessor

from ulysses.profiles.models import Individual, Organization
from ulysses.profiles.utils import get_profile


class CustomElasticBackend(ElasticsearchSearchBackend):
    """custom backend: keep same behavior that default"""
    def setup(self):
        eb = super(CustomElasticBackend, self)
        eb.setup()


class CustomElasticSearchQuery(ElasticsearchSearchQuery):
    """custom search_query: keep same behavior that default"""

    def build_query_fragment(self, field, filter_type, value):
        value = super(CustomElasticSearchQuery, self).build_query_fragment(field, filter_type, value)
        return value


class CustomElasticEngine(ElasticsearchSearchEngine):
    """The custom engine that determine backend and search_query"""
    backend = CustomElasticBackend
    query = CustomElasticSearchQuery


class UlyssesRealtimeSignalProcessor(RealtimeSignalProcessor):

    def handle_save(self, sender, instance, **kwargs):
        if sender is User:
            if instance.id:
                profile = get_profile(instance)
                if profile and profile.id:
                    profile_model = Individual if profile.is_individual() else Organization
                    if instance.is_active:
                        super().handle_save(profile_model, profile)
                    else:
                        super().handle_delete(profile_model, profile)
        super().handle_save(sender, instance, **kwargs)
