# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from django.contrib.auth import views as auth_views
from django.shortcuts import redirect
from django.urls import reverse, reverse_lazy
from django.views.generic.base import TemplateView
from django.views.generic import RedirectView

from ulysses.profiles.backends.activation import views as registration_views

from ulysses.competitions import get_admin_site
from ulysses.competitions.views import upload_from_superadmin, remove_group
from ulysses.profiles.utils import get_profile
from ulysses.profiles.views.base import LoginView
from ulysses.social.utils import create_welcome_messages
from ulysses.super_admin.sites import super_admin_site
from ulysses.web.forms import UlysseRegistrationForm, PasswordResetForm, PasswordChangeForm, SetPasswordForm
from ulysses.web.views.misc import RedirectWebView


def post_login(request):
    """where to go after a user logs in"""
    profile = get_profile(request.user)
    if profile and not profile.has_already_login:
        # 1st login : goto personal space
        profile.has_already_login = True
        profile.save()
        create_welcome_messages(profile)
        return redirect(reverse("profiles:personal_space"))
    else:
        # not the 1st login : goto home
        return redirect(reverse("web:home"))


# Main urls

urlpatterns = [
    url(r'^login/$', LoginView.as_view(), name='login'),
    url(
        r'^admin/login/$',
        RedirectView.as_view(url=reverse_lazy('login'), permanent=False, query_string=True)
    ),
    url(
        r'^super-admin/login/$',
        RedirectView.as_view(url=reverse_lazy('login'), permanent=False, query_string=True)
    ),
    url(r'^logout/$', registration_views.LogoutView.as_view(), name='logout'),
    url(
        r'^auth/password_change/$',
        auth_views.PasswordChangeView.as_view(
            template_name='registration/password_change_form.html',
            form_class=PasswordChangeForm,
            success_url=reverse_lazy('profiles:personal_space')
        ),
        name='auth_password_change'
    ),
    url(
        r'^auth/password_reset/$',
        auth_views.PasswordResetView.as_view(
            template_name='registration/password_reset_form.html',
            form_class=PasswordResetForm,
            success_url=reverse_lazy('login')
        ),
        name='auth_password_reset'
    ),
    url(
        r'^auth/reset/(?P<uidb64>[0-9A-Za-z]+)-(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.PasswordResetConfirmView.as_view(
            template_name='registration/password_reset_confirm.html',
            form_class=SetPasswordForm,
            success_url=reverse_lazy('login'),
        ),
        name='password_reset_confirm'
    ),
    # Redirect the logged user to web or jury interface, depending on its profile
    url(r'^auth/post_login', post_login, name="post_login"),

    # Activation keys get matched by \w+ instead of the more specific
    # [a-fA-F0-9]{40} because a bad activation key should still get to
    # the view; that way it can return a sensible "invalid key"
    # message instead of a confusing 404.
    url(
        r'^activate/(?P<activation_key>.+)/$',
        registration_views.ActivationView.as_view(
           template_name='registration/activate.html'
        ),
        name='registration_activate'
    ),
    url(
        r'^register/$',
        registration_views.RegistrationView.as_view(
            form_class=UlysseRegistrationForm,
            template_name='registration/registration_form.html',
            success_url=reverse_lazy("registration_complete")
        ),
        name='registration_register'
    ),
    url(
        r'^register/complete/$',
        registration_views.RegistrationCompleteView.as_view(),
        name='registration_complete'
    ),
    url(
        r'^register/closed/$',
        TemplateView.as_view(
            template_name='registration/registration_closed.html'
        ),
        name='registration_disallowed'
    ),

    url(r'^upload-from-superadmin/(?P<candidate_id>\d+)/', upload_from_superadmin, name='upload_from_superadmin'),
    url(r'^admin/remove-group/$', remove_group, name="admin_remove_group"),
    url(r'^super-admin/', super_admin_site.urls),
    url(r'^admin/', get_admin_site().urls),
    # url(r'^tinymce/', include('tinymce.urls')),
    url(r'^system/', include('ulysses.system.urls')),
    url(r'^', include('ulysses.web.urls', namespace="web")),
    url(r'^works/', include('ulysses.works.urls', namespace="works")),
    url(r'^events/', include('ulysses.events.urls', namespace="events")),
    url(r'^jury/', include('ulysses.jury.urls', namespace="jury")),
    url(r'^profiles/', include('ulysses.profiles.urls', namespace="profiles")),
    url(r'^social/', include('ulysses.social.urls', namespace="social")),
    url(r'^', include('ulysses.competitions.urls', namespace="competitions")),
]

# Redirect web to same page without the web prefix
urlpatterns += [
    url(r'^web/(?P<path>.*)', RedirectWebView.as_view(), name='redirect_web'),
]
