# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-20 10:21


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_community','0006_folder_work_length'),
    ]

    operations = [
        migrations.AddField(
            model_name='communityuser',
            name='view_count',
            field=models.IntegerField(default=0),
        ),
    ]
