# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.community'
    label = 'ulysses_community'
    verbose_name = "Ulysses Community"
