# -*- coding: utf-8 -*-



from contextlib import closing
from ftplib import FTP
import shutil
import os.path

from django.conf import settings
from django.db import connections
from django.utils.text import slugify


def query_community_db(sql_query):
    """execute a sql query on community_db"""

    community_db = connections['community_db'].cursor()
    community_db.execute(sql_query)
    field_names = [i[0] for i in community_db.description]
    rows = community_db.fetchall()

    for row in rows:
        # Iterate over every row
        # Convert position-based row to field-base row
        yield dict([
            (field, val)  # tuple key, value
            for (field, val) in zip(field_names, row)
        ])


def to_utf(value):
    """execute a sql query on community_db"""
    return value or ""
    # if value:
    #     try:
    #         value.decode('utf-8')
    #     except (UnicodeDecodeError, UnicodeEncodeError):
    #         return u""
    # return value or u""


def download_ftp(storage_path, dir_name, dest_filename, fake=False):
    """download something from ulysses.community ftp"""
    path_name, file_name = os.path.split(storage_path)

    # FielField has limit of 100 chars
    base_name, ext = os.path.splitext(dest_filename)
    base_name = slugify(base_name)[:50]

    dest_filename = base_name + ext

    media_path = '{0}/{1}'.format(dir_name, dest_filename)

    dest_path = os.path.join(settings.MEDIA_ROOT, dir_name)

    if not os.path.exists(dest_path):
        os.makedirs(dest_path)

    dest_file_path = os.path.join(settings.MEDIA_ROOT, media_path)

    if not fake:

        with closing(FTP(settings.ULYSSES_COMMUNITY_FTP['host'])) as ftp:
            ftp.login(user=settings.ULYSSES_COMMUNITY_FTP['user'], passwd=settings.ULYSSES_COMMUNITY_FTP['password'])
            ftp.cwd("/web/" + path_name)

            try:
                with open(dest_file_path, 'w+b') as dest_file:
                    ftp.retrbinary('RETR %s' % file_name, dest_file.write)
            except IOError as err:
                print("> download_ftp - IOError:", err, "///", dir_name, dest_filename)
                return ""

    return media_path


def copy_media(storage_path, dir_name, dest_filename, fake=False):
    """download something from ulysses.community ftp"""
    path_name, file_name = os.path.split(storage_path)

    # FielField has limit of 100 chars
    base_name, ext = os.path.splitext(dest_filename)
    base_name = slugify(base_name)[:50]

    dest_filename = base_name + ext

    media_path = '{0}/{1}'.format(dir_name, dest_filename)

    dest_path = os.path.join(settings.MEDIA_ROOT, dir_name)

    if not os.path.exists(dest_path):
        os.makedirs(dest_path)

    dest_file_path = os.path.join(settings.MEDIA_ROOT, media_path)

    try:
        shutil.move('/srv/media/community1/' + path_name + '/' + file_name, dest_file_path)
    except IOError as err:
        print("> copy_media - ERR", path_name, ":", err)

    return media_path

