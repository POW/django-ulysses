# -*- coding: utf-8 -*-

from django.db import models
from django.utils.translation import ugettext_lazy as _


class FieldOptionModel(models.Model):
    """values loaded from engine4_user_fields_options"""
    field_id = None

    option_id = models.IntegerField(default=0)
    label = models.CharField(max_length=100)
    order = models.IntegerField(default=0)

    def __str__(self):
        return self.label

    @classmethod
    def get_community_sql(cls):
        return """
            select option_id, label, `order`
            from engine4_user_fields_options
            where field_id in ({0})
        """.format(
            cls.field_id if type(cls.field_id) is int else ', '.join([str(elt) for elt in cls.field_id])
        )

    class Meta:
        abstract = True
        ordering = ['order']


class Profile(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = 1

    class Meta:
        verbose_name = _('Profile')
        verbose_name_plural = _('Profiles')
        ordering = ['order']


class Interest(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = (31, 48, )

    def users_count(self):
        return self.communityuser_set.count()

    class Meta:
        verbose_name = _('Interest')
        verbose_name_plural = _('Interests')
        ordering = ['order']


class Occupation(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = 26

    class Meta:
        verbose_name = _('Occupation')
        verbose_name_plural = _('Occupations')
        ordering = ['order']


class Organization(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = 43

    class Meta:
        verbose_name = _('Organization')
        verbose_name_plural = _('Organizations')
        ordering = ['order']


class OrganizationType(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = 51

    class Meta:
        verbose_name = _('Organization type')
        verbose_name_plural = _('Organization types')
        ordering = ['order']


class Instrument(FieldOptionModel):
    """
    User Field of Ulysses Community
    """
    field_id = 57

    class Meta:
        verbose_name = _('Instrument')
        verbose_name_plural = _('Instruments')
        ordering = ['order']


class UserLevel(models.Model):
    """
    type of users
    """
    level_id = models.IntegerField(default=0)
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=200, blank=True, default="")
    type = models.CharField(max_length=100, blank=True, default="")
    flag = models.CharField(max_length=100, blank=True, default="")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('User level')
        verbose_name_plural = _('User levels')
        ordering = ['title']


class CommunityUser(models.Model):
    """A user imported from Ulysses Community"""

    community_id = models.IntegerField(default=0)
    email = models.EmailField(blank=True, db_index=True)

    first_name = models.CharField(max_length=100, default="", blank=True)
    last_name = models.CharField(max_length=100, default="", blank=True)

    level = models.ForeignKey(UserLevel, blank=True, default=None, null=True, on_delete=models.CASCADE)
    birth_date = models.DateField(default=None, blank=True, null=True)
    country = models.CharField(max_length=10, blank=True, default="")
    timezone = models.CharField(max_length=100, blank=True, default="")
    photo = models.CharField(max_length=200, blank=True, default="")
    photo_storage_path = models.CharField(max_length=200, blank=True, default="")

    web_site = models.CharField(max_length=200, blank=True, default="")
    facebook = models.CharField(max_length=200, blank=True, default="")
    twitter = models.CharField(max_length=200, blank=True, default="")

    about_me = models.TextField(blank=True, default="")

    creation_date = models.DateTimeField(blank=True, default=None, null=True)
    last_login_date = models.DateTimeField(blank=True, default=None, null=True)

    profile = models.ForeignKey(Profile, blank=True, default=None, null=True, on_delete=models.CASCADE)
    organization = models.ForeignKey(Organization, blank=True, null=True, default=None, on_delete=models.CASCADE)
    organization_types = models.ManyToManyField(OrganizationType, blank=True)

    interests = models.ManyToManyField(Interest, blank=True)
    occupation = models.ForeignKey(Occupation, blank=True, null=True, default=None, on_delete=models.CASCADE)
    occupation_more = models.CharField(max_length=100, default="", blank=True)

    instruments = models.ManyToManyField(Instrument, blank=True)
    view_count = models.IntegerField(default=0)

    def __str__(self):
        return self.email

    class Meta:
        verbose_name = _('Community user')
        verbose_name_plural = _('Community users')
        ordering = ['last_name', 'first_name', 'email', ]


class FolderCategory(models.Model):
    """category of works"""
    category_id = models.IntegerField(default=0)
    parent = models.ForeignKey("self", blank=True, default=None, null=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, default="", blank=True)
    in_use = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    @property
    def full_path(self):
        path = []
        parent = self.parent
        while parent:
            path.append(parent.name)
            parent = parent.parent
        return " > ".join(reversed(path))
    full_path.fget.short_description = _("Full path")

    class Meta:
        verbose_name = _('Folder category')
        verbose_name_plural = _('Folder categories')


class FolderKeyword(models.Model):
    name = models.CharField(max_length=100, default="", blank=True)
    in_use = models.BooleanField(default=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = _('Folder keyword')
        verbose_name_plural = _('Folder keywords')


class Folder(models.Model):
    """works"""
    folder_id = models.IntegerField(default=0)
    photo_id = models.IntegerField(default=0)
    owner = models.ForeignKey(CommunityUser, on_delete=models.CASCADE)
    category = models.ForeignKey(FolderCategory, on_delete=models.CASCADE)
    title = models.CharField(max_length=100, default="", blank=True)
    description = models.TextField(default="", blank=True)
    keywords = models.ManyToManyField(FolderKeyword, blank=True)
    creation_date = models.DateTimeField()
    view_count = models.IntegerField(default=0)
    photo = models.CharField(max_length=200, blank=True, default="")
    photo_storage_path = models.CharField(max_length=200, blank=True, default="")
    composer = models.CharField(max_length=100, blank=True, default="")
    composer_nationality = models.CharField(max_length=100, blank=True, default="")
    work_creation_date = models.DateField(default=None, blank=True, null=True)
    work_length = models.IntegerField(default=0)
    post_as = models.IntegerField(default=0)
    copyright = models.BooleanField(default=False)
    allow_printing = models.BooleanField(default=False)

    @property
    def keywords_str(self):
        return ", ".join([keyword.name for keyword in self.keywords.all()])
    keywords_str.fget.short_description = _("keywords")

    @property
    def attachments_count(self):
        return "{0}".format(self.attachment_set.count())
    attachments_count.fget.short_description = _("attachments")

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = _('Folder')
        verbose_name_plural = _('Folders')


class Attachment(models.Model):
    """files"""
    owner = models.ForeignKey(CommunityUser, on_delete=models.CASCADE)
    folder = models.ForeignKey(Folder, on_delete=models.CASCADE)
    file = models.CharField(max_length=200, blank=True, default="")
    file_storage_path = models.CharField(max_length=200, blank=True, default="")

    def __str__(self):
        return self.file

    class Meta:
        verbose_name = _('Attachment')
        verbose_name_plural = _('Attachments')
