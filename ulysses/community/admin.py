# -*- coding: utf-8 -*-

from django.contrib import admin

from ulysses.super_admin.sites import super_admin_site

from . import models


class InterestAdmin(admin.ModelAdmin):
    list_display = ('label', 'users_count')


class UserLevelAdmin(admin.ModelAdmin):
    list_display = ('title', 'description', "type", "flag")


class CommunityUserAdmin(admin.ModelAdmin):
    list_display = (
        "email", "first_name", "last_name", "birth_date", "profile", "level", "timezone", "last_login_date",
    )
    list_filter = ("level", "profile", "timezone", )
    search_fields = ('email', "last_name", )
    date_hierarchy = "last_login_date"


class FolderCategoryAdmin(admin.ModelAdmin):
    list_display = ("name", "in_use", "full_path")
    list_filter = ("in_use", )


class FolderKeywordAdmin(admin.ModelAdmin):
    list_display = ("name", "in_use")
    list_filter = ("in_use", )


class AttachmentInline(admin.TabularInline):
    model = models.Attachment


class FolderAdmin(admin.ModelAdmin):
    list_display = (
        "title", "category", "owner", "attachments_count", "keywords_str",
    )
    list_filter = ("category", "keywords", )
    search_fields = ("title", "owner__last_name", )
    date_hierarchy = "creation_date"
    inlines = [AttachmentInline]


# super_admin_site.register(models.Interest, InterestAdmin)
# super_admin_site.register(models.Instrument)
# super_admin_site.register(models.Occupation)
# super_admin_site.register(models.Organization)
# super_admin_site.register(models.OrganizationType)
# super_admin_site.register(models.Profile)
# super_admin_site.register(models.UserLevel, UserLevelAdmin)
# super_admin_site.register(models.CommunityUser, CommunityUserAdmin)
# super_admin_site.register(models.Folder, FolderAdmin)
# super_admin_site.register(models.FolderCategory, FolderCategoryAdmin)
# super_admin_site.register(models.FolderKeyword, FolderKeywordAdmin)
# # super_admin_site.register(models.Attachment)
