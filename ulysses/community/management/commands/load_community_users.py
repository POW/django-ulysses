# -*- coding: utf-8 -*-



from django.core.management.base import BaseCommand

from ulysses.community.utils import query_community_db, to_utf
from ulysses.community import models


class Command(BaseCommand):
    """
    load users from Ulysses Community DB
    """
    help = "load users from Ulysses Community DB"

    def _set_many_to_many(self, many_to_many, model_class, value_ids):
        if value_ids:
            value_ids = [int(id_) for id_ in value_ids.split(",")]
            for value_id in value_ids:
                value = model_class.objects.get(option_id=value_id)
                many_to_many.add(value)

    def _set_foreign_key(self, obj, foreign_key, model_class, value_id):
        if value_id:
            setattr(obj, foreign_key, model_class.objects.get(option_id=value_id))

    def get_user_field(self, user_id, field_id):
        users_field_sql = """
            select value
            from engine4_user_fields_values
            WHERE item_id={0} and field_id={1}
        """.format(user_id, field_id)
        rows = list(query_community_db(users_field_sql))
        if rows:
            return rows[0]['value']

    def get_about_me(self, user_id):
        value = self.get_user_field(user_id, 42)
        if value:
            return value

        value = self.get_user_field(user_id, 28)
        if value:
            return value

        return ""

    def handle(self, *args, **options):

        models_to_load = [
            models.Instrument, models.Organization, models.OrganizationType, models.Interest, models.Occupation,
            models.Profile,
        ]

        for model_class in models_to_load:
            sql_query = model_class.get_community_sql()

            model_class.objects.all().delete()
            print(model_class.__name__)
            for row in query_community_db(sql_query):
                option_id = row['option_id']
                label = row['label']
                order = row['order']
                model_class.objects.create(option_id=option_id, label=label, order=order)

        print("Levels")
        models.UserLevel.objects.all().delete()
        levels_sql = """
            select level_id, title, description, type, flag
            from engine4_authorization_levels
        """
        for row in query_community_db(levels_sql):
            models.UserLevel.objects.create(**row)


        print("Users")
        models.CommunityUser.objects.all().delete()
        users_sql = """
            select user_id, email, photo_id, `timezone`, level_id, creation_date, lastlogin_date, view_count
            from engine4_users
        """

        for row in query_community_db(users_sql):
            user = models.CommunityUser.objects.create(
                community_id=row["user_id"],
                email=row["email"],
                timezone=row["timezone"],
                creation_date=row["creation_date"],
                last_login_date=row["lastlogin_date"],
                view_count=row['view_count']
            )
            if row['level_id']:
                user.level = models.UserLevel.objects.get(level_id=row['level_id'])
                user.save()

        print("User fields")
        users_field_sql = """
            select item_id, profile_type, first_name, last_name, birthdate, field_31, website, facebook, twitter,
            field_43, field_48, field_57, field_51, country, field_66, field_26
            from engine4_user_fields_search
        """

        for row in query_community_db(users_field_sql):

            try:
                user = models.CommunityUser.objects.get(community_id=row["item_id"])
                user.first_name = to_utf(row['first_name'])
                user.last_name = to_utf(row['last_name'])
                user.birth_date = row['birthdate']
                user.website = to_utf(row['website'])
                user.facebook = to_utf(row['facebook'])
                user.twitter = to_utf(row['twitter'])
                # user.about_me = to_utf(row['about_me'])
                user.about_me = to_utf(self.get_about_me(row["item_id"]))
                user.country = row['country'] or ""
                user.occupation_more = row['field_66'] or ""

                self._set_foreign_key(user, 'profile', models.Profile, row['profile_type'])
                self._set_foreign_key(user, 'organization', models.Organization, row['field_43'])
                self._set_foreign_key(user, 'occupation', models.Occupation, row['field_26'])

                self._set_many_to_many(user.interests, models.Interest, row["field_31"])
                self._set_many_to_many(user.interests, models.Interest, row["field_48"])
                self._set_many_to_many(user.instruments, models.Instrument, row["field_57"])
                self._set_many_to_many(user.organization_types, models.OrganizationType, row["field_51"])

                user.save()
            except Exception as err:
                print("ERROR", err, "************************************")
                for key, val in list(row.items()):
                    print("#", key, val)
                raise

        print("Photos")
        photos_sql = """
            select user_id, storage_path, `name`, mime_major, mime_minor
            from engine4_storage_files
            where parent_type='user' and user_id is NOT NULL and type is NULL
            order by creation_date asc
        """
        for row in query_community_db(photos_sql):
            user = models.CommunityUser.objects.get(community_id=row["user_id"])
            user.photo_storage_path = to_utf(row["storage_path"])
            user.photo = to_utf(row["name"])
            user.save()

