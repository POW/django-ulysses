# -*- coding: utf-8 -*-

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ulysses.community.utils import query_community_db
from ulysses.community import models
from ulysses.composers.models import is_composer
from ulysses.competitions.models import is_jury_member


class Command(BaseCommand):
    """
    print info about the common users between Ulysses Concours et Ulysses Community
    """
    help = "load users from Ulysses Community DB"

    def handle(self, *args, **options):

        emails = User.objects.exclude(email='').values('email').distinct()

        duplicated_count = 0
        community_user_count = 0
        concours_user_count = 0
        both_user_count = 0
        verif_both_count = 0

        for index, data in enumerate(emails):

            email = data['email']
            email_users = User.objects.filter(email=email)

            is_duplicated = len(email_users) > 1
            if is_duplicated:
                duplicated_count += 1

            if models.CommunityUser.objects.filter(email=email).count() > 0:
                both_user_count += 1
            else:
                concours_user_count += 1

        for community_user in models.CommunityUser.objects.all():
            if User.objects.filter(email=community_user.email).count() == 0:
                community_user_count += 1
            else:
                verif_both_count += 1

        print("Nb d'utilisateurs communs", both_user_count)
        print("Nb d'utilisateurs Seulement Concours", concours_user_count)
        print("Nb d'utilisateurs Seulement Community", community_user_count)
        print("Nb d'utilisateurs avec plusieurs comptes Concours", duplicated_count)
