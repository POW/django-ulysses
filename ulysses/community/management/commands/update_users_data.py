# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from ulysses.community.utils import query_community_db, to_utf
from ulysses.community import models


class Command(BaseCommand):
    """
    load users from Ulysses Community DB
    """
    help = "load users from Ulysses Community DB"

    def handle(self, *args, **options):


        users_sql = """
            select user_id, email, photo_id, `timezone`, level_id, creation_date, lastlogin_date, view_count
            from engine4_users
        """

        for row in query_community_db(users_sql):
            user = models.CommunityUser.objects.get(email=row["email"])
            user.view_count = row['view_count']
            user.save()
