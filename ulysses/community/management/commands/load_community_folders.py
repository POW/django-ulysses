# -*- coding: utf-8 -*-

from datetime import date

from django.core.management.base import BaseCommand

from ulysses.community.utils import query_community_db, to_utf
from ulysses.community import models


def to_date(text):
    if text:
        year, month, day = [int(x) for x in text.split("-")]
        if year:

            if month == 0:
                month = 1

            if day == 0:
                day = 1

            try:
                return date(year, month, day)
            except ValueError:
                print("ERR", text, year, month, day)
                if day == 31 and month == 11:
                    day = 30
                    return date(year, month, day)
                else:
                    raise


def to_int(text):
    if text:
        return int(text)
    return 0


def bool_int(text):
    if text:
        return bool(int(text))
    return False


def can_print_to_bool(text):
    if text:
        return (int(text)) == 4
    return False


class Command(BaseCommand):
    """
    load works from Ulysses Community DB
    """
    help = "load works from Ulysses Community DB"


    def handle(self, *args, **options):

        print("Categories")
        models.FolderCategory.objects.all().delete()

        sql = """
            select category_id, category_name, parent_id
            from engine4_folder_categories
            order by category_id asc
        """
        for row in query_community_db(sql):
            category = models.FolderCategory.objects.create(
                category_id=row["category_id"],
                name=row["category_name"]
            )
            if row["parent_id"]:
                try:
                    parent = models.FolderCategory.objects.get(category_id=row["parent_id"])
                except models.FolderCategory.DoesNotExist:
                    parent = None
                category.parent = parent
                category.save()

        print("Folders")
        models.FolderKeyword.objects.all().delete()
        models.Folder.objects.all().delete()
        sql = """
                select user_id, category_id, folder_id, photo_id, title, description, keywords, creation_date,
                view_count
                from engine4_folder_folders
                where parent_type='user'
                order by category_id asc
            """
        for row in query_community_db(sql):
            owner = models.CommunityUser.objects.get(community_id=row["user_id"])
            category = models.FolderCategory.objects.get(category_id=row["category_id"])

            folder = models.Folder.objects.create(
                owner=owner,
                category=category,
                folder_id=row["folder_id"],
                creation_date=row["creation_date"],
                photo_id=row["photo_id"],
                title=to_utf(row["title"]),
                description=to_utf(row["description"]),
                view_count=row["view_count"]
            )

            keywords = row["keywords"].strip()
            if "," in keywords:
                keywords = keywords.split(",")
            elif "." in keywords:
                keywords = keywords.split(".")
            else:
                keywords = [keywords]

            keywords = [keyword.strip() for keyword in keywords]
            for keyword_name in keywords:
                if keyword_name:
                    keyword = models.FolderKeyword.objects.get_or_create(name=keyword_name)[0]
                    folder.keywords.add(keyword)

            folder_photos_sql = """
                select storage_path, `name`
                from engine4_storage_files
                where parent_type='folder' and file_id={0}
            """.format(
                folder.photo_id
            )
            for row in query_community_db(folder_photos_sql):
                folder.photo = row['name']
                folder.photo_storage_path = row['storage_path']

            folder.save()

            fields = {
                'composer': (2, str),
                'composer_nationality': (3, str),
                'work_creation_date': (5, to_date),
                'work_length': (6, to_int),
                'post_as': (7, to_int),
                'copyright': (7, bool_int),
                'allow_printing': (15, can_print_to_bool),
            }

            for field_name, (field_id, converter) in list(fields.items()):
                fields_sql = """
                    select `value`
                    from engine4_folder_fields_values
                    where item_id = {0} and field_id = {1}
                """.format(folder.folder_id, field_id)

                for row in query_community_db(fields_sql):
                    setattr(folder, field_name, converter(row['value']))

            folder.save()

        print("Attachments")
        models.Attachment.objects.all().delete()
        attachments_sql = """
            select user_id, folder_id, storage_path, `name`, mime_major, mime_minor
            from engine4_storage_files
            INNER JOIN engine4_folder_attachments
              on engine4_folder_attachments.attachment_id = engine4_storage_files.parent_id
            where parent_type='folder_attachment' and user_id is NOT NULL
        """
        for row in query_community_db(attachments_sql):
            user = models.CommunityUser.objects.get(community_id=row["user_id"])
            folder = models.Folder.objects.get(folder_id=row["folder_id"])
            models.Attachment.objects.create(
                owner=user,
                folder=folder,
                file=to_utf(row["name"]),
                file_storage_path=to_utf(row["storage_path"])
            )

        # Keep works without attachments
        # print "clean folders"
        # for folder in models.Folder.objects.all():
        #     if folder.attachment_set.count() == 0:
        #         folder.delete()

        print("clean categories")
        for category in models.FolderCategory.objects.all():
            if category.folder_set.count() == 0:
                category.in_use = False
                category.save()

        # Parent categories are marked as in use
        for category in models.FolderCategory.objects.filter(in_use=True):
            parent = category.parent
            while parent:
                parent.in_use = True
                parent.save()
                parent = parent.parent

        print("clean keywords")
        for keyword in models.FolderKeyword.objects.all():
            if keyword.folder_set.count() == 0:
                keyword.in_use = False
                keyword.save()
