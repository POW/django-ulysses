# -*- coding: utf-8 -*-

import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class CnilRulesValidator(object):

    help_text = _('Your password must contain at least one lower case and one upper case letter, one number, and be at least 8 characters long')

    def validate(self, password, user=None):

        validated = True

        if len(password) < 8:
            validated = False

        if not re.findall('[A-Z]', password):
            validated = False

        if not re.findall('[a-z]', password):
            validated = False

        if not re.findall('[0-9]', password):
            validated = False

        if not validated:
            raise ValidationError(
                self.help_text,
                code='password_not_cnil_compliant',
            )

    def get_help_text(self):
        return self.help_text