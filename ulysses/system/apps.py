# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.system'
    label = 'ulysses_system'
    verbose_name = "Ulysses System"
