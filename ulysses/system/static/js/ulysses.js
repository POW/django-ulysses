/**
 * Uysses
 */


String.prototype.replaceAll = function(search, replacement) {
    var target = this;
    return target.replace(new RegExp(search, 'g'), replacement);
};

function getPage(url){
  var regex = new RegExp("[\\?&]page=([^&#]*)");
  var results = regex.exec(url);
  if (results == null){
    return 1;
  } else{
    return decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}

function insertUrlParam(key, value) {
    key = encodeURI(key);
    value = encodeURI(value);

    var kvp = document.location.search.substr(1).split('&');
    var i = kvp.length, x;

    while(i--)
    {
        x = kvp[i].split('=');
        if (x[0] === key) {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }
    if (i < 0) {
      kvp[kvp.length] = [key,value].join('=');
    }
    //this will reload the page, it's likely better to store this until finished
    var search = kvp.join('&');
    var url = document.location.pathname + "?" + search;
    loadUrl(url);
}

function buildUrlParam(key, value, starter) {
    key = encodeURI(key);
    value = encodeURI(value);
    var starter = (starter !== null)?starter:document.location.search.substr(1);
    var kvp = starter.split('&');
    var i = kvp.length, x;

    while(i--)
    {
        x = kvp[i].split('=');
        if (x[0] === key) {
            x[1] = value;
            kvp[i] = x.join('=');
            break;
        }
    }
    if (i < 0) {
      kvp[kvp.length] = [key,value].join('=');
    }
    //this will reload the page, it's likely better to store this until finished
    return kvp.join('&');
    //var url = document.location.pathname + "?" + search;
    //return url;
}

function getFilter(url){
  var regex = new RegExp("[\\?&]filter=([^&#]*)");
  var results = regex.exec(url);
  if (results == null){
    return '';
  } else{
    return decodeURIComponent(results[1].replace(/\+/g, " "));
  }
}

function scrollHelpMenu(scrollTop) {
    if ($(window).width() >= 768) {
        var menuHeight = $(".help-menu").height();
        var footerHeight = 2 * $('.footer').height();
        var docHeight = $(document).height();
        if ((scrollTop + menuHeight + footerHeight) > docHeight) {
            scrollTop = docHeight - menuHeight - footerHeight;
        }
    } else {
        scrollTop = 0;
    }
    $(".help-menu").css({"top": scrollTop});
}

var selectWithAutocomplete = function (field_id, url, forceInit) {

  var name_to_id = {};
  var chosen_id = field_id.replaceAll('-', '_');

  var style = $('<style>#' + chosen_id + '_chosen.chosen-container ul.chosen-results li.no-results {display: none !important; }</style>');
  $('html > head').append(style);

  $("#" + chosen_id + "_chosen .search-field input").autocomplete({
    source: function (request, add) {
      $.ajax({
        url: url + "?term=" + request.term,
        success: function (data) {
          var items = [];
          $.each(data, function (i, val) {
            name_to_id[val.name] = val.id; //store in local dict
            items.push(val.name);
          });
          add(items);
        }
      });
    },
    select: function (event, ui) {
      var id = name_to_id[ui.item.value]; //get id from local dict
      if (id) {
        $("select#" + field_id).append($('<option>', {
          value: id,
          selected: 'selected',
          text: ui.item.value
        }));
        $('#' + field_id).trigger("chosen:updated");
      }
      $(this).val('');
      return false;
    }
  });

  if (forceInit) {
    $("select#" + field_id + " option").each(function(idx, elt) {
      $(elt).attr('selected', 'selected');
    });
    $('#' + field_id).trigger("chosen:updated");
  }
};


var generateMissingAvatar = function(avatar) {
  var avatarName = avatar.getAttribute("alt");
  var width = avatar.width || avatar.clientWidth || 120,
      height = avatar.height || avatar.clientHeight || 120,
      avatarSvg = jdenticon.toSvg(avatarName, Math.min(width, height));
  avatar.src = "data:image/svg+xml," + encodeURIComponent(avatarSvg);
};


var generateMissingAvatars = function(avatarClass) {
  var avatars = $("img." + avatarClass) ;
  for (var i = 0; i < avatars.length; i++) {
    var avatar = avatars[i];
    generateMissingAvatar(avatar);
  }
};


var onMemberAvatarError = function(image) {
    image.onerror = "";
    generateMissingAvatar(image);
    return true;
};


var isBigScreen = function() {
  return (window.innerHeight >= 900 && window.innerWidth >= 1200);
};


var confirmPopup = function(title, message, okHandler, cancelHandler) {
  //the design of the box will be modified
  var html = '<div class="main">' +
      '<form class="colorbox-form"><div class="popup_buttons"><div>' + message +
      '</div><a href="" id="confirmClose" class="btn btn-secondary">Cancel</a>' +
      '<input id="confirmOk" class="btn btn-primary" type="submit" value="Ok" />' +
      '</div></form></div>';
  var closedByOk = false;

  $.colorbox(
    {
      html: html,
      title: title,
      innerWidth: '400px',
      onComplete: function(){
        $("#confirmOk").unbind();
        $("#confirmClose").unbind();
        $("#cboxTitle").text(title);
        $("#confirmOk").click(
          function(){
            okHandler();
            closedByOk = true;
            $.fn.colorbox.close();
            return false;
          }
        );
        $("#confirmClose").click(
          function(){
            $.fn.colorbox.close();
            return false;
          }
        );
      },
      onClosed: function () {
        if ((!closedByOk) && cancelHandler) {
            cancelHandler()
        }
      }
    }
  );
};



var setCounterValue = function($elt, maxLength) {
    var val = $elt.val();
    var target = $("#word_counter_" + $elt.attr('id'));
    target.text('' + val.length + '/' + maxLength);
};


var setTextInputCount = function () {
    $("input.maxlength").each(function (idx, elt) {
        var maxLength = $(elt).attr('maxlength');
        if (maxLength > 0 ) {
            setCounterValue($(elt), maxLength);
            $(elt).change(function () {
                setCounterValue($(this), maxLength);
            });
            $(elt).keydown(function () {
                setCounterValue($(this), maxLength);
            });
        }
    })
};


var loadUrl = function(url, noHistory, callback) {
  var dest = $(".ulysses-content");
  if (dest.length) {
    console.log('# loadUrl', url);
    var data = {
      url: url
    };
    if (!noHistory) {
      window.history.pushState(data, 'Ulysses', url);
    }
    $.ajax({
      url: url,
      type: "GET",
      headers: {'X-ULYSSES-PLAYER': 'on'},  // In django request.META['HTTP_X_ULYSSES_PLAYER']
      success: function (html, textStatus, xhr) {
        if (html.match(/^<script>.*<\/script>$/)) {
          $('body').append(html);
        }
        else {
          dest.replaceWith(html).promise().done(function(elem) {
            onPageLoaded(false, callback);
          });
        }
      },
      error: function (html, textStatus, xhr){
        dest.replaceWith(html);
      }
    });

  } else {
    window.location = url;
  }
};


var clearFilterElements = function(key) {
  var filterInput = $('.filter-field input[name=filter]');
  var filterValue = filterInput.val();
  var filterDict = {};
  if (filterValue) {
    filterDict = JSON.parse(filterValue);
  }
  filterDict[key] = [];
  if(key == "filters")
    delete filterDict[key]
  var data = JSON.stringify(filterDict);
  filterInput.val(data);
  filterInput.attr('value', data);
};


var getFilterDict = function(filterInput, key, fieldName, fieldValue, remove, replace, isPopup) {
  var filterValue = filterInput.attr('value');
  var filterDict = {};
  if (filterValue) {
    filterDict = JSON.parse(filterValue);
  }
  if (filterDict[key] === undefined || replace) {
    filterDict[key] = [];
  }
  var element = {};
  element[fieldName] = fieldValue;
  if (remove) {
    var index = -1;
    for (var i=0; i<filterDict[key].length; i++) {
      var currentElt = filterDict[key][i];
      if (currentElt[fieldName] === element[fieldName]) {
        index = i;
        break;
      }
      if (isPopup && (currentElt[fieldName] === element['p_' + fieldName])) {
        index = i;
        break;
      }
    }
    if (index >= 0) {
      filterDict[key].splice(index, 1);
    }
  }
  else {
    filterDict[key].push(element);
  }
  var data = JSON.stringify(filterDict);
  filterInput.attr('value', data);
  return filterDict;
};


var getFilterQueryString = function(key, fieldName, fieldValue, remove, replace, isPopup) {
  var form = $('.filter-form' + (isPopup ? '.popup' : ':not(.popup)'));
  var filterInput = form.find('.filter-field input[name=filter]');
  var filterDict = getFilterDict(filterInput, key, fieldName, fieldValue, remove, replace, isPopup);
  var url = '';
  var queryString = null;
  $.each(filterDict, function(key1, elt1) {
    var elements = '';
    for (var j=0; j<elt1.length; j++) {
      var elt2 = elt1[j];
      $.each(elt2, function(key3, elt3){
        elements += key3 + ':' + elt3 + '$';
      });
    }
    queryString = buildUrlParam(key1, elements, queryString);
  });
  return queryString;
};


var setFilterElement = function(key, fieldName, fieldValue, remove, replace, isPopup) {
  var queryString = getFilterQueryString(key, fieldName, fieldValue, remove, replace, isPopup);
  if (queryString) {
    if (isPopup) {
      var popupUrl = $('.filter-form.popup').data('popup-url') + "?" + queryString;
      // console.log('>popupUrl', popupUrl);
      $.ajax({
        url: popupUrl,
        success: function (data) {
          // console.log('<data', data);
          $('.filter-fields-items-content.popup').html(data.html);
          $('.filter-form.popup #id_label').val(data.label);
        }
      });
    } else {
      var url = document.location.pathname + "?" + queryString;
      var scrollPosition = $(document).scrollTop();
      loadUrl(url, false, function () {
        $(document).scrollTop(scrollPosition);
      });
    }
  }
};


function textOverflowEllipsis() {
  $('.text-overflow-ellipsis').each(
    function (idx, elt) {
      var text = $(elt).text().trim();
      $(elt).data('text', $(elt).text());
      var wordArray = text.split(' ');
      while ((wordArray.length > 0) && (elt.scrollHeight > (elt.offsetHeight + 2))) {
        wordArray.pop();
        elt.innerHTML = wordArray.join(' ') + '...';
      }
    }
  )
}


function htmlOverflow() {
  $('.html-overflow').each(
    function (idx, elt) {
      if (elt.offsetHeight < elt.scrollHeight || elt.offsetWidth < elt.scrollWidth) {
        // the element has overflow
        $(elt).after("<div class='html-overflow-show-content'><div class='ellipsis'>Read more...</div></div>" );
        $(elt).addClass('has-overflow');
      }

    }
  )
}

function fitMessageBoxHelpZone() {
    try {
        var zone = $('.message-box-help-zone-content');
        var footer = $('.message-box-help-zone-actions');
        var maxHeightStr = zone.css('max-height');
        var footerHeight = footer.height() - 40;
        var maxHeight = parseInt(maxHeightStr.replace('px', ''));
        zone.css({
            height: '' + (maxHeight - footerHeight) + 'px',
            'margin-bottom': '' + footerHeight + 'px',
        });
    } catch (e) {
        // console.warn(e);
    }
}


/** DatetimeeWidget */
var setDatetimeWidgetTime = function(widget) {
    var timeValueWidget = widget.find(".timepicker");
    var allDayWidget = widget.find(".time-all-day");
    if (allDayWidget.is(':checked')) {
      timeValueWidget.val("00:00:00").change();
      timeValueWidget.hide();
    } else {
      timeValueWidget.show();
    }
};


var initDatetimeWidget = function() {
    setDatetimeWidgetTime($('.datetime-widget'));
};


var makeSquare = function() {

   // make square some elements : upload-work button
   $('.make-square').each(function (idx, elt) {
      var imageLoaded = function () {
          var insideHeight = $(elt).find('.center-vertical').height();
          if (insideHeight) {
              var height = $(elt).width();
              var margin = (height - insideHeight) / 2;
              $(elt).css('padding-top', margin);
              $(elt).height(height - margin);
          } else {
              $(elt).height($(elt).width());
          }
      };
      var tmpImg = new Image() ;
      tmpImg.onload = imageLoaded ;
      tmpImg.src = $(elt).find('img').attr('src') ;
   });
};


var adaptFocusOnContent = function () {
  $('.focus-on-text img').each(function (idx, elt) {
    $(elt).removeAttr('width');
    $(elt).removeAttr('height');
    $(elt).addClass('img-fluid');
  });

  // $('.focus-on-text iframe').each(function (idx, elt) {
  //   $(elt).attr('width', '100%');
  //   $(elt).attr('height', $(elt).width());
  // })
};


function initLocationSearch(popup) {
    var placeFieldSelector = popup ? '#p_loc-place' : '#loc-place';
    var countryNameFieldSelector = popup ? '#p_loc-country-name' : '#loc-country-name';
    var countryFieldSelector = popup ? '#p_loc-country' : "#loc-country";
    var cityFieldSelector = popup ? '#p_loc-city' : '#loc-city';
    var placeNameFieldSelector = popup ? '#p_loc-place-name' : '#loc-place-name';
    var locationFieldSelector = popup ? '#p_loc-location' : '#loc-location';
    var locFormSelector = popup ? '.p_loc-form' : '.loc-form';
    var eventLocationPickerSelector = popup ? '.p_event-location-picker' : '.event-location-picker';
    var filterFormSelector = popup ? '.p_filter-form' : '.filter-form';
    var closeLocSelector = popup ? '.p_close-loc-submit' : '.close-loc-submit';
    var eventLocationSubmitSelector = popup ? '.p_event-location-submit' : '.event-location-submit';
    var locRadiusSelector = popup ? '#p_loc-radius' : '#loc-radius';

    if ($(placeFieldSelector).length) {
        var placeField = $(placeFieldSelector)[0];
        var countryNameField = $(countryNameFieldSelector)[0];
        var countryField = $(countryFieldSelector)[0];
        var cityField = $(cityFieldSelector)[0];
        var placeNameField = $(placeNameFieldSelector)[0];
        var locationField = $(locationFieldSelector)[0];
        setTimeout(
            function () {
                initGooglePlacesAutocomplete(
                    placeField, countryNameField, countryField, cityField, placeNameField, locationField
                );
            },
            1000
        );
        $(document).off('click', '.filter-field select,.filter-field .event-select-datepicker');
        $(document).on('click', '.filter-field select,.filter-field .event-select-datepicker', function () {
            $(locFormSelector).hide();
        });

        function getOffset(el) {
            var _x = 0;
            var _y = 0;
            while (el && !isNaN(el.offsetLeft) && !isNaN(el.offsetTop)) {
                _x += el.offsetLeft - el.scrollLeft;
                _y += el.offsetTop - el.scrollTop;
                el = el.offsetParent;
            }
            return {top: _y, left: _x};
        }

        $(document).off('click', eventLocationPickerSelector);
        $(document).on('click', eventLocationPickerSelector, function () {
            var eltPos = $(this).offsetParent().position();
            var xPos = getOffset($(filterFormSelector)[0]);
            $(locFormSelector).css({
                top: 50, //xPos.top,
                left: eltPos.left
            }).toggle();
            return false;
        });

        var formAndInputSelector = locFormSelector + ',' + locFormSelector + ' input';
        $(document).off('click', formAndInputSelector);
        $(document).on('click', formAndInputSelector, function () {
            return false;
        });
        $(document).off('click', closeLocSelector);
        $(document).on('click', closeLocSelector, function () {
            $(locFormSelector).hide();
            return false;
        });

        $(document).off('click', eventLocationSubmitSelector);
        $(document).on('click', eventLocationSubmitSelector, function (event) {
            var city = $(cityFieldSelector).val();
            if (city) {
                var coords = $(locationFieldSelector).val().replace(/#/g, '--').replace(/\./g, ',');
                var value = "" + city + "--" + coords + "--" + $(locRadiusSelector).val();
                setFilterElement(
                    'filters', 'event_location', value, false, false, popup
                );
                $(locFormSelector).hide();
            }
            return false;
        });

        $(document).off('change', placeFieldSelector);
        $(document).on('change', placeFieldSelector, function () {
            if ($(placeFieldSelector).val()) {
                $(locRadiusSelector).removeAttr('disabled');
                $(eventLocationSubmitSelector).removeAttr('disabled');
            } else {
                $(locRadiusSelector).attr('disabled', 'disabled');
                $(eventLocationSubmitSelector).attr('disabled', 'disabled');
            }
        });
        $(placeFieldSelector).change();
    }
}

var initSelectEventDatepicker = function () {
  $('.event-select-datepicker').datepicker(
    {
      dateFormat: 'yy-mm-dd',
      // defaultDate: '',
      changeMonth: true,
      beforeShowDay: function(date) {
        var now = new Date();
        now.setHours(0,0,0,0);
        if (date >= now) {
          return [1];
        } else {
          return [0];
        }
      },
      changeYear: true,
      onSelect: function (selectedDate, inst) {
        var fieldName = $(this).attr('name');
        var date = new Date(selectedDate);
        var value = date.getTime();
        $(this).val($(this).attr('placeholder'));
        setFilterElement('filters', fieldName, value, false, false, false);
        return false;
      }
    }
  );
};

var initSelectAgeDatepicker = function () {
  var today = new Date()
  $('.select-datepicker').datepicker(
    {
      dateFormat: 'yy-mm-dd',
      defaultDate: '1990-01-01',
      yearRange: '1900:' + today.getFullYear(),
      changeMonth: true,
      changeYear: true,
      minDate: '1900-01-01',
      maxDate: today,
      onSelect: function (selectedDate, inst) {
        var fieldName = $(this).attr('name');
        var date = new Date(selectedDate);
        var value = date.getTime();
        $(this).val($(this).attr('placeholder'));
        setFilterElement('filters', fieldName, value, false, false, false);
        return false;
      }
    }
  );
};

var fixCompetitionTimelineBlock = function() {
    if ($('.competition-timeline-block').length) {
        var height = 125 + $('.competition-timeline').height()
        $('.competition-timeline-block').height('' + height + 'px');
    }
}

var addFacebookPlugin = function () {
    if ($('.facebook-page-holder').length > 0) {
        console.log('> addFacebookPlugin')
        var pageWidth = $( document ).width();
        var holderWidth = 400;
        if (pageWidth < 415) {
            holderWidth = pageWidth - 30
        }
        var fbPlugin = '' +
            '<div id="fb-root"></div>\n' +
            '<script async defer src="https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v6.0&appId=628712167150475&autoLogAppEvents=1"></script>\n' +
            '<div class="fb-page" data-href="https://www.facebook.com/UlyssesNetwork"\n' +
            'data-tabs="timeline" data-width="' + holderWidth + '" data-height="350" data-small-header="false"\n' +
            'data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">\n' +
            '<blockquote cite="https://www.facebook.com/UlyssesNetwork" class="fb-xfbml-parse-ignore">\n' +
            '<a href="https://www.facebook.com/UlyssesNetwork">Ulysses Network</a>\n' +
            '</blockquote>\n' +
            '</div>';
        $('.facebook-page-holder').html(fbPlugin);
        try {
            FB.XFBML.parse();
        } catch (e) {
            console.warn(e)
        }
    }
}

var onPageLoaded = function(dynamic, callback) {
   $('html, body').scrollTop(0);
   $('.up-to-top').hide();
   textOverflowEllipsis();
   htmlOverflow();

   if (!isBigScreen()) {
    $('.hide-on-md').hide();
   }

   adaptFocusOnContent();
   makeSquare();

   $('a[href="' + window.location.pathname + '"]').each(function (idx, elt) {
     $(elt).closest('li').addClass('active');
   });

   //$("select[multiple=multiple]").attr('data-placeholder', "Select Option(s)");
   $("select[multiple=multiple]").chosen({width: "100%"});

   initSelectEventDatepicker();

   $('.birth-datepicker').datepicker(
      {
          dateFormat: 'yy-mm-dd',
          defaultDate: '1990-01-01',
          changeMonth: true,
          changeYear: true,
      }
  );

  initSelectAgeDatepicker();

  initFileUploads();

  setTimeout(function () {$('ul.user-messages li').slideUp(1000);}, 10000);

  setTimeout(function () {
      selectDiscussion($('.messagebox-init').data('id'));
  }, 500);

  generateMissingAvatars('no-avatar');

  initTinyMce();

  setTextInputCount();

  searchAutocomplete($("#id_q"));

  initWorkForm();

  initShareButtons();

  fixCompetitionTimelineBlock();

  initLocationSearch(false);

  addFacebookPlugin();

  $('body').trigger("pageLoaded");
  if (callback) {
      callback();
  }
};

var loadMessageReplies = function(replies) {
  var detailZone = $('.messagebox-discussion-detail');
  for (var i=0, l=replies.length; i<l; i++){
    var reply = replies[i];
    if (detailZone.find('.message-reply-line[data-id="' + reply.id + '"]').length === 0) {
     // Do not add th eline again if it already exists
     var line = '<div class="message-reply-line" data-id="' + reply.id + '">';
     if (reply.is_group) {
      line += '<div class="message-reply-sent-by">' + reply.sent_by + '</div>';
     }
     line += '<div class="message-reply">';
     line += reply.html_body;
     line += '</div>';
     line += '<div class="message-reply-sent-on">' + reply.sent_on + '</div>';
     line += '</div>';
     var elt = $(line).appendTo(detailZone);
     if (!reply.send_by_me) {
       $(elt).addClass('sent-by-other');
     }
     if (reply.is_attachment) {
       $(elt).addClass('attachment');
     }
     if (reply.is_action) {
      $(elt).addClass('message-action');
     }
    }
  }
  var area = $('.messagebox-discussion-detail');
  area.animate({scrollTop: 10000});
};

var selectDiscussion = function(discussionId) {
  $('.messagebox-discussion[rel="' + discussionId + '"]').click();
};


var refreshDiscussionsList = function(origin) {
  var $elt = $('.messagebox-discussions');
  if ($elt.length) {
    var refresh_url = $elt.data('refresh_url');
    if (refresh_url) {
      $.ajax({
        url: refresh_url,
        contentType: 'html',
        success: function (html) {
          var messageId = $('.messagebox-init').data('id');
          $elt.html(html);
          textOverflowEllipsis();
          $elt.find('.messagebox-discussion[rel="' + messageId + '"]').addClass('active');
          var unread = $elt.find('.messagebox-discussion[rel="' + messageId + '"]').data('unread');
          if (parseInt(unread)) {
            selectDiscussion(messageId);
          }
          generateMissingAvatars('no-avatar');
        }
      })
    }
  }
};

/**
 * Enable elasticsearch autocomplete on a inout field
 * @param $elt
 */
function searchAutocomplete($elt) {
    if ($elt.length === 0) {
        return;
    }
    var autocompleteUrl = $elt.closest('.search-field').data('autocomplete');
    var searchUrl = $elt.closest('.search-field').data('href');
    $elt.autocomplete({
        minLength: 3,
        source: function(request, response) {
            var val = $elt.val();
            $.ajax({
                url: autocompleteUrl,
                dataType: 'json',
                data: {
                    q: val
                },
                success: function (data) {
                    response(data.results);
                }
            });
        },
        select: function(event, ui) {
            console.log(ui);
            //loadUrl(searchUrl + '?q=' + ui.item.value);
        }
    }).data("ui-autocomplete")._renderItem = function (ul, item) {
        return $("<li></li>")
            .data("item.autocomplete", item)
            .append(item.label)
            .appendTo(ul);
    };
}


initWorkForm = function() {

  if ($(".work-form").lenght === 0) {
      return;
  }

  var membersUrl = $('.work-form').data('members-url')

  $("#id_work_creation_date").datepicker({
    changeMonth: true,
    changeYear: true
  });

  var name_to_id = {};
  $("#id_composer_name").autocomplete({
    source: function(request, add){
      $.ajax({
        url: membersUrl + "?term=" + request.term,
        success : function(data) {
          var items = [];
          $.each(data, function(i, val){
              name_to_id[val.name] = val.id; //store in local dict
              items.push(val.name);
          });
          add(items);
        }
      });
    },
    select: function(event, ui) {
      var id = name_to_id[ui.item.value]; //get id from local dict
      if (id) {
          $("#id_composer").val(id);
      }
      $("#id_composer_name").attr('value', ui.item.value);
    }
  });

  selectWithAutocomplete("id_performers", membersUrl);

  var placeField = $("#id_premiere_venue")[0];
  var countryNameField = $("#id_country_name")[0];
  var countryField = $("#id_country")[0];
  var cityField = $("#id_city")[0];
  var placeNameField = $("#id_place_name")[0];
  var locationField = $("#id_location")[0];

  initGooglePlacesAutocomplete(
      placeField, countryNameField, countryField, cityField, placeNameField, locationField
  );

  $(document).on('submit', 'form.save-work', function (event) {

    // The click on the submit cause checked=0 and it should trigger a checking of the data
    // If some warning, we display a popup for confirmation
    // If ok or user confirmed, we resubmit by js with checked=1
    var checked = $(this).data('checked');

    if (checked) {
        // ok we can submit the form
    } else {
      event.stopPropagation();
      event.preventDefault();

      var warnings = [];
      var fields = [
        ['You did not select any composer', '#id_composer_name'],
        ['You did not select any date of work creation', '#id_work_creation_date'],
        ['You did not select any duration', '#id_duration'],
        ['You did not select any media', '#id_audio_media', '#id_video_media', '#id_score_media']
      ];

      // build the list of warnings
      for (var i = 0, l = fields.length; i < l; i++) {
        var message = fields[i][0];
        var field_ids = fields[i];
        var hasValue = false;
        for (var i2 = 1, l2 = field_ids.length; i2 < l2 && !hasValue; i2++) {
          var selector = field_ids[i2];
          console.log('>', i2, selector, !$(selector).length, $(selector).val());
          if ($(selector).val()) {
              hasValue = true;
          }
        }
        if (!hasValue) {
          warnings.push(message);
        }
      }

      var $that = $(this);
      if (warnings.length > 0) {
        var text = warnings.join('<br />');
        confirmPopup(
          'Are you sure you want to save?',
          text,
          function () {
            $that.data('checked', 1);
            $that.submit();
            return false;
          },
          function () {
            return false;
          }
        );
      } else {
        $that.data('checked', 1);
        $that.submit();
      }
    }
  });
};

setTitleIconsPosition = function() {
  $('.h1-icons').each(
    function (idx, elt) {
      $(elt).hide();
      var holder = $(elt).closest('.h1-holder');
      var h1Text = holder.find('.h1-text');
      var h1TextPos = h1Text.position();
      var marginLeft = 10;
      var h1TextLeft = h1TextPos.left;
      var h1TextWidth = holder.width() - (2 * h1TextLeft);
      if ((h1Text.width() + h1TextLeft + marginLeft) < (holder.width() - $(elt).width())) {
        var top = h1TextPos.top;
        var right = h1TextPos.left + h1Text.width() + marginLeft;
        $(elt).css({position: 'absolute', top: top, left: right});
        $('h1.competition-banner-title').css('margin-bottom', '');
      } else {
        $(elt).removeAttr('style');
        $('h1.competition-banner-title').css('margin-bottom', '0px');
      }
      $(elt).show();
    }
  );
};

initShareButtons = function () {
  $(".bottom-tooltip").tooltip({
    position: {
      my: "center top+10",
      at: "center bottom",
      using: function( position, feedback ) {
        $( this ).css( position );
      }
    },
    tooltipClass: 'top'
  });

  // Share on Facebook button
  $("a.facebook-button").click(function() {
    try{
      var loc = $(this).attr('href');
      var title = $(this).data('title');
      window.open(
        'http://www.facebook.com/sharer/sharer.php?u=' + loc + '&t='+title,
        'facebook_share',
        'height=320, width=640, toolbar=no, menubar=no, scrollbars=no, resizable=no, location=no, directories=no, status=no'
      );
    } catch (err) {
      alert(err);
    }
    return false;
  });

  $("a.twitter-button").click(function() {
    try{
      var loc = $(this).attr('href');
      var title = $(this).data('title');
      window.open(
        'http://twitter.com/share?url=' + loc + '&text=' + title + '&', 'twitterwindow',
        'height=450, width=550, toolbar=0, location=0, menubar=0, directories=0, scrollbars=0'
      );
    } catch (err) {
      alert(err);
    }
    return false;
  });

  setTitleIconsPosition();
};

$(document).ready(function() {

  onPageLoaded();

  setInterval(function () {refreshDiscussionsList("auto");}, 30 * 1000);

  $(window).on("popstate", function () {
    // if the state is the page you expect, pull the name and load it.
    if (!history.state) {
        // no history, hide the back button or something
        //this.back.fadeOut();
        return;
    } else {
        // reload the page (don't add it to history)
        loadUrl(history.state.url, true);
    }
  });

  $(window).on("scroll", function () {
    var scroll = $(window).scrollTop();
    if (scroll > 20) {
      $('.up-to-top').show();
    } else {
      $('.up-to-top').hide();
    }
    scrollHelpMenu(scroll);
  });

  $('body').on('click', '.help-menu h3', function() {
    $(this).next('ul').find('li').toggle();
    return false;
  });

  $('body').on('click', '.up-to-top', function() {
    $('html, body').animate({scrollTop: 0}, 500);
    return false;
  });

  function handleOutboundLinkClicks(href, target) {
      // Google Analytics : Tracks external links
      var visitDone = {done: false};
      var visitPage = function (message, link) {
          if (!visitDone.done) {
            visitDone.done = true;  // Do not visit again
            console.log(message, link);
            window.open(link, '_blank');
          }
      };
      setTimeout(function () {
          visitPage('ga timeout', href);
      }, 1000);

      try {
          if (GA.track) {
            GA.track(href, function(link) {
              visitPage('ga returned', link);
            });
          } else {
            visitPage('ga not set', href);
          }
      } catch (e) {
          console.log('error', e);
          visitPage('ga error', href);
      }
  }


  $('body').on('click', 'a', function () {
    if ((!$(this).hasClass('colorbox-form')) && (!$(this).hasClass('cancel_button'))) {
      var href = $(this).attr('href');
      var isTarget = href && (href[0] === '#');
      var noAjax = $(this).hasClass('no-ajax');
      var target = $(this).attr('target');
      var js = href === "javascript:;";
      if (target === '_blank') {
        handleOutboundLinkClicks(href, target);
        return false;
      } else if ((noAjax) || (isTarget)) {
        return true;
      }
      if (href && !js) {
        loadUrl(href);
      }
      return false;
    }
  });

  // Set the order by field on the search form (this field is not in the form so we need to synchronize
  $('body').on('click', '.order-by-links a', function () {
    // insertUrlParam("order_by", );
    var fieldName = $(this).attr('rel');
    var value = 1;
    clearFilterElements('counter');
    var isPopup = false;  // No order links in create alert for events
    setFilterElement('orders', fieldName, value, false, true, isPopup);
    return false;
  });

  $(document).on('click', '.disable-double-click form input[type=submit]', function() {
    $(this).prop('disabled', true);
    $(this).closest('form').submit();
    setTimeout(function () {
      $(this).prop('disabled', false);
    }, 1000);
    return false;
  });

  // show full content when clicking ... on a post
  $(document).on('click', '.html-overflow-show-content', function () {
      $(this).closest('.html-overflow-container').find(
          '.html-overflow'
      ).css('max-height', 'none').removeClass('has-overflow');
      $(this).hide();
  });

  $(document).on('click', 'ul.user-messages li img.exit-icon', function () {
    $(this).parent().hide('slow');
    return false;
  });

  $(document).on('click', '.work-item-foreground', function () {
      loadUrl($(this).data('href'));
      return false;
  });

  /** search */
  $(document).on('keypress', '.search-field input', function (event) {
    if (event.keyCode === 13) {
      var href = $(this).closest('.search-field').data('href');
      var value = $(this).val();
      loadUrl(href + "?q=" + value);
      return false;
    }
  });

  $(document).on('click', '.search-field img', function (event) {
    var href = $(this).closest('.search-field').data('href');
    var value = $(this).closest('.search-field').find('input').val();
    loadUrl(href + "?q=" + value);
    return false;
  });
  /* end search */

  $(document).on('uploadDone', '.messagebox-new-message', function (data) {
     if ($('.message-body-input').html()) {
       $('.message-body-input').show();
       $('.message-body-placeholder').hide();
     } else {
       $('.message-body-input').hide();
       $('.message-body-placeholder').show();
     }
     $('.messagebox-send-message').show();
     setTimeout(function () {
        refreshDiscussionsList('after-upload');  // short delay. let the message be created
     }, 1000);

  });

  $(document).on('fileUploaded', '.profileimage-upload .fileupload-target', function (event, data) {
      $(this).closest('.profileimage-upload').find('.profileimage-avatar img').attr('src', data.file_path);
  });

  $(document).on('click', '.trash-profile-image', function () {
     var avatar = $(this).closest('.profileimage-upload').find('.profileimage-avatar img');
     generateMissingAvatar(avatar.get(0));
     var target = $(this).data('target');
     $("#" + target).val('');
     return false;
  });

  $(document).on('click', '.messagebox-discussion', function () {
      $('.messagebox-discussion').removeClass('active');
      var detailZone = $('.messagebox-discussion-detail');
      var messageBar = $('.messagebox-new-message-bar');
      $(this).addClass('active');
      $('.message-box-header-discussion').html($(this).html());
      messageBar.show();
      $('.messagebox-send-message').attr('rel', $(this).data('post_url'));
      $('.fileupload-button').data('url', $(this).data('attachment_url'));
      $('a.delete-message-button').attr('href', $(this).data('delete_url'));
      var isParticipant = $(this).data('is_participant');

      var exit_url = $(this).data('exit_url');
      if (exit_url) {
        $('a.exit-message-button').attr('href', exit_url).show();
      } else {
        $('a.exit-message-button').hide();
      }
      var duplicate_url = $(this).data('duplicate_url');
      if (duplicate_url) {
        $('a.duplicate-message-button').attr('href', duplicate_url).show();
      } else {
        $('a.duplicate-message-button').hide();
      }
      var update_subject_url = $(this).data('update_subject_url');
      if (update_subject_url) {
        $('a.update-subject-button').attr('href', update_subject_url).show();
      } else {
        $('a.update-subject-button').hide();
      }
      var add_participant_url = $(this).data('add_participant_url');
      if (add_participant_url) {
        $('a.add-participant-button').attr('href', add_participant_url).show();
      } else {
        $('a.add-participant-button').hide();
      }

      $('.message-box-help-zone-participants').html(
        $(this).find('.messagebox-discussion-participants').html()
      );
      $('.message-box-help-zone-attachments').html(
        $(this).find('.messagebox-discussion-attachments').html()
      );
      fitMessageBoxHelpZone();
      $('.messagebox-init').data('id', $(this).attr('rel'));

      if (isParticipant) {
          messageBar.show();
          $('.messagebox-new-message-disabled').hide();
      } else {
          messageBar.hide();
          $('.messagebox-new-message-disabled').show();
      }

      textOverflowEllipsis();
      var that = $(this);
      var currentHref = detailZone.data('current-href');
      var url = $(this).data('href');
      if (currentHref !== url) {
        detailZone.html('');
        detailZone.data('current-href', url);
      }
      $.ajax({
        url: url,
        success: function (replies) {
          that.removeClass('unread');
          loadMessageReplies(replies);
        }
      });
      return false;
  });

  /** Datetime widget */
  $(document).on('change', ".datetime-widget .datepicker,.datetime-widget .timepicker", function() {
    var datetimeWidget = $(this).closest(".datetime-widget");
    var dateValue = datetimeWidget.find(".datepicker").val();
    var timeValue = datetimeWidget.find(".timepicker").val();
    var target = datetimeWidget.find('.datetime-value');

    if (dateValue) {
      target.val(dateValue + ' ' + timeValue);
    } else {
      target.val('');
    }
  });

  $(document).on('click', ".datetime-widget .time-all-day", function() {
    var datetimeWidget = $(this).closest(".datetime-widget");
    setDatetimeWidgetTime(datetimeWidget);
  });

  $(document).on('click', '.message-body-placeholder', function () {
    $(this).hide();
    $('.message-body-input').show();
    $('.message-body-input').focus();
  });

  $(document).on('click', '.message-body-input', function () {
    if ($(this).text() === '') {
      $(this).hide();
      $('.message-body-placeholder').show();
    }
  });

  $(document).on('blur', '.message-body-input', function () {
    if ($(this).text() === '') {
      $(this).hide();
      $('.message-body-placeholder').show();
    }
  });

  $(document).on('click', '.messagebox-send-message', function () {
    var url = $(this).attr('rel');
    var html = $('.messagebox-new-message .message-body-input').html().replace(/<div>/gi,'<br />').replace(/<\/div>/gi,'');
    $('.messagebox-new-message .message-body-input').html('');

    var token = $('input[name="csrfmiddlewaretoken"]').val();
    if (url && html) {
      $.post(
        url,
        {
          body: html,
          csrfmiddlewaretoken: token
        },
        function (replies) {
          // $('.messagebox-discussion-detail').html('');
          loadMessageReplies(replies);
        }
      );
    }
    return false;
  });

  $(document).on('click', '.message-box-help img', function () {
      $('.message-box-help-zone').toggle(100, function () {
          textOverflowEllipsis();
          fitMessageBoxHelpZone();
      });
  });

  $(document).on('change', '.filter-field select', function () {
      var value = $(this).val();
      if (value) {
        $(this).val('');
        clearFilterElements('counter');
        value = parseInt(value);
        var text = $(this).find('option:selected').text();
        var fieldName = $(this).attr('name');
        var isPopup = $(this).closest('.filter-field').hasClass('popup');
        setFilterElement('filters', fieldName, value, false, false, isPopup);
      }
  });

  $(document).on('click', '.work-audio', function () {
     var that = $(this);
     var playListElement = {};
     playListElement.title = that.data('title');
     playListElement.playerImage = that.data('image');
     playListElement.audio = that.data('audio');
     playListElement.owner = that.data('owner');
     playListElement.workUrl = that.data('work_url');
     playListElement.ownerUrl = that.data('owner_url');
     playListElement.ownerAvatar = that.data('owner_avatar');
     playListElement.ownerAvatarClass = that.data('owner_avatar_class');
     playListElement.ownerFollowersCount = that.data('owner_followers_count');
     playListElement.ownerWorksCount = that.data('owner_works_count');
     playListElement.isBookmarked = that.data('is_bookmarked');
     playListElement.bookmarkUrl = that.data('bookmark_url');
     playListElement.refreshUrl = that.data('refresh_url');
     playListElement.composer = that.data('composer');
     var currentTrack = getCurrentTrack() + 1;
     playlist.splice(currentTrack, 0, playListElement);
     nextButton.click();
     setTimeout(playTrack, 1000);
  });

  $(document).on('click', '.filter-fields-item-remove', function () {
    clearFilterElements('counter');
    var elt = $(this).closest('.filter-fields-item');
    var value = parseInt(elt.data('value'));
    if (isNaN(value)) {
        value = elt.data('value');
    }
    var fieldName = elt.data('key');
    var isPopup = $(this).hasClass('popup');
    setFilterElement('filters', fieldName, value, true, false, isPopup);
    elt.remove();
  });

  $(document).on('click', '.load-more-button', function () {
    var step = parseInt($(this).attr('rel'));
    var value = $('.list-item .item:visible').length + step;
    var isPopup = false;   // No loadmore in create events alert popup
    setFilterElement('counter', 'items', value, false, true, isPopup);
    return false;
  });

  $(document).on('click', '.load-more-page', function () {
    var url = $(this).data('href');
    var scrollPosition = $(document).scrollTop();
    loadUrl(url, false, function () {
      $(document).scrollTop(scrollPosition);
    });
    return false;
  });

  $(document).on('click', '.filter-fields-items-clear', function () {
    var step = parseInt($(this).attr('rel'));
    
    var isPopup = $(this).hasClass('popup');
    // remove all filters for the hidden input
    clearFilterElements('filters');

    // rebuild url
    var form = $('.filter-form' + (isPopup ? '.popup' : ':not(.popup)'));
    var filterInput = form.find('.filter-field input[name=filter]');
    var queryString = "";
    $.each(JSON.parse(filterInput.val()), function(key1, elt1) {
      var elements = '';
      for (var j=0; j<elt1.length; j++) {
        var elt2 = elt1[j];
        $.each(elt2, function(key3, elt3){
          elements += key3 + ':' + elt3 + '$';
        });
      }
      queryString = buildUrlParam(key1, elements, queryString);
    });
    // reload page with new url
    var url = document.location.pathname + "?" + queryString;
    var scrollPosition = $(document).scrollTop();
    loadUrl(url, false, function () {
      $(document).scrollTop(scrollPosition);
    });

    return false;
  });

  $(document).on('click', '.application-remove-document,.application-remove-media', function () {
    var $field = $(this).closest(".upload-field").find('.form-control');
    var id = $field.attr('id');
    $field.html('');
    $("#" + id + "_hidden").val('');
    return false;
  });

  $(document).on('click', 'input[type=submit].submit-confirm', function () {
    var title = $(this).data('title');
    var message = $(this).data('message');
    var inputName = $(this).attr('name');
    var that = $(this);
    confirmPopup(title, message, function () {
      if (inputName) {
        that.append('<input type="hidden" name="' + inputName + '" value="' + inputName + '" /> ');
      }
      $("form.submit-confirm").submit();
      return false;
    });
    return false;
  });

  $(document).on('click', '.follow-click', function () {
    var url = $(this).data('href');
    var $this = $(this);
    if ($this.hasClass('no-dbl-click')) {
        return;
    }
    $this.addClass('no-dbl-click');  // avoid double click click
    $.ajax(
      {
        url: url,
        success: function (data) {
          var $parent = $this.closest('.member-action');
          $parent.find('.follow-click').hide();
          if (data.following) {
            $parent.find('.unfollow-btn').show();
          } else {
            $parent.find('.follow-btn').show();
          }
          $this.removeClass('no-dbl-click');  // restore click
        },
        error: function (err) {
          console.error(err);
          $this.removeClass('no-dbl-click');  // restore click
        }
      }
    )
  });

  $(document).on('click', '.select-elements li', function () {
    $(".select-target").val($(this).attr('rel'));
    $('form.colorbox-form').submit();
  });

  var replaceUlysesTextByLogo = function (fieldId) {
    var labelSelector = 'label[for="' + fieldId + '"]';
    var text = $(labelSelector).text();
    var htmlText = text.replace('Ulysses', '<img src="/static/img/ulysses-name.png" class="ulysses-name" />');
    $(labelSelector).html(htmlText);
  };
  replaceUlysesTextByLogo('id_ulysses_label');
  replaceUlysesTextByLogo('id_ulysses_partner');

  $(document).on('mouseenter', '.nav-dropdown-reminder', function () {
    var href = $(this).data('href');
    if (href) {
      $.ajax({
        url: href,
        success: function (data) {
        }
      })
    }
  });

  $('.community-news .share-item-description .share-item-description-content').each(function (idx, elt) {
    if (elt.offsetHeight < elt.scrollHeight) {
        // The element has overflow
        $(elt).closest('.share-item-description').addClass('with-overflow');
    }
  });

  $('.logged-block.community-news .item .item-content,.logged-block.looking-for .item .item-content').each(function (idx, elt) {
    if (elt.offsetHeight < elt.scrollHeight) {
        // The element has overflow
        $(elt).addClass('with-overflow');
        $(elt).parent().find(".overflow-indicator").show();
    }
  });

  // $(".news-share-bar a").tooltip({
  //   position: {
  //     my: "center top+10",
  //     at: "center bottom",
  //     using: function( position, feedback ) {
  //       $( this ).css( position );
  //     }
  //   },
  //   tooltipClass: 'top'
  // });

  var rowEqHeight = function(){
    $(".row-eq-height").each(function (idx, elt) {
      var maxHeight = 0;
      $(elt).children().css({height: 'auto'});
      if ($(window).width() > 768) {
        $(elt).children().each(function (idx2, elt2) {
          maxHeight = Math.max(maxHeight, $(elt2).height());
        });
        $(elt).children().css({'min-height': maxHeight}).children().css({'min-height': maxHeight});
      }
    });
  };

  //$('form.auto-form select').change(getFilter(window.location));
  // Auto sumbmit form : for example filter form on Community news
  $(document).on('change', 'form.auto-form select', function() {
    $('form.auto-form').submit();
  });
  $(document).on('keydown', 'form.auto-form input[type=text]', function(event) {
    if (event.key === 13) {
      event.stopPropagation();
      $('form.auto-form').submit();
    }
  });
  $(document).on('click', 'form.auto-form input[type=checkbox]', function(event) {
    $('form.auto-form').submit();
  });

  // Cookie Bar
  $(document).on('click', '.cookie-bar .hide-cookie-bar', function () {
    $('.cookie-bar').hide("slide", {direction: "down"}, 1000, function () {
      $('.cookie-bar-shown').removeClass('cookie-bar-shown');
    });
    var hideCookieBarHref = $(this).data('href');
    $.post(
      hideCookieBarHref,
      function(data) {
        console.log('hide_accept_cookies_message', data);
      }
    );
    return false;
  });

  $(document).on('click', '.info-box .close-info-box', function () {
    $(this).closest('.info-box').hide("slide", {direction: "up"}, 1000, function () {
    });
    var hideInfoboxHref = $(this).data('href');
    $.post(
      hideInfoboxHref,
      function(data) {
        console.log('hide_info_box', data);
      }
    );
    return false;
  });

  $(document).on('click', '.btn-toggle', function () {
     var targetSelector = $(this).data('target');
     $(targetSelector).siblings('.btn-toggle-target').hide();
     $(targetSelector).toggle({direction: "down"});
     return false;
  });

  $(document).on('click', "a.btn-disabled,a.disabled", function () {
    return false;
  });

  $(document).on('click', "a.btn-active", function () {
    return false;
  });

  $(document).on('click', ".feed-item", function () {
    window.location = $(this).attr('href');
    return false;
  });

  rowEqHeight();
  $(window).resize(function() {
    rowEqHeight();
    setTitleIconsPosition();
    fitMessageBoxHelpZone();
  });
  $('img').load(function() {
    rowEqHeight();
    setTitleIconsPosition();
  });

  // colorbox-form links open in popup
  $("a.colorbox-form").colorboxify();

});

