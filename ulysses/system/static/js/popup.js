/* Fork of Django-admin javascript functions */

// IE doesn't accept periods or dashes in the window name, but the element IDs
// we use to generate popup window names may contain them, therefore we map them
// to allowed characters in a reversible way so that we can locate the correct 
// element when the popup window is dismissed.
function encode_windowname(text) {
    text = text.replace(/\./g, '__dot__');
    text = text.replace(/\-/g, '__dash__');
    return text;
}

function decode_windowname(text) {
    text = text.replace(/__dot__/g, '.');
    text = text.replace(/__dash__/g, '-');
    return text;
}

function show_popup(url,name) {
    /* name = id of the widget to be updated in opener window */
    var win = window.open(url, encode_windowname(name), 'height=500,width=850,resizable=yes,scrollbars=yes');    
    win.focus();        
    return false;
}

