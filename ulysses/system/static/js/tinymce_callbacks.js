var setupInitPlainText = function(ed) {
    ed.onInit.add(
        function (ed) {
            ed.pasteAsPlainText = true;
            //adding handlers crossbrowser
            if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
                ed.onKeyDown.add(
                    function (ed, e) {
                        if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                            ed.pasteAsPlainText = true;
                    }
                );
            } else {
                ed.onPaste.addToTop(
                    function (ed, e) {
                        ed.pasteAsPlainText = true;
                    }
                );
            }
        }
    )
};

var pastePreprocessHandleLinebreaks = function(pl, o) {
    // Content string containing the plain text from the clipboard
    // replace \n by <br /> in order to keep linbe breaks
    o.content = o.content.split('\n').join('<br />');
};
