var isTinyMCEMaxLengthReached = function (editor) {
    var content = editor.getBody().textContent;
    var charCount = content.length;
    var maximumLength = $('#' + editor.id).data('maximum_length');
    if (maximumLength && (charCount === maximumLength)) {
        return 1;
    }
    if (maximumLength && (charCount > maximumLength)) {
        return 2;
    }
    return 0;
};


var setTinyMCECharCount = function (editor) {
    var content = editor.getBody().textContent;
    var charCount = content.length;
    var maximumLength = $('#' + editor.id).data('maximum_length');
    var text = '' + charCount;
    if (maximumLength) {
        text += ' / ' + maximumLength;
    }
    var $elt = $("#word_counter_" + editor.id);
    $elt.text(text);
    var classError = 'max-length-error';
    if (maximumLength && (charCount >= maximumLength)) {
        $elt.addClass('max-length-error');
    } else {
        $elt.removeClass('max-length-error');
    }
};


var setCounterValue = function($elt, maxLength) {
    var val = $elt.val();
    var target = $("#word_counter_" + $elt.attr('id'));
    target.text('' + val.length + '/' + maxLength);
};

var setTextInputCount = function () {
    $("input.maxlength").each(function (idx, elt) {
        var maxLength = $(elt).attr('maxlength');
        if (maxLength > 0 ) {
            setCounterValue($(elt), maxLength);
            $(elt).change(function () {
                setCounterValue($(this), maxLength);
            });
            $(elt).keydown(function () {
                setCounterValue($(this), maxLength);
            });
        }
    })
};

$(document).on('ready', function () {
  $(document).on('click', '.clear-tinymce', function () {
     var rel = $(this).attr('rel');
     tinyMCE.getInstanceById(rel).setContent('');
     setTinyMCECharCount(tinyMCE.getInstanceById(rel));
     return false;
  });
});

var initTinyMce = function(exact, attrs) {
    var options = {
        mode : "specific_textareas",
        theme : "advanced",
        height : (attrs && attrs.height)?attrs.height:"480",
        width: (attrs && attrs.width)?attrs.width:"100%",
        plugins : "table,paste",
        theme_advanced_resizing : "true",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_toolbar_location : "top",
        theme_advanced_disable : "image,anchor",
        theme_advanced_blockformats : "p,h2,h3,h4,h5,h6",
        theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
        setup : function(ed) {
            ed.onInit.add(function(ed) {
                attrs = attrs || {};
                if (!attrs.fontSize) {
                    attrs.fontSize = 16;
                }
                this.getDoc().body.style.fontSize = attrs.fontSize;
                this.getDoc().body.style.fontFamily = 'muliregular';

                ed.execCommand("fontName", false, 'muliregular');
                ed.execCommand("fontSize", false, '16');

                var allowedKeys = [8, 37, 38, 39, 40, 46, 88]; // backspace, delete and cursor keys + Ctrl X
                var spaceKey = 32; // space

                setTinyMCECharCount(ed);
                ed.onKeyDown.add(function (ed, e) {
                    setTinyMCECharCount(ed);
                    if (allowedKeys.indexOf(e.keyCode) !== -1) {
                        return true;
                    }
                    var reached = isTinyMCEMaxLengthReached(ed);
                    if (reached > 0) {
                        // si egal a la limite mais different de espace ou plus que la limite
                        e.preventDefault();
                        e.stopPropagation();
                        return false;
                    }
                });
                ed.pasteAsPlainText = true;
                //adding handlers crossbrowser
                if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
                    ed.onKeyDown.add(function (ed, e) {
                        if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                            ed.pasteAsPlainText = true;
                    });
                } else {
                    ed.onPaste.addToTop(function (ed, e) {
                        ed.pasteAsPlainText = true;
                    });
                }
            });
            if ($('#' + ed.id).data('readonly') || $('#' + ed.id).attr('readonly')) {
                ed.settings.readonly = true;
            }
            var elements = [
                'theme_advanced_statusbar_location', 'theme_advanced_buttons1', 'theme_advanced_buttons2',
                'theme_advanced_buttons3', "height"
            ];
            for (var i=0; i<elements.length; i++) {
                var element = elements[i];
                var value = $('#' + ed.id).data(element);
                if (value !== undefined) {
                    ed.settings[element] = value;
                }
            }
        },
        paste_auto_cleanup_on_paste : true,
        paste_preprocess : function(pl, o) {
            // Content string containing the plain text from the clipboard
            // replace \n by <br /> in order to keep linbe breaks
            o.content = o.content.split('\n').join('<br />');
        },
        paste_postprocess : function(pl, o) {
            // Content DOM node containing the DOM structure of the clipboard
            // o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
        }
    };
    if (exact) {
        options.mode = 'exact';
        options.elements = exact;
    } else {
        options.editor_selector = "tinymce";
    }
    tinyMCE.init(options);
};
