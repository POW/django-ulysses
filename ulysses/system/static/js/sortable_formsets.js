/* sortable formset plugin */


function make_sortable(the_formset) {

    var update_order_indexes = function() {
	$.each($(the_formset).find(".formset-form"),function(index,value){
		      order_index_field = $(value).find(".order-index")
		      $(order_index_field).val(index+1);
		  });
    }

    var make_sortable = function() {
	the_formset.sortable({
	  axis: "y",
	  stop: function(event, ui) {
		update_order_indexes();
             }
        });
	/* Perform initial order index update */
	update_order_indexes();
    }

    make_sortable();

    /* Return this, to ensure chainability */
    return this;
  };



