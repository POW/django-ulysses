var setFileUpload = function ($elt) {
  var targetSelector = $elt.data('target');
  var pathSelector = $elt.data('path');
  var statusSelector = $elt.data('status');
  var hideOnAddSelector = $elt.data('hide');
  var doneSelector = $elt.data('done');
  $(statusSelector).hide();
  $elt.fileupload({
    add: function (e, data) {
      data.url = $(this).data('url');
      $(targetSelector).val("");
      $(statusSelector).removeClass('error').removeClass('success').hide();

      var ext = $(this).data('extensions');
      if (!ext) {
        ext = '.+';
      }
      var allowedTypes = new RegExp("(\.|\/)(" + ext + ")$", "i");
      var theFile = data.files[0];
      if (allowedTypes.test(theFile.type) || allowedTypes.test(theFile.name)) {
        data.submit();
      } else {
        $(statusSelector).addClass('error').text('This extension is not supported.').show();
      }
    },
    progressall: function (e, data) {
      var progress = parseInt(data.loaded / data.total * 100, 10);
      $(statusSelector).addClass('success').text('Uploading: ' + progress + '%').show();
      if (progress === 100) {
        console.log("UPLOAD 100%", doneSelector);
        if (doneSelector) {
          $(doneSelector).trigger("uploadDone", data);
          $(statusSelector).hide();
        }
      }
    },
    done: function (e, data) {
      if (data.result.filename) {
        $(targetSelector).val(data.result.filename);
        $(pathSelector).val(data.result.file_path);
      } else {
        $(targetSelector).val(data.result);
        $(pathSelector).val('?');
      }
      console.log("UPLOAD done", data, targetSelector);
      $(targetSelector).trigger("fileUploaded", data.result);
      $(statusSelector).addClass('success').show().text('OK');
      setTimeout(function () {
        $(statusSelector).hide(500);
      }, 2500);
    }
  });
};

var initFileUploads = function() {
  $('.fileupload-button').each(
      function (idx, elt) {
          setFileUpload($(elt));
      }
  )
};

