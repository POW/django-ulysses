tinyMCE.init({
  mode : 'textareas',
  theme : "advanced",
  height : "480",
  plugins : "table,paste",
  theme_advanced_buttons3_add : "tablecontrols",
  theme_advanced_resizing : "true",
  theme_advanced_statusbar_location : "bottom",
  theme_advanced_toolbar_location : "top",
  theme_advanced_disable : "image,anchor",
  theme_advanced_blockformats : "h4,p",
  theme_advanced_buttons3_add : "pastetext,pasteword,selectall",
  setup : function(ed) {
    ed.onInit.add(function(ed) {
      ed.pasteAsPlainText = true;
      //adding handlers crossbrowser
      if (tinymce.isOpera || /Firefox\/2/.test(navigator.userAgent)) {
          ed.onKeyDown.add(function (ed, e) {
              if (((tinymce.isMac ? e.metaKey : e.ctrlKey) && e.keyCode == 86) || (e.shiftKey && e.keyCode == 45))
                  ed.pasteAsPlainText = true;
          });
      } else {
          ed.onPaste.addToTop(function (ed, e) {
              ed.pasteAsPlainText = true;
          });
      }
    });
  },
  paste_auto_cleanup_on_paste : true,
  paste_preprocess : function(pl, o) {
    // Content string containing the plain text from the clipboard
    // replace \n by <br /> in order to keep linbe breaks
    o.content = o.content.split('\n').join('<br />');
  },
  paste_postprocess : function(pl, o) {
    // Content DOM node containing the DOM structure of the clipboard
    // o.node.innerHTML = o.node.innerHTML + "\n-: CLEANED :-";
  }
});