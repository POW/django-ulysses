function select_element(button,url) {
    /* Determine target id from button id */
    /* button_id = <target_id>_select     */    
    var button_id = button.attr("id")
    var target_id = button_id.substr(0,button_id.length-7);
    var competition_url = $("#competition_url").val(); 
    var popup_url = "/web/competitions/"+competition_url+"/"+url;    
    show_popup(popup_url,target_id);    
}

function select_biographic_element(button) {
    select_element(button,"choose_biographic_element");    
}

function select_document(button) {
    select_element(button,"choose_document");    
}

function select_work(button) {
    select_element(button,"choose_work");    
}

function upload_element(button,url) {
    /* Determine target id from button id */
    /* button_id = <target_id>_upload     */    
    var button_id = button.attr("id")
    var target_id = button_id.substr(0,button_id.length-7);
    var key = target_id.substr(3);
    var competition_url = $("#competition_url").val(); 
    var popup_url = "/web/competitions/"+competition_url+"/"+url+"/"+key; 
    show_popup(popup_url,target_id);    
}

function upload_document(button) {
    upload_element(button,"upload_document");
}

function upload_work(button) {
    upload_element(button,"upload_work");
}

$(function() {
   
   /* 'Select biographic element' button */
    $(".select-bio-button").click( function(){
        select_biographic_element($(this));        
    });
    
    /* 'Select document' button */
    $(".select-doc-button").click( function(){        
        select_document($(this));        
    });
    
    /* 'Upload document button */
    $(".upload-doc-button").click( function(){        
        upload_document($(this));        
    });
    
    /* 'Select work' button */
    $(".select-work-button").click( function(){        
        select_work($(this));        
    });
    
    /* 'Upload work button */
    $(".upload-work-button").click( function(){        
        upload_work($(this));        
    });        
        
    $("div.choose-element li.action").click( function(){                
        var prefix_length = 7; /* Prefix = "button_" */
        var action_id = $(this).attr("id"); /* Should be button_<action_name> */
        var selected_element_name = action_id.substr(prefix_length);
        /* Iterate through actions and show selected zone */        
        $("li.action").each(function(index,elt) {            
            var element_name = $(this).attr("id").substr(prefix_length);
            var zone_key = "#zone_"+element_name;
            if (element_name==selected_element_name) {
                $(zone_key).show();
            }
            else {
                $(zone_key).hide();
            }            
        });        
        
    });
    
});

function get_loading_html(message) {    
    return "<span class='loading-message'><img class='loading-image' src='/static/img/wait_animated.gif'/>"+message+"</span>";    
}

function dismissSelectElementPopup(win, chosenId) {
    
    /* Get target_input & entity name from window name */
    var target_id = decode_windowname(win.name);        
    var tokens = chosenId.split('_');
    var category = tokens[0];  /* 'personal' or 'history' */
    var type = tokens[1] /* 'bio' or 'doc' */
    var competition_id = tokens[2];
    var element_id = tokens[3];
    var key = target_id.substring(3);    
    var url, suffix, type;
    var loading_html;
    
    if (category=="personal") {                               
        /* Personal space element */        
        if (type=="bio") {                               
            url = "/web/ajax/get_biographic_element?id="+element_id;
        }
        else if (type=="doc") {
            loading_html = get_loading_html("Importing document from your personal space...");
            url = "/web/ajax/import_document?competition="+competition_id+"&key="+key+"&id="+element_id;            
        }
        else if (type=="work") {
            loading_html = get_loading_html("Importing work from your personal space...");
            url = "/web/ajax/import_work?competition="+competition_id+"&key="+key+"&id="+element_id;
        }
        else if (type=="ulysseswork") {
            loading_html = get_loading_html("Importing work from your ULysses personal space...");
            url = "/web/ajax/import_ulysses_work?competition="+competition_id+"&key="+key+"&id="+element_id;
        }
        else  {
            console.log("unhandled type : " + type);   
        }
    }
    else if (category=="history") {   
        /* Application history element */        
        if (type=="bio") {            
            url = "/web/ajax/get_candidate_biographic_element?id="+element_id;            
        }
        else if (type=="doc") {
            loading_html = get_loading_html("Importing document from your application history...");
            url = "/web/ajax/import_candidate_document?competition="+competition_id+"&key="+key+"&id="+element_id;            
            type = "doc";
        }
        else if (type=="work") {
            loading_html = get_loading_html("Importing work from your application history...");
            url = "/web/ajax/import_candidate_work?competition="+competition_id+"&key="+key+"&id="+element_id;            
            type = "doc";
        }
        else  {
            console.log("unhandled type : >" + type);
        }                
    }
    else {
        console.log("unhandled category : " + category);   
    }

    if (type=="bio") {
        $.getJSON(url, function(html) {        
            tinyMCE.get(target_id).setContent(html);
        });
    } else if (type=="doc") {
        $("#"+target_id).html(loading_html);
        $.getJSON(url, function(output) {            
            $("#"+target_id).html(output["html"]);
            $("#"+target_id+"_hidden").val(output["id"]);
        });    
    } else if ((type=="work") || (type=="ulysseswork")) {
        $("#"+target_id).html(loading_html);
        $.getJSON(url, function(output) {
            console.log('# output', output);
            $("#"+target_id).html(output["html"]);
            $("#"+target_id+"_hidden").val(output["id"]);
        });
    }
    else {
        console.log("unhandled type" + type);
    }
    
    /* Close window */
    win.close(); 
}

function dismissAddAnotherPopup(win,key,val) {    
    target = $("#id_"+key);
    update_field(target,val)
    win.close();
}
