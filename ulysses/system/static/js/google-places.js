// This example displays an address form, using the autocomplete feature
// of the Google Places API to help users fill in the information.

// This example requires the Places library. Include the libraries=places
// parameter when you first load the API. For example:
// <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">


function initGooglePlacesAutocomplete(
    placeField, countryNameField, countryField, cityField, placeNameField, locationField
) {
  // Create the autocomplete object, restricting the search to geographica location types.

  if (placeField) {
      var googlePlacesAutocomplete = new google.maps.places.Autocomplete(
          placeField,
          {types: ['establishment', 'geocode']}
      );
      // avoid form submission when enter is pressed
      google.maps.event.addDomListener(placeField, 'keydown', function (event) {
          if (event.keyCode === 13) {
              event.preventDefault();
              event.stopPropagation();
          }
      });
      googlePlacesAutocomplete.addListener('place_changed', function () {
          // Get the place details from the autocomplete object.
          var place = googlePlacesAutocomplete.getPlace();

          countryNameField.value = '';
          countryField.value = '';
          cityField.value = '';
          placeNameField.value = '';
          placeNameField.value = '';
          locationField.value = '';

          if (place && place.address_components) {
              var countryCode = '';
              for (var i = 0; i < place.address_components.length; i++) {
                  var addressComponent = place.address_components[i];
                  for (var j = 0; j < addressComponent.types.length; j++) {
                      var addressType = addressComponent.types[j];
                      if (addressType === 'country') {
                          countryCode = addressComponent.short_name;
                          countryNameField.value = addressComponent.long_name;
                          countryField.value = countryCode;
                      }
                      if (addressType === 'locality') {
                          cityField.value = addressComponent.long_name;
                          if (addressComponent.long_name !== place.name) {
                              placeNameField.value = place.name;
                          }
                      }
                  }
              }
              // gpl_latitude#gps_longitude#country_code
              // console.log("GPS", place.geometry.location.lat(), place.geometry.location.lng());
              var location = '' + place.geometry.location.lat() + "#" + place.geometry.location.lng() + "#" + countryCode;
              locationField.value = location;
          }

      });
  }
}
