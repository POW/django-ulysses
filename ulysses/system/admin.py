from django.contrib.admin import ModelAdmin

from ulysses.super_admin.sites import super_admin_site

from .models import Error


class ErrorAdmin(ModelAdmin):
    date_hierarchy = 'datetime'
    search_fields = ['id', 'path', 'trace']
    list_display = ('id', 'datetime', 'ip', 'user', 'path', 'referer', 'trace')
    raw_id_fields = ['user']


# Global administration registration    
super_admin_site.register(Error, ErrorAdmin)
