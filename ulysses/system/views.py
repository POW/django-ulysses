# -*- coding: utf-8 -*-

from django.http import HttpResponseServerError
from django.template import loader

from .models import save_last_error


def raise_error(request):
    raise RuntimeError("This is a voluntary generated error, for test purposes.")


def error_view(request):
    save_last_error(request)
    # Explicitly returns an HttpResponseServerError
    # in order to let django notify admins
    content = loader.render_to_string('500.html', {}, request)
    return HttpResponseServerError(content)
