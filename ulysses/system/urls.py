
from django.conf.urls import url

from .views import raise_error, error_view

urlpatterns = [
    url(r'^raise_error', raise_error),
]
