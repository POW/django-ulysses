{% load i18n %}

{{ obj.first_name }} {{ obj.last_name }} {% trans "has requested the creation of a new call" %}

- {% trans "Organization" %} {{ obj.organization_name }}
- {% trans "Country" %} {{ obj.country }}
- {% trans "Email address" %} {{ obj.email }}

{% trans "View request at" %} http://{{site.domain}}{% url 'admin:ulysses_competitions_callrequest_change' obj.id %}
