# -*- coding: utf-8 -*-

import datetime
import traceback

from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.mail import send_mail
from django.urls import reverse
from django.db import models


class Error(models.Model):
    datetime = models.DateTimeField()
    ip = models.CharField(max_length=200, blank=True, null=True)
    path = models.CharField(max_length=800, blank=True, null=True)
    referer = models.CharField(max_length=800, blank=True, null=True)
    trace = models.TextField(blank=True, null=True)
    user = models.ForeignKey(User, blank=True, null=True, on_delete=models.SET_NULL)


def save_error(request,error_msg):
    try:
        # Initialize error
        error = Error()
        ip = request.META["REMOTE_ADDR"]
        error.datetime = datetime.datetime.now()
        error.trace = error_msg[0:20000]
        error.path = request.path
        error.ip = ip
        if "HTTP_REFERER" in request.META:
            error.referer = request.META["HTTP_REFERER"]
        if request.user and not request.user.is_anonymous:
            error.user = request.user
        http_prefix = 'http'
        if request.is_secure():
            http_prefix = 'https'
        # Save error
        error.save()
        notify_by_mail = False # Default
        if hasattr(settings,'NOTIFY_APPLICATION_ERRORS_BY_EMAIL'):
            notify_by_mail = settings.NOTIFY_APPLICATION_ERRORS_BY_EMAIL
        if notify_by_mail:
            site = Site.objects.get_current()
            # Send mail
            subject = "Ulysses platform : error"
            url = reverse('admin:ulysses_system_error_change', args=[error.id])
            message = "An error occurred on Ulysses platform.\n\n"
            message += "More informations : {0}://{1}{2}".format(
                http_prefix, site.domain, url
            )
            recipients = [admin[1] for admin in settings.ADMINS]
            send_mail(subject, message, settings.DEFAULT_FROM_EMAIL, recipients)
    except:
        # If we fail to save error, we fail silently
        pass


def save_last_error(request):
    try:
        error_msg = traceback.format_exc()
        save_error(request,error_msg)
    except:
        # If we fail to save error, we fail silently
        pass
