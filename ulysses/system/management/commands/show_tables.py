
from django.core.management.base import BaseCommand, CommandError
from django.db import connection


class Command(BaseCommand):
    help = """Show tables"""

    def handle(self, *args, **options):

        cursor = connection.cursor()

        cursor.execute("show table status")
        for line in cursor:
            print(line)

