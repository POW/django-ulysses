
from django.core.management.base import BaseCommand
from django.contrib.sites.models import Site


class Command(BaseCommand):
    help = """Set the Site domain"""

    def add_arguments(self, parser):
        """Named (optional) arguments"""
        parser.add_argument('domain', type=str)

    def handle(self, domain, *args, **options):
        site = Site.objects.get_current()
        site.domain = domain
        site.name = domain
        site.save()
        print('MAJ Site domain', domain)
