
from django.conf import settings

from django.core.management.base import BaseCommand, CommandError
from django.db import connection


class Command(BaseCommand):
    help = """Set UTF-8 tables"""

    def handle(self, *args, **options):

        cursor = connection.cursor()
        tables = connection.introspection.table_names()

        db_name = settings.DATABASES['default']['NAME']

        print("Processing database", db_name)
        sql = "ALTER DATABASE {0} DEFAULT CHARACTER SET utf8 collate utf8_general_ci".format(db_name)
        cursor.execute(sql)

        for table in tables:
            print("Processing", table)
            sql = "ALTER TABLE {0} CONVERT TO CHARACTER SET utf8 collate utf8_general_ci".format(table)
            cursor.execute(sql)
