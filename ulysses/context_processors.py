# -*- coding: utf-8 -*-

from django.conf import settings
from django.urls import reverse
from django.utils.translation import ugettext as _

from ulysses.competitions import get_admin_site
from ulysses.competitions.models import Competition, CompetitionManager
from ulysses.composers.models import BiographicElement, Media, Document, is_composer
from ulysses.profiles.utils import get_profile
from ulysses.social.utils import get_notifications, get_discussions
from ulysses.web.forms import AcceptNewsletterForm
from ulysses.web.models import EditableText
from ulysses.works.models import Work

from .version import get_version


def in_web(request):
    return request.path.startswith("/web")


def in_profiles(request):
    return request.path.startswith("/profiles")


def in_auth(request):
    return request.path.startswith("/auth")


def in_competition_admin(request):
    return request.path.startswith("/admin")


def get_list_of_competitions(active_user, active_competition, archived=False):
    """
    Get others competitions that can be managed by specified user
    """
    if active_user.is_superuser:
        queryset = Competition.objects.all()
    else:
        active_manager = CompetitionManager.objects.get(user=active_user)
        queryset = Competition.objects.filter(managed_by=active_manager)

    return queryset.filter(archived=archived).order_by("title")


def get_list_of_archived_competitions(active_user, active_competition):
    return get_list_of_competitions(active_user, active_competition, archived=True)


def get_web_button(request, id, label, url):
    is_selected = False
    if request.path.startswith("/web%s" % url):
        is_selected = True
    return {"id": id, "label": label, "url": url, "is_selected": is_selected}


def get_web_menu(request):
    menu = []
    menu.append(get_web_button(request, "TabHome", _("Home"), reverse("web:home")))
    menu.append(get_web_button(request, "TabCompetitions", _("Competitions"), reverse("web:competitions")))
    menu.append(get_web_button(request, "TabAbout", _("Ulysses project"), reverse("web:about")))
    menu.append(get_web_button(request, "TabHelp", _("Help"), reverse("web:help")))
    return menu


def get_personal_bar_item(request,id,label,url,is_collapsable=False):
    item = get_web_button(request,id,label,url)
    item["is_collapsable"] = is_collapsable
    item["id"] = id
    return item


def get_personal_bar_menu(request):
    menu = []

    if get_profile(request.user):
        menu.append(
            get_personal_bar_item(
                request, "profile", _("Your profile"),  reverse(
                    "profiles:personal_space",
                )
            )
        )
    # Biography
    biography = get_personal_bar_item(
        request, "biographic_elements", _("Your biographic details"),
        reverse("web:show_biographic_elements"),
        True
    )
    biography["children"] = [
        {
            'id': "biographic_element_%s" % bio_elt.id,
            'text': bio_elt.title,
            'url': reverse("web:edit_biographic_element", args=[bio_elt.id])
        }
        for bio_elt in BiographicElement.objects.filter(composer__user=request.user)
    ]
    menu.append(biography)
    # Documents
    documents = get_personal_bar_item(
        request, "documents", _("Your documents"), reverse("web:show_documents"), True
    )
    documents["children"] = [
        {
            'id': "document_%s" % doc.id,
            'text': doc.title,
            'url': reverse("web:edit_document",args=[doc.id])
        }
        for doc in Document.objects.filter(composer__user=request.user)
    ]
    menu.append(documents)
    # Media
    medias = get_personal_bar_item(
        request, "medias", _("Your works"), reverse("web:show_medias"), True
    )
    medias["children"] = [
        {
            'id': "media_%s" % medium.id,
            'text': medium.title,
            'url': reverse("web:edit_media",args=[medium.id])
        } for medium in Media.objects.filter(composer__user=request.user)
    ]
    menu.append(medias)

    return menu


def ulysse_context_processor(request):
    context = {}
    context["in_competition_admin"] = in_competition_admin(request)
    context["is_user_authenticated"] = request.user.is_authenticated
    context["is_composer"] = is_composer(request.user)
    context["version"] = get_version()
    context["allow_hijack"] = settings.ALLOW_HIJACK
    if not hasattr(settings, "CONTACT_EMAIL"):
        raise RuntimeError("You should specify a CONTACT_EMAIL in settings")
    context["contact_email"] = settings.CONTACT_EMAIL
    context["GOOGLE_PLACES_API_KEY"] = getattr(settings, "GOOGLE_PLACES_API_KEY", "")

    header, is_new = EditableText.objects.get_or_create(key="header")
    if is_new:
        header.about = "Text shown at the top of the site"
        header.text = _('''
            <span>Welcome to the beta version of our new website!</span>
            <span>Find more information about the platform functions and the whole project
            <a href="{0}">here</a></span>
        ''').format(reverse('web:about'))
        header.save()
    context['header'] = header

    admin_site = get_admin_site()
    if in_competition_admin(request):
        competition = admin_site.get_active_competition(request)
        if competition:
            context["active_competition"] = competition
            context["admin_title"] = _("Administrate competition \"%s\"") % competition
            if request.user.is_superuser or admin_site.is_competition_admin(request):
                context["list_of_competitions"] = get_list_of_competitions(request.user, competition)
                context["list_of_archived_competitions"] = get_list_of_archived_competitions(request.user, competition)
            context["menu"] = competition.get_menu(request)
    context["jury_member"] = admin_site.is_jury_member(request)
    context["competition_admin"] = admin_site.is_competition_admin(request)

    context['hide_accept_cookie_message'] = request.session.get('hide_accept_cookie_message', False)
    context['newsletter_form'] = AcceptNewsletterForm()

    if request.user.is_authenticated:
        # Unread notifications
        notifications = get_notifications(request.user)
        context["notifications"] = notifications
        if notifications:
            context["last_notification"] = notifications[0]
        # Unread messages
        context["unread_discussions_count"] = get_discussions(request.user, True).count()

    return context


def playlist(request):
    context = {}
    playlist_works = Work.objects.exclude(
        exclude_from_playlist=True
    ).exclude(
        audio__isnull=True
    ).exclude(
        audio=''
    )
    if request.user.is_authenticated:
        playlist_works = playlist_works.exclude(
            visibility=Work.VISIBILITY_PRIVATE
        )
    else:
        playlist_works = playlist_works.filter(
            visibility=Work.VISIBILITY_PUBLIC
        )
    playlist_works = playlist_works.order_by('?')[:20]
    context['playlist_works'] = playlist_works
    if playlist_works.count() > 0:
        context['first_playlist_work'] = playlist_works[0]
    return context
