# -*- coding: utf-8 -*-

VERSION = (4, 1, 5)


def get_version():
    return "%s.%s.%s" % (VERSION[0], VERSION[1], VERSION[2])
