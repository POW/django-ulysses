#!/usr/bin/env python

import datetime
import json
from html.parser import HTMLParser
import os
import os.path
from re import sub, match, search as re_search
import subprocess
from sys import stderr
import time
import traceback
from traceback import print_exc
from urllib.parse import urlparse, parse_qs
from urllib.request import urlopen

from bs4 import BeautifulSoup
import xlwt

import django.dispatch
from django.db.models.query import QuerySet
from django.conf import settings
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.serializers import serialize
from django.http import HttpResponse
from django.template.defaultfilters import slugify, date as date_filter
from django.urls import reverse, resolve
from django.utils.encoding import smart_text
from django.utils.safestring import mark_safe

from ulysses.middleware import get_request


upload_received = django.dispatch.Signal(providing_args=['data', 'path'])


def make_string_storable_in_dict(str_value):
    str_value = str_value.replace("\"", "\\\"")
    str_value = str_value.replace("\r", "")
    str_value = str_value.replace("\n", "")
    return str_value


def _get_style_line():
    style_line = xlwt.easyxf("font: name Verdana, color-index black")
    style_line.borders.left = 1
    style_line.borders.top = 1
    style_line.borders.bottom = 1
    style_line.borders.right = 1
    return style_line


def get_safe_sheet_name(name):
    invalid_chars = "[]:?/*\\"
    removed_valid_chars = ''.join(char for char in name if char not in invalid_chars)
    return removed_valid_chars[:31]


def add_excel_worksheet(workbook, sheet_name, columns, lines):
    """
    Add a sheet to the specified workbook, populated from specified columns & lines

    columns : a list of columns title (e.g : ['id','Last name','First name'])

    lines : a list of lines, each line must be a dictionary containing at least a 'cells' key
    cells : a list of cells, each cell must be a dictionary containing at least a 'value' key

    """
    # style_top
    style_top = xlwt.easyxf("font: name Verdana, color-index black, bold on")
    style_top.pattern.pattern = 0x01 # solid/solid_fill/solid_pattern
    style_top.pattern.pattern_fore_colour = 0x1D # Coral
    style_top.borders.left = 1
    style_top.borders.top = 1
    style_top.borders.bottom = 1
    style_top.borders.right = 1

    # Create workbook
    ws = workbook.add_sheet(get_safe_sheet_name(sheet_name))
    # Create columns
    col_index = 0
    for col in columns:
        ws.write(0, col_index, col, style_top)
        col_index += 1
    # Create lines
    line_index = 1
    for line in lines:
        col_index = 0
        for item in line["cells"]:
            style_line = _get_style_line()
            if 'format' in item:
                style_line.num_format_str = item['format']
            value = item["value"]
            maximum_cell_length = 32767
            if isinstance(value, str) and len(value) > maximum_cell_length:
                truncated_sign = '[TRUNCATED] '
                value = truncated_sign + value[:maximum_cell_length - len(truncated_sign)]
                style_line = xlwt.XFStyle()
                style_line.pattern = xlwt.Pattern()
                style_line.pattern.pattern = xlwt.Pattern.SOLID_PATTERN
                style_line.pattern.pattern_fore_colour = 5
            ws.write(line_index, col_index, value, style_line)
            col_index += 1
        line_index += 1


def get_excel_workbook(columns, lines):
    wb = xlwt.Workbook(encoding='utf8', style_compression=2)
    add_excel_worksheet(wb, "Export", columns, lines)
    return wb


def get_excel_response(workbook):
    """
    Gets an HTTP response corresponding to an Excel file, initialized from the given Excel workbook
    """
    filename = settings.MEDIA_ROOT + "/excel/export.xls"
    response = HttpResponse(content_type="application/ms-excel")
    response['Content-Disposition'] = 'attachment; filename=%s' % filename
    workbook.save(response)
    return response


def json_response(function):
    """
    Decorator (@json_response) that encapsulates the result into a
    JsonResponse object and handle exceptions

    Nota : decorated function should accept one and only one argument : request
    """
    def _decorated_function(request):
        try:
            result = function(request)
        except:
            result = [traceback.format_exc()]
        return JsonResponse(result)
    return _decorated_function


class JsonResponse(HttpResponse):
    def __init__(self, object):
        if isinstance(object, QuerySet):
            content = serialize('json', object)
        else:
            content = json.dumps(object)
        super(JsonResponse, self).__init__(content, content_type='application/json')


def upload_received_handler(sender, data, path, **kwargs):
    # Save to final location
    try:
        with open(path, 'wb+') as target_file:
            for chunk in data.chunks():
                target_file.write(chunk)
    except:
        print("#ERROR", traceback.format_exc())
        f = open(settings.LOG_FILE, "a")
        f.write("upload_received_handler : error occurred\n")
        f.write("Error : %s\n" % traceback.format_exc())
        f.close()

upload_received.connect(upload_received_handler, dispatch_uid='uploadify.media.upload_received')


def get_valid_filename(target_dir, file):
    tokens = os.path.splitext(file.name)
    base_name = tokens[0][:100]
    filename_without_extension = slugify(smart_text(base_name)) + "_" + str(int(time.time()))
    extension = tokens[1]
    target_filename = "{0}/{1}{2}".format(target_dir, filename_without_extension, extension)
    while os.path.isfile(target_filename):
        filename_without_extension += "_"
        target_filename = "{0}/{1}{2}".format(target_dir, filename_without_extension, extension)
    return "{0}{1}".format(filename_without_extension, extension)


def handle_upload(request, target_dir, done_callback=None):
    target_filename = ''
    file_path = ''
    if request.method == 'POST':
        if request.FILES:
            data = request.FILES['Filedata']
            # Determine target file
            target_filename = get_valid_filename(target_dir, data)
            target_path = os.path.join(target_dir, target_filename)
            upload_received.send(sender='uploadify', data=data, path=target_path)
            if done_callback is not None:
                target_filename, file_path = done_callback(target_filename, target_path)
            else:
                target_path = os.path.join(target_dir, target_filename)
                file_path = target_path.replace(settings.MEDIA_ROOT, settings.MEDIA_URL)
    return {'filename': target_filename, 'file_path': file_path}


def upload_helper_ex(request, target_dir, done_callback=None):
    data = handle_upload(request, target_dir, done_callback)
    return HttpResponse(json.dumps(data), content_type='application/json')


def upload_helper(request, target_dir):
    data = handle_upload(request, target_dir)
    return HttpResponse(data['filename'])


def yearsago(years, from_date=None):
    if from_date is None:
        from_date = datetime.datetime.now()
    try:
        return from_date.replace(year=from_date.year - years)
    except:
        # Must be 2/29!
        assert from_date.month == 2 and from_date.day == 29  # can be removed
        return from_date.replace(month=2, day=28, year=from_date.year-years)


def get_age(birth_date):
    birth_date_dt = datetime.datetime(birth_date.year, birth_date.month, birth_date.day)
    now = datetime.datetime.now()
    num_years = int((now - birth_date_dt).days / 365.25)
    if birth_date_dt  > yearsago(num_years, now):
        return num_years - 1
    else:
        return num_years


def get_last_day_of_month(date_value):
    """return the last day in the month of a given date"""
    # Get 1st day in month
    first_day_of_month = datetime.date(date_value.year, date_value.month, 1)
    # get a date in the next month
    next_month = first_day_of_month + datetime.timedelta(days=32)
    # return 1st day of next month - 1 day -> last day of month
    return datetime.date(next_month.year, next_month.month, 1) - datetime.timedelta(days=1)


# Borrowed from https://github.com/ljean/coop_cms
class _DeHTMLParser(HTMLParser):
    """html to text parser"""
    def __init__(self, allow_spaces=False, allow_html_chars=False):
        HTMLParser.__init__(self)
        self._text = []
        self._allow_spaces = allow_spaces
        self._allow_html_chars = allow_html_chars
        self._in_style = False

    def handle_data(self, data):
        """parser"""
        if not self._in_style:
            text = data
            if not self._allow_spaces:
                text = sub('[ \t\r\n]+', ' ', text)
            self._text.append(text)

    def handle_entityref(self, name):
        html_char = '&' + name + ";"
        if self._allow_html_chars:
            value = html_char
        else:
            value = self.unescape(html_char).replace('\xa0', ' ')
        self._text.append(value)

    def handle_charref(self, name):
        self.handle_entityref("#" + name)

    def handle_starttag(self, tag, attrs):
        """parser"""
        if tag == 'p':
            self._text.append('\n\n')
        elif tag == 'br':
            self._text.append('\n')
        elif tag == 'tr':
            self._text.append('\n')
        elif tag == 'style':
            self._in_style = True

    def handle_endtag(self, tag):
        """parser"""
        if tag == 'style':
            self._in_style = False

    def handle_startendtag(self, tag, attrs):
        """parser"""
        if tag == 'br':
            # Only 1 \n is enough
            self._text.append('\n')
        pass

    def text(self):
        """parser"""
        return ''.join(self._text).strip()


def dehtml(text, allow_spaces=False, allow_html_chars=False):
    """
    html to text
    copied from http://stackoverflow.com/a/3987802/117092
    """
    try:
        parser = _DeHTMLParser(allow_spaces=allow_spaces, allow_html_chars=allow_html_chars)
        parser.feed(text)
        parser.close()
        return parser.text()
    except Exception:  # pylint: disable=broad-except
        print_exc(file=stderr)
        return text


class ChunkedFile(object):
    """return file in small parts"""

    def __init__(self, file):
        self.file = file

    def __iter__(self):
        return self.file.chunks()


def text_overview(text, max_nb_words):
    """returns the max_nb_words first words of a text or text is less than that"""
    words = dehtml(text).split(" ")
    if len(words) > max_nb_words:
        return " ".join(words[:max_nb_words]) + "..."
    return text


def get_user_personal_folder(username):
    # Create media root, if necessary
    if not os.path.exists(settings.MEDIA_ROOT):
        os.mkdir(settings.MEDIA_ROOT)
    # Create a personal files directory, if if doesn't exist
    if not os.path.exists(settings.PERSONAL_FILES_ROOT):
        os.mkdir(settings.PERSONAL_FILES_ROOT)
    # Create a user temporary directory inside the personal files directory, if if doesn't exist
    user_personal_folder = os.path.join(settings.PERSONAL_FILES_ROOT, username)
    if not os.path.exists(user_personal_folder):
        os.mkdir(user_personal_folder)
    # Return folder
    return user_personal_folder


def get_back_url_for_search(request, allowed_url_name):
    back_url = ''
    referer = request.META.get('HTTP_REFERER', None)
    # Check that the referer is the works list page
    if referer:
        try:
            parse_result = urlparse(referer)
            resolved_view = resolve(parse_result.path)
            allowed_app_name, allowed_view_name = allowed_url_name.split(':')
            if resolved_view.url_name == allowed_view_name and resolved_view.app_name == allowed_app_name:
                back_url = referer
        except Exception as err:
            pass
    return back_url


def generate_username(email):
    return 'user_{0}_{1}'.format(User.objects.count() + 1, email)[:30]


class LimitedHTMLParser(HTMLParser):
    """html to text parser"""
    allowed_tags = ['p', 'ul', 'li', 'ol', 'b', 'strong', 'i', 'u', 'em', 'span', 'br']
    allowed_attrs = {
        'span': [('style', 'text-decoration: underline;'), ],
    }

    def __init__(self):
        HTMLParser.__init__(self)
        self._text = []
        self._allow_spaces = True
        self._allow_html_chars = True

    def handle_data(self, data):
        """parser"""
        text = data
        self._text.append(text)

    def handle_entityref(self, name):
        html_char = '&' + name + ";"
        if self._allow_html_chars:
            value = html_char
        else:
            value = self.unescape(html_char).replace('\xa0', ' ')
        self._text.append(value)

    def handle_charref(self, name):
        self.handle_entityref("#" + name)

    def handle_starttag(self, tag, attrs):
        """parser"""
        if tag in self.allowed_tags:
            allowed_attrs = self.allowed_attrs.get(tag)
            if allowed_attrs:
                tag_attrs = []
                for attr in attrs:
                    if attr in allowed_attrs:
                        tag_attrs.append(attr)
                attr_line = " ".join(['{0}="{1}"'.format(a[0], a[1]) for a in tag_attrs])
                self._text.append('<{0} {1}>'.format(tag, attr_line))
            else:
                self._text.append('<{0}>'.format(tag))

    def handle_startendtag(self, tag, attrs):
        """parser"""
        if tag == 'br':
            self._text.append('<br />')

    def handle_endtag(self, tag):
        """parser"""
        if tag in self.allowed_tags:
            self._text.append('</{0}>'.format(tag))

    def text(self):
        """parser"""
        return ''.join(self._text).strip()


def limited_html(text):
    """remove html tags except accept links and linebreaks"""
    try:
        parser = LimitedHTMLParser()
        parser.feed(text)
        parser.close()
        return mark_safe(parser.text())
    except Exception:  # pylint: disable=broad-except
        print_exc(file=stderr)
        return text


def date_to_str(date_value, show_hour_if_midnight=True, day=False):
    # warning format de l'heure sur 12 heures retourne 12 pour minuit!
    if date_value.year == datetime.date.today().year:
        date_format = 'D M j'
    else:
        date_format = 'D M j, Y'

    if day or not show_hour_if_midnight and date_value.time() == datetime.time.min:
        minutes = ''
    else:
        if date_value.time().minute == 0:
            minutes = date_value.strftime('%I%P').upper()
        else:
            minutes = date_value.strftime('%I:%M%P').upper()
        minutes = ' at ' + minutes
    return date_filter(date_value, date_format) + minutes


def get_info_box_closed(request, name):
    key = 'close_info_box_{0}'.format(name)
    return not request.session.get(key, False)


def set_info_box_closed(request, name):
    key = 'close_info_box_{0}'.format(name)
    request.session[key] = True


def full_path_url(url):
    request = get_request()
    is_secured = not settings.DEBUG
    if request:
        is_secured = request.is_secure()
    site = Site.objects.get_current(request)
    if url is None:
        url = ''
    full_url = site.domain + url
    if is_secured:
        return 'https://' + full_url
    else:
        return 'http://' + full_url


def get_youtube_code(url):
    res = urlparse(url)

    if res.scheme == 'https' and res.netloc == 'www.youtube.com' and res.path == '/watch':
        args = parse_qs(res.query)
        if args:
            try:
                return args['v'][0]
            except (IndexError, KeyError):
                pass
    if res.scheme == 'https' and res.netloc == 'youtu.be':
        # youtu.be/fbe9nkvGcSY -> remove the leading /
        return res.path.strip('/')
    return None


def get_media_link(link):
    if link:
        youtube_code = get_youtube_code(link)
        if youtube_code:
            return '<a href="{0}" class="colorbox-form">YouTube</a>'.format(
                reverse('web:youtube_popup', args=[youtube_code])
            )
        else:
            return '<a href="{0}" target="_blank">{0}</a>'.format(link)
    return ""


def get_soundcloud_code(url):
    result = match(r'https\:\/\/soundcloud\.com\/.*\/.*', url)
    if result:
        try:
            content = urlopen(url).read().decode('utf-8')
            soup = BeautifulSoup(content, 'html.parser')
            for meta in soup.head.findAll('meta'):
                meta_content = meta.get('content')
                if meta_content:
                    result = match('soundcloud\:\/\/sounds:(?P<code>.+)', meta_content)
                    if result:
                        return result.group('code')
        except Exception as err:
            pass  # Sorry not able to get the sound-cloud code
    return None


def is_mp3_valid(filename):
    mp3_file_type = subprocess.run(["file", filename], capture_output=True).stdout.decode()
    return True if re_search('(Audio file with ID3)|(layer III, v1)', mp3_file_type) else False
