# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.web'
    label = 'ulysses_web'
    verbose_name = "Ulysses Web"
