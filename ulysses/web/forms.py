# -*- coding: utf-8 -*-

import os.path

from django import forms
from django.contrib.auth.forms import (
    PasswordResetForm as BasePasswordResetForm, PasswordChangeForm as BasePasswordChangeForm,
    SetPasswordForm as BaseSetPasswordForm
)
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, REDIRECT_FIELD_NAME, password_validation
from django.contrib import messages
from django.template import loader
from django.urls import reverse, reverse_lazy
from django.utils.translation import ugettext_lazy as _, ugettext

from haystack.forms import SearchForm as HaystackSearchForm
from django_registration.forms import RegistrationForm
from snowpenguin.django.recaptcha2.fields import ReCaptchaField
from snowpenguin.django.recaptcha2.widgets import ReCaptchaWidget

from ulysses.generic.emails import send_email
from ulysses.generic.forms import BootstrapFormMixin
from ulysses.generic.widgets import FileUploadControl, MINI_TINCY_MCE_ATTRS
from ulysses.utils import generate_username
from ulysses.middleware import get_request
from ulysses.competitions.forms import patch_media_fields
from ulysses.competitions.models import (
    BiographicElement, CandidateDocument, CandidateMedia, CandidateBiographicElement, Document,
    get_user_temporary_folder
)
from ulysses.composers.models import Composer, Gender
from ulysses.profiles.models import Organization, Individual
from ulysses.utils import get_youtube_code, is_mp3_valid
from ulysses.works.models import Work


class UploadDocumentForm(forms.Form, BootstrapFormMixin):
    title = forms.CharField(max_length=200)
    file = forms.CharField(
        max_length=400,
        widget=FileUploadControl(upload_url='', allowed_extensions='pdf|jpg', no_help=True),
        help_text=_("Supported formats: pdf, jpg. 100MB max.")
    )

    def __init__(self, *args, **kwargs):
        upload_url = kwargs.pop('upload_url')
        super().__init__(*args, **kwargs)
        self.fields['file'].widget.upload_url = upload_url
        self._patch_fields()


class UploadFileForm(forms.Form, BootstrapFormMixin):
    file = forms.CharField(
        max_length=400,
        widget=FileUploadControl(upload_url='', allowed_extensions='pdf|jpg', no_help=True),
        help_text=_("Supported formats: pdf, jpg. 100MB max.")
    )

    def __init__(self, *args, **kwargs):
        upload_url = kwargs.pop('upload_url')
        super().__init__(*args, **kwargs)
        self.fields['file'].widget.upload_url = upload_url
        self._patch_fields()


class UploadMediaForm(forms.Form, BootstrapFormMixin):
    title = forms.CharField(max_length=50)
    score = forms.CharField(
        required=False,
        max_length=400,
        widget=FileUploadControl(
            upload_url=reverse_lazy('web:upload_as_temporary_file'), allowed_extensions='pdf', no_help=True
        ),
        help_text=_("Supported format: pdf. 100MB max.")
    )
    audio = forms.CharField(
        max_length=400,
        required=False,
        widget=FileUploadControl(
            upload_url=reverse_lazy('web:upload_as_temporary_file'), allowed_extensions='mp3',
            no_help=True
        ),
        help_text=_("Supported format: mp3 (bitrate ≥ 256Kbps). 100MB max.")
    )
    video = forms.CharField(
        max_length=400,
        required=False,
        widget=FileUploadControl(
            upload_url=reverse_lazy('web:upload_as_temporary_file'), allowed_extensions='mp4|webm',
            no_help=True
        ),
        help_text=_(
            "Supported formats: mp4 (H.264 video codec with AAC or MP3 stereo audio codec) or "
            "WebM (VP8 video codec with Vorbis audio codec). 100MB max."
        )
    )
    notes = forms.CharField(widget=forms.Textarea(),required=False)
    link = forms.URLField(
        max_length=400, required=False, widget=forms.TextInput(attrs={'size': '50'}),
        help_text=_('max length is 400 characters')
    )

    def __init__(self, *args, **kwargs):
        upload_url = kwargs.pop('upload_url', None)
        self.field_type = kwargs.pop('field_type', '')
        super(UploadMediaForm, self).__init__(*args, **kwargs)
        if upload_url:
            self.fields['score'].widget.upload_url = upload_url
            self.fields['audio'].widget.upload_url = upload_url
            self.fields['video'].widget.upload_url = upload_url
        patch_media_fields(self.fields, self.field_type)
        self._patch_fields()

    def clean_link(self):
        link = self.cleaned_data['link']
        if link and self.field_type in ('VideoYouTubeField', 'YouTubeField'):
            if not get_youtube_code(link):
                raise forms.ValidationError(_('This is not a valid YouTube url'))
        return link

    def clean_audio(self):
        audio = self.cleaned_data['audio']
        request = get_request()
        target_dir = get_user_temporary_folder(request.user.username)
        full_path = os.path.join(target_dir, audio)
        if audio and not is_mp3_valid(full_path):
            raise forms.ValidationError(_('This is not a valid mp3'))
        return audio

    def clean(self):
        # Call base class first
        super(UploadMediaForm, self).clean()
        if self._errors:  # no need to go further if the form is already invalid
            return self.cleaned_data

        # Ensure at least one one the following elements (audio, score, video) has been provided
        score = self.cleaned_data["score"]
        audio = self.cleaned_data["audio"]
        video = self.cleaned_data["video"]
        link = self.cleaned_data.get("link")

        if self.field_type == 'YouTubeField':
            pass
        elif self.field_type == 'VideoYouTubeField':
            if not (link or video):
                raise forms.ValidationError(
                    ugettext("Please provide a YouTube link or a video")
                )
        else:
            if not (score or audio or video):
                raise forms.ValidationError(
                    ugettext("Please provide at least one on the following files: score, audio or video")
                )

        # Return cleaned data
        return self.cleaned_data


class BiographicElementWidget(forms.Textarea):

    def __init__(self, attrs=None, *args, **kwargs):
        if attrs is None:
            attrs = {}
        attrs.update(MINI_TINCY_MCE_ATTRS)
        attrs.update({'class': 'form-control tinymce'})
        super(BiographicElementWidget, self).__init__(attrs, *args, **kwargs)


class BiographicElementForm(forms.Form, BootstrapFormMixin):
    title = forms.CharField(max_length=100)
    text = forms.CharField(widget=BiographicElementWidget())

    def __init__(self, *args, **kwargs):
        super(BiographicElementForm, self).__init__(*args, **kwargs)
        self._patch_fields()


class UlysseRegistrationForm(RegistrationForm, BootstrapFormMixin):
    """This form makes possible to create a new account to access the site"""
    last_name = forms.CharField(required=False, label=_('Last name'), max_length=30)
    first_name = forms.CharField(required=False, label=_('First name'), max_length=30)
    organization_name = forms.CharField(required=False, label=_('Organization name'), max_length=30)
    captcha = ReCaptchaField(widget=ReCaptchaWidget())
    newsletter = forms.BooleanField(required=False, label=_('I want to receive the ULYSSES Network newsletter'))
    contact_name = forms.CharField(required=False, label=_('Contact name'), max_length=100)

    ORGANIZATION = 'organization'
    INDIVIDUAL = 'individual'

    user_type = forms.ChoiceField(
        choices=[
            (INDIVIDUAL, _('Individual (composer, performer, ... )')),
            (ORGANIZATION, _('Organization (festival, academy, ...)')),
        ],
        label=_('You are an'),
        required=True
    )
    field_order = [
        'user_type', 'first_name', 'last_name', 'organization_name', 'contact_name', 'email', 'password1', 'password2',
        'username', 'captcha,' 'newsletter',
    ]

    def __init__(self, *args, **kwargs):
        super(UlysseRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['email'].required = True
        self.fields['email'].help_text = ''
        self.fields['email'].label = ugettext('Email address')
        self.fields['username'].required = False
        self.fields['username'].widget = forms.HiddenInput()
        self.fields['password2'].widget = forms.HiddenInput()
        self.fields['password2'].required = False
        self._patch_fields()

    def clean_username(self):
        """build a unique username"""
        email = self.cleaned_data.get('email')
        if email:
            return generate_username(email)
        return ""

    def _check_field(self, required_for_user_type, field_name):
        user_type = self.cleaned_data.get('user_type', None)
        if required_for_user_type == user_type:
            value = self.cleaned_data[field_name]
            if not value:
                raise forms.ValidationError(ugettext('"This field is required"'))
            return value
        else:
            return ""

    def clean_last_name(self):
        """check individual information"""
        return self._check_field(self.INDIVIDUAL, 'last_name')

    def clean_first_name(self):
        """check individual information"""
        return self._check_field(self.INDIVIDUAL, 'first_name')

    def clean_organization_name(self):
        """check individual information"""
        return self._check_field(self.ORGANIZATION, 'organization_name')

    def clean_contact_name(self):
        """check individual information"""
        return self._check_field(self.ORGANIZATION, 'contact_name')

    def clean_email(self):
        email = self.cleaned_data['email']
        if User.objects.filter(email=email, is_active=True).exists():
            sign_in = '<a href="{0}">{1}</a>'.format(reverse('login'), 'sign in')
            reset_password = '<a href="{0}">{1}</a>'.format(reverse('auth_password_reset'), 'reset your password')
            raise forms.ValidationError(
                ugettext('This email address is already registered. Want to {0} or {1}?').format(
                    sign_in, reset_password
                )
            )
        return email

    def clean_password1(self):
        """By pass password verification"""
        password = self.cleaned_data['password1']
        password_validation.validate_password(password)
        return password

    def clean_password2(self):
        """By pass password verification"""
        password = self.cleaned_data.get('password1', None)
        return password

    def clean(self):
        # Call base class first
        super(UlysseRegistrationForm, self).clean()

        if self._errors:  # no need to go further if the form is already invalid
            return self.cleaned_data

        # # Check no user already exists with this email address
        # email = self.cleaned_data["email"]
        # failed = User.objects.filter(email=email).exists()
        #
        # if failed:
        #     raise forms.ValidationError(ugettext(u"A user account already exists with the same email address"))

        # Return cleaned data
        return self.cleaned_data

    def save(self, *args, **kwargs):
        """extend the save by saving organization name as last name"""
        user = super(UlysseRegistrationForm, self).save(*args, **kwargs)
        ulysses_newsletter = self.cleaned_data['newsletter']
        if self.cleaned_data["user_type"] == 'organization':
            user.last_name = self.cleaned_data["organization_name"][:30]
            contact_name = self.cleaned_data["contact_name"]
            user.save()
            Organization.objects.create(
                user=user, name=user.last_name, contact_name=contact_name, ulysses_newsletter=ulysses_newsletter
            )
        else:
            user.last_name = self.cleaned_data["last_name"]
            user.first_name = self.cleaned_data["first_name"]
            user.save()
            Individual.objects.create(
                user=user, ulysses_newsletter=ulysses_newsletter
            )
        return user


class UlysseProfileForm(forms.ModelForm):
    gender = forms.ModelChoiceField(Gender.objects.all(),required=True)

    def __init__(self, *args, **kwargs):
        super(UlysseProfileForm, self).__init__(*args, **kwargs)
        try:
            first_name = self.instance.user.first_name
            last_name = self.instance.user.last_name
            email = self.instance.user.email
            self.fields.insert(0,'email',forms.EmailField())
            self.fields.insert(0,'last_name',forms.CharField())
            self.fields.insert(0,'first_name',forms.CharField())
            self.fields['first_name'].initial = first_name
            self.fields['last_name'].initial = last_name
            if self.instance.gender:
                self.fields['gender'].initial = self.instance.gender.id
            self.fields['email'].initial = email
        except User.DoesNotExist:
            pass

    def clean(self):
        # Call base class first
        super(UlysseProfileForm,self).clean()
        if self._errors:  # no need to go further if the form is already invalid
            return self.cleaned_data

        try:
            # If we are editing a profile, check no user already exists with this email address
            user = self.instance.user
            email = self.cleaned_data["email"]
            failed = User.objects.filter(email=email).exclude(id=user.id).exists()

            if failed:
                raise forms.ValidationError(
                    ugettext("An account already exists with the same email address")
                )
        except User.DoesNotExist:
            pass

        # Return cleaned data
        return self.cleaned_data

    def save(self, *args, **kwargs):
        try:
            user = self.instance.user
            user.first_name = self.cleaned_data['first_name']
            user.last_name = self.cleaned_data['last_name']
            user.email = self.cleaned_data['email']
            user.save()
        except User.DoesNotExist:
            pass
        return super(UlysseProfileForm, self).save(*args, **kwargs)

    class Meta:
        model = Composer
        exclude = ('user', )  # User will be filled in by the view.


class EmailAuthForm(forms.Form, BootstrapFormMixin):
    """Email Authentication form"""
    user_cache = None
    email = forms.EmailField(required=True, label=_("Email"))
    password = forms.CharField(label=_("Password"), widget=forms.PasswordInput)

    def __init__(self, request=None, *args, **kwargs):
        super(EmailAuthForm, self).__init__(*args, **kwargs)

        if request:
            # Redirect to the next url after login
            if REDIRECT_FIELD_NAME in request.GET:
                self.fields[REDIRECT_FIELD_NAME] = forms.CharField(
                    initial=request.GET[REDIRECT_FIELD_NAME],
                    widget=forms.HiddenInput()
                )
            elif REDIRECT_FIELD_NAME in request.POST:
                # redirect to next even after error on the initial login form
                self.fields[REDIRECT_FIELD_NAME] = forms.CharField(
                    widget=forms.HiddenInput()
                )
        self._patch_fields()

    def _authenticate(self, check_password=False):
        """check authentication"""
        email = self.cleaned_data.get('email')
        password = self.cleaned_data.get('password')

        error_messages = {
            'invalid_login': _(
                "Please enter a correct %(email)s and password. Note that both fields may be case-sensitive."
            ),
        }

        if email and password:
            self.user_cache = authenticate(
                email=email, password=password, backend='ulysses.web.auth_backends.EmailAuthBackend'
            )
            if self.user_cache is None:
                if check_password:
                    raise forms.ValidationError(
                        ugettext(
                            'The password you entered is incorrect. <a href="{0}">{1}</a>').format(
                            reverse('auth_password_reset'), ugettext('Reset your password?')
                        )
                    )
                else:
                    raise forms.ValidationError(
                        error_messages['invalid_login'],
                        code='invalid_login',
                        params={'email': _("email")},
                    )

    def get_user(self):
        """return the user"""
        return self.user_cache

    def clean_email(self):
        email = self.cleaned_data['email']
        if not User.objects.filter(email=email, is_active=True).exists():
            if User.objects.filter(email=email, is_active=False).exists():
                message_text = ugettext(
                    'Your account is not active yet. Please follow the instructions we sent you by email.<br />'
                    'If you don\'t receive your activation email, please also check your spams, or contact '
                    '<a href="mailto:technical.assistance@ulysses-network.eu">technical.assistance@ulysses-network.eu'
                    '</a> if needed.'
                )
                raise forms.ValidationError(message_text)
            else:
                raise forms.ValidationError(
                    ugettext('This email address doesn\'t match any existing account. <a href="{0}">{1}</a>').format(
                        reverse('registration_register'), ugettext('Sign up for an account')
                    )
                )
        return email

    def clean_password(self):
        password = self.cleaned_data.get('password')
        self._authenticate(True)
        return password

    def clean(self):
        """clean data"""
        self._authenticate()
        return self.cleaned_data


class PasswordResetForm(BasePasswordResetForm, BootstrapFormMixin):
    def __init__(self, *args, **kwargs):
        super(PasswordResetForm, self).__init__(*args, **kwargs)
        self._patch_fields()

    def clean_email(self):
        email = self.cleaned_data['email']
        request = get_request()
        messages.success(
            request,
            ugettext(
                'If your email address exists in our database, you will receive a password recovery link ' \
                'at your email address in a few minutes.'
            )
        )
        return email

    def send_mail(self, subject_template_name, email_template_name,
                  context, from_email, to_email, html_email_template_name=None):
        subject = loader.render_to_string(subject_template_name, context)
        # Email subject *must not* contain newlines
        subject = ''.join(subject.splitlines())
        send_email(subject, email_template_name, context, [to_email])


class PasswordChangeForm(BasePasswordChangeForm, BootstrapFormMixin):

    def __init__(self, *args, **kwargs):
        super(PasswordChangeForm, self).__init__(*args, **kwargs)
        self._patch_fields()

    def save(self, *args, **kwargs):
        ret = super(PasswordChangeForm, self).save(*args, **kwargs)
        request = get_request()
        messages.success(
            request,
            ugettext(
                'Your password has been successfully changed.'
            )
        )
        return ret


class SetPasswordForm(BaseSetPasswordForm, BootstrapFormMixin):
    def __init__(self, *args, **kwargs):
        super(SetPasswordForm, self).__init__(*args, **kwargs)
        self._patch_fields()
        self.fields['new_password1'].label = ugettext('New password')
        self.fields['new_password2'].label = ugettext('Confirm new password')

    def save(self, *args, **kwargs):
        ret = super(SetPasswordForm, self).save(*args, **kwargs)
        request = get_request()
        messages.success(
            request,
            ugettext(
                'Your password has been successfully changed. You can now sign in.'
            )
        )
        return ret


class SelectElementForm(forms.Form):

    element_origin = forms.ChoiceField(
        choices=[
            ('portfolio', _('From your portfolio')),
            ('history', _('From your application history')),
        ],
        widget=forms.RadioSelect(),
        label='',
        required=False
    )
    element = forms.CharField(widget=forms.HiddenInput(attrs={'class': 'select-target'}))

    def portfolio_queryset(self, composer, element_id):
        try:
            return self.element_class.objects.get(composer=composer, id=element_id)
        except self.element_class.DoesNotExist:
            return None

    def history_queryset(self, composer, element_id):
        try:
            element = self.candidate_element_class.objects.get(
                candidate__composer=composer, is_admin=False, id=element_id
            )
            return element
        except self.candidate_element_class.DoesNotExist:
            return None

    def clean_element(self):
        element_origin = self.cleaned_data['element_origin']
        element_id = self.cleaned_data['element']
        request = get_request()
        composer = Composer.get_from_user(request.user)

        if element_origin == 'portfolio':
            element = self.portfolio_queryset(composer, element_id)
            if element is None:
                raise forms.ValidationError(_('Unknown profile element'))
            return element

        elif element_origin == 'history':
            element = self.history_queryset(composer, element_id)
            if element is None:
                raise forms.ValidationError(_('Unknown application element'))
            return element

        else:
            raise forms.ValidationError(_('Unknown type of document'))


class SelectDocumentForm(SelectElementForm):
    element_class = Document
    candidate_element_class = CandidateDocument


class SelectMediaForm(SelectElementForm):
    element_class = Work
    candidate_element_class = CandidateMedia

    def portfolio_queryset(self, composer, element_id):
        try:
            return self.element_class.objects.get(owner=composer.user, id=element_id)
        except self.element_class.DoesNotExist:
            return None


class SelectBiographyForm(SelectElementForm):
    element_class = BiographicElement
    candidate_element_class = CandidateBiographicElement


class SearchForm(HaystackSearchForm, BootstrapFormMixin):

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self._patch_fields()
        self.fields['q'].widget.attrs['placeholder'] = _('Search for members, works, instruments...')
        self.fields['q'].widget.attrs['autocomplete'] = 'off'


class AcceptNewsletterForm(forms.Form):
    email = forms.EmailField()
    accept_cgu = forms.BooleanField()

    def __init__(self, *args, **kwargs):
        super(AcceptNewsletterForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs['placeholder'] = _('Your email address')
        self.fields['email'].widget.attrs['autocomplete'] = 'off'
        link = _('<a href="{0}" target="_blank">Privacy Policy</a>').format(
            reverse('web:privacy_policy')
        )
        self.fields['accept_cgu'].label = _(
            'I agree to the {0} and understand I can unsubscribe at any time'
        ).format(link)