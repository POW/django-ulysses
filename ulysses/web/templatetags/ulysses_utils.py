# -*- coding: utf-8 -*-
"""
Borrowed from https://github.com/ljean/coop_cms/blob/master/coop_cms/templatetags/coop_utils.py
"""

from datetime import date
import os
import re
import time as ctime
import pytz
from bs4 import BeautifulSoup

from django import template
from django.conf import settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.sites.models import Site
from django.forms import CheckboxInput
from django.template.base import TemplateSyntaxError
from django.template.exceptions import TemplateDoesNotExist
from django.template.loader import get_template
from django.template.defaultfilters import linebreaksbr, date as date_filter, urlencode
from django.urls import reverse
from django.utils.text import slugify
from django.utils.safestring import mark_safe

from ulysses.middleware import get_request
from ulysses.utils import (
    dehtml, limited_html, date_to_str, get_info_box_closed, full_path_url as utils_full_path_url, get_youtube_code
)
from ulysses.web.models import HomepageImage

from ulysses.competitions.models import is_jury_member as utils_is_jury_member, CompletedEvaluationWidget
from ulysses.profiles.utils import get_profile

register = template.Library()


@register.filter
def make_text(value, max_length=0):
    """html to text"""
    if value:
        text = dehtml(value).replace('\n', ' ')
        if max_length and len(text) > max_length:
            text = text[:(max_length - 3)] + '...'
        return text
    else:
        return ""


@register.filter
def unhtml(value):
    """html to text"""
    return linebreaksbr(
        re.sub(
            r"[\r?\n]+",
            "\n",
            dehtml(value, allow_spaces=True, allow_html_chars=False)
        )
    )


@register.filter
def facebook_headers(item, extra_context=None):
    """returns the content of an object for displaying it the community feed"""
    try:
        content_type = ContentType.objects.get_for_model(item)
        template_name = '{0}'.format(content_type.model)
    except AttributeError:
        template_name = item
    try:
        template = get_template('facebook_headers/{0}.html'.format(template_name))
    except TemplateDoesNotExist:
        return ''
    protocol = 'http://'
    request = get_request()
    if request and request.is_secure():
        protocol = 'https://'

    context = {'item': item, 'site': Site.objects.get_current(), 'protocol': protocol}
    if extra_context:
        context.update(extra_context)

    return template.render(context)


@register.filter
def facebook_headers_home(item):
    """returns the content of an object for displaying it the community feed"""
    try:
        homepage_image = HomepageImage.objects.get(is_active=True)
    except (HomepageImage.DoesNotExist, HomepageImage.MultipleObjectsReturned):
        homepage_image = None
    return facebook_headers(item, {'homepage_image': homepage_image})


@register.filter
def group_in_sublists(list_of_objs, subslist_size):
    """group_in_sublists([1, 2, 3, 4], 2) --> [[1, 2], [3, 4]]"""
    return [list_of_objs[i:i+subslist_size] for i in range(0, len(list_of_objs), subslist_size)]


class VersionedStaticFileNode(template.Node):
    """return static path with ?v=dateoflastchange"""
    def __init__(self, static_path):
        self.static_path = static_path.strip("'").strip('"')
        self.is_string = self.static_path == static_path

    def render(self, context):
        if settings.DEBUG:
            version = ctime.time()
        else:
            static_file_path = os.path.join(settings.STATIC_ROOT, self.static_path)
            try:
                #last Modification of the file
                version = os.path.getmtime(static_file_path)
            except OSError:
                version = 'x'
        return "{0}{1}?v={2}".format(settings.STATIC_URL, self.static_path, version)


@register.tag
def static_vs(parser, token):
    """image list"""
    args = token.split_contents()
    try:
        static_path = args[1]
    except IndexError:
        raise TemplateSyntaxError("coop_image_list: usage --> {% versioned_static_file 'static_path' %}")
    return VersionedStaticFileNode(static_path)


@register.filter
def content_type(obj):
    if not obj:
        return False
    return ContentType.objects.get_for_model(obj)


@register.filter
def is_jury_member(user):
    return utils_is_jury_member(user)


@register.filter
def full_path_url(url):
    return utils_full_path_url(url)


@register.filter
def is_user_allowed(elt, user):
    if hasattr(elt, 'is_user_allowed') and callable(elt.is_user_allowed):
        return elt.is_user_allowed(user)
    return False


@register.filter
def to_date(date_value):
    # if date_value.year == date.today().year:
    #     return date_filter(date_value, 'M j')
    return date_filter(date_value, 'M j, Y')


@register.filter
def is_checkbox(field):
    field = getattr(field, 'field', field)  # get the field attribute of the field or the field itself
    if hasattr(field, 'widget'):
        checkbox_classes = (
            CheckboxInput().__class__.__name__,
            'ApplicationBooleanElementWidget',
            'ApplicationBooleanElementPreviewWidget',
        )
        return field.widget.__class__.__name__ in checkbox_classes
    return False


@register.filter
def is_completed_evaluation(field):
    field = getattr(field, 'field', field)  # get the field attribute of the field or the field itself
    if hasattr(field, 'widget'):
        widget_classes = (
            CompletedEvaluationWidget().__class__.__name__,
        )
        return field.widget.__class__.__name__ in widget_classes
    return False


@register.filter
def to_competition_date(date_value):
    time_zone = pytz.timezone(settings.TIME_ZONE)
    date_time = time_zone.localize(date_value)  # make datetime timezone aware
    utc_date_time = date_time.astimezone(pytz.utc)  # convert to UTC
    return date_to_str(utc_date_time) + " UTC"


@register.filter
def to_datetime(date_value):
    return date_to_str(date_value)


@register.filter
def to_competition_day(date_value):
    return date_to_str(date_value, day=True)


@register.filter
def competition_opening_period(competition):
    if competition.opening_date.year == competition.closing_date.year:
        opening = date_filter(competition.opening_date, 'M j')
    else:
        opening = date_filter(competition.opening_date, 'M j, Y')
    closing = date_filter(competition.closing_date, 'M j, Y')
    return opening + ' - ' + closing


@register.filter
def message_datetime(msg_dt, full=False):
    if msg_dt.date() != date.today():
        if full:
            return date_filter(msg_dt, "d/m/y H:i")
        else:
            return date_filter(msg_dt, 'd/m/y')
    else:
        return date_filter(msg_dt, "H:i")


@register.filter
def odd_length(sequence):
    return len(sequence) % 2


@register.filter
def as_safe_html(text):
    return limited_html(text)


@register.filter
def search_content(obj):
    """returns the html to display in search results """
    content_type = ContentType.objects.get_for_model(obj)
    context_data = {
        'content_object': obj,
    }
    template = get_template('search/results/{0}.html'.format(content_type.model))
    return template.render(context_data)


@register.filter
def is_info_box_visible(name):
    request = get_request()
    return get_info_box_closed(request, name)


@register.filter
def as_menu(static_content):
    soup = BeautifulSoup(static_content.text, 'html.parser')
    titles = soup.select('h4')
    return mark_safe(u''.join(
        u'<li><a class="no-ajax" href="#{0}-{1}">{2}</a></li>'.format(static_content.key, slugify(title.text), title.text)
        for title in titles
    ))


@register.filter
def as_menu_content(static_content):
    soup = BeautifulSoup(static_content.text, 'html.parser')
    titles = soup.select('h4')
    for title_node in titles:
        text = title_node.text
        title_node.clear()
        link = soup.new_tag("a")
        link['name'] = "{0}-{1}".format(static_content.key, slugify(text))
        link['id'] = "{0}-{1}".format(static_content.key, slugify(text))
        link.string = text
        title_node.append(link)
    return mark_safe(u'{0}'.format(soup))


@register.filter
def share_by_email(item):
    protocol = 'http://'
    request = get_request()
    if request and request.is_secure():
        protocol = 'https://'
    profile = None
    if request:
        profile = get_profile(request.user)

    context = {'item': item, 'site': Site.objects.get_current(), 'protocol': protocol, 'profile': profile}

    try:
        content_type = ContentType.objects.get_for_model(item)
        template_name = '{0}'.format(content_type.model)
    except AttributeError:
        template_name = item

    try:
        subject_template = get_template('mailto/{0}_subject.txt'.format(template_name))
        subject = subject_template.render(context)
    except TemplateDoesNotExist:
        subject = str(item)

    try:
        body_template = get_template('mailto/{0}_body.txt'.format(template_name))
        body = body_template.render(context)
    except TemplateDoesNotExist:
        body = ''

    return "mailto:?subject={0}&body={1}".format(
        urlencode(subject),
        urlencode(body)
    )


@register.filter
def as_link(link):
    html = ''
    if link:
        youtube_code = get_youtube_code(link)
        if youtube_code:
            html = '<a href="{0}" class="colorbox-form">{1}</a>'.format(
                reverse('web:youtube_popup', args=[youtube_code]), link
            )
        else:
            html = '<a href="{0}" target="_blank">{0}</a>'.format(link)
    return mark_safe(html)


@register.filter
def ltrim(text, max_length=100):
    if len(text) > max_length:
        return text[:max_length] + "..."
    return text
