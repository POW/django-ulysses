# -*- coding: utf-8 -*-

import factory
from datetime import datetime

from .models import FocusOn, HomepageImage, AnnounceCallImage


class FocusOnFactory(factory.DjangoModelFactory):
    class Meta:
        model = FocusOn

    title = factory.Sequence(lambda n: 'title{0}'.format(n))
    slug = factory.Sequence(lambda n: 'slug{0}'.format(n))
    datetime = datetime.now()
    text = factory.Sequence(lambda n: 'focus-on-content-{0}'.format(n))
    is_published = True
    is_headline = True


class HomepageImageFactory(factory.DjangoModelFactory):
    class Meta:
        model = HomepageImage

    image = factory.django.ImageField()
    is_active = True


class AnnounceCallImageFactory(factory.DjangoModelFactory):
    class Meta:
        model = AnnounceCallImage

    image = factory.django.ImageField()
    is_active = True
