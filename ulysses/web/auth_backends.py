# -*- coding: utf-8 -*-

from datetime import datetime

from django.contrib.auth import get_user_model
from django.contrib.auth.backends import ModelBackend

from ulysses.profiles.utils import get_profile


class EmailAuthBackend(ModelBackend):
    """
    Email Authentication Backend: make possible to use email rather than username for user authentication
    """

    def authenticate(self, request, email=None, password=None, **kwargs):
        """
            check if user can log in:
            return the user if login is successful
            return None if login fails
         """

        # clean email
        email = email.strip() if email else email

        if email:
            # if email is defined, get every active user corresponding to the given email
            user_model = get_user_model()
            for user in user_model.objects.filter(email=email, is_active=True):
                if user.check_password(password):
                    # If password is correct: return user to accept it as logged user
                    # Enable the profile
                    profile = get_profile(user)
                    if profile and not profile.is_enabled:
                        profile.is_enabled = True
                        user.date_joined = datetime.now()
                        profile.save()

                    return user

        # No valid user found : refuse login
        return None
