# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import datetime, timedelta

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.profiles.factories import IndividualFactory

from ..factories import FocusOnFactory


class FocusOnHomepageTest(BaseTestCase):
    """The posts on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_focus_on_anonymous(self):
        """it should display focus on items on homepage"""

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True, datetime=datetime.now() - timedelta(1))
        focus3 = FocusOnFactory.create(is_headline=False, is_published=True)
        focus4 = FocusOnFactory.create(is_headline=True, is_published=False)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-focus_on")), 1)
        self.assertEqual(len(soup.select(".home-focus_on .home-focus_on-item")), 2)
        self.assertContains(response, focus1.title)
        self.assertContains(response, focus2.title)
        self.assertNotContains(response, focus3.title)
        self.assertNotContains(response, focus4.title)

    def test_view_focus_limit_to_4(self):
        """it should display focus on items on homepage"""

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True, datetime=datetime.now() - timedelta(1))
        focus3 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus4 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus5 = FocusOnFactory.create(is_headline=True, is_published=True)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-focus_on")), 1)
        self.assertEqual(len(soup.select(".home-focus_on .home-focus_on-item")), 4)
        self.assertContains(response, focus1.title)
        self.assertNotContains(response, focus2.title)
        self.assertContains(response, focus3.title)
        self.assertContains(response, focus4.title)
        self.assertContains(response, focus5.title)

    def test_view_focus_on_login(self):
        """it should display focus on items on homepage"""

        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True, datetime=datetime.now() - timedelta(1))
        focus3 = FocusOnFactory.create(is_headline=False, is_published=True)
        focus4 = FocusOnFactory.create(is_headline=True, is_published=False)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-focus_on")), 1)
        self.assertEqual(len(soup.select(".home-focus_on .home-focus_on-item")), 2)
        self.assertContains(response, focus1.title)
        self.assertContains(response, focus2.title)
        self.assertNotContains(response, focus3.title)
        self.assertNotContains(response, focus4.title)

    def test_view_focus_on_empty(self):
        """it should display focus on items on homepage"""

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-focus_on")), 1)
        self.assertEqual(len(soup.select(".home-focus_on .home-focus_on-item")), 0)


class FocusOnItemTest(BaseTestCase):

    def test_view_item_anonymous(self):
        """it should display homepage"""

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)

        url = focus1.get_absolute_url()
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, focus1.title)
        self.assertContains(response, focus1.text)

    def test_view_item_logged(self):
        """it should display homepage"""

        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))

        focus1 = FocusOnFactory.create(is_headline=False, is_published=True)

        url = focus1.get_absolute_url()
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, focus1.title)
        self.assertContains(response, focus1.text)

    def test_view_item_not_published(self):
        """it should display homepage"""

        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))

        focus1 = FocusOnFactory.create(is_headline=True, is_published=False)

        url = focus1.get_absolute_url()
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)


class FocusOnHeadlineTest(BaseTestCase):

    def test_view_item_anonymous(self):
        """it should display homepage"""

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True, datetime=datetime.now() - timedelta(1))

        url = reverse('web:focus_on_headline')
        self.client.get(url)

        response = self.client.get(url, follow=True)
        self.assertRedirects(response, focus1.get_absolute_url())

        self.assertContains(response, focus1.title)
        self.assertContains(response, focus1.text)

        self.assertNotContains(response, focus2.title)
        self.assertNotContains(response, focus2.text)

    def test_view_item_logged(self):
        """it should display homepage"""

        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))

        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True, datetime=datetime.now() - timedelta(1))

        url = reverse('web:focus_on_headline')
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, focus1.get_absolute_url())

        self.assertContains(response, focus1.title)
        self.assertContains(response, focus1.text)

        self.assertNotContains(response, focus2.title)
        self.assertNotContains(response, focus2.text)

    def test_view_no_headlines(self):
        """it should display homepage"""

        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))

        focus1 = FocusOnFactory.create(is_headline=False, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=False, is_published=True, datetime=datetime.now() - timedelta(1))
        focus3 = FocusOnFactory.create(is_headline=False, is_published=False)

        url = reverse('web:focus_on_headline')
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('web:focus_on_list'))

        self.assertContains(response, focus1.title)
        self.assertContains(response, focus1.text)

        self.assertContains(response, focus2.title)
        self.assertContains(response, focus2.text)

        self.assertNotContains(response, focus3.title)
        self.assertNotContains(response, focus3.text)


class FocusOnListTest(BaseTestCase):
    """The posts on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_focus_on_anonymous(self):
        """it should display focus on items on homepage"""
        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus3 = FocusOnFactory.create(is_headline=False, is_published=True)
        focus4 = FocusOnFactory.create(is_headline=True, is_published=False)
        url = reverse('web:focus_on_list')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".item")), 3)
        self.assertContains(response, focus1.text)
        self.assertContains(response, focus2.text)
        self.assertContains(response, focus3.text)
        self.assertNotContains(response, focus4.text)
        self.assertEqual(len(soup.select(".load-more-button")), 0)

    def test_view_focus_on_login(self):
        """it should display focus on items on homepage"""
        profile = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=profile.user.email, password="1234"))
        focus1 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus2 = FocusOnFactory.create(is_headline=True, is_published=True)
        focus3 = FocusOnFactory.create(is_headline=False, is_published=True)
        focus4 = FocusOnFactory.create(is_headline=True, is_published=False)
        url = reverse('web:focus_on_list')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".item")), 3)
        self.assertContains(response, focus1.text)
        self.assertContains(response, focus2.text)
        self.assertContains(response, focus3.text)
        self.assertNotContains(response, focus4.text)
        self.assertEqual(len(soup.select(".load-more-button")), 0)

    def test_view_focus_on_empty(self):
        """it should display focus on items on homepage"""
        url = reverse('web:focus_on_list')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".item")), 0)
        self.assertEqual(len(soup.select(".load-more-button")), 0)

    def test_view_focus_on_pagination_page1(self):
        """it should display focus on items on homepage"""
        for focus in range(7):
            FocusOnFactory.create(is_published=True, datetime=datetime.now())
        last_focus = FocusOnFactory.create(is_published=True, datetime=datetime.now() + timedelta(seconds=5))
        url = reverse('web:focus_on_list')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".item")), 6)
        self.assertContains(response, last_focus.title)
        self.assertEqual(len(soup.select(".load-more-button")), 1)

    def test_view_focus_on_pagination_page2(self):
        """it should display focus on items on homepage"""
        for focus in range(6):
            FocusOnFactory.create(is_published=True, datetime=datetime.now())
        last_focus = FocusOnFactory.create(is_published=True, datetime=datetime.now() + timedelta(seconds=5))
        url = reverse('web:focus_on_list')
        self.client.get(url)
        response = self.client.get(url, data={'counter': 'items:12'})
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".item")), 7)  # view all
        self.assertContains(response, last_focus.title)
        self.assertEqual(len(soup.select(".load-more-button")), 0)
