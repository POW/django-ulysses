# -*- coding: utf-8 -*-

import os
from os.path import dirname, abspath

from bs4 import BeautifulSoup

from django.test.client import RequestFactory
from django.urls import reverse

from colorbox.utils import assert_popup_redirects as colorbox_assert_popup_redirects
from ulysses.generic.tests import BaseTestCase, assert_popup_redirects

from ulysses.composers.models import is_composer
from ulysses.composers.factories import ComposerFactory, MediaFactory, DocumentFactory, BiographicElementFactory
from ulysses.competitions.factories import CompetitionFactory
from ulysses.competitions.models import BiographicElement, Document, Media
from ulysses.profiles.factories import IndividualFactory
from ulysses.works.factories import WorkFactory
from ulysses.works.models import Work

from ..views.applications import upload_in_personal_space


class ComposerPersonalSpaceTest(BaseTestCase):

    def _login_as_composer(self):
        individual = IndividualFactory.create()
        self.user = individual.user

        # Create a valid composer profile for this user
        self.composer = ComposerFactory.create(user=self.user)
        self.assertTrue(self.composer.pk)
        self.assertTrue(is_composer(self.composer.user))

        self.client.login(email=individual.user.email, password="1234")

    def test_view_add_bio(self):
        self._login_as_composer()
        url = reverse('web:add_biographic_element')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_add_biographic_element(self):
        self._login_as_composer()
        url = reverse('web:add_biographic_element')
        self.client.get(url)
        title = "My biographic element"
        post_data = {
            'title': "My biographic element",
            'text': "Sample text",
        }
        response = self.client.post(url, post_data)
        colorbox_assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(BiographicElement.objects.count(), 1)
        bio = BiographicElement.objects.get(composer=self.composer, title=title)
        self.assertEqual(bio.title, post_data['title'])
        self.assertEqual(bio.text, post_data['text'])

    def test_add_biographic_element_anonymous(self):
        # self._login_as_composer()
        url = reverse('web:add_biographic_element')
        self.client.get(url)
        title = "My biographic element"
        post_data = {
            'title': "My biographic element",
            'text': "Sample text",
        }
        response = self.client.post(url, post_data)
        colorbox_assert_popup_redirects(response, reverse("login") + "?next={0}".format(url))
        self.assertEqual(BiographicElement.objects.count(), 0)

    def test_view_edit_bio(self):
        self._login_as_composer()
        bio = BiographicElementFactory.create(composer=self.composer)
        url = reverse('web:edit_biographic_element', args=[bio.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_edit_biographic_element(self):
        self._login_as_composer()
        bio = BiographicElementFactory.create(composer=self.composer)
        url = reverse('web:edit_biographic_element', args=[bio.id])
        post_data = {
            'title': "My biographic element",
            'text': "Updated text",
        }
        response = self.client.post(url, post_data)
        colorbox_assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(BiographicElement.objects.count(), 1)
        bio = BiographicElement.objects.get(id=bio.id)
        self.assertEqual(bio.title, post_data['title'])
        self.assertEqual(bio.text, post_data['text'])

    def test_edit_biographic_element_not_allowed(self):
        self._login_as_composer()
        other_composer = ComposerFactory.create()
        bio = BiographicElementFactory.create(composer=other_composer)
        url = reverse('web:edit_biographic_element', args=[bio.id])
        post_data = {
            'title': "My biographic element",
            'text': "Updated text",
        }
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 404)

    def test_view_document_upload(self):
        self._login_as_composer()
        url = reverse('web:add_document')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_view_document_upload_anonymous(self):
        url = reverse('web:add_document')
        response = self.client.get(url)
        assert_popup_redirects(response, reverse("login") + "?next={0}".format(url))

    def test_document_upload(self):
        self._login_as_composer()
        url = reverse('web:add_document')

        # Upload sample doc in personal space
        base_dir = abspath(dirname(__file__))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')

        uploaded_file = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": uploaded_file})
        upload_request.user = self.user

        upload_response = upload_in_personal_space(upload_request)
        file_name = upload_response
        post_data = {'title': 'Sample doc', 'file': file_name}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Document.objects.count(), 1)
        doc = Document.objects.get(composer=self.composer)
        self.assertEqual(doc.title, post_data['title'])

    def test_edit_document_upload(self):
        self._login_as_composer()
        doc = DocumentFactory.create(composer=self.composer)
        url = reverse('web:edit_document', args=[doc.id])

        # Upload sample doc in personal space
        base_dir = abspath(dirname(__file__))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')

        uploaded_file = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": uploaded_file})
        upload_request.user = self.user

        upload_response = upload_in_personal_space(upload_request)
        file_name = upload_response

        post_data = {'title': 'Sample doc', 'file': file_name}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Document.objects.count(), 1)
        doc = Document.objects.get(id=doc.id)
        self.assertEqual(doc.title, post_data['title'])

    def test_view_media_upload(self):
        self._login_as_composer()
        url = reverse('web:add_media')
        self.client.get(url)
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

    def test_media_upload(self):
        self._login_as_composer()
        url = reverse('web:add_media')

        # Upload sample score in personal space
        base_dir = abspath(dirname(__file__))
        sample_score_path = os.path.join(base_dir, "fixtures", 'sample-score.pdf')

        uploaded_score = open(sample_score_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": uploaded_score})
        upload_request.user = self.user

        upload_response = upload_in_personal_space(upload_request)
        score_file_name = upload_response

        post_data = {'title': 'Sample doc', 'score': score_file_name}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Media.objects.count(), 1)
        media = Media.objects.get(composer=self.composer)
        self.assertEqual(media.title, post_data['title'])

    def test_edit_media(self):
        self._login_as_composer()
        media = MediaFactory.create(composer=self.composer)
        url = reverse('web:edit_media', args=[media.id])

        # Upload sample score in personal space
        base_dir = abspath(dirname(__file__))
        sample_score_path = os.path.join(base_dir, "fixtures", 'sample-score.pdf')

        uploaded_score = open(sample_score_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": uploaded_score})
        upload_request.user = self.user

        upload_response = upload_in_personal_space(upload_request)
        score_file_name = upload_response

        post_data = {'title': 'Sample doc', 'score': score_file_name}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Media.objects.count(), 1)
        media = Media.objects.get(id=media.id)
        self.assertEqual(media.title, post_data['title'])

    def test_edit_media_not_allowed(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        media = MediaFactory.create(composer=other)
        url = reverse('web:edit_media', args=[media.id])

        # Upload sample score in personal space
        base_dir = abspath(dirname(__file__))
        sample_score_path = os.path.join(base_dir, "fixtures", 'sample-score.pdf')

        uploaded_score = open(sample_score_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": uploaded_score})
        upload_request.user = self.user

        upload_response = upload_in_personal_space(upload_request)
        score_file_name = upload_response

        post_data = {'title': 'Sample doc', 'score': score_file_name}
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Media.objects.count(), 1)
        media = Media.objects.get(id=media.id)
        self.assertNotEqual(media.title, post_data['title'])

    def test_view_delete_media(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        media = MediaFactory.create(composer=self.composer)
        url = reverse('web:delete_media', args=[media.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(Media.objects.count(), 1)

    def test_delete_media(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        media = MediaFactory.create(composer=self.composer)
        url = reverse('web:delete_media', args=[media.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Media.objects.count(), 0)

    def test_delete_media_other(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        media = MediaFactory.create(composer=other)
        url = reverse('web:delete_media', args=[media.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Media.objects.count(), 1)

    def test_delete_doc(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        doc = DocumentFactory.create(composer=self.composer)
        url = reverse('web:delete_document', args=[doc.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(Document.objects.count(), 0)

    def test_delete_doc_other(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        doc = DocumentFactory.create(composer=other)
        url = reverse('web:delete_document', args=[doc.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(Document.objects.count(), 1)

    def test_delete_bio(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        bio = BiographicElementFactory.create(composer=self.composer)
        url = reverse('web:delete_biographic_element', args=[bio.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        assert_popup_redirects(response, reverse("profiles:personal_space") + "?tab=portfolio")
        self.assertEqual(BiographicElement.objects.count(), 0)

    def test_delete_bio_other(self):
        self._login_as_composer()
        other = ComposerFactory.create()
        bio = BiographicElementFactory.create(composer=other)
        url = reverse('web:delete_biographic_element', args=[bio.id])
        post_data = {'confirm': 'confirm'}
        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(BiographicElement.objects.count(), 1)


class SelectFromPersonalSpaceTest(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.competition = CompetitionFactory.create()

    def _login_as_composer(self):
        individual = IndividualFactory.create()
        self.user = individual.user

        # Create a valid composer profile for this user
        self.composer = ComposerFactory.create(user=self.user)
        self.assertTrue(self.composer.pk)
        self.assertTrue(is_composer(self.composer.user))

        self.client.login(email=individual.user.email, password="1234")

    def test_select_media_from_personal_space_empty(self):
        self._login_as_composer()
        url = reverse('web:select_media_from_portfolio', args=[self.competition.url, 'key', "MediaField"])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(0, len(soup.select('.portfolio-elements li')))

    def test_select_media_from_personal_space_all(self):
        self._login_as_composer()
        url = reverse('web:select_media_from_portfolio', args=[self.competition.url, 'key', "MediaField"])
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, owner=self.user)
        work2 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, owner=self.user)
        work3 = WorkFactory.create(no_video=True, visibility=Work.VISIBILITY_MEMBER, owner=self.user)
        WorkFactory.create()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(3, len(soup.select('.portfolio-elements li')))
        self.assertNotEqual(response, "Here is the list of your works which match these requirements")
        self.assertContains(response, work1.title)
        self.assertContains(response, work2.title)
        self.assertContains(response, work3.title)

    def test_select_media_from_personal_space_video(self):
        self._login_as_composer()
        url = reverse('web:select_media_from_portfolio', args=[self.competition.url, 'key', "VideoField"])
        work1 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, owner=self.user)
        work2 = WorkFactory.create(visibility=Work.VISIBILITY_MEMBER, owner=self.user)
        work3 = WorkFactory.create(no_video=True, owner=self.user, visibility=Work.VISIBILITY_MEMBER)
        WorkFactory.create()
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(2, len(soup.select('.portfolio-elements li')))
        self.assertNotEqual(
            response,
            "This field requires a video. Here is the list of your works which match these requirements"
        )
        self.assertContains(response, work1.title)
        self.assertContains(response, work2.title)
        self.assertNotContains(response, work3.title)
