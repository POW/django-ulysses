# -*- coding: utf-8 -*-

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase


class PlayerMiddlewareTest(BaseTestCase):

    def test_player_on(self):
        url = reverse('web:home')
        response = self.client.get(url, **{'HTTP_X_ULYSSES_PLAYER': 'on'})
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8)')
        self.assertTrue(content.find('<div class="ulysses-content">') == 0)
        self.assertNotContains(response, '<body>')

    def test_player_off(self):
        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        content = response.content.decode('utf-8)')
        self.assertTrue(content.find('<div class="ulysses-content">') > 0)
        self.assertContains(response, '<body>')
