# -*- coding: utf-8 -*-

import os
import shutil
from os.path import dirname, abspath
from bs4 import BeautifulSoup

from django.test.client import RequestFactory
from django.urls import reverse

from ulysses.composers.models import is_composer
from ulysses.composers.factories import ComposerFactory
from ulysses.competitions.models import (
    CompetitionStatus, Candidate, get_candidate_folder_helper, ApplicationFormFieldType, ApplicationDraft,
    ApplicationFormFieldDefinition, ApplicationFormFieldSetDefinition
)
from ulysses.competitions.factories import (
    CompetitionFactory, TemporaryMediaFactory, TemporaryDocumentFactory, ApplicationFormFieldSetDefinitionFactory,
    ApplicationFormFieldDefinitionFactory, DynamicApplicationFormFactory, ApplicationDraftFactory
)
from ulysses.generic.tests import assert_popup_redirects, BaseTestCase
from ulysses.profiles.factories import IndividualFactory, OrganizationFactory

from ..views.applications import upload_as_temporary_file, get_temporary_file_url


class ComposerApplicationTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):

        # Create a active user
        individual = IndividualFactory.create()
        self.user = individual.user

        # Create a valid composer profile for this user
        self.composer = ComposerFactory.create(user=self.user)
        self.assertTrue(self.composer.pk)
        self.assertTrue(is_composer(self.composer.user))

        self.client.login(email=individual.user.email, password="1234")

    def create_temporary_file(self):

        base_dir = abspath(dirname(__file__))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": file_to_upload})
        upload_request.user = self.user

        upload_response = upload_as_temporary_file(upload_request)
        return upload_response.content.decode('utf-8')

    def test_submit_application(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            open=True,
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        motivation_letter = self.create_temporary_file()
        official_id = self.create_temporary_file()
        work1 = self.create_temporary_file()
        work2 = self.create_temporary_file()

        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)
        official_id_url = get_temporary_file_url(self.user, official_id)
        work1_url = get_temporary_file_url(self.user, work1)
        work2_url = get_temporary_file_url(self.user, work2)

        post_data = {
            'submit_application': True,
            'education': "Sample text for education",
            'experience': "Sample text for experience",
            'prices': "Sample text for prizes",
            'motivation_letter_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
            ).pk,
            'official_id_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, file=official_id_url, competition=competition, key='official_id'
            ).pk,
            'work1_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work1_url, competition=competition, key='work1'
            ).pk,
            'work2_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work2_url, competition=competition, key='work2'
            ).pk,
            'tef_hidden': '',
            'other_document1_hidden': '',
            'other_document2_hidden': '',
        }

        # Remove test file for target directory
        candidate_folder = get_candidate_folder_helper(competition, self.composer)
        shutil.rmtree(candidate_folder)

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has been created
        Candidate.objects.get(composer=self.composer, competition=competition)

    def test_submit_application_closed_competition(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            closed=True,
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        motivation_letter = self.create_temporary_file()
        official_id = self.create_temporary_file()
        work1 = self.create_temporary_file()
        work2 = self.create_temporary_file()

        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)
        official_id_url = get_temporary_file_url(self.user, official_id)
        work1_url = get_temporary_file_url(self.user, work1)
        work2_url = get_temporary_file_url(self.user, work2)

        post_data = {
            'submit_application': True,
            'education': "Sample text for education",
            'experience': "Sample text for experience",
            'prices': "Sample text for prizes",
            'motivation_letter_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
            ).pk,
            'official_id_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, file=official_id_url, competition=competition, key='official_id'
            ).pk,
            'work1_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work1_url, competition=competition, key='work1'
            ).pk,
            'work2_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work2_url, competition=competition, key='work2'
            ).pk,
            'tef_hidden': '',
            'other_document1_hidden': '',
            'other_document2_hidden': '',
        }

        # Remove test file for target directory
        candidate_folder = get_candidate_folder_helper(competition, self.composer)
        shutil.rmtree(candidate_folder)

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_with_dynamic_form(self):

        # Create a dynamic application form, with fieldsets & fields

        dynamic_application_form = DynamicApplicationFormFactory.create()

        for fieldset_order_index in range(0, 3):
            fieldset_definition = ApplicationFormFieldSetDefinitionFactory.create(
                parent=dynamic_application_form,
                order=fieldset_order_index
            )
            for field_order_index in range(0, 3):
                ApplicationFormFieldDefinitionFactory.create(
                    parent=fieldset_definition,
                    order=field_order_index
                )

        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
        }

        for fieldset_definition in ApplicationFormFieldSetDefinition.objects.filter(parent=dynamic_application_form):
            for field in ApplicationFormFieldDefinition.objects.filter(parent=fieldset_definition):
                field_name = field.name
                if field.kind.name == "BiographicElementField":
                    val = "Sample biographic element"
                elif field.kind.name == "ApplicationBooleanElementField":
                    val = True
                elif field.kind.name == "ApplicationHtmlElementField":
                    val = "<p>Test data</p>"
                elif field.kind.name == "ApplicationTextElementField":
                    val = "Test data"
                elif field.kind.name == "DocumentField":
                    tmp_file = self.create_temporary_file()
                    tmp_file_url = get_temporary_file_url(self.user, tmp_file )
                    val = TemporaryDocumentFactory.create(
                        composer=self.composer, competition=competition, file=tmp_file_url, key=field.name
                    ).pk,
                    field_name += "_hidden"
                elif field.kind.name == "MediaField":
                    tmp_file = self.create_temporary_file()
                    tmp_file_url = get_temporary_file_url(self.user, tmp_file )
                    val = TemporaryMediaFactory.create(
                        composer=self.composer, competition=competition, score=tmp_file_url, key=field.name
                    ).pk,
                    field_name += "_hidden"
                else:
                    raise NotImplementedError("Unhandled field type : %s" % field.kind.name)
                post_data[field_name] = val

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has been created
        Candidate.objects.get(composer=self.composer, competition=competition)

    def test_submit_application_with_fields_not_visible_by_jury(self):
        # Create a dynamic application form, with fieldsets & fields

        dynamic_application_form = DynamicApplicationFormFactory.create()

        for fieldset_order_index in range(0, 3):
            fieldset_definition = ApplicationFormFieldSetDefinitionFactory.create(
                parent=dynamic_application_form,
                order=fieldset_order_index
            )
            for field_order_index in range(0, 3):
                ApplicationFormFieldDefinitionFactory.create(
                    parent=fieldset_definition,
                    order=field_order_index,
                    is_visible_by_jury=True if field_order_index != 1 else False
                )

        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form)

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
        }

        for fieldset_definition in ApplicationFormFieldSetDefinition.objects.filter(parent=dynamic_application_form):
            for field in ApplicationFormFieldDefinition.objects.filter(parent=fieldset_definition):
                field_name = field.name
                if field.kind.name == "BiographicElementField":
                    val = "Sample biographic element"
                elif field.kind.name == "ApplicationBooleanElementField":
                    val = True
                elif field.kind.name == "ApplicationHtmlElementField":
                    val = "<p>Test data</p>"
                elif field.kind.name == "ApplicationTextElementField":
                    val = "Test data"
                elif field.kind.name == "DocumentField":
                    tmp_file = self.create_temporary_file()
                    tmp_file_url = get_temporary_file_url(self.user, tmp_file)
                    val = TemporaryDocumentFactory.create(
                        composer=self.composer, competition=competition, file=tmp_file_url, key=field.name
                    ).pk,
                    field_name += "_hidden"
                elif field.kind.name == "MediaField":
                    tmp_file = self.create_temporary_file()
                    tmp_file_url = get_temporary_file_url(self.user, tmp_file)
                    val = TemporaryMediaFactory.create(
                        composer=self.composer, competition=competition, score=tmp_file_url, key=field.name
                    ).pk,
                    field_name += "_hidden"
                else:
                    raise NotImplementedError("Unhandled field type : %s" % field.kind.name)
                post_data[field_name] = val

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has been created
        Candidate.objects.get(composer=self.composer, competition=competition)

    def test_submit_application_email_validation_fail(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="an_email",
            validator="email",
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'an_email': "this-is-not-an-email",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        errors = soup.select("ul.errorlist li")
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0].text, "this-is-not-an-email is not a valid email")

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_email_text_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="an_email",
            validator="email",
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'an_email': "john@acme.com",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_url_validation_fail(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="an_url",
            validator="url",
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'an_url': "this-is-not-an-url",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        errors = soup.select("ul.errorlist li")
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0].text, "this-is-not-an-url is not a valid URL")

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_url_text_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="an_url",
            validator="url",
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'an_url': "https://www.ircam.fr/",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_no_validator(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="a_text",
            validator="",
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'a_text': "a text",
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_text_fail(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="my_text",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_text': "12345678901"  #11 chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        errors = soup.select("ul.errorlist li")
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0].text, "The length is greater than 10 characters")

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_text_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationTextElementField"),
            name="my_text",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_text': "1234567890"  #10chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_html_fail(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationHtmlElementField"),
            name="my_html",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_html': "<p>12345678901</p>"  #11chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        errors = soup.select("ul.errorlist li")
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0].text, "The length is greater than 10 characters")

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_html_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="ApplicationHtmlElementField"),
            name="my_html",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_html': "<p>1234567890</p>"  #10chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_bio_fail(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="BiographicElementField"),
            name="my_html",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_html': "<p>12345678901</p>"  #11chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, "html.parser")
        errors = soup.select("ul.errorlist li")
        self.assertEqual(len(errors), 1)
        self.assertEqual(errors[0].text, "The length is greater than 10 characters")

        # Ensure 'candidate' object has not been created
        self.assertEqual(0, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_maximum_length_bio_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="BiographicElementField"),
            name="my_html",
            maximum_length=10
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_html': "<p>1234567890</p>"  #10chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_bio_no_maximum_success(self):

        dynamic_application_form = DynamicApplicationFormFactory.create()

        set1 = ApplicationFormFieldSetDefinitionFactory.create(
            parent=dynamic_application_form
        )

        field1 = ApplicationFormFieldDefinitionFactory.create(
            parent=set1,
            kind=ApplicationFormFieldType.objects.get(name="BiographicElementField"),
            name="my_html",
            maximum_length=0
        )
        # Create an open competition associated to this dynamic application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=True,
            dynamic_application_form=dynamic_application_form
        )

        # Display and submit application

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        post_data = {
            'submit_application': True,
            'my_html': "<p>12345678901234</p>"  # 10chars
        }

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has not been created
        self.assertEqual(1, Candidate.objects.filter(composer=self.composer, competition=competition).count())

    def test_submit_application_new_individual(self):

        # Log with a new user
        self.client.logout()

        individual = IndividualFactory.create()
        self.user = individual.user

        self.client.login(email=individual.user.email, password="1234")

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        motivation_letter = self.create_temporary_file()
        official_id = self.create_temporary_file()
        work1 = self.create_temporary_file()
        work2 = self.create_temporary_file()

        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)
        official_id_url = get_temporary_file_url(self.user, official_id)
        work1_url = get_temporary_file_url(self.user, work1)
        work2_url = get_temporary_file_url(self.user, work2)

        post_data = {
            'submit_application': True,
            'education': "Sample text for education",
            'experience': "Sample text for experience",
            'prices': "Sample text for prizes",
            'motivation_letter_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
            ).pk,
            'official_id_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, file=official_id_url, competition=competition, key='official_id'
            ).pk,
            'work1_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work1_url, competition=competition, key='work1'
            ).pk,
            'work2_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work2_url, competition=competition, key='work2'
            ).pk,
            'tef_hidden': '',
            'other_document1_hidden': '',
            'other_document2_hidden': '',
        }

        # Remove test file for target directory
        candidate_folder = get_candidate_folder_helper(competition, self.composer)
        shutil.rmtree(candidate_folder)

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has been created
        Candidate.objects.get(composer__user=individual.user, competition=competition)

    def test_submit_application_new_organization(self):

        # Log with a new user
        self.client.logout()

        organization = OrganizationFactory.create()
        self.user = organization.user

        self.client.login(email=organization.user.email, password="1234")

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:show_application_form", args=[competition.url])
        self.client.get(url)

        motivation_letter = self.create_temporary_file()
        official_id = self.create_temporary_file()
        work1 = self.create_temporary_file()
        work2 = self.create_temporary_file()

        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)
        official_id_url = get_temporary_file_url(self.user, official_id)
        work1_url = get_temporary_file_url(self.user, work1)
        work2_url = get_temporary_file_url(self.user, work2)

        post_data = {
            'submit_application': True,
            'education': "Sample text for education",
            'experience': "Sample text for experience",
            'prices': "Sample text for prizes",
            'motivation_letter_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
            ).pk,
            'official_id_hidden': TemporaryDocumentFactory.create(
                composer=self.composer, file=official_id_url, competition=competition, key='official_id'
            ).pk,
            'work1_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work1_url, competition=competition, key='work1'
            ).pk,
            'work2_hidden': TemporaryMediaFactory.create(
                composer=self.composer, score=work2_url, competition=competition, key='work2'
            ).pk,
            'tef_hidden': '',
            'other_document1_hidden': '',
            'other_document2_hidden': '',
        }

        # Remove test file for target directory
        candidate_folder = get_candidate_folder_helper(competition, self.composer)
        shutil.rmtree(candidate_folder)

        response = self.client.post(url, post_data)
        self.assertEqual(response.status_code, 302)

        # Ensure 'candidate' object has been created
        Candidate.objects.get(composer__user=organization.user, competition=competition)

    def test_view_confirm_delete_draft(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        draft = ApplicationDraftFactory.create(
            competition=competition, composer=self.composer
        )

        url = reverse("web:confirm_application_action", args=[competition.id, 'delete_draft'])
        response = self.client.get(url)
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, ApplicationDraft.objects.filter(id=draft.id).count())

    def test_post_confirm_delete_draft(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        draft = ApplicationDraftFactory.create(
            competition=competition, composer=self.composer
        )

        url = reverse("web:confirm_application_action", args=[competition.id, 'delete_draft'])
        response = self.client.post(url, data={'confirm': 'confirm'})
        assert_popup_redirects(response, competition.get_absolute_url())

        self.assertEqual(0, ApplicationDraft.objects.filter(id=draft.id).count())

    def test_post_confirm_delete_draft_other(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        draft = ApplicationDraftFactory.create(competition=competition)

        url = reverse("web:confirm_application_action", args=[competition.id, 'delete_draft'])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)

        self.assertEqual(1, ApplicationDraft.objects.filter(id=draft.id).count())

    def test_post_confirm_delete_draft_unknown(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:confirm_application_action", args=[competition.id, 'delete_draft'])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(200, response.status_code)

    def test_post_confirm_delete_draft_competition_unknown(self):

        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:confirm_application_action", args=[competition.id + 1, 'delete_draft'])
        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(404, response.status_code)

    def test_view_with_draft(self):
        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )
        ApplicationDraftFactory.create(
            competition=competition, composer=self.composer
        )
        url = reverse("web:show_application_form", args=[competition.url])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['has_draft'], True)

    def test_view_without_draft(self):
        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )

        url = reverse("web:show_application_form", args=[competition.url])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['has_draft'], False)

    def test_view_reset_draft(self):
        # Create an open competition with a concrete application form
        competition = CompetitionFactory.create(
            status=CompetitionStatus.objects.get(name='open'),
            use_dynamic_form=False,
            application_form_class="IrcamCursus1ApplicationForm"
        )
        ApplicationDraftFactory.create(
            competition=competition, composer=self.composer
        )
        url = reverse("web:show_application_form", args=[competition.url])
        response = self.client.get(url, data={'reset': ''})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['has_draft'], False)