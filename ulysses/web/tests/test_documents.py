# -*- coding: utf-8 -*-

import os
from os.path import dirname, abspath
from bs4 import BeautifulSoup

from django.test.client import RequestFactory
from django.urls import reverse

from ulysses.composers.models import is_composer
from ulysses.composers.factories import ComposerFactory
from ulysses.competitions.models import (
    ApplicationFormFieldType, TemporaryDocumentExtraFile, TemporaryDocument
)
from ulysses.competitions.factories import (
    CompetitionFactory, TemporaryDocumentFactory, ApplicationFormFieldSetDefinitionFactory,
    ApplicationFormFieldDefinitionFactory, DynamicApplicationFormFactory, TemporaryDocumentExtraFileFactory
)
from ulysses.generic.tests import BaseTestCase
from ulysses.profiles.factories import IndividualFactory

from ..views.applications import upload_as_temporary_file, get_temporary_file_url


class DocumentApplicationTest(BaseTestCase):
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def setUp(self):

        # Create a active user
        individual = IndividualFactory.create()
        self.user = individual.user

        # Create a valid composer profile for this user
        self.composer = ComposerFactory.create(user=self.user)
        self.assertTrue(self.composer.pk)
        self.assertTrue(is_composer(self.composer.user))

        self.client.login(email=individual.user.email, password="1234")

    def create_temporary_file(self):

        base_dir = abspath(dirname(__file__))
        sample_doc_path = os.path.join(base_dir, "fixtures", 'sample-doc.pdf')

        file_to_upload = open(sample_doc_path, 'r', encoding='latin-1')
        upload_request = RequestFactory().post("/", {"username": self.user.username, "Filedata": file_to_upload})
        upload_request.user = self.user

        upload_response = upload_as_temporary_file(upload_request)
        return upload_response.content.decode('utf-8')

    def test_post_new_document_file(self):

        form = DynamicApplicationFormFactory.create()
        fieldset = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset,
            kind=ApplicationFormFieldType.objects.get(name="DocumentField"),
            name="motivation_letter",
            extra_files=2,
        )

        competition = CompetitionFactory.create(
            open=True,
            use_dynamic_form=True,
            dynamic_application_form=form
        )

        motivation_letter = self.create_temporary_file()
        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)

        motivation2_letter = self.create_temporary_file()
        motivation2_letter_url = get_temporary_file_url(self.user, motivation2_letter)
        document = TemporaryDocumentFactory.create(
            composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
        )

        url = reverse("web:add_temporary_document_file", args=[competition.url, document.key])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('#id_file')), 1)
        self.assertEqual(len(soup.select('#id_file_path')), 1)
        self.assertEqual(len(soup.select('#id_title')), 0)

        post_data = {
            'file': os.path.split(motivation2_letter_url)[-1],
            'file_path': motivation2_letter_url,
        }
        response = self.client.post(url, data=post_data)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, TemporaryDocumentExtraFile.objects.count())
        extra_file = TemporaryDocumentExtraFile.objects.all()[0]
        self.assertEqual(extra_file.document, document)
        self.assertNotEqual(extra_file.file, '')

    def test_post_new_document_file_limit_reached(self):

        form = DynamicApplicationFormFactory.create()
        fieldset = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset,
            kind=ApplicationFormFieldType.objects.get(name="DocumentField"),
            name="motivation_letter",
            extra_files=1,
        )

        competition = CompetitionFactory.create(
            open=True,
            use_dynamic_form=True,
            dynamic_application_form=form
        )

        motivation_letter = self.create_temporary_file()
        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)

        motivation2_letter = self.create_temporary_file()
        motivation2_letter_url = get_temporary_file_url(self.user, motivation2_letter)

        document = TemporaryDocumentFactory.create(
            composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
        )
        TemporaryDocumentExtraFileFactory.create(document=document, file=motivation2_letter_url)

        url = reverse("web:add_temporary_document_file", args=[competition.url, document.key])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select('#id_file')), 0)
        self.assertEqual(len(soup.select('#id_file_path')), 0)
        self.assertEqual(len(soup.select('#id_title')), 0)
        self.assertEqual(1, TemporaryDocumentExtraFile.objects.count())

    def test_post_remove_document_file(self):

        form = DynamicApplicationFormFactory.create()
        fieldset = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset,
            kind=ApplicationFormFieldType.objects.get(name="DocumentField"),
            name="motivation_letter",
            extra_files=1,
        )

        competition = CompetitionFactory.create(
            open=True,
            use_dynamic_form=True,
            dynamic_application_form=form
        )

        motivation_letter = self.create_temporary_file()
        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)

        motivation2_letter = self.create_temporary_file()
        motivation2_letter_url = get_temporary_file_url(self.user, motivation2_letter)

        document = TemporaryDocumentFactory.create(
            composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
        )
        extra_file = TemporaryDocumentExtraFileFactory.create(document=document, file=motivation2_letter_url)

        url = reverse("web:remove_temporary_document_file", args=[document.id, extra_file.id])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, TemporaryDocumentExtraFile.objects.count())

        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, TemporaryDocumentExtraFile.objects.count())

    def test_post_remove_document(self):

        form = DynamicApplicationFormFactory.create()
        fieldset = ApplicationFormFieldSetDefinitionFactory.create(parent=form)
        field = ApplicationFormFieldDefinitionFactory.create(
            parent=fieldset,
            kind=ApplicationFormFieldType.objects.get(name="DocumentField"),
            name="motivation_letter",
            extra_files=1,
        )

        competition = CompetitionFactory.create(
            open=True,
            use_dynamic_form=True,
            dynamic_application_form=form
        )

        motivation_letter = self.create_temporary_file()
        motivation_letter_url = get_temporary_file_url(self.user, motivation_letter)

        motivation2_letter = self.create_temporary_file()
        motivation2_letter_url = get_temporary_file_url(self.user, motivation2_letter)

        document = TemporaryDocumentFactory.create(
            composer=self.composer, competition=competition, file=motivation_letter_url, key='motivation_letter'
        )
        TemporaryDocumentExtraFileFactory.create(document=document, file=motivation2_letter_url)

        url = reverse("web:remove_document", args=[competition.url, document.key])
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(1, TemporaryDocument.objects.count())
        self.assertEqual(1, TemporaryDocumentExtraFile.objects.count())

        response = self.client.post(url, data={'confirm': 'confirm'})
        self.assertEqual(response.status_code, 200)
        self.assertEqual(0, TemporaryDocument.objects.count())
        self.assertEqual(0, TemporaryDocumentExtraFile.objects.count())
