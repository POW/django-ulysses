# -*- coding: utf-8 -*-

from datetime import date, datetime, timedelta
import os.path

import xlrd
from io import BytesIO

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase
from ulysses.competitions.models import StaticContent, CompetitionManager, JuryMember
from ulysses.events.factories import EventFactory
from ulysses.profiles.factories import IndividualFactory
from ulysses.web.models import NewsletterSubscription
from ulysses.utils import get_youtube_code, is_mp3_valid


class RedirectWebTest(BaseTestCase):
    """The members should be displayed on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_redirect_web_home(self):
        """it should display homepage"""
        expected_url = reverse('web:home')
        web_url = '/web' + expected_url
        response = self.client.get(web_url)
        self.assertRedirects(response, expected_url)

    def test_redirect_web_home2(self):
        """it should display homepage"""
        web_url = '/web' + reverse('web:home_web')  # /web/home/
        response = self.client.get(web_url, follow=True)
        self.assertRedirects(response, reverse('web:home'))  # /

    def test_redirect_web_events(self):
        """it should display event detail page"""
        profile = IndividualFactory.create()
        profile2 = IndividualFactory.create()
        self.assertTrue(self.client.login(email=profile.user.email, password="1234"))
        event1 = EventFactory.create(owner=profile2.user)
        expected_url = reverse('events:view_event', args=[event1.id])
        web_url = '/web' + expected_url
        response = self.client.get(web_url)
        self.assertRedirects(response, expected_url)


class SubscribeNewsletterTest(BaseTestCase):

    def test_get_subscribe(self):
        url = reverse('web:subscribe_newsletter')
        response = self.client.get(url, follow=True)
        self.assertRedirects(response, reverse('web:home'))

    def test_post_subscribe(self):
        url = reverse('web:subscribe_newsletter')
        data = {
            'email': "toto@toto.fr",
            'accept_cgu': True,
        }
        response = self.client.post(url, data, follow=True)
        self.assertRedirects(response, reverse('web:home'))
        self.assertEqual(NewsletterSubscription.objects.count(), 1)
        subscription = NewsletterSubscription.objects.all()[0]
        self.assertEqual(subscription.email, data['email'])
        self.assertEqual(subscription.date.date(), date.today())
        self.assertEqual(subscription.at_registration, False)
        self.assertEqual(subscription.message_business, False)
        self.assertEqual(subscription.unsubscribe, False)
        self.assertEqual(subscription.deleted_account, False)

    def test_post_subscribe_dont_accept(self):
        url = reverse('web:subscribe_newsletter')
        data = {
            'email': "toto@toto.fr",
            # 'accept_cgu': True,
        }
        response = self.client.post(url, data, follow=True)
        self.assertRedirects(response, reverse('web:home'))
        self.assertEqual(NewsletterSubscription.objects.count(), 0)

    def test_post_subscribe_invalid_email(self):
        url = reverse('web:subscribe_newsletter')
        data = {
            'email': "bla",
            'accept_cgu': True,
        }
        response = self.client.post(url, data, follow=True)
        self.assertRedirects(response, reverse('web:home'))
        self.assertEqual(NewsletterSubscription.objects.count(), 0)


class ExportNewsletterSubscriptionTest(BaseTestCase):

    def test_subscribtion_anonymous(self):
        url = reverse('web:export_newsletter_subscriptions')
        response = self.client.get(url, follow=True)
        self.assertEqual(403, response.status_code)

    def test_subscription_staff(self):
        staff_member = IndividualFactory.create()
        staff_user = staff_member.user
        staff_user.is_staff = True
        staff_user.save()
        self.assertEqual(True, self.client.login(email=staff_user.email, password="1234"))
        url = reverse('web:export_newsletter_subscriptions')
        response = self.client.get(url, follow=True)
        self.assertEqual(403, response.status_code)

    def test_subscription_super_user(self):
        staff_member = IndividualFactory.create()
        staff_user = staff_member.user
        staff_user.is_superuser = True
        staff_user.save()
        self.assertEqual(True, self.client.login(email=staff_user.email, password="1234"))
        url = reverse('web:export_newsletter_subscriptions')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)

    def test_subscriptions(self):
        staff_member = IndividualFactory.create()
        staff_user = staff_member.user
        staff_user.is_superuser = True
        staff_user.save()
        self.assertEqual(True, self.client.login(email=staff_user.email, password="1234"))

        email1 = "unsubscribe@test.fr"
        NewsletterSubscription.objects.create(
            email=email1, date=datetime.now() - timedelta(days=1), at_registration=True, message_business=False,
            unsubscribe=False, deleted_account=False
        )
        NewsletterSubscription.objects.create(
            email=email1, date=datetime.now(), at_registration=True, message_business=False, unsubscribe=True,
            deleted_account=False
        )

        email2 = "deleteaccount@test.fr"
        NewsletterSubscription.objects.create(
            email=email2, date=datetime.now() - timedelta(days=1), at_registration=True, message_business=False,
            unsubscribe=False, deleted_account=False
        )
        NewsletterSubscription.objects.create(
            email=email2, date=datetime.now(), at_registration=True, message_business=False, unsubscribe=False,
            deleted_account=True
        )

        email3 = "valid1@test.fr"
        NewsletterSubscription.objects.create(
            email=email3, date=datetime.now() - timedelta(days=1), at_registration=False, message_business=True,
            unsubscribe=False, deleted_account=False
        )

        email4 = "valid2@test.fr"
        NewsletterSubscription.objects.create(
            email=email4, date=datetime.now() - timedelta(days=1), at_registration=True, message_business=False,
            unsubscribe=False, deleted_account=False
        )

        url = reverse('web:export_newsletter_subscriptions')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)

        data_io = BytesIO(response.content)
        workbook = xlrd.open_workbook(file_contents=data_io.getvalue())
        worksheet = workbook.sheet_by_index(0)

        emails = []
        line_index = 0
        while True:
            try:
                value = worksheet.cell(line_index, 0).value
            except IndexError:
                value = None
            if value:
                emails.append(value)
                line_index += 1
            else:
                break
        self.assertEqual(len(emails), 2)
        self.assertFalse(email1 in emails)
        self.assertFalse(email2 in emails)
        self.assertTrue(email3 in emails)
        self.assertTrue(email4 in emails)


class HelpTest(BaseTestCase):
    fixtures = ['groups.json']

    def test_help_anonymous(self):
        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=True
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=True
        )
        whats_new_guidelines = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=True
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=True
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, '<p>Test users</p>')
        self.assertNotContains(response, '<p>Test admin</p>')
        self.assertContains(response, '<p>Test news</p>')
        self.assertNotContains(response, '<p>Test jury</p>')
        self.assertNotContains(response, '<p>Super admin</p>')

    def test_help_member(self):
        member = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=member.user.email, password="1234"))

        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=True
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=True
        )
        whats_new = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=True
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=True
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, '<p>Test users</p>')
        self.assertNotContains(response, '<p>Test admin</p>')
        self.assertContains(response, '<p>Test news</p>')
        self.assertNotContains(response, '<p>Test jury</p>')
        self.assertNotContains(response, '<p>Super admin</p>')

    def test_help_call_admin(self):
        member = IndividualFactory.create()
        CompetitionManager.objects.create(user=member.user)
        self.assertEqual(True, self.client.login(email=member.user.email, password="1234"))

        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=True
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=True
        )
        whats_new = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=True
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=True
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, '<p>Test users</p>')
        self.assertContains(response, '<p>Test admin</p>')
        self.assertContains(response, '<p>Test news</p>')
        self.assertContains(response, '<p>Test jury</p>')
        self.assertNotContains(response, '<p>Super admin</p>')

    def test_help_jury(self):
        member = IndividualFactory.create()
        JuryMember.objects.create(user=member.user)
        self.assertEqual(True, self.client.login(email=member.user.email, password="1234"))

        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=True
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=True
        )
        whats_new = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=True
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=True
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, '<p>Test users</p>')
        self.assertNotContains(response, '<p>Test admin</p>')
        self.assertContains(response, '<p>Test news</p>')
        self.assertContains(response, '<p>Test jury</p>')
        self.assertNotContains(response, '<p>Super admin</p>')

    def test_help_not_published(self):
        member = IndividualFactory.create()
        CompetitionManager.objects.create(user=member.user)
        JuryMember.objects.create(user=member.user)
        self.assertEqual(True, self.client.login(email=member.user.email, password="1234"))

        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=False
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=False
        )
        whats_new = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=False
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=False
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertNotContains(response, '<p>Test users</p>')
        self.assertNotContains(response, '<p>Test admin</p>')
        self.assertNotContains(response, '<p>Test news</p>')
        self.assertNotContains(response, '<p>Test jury</p>')
        self.assertNotContains(response, '<p>Super admin</p>')

    def test_help_not_published_super_user(self):
        member = IndividualFactory.create()
        CompetitionManager.objects.create(user=member.user)
        JuryMember.objects.create(user=member.user)
        member.user.is_superuser = True
        member.user.save()

        self.assertEqual(True, self.client.login(email=member.user.email, password="1234"))

        user_guidelines = StaticContent.objects.create(
            key='user_guidelines', text="<h4>User</h4><p>Test users</p>", published=False
        )
        call_admin_guidelines = StaticContent.objects.create(
            key='call_admin_guidelines', text="<h4>Admin</h4><p>Test admin</p>", published=False
        )
        whats_new = StaticContent.objects.create(
            key='whats_new', text="<h4>News</h4><p>Test news</p>", published=False
        )
        jury_member_guidelines = StaticContent.objects.create(
            key='jury_member_guidelines', text="<h4>Jury</h4><p>Test jury</p>", published=False
        )
        superadmin_guidelines = StaticContent.objects.create(
            key='superadmin_guidelines', text="<h4>Super Admin</h4><p>Super admin</p>", published=True
        )

        url = reverse('web:help')
        response = self.client.get(url, follow=True)
        self.assertEqual(200, response.status_code)
        self.assertContains(response, '<p>Test users</p>')
        self.assertContains(response, '<p>Test admin</p>')
        self.assertContains(response, '<p>Test news</p>')
        self.assertContains(response, '<p>Test jury</p>')
        self.assertContains(response, '<p>Super admin</p>')


class YoutubeURLTest(BaseTestCase):

    def test_valid_url(self):
        self.assertEqual(
            get_youtube_code('https://www.youtube.com/watch?v=BX8j6wnWT6s'),
            'BX8j6wnWT6s'
        )

    def test_invalid_code(self):
        self.assertEqual(
            get_youtube_code('https://www.youtube.com/watch?feature=youtu.be'),
            None
        )

    def test_invalid_url(self):
        self.assertEqual(
            get_youtube_code('https://www.not-youtube.com/watch?v=BX8j6wnWT6s'),
            None
        )

    def test_features_arg1(self):
        self.assertEqual(
            get_youtube_code('https://www.youtube.com/watch?v=BX8j6wnWT6s&feature=youtu.be'),
            'BX8j6wnWT6s'
        )

    def test_features_arg2(self):
        self.assertEqual(
            get_youtube_code('https://www.youtube.com/watch?feature=youtu.be&v=BX8j6wnWT6s'),
            'BX8j6wnWT6s'
        )

    def test_valid_short(self):
        self.assertEqual(
            get_youtube_code('https://youtu.be/BX8j6wnWT6s'),
            'BX8j6wnWT6s'
        )

    def test_valid_short2(self):
        self.assertEqual(
            get_youtube_code('https://youtu.be/BX8j6wnWT6s/'),
            'BX8j6wnWT6s'
        )


class IsMp3ValidTest(BaseTestCase):

    @staticmethod
    def get_file_path(file_name):
        base_path = os.path.dirname(os.path.abspath(__file__))
        return os.path.join(base_path, 'fixtures/mp3', file_name)

    def test_is_valid_mp3(self):
        self.assertTrue(is_mp3_valid(self.get_file_path('valid-mp3.mp3')))

    def test_is_invalid_mp3(self):
        self.assertFalse(is_mp3_valid(self.get_file_path('invalid-mp3.mp3')))
