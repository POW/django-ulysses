# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, timedelta

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.competitions.factories import CompetitionFactory, CompetitionManagerFactory
from ulysses.profiles.factories import IndividualFactory


class CompetitionsListTest(BaseTestCase):
    """The competitions should be displayed in list"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def log_as(self, is_superuser=False):

        # Create a active user
        individual = IndividualFactory.create()
        self.user = individual.user
        if is_superuser:
            self.user.is_superuser = True
            self.user.save()

        self.client.login(email=individual.user.email, password="1234")

    def _create_competition(self, **kwargs):
        """create a competition"""
        today = date.today()
        data = {
            'publication_date': today,
            'opening_date': today + timedelta(days=1),
            'closing_date': today + timedelta(31),
            'result_date': today + timedelta(31),
        }
        data.update(kwargs)
        return CompetitionFactory.create(**data)

    def test_view_competitions_empty(self):
        """it should display page"""

        url = reverse('web:competitions')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".competition-block")), 0)

    def test_view_competitions(self):
        """it should display page"""

        competition1 = self._create_competition(open=True)
        competition2 = self._create_competition(published=True)
        competition3 = self._create_competition(draft=True)
        competition4 = self._create_competition(archived=True)

        url = reverse('web:competitions')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".competition-item")), 2)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))

    def test_view_competitions_draft_as_manager(self):
        """it should display page"""

        competition1 = self._create_competition(open=True)
        competition2 = self._create_competition(published=True)
        competition3 = self._create_competition(draft=True)
        competition4 = self._create_competition(archived=True)

        self.log_as(False)

        manager = CompetitionManagerFactory.create(user=self.user)
        for competition in (competition1, competition2, competition3, competition4):
            competition.managed_by.add(manager)
            competition.save()

        url = reverse('web:competitions')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".competition-item")), 3)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))

    def test_view_competitions_draft_as_manager_of_other(self):
        """it should display page"""

        competition1 = self._create_competition(open=True)
        competition2 = self._create_competition(published=True)
        competition3 = self._create_competition(draft=True)
        competition4 = self._create_competition(archived=True)

        self.log_as(False)

        manager = CompetitionManagerFactory.create(user=self.user)
        competition2.managed_by.add(manager)
        competition2.save()

        url = reverse('web:competitions')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".competition-item")), 2)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))

    def test_view_competitions_draft_as_superuser(self):
        """it should display page"""

        competition1 = self._create_competition(open=True)
        competition2 = self._create_competition(published=True)
        competition3 = self._create_competition(draft=True)
        competition4 = self._create_competition(archived=True)

        url = reverse('web:competitions')

        self.log_as(True)

        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".competition-item")), 3)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))


class CompetitionDetailsTest(BaseTestCase):
    """The competitions should be displayed in list"""
    fixtures = ['initial_data', 'initial_data_unittest', 'groups']

    def log_as(self, is_superuser=False):

        # Create a active user
        individual = IndividualFactory.create()
        self.user = individual.user
        if is_superuser:
            self.user.is_superuser = True
            self.user.save()

        self.client.login(email=individual.user.email, password="1234")

    def _create_competition(self, **kwargs):
        """create a competition"""
        today = date.today()
        data = {
            'publication_date': today,
            'opening_date': today + timedelta(days=1),
            'closing_date': today + timedelta(31),
            'result_date': today + timedelta(31),
        }
        data.update(kwargs)
        return CompetitionFactory.create(**data)

    def test_view_competitions_not_found(self):
        """it should display page"""

        url = reverse('web:competition', args=["dummy"])
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition_not_found.html")

    def test_view_competition_open(self):
        """it should display page"""
        closed_message = "Sorry, we are closed"
        competition1 = self._create_competition(open=True, closed_message=closed_message)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            1,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )
        self.assertNotContains(response, closed_message)

    def test_view_competition_closed(self):
        """it should display page"""
        closed_message = "Sorry, we are closed"
        competition1 = self._create_competition(closed=True, closed_message=closed_message)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            0,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )
        self.assertContains(response, closed_message)

    def test_view_competition_draft(self):
        """it should display page"""

        competition1 = self._create_competition(draft=True)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            1,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )

    def test_view_competition_published(self):
        """it should display page"""

        competition1 = self._create_competition(published=True)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            0,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )

    def test_view_competition_archived(self):
        """it should display page"""

        competition1 = self._create_competition(archived=True)

        url = reverse('web:competition', args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition_not_found.html")

    def test_view_competition_draft_as_manager(self):
        """it should display page"""

        competition1 = self._create_competition(draft=True)

        self.log_as(False)

        manager = CompetitionManagerFactory.create(user=self.user)
        competition1.managed_by.add(manager)
        competition1.save()

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            1,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )

    def test_view_competition_draft_as_manager_of_other(self):
        """it should display page"""

        competition1 = self._create_competition(draft=True)
        competition2 = self._create_competition(draft=True)

        self.log_as(False)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            1,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )

    def test_view_competition_draft_as_superuser(self):
        """it should display page"""

        competition1 = self._create_competition(draft=True)

        self.log_as(True)

        url = reverse('web:competition', args=[competition1.url])
        apply_url = reverse("web:show_application_form", args=[competition1.url])

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "web/competition.html")
        soup = BeautifulSoup(response.content, "html.parser")
        self.assertEqual(
            1,
            len(soup.select('a[href="{0}"]'.format(apply_url)))
        )
