# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import date, timedelta

from django.urls import reverse

from ulysses.generic.tests import BaseTestCase

from ulysses.competitions.factories import CompetitionFactory
from ulysses.profiles.factories import IndividualFactory, OrganizationFactory
from ulysses.social.factories import FollowingFactory, MemberPostFactory, FeedItemFactory
from ulysses.social.models import MemberPost
from ulysses.works.factories import WorkFactory

from ..factories import HomepageImageFactory, AnnounceCallImageFactory
from ..models import HomepageImage, AnnounceCallImage


class HomeMembersTest(BaseTestCase):
    """The members should be displayed on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_view_home_no_members(self):
        """it should display homepage"""

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 0)

    def test_view_home_anonymous(self):
        """it should display homepage"""

        individuals = IndividualFactory.create_batch(3)
        organizations = OrganizationFactory.create_batch(4)

        for individual in individuals:
            WorkFactory.create(owner=individual.user)

        for organization in organizations:
            WorkFactory.create(owner=organization.user)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 7)

        for individual in individuals:
            self.assertContains(response, individual.get_absolute_url())

        for organization in organizations:
            self.assertContains(response, organization.get_absolute_url())

    def test_view_home_anonymous_no_works(self):
        """it should display homepage"""

        individuals = IndividualFactory.create_batch(3)
        organizations = OrganizationFactory.create_batch(4)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 0)

    def test_view_home_anonymous_no_image(self):
        """it should display homepage"""

        individuals = IndividualFactory.create_batch(3, photo=None)
        organizations = OrganizationFactory.create_batch(4, logo=None)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        for individual in individuals:
            WorkFactory.create(owner=individual.user)

        for organization in organizations:
            WorkFactory.create(owner=organization.user)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 0)

    def test_view_home_anonymous_many_members(self):
        """it should display homepage"""

        individuals = IndividualFactory.create_batch(20)
        organizations = OrganizationFactory.create_batch(20)

        for individual in individuals:
            WorkFactory.create(owner=individual.user)

        for organization in organizations:
            WorkFactory.create(owner=organization.user)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 8)

    def test_view_home_anonymous_many_members_from_session(self):
        """it should display homepage"""

        individuals = IndividualFactory.create_batch(20)
        organizations = OrganizationFactory.create_batch(20)

        for individual in individuals:
            WorkFactory.create(owner=individual.user)

        for organization in organizations:
            WorkFactory.create(owner=organization.user)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        soup = BeautifulSoup(response.content, 'html.parser')
        self.assertEqual(len(soup.select(".member-item")), 8)
        session = self.client.session
        session_members = session["homepage_members"]
        self.assertEqual(len(session_members), 8)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.context['members'], session_members)

    def test_view_home_as_member(self):
        """it should display homepage"""

        me_as_individual = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=me_as_individual.user.email, password="1234"))

        individuals = IndividualFactory.create_batch(2)
        organizations = OrganizationFactory.create_batch(3)

        for individual in individuals:
            WorkFactory.create(owner=individual.user)

        for organization in organizations:
            WorkFactory.create(owner=organization.user)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".member-item")), 5)

        self.assertNotContains(response, me_as_individual.get_absolute_url())

        for individual in individuals:
            self.assertContains(response, individual.get_absolute_url())

        for organization in organizations:
            self.assertContains(response, organization.get_absolute_url())


class HomeCallsTest(BaseTestCase):
    """The calls should be displayed on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def _create_competition(self, **kwargs):
        """create a competition"""
        today = date.today()
        data = {
            'publication_date': today,
            'opening_date': today + timedelta(days=1),
            'closing_date': today + timedelta(31),
            'result_date': today + timedelta(31),
            'open': True,
            'archived': False,
        }
        data.update(kwargs)
        return CompetitionFactory.create(**data)

    def test_view_home_no_calls(self):
        """it should display homepage"""

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-competitions")), 1)
        self.assertEqual(len(soup.select(".competition-item")), 0)

    def test_view_home_anonymous_no_hompeage(self):
        """it should display homepage"""

        competition = self._create_competition()

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-competitions")), 1)
        self.assertEqual(len(soup.select(".competition-item")), 0)

        self.assertNotContains(response, reverse('web:competition', args=[competition.url]))

    def test_view_home_anonymous_homepage(self):
        """it should display homepage"""

        competition = self._create_competition(show_on_homepage=True)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-competitions")), 1)
        self.assertEqual(len(soup.select(".competition-item")), 1)

        self.assertContains(response, reverse('web:competition', args=[competition.url]))

    def test_view_home_anonymous_4(self):
        """it should display homepage"""

        competition1 = self._create_competition(show_on_homepage=True, order_index=1)
        competition2 = self._create_competition(show_on_homepage=True, order_index=2)
        competition3 = self._create_competition(show_on_homepage=True, order_index=3)
        competition4 = self._create_competition(show_on_homepage=True, order_index=5)
        competition5 = self._create_competition(show_on_homepage=True, order_index=4)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-competitions")), 1)
        self.assertEqual(len(soup.select(".competition-item")), 4)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))
        self.assertContains(response, reverse('web:competition', args=[competition5.url]))

    def test_view_home_as_member(self):
        """it should display homepage"""

        me_as_individual = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=me_as_individual.user.email, password="1234"))

        competition1 = self._create_competition(show_on_homepage=True, order_index=1)
        competition2 = self._create_competition(show_on_homepage=True, order_index=2)
        competition3 = self._create_competition(show_on_homepage=True, order_index=3)
        competition4 = self._create_competition(show_on_homepage=True, order_index=5)
        competition5 = self._create_competition(show_on_homepage=True, order_index=4)

        url = reverse('web:home')
        self.client.get(url)

        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        self.assertEqual(len(soup.select(".home-competitions")), 1)
        self.assertEqual(len(soup.select(".competition-item")), 4)

        self.assertContains(response, reverse('web:competition', args=[competition1.url]))
        self.assertContains(response, reverse('web:competition', args=[competition2.url]))
        self.assertContains(response, reverse('web:competition', args=[competition3.url]))
        self.assertNotContains(response, reverse('web:competition', args=[competition4.url]))
        self.assertContains(response, reverse('web:competition', args=[competition5.url]))


class HomepageImageTest(BaseTestCase):
    """The posts on the homepage"""
    fixtures = ['initial_data', 'initial_data_unittest', ]

    def test_homepage_image_active(self):
        """it should allow only 1 active homepage image"""

        image1 = HomepageImageFactory.create()
        self.assertEqual(image1.is_active, True)

        image2 = HomepageImageFactory.create()
        self.assertEqual(image2.is_active, True)

        image1 = HomepageImage.objects.get(id=image1.id)
        self.assertEqual(image1.is_active, False)

    def test_announce_call_image_active(self):
        """it should allow only 1 active homepage image"""

        image1 = AnnounceCallImageFactory.create()
        self.assertEqual(image1.is_active, True)

        image2 = AnnounceCallImageFactory.create()
        self.assertEqual(image2.is_active, True)

        image1 = AnnounceCallImage.objects.get(id=image1.id)
        self.assertEqual(image1.is_active, False)

    def test_announce_call_image_and_hompage_active(self):
        """it should allow only 1 active homepage image"""

        image1 = AnnounceCallImageFactory.create()
        self.assertEqual(image1.is_active, True)

        image2 = HomepageImageFactory.create()
        self.assertEqual(image2.is_active, True)

        image1 = AnnounceCallImage.objects.get(id=image1.id)
        self.assertEqual(image1.is_active, True)

    def test_homepage_anonymous_view_active_image(self):
        """it should display homepage image in the banner"""

        HomepageImageFactory.create(is_active=False)
        image2 = HomepageImageFactory.create(is_active=True)

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-banner-background")
        self.assertEqual(len(elts), 1)
        self.assertEqual(elts[0]['style'], "background-image: url('{0}');".format(image2.get_absolute_url()))

    def test_homepage_announce_call_view_active_image(self):
        """it should display homepage image in the banner"""

        HomepageImageFactory.create(is_active=False)
        HomepageImageFactory.create(is_active=True)
        AnnounceCallImageFactory.create(is_active=False)
        image4 = AnnounceCallImageFactory.create(is_active=True)

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-announce_call-background")
        self.assertEqual(len(elts), 1)
        self.assertEqual(elts[0]['style'], "background-image: url('{0}');".format(image4.get_absolute_url()))

    def test_homepage_anonymous_no_active_image(self):
        """it should display homepage image in the banner"""

        HomepageImageFactory.create(is_active=False)
        image2 = HomepageImageFactory.create(is_active=False)

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-banner-background")
        self.assertEqual(len(elts), 0)

    def test_homepage_announce_call_view_no_active_image(self):
        """it should display homepage image in the banner"""

        HomepageImageFactory.create(is_active=False)
        HomepageImageFactory.create(is_active=True)
        AnnounceCallImageFactory.create(is_active=False)
        AnnounceCallImageFactory.create(is_active=False)

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-announce_call-background")
        self.assertEqual(len(elts), 0)

    def test_homepage_logged_view_active_image(self):
        """it should not display banner with homepage image"""

        HomepageImageFactory.create(is_active=False)
        image2 = HomepageImageFactory.create(is_active=True)

        me_as_individual = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=me_as_individual.user.email, password="1234"))

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-banner-background")
        self.assertEqual(len(elts), 0)

    def test_homepage_logged_view_announce_call_image(self):
        """it should not display banner with homepage image"""

        image2 = AnnounceCallImageFactory.create(is_active=True)

        me_as_individual = IndividualFactory.create()
        self.assertEqual(True, self.client.login(email=me_as_individual.user.email, password="1234"))

        url = reverse('web:home')
        response = self.client.get(url)
        self.assertEqual(response.status_code, 200)

        soup = BeautifulSoup(response.content, 'html.parser')

        elts = soup.select(".home-announce_call-background")
        self.assertEqual(len(elts), 1)
        self.assertEqual(elts[0]['style'], "background-image: url('{0}');".format(image2.get_absolute_url()))
