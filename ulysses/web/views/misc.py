# -*- coding: utf-8 -*-

import json

from django.contrib import messages
from django.contrib.auth.models import User
from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse
from django.utils.translation import ugettext as _
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import RedirectView, FormView

from ulysses.competitions.models import StaticContent, is_jury_member
from ulysses.competitions.utils import is_competition_admin
from ulysses.generic.views import XlsExportView, LoginRequiredPopupView
from ulysses.profiles.utils import get_profile
from ulysses.utils import set_info_box_closed

from ..forms import AcceptNewsletterForm
from ..models import NewsletterSubscription


def _show_static_page(request, key, template):
    content = StaticContent.objects.get_or_create(key=key)[0]
    if not content.published:
        if request.user.is_superuser:
            messages.warning(request, _("This page is not published. Only super-admin can have access"))
        else:
            raise Http404
    context = {
        key: content,
    }
    return render(request, template, context)


def show_contact(request):
    return _show_static_page(request, "contact", 'web/contact.html')


def show_legal_mentions(request):
    return _show_static_page(request, "legal_mentions", 'web/legal_mentions.html')


def show_privacy_policy(request):
    return _show_static_page(request, "privacy_policy", 'web/privacy_policy.html')


def show_about(request):
    return _show_static_page(request, "about", 'web/about.html')


def show_help(request):

    user = request.user

    # User guidelines
    user_guidelines = StaticContent.objects.get_or_create(key='user_guidelines')[0]
    if not user_guidelines.published:
        if request.user.is_superuser:
            messages.warning(request, _("user_guidelines are not published yet. Only super-admin can have access"))
        else:
            user_guidelines = None

    # What's new
    whats_new = StaticContent.objects.get_or_create(key='whats_new')[0]
    if not whats_new.published:
        if request.user.is_superuser:
            messages.warning(
                request, _("whats_new is not published yet. Only super-admin can have access")
            )
        else:
            whats_new = None

    # competition admins
    if user.is_authenticated and is_competition_admin(user):
        call_admin_guidelines = StaticContent.objects.get_or_create(key='call_admin_guidelines')[0]
        if not call_admin_guidelines.published:
            if request.user.is_superuser:
                messages.warning(
                    request, _("call_admin_guidelines are not published yet. Only super-admin can have access")
                )
            else:
                call_admin_guidelines = None
    else:
        call_admin_guidelines = None

    # Jury
    if user.is_authenticated and (is_jury_member(user) or is_competition_admin(user)):
        jury_member_guidelines = StaticContent.objects.get_or_create(key='jury_member_guidelines')[0]
        if not jury_member_guidelines.published:
            if request.user.is_superuser:
                messages.warning(
                    request, _("jury_member_guidelines are not published yet. Only super-admin can have access")
                )
            else:
                jury_member_guidelines = None
    else:
        jury_member_guidelines = None

    # Super admin
    if user.is_authenticated and request.user.is_superuser:
        superadmin_guidelines = StaticContent.objects.get_or_create(key='superadmin_guidelines')[0]
    else:
        superadmin_guidelines = None

    context = {
        'user_guidelines': user_guidelines,
        'call_admin_guidelines': call_admin_guidelines,
        'whats_new': whats_new,
        'jury_member_guidelines': jury_member_guidelines,
        'superadmin_guidelines': superadmin_guidelines,
    }
    return render(request, 'web/help.html', context)


class RedirectWebView(RedirectView):
    """redirect "/web/xxx/ to '/xxx/"""

    def get_redirect_url(self, *args, **kwargs):
        path = self.kwargs['path']
        return "/" + path


@csrf_exempt
def hide_accept_cookies_message(request):
    """force cookie warning message to be hidden"""
    if request.method == 'POST':
        request.session['hide_accept_cookie_message'] = True
        data = {"Ok": True}
    else:
        data = {"Ok": False}
    return HttpResponse(json.dumps(data), content_type="application/json")


@csrf_exempt
def close_info_box(request, name):
    """force cookie warning message to be hidden"""
    if request.method == 'POST':
        set_info_box_closed(request, name)
        data = {"Ok": True}
    else:
        data = {"Ok": False}
    return HttpResponse(json.dumps(data), content_type="application/json")


class NewsletterSubscriptionView(FormView):
    form_class = AcceptNewsletterForm

    def get_success_url(self):
        success_url = self.request.META.get('HTTP_REFERER')
        if not success_url:
            success_url = reverse('web:home')
        return success_url

    def get(self, request, *args, **kwargs):
        return HttpResponseRedirect(self.get_success_url())

    def form_invalid(self, form):
        messages.error(self.request, '{0}'.format(form.errors))
        return super(NewsletterSubscriptionView, self).form_valid(form)

    def form_valid(self, form):
        email = form.cleaned_data['email']
        NewsletterSubscription.objects.create(email=email)
        # set the flag in profile
        users = User.objects.filter(email=email)
        for user in users:
            profile = get_profile(user)
            if profile:
                profile.ulysses_newsletter = True
                profile.save()
        messages.success(self.request, _('Thank you! You have been added to our mailing list.'))
        return super(NewsletterSubscriptionView, self).form_valid(form)


class NewsletterSubscriptionExcelView(XlsExportView):
    only_superuser = True

    def do_fill_workbook(self, workbook):
        """implement it in base class"""
        handled_emails = set()
        sheet = workbook.add_sheet(_(u"Emails"))
        line_index = 0
        for subscription in NewsletterSubscription.objects.all().order_by('-date'):
            if not subscription.email in handled_emails:
                handled_emails.add(subscription.email)
                if not subscription.unsubscribe and not subscription.deleted_account:
                    self.write_cell(sheet, line_index, 0, subscription.email)
                    line_index += 1


class YoutubePopupView(LoginRequiredPopupView):
    template_name = 'web/popup_youtube.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['youtube_code'] = self.kwargs['code']
        return context
