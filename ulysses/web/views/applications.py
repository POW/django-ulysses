# -*- coding: utf-8 -*-

import os

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib import messages
from django.core.exceptions import PermissionDenied
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import render, redirect, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from django.urls import reverse
from django.utils.translation import ugettext as _

from colorbox.decorators import popup_redirect

from ulysses.competitions.models import (
    get_user_temporary_folder, get_user_personal_folder,
    ApplicationDraft, BiographicElement, Candidate, CandidateBiographicElement, CandidateDocument, CandidateMedia,
    Competition, Document, Composer, DraftBiographicElement, DraftDocument, DraftMedia, Media, TemporaryDocument,
    TemporaryMedia
)
from ulysses.competitions.tasks import create_application
from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView, ulysses_popup_redirect
from ulysses.profiles.utils import get_profile
from ulysses.utils import make_string_storable_in_dict, upload_helper
from ulysses.works.models import Work

from ..forms import UploadDocumentForm, BiographicElementForm, UploadMediaForm


@csrf_exempt
@login_required
def upload_in_personal_space(request, user_id=0):
    if user_id == 0:
        user_id = request.user.id
    user = get_object_or_404(User, id=user_id)
    target_dir = get_user_personal_folder(user.username)
    return upload_helper(request, target_dir)


@csrf_exempt
@login_required
def upload_as_temporary_file(request, user_id=0):
    if user_id == 0:
        user_id = request.user.id
    user = get_object_or_404(User, id=user_id)
    target_dir = get_user_temporary_folder(user.username)
    return upload_helper(request, target_dir)


def get_composer_file_url(user, filename):
    return "%s/%s/%s" % (
        os.path.split(settings.PERSONAL_FILES_ROOT)[1], user.username,filename
    )


def get_temporary_file_url(user, filename):
    return "%s/%s/%s" % (
        os.path.split(settings.TEMPORARY_FILES_ROOT)[1], user.username, filename
    )


def get_composer_filename_from_url(url):
    return url.split('/')[2]


@ulysses_popup_redirect
@login_required
def edit_document(request, id):

    upload_url = reverse('web:upload_in_personal_space')
    context = {}
    doc = get_object_or_404(Document, composer=Composer.get_from_user(request.user), id=id)

    if request.method == 'POST':  # If the form has been submitted...
        form = UploadDocumentForm(request.POST, upload_url=upload_url)
        if form.is_valid():  # All validation rules pass
            # Update document object
            doc.file = get_composer_file_url(request.user, form.cleaned_data["file"])
            doc.title = form.cleaned_data["title"]
            doc.save()
            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        file = get_composer_filename_from_url(doc.file)
        form = UploadDocumentForm(data={"title": doc.title, "file": file}, upload_url=upload_url)

    context["form"] = form
    context["document"] = doc
    context["title"] = _("Edit document")

    return render(request, 'web/composers/edit_document.html', context)


@ulysses_popup_redirect
@login_required
def edit_media(request, id):
    composer = Composer.get_from_user(request.user)

    context = {}
    upload_url = reverse('web:upload_in_personal_space')
    media = get_object_or_404(Media, composer=composer, id=id)

    if request.method == 'POST':  # If the form has been submitted...
        form = UploadMediaForm(request.POST, upload_url=upload_url)
        if form.is_valid():
            # Update media object
            media.title = form.cleaned_data["title"]
            score = form.cleaned_data["score"]
            if score:
                media.score = get_composer_file_url(request.user,score)
            audio = form.cleaned_data["audio"]
            if audio:
                media.audio = get_composer_file_url(request.user,audio)
            video = form.cleaned_data["video"]
            if video:
                media.video = get_composer_file_url(request.user,video)
            media.link = form.cleaned_data["link"]
            media.notes = form.cleaned_data["notes"]
            media.save()
            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        data = {'title': media.title}
        if media.score:
            data['score'] = get_composer_filename_from_url(media.score)
        if media.audio:
            data['audio'] = get_composer_filename_from_url(media.audio)
        if media.video:
            data['video'] = get_composer_filename_from_url(media.video)
        if media.link:
            data['link'] = media.link
        if media.notes:
            data['notes'] = media.notes
        form = UploadMediaForm(data=data, upload_url=upload_url)

    context["form"] = form
    context["media"] = media
    context["title"] = "Edit Media"

    return render(request, 'web/composers/edit_media.html', context)


@login_required
def show_application_asynchronous_submission(request, url):
    context = {}
    competition = Competition.objects.get(url=url)
    context["competition"] = competition
    return render(request, 'web/application_asynchronous_submission.html', context)


@login_required
def show_application_submission_error(request, url):
    context = {}
    competition = Competition.objects.get(url=url)
    context["competition"] = competition
    return render(request, 'web/application_submission_error.html', context)


@login_required
def show_submitted_application(request, id):
    """show show_submitted_application"""
    composer = Composer.get_from_user(request.user)
    candidate = get_object_or_404(Candidate, composer=composer, id=id)  # only if we have a candidate
    return show_application_form(request, candidate.competition.url)
    #
    # params = {
    #     "candidate": ,
    # }
    # return render(request, 'web/composers/submitted_application.html', params)


def delete_draft(user, competition):
    draft_filter_args = {'composer': Composer.get_from_user(user), 'competition': competition}
    if ApplicationDraft.objects.filter(**draft_filter_args).exists():
        draft = ApplicationDraft.objects.get(**draft_filter_args)
        DraftBiographicElement.objects.filter(draft=draft).delete()
        DraftDocument.objects.filter(draft=draft).delete()
        DraftMedia.objects.filter(draft=draft).delete()
        draft.delete()
        return True
    return False


def save_application_draft(data, user, competition):
    """
    Save specified data into an application draft
    """
    # Create new draft
    form = competition.get_application_form_instance()
    form_fields_keys = [(fd.get_value_key(), fd.get_draft_key()) for fd in form.field_definitions]
    biographic_element_keys = form.get_biographic_element_keys()
    document_keys = form.get_document_keys()
    media_keys = form.get_media_keys()
    composer = Composer.get_from_user(user)
    draft, created = ApplicationDraft.objects.update_or_create(competition=competition, composer=composer)

    if created:
        draft.competition = competition
        draft.composer = composer

    draft_elements = {}
    for key_tuple in form_fields_keys:
        value_key = key_tuple[0]
        draft_key = key_tuple[1]
        # Don't save biographic elements here (they will be saved later)
        if value_key in biographic_element_keys:
            continue
        # Don't save document here (they will be saved later)
        if value_key in document_keys:
            continue
        # Don't save media here (they will be saved later)
        if value_key in media_keys:
            continue
        values_list = data.getlist(value_key)
        if values_list:
            if len(values_list) > 1:
                value = values_list
            else:
                value = make_string_storable_in_dict(values_list[0])
            draft_elements[draft_key] = value
    draft.data = "{" + ",".join(["\"%s\":\"%s\"" % (t[0], t[1]) for t in list(draft_elements.items()) if t[1]]) + "}"
    draft.save()

    # Create draft biographic elements
    for key in biographic_element_keys:
        if key in data and data[key]:
            bio = DraftBiographicElement()
            bio.draft = draft
            bio.key = key
            bio.title = form.get_field_definition_from_name(key).label
            bio.text = data[key]
            bio.save()

    # Create draft documents
    for key in document_keys:
        hidden_key = key + "_hidden"
        if hidden_key in data and data[hidden_key]:
            try:
                temporary_doc = TemporaryDocument.objects.get(id=data[hidden_key])
                temporary_doc.move_as_draft_element(draft)
            except TemporaryDocument.DoesNotExist:
                pass
        else:
            # Remove documents which have been cleared
            DraftDocument.objects.filter(draft=draft, key=key).delete()

    # Create draft medias
    for key in media_keys:
        hidden_key = key + "_hidden"
        if hidden_key in data and data[hidden_key]:
            temporary_work = TemporaryMedia.objects.get(id=data[hidden_key])
            temporary_work.move_as_draft_element(draft)
        else:
            # Remove documents which have been cleared
            DraftMedia.objects.filter(draft=draft, key=key).delete()
    return draft


@login_required
def get_application_filled_form_from_draft(request, competition):
    composer = Composer.get_from_user(request.user)
    draft = ApplicationDraft.objects.get(composer=composer, competition=competition)
    application_form = competition.get_application_form_instance()
    form_data = application_form.get_form_data_from_draft(draft)
    return competition.get_application_form_instance(initial=form_data)


def get_application_filled_form_from_application(candidate):
    competition = candidate.competition
    application_form = competition.get_application_form_instance(read_only=True)
    form_data = application_form.get_form_data_from_candidate(application_form, candidate)
    for elt, val in form_data.items(): print('>>', elt, val)
    return competition.get_application_form_instance(initial=form_data, read_only=True)


@login_required
def show_application_form(request, url):
    """a candidate's apply to a competition"""

    context = {}

    competition = get_object_or_404(Competition, url=url)

    # Automatic update of the status
    if competition.get_status() != competition.status:
        competition.save()

    composer = Composer.get_from_user(request.user)
    if not composer:
        raise Http404

    context["composer"] = composer
    context["competition"] = competition
    context["apply_allowed"] = competition.is_open()

    try:
        # The application has already been submitted
        candidate = Candidate.objects.get(composer=composer, competition=competition)
    except Candidate.DoesNotExist:
        candidate = None

    fields_labels = {
        'address1': _('Address (1)'),
        'address2': _('Address (2)'),
        'birth_date': _('Date of birth'),
        'citizenship': _('Citizenship'),
        'city': _('City'),
        'country': _('Country of residence'),
        'first_name': _('First name'),
        'gender': _('Pronoun'),
        'last_name': _('Last name'),
        'phone1': _('Phone (1)'),
        'phone2': _('Phone (2)'),
        'website': _('Website'),
        'zipcode': _('Zip code'),
    }

    application_warning = ""
    mandatory_fields = [_("Email address"), ] + [
        fields_labels.get(field.name, field.name) for field in competition.profile_mandatory_fields.all()
    ]

    fields_to_define = []
    profile = get_profile(request.user)
    if not profile:
        application_warning = _('You need a profile in order to apply to this competition')
    else:
        for field in competition.profile_mandatory_fields.all():
            if hasattr(profile, field.name) and not getattr(profile, field.name, ''):
                fields_to_define.append(field.name)

        if fields_to_define:
            fields_to_define_labels = [
                '<span class="mandatory-field">{0}</span>'.format(fields_labels.get(field_name, field_name))
                for field_name in fields_to_define
            ]
            application_warning = "{0}<br />{1}{2}".format(
                _('The organizer of the call requires more information.'),
                _('Please fill out the following information on your profile in order to apply to this competition: '),
                ", ".join(fields_to_define_labels)
            )

    if request.method == 'POST':  # If the form has been submitted...

        if candidate:
            # There is already an application for this candidate
            raise PermissionDenied

        elif "save_as_draft" in request.POST:
            save_application_draft(request.POST, request.user, competition)
            next_url = competition.get_absolute_url()
            return redirect(next_url)

        elif fields_to_define:
            form = competition.get_application_form_instance(data=request.POST)
            if form.is_valid():
                form.require_mandatory_fields = True

        elif "back_to_edition" in request.POST:
            form = competition.get_application_form_instance(data=request.POST)
            if form.is_valid():  # All validation rules pass
                pass

        elif "preview" in request.POST:
            form = competition.get_application_form_instance(data=request.POST)
            if form.is_valid():  # All validation rules pass
                form.preview = True

        elif "submit_application" in request.POST:
            form = competition.get_application_form_instance(data=request.POST)
            if competition.is_open():
                if form.is_valid() and not application_warning:  # All validation rules pass
                    # Create application
                    create_application(form, request.user, request.POST, competition)
                    info_message = _(
                        'Your application to {0} has been sent. You will receive a confirmation email shortly.'
                    ).format(
                        competition.title
                    )
                    messages.success(request, info_message)
                    next_url = reverse("profiles:personal_space") + '?tab=applications'
                    return redirect(next_url)
            else:
                error_message = _(
                    'Sorry, the competition {0} is now closed. Your application was not sent'
                ).format(
                    competition.title
                )
                messages.success(request, error_message)

        else:
            raise Http404

    else:
        # Delete the TemporaryDocument
        TemporaryDocument.objects.filter(
            competition=competition,
            composer=composer
        ).delete()

        if candidate:
            context["candidate"] = candidate
            try:
                form = get_application_filled_form_from_application(candidate)
            except Exception as err:
                form = None
                context['application_error'] = '{0}'.format(err)
        else:
            has_draft = False
            if 'reset' not in request.GET:
                try:
                    ApplicationDraft.objects.get(composer=composer, competition=competition)
                    has_draft = True
                except ApplicationDraft.DoesNotExist:
                    has_draft = False
            context["has_draft"] = has_draft
            if has_draft:
                try:
                    form = get_application_filled_form_from_draft(request, competition)
                except Exception as err:
                    form = competition.get_application_form_instance()
                    context['draft_error'] = '{0}'.format(err)
            else:
                # Initial form display :
                form = competition.get_application_form_instance()

    context["form"] = form
    context["application_warning"] = application_warning
    context["mandatory_fields"] = ", ".join(mandatory_fields)

    return render(request, 'web/composers/application.html', context)


@login_required
def show_medias(request):
    composer = Composer.get_from_user(request.user)
    context = {
        "medias": Media.objects.filter(composer=composer),
        "media_url": settings.MEDIA_URL,
    }
    return render(request, 'web/composers/media_list.html', context)


@login_required
def show_biographic_elements(request):
    composer = Composer.get_from_user(request.user)
    context = {
        "texts": BiographicElement.objects.filter(composer=composer)
    }
    return render(request, 'web/composers/biographic_element_list.html', context)


@login_required
def delete_application_draft(request, id):
    """delete an application draft"""
    composer = Composer.get_from_user(request.user)
    get_object_or_404(ApplicationDraft, composer=composer, id=id).delete()
    next_url = reverse("profiles:personal_space") + '?tab=calls'
    return redirect(next_url)


class DeletePortfolioElementBaseView(LoginRequiredPopupFormView):
    template_name = ''
    key = ''
    model = None
    form_class = ConfirmForm
    instance = None

    def get_instance(self):
        if not self.instance:
            composer = Composer.get_from_user(self.request.user)
            self.instance = get_object_or_404(self.model, composer=composer, id=self.kwargs['id'])
        return self.instance

    def get_context_data(self, **kwargs):
        context = super(DeletePortfolioElementBaseView, self).get_context_data(**kwargs)
        instance = self.get_instance()
        context[self.key] = instance
        return context

    def form_valid(self, form):
        if form.cleaned_data['confirm']:
            instance = self.get_instance()
            instance.delete()
        next_url = reverse("profiles:personal_space") + "?tab=portfolio"
        return HttpResponseRedirect(next_url)


class DeleteMediaView(DeletePortfolioElementBaseView):
    template_name = 'web/composers/delete_media.html'
    key = 'media'
    model = Media


class DeleteDocumentView(DeletePortfolioElementBaseView):
    template_name = 'web/composers/delete_document.html'
    key = 'document'
    model = Document


class DeleteBiographicElementView(DeletePortfolioElementBaseView):
    template_name = 'web/composers/delete_biographic_element.html'
    key = 'element'
    model = BiographicElement


@login_required
def show_documents(request):
    context = {
        "documents": Document.objects.filter(composer=Composer.get_from_user(request.user)),
        "media_url": settings.MEDIA_URL,
    }
    return render(request, 'web/composers/document_list.html', context)


# @ulysses_popup_redirect
# Having tinymce in colorbox cause a lot of problems. So we need to reload the page
# rather than using ajax
@popup_redirect
@login_required
def edit_biographic_element(request, id):
    context = {}
    composer = Composer.get_from_user(request.user)
    instance = get_object_or_404(BiographicElement, composer=composer, id=id)
    if request.method == 'POST':  # If the form has been submitted...
        form = BiographicElementForm(request.POST)
        if form.is_valid():  # All validation rules pass
            # Update text element
            instance.title = form.cleaned_data["title"]
            instance.text = form.cleaned_data["text"]
            instance.save()
            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        form = BiographicElementForm({'title': instance.title, 'text': instance.text})
    context["form"] = form
    context["element"] = instance
    context["title"] = _('Edit text')
    return render(request, 'web/composers/edit_biographic_element.html', context)


# @ulysses_popup_redirect
# see edit_biographic_element
@popup_redirect
@login_required
def add_biographic_element(request):
    context = {}
    if request.method == 'POST':  # If the form has been submitted...
        form = BiographicElementForm(request.POST)
        if form.is_valid():  # All validation rules pass
            # Create biographic element
            txt = BiographicElement()
            txt.composer = Composer.get_from_user(request.user)
            txt.title = form.cleaned_data["title"]
            txt.text = form.cleaned_data["text"]
            txt.save()
            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        form = BiographicElementForm()
    context["form"] = form
    context["title"] = _('Add text')
    return render(request, 'web/composers/edit_biographic_element.html', context)


@login_required
def copy_biographic_element(request):

    context = {
        "elements": get_application_history_elements(CandidateBiographicElement, request.user),
    }

    if request.method == 'POST':  # If the form has been submitted...
        if "elements" in request.POST:
            bio = CandidateBiographicElement.objects.get(id=request.POST["elements"])
            bio.copy_as_personal_element()
            return redirect(reverse("web:show_biographic_elements"))
        else:
            context["error_message"] = _("Please select an element")

    return render(request, 'web/composers/copy_biographic_element.html', context)


@login_required
def copy_document(request):

    context = {
        "elements": get_application_history_elements(CandidateDocument, request.user)
    }

    if request.method == 'POST':  # If the form has been submitted...
        if "elements" in request.POST:
            doc = CandidateDocument.objects.get(id=request.POST["elements"])
            doc.copy_as_personal_element()
            return redirect(reverse("web:show_documents"))
        else:
            context["error_message"] = _("Please select an element")

    return render(request, 'web/composers/copy_document.html', context)


@login_required
def copy_media(request):

    context = {
        "elements": get_application_history_elements(CandidateMedia, request.user),
    }

    if request.method == 'POST': # If the form has been submitted...
        if "elements" in request.POST:
            doc = CandidateMedia.objects.get(id=request.POST["elements"])
            doc.copy_as_personal_element()
            return redirect(reverse("web:show_medias"))
        else:
            context["error_message"] = _("Please select an element")

    return render(request, 'web/composers/copy_media.html', context)


@ulysses_popup_redirect
@login_required
def add_document(request):
    context = {}
    upload_url = reverse('web:upload_in_personal_space')
    if request.method == 'POST':  # If the form has been submitted...
        form = UploadDocumentForm(request.POST, upload_url=upload_url)
        if form.is_valid():  # All validation rules pass
            # Create document object
            doc = Document()
            doc.composer = Composer.get_from_user(request.user)
            doc.file = get_composer_file_url(request.user, form.cleaned_data["file"])
            doc.title = form.cleaned_data["title"]
            doc.save()

            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        form = UploadDocumentForm(upload_url=upload_url)

    context["form"] = form
    context["title"] = _("Upload document")

    return render(request, 'web/composers/edit_document.html', context)


@ulysses_popup_redirect
@login_required
def add_media(request):
    context = {}
    upload_url = reverse('web:upload_in_personal_space')
    if request.method == 'POST':  # If the form has been submitted...

        form = UploadMediaForm(request.POST, upload_url=upload_url)
        if form.is_valid():  # All validation rules pass
            # Create media object
            media = Media()
            media.composer = Composer.get_from_user(request.user)
            media.title = form.cleaned_data["title"]
            score = form.cleaned_data["score"]
            if score:
                media.score = get_composer_file_url(request.user, score)
            audio = form.cleaned_data["audio"]
            if audio:
                media.audio = get_composer_file_url(request.user, audio)
            video = form.cleaned_data["video"]
            if video:
                media.video = get_composer_file_url(request.user, video)
            media.link = form.cleaned_data["link"]
            media.notes = form.cleaned_data["notes"]
            media.save()
            next_url = reverse("profiles:personal_space") + "?tab=portfolio"
            return redirect(next_url)
    else:
        form = UploadMediaForm(upload_url=upload_url)
    context["form"] = form
    context["title"] = _("Upload a media")
    return render(request, 'web/composers/edit_media.html', context)


def get_application_history_elements(ElementClass, user):
    application_elements = ElementClass.objects.filter(
        candidate__composer=Composer.get_from_user(user), is_admin=False
    )
    application_history_elements = {}
    elements_key = 'application_elements'
    for item in application_elements:
        key = item.candidate.competition.url
        if key not in application_history_elements:
            application_history_elements[key] = {
                'competition': item.candidate.competition.title,
                'application_date': item.candidate.application_date,
                elements_key: []
            }
        application_history_elements[key][elements_key].append(item)
    return application_history_elements


def choose_element(
        request, competition_url, element_type, element_code, PersonalSpaceClass, ApplicationHistoryClass,
        is_file_element=False, allow_works=False
    ):

    context = {
        "element_type": element_type,
        "element_code": element_code,
        "competition": Competition.objects.get(url=competition_url),
        "personal_space_elements": PersonalSpaceClass.objects.filter(
            composer=Composer.get_from_user(request.user)
        ),
        "application_history_elements": get_application_history_elements(ApplicationHistoryClass,request.user),
        "is_file_element": is_file_element,
    }

    if allow_works:
        context['works'] = Work.objects.filter(owner=request.user)

    return render(request, 'web/composers/choose_element.html', context)


@login_required
def choose_biographic_element(request,url):
    return choose_element(request, url, "biographic element", "bio", BiographicElement, CandidateBiographicElement)


@login_required
def choose_document(request,url):
    return choose_element(request, url,"document", "doc", Document, CandidateDocument, True)


def choose_work(request,url):
    return choose_element(request, url, "work", "work", Media, CandidateMedia, True, allow_works=True)


class ConfirmApplicationActionView(LoginRequiredPopupFormView):
    template_name = 'web/popup_confirm_application_action.html'
    form_class = ConfirmForm
    content_object = None
    instance = None

    def get_form_kwargs(self):
        form_kwargs = super(ConfirmApplicationActionView, self).get_form_kwargs()
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(ConfirmApplicationActionView, self).get_context_data(**kwargs)
        action = self.kwargs['action']
        context['action'] = action
        context['next'] = self.request.GET.get('next')
        context['competition'] = get_object_or_404(Competition, id=self.kwargs['id'])
        if action == "delete_draft":
            context['title'] = _('This will delete your draft. Proceed ?')
        return context

    def form_valid(self, form):
        competition = get_object_or_404(Competition, id=self.kwargs['id'])
        if form.cleaned_data['confirm']:
            action = self.kwargs['action']
            if action == 'delete_draft':
                found = delete_draft(self.request.user, competition)
                if not found:
                    messages.error(self.request, _('Your application draft was not found.'))
                else:
                    messages.success(self.request, _('Your application draft has been deleted.'))
        next = self.request.GET.get('next')
        if next == 'dashboard':
            url = reverse('profiles:personal_space') + '?tab=applications'
            return HttpResponseRedirect(url)
        return HttpResponseRedirect(competition.get_absolute_url())
