# -*- coding: utf-8 -*-

from django.conf import settings
from django.http import HttpResponse

from ulysses.competitions.models import (
    BiographicElement, CandidateBiographicElement, CandidateDocument, CandidateMedia, Competition, Composer,
    Document, create_temporary_media
)
from ulysses.generic.views import LoginRequiredPopupFormView
from ulysses.works.models import Work

from ..forms import SelectDocumentForm, SelectMediaForm, SelectBiographyForm


def get_application_history_elements(element_class, user, media_type=None):
    application_elements = element_class.objects.filter(
        candidate__composer=Composer.get_from_user(user), is_admin=False
    )
    # Do not display media where required fields are missing
    if media_type in ('VideoField', 'ScoreVideoField'):
        application_elements = application_elements.exclude(video__isnull=True).exclude(video__exact='')
    if media_type in ('AudioField', 'ScoreAudioField'):
        application_elements = application_elements.exclude(audio__isnull=True).exclude(audio__exact='')
    if media_type in ('ScoreVideoField', 'ScoreAudioField'):
        application_elements = application_elements.exclude(score__isnull=True).exclude(score__exact='')

    application_history_elements = {}
    elements_key = 'elements'
    for item in application_elements:
        key = item.candidate.competition.url
        if key not in application_history_elements:
            application_history_elements[key] = {
                'competition': item.candidate.competition.title,
                'application_date': item.candidate.application_date,
                elements_key: []
            }
        application_history_elements[key][elements_key].append(item)
    return reversed(sorted(list(application_history_elements.values()), key=lambda elt: elt['application_date']))


class SelectElementFromPortfolioBaseView(LoginRequiredPopupFormView):

    def portfolio_queryset(self, composer):
        return self.element_class.objects.filter(composer=composer)

    def get_context_data(self, **kwargs):
        context = super(SelectElementFromPortfolioBaseView, self).get_context_data(**kwargs)
        context.update(self.kwargs)
        composer = Composer.get_from_user(self.request.user)
        media_type = self.kwargs.get('type', "MediaField")
        required_fields = {
            'AudioField': "an audio",
            'VideoField': "a video",
            'ScoreVideoField': "a score and a video",
            'ScoreAudioField': "a score and an audio",
        }.get(media_type, '')
        context.update({
            "media_type": media_type,
            "required_fields": required_fields,
            "portfolio_elements": self.portfolio_queryset(composer),
            "history_elements": get_application_history_elements(self.candidate_element_class, self.request.user, media_type),
        })
        return context

    def _get_media_file(self, file_field):
        if file_field:
            path = file_field.path
            return path.replace(settings.MEDIA_ROOT, '', 1)
        return ''

    def copy_as_temporary_element(self, elt, composer, competition, key):
        if isinstance(elt, Work):
            # Works
            media_type = self.kwargs.get('type', "MediaField")
            score = self._get_media_file(elt.score)
            audio = self._get_media_file(elt.audio)
            video = self._get_media_file(elt.video)
            # Just keep the required fields
            if media_type == 'VideoField':
                audio = None
                score = None
            if media_type == 'AudioField':
                video = None
                score = None
            if media_type == 'ScoreVideoField':
                audio = None
            if media_type == 'ScoreAudioField':
                video = None

            return create_temporary_media(
                competition,
                composer,
                key,
                elt.title,
                score,
                audio,
                video,
                elt.notes,
                ''  # no links
            )
        else:
            return elt.copy_as_temporary_element(composer, competition, key)

    def get_script(self, composer, competition, key, origin_element):
        element = self.copy_as_temporary_element(origin_element, composer, competition, key)
        js_script = "<script>$.colorbox.close(); $('#id_{0}').html('{1}'); $('#id_{0}_hidden').val('{2}');</script>"
        js_script = js_script.format(
            key, element.as_html, element.id
        )
        return js_script

    def form_valid(self, form):
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])
        origin_element = form.cleaned_data['element']
        # Close popup and update parent
        js_script = self.get_script(composer, competition, key, origin_element)
        return HttpResponse(js_script)


class SelectDocumentFromPortfolioView(SelectElementFromPortfolioBaseView):
    form_class = SelectDocumentForm
    element_class = Document
    candidate_element_class = CandidateDocument
    template_name = 'web/popup_select_document_form_portfolio.html'


class SelectMediaFromPortfolioView(SelectElementFromPortfolioBaseView):
    form_class = SelectMediaForm
    element_class = Work
    candidate_element_class = CandidateMedia
    template_name = 'web/popup_select_media_form_portfolio.html'

    def portfolio_queryset(self, composer):
        media_type = self.kwargs.get('type', "MediaField")
        media_queryset = self.element_class.objects.filter(owner=composer.user)
        # Do not display media where required fields are missing
        if media_type in ('VideoField', 'ScoreVideoField'):
            media_queryset = media_queryset.exclude(video='')
        if media_type in ('AudioField', 'ScoreAudioField'):
            media_queryset = media_queryset.exclude(audio='')
        if media_type in ('ScoreVideoField', 'ScoreAudioField'):
            media_queryset = media_queryset.exclude(score='')
        return media_queryset


class SelectBiographyFromPortfolioView(SelectElementFromPortfolioBaseView):
    form_class = SelectBiographyForm
    element_class = BiographicElement
    candidate_element_class = CandidateBiographicElement
    template_name = 'web/popup_select_biography_form_portfolio.html'

    def get_script(self, composer, competition, key, origin_element):
        js_script = "<script>$.colorbox.close(); tinyMCE.getInstanceById('id_{0}').setContent('{1}');</script>"
        js_script = js_script.format(
            key, origin_element.as_html
        )
        return js_script
