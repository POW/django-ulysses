# -*- coding: utf-8 -*-

from datetime import datetime

from django.shortcuts import render

from ulysses.competitions.models import Competition, Composer
from ulysses.competitions.utils import get_visible_competitions
from ulysses.composers.models import is_composer

from ..models import AnnounceCallImage


def show_competition(request, url):
    params = {}
    results = Competition.objects.filter(url=url)
    if results:
        if len(results) == 1:

            competition = Competition.objects.get(url=url)

            # Automatic update of the status
            if competition.get_status() != competition.status:
                competition.save()

            # Check that the competition can be viewed by the current user
            # status : "published", "open", "jury"
            # status draft if user is super-user or manager

            # Anyone should be able to access a draft competition
            # The competition is not listed in open competitions but anyone with the link shoud be able to access
            if get_visible_competitions(request.user, allow_draft=True).filter(id=competition.id).exists():
                params["competition"] = competition

                if is_composer(request.user):
                    composer = Composer.get_from_user(request.user)
                    # Get candidate application status for this competition / this composer
                    params["candidate"] = competition.get_composer_application_candidate(composer)
                    params["application_status"] = competition.get_composer_application_status(composer)
                    params["application_date"] = competition.get_composer_application_date(composer)
                return render(request, 'web/competition.html', params)

    # In other cases : show competition not found
    return render(request, 'web/competition_not_found.html', params)


def show_competitions(request):
    """list of calls"""
    competitions = get_visible_competitions(request.user)
    # Automatic update of the status
    for competition in competitions:
        if competition.get_status() != competition.status:
            competition.save()
    open_competitions = competitions.filter(
        closing_date__gte=datetime.now()
    ).distinct().order_by("closing_date")
    closed_competitions = competitions.filter(
        closing_date__lt=datetime.now()
    ).distinct().order_by('-closing_date')
    context = {
        "open_competitions": open_competitions,
        "closed_competitions": closed_competitions,
    }
    try:
        context['announce_call_image'] = AnnounceCallImage.objects.get(is_active=True)
    except (AnnounceCallImage.DoesNotExist, AnnounceCallImage.MultipleObjectsReturned):
        context['announce_call_image'] = None
    return render(request, 'web/competitions.html', context)