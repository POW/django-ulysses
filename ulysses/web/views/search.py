# -*- coding: utf-8 -*-

import json

from django.conf import settings
from django.core.paginator import InvalidPage, Paginator
from django.http import HttpResponse, Http404
from django.utils.safestring import mark_safe

from haystack.views import SearchView as HaystackSearchView
from haystack.query import SearchQuerySet

from ..forms import SearchForm


class SearchView(HaystackSearchView):
    template = 'search/search.html'

    def __init__(self):
        # , template=None, load_all=True, form_class=None, searchqueryset=None, results_per_page=None
        super(SearchView, self).__init__(form_class=SearchForm, results_per_page=25)

    def extra_context(self):
        return {
            # keep pagination control happy
            'get_querystr': '&q={0}'.format(self.query),
        }

    def build_page(self):
        """
        Paginates the results appropriately.

        In case someone does not want to use Django's built-in pagination, it
        should be a simple matter to override this method to do what they would
        like.
        """
        try:
            page_no = int(self.request.GET.get('page', 1))
        except (TypeError, ValueError):
            raise Http404("Not a valid number for page.")

        if page_no < 1:
            raise Http404("Pages should be 1 or greater.")

        start_offset = (page_no - 1) * self.results_per_page
        self.results[0:start_offset + self.results_per_page]

        paginator = Paginator(self.results, self.results_per_page)

        try:
            page = paginator.page(page_no)
        except InvalidPage:
            pass

        return (paginator, page)


def autocomplete_search(request):
    query_val = request.GET.get('q', '')

    def html_label(result):
        return mark_safe('<a href="{0}"><img src="{2}icons/search-autocomplete.svg" />{1}</a>'.format(
            result.object.get_absolute_url(),
            result.object.name,
            settings.STATIC_URL
        ))

    if query_val:
        search_queryset = SearchQuerySet().autocomplete(content_auto=query_val)[:5]
        suggestions = [
            {
                'label': html_label(result),
                'value': result.object.name
            }
            for result in search_queryset
            if result.object
        ]
    else:
        suggestions = []
    data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(data, content_type='application/json')
