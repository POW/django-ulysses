#!/usr/bin/env python

import json

from django.core.exceptions import ObjectDoesNotExist, PermissionDenied
from django.core.serializers import serialize
from django.db.models.query import QuerySet
from django.http import HttpResponse, Http404

from ulysses.composers.models import BiographicElement, Document, Media, Composer
from ulysses.competitions.models import (
    CandidateBiographicElement, CandidateDocument, CandidateMedia, TemporaryMedia, TemporaryDocument, Competition
)

from ulysses.works.models import Work
from ulysses.works.utils.media import copy_work_as_temporary_element


class JsonResponse(HttpResponse):
    def __init__(self, object):
        if isinstance(object, QuerySet):
            content = serialize('json', object)
        else:
            content = json.dumps(object)
        super(JsonResponse, self).__init__(content, content_type='application/json')


def get_biographic_element(request):
    id = request.GET['id']
    return JsonResponse(BiographicElement.objects.get(id=id).get_html())


def get_candidate_biographic_element(request):
    id = request.GET['id']
    return JsonResponse(CandidateBiographicElement.objects.get(id=id).get_html())


def get_media(request):
    id = request.GET['id']
    return JsonResponse(Media.objects.get(id=id).get_html())


def get_application_field_value(request):
    obj_id = request.GET.get('id', None)
    model = request.GET.get('model', None)
    if not obj_id or not model:
        raise Http404
    try:
        val = eval(model).objects.get(id=obj_id).get_html()
        return JsonResponse(val)
    except:
        raise Http404


def import_element_helper(element_class, request):
    elt_id = request.GET['id']
    key = request.GET['key']
    composer = Composer.get_from_user(request.user)
    competition_id = request.GET['competition']
    competition = Competition.objects.get(id=competition_id)

    try:

        if element_class == Work:

            work = Work.objects.get(id=elt_id)
            if work.owner != composer.user:
                raise PermissionDenied

            element = copy_work_as_temporary_element(work, competition, key)
            if not element:
                raise PermissionDenied
        else:
            # Load selected element & copy it as temporary element
            selected_element = element_class.objects.get(id=elt_id)
            element = selected_element.copy_as_temporary_element(composer, competition, key)

    except ObjectDoesNotExist:
        raise Http404

    # Return temporary element id & html
    output = {
        "id": element.id,
        "html": element.get_html(),
    }

    return JsonResponse(output)


def import_document(request):
    return import_element_helper(Document, request)


def import_candidate_document(request):
    return import_element_helper(CandidateDocument, request)


def import_work(request):
    return import_element_helper(Media, request)


def import_ulysses_work(request):
    return import_element_helper(Work, request)


def import_candidate_work(request):
    return import_element_helper(CandidateMedia, request)
