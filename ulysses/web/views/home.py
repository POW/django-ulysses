# -*- coding: utf-8 -*-

from django.shortcuts import get_object_or_404
from django.urls import reverse
from django.views.generic import TemplateView, RedirectView

from ulysses.competitions.utils import get_homepage_competitions
from ulysses.generic.views import ListBasePageView
from ulysses.profiles.utils import (
    get_homepage_members, get_individuals_count, get_organizations_count, get_profile
)
from ulysses.social.utils import get_homepage_feeds

from ..models import FocusOn, HomepageImage, AnnounceCallImage


class HomepageView(TemplateView):
    """Homepage"""
    template_name = 'web/home.html'

    def get_context_data(self, **kwargs):
        """Add elements to template context"""
        context = super(HomepageView, self).get_context_data(**kwargs)

        # Number of members
        context['individuals_count'] = get_individuals_count()
        context['organizations_count'] = get_organizations_count()

        # Most active members
        session_key = 'homepage_members'
        if session_key in self.request.session:
            members = self.request.session[session_key]
        else:
            members = [
                get_profile(user)
                for user in get_homepage_members()
            ]
            self.request.session[session_key] = members
        context['members'] = members

        # open competitions
        competitions = get_homepage_competitions()
        # Automatic update of the status
        for competition in competitions:
            if competition.get_status() != competition.status:
                competition.save()
        context['competitions'] = competitions

        # Focus On
        context['focus_on_items'] = FocusOn.objects.filter(
            is_published=True, is_headline=True
        ).order_by('-datetime')[:4]

        # posts
        if self.request.user.is_authenticated:
            context['community_news'] = get_homepage_feeds()[:4]

        else:
            try:
                context['homepage_image'] = HomepageImage.objects.get(is_active=True)
            except (HomepageImage.DoesNotExist, HomepageImage.MultipleObjectsReturned):
                context['homepage_image'] = None

        try:
            context['announce_call_image'] = AnnounceCallImage.objects.get(is_active=True)
        except (AnnounceCallImage.DoesNotExist, AnnounceCallImage.MultipleObjectsReturned):
            context['announce_call_image'] = None

        return context


class FocusOnView(TemplateView):
    """Focus On"""
    template_name = 'web/focus_on.html'
    focus_on = None

    def get_focus_on(self):
        if not self.focus_on:
            self.focus_on = get_object_or_404(FocusOn, is_published=True, slug=self.kwargs['slug'])
        return self.focus_on

    def get_context_data(self, **kwargs):
        """Add elements to template context"""
        context = super(FocusOnView, self).get_context_data(**kwargs)

        # Focus On
        context['focus_on'] = self.get_focus_on()

        return context


class FocusOnHeadlineView(RedirectView):
    """Focus On headline view"""

    def get_redirect_url(self, *args, **kwargs):
        """return the latest headline"""
        items = FocusOn.objects.filter(is_published=True, is_headline=True).order_by('-datetime')
        if items.count() > 0:
            return items[0].get_absolute_url()
        else:
            return reverse('web:focus_on_list')


class FocusOnListView(ListBasePageView):
    """Displays the list of focus on"""
    template_name = 'web/focus_on_list.html'

    ITEMS_COUNTER_STEP = 6
    ROWS_LG_LIMIT = 0  # By default only 4 lines on small screens
    ORDER_BY_CHOICES = []

    def get_ordering(self, order_by):
        """returns the ordering fields"""
        ordering = ['-datetime']
        return ordering

    def get_queryset(self):
        return FocusOn.objects.filter(is_published=True)

    def get_filter_form_class(self):
        return None
