# -*- coding: utf-8 -*-

import os

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse, Http404
from django.urls import reverse
from django.views.decorators.csrf import csrf_exempt

from ulysses.competitions.models import (
    create_temporary_media, Competition, Composer, TemporaryDocument, ApplicationFormFieldDefinition,
    copy_file_to_user_temporary_folder, TemporaryDocumentExtraFile
)
from ulysses.generic.forms import ConfirmForm
from ulysses.generic.views import LoginRequiredPopupFormView
from ulysses.utils import upload_helper

from ..forms import UploadDocumentForm, UploadFileForm, UploadMediaForm


def get_temporary_dir(user):
    """Get the path where to store file """

    temp_directory = os.path.split(settings.TEMPORARY_FILES_ROOT)[1]
    user_directory = '{0}'.format(user.username)

    folders = (temp_directory, user_directory)
    full_path = settings.MEDIA_ROOT
    if not os.path.exists(full_path):
        os.mkdir(full_path)

    for directory in folders:
        full_path = os.path.join(full_path, directory)
        # Create folder, if necessary
        if not os.path.exists(full_path):
            os.mkdir(full_path)

    return os.path.sep.join(folders)


def get_temporary_file_url(user, filename):
    return os.path.join(
        get_temporary_dir(user), filename
    )


@csrf_exempt
@login_required
def upload_document_file(request):
    target_dir = get_temporary_dir(request.user)
    absolute_target_dir = os.path.join(settings.MEDIA_ROOT, target_dir)
    return upload_helper(request, absolute_target_dir)


class UploadDocumentView(LoginRequiredPopupFormView):
    template_name = 'web/popup_upload_document.html'
    form_class = UploadDocumentForm

    def get_form_kwargs(self):
        kwargs = super(UploadDocumentView, self).get_form_kwargs()
        kwargs['upload_url'] = reverse('web:upload_document_file')
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])
        try:
            doc = TemporaryDocument.objects.get(competition=competition, composer=composer, key=key)
            kwargs['initial'] = {'title': doc.title}
        except TemporaryDocument.DoesNotExist:
            pass

        return kwargs

    def get_context_data(self, **kwargs):
        context = super(UploadDocumentView, self).get_context_data(**kwargs)
        context.update(self.kwargs)

        competition = Competition.objects.get(url=self.kwargs['url'])
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)

        # get Field
        try:
            application_form = competition.dynamic_application_form
            field = ApplicationFormFieldDefinition.objects.get(parent__parent=application_form, name=key)
        except ApplicationFormFieldDefinition.DoesNotExist:
            raise Http404

        try:
            doc = TemporaryDocument.objects.get(competition=competition, composer=composer, key=key)
            nb_files = doc.get_extra_files().count() + 1
        except TemporaryDocument.DoesNotExist:
            nb_files = 0

        if field.extra_files and nb_files >= field.extra_files:
            context['no_more_uploads'] = True

        return context

    def form_valid(self, form):
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])
        title = form.cleaned_data["title"]
        file = get_temporary_file_url(self.request.user, form.cleaned_data["file"])

        # get Field
        try:
            application_form = competition.dynamic_application_form
            field = ApplicationFormFieldDefinition.objects.get(parent__parent=application_form, name=key)
        except ApplicationFormFieldDefinition.DoesNotExist:
            raise Http404

        try:
            doc = TemporaryDocument.objects.get(competition=competition, composer=composer, key=key)
        except TemporaryDocument.DoesNotExist:
            # Create new Temporary document
            doc = TemporaryDocument(competition=competition, composer=composer, key=key)

        doc.title = title
        doc.file = copy_file_to_user_temporary_folder(composer, file)
        doc.save()

        if field.extra_files:
            html = doc.as_edit_html()
        else:
            html = doc.as_html

        # Close popup and update parent
        js_script = """
        <script>
        $.colorbox.close(); 
        $('#id_{0}').html('{1}');
        $('#id_{0}_hidden').val('{2}');
        $('#{0}_add_file').removeClass('disabled');
        </script>
        """
        js_script = js_script.format(
            key, html.replace("'", "\\'"), doc.id
        )
        return HttpResponse(js_script.strip())


class RemoveDocumentView(LoginRequiredPopupFormView):
    template_name = 'web/popup_remove_document.html'
    form_class = ConfirmForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)
        return context

    def form_valid(self, form):
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])

        try:
            doc = TemporaryDocument.objects.get(composer=composer, competition=competition, key=key)
            doc.delete()
        except TemporaryDocument.DoesNotExist:
            pass

        # Close popup and update parent
        js_script = """
        <script>
        $.colorbox.close();
        $('#id_{0}').html('');
        $('#id_{0}_hidden').val('');
        $('#{0}_add_file').addClass('disabled');
        </script>
        """
        js_script = js_script.format(key)
        return HttpResponse(js_script.strip())


class AddTemporaryDocumentFileView(LoginRequiredPopupFormView):
    template_name = 'web/popup_upload_document_file.html'
    form_class = UploadFileForm
    extra_file_model = TemporaryDocumentExtraFile

    def get_action_url(self):
        return reverse('web:add_temporary_document_file', kwargs=self.kwargs)

    def get_upload_url(self):
        return reverse('web:upload_document_file')

    def copy_file(self, document, file):
        return copy_file_to_user_temporary_folder(document.composer, file)

    def get_document(self):
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])
        try:
            return TemporaryDocument.objects.get(competition=competition, composer=composer, key=key)
        except TemporaryDocument.DoesNotExist:
            raise Http404

    def get_field(self):
        competition = Competition.objects.get(url=self.kwargs['url'])
        key = self.kwargs['key']
        try:
            application_form = competition.dynamic_application_form
            return ApplicationFormFieldDefinition.objects.get(parent__parent=application_form, name=key)
        except ApplicationFormFieldDefinition.DoesNotExist:
            raise Http404

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['upload_url'] = self.get_upload_url()
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(self.kwargs)
        document = self.get_document()
        field = self.get_field()
        nb_files = document.get_extra_files().count()
        if nb_files >= field.extra_files:
            context['no_more_uploads'] = True
        context['action_url'] = self.get_action_url()
        return context

    def form_valid(self, form):
        file = get_temporary_file_url(self.request.user, form.cleaned_data["file"])
        document = self.get_document()

        # recreate extra files
        extra_file = self.extra_file_model(document=document)
        extra_file.file = self.copy_file(document, file)
        extra_file.save()

        html = document.as_edit_html()

        # Close popup and update parent
        js_script = "<script>$.colorbox.close(); $('#id_{0}').html('{1}'); $('#id_{0}_hidden').val('{2}');</script>"
        js_script = js_script.format(
            document.key, html.replace("'", "\\'"), document.id
        )
        return HttpResponse(js_script)


class RemoveTemporaryDocumentFileView(LoginRequiredPopupFormView):
    template_name = 'web/popup_remove_document_file.html'
    form_class = ConfirmForm

    def get_action_url(self):
        return reverse('web:remove_temporary_document_file', kwargs=self.kwargs)

    def get_document(self):
        composer = Composer.get_from_user(self.request.user)
        doc_id = self.kwargs['doc_id']
        try:
            return TemporaryDocument.objects.get(composer=composer, id=doc_id)
        except TemporaryDocument.DoesNotExist:
            raise Http404

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['action_url'] = self.get_action_url()
        return context

    def form_valid(self, form):
        document = self.get_document()
        extra_file_id = self.kwargs['extra_file_id']

        try:
            extra_file = document.get_extra_files().get(id=extra_file_id)
        except ObjectDoesNotExist:
            raise Http404

        extra_file.delete()

        html = document.as_edit_html()

        # Close popup and update parent
        js_script = "<script>$.colorbox.close(); $('#id_{0}').html('{1}'); $('#id_{0}_hidden').val('{2}');</script>"
        js_script = js_script.format(
            document.key, html.replace("'", "\\'"), document.id
        )
        return HttpResponse(js_script)


class UploadMediaView(LoginRequiredPopupFormView):
    template_name = 'web/popup_upload_media.html'
    form_class = UploadMediaForm

    def get_context_data(self, **kwargs):
        context = super(UploadMediaView, self).get_context_data(**kwargs)
        context.update(self.kwargs)
        context['disable_double_click'] = True
        return context

    def get_form_kwargs(self):
        ret = super().get_form_kwargs() or {}
        field_type = self.kwargs.get('type', '')
        media_fields = [
            "MediaField", "VideoField", "AudioField", "ScoreVideoField", "ScoreAudioField", "YouTubeField",
            "VideoYouTubeField",
        ]

        if field_type not in media_fields:
            raise Http404
        ret.update({'field_type': field_type})
        return ret

    def form_valid(self, form):
        key = self.kwargs['key']
        composer = Composer.get_from_user(self.request.user)
        competition = Competition.objects.get(url=self.kwargs['url'])
        title = form.cleaned_data["title"]
        score = form.cleaned_data["score"]
        if score:
            score = get_temporary_file_url(self.request.user, score)

        audio = form.cleaned_data["audio"]
        if audio:
            audio = get_temporary_file_url(self.request.user, audio)

        video = form.cleaned_data["video"]
        if video:
            video = get_temporary_file_url(self.request.user, video)

        notes = form.cleaned_data["notes"]
        link = form.cleaned_data["link"]
        media = create_temporary_media(competition, composer, key, title, score, audio, video, notes, link)

        # Close popup and update parent

        js_script = "<script>$.colorbox.close(); $('#id_{0}').html('{1}'); $('#id_{0}_hidden').val('{2}');</script>"
        js_script = js_script.format(
            key, media.as_html.replace("'", "\\'"), media.id
        )
        return HttpResponse(js_script)
