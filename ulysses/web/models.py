# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
from datetime import datetime

from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.translation import ugettext_lazy as _

from sorl.thumbnail import get_thumbnail

from ulysses.competitions.models import Competition
from ulysses.events.models import Event
from ulysses.profiles.utils import get_active_members_queryset
from ulysses.utils import dehtml
from ulysses.works.models import Work


class FocusOn(models.Model):
    """an article"""
    title = models.CharField(verbose_name=_("title"), max_length=200)
    slug = models.SlugField(max_length=200, verbose_name=_('slug'), db_index=True, unique=True)
    datetime = models.DateTimeField(verbose_name=_("datetime"), auto_created=True)
    text = models.TextField(verbose_name=_("text"), help_text=("html content"))
    image = models.ImageField(
        verbose_name=_("image"), blank=True, null=True, default=None, upload_to="focus_on", max_length=200
    )
    is_published = models.BooleanField(
        default=False, verbose_name=_("is published"), help_text=_('visible on the site')
    )
    is_headline = models.BooleanField(
        default=False, verbose_name=_("is headline"), help_text=_('displayed on homepage')
    )
    summary = models.TextField(
        verbose_name=_("summary"), blank=True, default='',
        help_text=_("text displayed on homepage, filled with first words of text if blank")
    )
    competitions = models.ManyToManyField(Competition, blank=True, verbose_name=_('Competitions'))
    members = models.ManyToManyField(User, blank=True, verbose_name=_('Members'))
    events = models.ManyToManyField(Event, blank=True, verbose_name=_('Events'))
    works = models.ManyToManyField(Work, blank=True, verbose_name=_('Works'))

    class Meta:
        verbose_name = _("focus on")
        verbose_name_plural = _("focus on")
        ordering = ['-datetime', '-id']

    def __str__(self):
        return self.title

    def date(self):
        return self.datetime.date()

    def get_image(self):
        if self.image:
            return get_thumbnail(self.image, "400").url

    def get_square_medium_image(self):
        """image on dashboard"""
        if self.image:
            return get_thumbnail(self.image, "400x400", crop="center").url

    def get_thumbnail(self):
        if self.image:
            return get_thumbnail(self.image, "200").url

    def save(self, *args, **kwargs):
        if not self.summary:
            self.summary = self.get_summary()
        return super(FocusOn, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse('web:focus_on', args=[self.slug])

    def get_members(self):
        return get_active_members_queryset(queryset=self.members.all())

    @property
    def raw_text(self):
        return dehtml(self.text)

    @property
    def name(self):
        return self.title

    def get_summary(self):
        max_words = 50
        words_in_summary = 0
        summary_text = ''

        soup = BeautifulSoup(self.text, 'html.parser')
        for child in soup.children:
            text = getattr(child, 'text', '{0}'.format(child))
            words = text.split(' ')
            words_to_go = max(0, max_words - words_in_summary)
            if words_to_go:
                if words_to_go < len(words):
                    value = ' '.join(words[0:words_to_go]) + '...'
                else:
                    value = ' '.join(words)
                summary_text += '<{0}>{1}</{0}>'.format(getattr(child, 'name', 'p'), value)
                words_in_summary += len(words)
            else:
                break
        return summary_text


class EditableText(models.Model):

    key = models.CharField(max_length=100, db_index=True, verbose_name=_('Key'))
    text = models.TextField(blank=True, default='', verbose_name=_('Text'))
    about = models.CharField(max_length=200, verbose_name=_('About'), blank=True, default="")
    active = models.BooleanField(default=True, verbose_name=_('Active'), help_text=_('shown if active'))

    class Meta:
        verbose_name = _("Editable text")
        verbose_name_plural = _("Editable texts")
        ordering = ['key']

    def __str__(self):
        return self.key


class FocusOnImage(models.Model):

    image = models.ImageField(upload_to="focus_on")

    class Meta:
        verbose_name = _("FocusOn image")
        verbose_name_plural = _("FocusOn images")

    def get_absolute_url(self):
        return get_thumbnail(self.image, "600").url

    @property
    def name(self):
        return self.image.name

    def __str__(self):
        return self.name


class HomepageBaseImage(models.Model):
    image = models.ImageField(upload_to="homepage")
    is_active = models.BooleanField(default=False)

    class Meta:
        abstract = True

    def get_absolute_url(self):
        return get_thumbnail(self.image, "1200").url

    @property
    def name(self):
        return self.image.name

    def get_siblings(self):
        raise NotImplemented

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        ret = super(HomepageBaseImage, self).save(*args, **kwargs)
        if self.id and self.is_active:
            # Only 1 active image
            self.get_siblings().exclude(id=self.id).update(is_active=False)
        return ret


class HomepageImage(HomepageBaseImage):

    class Meta:
        verbose_name = _("Homepage image")
        verbose_name_plural = _("Homepage images")

    def get_siblings(self):
        return HomepageImage.objects.all()


class AnnounceCallImage(HomepageBaseImage):

    class Meta:
        verbose_name = _("AnnounceCall image")
        verbose_name_plural = _("AnnounceCall images")

    def get_siblings(self):
        return AnnounceCallImage.objects.all()


class NewsletterSubscription(models.Model):
    email = models.EmailField()
    date = models.DateTimeField(default=datetime.now)
    at_registration = models.BooleanField(default=False)
    message_business = models.BooleanField(default=False)
    unsubscribe = models.BooleanField(
        default=False,
        help_text=_('If checked, the user has requested to unregister from his preferences')
    )
    deleted_account = models.BooleanField(default=False)

    class Meta:
        verbose_name = _("Newsletter subscription")
        verbose_name_plural = _("Newsletter subscriptions")

    def __str__(self):
        return self.email
