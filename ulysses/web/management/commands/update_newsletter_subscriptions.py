
from os.path import abspath, dirname, join
from django.core.management.base import BaseCommand, CommandError
from django.contrib.auth.models import User

from ulysses.profiles.utils import get_profile

class Command(BaseCommand):
    """
    Import email subscription from message-businness
    """
    help = ""
    
    def handle(self, *args, **options):
        app_dir = dirname(dirname(dirname(abspath(__file__))))
        mb_filename = join(app_dir, 'fixtures', 'message-business.txt')
        counter = 0
        with open(mb_filename, 'r') as mb_file:
            for index, line in enumerate(mb_file.readlines()):
                if index == 0:
                    continue
                fields = line.split('\t')
                email = fields[3]
                if email:
                    users = User.objects.filter(email=email)
                    for user in users:
                        profile = get_profile(user)
                        if profile and not profile.ulysses_newsletter:
                            profile.ulysses_newsletter = True
                            profile.save()
                            counter += 1
        print(counter, "emails have been subscribe to the newsletter")
