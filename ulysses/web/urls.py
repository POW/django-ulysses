# -*- coding: utf-8 -*-

from django.conf.urls import url
from django.urls import reverse_lazy
from django.views.generic import RedirectView

from .views.ajax import (
    import_work, import_ulysses_work, import_candidate_work, get_application_field_value, get_biographic_element,
    get_candidate_biographic_element, import_document, import_candidate_document
)
from .views.applications import (
    show_medias, show_documents, show_biographic_elements, edit_document, edit_media,
    add_biographic_element, add_document, edit_biographic_element,
    add_media, delete_application_draft, show_application_form, upload_in_personal_space,
    upload_as_temporary_file, show_submitted_application, show_application_asynchronous_submission,
    show_application_submission_error, choose_biographic_element, choose_document, choose_work,
    copy_biographic_element, copy_document, copy_media, ConfirmApplicationActionView, DeleteDocumentView,
    DeleteBiographicElementView, DeleteMediaView
)
from .views.calls import show_competition, show_competitions
from .views.misc import (
    show_about, show_legal_mentions, show_contact, show_privacy_policy, hide_accept_cookies_message,
    NewsletterSubscriptionView, close_info_box, NewsletterSubscriptionExcelView, show_help, YoutubePopupView
)
from .views.home import HomepageView, FocusOnView, FocusOnListView, FocusOnHeadlineView
from .views.portfolio import (
    SelectDocumentFromPortfolioView, SelectMediaFromPortfolioView, SelectBiographyFromPortfolioView
)
from .views.search import SearchView, autocomplete_search
from .views.uploads import (
    UploadDocumentView, upload_document_file, UploadMediaView, RemoveTemporaryDocumentFileView, RemoveDocumentView,
    AddTemporaryDocumentFileView
)


app_name = "ulysses-web"


urlpatterns = [

    # Main pages
    url(r'^$', HomepageView.as_view(), name="home"),
    url(r'^home/$', RedirectView.as_view(url='/', permanent=False), name="home_web"),

    url(r'^search/$', SearchView(), name="search"),
    url(r'^autocomplete-search/$', autocomplete_search, name="autocomplete_search"),

    url(r'^about/$', show_about, name="about"),
    url(r'^help/$', show_help, name="help"),
    url(r'^legal/$',  show_legal_mentions, name="legal_mentions"),
    url(r'^legal-mentions/$',  RedirectView.as_view(url=reverse_lazy('web:legal_mentions'), permanent=False)),
    url(r'^privacy-policy/$',  show_privacy_policy, name="privacy_policy"),
    url(r'^contact-us/$', show_contact, name="contact_us"),

    # File uploads (via uploadify)
    url(r'^upload_in_personal_space/$', upload_in_personal_space, name='upload_in_personal_space'),
    url(r'^upload_as_temporary_file/$', upload_as_temporary_file, name='upload_as_temporary_file'),

    # Composer space
    url(
        r'^composers/biographic-elements/delete/(?P<id>.+)/$',
        DeleteBiographicElementView.as_view(),
        name="delete_biographic_element")
    ,
    url(
        r'^composers/biographic-elements/add/$',
        add_biographic_element,
        name="add_biographic_element"
    ),
    url(
        r'^composers/biographic-elements/copy/$',
        copy_biographic_element,
        name="copy_biographic_element"
    ),
    url(
        r'^composers/biographic-elements/(?P<id>.*)/$',
        edit_biographic_element,
        name="edit_biographic_element"
    ),
    url(r'^composers/biographic-elements/$', show_biographic_elements, name="show_biographic_elements"),
    url(r'^composers/documents/delete/(?P<id>.*)/$', DeleteDocumentView.as_view(), name="delete_document"),
    url(r'^composers/documents/add/$', add_document, name="add_document"),
    url(r'^composers/documents/copy/$', copy_document, name="copy_document"),
    url(r'^composers/documents/(?P<id>.*)/$', edit_document, name="edit_document"),
    url(r'^composers/documents/$', show_documents, name="show_documents"),
    url(r'^composers/medias/delete/(?P<id>.*)/$', DeleteMediaView.as_view(), name="delete_media"),
    url(r'^composers/medias/add/$', add_media, name="add_media"),
    url(r'^composers/medias/copy/$', copy_media, name="copy_media"),
    url(r'^composers/medias/(?P<id>.*)/$', edit_media, name="edit_media"),
    url(r'^composers/medias/$', show_medias, name="show_medias"),
    url(
        r'^composers/applications/drafts/delete/(?P<id>.+)/$',
        delete_application_draft,
        name="delete_application_draft"
    ),
    url(
        r'^composers/applications/(?P<id>.+)/$',
        show_submitted_application,
        name="show_submitted_application"
    ),
    url(
        r'^confirm-application-action/(?P<id>\d+)/(?P<action>[\w_]+)/$',
        ConfirmApplicationActionView.as_view(),
        name="confirm_application_action"
    ),

    # Ajax urls (for application form)
    url(r'^ajax/get_biographic_element/$', get_biographic_element),
    url(r'^ajax/get_candidate_biographic_element/$', get_candidate_biographic_element),
    url(r'^ajax/import_document/$', import_document),
    url(r'^ajax/import_candidate_document/$', import_candidate_document),
    url(r'^ajax/import_ulysses_work/$', import_ulysses_work),
    url(r'^ajax/import_work/$', import_work),
    url(r'^ajax/import_candidate_work/$', import_candidate_work),
    url(r'^ajax/get_application_field_value/$', get_application_field_value),

    # Competitions
    url(
        r'^competitions/(?P<url>.+)/application_submission_error/$',
        show_application_submission_error,
        name="show_application_submission_error"
    ),
    url(
        r'^competitions/(?P<url>.+)/application_asynchronous_submission/$',
        show_application_asynchronous_submission,
        name="show_application_asynchronous_submission"
    ),
    url(r'^competitions/(?P<url>.+)/application/$', show_application_form, name="show_application_form"),
    url(
        r'^competitions/(?P<url>.+)/choose_biographic_element/$',
        choose_biographic_element,
        name="choose_biographic_element"
    ),
    url(r'^competitions/(?P<url>.+)/choose_document/$', choose_document, name="choose_document"),
    url(r'^competitions/(?P<url>.+)/choose_work/$', choose_work, name="choose_work"),
    url(r'^competitions/(?P<url>.+)/$', show_competition, name="competition"),

    url(r'^competitions/$', show_competitions, name="competitions"),

    url(r'^focus-on/$', FocusOnListView.as_view(), name="focus_on_list"),
    url(r'^focus-on/headline/$', FocusOnHeadlineView.as_view(), name="focus_on_headline"),
    url(r'^focus-on/(?P<slug>.+)/$', FocusOnView.as_view(), name="focus_on"),


    url(r'^uploads/upload-document/(?P<url>.+)/(?P<key>.+)/$', UploadDocumentView.as_view(), name="upload_document"),
    url(r'^remove_document/(?P<url>.+)/(?P<key>.+)/$', RemoveDocumentView.as_view(), name="remove_document"),
    url(
        r'^add-temporary-document-file/(?P<url>.+)/(?P<key>.+)/$',
        AddTemporaryDocumentFileView.as_view(),
        name="add_temporary_document_file"
    ),
    url(
        r'^remove-temporary-document-file/(?P<doc_id>\d+)/(?P<extra_file_id>\d+)/$',
        RemoveTemporaryDocumentFileView.as_view(),
        name="remove_temporary_document_file"
    ),

    url(
        r'^uploads/upload-media/(?P<url>.+)/(?P<key>.+)/(?P<type>.+)/$',
        UploadMediaView.as_view(),
        name="upload_media"
    ),
    url(r'^uploads/upload-document-file/$', upload_document_file, name="upload_document_file"),
    url(
        r'^portfolio/select-document/(?P<url>.+)/(?P<key>.+)/$',
        SelectDocumentFromPortfolioView.as_view(),
        name="select_document_from_portfolio"
    ),
    url(
        r'^portfolio/select-media/(?P<url>.+)/(?P<key>.+)/(?P<type>.+)/$',
        SelectMediaFromPortfolioView.as_view(),
        name="select_media_from_portfolio"
    ),
    url(
        r'^portfolio/select-biography/(?P<url>.+)/(?P<key>.+)/$',
        SelectBiographyFromPortfolioView.as_view(),
        name="select_biography_from_portfolio"
    ),
    url(
        r'^hide-accept-cookies-message/$',
        hide_accept_cookies_message,
        name='hide_accept_cookies_message'
    ),
    url(
        r'^close-info-box/(?P<name>.+)/$',
        close_info_box,
        name='close_info_box'
    ),
    url(
        r'^subscribe-newsletter/$',
        NewsletterSubscriptionView.as_view(),
        name="subscribe_newsletter",
    ),
    url(
        r'^export-newsletter-subscriptions/$',
        NewsletterSubscriptionExcelView.as_view(),
        name="export_newsletter_subscriptions",
    ),
    url(r'^view-youtube/(?P<code>.+)/$', YoutubePopupView.as_view(), name='youtube_popup'),
]
