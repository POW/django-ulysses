# -*- coding: utf-8 -*-

from django.contrib import admin

from ulysses.super_admin.sites import super_admin_site

from .models import FocusOn, EditableText, FocusOnImage, HomepageImage, AnnounceCallImage, NewsletterSubscription


class FocusOnAdmin(admin.ModelAdmin):
    list_display = ('slug', 'title', 'datetime', 'is_published', 'is_headline', )
    list_filter = ('is_published', 'is_headline', )
    search_fields = ('text', )
    date_hierarchy = 'datetime'
    prepopulated_fields = {"slug": ("title", )}
    raw_id_fields = ('members', 'competitions', 'works', 'events', )
    fieldsets = (
        (
            'Content', {
                'classes': ('wide',),
                'fields': ('title', 'text', 'image', )
            }
        ),
        (
            'Publication', {
                'classes': ('wide',),
                'fields': ('slug', 'datetime', 'is_published',)
            }
        ),
        (
            'Homepage', {
                'classes': ('wide',),
                'fields': ('summary', 'is_headline', )
            }
        ),
        (
            'Associated', {
                'classes': ('wide',),
                'fields': ('members', 'competitions', 'works', 'events', )
            }
        ),

    )

    class Media:
        js = (
            'js/tiny_mce/tiny_mce.js',
            'js/admin_img_pages.js'
        )


class EditableTextAdmin(admin.ModelAdmin):
    list_display = ('key', 'about', 'active', )
    readonly_fields = ('key', 'about', )

    class Media:
        js = (
            'js/tiny_mce/tiny_mce.js',
            'js/admin_img_pages.js'
        )


class FocusOnImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'get_absolute_url')


class HomepageBaseImageAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', )
    list_filter = ('is_active', )


class NewsletterSubscriptionAdmin(admin.ModelAdmin):
    list_display = ('email', 'date', 'message_business', 'at_registration', 'unsubscribe', 'deleted_account')
    list_filter = ('unsubscribe', 'message_business', 'at_registration', 'deleted_account')
    date_hierarchy = 'date'
    search_fields = ('email', )


super_admin_site.register(FocusOn, FocusOnAdmin)
super_admin_site.register(EditableText, EditableTextAdmin)
super_admin_site.register(FocusOnImage, FocusOnImageAdmin)
super_admin_site.register(HomepageImage, HomepageBaseImageAdmin)
super_admin_site.register(AnnounceCallImage, HomepageBaseImageAdmin)
super_admin_site.register(NewsletterSubscription, NewsletterSubscriptionAdmin)
