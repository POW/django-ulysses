# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-03-17 13:54


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_web','0005_auto_20170227_0745'),
    ]

    operations = [
        migrations.AlterField(
            model_name='focuson',
            name='image',
            field=models.ImageField(blank=True, default=None, max_length=200, null=True, upload_to=b'focus_on', verbose_name='image'),
        ),
    ]
