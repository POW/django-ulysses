# -*- coding: utf-8 -*-

import os
import sys

from django.conf import settings
from django.contrib.auth.models import User
from django.urls import reverse
from django.db import models
from django.utils.translation import ugettext as _

from ulysses.reference.models import Citizenship, Country, Gender as ReferenceGender
from ulysses.profiles.models import Individual, Organization
from ulysses.profiles.utils import get_profile
from ulysses.utils import get_age, get_media_link


def is_composer(user):
    """
    Indicates whether the current user is a composer
    """
    return Composer.objects.filter(user__username=user.username).exists()


class Gender(models.Model):
    name = models.CharField(verbose_name=_("name"), max_length=200)

    def __str__(self):
        return self.name


class Composer(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    gender = models.ForeignKey(Gender, blank=True, null=True, default=None, on_delete=models.SET_NULL)
    reference_gender = models.ForeignKey(
        ReferenceGender, blank=True, null=True, default=None, on_delete=models.SET_NULL
    )
    birth_date = models.DateField(verbose_name=_("birth date"), blank=True, default=None, null=True)
    citizenship = models.ForeignKey(
        Citizenship, verbose_name=_("citizenship"), blank=True, default=None, null=True, on_delete=models.SET_NULL
    )
    address1 = models.CharField(verbose_name=_("address (1)"), max_length=200, blank=True, default='')
    address2 = models.CharField(verbose_name=_("address (2)"), max_length=200, blank=True, null=True)
    zipcode = models.CharField(verbose_name=_("zip code"), max_length=10, blank=True, default='')
    city = models.CharField(verbose_name=_("city"), max_length=50, blank=True, default='')
    country = models.ForeignKey(
        Country, verbose_name=_("country"), blank=True, default=None, null=True, on_delete=models.SET_NULL
    )
    phone1 = models.CharField(verbose_name=_("phone (1)"), max_length=100, blank=True, default='')
    phone2 = models.CharField(verbose_name=_("phone (2)"), max_length=100, blank=True, null=True)
    website = models.URLField(verbose_name=_("website"), blank=True, null=True)

    @classmethod
    def get_from_user(cls, user):
        """ Gets the composer associated to the specified user """
        try:
            return Composer.objects.get(user=user)
        except Composer.DoesNotExist:

            try:
                individual = Individual.objects.get(user=user)
            except Individual.DoesNotExist:
                individual = None

            if individual:
                composer = Composer.objects.create(
                    user=user,
                    birth_date=individual.birth_date,
                    citizenship=individual.citizenship,
                    city=individual.city,
                    country=individual.country
                )
                return composer

            try:
                organization = Organization.objects.get(user=user)
            except Organization.DoesNotExist:
                organization = None

            if organization:
                composer = Composer.objects.create(
                    user=user,
                    city=organization.city,
                    country=organization.country
                )
                return composer

    def age(self):
        if self.birth_date:
            return get_age(self.birth_date)
        else:
            return "#"

    @property
    def name(self):
        profile = get_profile(self.user)
        if profile:
            return profile.name_as_candidate
        return "{0}, {1}".format(self.user.last_name, self.user.first_name)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['user__last_name', ]


class MediaBase(models.Model):
    title = models.CharField(max_length=400)
    score = models.CharField(
        max_length=400, blank=True, null=True, help_text="score file url, relative to application's MEDIA_URL"
    )
    audio = models.CharField(
        max_length=400, blank=True, null=True, help_text="audio file url, relative to application's MEDIA_URL"
    )
    video = models.CharField(
        max_length=400, blank=True, null=True, help_text="video file url, relative to application's MEDIA_URL"
    )
    notes = models.TextField(verbose_name=_("notes"), blank=True, null=True)
    link = models.URLField(verbose_name=_("link"), blank=True, null=True, max_length=400)
    creation_date = models.DateTimeField(verbose_name=_("creation date"), auto_now_add=True)
    update_date = models.DateTimeField(verbose_name=_("last update date"), blank=True, null=True, auto_now=True)
    order_index = models.IntegerField(
        verbose_name=_("order index"), help_text=_("used to handle display order"), blank=True, null=True
    )

    def file_exists(self):
        result = True
        if self.score:
            if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, self.score)):
                result = False
        if self.audio:
            if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, self.audio)):
                result = False
        if self.video:
            if not os.path.isfile(os.path.join(settings.MEDIA_ROOT, self.video)):
                result = False
        return result

    def copy_as_temporary_element(self, composer, competition, key):
        """
        Copy the selected element as a temporary media
        Nota : the physical files are copied
        """
        from ulysses.competitions.models import create_temporary_media
        return create_temporary_media(
            competition, composer, key, self.title, self.score, self.audio, self.video, self.notes, self.link
        )

    def move_to_other_composer(self, composer):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        if self.score:
            self.score = move_file_to_other_personal_folder(composer, self.score)
        if self.audio:
            self.audio = move_file_to_other_personal_folder(composer, self.audio)
        if self.video:
            self.video = move_file_to_other_personal_folder(composer, self.video)
        self.save()

    def reassign_to_other_composer(self, composer, old_username):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        verbose = 'reassign_candidate_files' in sys.argv
        if self.score and old_username in self.score:
            if verbose:
                print('> move score', self.score, 'to personal folder', composer.user.username)
            self.score = move_file_to_other_personal_folder(composer, self.score)
        if self.audio and old_username in self.audio:
            if verbose:
                print('> move audio', self.audio, 'to personal folder', composer.user.username)
            self.audio = move_file_to_other_personal_folder(composer, self.audio)
        if self.video and old_username in self.video:
            if verbose:
                print('> move video', self.video, 'to personal folder', composer.user.username)
            self.video = move_file_to_other_personal_folder(composer, self.video)
        self.save()

    def file_exists_helper(self, file):
        return os.path.isfile(settings.MEDIA_ROOT + "/" + file)

    def score_file_exists(self):
        if self.score:
            return self.file_exists_helper(self.score)

    def audio_file_exists(self):
        if self.audio:
            return self.file_exists_helper(self.audio)

    def video_file_exists(self):
        if self.video:
            return self.file_exists_helper(self.video)

    def get_score_filename(self):
        if self.score:
            return os.path.basename(self.score)
        else:
            return None

    def get_audio_filename(self):
        if self.audio:
            return os.path.basename(self.audio)
        else:
            return None

    def get_video_filename(self):
        if self.video:
            return os.path.basename(self.video)
        else:
            return None

    def __str__(self):
        return self.title

    def get_link(self, label, file):
        return '<a href="{0}{1}" target="_blank">{2}</a>'.format(settings.MEDIA_URL, file, label)

    def get_html(self):
        tokens = []
        if self.score:
            tokens.append(self.get_link("score", self.score))
        if self.audio:
            tokens.append(self.get_link("audio", self.audio))
        if self.video:
            tokens.append(self.get_link("video", self.video))

        lines = []
        tokens_str = ", ".join(tokens)
        if tokens_str:
            tokens_str = ' ({0})'.format(tokens_str)
        lines.append("<div class='Media'>{0}{1}</div>".format(self.title, tokens_str))
        if self.link:
            lines.append('<div class="Link">{0}</div>'.format(get_media_link(self.link)))
        if self.notes:
            lines.append("<div class='Notes'>{0}</div>".format(self.notes))
        results = ""
        for line in lines:
            results += line
        return results

    class Meta:
        verbose_name = _("media")
        abstract = True
        ordering = ['creation_date',]


class Media(MediaBase):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)

    def on_post_delete(self):
        from ulysses.competitions.models import delete_media_helper
        delete_media_helper(self)


class DocumentBase(models.Model):
    """ Base class for documents """
    title = models.CharField(verbose_name=_("title"), max_length=400)
    file = models.CharField(
        verbose_name=_("file"), max_length=400, help_text="file url, relative to application's MEDIA_URL"
    )
    order_index = models.IntegerField(
        verbose_name=_("display order"), help_text=_("used to handle document display order"), blank=True, null=True
    )
    creation_date = models.DateTimeField(verbose_name=_("creation date"), auto_now_add=True)
    update_date = models.DateTimeField(verbose_name=_("update date"), blank=True, null=True, auto_now=True)

    def __str__(self):
        result = self.file
        options = []
        if self.creation_date:
            options.append("created %s" % self.creation_date)
        if self.update_date:
            options.append("updated %s" % self.update_date)
        if options:
            result += " ("
            result += ",".join(options)
            result += ")"
        return result

    def get_html(self):
        html = ''
        if self.file:
            html = '{2}: <a class="document-file-item" href="{0}{1}" target="_blank">{3}</a>'.format(
                settings.MEDIA_URL, self.file, self.title, os.path.split(self.file)[-1]
            )
        for extra_file in self.get_extra_files():
            html += extra_file.as_edit_html()
        return html

    def get_filename(self):
        return os.path.basename(self.file)

    def file_exists(self):
        return os.path.isfile(os.path.join(settings.MEDIA_ROOT, self.file))

    def copy_as_temporary_element(self, composer, competition, key):
        """
        Copy the selected element as a temporary document
        Nota : the physical file itself if copied
        """
        from ulysses.competitions.models import create_temporary_document
        value = create_temporary_document(competition, composer, key, self.title, self.file, self.get_extra_files())
        return value

    def move_to_other_composer(self, composer):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        self.file = move_file_to_other_personal_folder(composer, self.file)
        self.save()
        for extra_file in self.get_extra_files():
            extra_file.move_to_other_composer(composer)

    def reassign_to_other_composer(self, composer, old_username):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to personal folder', composer.user.username)
            self.file = move_file_to_other_personal_folder(composer, self.file)
            self.save()
            for extra_file in self.get_extra_files():
                extra_file.reassign_to_other_composer(composer, old_username)

    def get_extra_files(self):
        raise NotImplemented

    @property
    def as_html(self):
        return self.as_edit_html(True)

    def as_edit_html(self, preview=False):
        html = '{2}: <a class="document-file-item" href="{0}{1}" target="_blank">{3}</a>'.format(
            settings.MEDIA_URL, self.file, self.title, os.path.split(self.file)[-1]
        )
        for extra_file in self.get_extra_files():
            html += extra_file.as_edit_html(preview)
        return html

    class Meta:
        abstract = True
        verbose_name = _("document")
        ordering = ['creation_date', ]


class DocumentExtraFileBase(models.Model):
    file = models.CharField(
        verbose_name=_("file"), max_length=400, help_text="file url, relative to application's MEDIA_URL"
    )

    class Meta:
        abstract = True
        verbose_name = _("document extra file")

    def move_to_other_composer(self, composer):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        self.file = move_file_to_other_personal_folder(composer, self.file)
        self.save()

    def reassign_to_other_composer(self, composer, old_username):
        # Create new candidate media
        from ulysses.competitions.models import move_file_to_other_personal_folder
        if old_username in self.file:
            verbose = 'reassign_candidate_files' in sys.argv
            if verbose:
                print('> move file', self.file, 'to personal folder', composer.user.username)
            self.file = move_file_to_other_personal_folder(composer, self.file)
            self.save()

    @property
    def as_html(self):
        html = '<span class="document-file-item"><a href="{0}{1}" target="_blank">{2}</a></span>'.format(
            settings.MEDIA_URL, self.file, os.path.split(self.file)[-1]
        )
        return html

    def as_edit_html(self, preview=False):
        html = '<span class="document-file-item"><a href="{0}{1}" target="_blank">{2}</a></span>'.format(
            settings.MEDIA_URL, self.file, os.path.split(self.file)[-1]
        )
        return html


class Document(DocumentBase):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)

    def on_post_delete(self):
        # Delete physical file, if it exists
        from ulysses.competitions.models import delete_file_helper
        delete_file_helper(self.file)

    def get_extra_files(self):
        return self.extra_files.all()


class DocumentExtraFile(DocumentExtraFileBase):
    document = models.ForeignKey(Document, on_delete=models.CASCADE, related_name='extra_files')


class BiographicElementBase(models.Model):
    """
    Base class for biographic elements
    """
    title = models.CharField(max_length=400)
    text = models.TextField(blank=True)
    order_index = models.IntegerField(
        verbose_name=_("order index"), help_text=_("used to handle document display"), blank=True, null=True
    )
    creation_date = models.DateTimeField(verbose_name=_("creation date"), auto_now_add=True)
    update_date = models.DateTimeField(verbose_name=_("last update date"), blank=True, null=True, auto_now=True)

    def get_html(self):
        return self.text

    @property
    def as_html(self):
        html = self.get_html()
        value = ''.join(html.replace("'", "\\'").splitlines())
        return value

    class Meta:
        abstract = True
        verbose_name = _("biographic detail")
        verbose_name_plural = _("biographic details")
        ordering = ['creation_date',]


class BiographicElement(BiographicElementBase):
    composer = models.ForeignKey(Composer, on_delete=models.CASCADE)


