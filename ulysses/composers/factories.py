import factory
import datetime

from django.contrib.auth.models import User

from .models import Composer, Media, Document, BiographicElement
from ulysses.reference.factories import CitizenshipFactory, CountryFactory


class UserFactory(factory.DjangoModelFactory):
    class Meta:
        model = User

    username = factory.Sequence(lambda n: 'testuser{0}'.format(n))
    first_name = factory.Sequence(lambda n: 'first-name{0}'.format(n))
    last_name = factory.Sequence(lambda n: 'last-name{0}'.format(n))
    email = factory.Sequence(lambda n: 'email{0}@example.fr'.format(n))
    password = "1234"

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        password = kwargs.pop('password', None)
        user = super(UserFactory, cls)._create(model_class, *args, **kwargs)
        if password:
            user.set_password(password)
            user.save()
        return user


class ComposerFactory(factory.DjangoModelFactory):
    class Meta:
        model = Composer

    user = factory.SubFactory(UserFactory)
    birth_date = datetime.date(1981, 1, 1)
    citizenship = factory.SubFactory(CitizenshipFactory)
    country = factory.SubFactory(CountryFactory)


class MediaFactory(factory.DjangoModelFactory):
    class Meta:
        model = Media


class DocumentFactory(factory.DjangoModelFactory):
    class Meta:
        model = Document


class BiographicElementFactory(factory.DjangoModelFactory):
    class Meta:
        model = BiographicElement
