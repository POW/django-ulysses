# -*- coding: utf-8 -*-

from ulysses.generic.app import BaseAppConfig


class AppConfig(BaseAppConfig):
    name = 'ulysses.composers'
    label = 'ulysses_composers'
    verbose_name = "Ulysses Composers"
