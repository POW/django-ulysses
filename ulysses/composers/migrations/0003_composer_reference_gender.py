# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-09-26 15:09


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_reference', '0005_gender'),
        ('ulysses_composers', '0002_auto_20170518_2309'),
    ]

    operations = [
        migrations.AddField(
            model_name='composer',
            name='reference_gender',
            field=models.ForeignKey(blank=True, default=None, null=True, on_delete=django.db.models.deletion.CASCADE, to='ulysses_reference.Gender'),
        ),
    ]
