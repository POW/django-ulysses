# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-09-26 15:14


from django.db import migrations


def forwards_func(apps, schema_editor):
    """create profiles models from ulysses.community model"""
    composer_gender_model = apps.get_model("ulysses_composers", "Gender")
    reference_gender_model = apps.get_model("ulysses_reference", "Gender")
    composer_model = apps.get_model("ulysses_composers", "Composer")
    individual_model = apps.get_model("ulysses_profiles", "Individual")
    organization_model = apps.get_model("ulysses_profiles", "Organization")

    for composer_gender in composer_gender_model.objects.all():
        reference_gender_model.objects.get_or_create(name=composer_gender.name)

    for composer in composer_model.objects.all():
        try:
            profile = individual_model.objects.get(user=composer.user)
        except individual_model.DoesNotExist:
            print(">", composer.user.email, "has no individual profile")
            try:
                profile = None  #
                organization_model.objects.get(user=composer.user)
            except organization_model.DoesNotExist:
                print(">", composer.user.email, "has no organization profile")
                profile = None

        if profile:
            if composer.gender:
                reference_gender = reference_gender_model.objects.get(name=composer.gender.name)
                composer.reference_gender = reference_gender
                composer.save()

                profile.gender = reference_gender

            if profile.city == composer.city:
                profile.address1 = composer.address1
                profile.address2 = composer.address2
                profile.zipcode = composer.zipcode
            else:
                print("#", composer.user.email, "changed city from", composer.city, "to", profile.city)

            profile.phone1 = composer.phone1
            profile.phone2 = composer.phone2
            profile.save()


def reverse_func(apps, schema_editor):
    """delete everything created byt forward func"""

    reference_gender_model = apps.get_model("ulysses_reference", "Gender")
    composer_model = apps.get_model("ulysses_composers", "Composer")
    individual_model = apps.get_model("ulysses_profiles", "Individual")
    organization_model = apps.get_model("ulysses_profiles", "Organization")

    for composer in composer_model.objects.all():
        try:
            profile = individual_model.objects.get(user=composer.user)
        except individual_model.DoesNotExist:
            print(">", composer.user.email, "has no individual profile")
            try:
                organization_model.objects.get(user=composer.user)
                profile = None
            except organization_model.DoesNotExist:
                print(">", composer.user.email, "has no organization profile")
                profile = None

        if profile:
            profile.gender = None
            profile.address1 = ""
            profile.address2 = ""
            profile.phone1 = ""
            profile.phone2 = ""
            profile.zipcode = ""
            profile.save()

        composer.reference_gender = None
        composer.save()

        # reference_gender_model.objects.all().delete()


class Migration(migrations.Migration):

    dependencies = [
        ('ulysses_composers', '0003_composer_reference_gender'),
        ('ulysses_reference', '0005_gender'),
        ('ulysses_profiles','0029_auto_20170926_1507'),
    ]

    operations = [
        migrations.RunPython(forwards_func, reverse_func)
    ]
