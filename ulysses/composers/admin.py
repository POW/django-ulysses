# -*- coding: utf-8 -*-

from django.conf import settings
from django.contrib.admin import ModelAdmin, TabularInline

from ulysses.composers.models import Composer, Gender
from ulysses.composers.models import Media, Document, BiographicElement, DocumentExtraFile
from ulysses.super_admin.sites import super_admin_site


class BiographicElementInline(TabularInline):
    model = BiographicElement


class DocumentInline(TabularInline):
    model = Document


class ComposerAdmin(ModelAdmin):
    list_display = (
        'id', 'last_name', 'first_name', 'username', 'email', 'birth_date', 'zipcode', 'city', 'country', 'website'
    )
    list_display_links = ('id', 'last_name',)
    search_fields = ['user__username', 'user__last_name', 'user__first_name', 'user__email']
    inlines = [BiographicElementInline, DocumentInline]
    date_hierarchy = "birth_date"

    def last_name(self, obj):
      return obj.user.last_name
    last_name.short_description = 'last name'
    
    def username(self, obj):
      return obj.user.username
    last_name.short_description = 'username'

    def email(self, obj):
        return obj.user.email
    email.short_description = 'email'
    
    def first_name(self, obj):
      return obj.user.first_name
    first_name.short_description = 'first name'

    fieldsets = (
        ('Etat-civil', {
            'classes': ('wide',),
            'fields': ('user','gender','birth_date','citizenship')  
        }),        
        ('Adresse postale', {
            'classes': ('wide',),
            'fields': ('address1','address2','zipcode','city','country','phone1','phone2')                
        }),
        ('Divers', {
            'classes': ('wide',),
            'fields': ('website',)                
        }),        
    )
    
    class Media:
            js = (
            '%sjs/tiny_mce/tiny_mce.js' % settings.STATIC_URL,
            '%sjs/admin_pages.js' % settings.STATIC_URL
        )
    

class MediaAdmin(ModelAdmin):
    list_display = ('composer', 'id', 'title', 'creation_date', 'update_date')
    list_display_links = ('id', 'title',)
    list_filter = ['composer',]
    search_fields = ['id', 'composer__user__username', 'composer__user__last_name', 'title']


class DocumentExtraFileInline(TabularInline):
    model = DocumentExtraFile
    fields = ['file', ]
    extra = 0


class DocumentAdmin(ModelAdmin):
    list_display = ('composer', 'id', 'title', 'creation_date', 'update_date')
    list_display_links = ('id', 'title', )
    raw_id_fields = ['composer', ]
    search_fields = ['id', 'composer__user__username', 'composer__user__last_name', 'title']
    inlines = [DocumentExtraFileInline, ]


class BiographicElementAdmin(ModelAdmin):
    list_display = ('composer', 'id', 'title', 'creation_date', 'update_date')
    list_display_links = ('id', 'title',)
    list_filter = ['composer',]
    search_fields = ['id', 'composer__user__username', 'composer__user__last_name', 'title']


class GenderAdmin(ModelAdmin):
    pass


def register(site):
    site.register(Composer,ComposerAdmin)
    site.register(Media, MediaAdmin)

# Global administration registration
super_admin_site.register(Gender, GenderAdmin)
super_admin_site.register(Composer, ComposerAdmin)
super_admin_site.register(Media, MediaAdmin)
super_admin_site.register(Document, DocumentAdmin)
super_admin_site.register(BiographicElement, BiographicElementAdmin)
